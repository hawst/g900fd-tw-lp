.class public Lcom/android/emailcommon/internet/Rfc822Output;
.super Ljava/lang/Object;
.source "Rfc822Output.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/internet/Rfc822Output$BodyTextHtml;
    }
.end annotation


# static fields
.field private static final PATTERN_ENDLINE_CRLF:Ljava/util/regex/Pattern;

.field private static final PATTERN_ENDLINE_LF:Ljava/util/regex/Pattern;

.field private static final PATTERN_START_OF_LINE:Ljava/util/regex/Pattern;

.field static final mDateFormat:Lorg/apache/commons/lang/time/FastDateFormat;

.field private static mDeviceId:Ljava/lang/String;

.field private static mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

.field private static mIsExchangeAccount:Z

.field private static mPolicySet:Lcom/android/emailcommon/service/PolicySet;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    const-string v0, "(?m)^"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/Rfc822Output;->PATTERN_START_OF_LINE:Ljava/util/regex/Pattern;

    .line 88
    const-string v0, "\r\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/Rfc822Output;->PATTERN_ENDLINE_CRLF:Ljava/util/regex/Pattern;

    .line 90
    const-string v0, "\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/Rfc822Output;->PATTERN_ENDLINE_LF:Ljava/util/regex/Pattern;

    .line 92
    sput-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 126
    const-string v0, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, v1}, Lorg/apache/commons/lang/time/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/Locale;)Lorg/apache/commons/lang/time/FastDateFormat;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/Rfc822Output;->mDateFormat:Lorg/apache/commons/lang/time/FastDateFormat;

    .line 128
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/emailcommon/internet/Rfc822Output;->mIsExchangeAccount:Z

    .line 129
    sput-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 130
    sput-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mDeviceId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    return-void
.end method

.method private static addAttachment(Landroid/content/Context;JLcom/android/emailcommon/smime/SMIMEHelper$Message;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "msg"    # Lcom/android/emailcommon/smime/SMIMEHelper$Message;

    .prologue
    const/4 v3, 0x0

    .line 554
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->MESSAGE_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 555
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 558
    .local v8, "attachmentsCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 559
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 560
    const-class v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-static {v8, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v7

    check-cast v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 563
    .local v7, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 564
    new-instance v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;

    invoke-direct {v6}, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;-><init>()V

    .line 565
    .local v6, "att":Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mUri:Landroid/net/Uri;

    .line 566
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    iput-object v0, v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    .line 572
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v2, "CscFeature_Email_ReplaceUnsupportedCharWithUnderbarInAttachment"

    invoke-virtual {v0, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/emailcommon/internet/Rfc822Output;->writeModifiedFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    .line 581
    :cond_1
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 582
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    iput-object v0, v6, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mContentId:Ljava/lang/String;

    .line 585
    :cond_2
    iget-object v0, p3, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 589
    .end local v6    # "att":Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;
    .end local v7    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 591
    :cond_4
    return-void
.end method

.method static buildBodyText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "appendQuotedText"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 136
    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {p0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v1

    .line 137
    .local v1, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    if-nez v1, :cond_0

    .line 138
    const/4 v8, 0x0

    .line 182
    :goto_0
    return-object v8

    .line 141
    :cond_0
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 143
    .local v7, "sbText":Ljava/lang/StringBuffer;
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 144
    .local v2, "flags":I
    and-int/lit8 v9, v2, 0x1

    if-eqz v9, :cond_4

    move v6, v8

    .line 145
    .local v6, "isReply":Z
    :goto_1
    and-int/lit8 v9, v2, 0x2

    if-eqz v9, :cond_1

    move v5, v8

    .line 147
    .local v5, "isForward":Z
    :cond_1
    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    if-nez v9, :cond_5

    const-string v3, ""

    .line 149
    .local v3, "intro":Ljava/lang/String;
    :goto_2
    const-string v9, "&lt;"

    const-string v10, "<"

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 150
    const-string v9, "&gt;"

    const-string v10, ">"

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 151
    const-string v9, "<br>"

    const-string v10, "\n"

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 153
    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {p0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 155
    .local v0, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    invoke-virtual {v0, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->getStoreUri(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "eas"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    .line 157
    .local v4, "isExchange":Z
    if-ne v8, p2, :cond_6

    .line 158
    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v9, :cond_2

    .line 159
    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 161
    :cond_2
    if-eq v8, v6, :cond_3

    if-ne v8, v5, :cond_7

    .line 162
    :cond_3
    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 165
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .end local v0    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v3    # "intro":Ljava/lang/String;
    .end local v4    # "isExchange":Z
    .end local v5    # "isForward":Z
    .end local v6    # "isReply":Z
    :cond_4
    move v6, v5

    .line 144
    goto :goto_1

    .line 147
    .restart local v5    # "isForward":Z
    .restart local v6    # "isReply":Z
    :cond_5
    iget-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    goto :goto_2

    .line 168
    .restart local v0    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v3    # "intro":Ljava/lang/String;
    .restart local v4    # "isExchange":Z
    :cond_6
    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 173
    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    :cond_7
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    if-gtz v8, :cond_8

    .line 179
    const-string v8, ""

    goto :goto_0

    .line 182
    :cond_8
    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method static buildBodyTextHtml(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "appendQuotedText"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 202
    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {p0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v1

    .line 203
    .local v1, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    if-nez v1, :cond_0

    .line 233
    :goto_0
    return-object v8

    .line 208
    :cond_0
    iget-object v9, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 209
    new-instance v6, Lcom/android/emailcommon/mail/PackedString;

    iget-object v9, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    invoke-direct {v6, v9}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 210
    .local v6, "packedString":Lcom/android/emailcommon/mail/PackedString;
    const-string v9, "1"

    const-string v10, "MEETING_FORWARD"

    invoke-virtual {v6, v10}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 211
    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    goto :goto_0

    .line 215
    .end local v6    # "packedString":Lcom/android/emailcommon/mail/PackedString;
    :cond_1
    const/4 v0, 0x0

    .line 217
    .local v0, "BodyHtml":Ljava/lang/String;
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 218
    .local v2, "flags":I
    and-int/lit8 v9, v2, 0x1

    if-eqz v9, :cond_6

    move v5, v7

    .line 219
    .local v5, "isReply":Z
    :goto_1
    and-int/lit8 v9, v2, 0x2

    if-eqz v9, :cond_2

    move v4, v7

    .line 220
    .local v4, "isForward":Z
    :cond_2
    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    if-nez v9, :cond_7

    const-string v3, ""

    .line 225
    .local v3, "intro":Ljava/lang/String;
    :goto_2
    if-eq v7, v5, :cond_3

    if-ne v7, v4, :cond_4

    .line 226
    :cond_3
    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-ne v7, p2, :cond_8

    iget-object v7, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    :goto_3
    invoke-static {v9, v7, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->getIntergratedHTML(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    :cond_4
    if-nez v0, :cond_5

    .line 231
    iget-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    :cond_5
    move-object v8, v0

    .line 233
    goto :goto_0

    .end local v3    # "intro":Ljava/lang/String;
    .end local v4    # "isForward":Z
    .end local v5    # "isReply":Z
    :cond_6
    move v5, v4

    .line 218
    goto :goto_1

    .line 220
    .restart local v4    # "isForward":Z
    .restart local v5    # "isReply":Z
    :cond_7
    iget-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    goto :goto_2

    .restart local v3    # "intro":Ljava/lang/String;
    :cond_8
    move-object v7, v8

    .line 226
    goto :goto_3
.end method

.method private static getEmailPriority(I)Ljava/lang/String;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 346
    packed-switch p0, :pswitch_data_0

    .line 354
    const-string v0, "normal"

    :goto_0
    return-object v0

    .line 348
    :pswitch_0
    const-string v0, "high"

    goto :goto_0

    .line 350
    :pswitch_1
    const-string v0, "normal"

    goto :goto_0

    .line 352
    :pswitch_2
    const-string v0, "low"

    goto :goto_0

    .line 346
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getEncryptAlforithm(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;
    .locals 4
    .param p0, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 615
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/emailcommon/internet/Rfc822Output;->getEncryptionAlgorithm(I)Ljava/lang/String;

    move-result-object v0

    .line 616
    .local v0, "encryptionAlgorithm":Ljava/lang/String;
    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget-boolean v1, v1, Lcom/android/emailcommon/service/PolicySet;->mRequireEncryptedSMIMEMessages:Z

    if-eqz v1, :cond_1

    .line 617
    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v1, v1, Lcom/android/emailcommon/service/PolicySet;->mRequireEncryptionSMIMEAlgorithm:I

    invoke-static {v1}, Lcom/android/emailcommon/internet/Rfc822Output;->getEncryptionAlgorithm(I)Ljava/lang/String;

    move-result-object v0

    .line 623
    :cond_0
    :goto_0
    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v1, :cond_3

    .line 624
    const-string v1, "RFC822Output"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPolicySet.mRequireEncryptionSMIMEAlgorithm = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v3, v3, Lcom/android/emailcommon/service/PolicySet;->mRequireEncryptionSMIMEAlgorithm:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    :goto_1
    return-object v0

    .line 618
    :cond_1
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncryptionAlgorithm:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 619
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncryptionAlgorithm:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/android/emailcommon/internet/Rfc822Output;->getEncryptionAlgorithm(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 620
    :cond_2
    if-eqz p1, :cond_0

    iget v1, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mSmimeEncryptionAlgorithm:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_0

    .line 621
    iget v1, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mSmimeEncryptionAlgorithm:I

    invoke-static {v1}, Lcom/android/emailcommon/internet/Rfc822Output;->getEncryptionAlgorithm(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 627
    :cond_3
    const-string v1, "RFC822Output"

    const-string v2, "mPolicySet is NULL"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static getEncryptionAlgorithm(I)Ljava/lang/String;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 460
    packed-switch p0, :pswitch_data_0

    .line 472
    const-string v0, "-des3"

    :goto_0
    return-object v0

    .line 462
    :pswitch_0
    const-string v0, "-des3"

    goto :goto_0

    .line 464
    :pswitch_1
    const-string v0, "-des"

    goto :goto_0

    .line 466
    :pswitch_2
    const-string v0, "-rc2-128"

    goto :goto_0

    .line 468
    :pswitch_3
    const-string v0, "-rc2-64"

    goto :goto_0

    .line 470
    :pswitch_4
    const-string v0, "-rc2-40"

    goto :goto_0

    .line 460
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getIntergratedHTML(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "newHtml"    # Ljava/lang/String;
    .param p1, "htmlReply"    # Ljava/lang/String;
    .param p2, "IntroText"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 282
    const/4 v4, 0x0

    .line 283
    .local v4, "tmpString":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 285
    .local v0, "IntergratedHTML":Ljava/lang/StringBuffer;
    if-eqz p0, :cond_4

    .line 286
    move-object v4, p0

    .line 288
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_Framework_EnableBidirection"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 289
    if-nez p1, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<body dir=\"rtl\">"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v8, v5, :cond_0

    .line 294
    const-string v5, "<div dir=\"RTL\">"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 297
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "</body>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v8, v5, :cond_1

    .line 298
    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "</body>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    :cond_1
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 304
    if-eqz p1, :cond_2

    .line 305
    const/4 v2, -0x1

    .line 306
    .local v2, "startIndexOfBODY":I
    move-object v4, p1

    .line 307
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<body"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v8, v5, :cond_3

    .line 308
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<body"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 309
    .local v3, "tmp":I
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 310
    .local v1, "startBody":Ljava/lang/String;
    const-string v5, ">"

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v2, v5, 0x1

    .line 313
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 334
    .end local v1    # "startBody":Ljava/lang/String;
    .end local v2    # "startIndexOfBODY":I
    .end local v3    # "tmp":I
    :cond_2
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    if-gtz v5, :cond_6

    .line 335
    const/4 v5, 0x0

    .line 338
    :goto_1
    return-object v5

    .line 315
    .restart local v2    # "startIndexOfBODY":I
    :cond_3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "</body></html>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 318
    .end local v2    # "startIndexOfBODY":I
    :cond_4
    if-eqz p1, :cond_2

    .line 319
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    const/4 v2, -0x1

    .line 322
    .restart local v2    # "startIndexOfBODY":I
    move-object v4, p1

    .line 323
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<body"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-ne v8, v5, :cond_5

    .line 324
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<body"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 325
    .restart local v3    # "tmp":I
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 326
    .restart local v1    # "startBody":Ljava/lang/String;
    const-string v5, ">"

    invoke-virtual {v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v2, v5, 0x1

    .line 328
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 330
    .end local v1    # "startBody":Ljava/lang/String;
    .end local v3    # "tmp":I
    :cond_5
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "</body></html>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 338
    .end local v2    # "startIndexOfBODY":I
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method public static getIsExchangeAccount()Z
    .locals 1

    .prologue
    .line 1942
    sget-boolean v0, Lcom/android/emailcommon/internet/Rfc822Output;->mIsExchangeAccount:Z

    return v0
.end method

.method private static getSMIMEHelper(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Lcom/android/emailcommon/smime/SMIMEHelper;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3, "credentialAccount"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/cac/CACException;
        }
    .end annotation

    .prologue
    .line 533
    const/4 v1, 0x0

    .line 535
    .local v1, "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    const-string v2, "RFC822Output"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CAC debug] credentialAccount is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    if-eqz p3, :cond_1

    .line 537
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getCACState(Landroid/content/Context;)I

    move-result v0

    .line 538
    .local v0, "cacStatus":I
    const-string v2, "RFC822Output"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[CAC debug] cacStatus is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    if-nez v0, :cond_0

    .line 540
    new-instance v1, Lcom/android/emailcommon/smime/SMIMEHelper;

    .end local v1    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getProvider(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/emailcommon/smime/SMIMEHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 549
    .end local v0    # "cacStatus":I
    .restart local v1    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    :goto_0
    return-object v1

    .line 542
    .restart local v0    # "cacStatus":I
    :cond_0
    const-string v2, "RFC822Output"

    const-string v3, "[CAC debug] throw new CACException(cacStatus)"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    new-instance v2, Lcom/android/emailcommon/cac/CACException;

    invoke-direct {v2, v0}, Lcom/android/emailcommon/cac/CACException;-><init>(I)V

    throw v2

    .line 546
    .end local v0    # "cacStatus":I
    :cond_1
    new-instance v1, Lcom/android/emailcommon/smime/SMIMEHelper;

    .end local v1    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    invoke-direct {v1, p0}, Lcom/android/emailcommon/smime/SMIMEHelper;-><init>(Landroid/content/Context;)V

    .restart local v1    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    goto :goto_0
.end method

.method private static getSigningAlgorithm(Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;
    .locals 4
    .param p0, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v3, -0x1

    .line 594
    sget-object v1, Lcom/android/emailcommon/smime/SMIMEHelper;->SIGNING_ALGORITHMS:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 595
    .local v0, "signingAlgorithm":Ljava/lang/String;
    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget-boolean v1, v1, Lcom/android/emailcommon/service/PolicySet;->mRequireSignedSMIMEMessages:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v1, v1, Lcom/android/emailcommon/service/PolicySet;->mRequireSignedSMIMEAlgorithm:I

    if-le v1, v3, :cond_1

    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v1, v1, Lcom/android/emailcommon/service/PolicySet;->mRequireSignedSMIMEAlgorithm:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 598
    sget-object v1, Lcom/android/emailcommon/smime/SMIMEHelper;->SIGNING_ALGORITHMS:[Ljava/lang/String;

    sget-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v2, v2, Lcom/android/emailcommon/service/PolicySet;->mRequireSignedSMIMEAlgorithm:I

    aget-object v0, v1, v2

    .line 602
    :cond_0
    :goto_0
    sget-object v1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v1, :cond_2

    .line 603
    const-string v1, "RFC822Output"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPolicySet.mRequireSignedSMIMEAlgorithm= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget v3, v3, Lcom/android/emailcommon/service/PolicySet;->mRequireSignedSMIMEAlgorithm:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    :goto_1
    const-string v1, "RFC822Output"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "signingAlgorithm= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    return-object v0

    .line 599
    :cond_1
    if-eqz p0, :cond_0

    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSmimeSignAlgorithm:I

    if-le v1, v3, :cond_0

    .line 600
    sget-object v1, Lcom/android/emailcommon/smime/SMIMEHelper;->SIGNING_ALGORITHMS:[Ljava/lang/String;

    iget v2, p0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSmimeSignAlgorithm:I

    aget-object v0, v1, v2

    goto :goto_0

    .line 606
    :cond_2
    const-string v1, "RFC822Output"

    const-string v2, "mPolicySet is NULL!"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static isHandleMeetingInline(Landroid/database/Cursor;Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    .locals 5
    .param p0, "attachmentsCursor"    # Landroid/database/Cursor;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 1069
    const/4 v2, 0x0

    .local v2, "isMeeting":Z
    const/4 v1, 0x0

    .line 1071
    .local v1, "isInline":Z
    if-eqz p0, :cond_4

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_4

    .line 1072
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1075
    :cond_0
    const-class v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-static {p0, v3}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1076
    .local v0, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v4, "text/calendar; method=REPLY"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v4, "text/calendar; method=COUNTER"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1078
    :cond_1
    const/4 v2, 0x1

    .line 1080
    :cond_2
    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1081
    const/4 v1, 0x1

    .line 1083
    :cond_3
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1086
    .end local v0    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_4
    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    .line 1087
    const/4 v3, 0x1

    .line 1089
    :goto_0
    return v3

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static isHandleMeetingInlineForward(Landroid/database/Cursor;Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    .locals 5
    .param p0, "attachmentsCursor"    # Landroid/database/Cursor;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 1099
    const/4 v1, 0x0

    .local v1, "isInline":Z
    const/4 v2, 0x0

    .line 1100
    .local v2, "isMeetingForward":Z
    if-eqz p0, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 1101
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1103
    :cond_0
    const-class v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-static {p0, v3}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1104
    .local v0, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1105
    const/4 v1, 0x1

    .line 1107
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1110
    .end local v0    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_2
    iget v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1111
    const/4 v2, 0x1

    .line 1114
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 1115
    const/4 v3, 0x1

    .line 1118
    :goto_0
    return v3

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static setIsExchangeAccount(ZLcom/android/emailcommon/service/PolicySet;Ljava/lang/String;)V
    .locals 0
    .param p0, "isExchangeAcoount"    # Z
    .param p1, "ps"    # Lcom/android/emailcommon/service/PolicySet;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 1936
    sput-boolean p0, Lcom/android/emailcommon/internet/Rfc822Output;->mIsExchangeAccount:Z

    .line 1937
    sput-object p1, Lcom/android/emailcommon/internet/Rfc822Output;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 1938
    sput-object p2, Lcom/android/emailcommon/internet/Rfc822Output;->mDeviceId:Ljava/lang/String;

    .line 1939
    return-void
.end method

.method private static writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1858
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1859
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1860
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1861
    invoke-static {p2}, Lcom/android/emailcommon/mail/Address;->packedToHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1862
    .local v0, "packedToHeader":Ljava/lang/String;
    if-eqz v0, :cond_1

    .end local v0    # "packedToHeader":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MimeUtility;->fold(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1863
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1865
    :cond_0
    return-void

    .line 1862
    .restart local v0    # "packedToHeader":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private static writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V
    .locals 1
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "boundary"    # Ljava/lang/String;
    .param p2, "end"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1876
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1877
    if-nez p2, :cond_0

    .line 1883
    :cond_0
    const-string v0, "--"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1884
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1885
    if-eqz p2, :cond_1

    .line 1886
    const-string v0, "--"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1888
    :cond_1
    if-nez p2, :cond_2

    .line 1889
    const-string v0, "\r\n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1890
    :cond_2
    return-void
.end method

.method private static writeData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "appendQuotedText"    # Z
    .param p3, "writer"    # Ljava/io/Writer;
    .param p4, "stream"    # Ljava/io/OutputStream;
    .param p5, "bIsAttachment"    # Ljava/lang/Boolean;
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "attachmentsCursor"    # Landroid/database/Cursor;
    .param p8, "plainText"    # Ljava/lang/String;
    .param p9, "HTMLText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1393
    const/4 v6, 0x0

    .line 1394
    .local v6, "boundary_offset":I
    const/4 v12, 0x0

    .line 1395
    .local v12, "multipartSubType":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1396
    .local v9, "multipartBoundary1":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1397
    .local v10, "multipartBoundary2":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1399
    .local v11, "multipartBoundary3":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1400
    .local v8, "inlineCount":I
    const/4 v5, 0x0

    .line 1402
    .local v5, "attachmentCount":I
    if-nez p1, :cond_1

    .line 1572
    :cond_0
    :goto_0
    return-void

    .line 1406
    :cond_1
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "--_com.android.email_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v7, v6, 0x1

    .end local v6    # "boundary_offset":I
    .local v7, "boundary_offset":I
    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1408
    invoke-virtual/range {p5 .. p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_2

    if-nez p6, :cond_12

    .line 1409
    :cond_2
    const-string v12, "alternative"

    .line 1434
    :goto_1
    const-string v13, "Content-Type"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "multipart/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "; boundary=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-static {v0, v13, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1437
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v9, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1439
    const/4 v13, 0x1

    const-string v14, "mixed"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-ne v13, v14, :cond_3

    .line 1440
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "--_com.android.email_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "boundary_offset":I
    .restart local v6    # "boundary_offset":I
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1442
    const-string v13, "Content-Type"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "multipart/alternative; boundary=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-static {v0, v13, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    move v7, v6

    .line 1447
    .end local v6    # "boundary_offset":I
    .restart local v7    # "boundary_offset":I
    :cond_3
    if-nez p8, :cond_4

    .line 1449
    const-string p8, ""

    .line 1451
    :cond_4
    if-eqz v10, :cond_5

    .line 1452
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v10, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1456
    :cond_5
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/android/emailcommon/provider/EmailContent;->isSNCAccount(Landroid/content/Context;Ljava/lang/Long;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1457
    const-string v13, "Content-Disposition"

    const-string v14, "inline"

    move-object/from16 v0, p3

    invoke-static {v0, v13, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1460
    :cond_6
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p8

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTextWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1461
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1463
    if-nez p9, :cond_7

    .line 1464
    if-eqz p8, :cond_17

    .line 1465
    const-string v13, "<"

    const-string v14, "&lt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1466
    const-string v13, ">"

    const-string v14, "&gt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1467
    const-string v13, "\n"

    const-string v14, "<br>"

    move-object/from16 v0, p8

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1475
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "<html><head></head><body><div style=\"word-break;keep-all;\">"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p8

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "</div></body></html>"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p9

    .line 1481
    :cond_7
    :goto_2
    if-eqz v10, :cond_18

    .line 1482
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v10, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1487
    :goto_3
    if-lez v8, :cond_1c

    .line 1489
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "--_com.android.email_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    add-int/lit8 v6, v7, 0x1

    .end local v7    # "boundary_offset":I
    .restart local v6    # "boundary_offset":I
    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 1491
    const-string v13, "Content-Type"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "multipart/relative; boundary=\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-static {v0, v13, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1496
    :goto_4
    if-eqz v11, :cond_8

    .line 1497
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v11, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1501
    :cond_8
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/android/emailcommon/provider/EmailContent;->isSNCAccount(Landroid/content/Context;Ljava/lang/Long;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 1502
    const-string v13, "Content-Disposition"

    const-string v14, "inline"

    move-object/from16 v0, p3

    invoke-static {v0, v13, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    :cond_9
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p9

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHTMLWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1506
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1508
    if-eqz v11, :cond_d

    .line 1509
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1510
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_c

    .line 1512
    :cond_a
    const-class v13, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v13}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1514
    .local v4, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v4, :cond_b

    iget-object v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v13, :cond_b

    if-eqz p9, :cond_b

    const/4 v13, 0x1

    iget-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-ne v13, v14, :cond_b

    .line 1519
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v13

    if-eqz v13, :cond_19

    if-nez p2, :cond_19

    iget v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v13, v13, 0x10

    if-eqz v13, :cond_19

    .line 1528
    :cond_b
    :goto_5
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_a

    .line 1531
    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_c
    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v11, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1532
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1535
    :cond_d
    if-eqz v10, :cond_1a

    .line 1536
    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v10, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1537
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1538
    const/4 v10, 0x0

    .line 1545
    :goto_6
    const/4 v13, 0x1

    const-string v14, "mixed"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-ne v13, v14, :cond_0

    .line 1546
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1547
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_11

    .line 1549
    :cond_e
    const-class v13, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v13}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1551
    .restart local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v4, :cond_10

    iget-object v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v13, :cond_f

    iget-object v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v13, :cond_10

    if-eqz p9, :cond_10

    const/4 v13, 0x1

    iget-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eq v13, v14, :cond_10

    .line 1557
    :cond_f
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v13

    if-eqz v13, :cond_1b

    if-nez p2, :cond_1b

    iget v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v13, v13, 0x10

    if-eqz v13, :cond_1b

    .line 1566
    :cond_10
    :goto_7
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_e

    .line 1569
    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_11
    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v9, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1570
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1411
    .end local v6    # "boundary_offset":I
    .restart local v7    # "boundary_offset":I
    :cond_12
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1412
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_14

    .line 1414
    :cond_13
    const-class v13, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v13}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1416
    .restart local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v13, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v13, :cond_15

    if-eqz p9, :cond_15

    const/4 v13, 0x1

    iget-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-ne v13, v14, :cond_15

    .line 1421
    add-int/lit8 v8, v8, 0x1

    .line 1425
    :goto_8
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_13

    .line 1427
    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_14
    if-lez v5, :cond_16

    .line 1428
    const-string v12, "mixed"

    goto/16 :goto_1

    .line 1423
    .restart local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_15
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 1430
    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_16
    const-string v12, "alternative"

    goto/16 :goto_1

    .line 1478
    :cond_17
    const-string p9, "<html><head></head><body><div style=\"word-break;keep-all;\"></div></body></html>"

    goto/16 :goto_2

    .line 1484
    :cond_18
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v9, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 1524
    .end local v7    # "boundary_offset":I
    .restart local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .restart local v6    # "boundary_offset":I
    :cond_19
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v11, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1525
    const/4 v13, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2, v4, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1526
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1540
    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_1a
    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v9, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1541
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1542
    const/4 v9, 0x0

    goto/16 :goto_6

    .line 1562
    .restart local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_1b
    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v9, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1563
    const/4 v13, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2, v4, v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1564
    const-string v13, "\r\n"

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_7

    .end local v4    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v6    # "boundary_offset":I
    .restart local v7    # "boundary_offset":I
    :cond_1c
    move v6, v7

    .end local v7    # "boundary_offset":I
    .restart local v6    # "boundary_offset":I
    goto/16 :goto_4
.end method

.method private static writeEncodedHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1768
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 1769
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1770
    const-string v0, ": "

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1771
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {p2, v0}, Lcom/android/emailcommon/internet/MimeUtility;->foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1772
    const-string v0, "\r\n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1774
    :cond_0
    return-void
.end method

.method private static writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1785
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 1786
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1787
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1788
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {p2, v1}, Lcom/android/emailcommon/internet/MimeUtility;->foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1790
    .local v0, "foldedString":Ljava/lang/String;
    const-string v1, "\r\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1792
    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1793
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1795
    .end local v0    # "foldedString":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static writeHTMLWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 3
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1927
    const-string v1, "Content-Type"

    const-string v2, "text/html; charset=utf-8"

    invoke-static {p0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1928
    const-string v1, "Content-Transfer-Encoding"

    const-string v2, "base64"

    invoke-static {p0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1929
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1930
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 1931
    .local v0, "bytes":[B
    invoke-virtual {p0}, Ljava/io/Writer;->flush()V

    .line 1932
    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 1933
    return-void
.end method

.method private static writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1745
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 1746
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1747
    const-string v0, ": "

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1748
    const-string v0, "Content-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1749
    const-string v0, "<"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1751
    :cond_0
    invoke-virtual {p0, p2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1752
    const-string v0, "Content-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1753
    const-string v0, ">"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1755
    :cond_1
    const-string v0, "\r\n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1757
    :cond_2
    return-void
.end method

.method private static writeInlineICSData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "appendQuotedText"    # Z
    .param p3, "writer"    # Ljava/io/Writer;
    .param p4, "stream"    # Ljava/io/OutputStream;
    .param p5, "bIsAttachment"    # Ljava/lang/Boolean;
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "attachmentsCursor"    # Landroid/database/Cursor;
    .param p8, "plainText"    # Ljava/lang/String;
    .param p9, "HTMLText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1267
    const/4 v4, 0x0

    .line 1268
    .local v4, "boundary_offset":I
    const/4 v8, 0x0

    .line 1269
    .local v8, "multipartSubType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1270
    .local v6, "multipartBoundary1":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1272
    .local v7, "multipartBoundary2":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1274
    .local v3, "attachmentCount":I
    if-nez p1, :cond_1

    .line 1386
    :cond_0
    :goto_0
    return-void

    .line 1278
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "--_com.android.email_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "boundary_offset":I
    .local v5, "boundary_offset":I
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1280
    invoke-virtual/range {p5 .. p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_2

    if-nez p6, :cond_c

    .line 1281
    :cond_2
    const-string v8, "alternative"

    .line 1301
    :goto_1
    const-string v9, "Content-Type"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multipart/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "; boundary=\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p3, v9, v10}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1303
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1305
    const/4 v9, 0x1

    const-string v10, "mixed"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-ne v9, v10, :cond_17

    .line 1306
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "--_com.android.email_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1308
    const-string v9, "Content-Type"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multipart/alternative; boundary=\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p3, v9, v10}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1312
    :goto_2
    if-nez p8, :cond_3

    .line 1313
    const-string p8, ""

    .line 1315
    :cond_3
    if-eqz v7, :cond_4

    .line 1316
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1318
    :cond_4
    move-object/from16 v0, p4

    move-object/from16 v1, p8

    invoke-static {p3, v0, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTextWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1319
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1321
    if-nez p9, :cond_5

    .line 1322
    const-string v9, "<"

    const-string v10, "&lt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1323
    const-string v9, ">"

    const-string v10, "&gt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1324
    const-string v9, "\n"

    const-string v10, "<br>"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1325
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "<html><head></head><body><div style=\"word-break;keep-all;\">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</div></body></html>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p9

    .line 1327
    :cond_5
    if-eqz v7, :cond_11

    .line 1328
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1333
    :goto_3
    move-object/from16 v0, p4

    move-object/from16 v1, p9

    invoke-static {p3, v0, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHTMLWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1334
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1336
    const/4 v9, 0x1

    const-string v10, "mixed"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-ne v9, v10, :cond_0

    .line 1337
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1338
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_8

    .line 1340
    :cond_6
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1341
    .local v2, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v2, :cond_7

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const-string v10, "invite.ics"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1342
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1343
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1344
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1346
    :cond_7
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_6

    .line 1349
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_8
    if-eqz v7, :cond_12

    .line 1350
    const/4 v9, 0x1

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1351
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1352
    const/4 v7, 0x0

    .line 1360
    :goto_4
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1361
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_b

    .line 1363
    :cond_9
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1364
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v2, :cond_14

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_14

    if-eqz p9, :cond_14

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-ne v9, v10, :cond_14

    .line 1365
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v9

    if-eqz v9, :cond_13

    if-nez p2, :cond_13

    iget v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v9, v9, 0x10

    if-eqz v9, :cond_13

    .line 1381
    :cond_a
    :goto_5
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_9

    .line 1383
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_b
    const/4 v9, 0x1

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1384
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1283
    .end local v4    # "boundary_offset":I
    .restart local v5    # "boundary_offset":I
    :cond_c
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1284
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_e

    .line 1286
    :cond_d
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1287
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_f

    if-eqz p9, :cond_f

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-ne v9, v10, :cond_f

    .line 1292
    :goto_6
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_d

    .line 1294
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_e
    if-lez v3, :cond_10

    .line 1295
    const-string v8, "mixed"

    goto/16 :goto_1

    .line 1290
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1297
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_10
    const-string v8, "alternative"

    goto/16 :goto_1

    .line 1330
    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    :cond_11
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 1354
    :cond_12
    const/4 v9, 0x1

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1355
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1356
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 1368
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_13
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1369
    const/4 v9, 0x1

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1370
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_5

    .line 1372
    :cond_14
    if-eqz v2, :cond_a

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const-string v10, "invite.ics"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_a

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_15

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_a

    if-eqz p9, :cond_a

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eq v9, v10, :cond_a

    .line 1374
    :cond_15
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v9

    if-eqz v9, :cond_16

    if-nez p2, :cond_16

    iget v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v9, v9, 0x10

    if-nez v9, :cond_a

    .line 1377
    :cond_16
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1378
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1379
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_5

    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v4    # "boundary_offset":I
    .restart local v5    # "boundary_offset":I
    :cond_17
    move v4, v5

    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    goto/16 :goto_2
.end method

.method private static writeInlineICSDataForward(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "appendQuotedText"    # Z
    .param p3, "writer"    # Ljava/io/Writer;
    .param p4, "stream"    # Ljava/io/OutputStream;
    .param p5, "bIsAttachment"    # Ljava/lang/Boolean;
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "attachmentsCursor"    # Landroid/database/Cursor;
    .param p8, "plainText"    # Ljava/lang/String;
    .param p9, "HTMLText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1132
    const/4 v4, 0x0

    .line 1133
    .local v4, "boundary_offset":I
    const/4 v8, 0x0

    .line 1134
    .local v8, "multipartSubType":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1135
    .local v6, "multipartBoundary1":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1137
    .local v7, "multipartBoundary2":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1139
    .local v3, "attachmentCount":I
    if-nez p1, :cond_1

    .line 1255
    :cond_0
    :goto_0
    return-void

    .line 1143
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "--_com.android.email_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "boundary_offset":I
    .local v5, "boundary_offset":I
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1145
    invoke-virtual/range {p5 .. p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_2

    if-nez p6, :cond_e

    .line 1146
    :cond_2
    const-string v8, "related"

    .line 1168
    :goto_1
    const-string v9, "Content-Type"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multipart/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "; boundary=\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p3, v9, v10}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1170
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1172
    const/4 v9, 0x1

    const-string v10, "related"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eq v9, v10, :cond_3

    const/4 v9, 0x1

    const-string v10, "mixed"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-ne v9, v10, :cond_1a

    .line 1173
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "--_com.android.email_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v4, v5, 0x1

    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1175
    const-string v9, "Content-Type"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "multipart/alternative; boundary=\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p3, v9, v10}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1179
    :goto_2
    if-nez p8, :cond_4

    .line 1180
    const-string p8, ""

    .line 1182
    :cond_4
    if-eqz v7, :cond_5

    .line 1183
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1185
    :cond_5
    move-object/from16 v0, p4

    move-object/from16 v1, p8

    invoke-static {p3, v0, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTextWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1186
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1188
    if-nez p9, :cond_6

    .line 1189
    const-string v9, "<"

    const-string v10, "&lt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1190
    const-string v9, ">"

    const-string v10, "&gt;"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1191
    const-string v9, "\n"

    const-string v10, "<br>"

    move-object/from16 v0, p8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p8

    .line 1192
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "<html><head></head><body><div style=\"word-break;keep-all;\">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "</div></body></html>"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p9

    .line 1196
    :cond_6
    if-eqz v7, :cond_14

    .line 1197
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1202
    :goto_3
    move-object/from16 v0, p4

    move-object/from16 v1, p9

    invoke-static {p3, v0, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHTMLWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1203
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1205
    const/4 v9, 0x1

    const-string v10, "related"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eq v9, v10, :cond_7

    const/4 v9, 0x1

    const-string v10, "mixed"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-ne v9, v10, :cond_0

    .line 1206
    :cond_7
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1207
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_a

    .line 1209
    :cond_8
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1210
    .local v2, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v2, :cond_9

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const-string v10, "invite.ics"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1211
    const/4 v9, 0x0

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1212
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1213
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1215
    :cond_9
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_8

    .line 1218
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_a
    if-eqz v7, :cond_15

    .line 1219
    const/4 v9, 0x1

    invoke-static {p3, v7, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1220
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1221
    const/4 v7, 0x0

    .line 1229
    :goto_4
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1230
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_d

    .line 1232
    :cond_b
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1233
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v2, :cond_17

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_17

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-ne v9, v10, :cond_17

    .line 1234
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v9

    if-eqz v9, :cond_16

    if-nez p2, :cond_16

    iget v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v9, v9, 0x10

    if-eqz v9, :cond_16

    .line 1250
    :cond_c
    :goto_5
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_b

    .line 1252
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_d
    const/4 v9, 0x1

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1253
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1148
    .end local v4    # "boundary_offset":I
    .restart local v5    # "boundary_offset":I
    :cond_e
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1149
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->getCount()I

    move-result v9

    if-lez v9, :cond_10

    .line 1151
    :cond_f
    const-class v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v0, p7

    invoke-static {v0, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1152
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_11

    if-eqz p9, :cond_11

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-ne v9, v10, :cond_11

    .line 1157
    :goto_6
    invoke-interface/range {p7 .. p7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-nez v9, :cond_f

    .line 1159
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_10
    const/4 v9, 0x1

    if-ge v9, v3, :cond_12

    .line 1160
    const-string v8, "mixed"

    goto/16 :goto_1

    .line 1155
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1161
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_12
    const/4 v9, 0x1

    if-ne v3, v9, :cond_13

    .line 1162
    const-string v8, "related"

    goto/16 :goto_1

    .line 1164
    :cond_13
    const-string v8, "alternative"

    goto/16 :goto_1

    .line 1199
    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    :cond_14
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    goto/16 :goto_3

    .line 1223
    :cond_15
    const/4 v9, 0x1

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1224
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1225
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 1237
    .restart local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_16
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1238
    const/4 v9, 0x1

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1239
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_5

    .line 1241
    :cond_17
    if-eqz v2, :cond_c

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const-string v10, "invite.ics"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_c

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_18

    iget-object v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v9, :cond_c

    const/4 v9, 0x1

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eq v9, v10, :cond_c

    .line 1243
    :cond_18
    invoke-static {}, Lcom/android/emailcommon/internet/Rfc822Output;->getIsExchangeAccount()Z

    move-result v9

    if-eqz v9, :cond_19

    if-nez p2, :cond_19

    iget v9, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v9, v9, 0x10

    if-nez v9, :cond_c

    .line 1246
    :cond_19
    const/4 v9, 0x0

    invoke-static {p3, v6, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1247
    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p3, v0, v2, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1248
    const-string v9, "\r\n"

    invoke-virtual {p3, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_5

    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v4    # "boundary_offset":I
    .restart local v5    # "boundary_offset":I
    :cond_1a
    move v4, v5

    .end local v5    # "boundary_offset":I
    .restart local v4    # "boundary_offset":I
    goto/16 :goto_2
.end method

.method private static writeModifiedFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 2151
    const/4 v2, 0x0

    .line 2152
    .local v2, "result":Ljava/lang/String;
    move-object v3, p0

    .line 2153
    .local v3, "temp":Ljava/lang/String;
    const/16 v4, 0xe

    new-array v1, v4, [C

    fill-array-data v1, :array_0

    .line 2155
    .local v1, "restrictChar":[C
    if-eqz p0, :cond_1

    .line 2156
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 2157
    aget-char v4, v1, v0

    const/16 v5, 0x5f

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    .line 2156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2159
    :cond_0
    move-object v2, v3

    .line 2162
    .end local v0    # "i":I
    :cond_1
    return-object v2

    .line 2153
    :array_0
    .array-data 2
        0x28s
        0x29s
        0x3cs
        0x3es
        0x40s
        0x2cs
        0x3bs
        0x2fs
        0x5cs
        0x3fs
        0x7cs
        0x2as
        0x22s
        0x3as
    .end array-data
.end method

.method private static writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "attachment"    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p4, "isInline"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1584
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    .line 1585
    const-string v18, "Unknown"

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 1587
    :cond_0
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    const-string v18, ""

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1588
    :cond_1
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/emailcommon/internet/MediaFileMini;->getMimeTypeForView(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 1590
    :cond_2
    const-string v18, "Rfc8220Output"

    const-string v19, "writeOneAttachment() : MimeType: "

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v18 .. v20}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, ".eml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1593
    const-string v18, "application/eml"

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 1594
    const-string v18, "Rfc8220Output"

    const-string v19, "writeOneAttachment() : New MimeType: "

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v18 .. v20}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    :cond_3
    const/4 v8, 0x1

    .line 1599
    .local v8, "ff":Z
    :try_start_0
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lorg/apache/james/mime4j/codec/EncoderUtil;->hasToBeEncoded(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 1603
    const-string v9, ""

    .line 1604
    .local v9, "fileName":Ljava/lang/String;
    if-eqz v8, :cond_b

    .line 1606
    const-string v7, ""

    .line 1609
    .local v7, "encFileName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v18

    const-string v19, "CscFeature_Email_ReplaceUnsupportedCharWithUnderbarInAttachment"

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 1610
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/emailcommon/internet/Rfc822Output;->writeModifiedFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lorg/apache/james/mime4j/codec/EncoderUtil;->encodeAddressDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1616
    :goto_0
    const-string v18, "Content-Type"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ";\n name=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    const-string v18, "Content-Transfer-Encoding"

    const-string v19, "base64"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1622
    move-object/from16 v0, p3

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x1

    if-nez v18, :cond_4

    .line 1625
    const/16 v18, 0x1

    move/from16 v0, v18

    move/from16 v1, p4

    if-ne v0, v1, :cond_a

    .line 1626
    const-string v18, "Content-Disposition"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "inline;\n filename=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\";"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n size="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    :cond_4
    :goto_1
    move-object v9, v7

    .line 1666
    .end local v7    # "encFileName":Ljava/lang/String;
    :goto_2
    const/16 v18, 0x1

    move/from16 v0, v18

    move/from16 v1, p4

    if-ne v0, v1, :cond_5

    .line 1667
    const-string v18, "Content-ID"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    const-string v18, "X-MS-UrlCompName"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1, v9}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1670
    :cond_5
    const-string v18, "\r\n"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 1673
    const/4 v13, 0x0

    .line 1674
    .local v13, "inStream":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 1677
    .local v4, "base64Out":Landroid/util/Base64OutputStream;
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Utility;->attachmentExistsAndHasRealData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 1679
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-object/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithMessageIdAndLocation(Landroid/content/Context;JLjava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v17

    .line 1682
    .local v17, "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v17, :cond_6

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v18, v0

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0x200

    move/from16 v18, v0

    if-eqz v18, :cond_6

    .line 1684
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Utility;->attachmentExistsAndHasRealData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 1685
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    .line 1686
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1695
    .end local v17    # "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_6
    :goto_3
    :try_start_2
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    move-object/from16 v18, v0

    if-eqz v18, :cond_10

    .line 1696
    new-instance v14, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .end local v13    # "inStream":Ljava/io/InputStream;
    .local v14, "inStream":Ljava/io/InputStream;
    move-object v13, v14

    .line 1708
    .end local v14    # "inStream":Ljava/io/InputStream;
    .restart local v13    # "inStream":Ljava/io/InputStream;
    :goto_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/Writer;->flush()V

    .line 1709
    new-instance v5, Landroid/util/Base64OutputStream;

    const/16 v18, 0x14

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-direct {v5, v0, v1}, Landroid/util/Base64OutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1713
    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .local v5, "base64Out":Landroid/util/Base64OutputStream;
    :try_start_3
    const-string v18, "Rfc8220Output"

    const-string v19, "EMAIL_PERFORMANCE s writeOneAttachment() before calling IOUtils.copy()"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1714
    invoke-static {v13, v5}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 1715
    const-string v18, "Rfc8220Output"

    const-string v19, "EMAIL_PERFORMANCE s writeOneAttachment() after calling IOUtils.copy()"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1721
    const/16 v18, 0xd

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1722
    const/16 v18, 0xa

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1723
    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1730
    if-eqz v5, :cond_7

    .line 1731
    invoke-virtual {v5}, Landroid/util/Base64OutputStream;->close()V

    .line 1732
    :cond_7
    if-eqz v13, :cond_15

    .line 1733
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V

    move-object v4, v5

    .line 1735
    .end local v5    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    :cond_8
    :goto_5
    return-void

    .line 1600
    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .end local v9    # "fileName":Ljava/lang/String;
    .end local v13    # "inStream":Ljava/io/InputStream;
    :catch_0
    move-exception v12

    .line 1601
    .local v12, "iae":Ljava/lang/IllegalArgumentException;
    new-instance v18, Lcom/android/emailcommon/mail/MessagingException;

    const-string v19, "Invalid attachment."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v12}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18

    .line 1613
    .end local v12    # "iae":Ljava/lang/IllegalArgumentException;
    .restart local v7    # "encFileName":Ljava/lang/String;
    .restart local v9    # "fileName":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lorg/apache/james/mime4j/codec/EncoderUtil;->encodeAddressDisplayName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1629
    :cond_a
    const-string v18, "Content-Disposition"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "attachment;\n filename=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\";"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n size="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1636
    .end local v7    # "encFileName":Ljava/lang/String;
    :cond_b
    const-string v16, ""

    .line 1639
    .local v16, "modifiedFileName":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v18

    const-string v19, "CscFeature_Email_ReplaceUnsupportedCharWithUnderbarInAttachment"

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_d

    .line 1640
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/emailcommon/internet/Rfc822Output;->writeModifiedFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1645
    :goto_6
    const-string v18, "Content-Type"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "; name=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    const-string v18, "Content-Transfer-Encoding"

    const-string v19, "base64"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    move-object/from16 v0, p3

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x1

    if-nez v18, :cond_c

    .line 1653
    const/16 v18, 0x1

    move/from16 v0, v18

    move/from16 v1, p4

    if-ne v0, v1, :cond_e

    .line 1654
    const-string v18, "Content-Disposition"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "inline; filename=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\";"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " size="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    :cond_c
    :goto_7
    move-object/from16 v9, v16

    goto/16 :goto_2

    .line 1643
    :cond_d
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v16, v0

    goto/16 :goto_6

    .line 1657
    :cond_e
    const-string v18, "Content-Disposition"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "attachment;\n filename=\""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\";"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n size="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 1690
    .end local v16    # "modifiedFileName":Ljava/lang/String;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v13    # "inStream":Ljava/io/InputStream;
    :catch_1
    move-exception v6

    .line 1691
    .local v6, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_3

    .line 1724
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v11

    .line 1726
    .local v11, "fnfe":Ljava/io/FileNotFoundException;
    :goto_8
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1730
    if-eqz v4, :cond_f

    .line 1731
    invoke-virtual {v4}, Landroid/util/Base64OutputStream;->close()V

    .line 1732
    :cond_f
    if-eqz v13, :cond_8

    .line 1733
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V

    goto/16 :goto_5

    .line 1697
    .end local v11    # "fnfe":Ljava/io/FileNotFoundException;
    :cond_10
    :try_start_6
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_11

    .line 1699
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 1700
    .local v10, "fileUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v13

    .line 1701
    goto/16 :goto_4

    .line 1702
    .end local v10    # "fileUri":Landroid/net/Uri;
    :cond_11
    const-string v18, "Rfc8220Output"

    const-string v19, "writeOneAttachment() : attachment.mContentBytes, attachment.mContentUri are null"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1704
    const-string v18, "Rfc8220Output"

    const-string v19, "writeOneAttachment() : cannot attach this attachment"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1730
    if-eqz v4, :cond_12

    .line 1731
    invoke-virtual {v4}, Landroid/util/Base64OutputStream;->close()V

    .line 1732
    :cond_12
    if-eqz v13, :cond_8

    .line 1733
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V

    goto/16 :goto_5

    .line 1727
    :catch_3
    move-exception v15

    .line 1728
    .local v15, "ioe":Ljava/io/IOException;
    :goto_9
    :try_start_7
    new-instance v18, Lcom/android/emailcommon/mail/MessagingException;

    const-string v19, "Invalid attachment."

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v15}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v18
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1730
    .end local v15    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    :goto_a
    if-eqz v4, :cond_13

    .line 1731
    invoke-virtual {v4}, Landroid/util/Base64OutputStream;->close()V

    .line 1732
    :cond_13
    if-eqz v13, :cond_14

    .line 1733
    invoke-virtual {v13}, Ljava/io/InputStream;->close()V

    :cond_14
    throw v18

    .line 1730
    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v5    # "base64Out":Landroid/util/Base64OutputStream;
    :catchall_1
    move-exception v18

    move-object v4, v5

    .end local v5    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    goto :goto_a

    .line 1727
    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v5    # "base64Out":Landroid/util/Base64OutputStream;
    :catch_4
    move-exception v15

    move-object v4, v5

    .end local v5    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    goto :goto_9

    .line 1724
    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v5    # "base64Out":Landroid/util/Base64OutputStream;
    :catch_5
    move-exception v11

    move-object v4, v5

    .end local v5    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    goto :goto_8

    .end local v4    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v5    # "base64Out":Landroid/util/Base64OutputStream;
    :cond_15
    move-object v4, v5

    .end local v5    # "base64Out":Landroid/util/Base64OutputStream;
    .restart local v4    # "base64Out":Landroid/util/Base64OutputStream;
    goto/16 :goto_5
.end method

.method private static writeRDNheader(Ljava/io/Writer;Lcom/android/emailcommon/provider/EmailContent$Message;ZZ)V
    .locals 7
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "isReadReceipt"    # Z
    .param p3, "isDeliveryReceipt"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1800
    const/4 v3, 0x0

    .line 1801
    .local v3, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 1803
    .local v2, "needDomainNameChange":Z
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1846
    :cond_0
    :goto_0
    return-void

    .line 1807
    :cond_1
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    const-string v5, "@m.google.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    const-string v5, "@google.com"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1809
    :cond_2
    const/4 v2, 0x1

    .line 1810
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1811
    .local v1, "i":I
    if-ltz v1, :cond_3

    .line 1812
    new-instance v3, Ljava/lang/StringBuffer;

    .end local v3    # "sb":Ljava/lang/StringBuffer;
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1816
    .restart local v3    # "sb":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "@gmail.com"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1833
    .end local v1    # "i":I
    :cond_3
    if-eqz v3, :cond_6

    if-eqz v2, :cond_6

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1836
    .local v0, "From":Ljava/lang/String;
    :goto_1
    if-eqz p2, :cond_4

    .line 1837
    const-string v4, "Disposition-Notification-To"

    invoke-static {p0, v4, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    :cond_4
    if-eqz p3, :cond_5

    .line 1841
    const-string v4, "Return-Receipt-To"

    invoke-static {p0, v4, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    :cond_5
    const-string v4, "Return-Path"

    invoke-static {p0, v4, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1833
    .end local v0    # "From":Ljava/lang/String;
    :cond_6
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    goto :goto_1
.end method

.method private static writeTextWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 3
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1905
    const-string v1, "Content-Type"

    const-string v2, "text/plain; charset=utf-8"

    invoke-static {p0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1906
    const-string v1, "Content-Transfer-Encoding"

    const-string v2, "base64"

    invoke-static {p0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1907
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1908
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 1909
    .local v0, "bytes":[B
    invoke-virtual {p0}, Ljava/io/Writer;->flush()V

    .line 1910
    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 1911
    return-void
.end method

.method public static writeTo(Landroid/content/Context;JLjava/io/OutputStream;ZZ)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "appendQuotedText"    # Z
    .param p5, "sendBcc"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;,
            Lcom/android/emailcommon/cac/CACException;
        }
    .end annotation

    .prologue
    .line 2181
    const/4 v7, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v1 .. v7}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTo(Landroid/content/Context;JLjava/io/OutputStream;ZZZ)V

    .line 2183
    return-void
.end method

.method public static writeTo(Landroid/content/Context;JLjava/io/OutputStream;ZZZ)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "appendQuotedText"    # Z
    .param p5, "sendBcc"    # Z
    .param p6, "isAttachmentDownloadRequired"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;,
            Lcom/android/emailcommon/cac/CACException;
        }
    .end annotation

    .prologue
    .line 494
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v8

    .line 495
    .local v8, "message":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v8, :cond_0

    .line 527
    :goto_0
    return-void

    .line 501
    :cond_0
    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {p0, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithAccountId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/Rfc822Output;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 504
    iget v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_2

    const/4 v9, 0x1

    .line 505
    .local v9, "isReadReceipt":Z
    :goto_1
    iget v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_3

    const/4 v10, 0x1

    .line 506
    .local v10, "isDeliveryReceipt":Z
    :goto_2
    iget-boolean v11, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mPGP:Z

    .line 508
    .local v11, "isPGPMessage":Z
    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mPGP:Z

    if-nez v0, :cond_4

    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-nez v0, :cond_1

    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v0, :cond_4

    .line 509
    :cond_1
    const-string v0, "RFC822Output"

    const-string v1, "message.mEncrypted || message.mSigned"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 511
    invoke-static/range {v1 .. v11}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoSMIME(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V

    goto :goto_0

    .line 504
    .end local v9    # "isReadReceipt":Z
    .end local v10    # "isDeliveryReceipt":Z
    .end local v11    # "isPGPMessage":Z
    :cond_2
    const/4 v9, 0x0

    goto :goto_1

    .line 505
    .restart local v9    # "isReadReceipt":Z
    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    .line 515
    .restart local v10    # "isDeliveryReceipt":Z
    .restart local v11    # "isPGPMessage":Z
    :cond_4
    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mPGP:Z

    if-eqz v0, :cond_6

    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-nez v0, :cond_5

    iget-boolean v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v0, :cond_6

    .line 516
    :cond_5
    const-string v0, "RFC822Output"

    const-string v1, "message type is PGP"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 518
    invoke-static/range {v1 .. v11}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoPGP(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V

    goto :goto_0

    .line 521
    :cond_6
    const-string v0, "RFC822Output"

    const-string v1, "writeTo() normal"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-wide v2, p1

    move-object/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    .line 522
    invoke-static/range {v1 .. v11}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoPlain(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V

    goto :goto_0
.end method

.method private static writetoPGP(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "appendQuotedText"    # Z
    .param p5, "sendBcc"    # Z
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p8, "isReadReceipt"    # Z
    .param p9, "isDeliveryReceipt"    # Z
    .param p10, "isPGPMessage"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 830
    if-nez p7, :cond_0

    .line 904
    :goto_0
    return-void

    .line 836
    :cond_0
    :try_start_0
    sget-object v4, Lcom/android/emailcommon/internet/Rfc822Output;->mDateFormat:Lorg/apache/commons/lang/time/FastDateFormat;

    new-instance v6, Ljava/util/Date;

    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v6}, Lorg/apache/commons/lang/time/FastDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    .line 837
    .local v14, "date":Ljava/lang/String;
    new-instance v16, Lcom/android/emailcommon/pgp/PGPHelper;

    invoke-direct/range {v16 .. v16}, Lcom/android/emailcommon/pgp/PGPHelper;-><init>()V

    .line 838
    .local v16, "helper":Lcom/android/emailcommon/pgp/PGPHelper;
    new-instance v17, Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-direct/range {v17 .. v17}, Lcom/android/emailcommon/pgp/PGPHelper$Message;-><init>()V

    .line 839
    .local v17, "msg":Lcom/android/emailcommon/pgp/PGPHelper$Message;
    move-object/from16 v0, v17

    iput-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mDate:Ljava/lang/String;

    .line 840
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSubject:Ljava/lang/String;

    .line 841
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageId:Ljava/lang/String;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mMessageID:Ljava/lang/String;

    .line 842
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    const/4 v6, 0x0

    aget-object v4, v4, v6

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    .line 843
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFromAddress:Ljava/lang/String;

    .line 844
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mTo:[Lcom/android/emailcommon/mail/Address;

    .line 845
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mCC:[Lcom/android/emailcommon/mail/Address;

    .line 846
    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    move-object/from16 v0, v17

    iput-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mEncrypted:Z

    .line 847
    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    move-object/from16 v0, v17

    iput-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    .line 848
    move/from16 v0, p8

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mRead:Z

    .line 849
    move/from16 v0, p9

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mDelivery:Z

    .line 850
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mKeyIds:Ljava/lang/String;

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mKeyIds:Ljava/lang/String;

    .line 851
    if-eqz p5, :cond_1

    .line 852
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mBCC:[Lcom/android/emailcommon/mail/Address;

    .line 854
    :cond_1
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mReplyTo:[Lcom/android/emailcommon/mail/Address;

    .line 855
    move-object/from16 v0, p7

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    invoke-static {v4}, Lcom/android/emailcommon/internet/Rfc822Output;->getEmailPriority(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mImportance:Ljava/lang/String;

    .line 856
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v18

    .line 857
    .local v18, "plainText":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyTextHtml(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v10

    .line 859
    .local v10, "HTMLText":Ljava/lang/String;
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 860
    const/4 v4, 0x1

    move-object/from16 v0, v17

    iput-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    .line 861
    move-object/from16 v0, v17

    iput-object v10, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mHtmlBodyText:Ljava/lang/String;

    .line 866
    :goto_1
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->MESSAGE_ID_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 867
    .local v5, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 869
    .local v13, "attachmentsCursor":Landroid/database/Cursor;
    if-eqz v13, :cond_6

    .line 870
    :cond_2
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 871
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-static {v13, v4}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v12

    check-cast v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 873
    .local v12, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 874
    new-instance v11, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;

    invoke-direct {v11}, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;-><init>()V

    .line 875
    .local v11, "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, v11, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mUri:Landroid/net/Uri;

    .line 876
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    iput-object v4, v11, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    .line 877
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 878
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    iput-object v4, v11, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mContentId:Ljava/lang/String;

    .line 880
    :cond_3
    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    iput-object v4, v11, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mMimeType:Ljava/lang/String;

    .line 881
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 894
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v10    # "HTMLText":Ljava/lang/String;
    .end local v11    # "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    .end local v12    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v13    # "attachmentsCursor":Landroid/database/Cursor;
    .end local v14    # "date":Ljava/lang/String;
    .end local v16    # "helper":Lcom/android/emailcommon/pgp/PGPHelper;
    .end local v17    # "msg":Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .end local v18    # "plainText":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 895
    .local v15, "ex":Ljava/lang/OutOfMemoryError;
    const-string v4, "Rfc8220Output"

    invoke-virtual {v15}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    invoke-virtual {v15}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 897
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 863
    .end local v15    # "ex":Ljava/lang/OutOfMemoryError;
    .restart local v10    # "HTMLText":Ljava/lang/String;
    .restart local v14    # "date":Ljava/lang/String;
    .restart local v16    # "helper":Lcom/android/emailcommon/pgp/PGPHelper;
    .restart local v17    # "msg":Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .restart local v18    # "plainText":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v17

    iput-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    .line 864
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mBodyText:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 898
    .end local v10    # "HTMLText":Ljava/lang/String;
    .end local v14    # "date":Ljava/lang/String;
    .end local v16    # "helper":Lcom/android/emailcommon/pgp/PGPHelper;
    .end local v17    # "msg":Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .end local v18    # "plainText":Ljava/lang/String;
    :catch_1
    move-exception v15

    .line 899
    .local v15, "ex":Ljava/lang/Exception;
    const-string v4, "Rfc8220Output"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception caught: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v15}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 884
    .end local v15    # "ex":Ljava/lang/Exception;
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v10    # "HTMLText":Ljava/lang/String;
    .restart local v13    # "attachmentsCursor":Landroid/database/Cursor;
    .restart local v14    # "date":Ljava/lang/String;
    .restart local v16    # "helper":Lcom/android/emailcommon/pgp/PGPHelper;
    .restart local v17    # "msg":Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .restart local v18    # "plainText":Ljava/lang/String;
    :cond_5
    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 887
    :cond_6
    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-eqz v4, :cond_8

    .line 888
    const-string v4, "RFC822Output"

    const-string v6, "writeTo() PGP Encrypted Message"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->encryptMessage(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/OutputStream;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 902
    :cond_7
    :goto_3
    invoke-virtual/range {p3 .. p3}, Ljava/io/OutputStream;->flush()V

    goto/16 :goto_0

    .line 890
    :cond_8
    :try_start_3
    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v4, :cond_7

    .line 891
    const-string v4, "RFC822Output"

    const-string v6, "writeTo() PGP Signed Message"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->signMessage(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3
.end method

.method private static writetoPlain(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V
    .locals 25
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "appendQuotedText"    # Z
    .param p5, "sendBcc"    # Z
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p8, "isReadReceipt"    # Z
    .param p9, "isDeliveryReceipt"    # Z
    .param p10, "isPGPMessage"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 960
    if-nez p7, :cond_0

    .line 1057
    :goto_0
    return-void

    .line 965
    :cond_0
    new-instance v12, Ljava/io/BufferedOutputStream;

    const/16 v6, 0x400

    move-object/from16 v0, p3

    invoke-direct {v12, v0, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 966
    .local v12, "stream":Ljava/io/OutputStream;
    new-instance v23, Ljava/io/OutputStreamWriter;

    move-object/from16 v0, v23

    invoke-direct {v0, v12}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 967
    .local v23, "writer":Ljava/io/Writer;
    move-object/from16 v0, p7

    iget-boolean v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-nez v6, :cond_1

    move-object/from16 v0, p7

    iget-boolean v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-nez v6, :cond_1

    .line 968
    move-object/from16 v0, v23

    move-object/from16 v1, p7

    move/from16 v2, p5

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoPlainHeader(Ljava/io/Writer;Lcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V

    .line 971
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v16

    .line 972
    .local v16, "plainText":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyTextHtml(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v17

    .line 974
    .local v17, "HTMLText":Ljava/lang/String;
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->MESSAGE_ID_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 975
    .local v7, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_PROJECTION:[Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 979
    .local v15, "attachmentsCursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p7

    iget-boolean v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    .line 980
    .local v13, "bIsAttachment":Ljava/lang/Boolean;
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v22

    .line 981
    .local v22, "totalAttachmentCount":I
    const/16 v19, 0x0

    .line 983
    .local v19, "flags":I
    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    move/from16 v0, v22

    if-lt v0, v6, :cond_2

    .line 984
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    .line 987
    :cond_2
    const/4 v6, 0x1

    move/from16 v0, v22

    if-ne v6, v0, :cond_3

    .line 988
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 989
    const/16 v6, 0xa

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 992
    :cond_3
    const/4 v6, 0x1

    move/from16 v0, v22

    if-ne v0, v6, :cond_a

    and-int/lit8 v6, v19, 0x1

    if-eqz v6, :cond_a

    .line 995
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "--_com.android.email_"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 999
    .local v20, "multipartBoundary1":Ljava/lang/String;
    const-string v21, "alternative"

    .line 1001
    .local v21, "multipartType":Ljava/lang/String;
    const-string v6, "Content-Type"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "multipart/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; boundary=\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v23

    invoke-static {v0, v6, v8}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    const-string v6, "\r\n"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1008
    if-nez v16, :cond_4

    if-eqz v17, :cond_6

    .line 1009
    :cond_4
    const/4 v6, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1013
    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/android/emailcommon/provider/EmailContent;->isSNCAccount(Landroid/content/Context;Ljava/lang/Long;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1014
    const-string v6, "Content-Disposition"

    const-string v8, "inline"

    move-object/from16 v0, v23

    invoke-static {v0, v6, v8}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    :cond_5
    if-eqz v17, :cond_9

    .line 1018
    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-static {v0, v12, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHTMLWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1021
    :goto_1
    const-string v6, "\r\n"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1023
    :cond_6
    if-eqz p6, :cond_8

    .line 1025
    :cond_7
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1026
    const-class v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-static {v15, v6}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v18

    check-cast v18, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1029
    .local v18, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const/4 v6, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1030
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v18

    invoke-static {v0, v1, v12, v2, v6}, Lcom/android/emailcommon/internet/Rfc822Output;->writeOneAttachment(Landroid/content/Context;Ljava/io/Writer;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Attachment;Z)V

    .line 1031
    const-string v6, "\r\n"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 1032
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_7

    .line 1035
    .end local v18    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_8
    const/4 v6, 0x1

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/android/emailcommon/internet/Rfc822Output;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 1036
    const-string v6, "\r\n"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051
    .end local v20    # "multipartBoundary1":Ljava/lang/String;
    .end local v21    # "multipartType":Ljava/lang/String;
    :goto_2
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 1054
    invoke-virtual/range {v23 .. v23}, Ljava/io/Writer;->flush()V

    .line 1055
    invoke-virtual/range {p3 .. p3}, Ljava/io/OutputStream;->flush()V

    goto/16 :goto_0

    .line 1020
    .restart local v20    # "multipartBoundary1":Ljava/lang/String;
    .restart local v21    # "multipartType":Ljava/lang/String;
    :cond_9
    :try_start_1
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v12, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTextWithHeaders(Ljava/io/Writer;Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1051
    .end local v13    # "bIsAttachment":Ljava/lang/Boolean;
    .end local v19    # "flags":I
    .end local v20    # "multipartBoundary1":Ljava/lang/String;
    .end local v21    # "multipartType":Ljava/lang/String;
    .end local v22    # "totalAttachmentCount":I
    :catchall_0
    move-exception v6

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v6

    .line 1039
    .restart local v13    # "bIsAttachment":Ljava/lang/Boolean;
    .restart local v19    # "flags":I
    .restart local v22    # "totalAttachmentCount":I
    :cond_a
    :try_start_2
    move-object/from16 v0, p7

    invoke-static {v15, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->isHandleMeetingInline(Landroid/database/Cursor;Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v6

    if-eqz v6, :cond_b

    move-object/from16 v8, p0

    move-object/from16 v9, p7

    move/from16 v10, p4

    move-object/from16 v11, v23

    move/from16 v14, p6

    .line 1040
    invoke-static/range {v8 .. v17}, Lcom/android/emailcommon/internet/Rfc822Output;->writeInlineICSData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1041
    :cond_b
    move-object/from16 v0, p7

    invoke-static {v15, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->isHandleMeetingInlineForward(Landroid/database/Cursor;Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v6

    if-eqz v6, :cond_c

    move-object/from16 v8, p0

    move-object/from16 v9, p7

    move/from16 v10, p4

    move-object/from16 v11, v23

    move/from16 v14, p6

    .line 1042
    invoke-static/range {v8 .. v17}, Lcom/android/emailcommon/internet/Rfc822Output;->writeInlineICSDataForward(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    move-object/from16 v8, p0

    move-object/from16 v9, p7

    move/from16 v10, p4

    move-object/from16 v11, v23

    move/from16 v14, p6

    .line 1046
    invoke-static/range {v8 .. v17}, Lcom/android/emailcommon/internet/Rfc822Output;->writeData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZLjava/io/Writer;Ljava/io/OutputStream;Ljava/lang/Boolean;ZLandroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private static writetoPlainHeader(Ljava/io/Writer;Lcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V
    .locals 6
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "sendBcc"    # Z
    .param p3, "isReadReceipt"    # Z
    .param p4, "isDeliveryReceipt"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 909
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 952
    :cond_0
    :goto_0
    return-void

    .line 916
    :cond_1
    sget-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mDateFormat:Lorg/apache/commons/lang/time/FastDateFormat;

    new-instance v3, Ljava/util/Date;

    iget-wide v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lorg/apache/commons/lang/time/FastDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 917
    .local v0, "date":Ljava/lang/String;
    const-string v2, "Date"

    invoke-static {p0, v2, v0}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    sget-object v2, Lcom/android/emailcommon/internet/Rfc822Output;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    if-eqz v2, :cond_6

    const-string v2, "m.google.com"

    sget-object v3, Lcom/android/emailcommon/internet/Rfc822Output;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "eas"

    sget-object v3, Lcom/android/emailcommon/internet/Rfc822Output;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 922
    const-string v2, "Subject"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 926
    :goto_1
    const-string v2, "Message-ID"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageId:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    invoke-static {v2}, Lcom/android/emailcommon/internet/Rfc822Output;->getEmailPriority(I)Ljava/lang/String;

    move-result-object v1

    .line 929
    .local v1, "importance":Ljava/lang/String;
    const-string v2, "Importance"

    invoke-static {p0, v2, v1}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    const-string v2, "From"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    const-string v2, "To"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    const-string v2, "Cc"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    if-eqz p2, :cond_2

    .line 939
    const-string v2, "Bcc"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    :cond_2
    if-nez p3, :cond_3

    if-eqz p4, :cond_4

    .line 943
    :cond_3
    invoke-static {p0, p1, p3, p4}, Lcom/android/emailcommon/internet/Rfc822Output;->writeRDNheader(Ljava/io/Writer;Lcom/android/emailcommon/provider/EmailContent$Message;ZZ)V

    .line 946
    :cond_4
    const-string v2, "Reply-To"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 948
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    const/16 v3, 0x100

    if-ne v2, v3, :cond_5

    .line 949
    const-string v2, "Comments"

    const-string v3, "SMS"

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    :cond_5
    const-string v2, "MIME-Version"

    const-string v3, "1.0"

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 924
    .end local v1    # "importance":Ljava/lang/String;
    :cond_6
    const-string v2, "Subject"

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/internet/Rfc822Output;->writeEncodedHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static writetoSMIME(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V
    .locals 33
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "appendQuotedText"    # Z
    .param p5, "sendBcc"    # Z
    .param p6, "isAttachmentDownloadRequired"    # Z
    .param p7, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p8, "isReadReceipt"    # Z
    .param p9, "isDeliveryReceipt"    # Z
    .param p10, "isPGPMessage"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/cac/CACException;
        }
    .end annotation

    .prologue
    .line 695
    if-nez p7, :cond_1

    .line 822
    :cond_0
    return-void

    .line 700
    :cond_1
    sget-object v4, Lcom/android/emailcommon/internet/Rfc822Output;->mDateFormat:Lorg/apache/commons/lang/time/FastDateFormat;

    new-instance v5, Ljava/util/Date;

    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v5}, Lorg/apache/commons/lang/time/FastDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v23

    .line 702
    .local v23, "date":Ljava/lang/String;
    new-instance v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;

    invoke-direct {v12}, Lcom/android/emailcommon/smime/SMIMEHelper$Message;-><init>()V

    .line 703
    .local v12, "msg":Lcom/android/emailcommon/smime/SMIMEHelper$Message;
    move-object/from16 v0, v23

    iput-object v0, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mDate:Ljava/lang/String;

    .line 704
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mSubject:Ljava/lang/String;

    .line 705
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageId:Ljava/lang/String;

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mMessageID:Ljava/lang/String;

    .line 706
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    .line 707
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mTo:[Lcom/android/emailcommon/mail/Address;

    .line 708
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mCC:[Lcom/android/emailcommon/mail/Address;

    .line 710
    move/from16 v0, p8

    iput-boolean v0, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mRead:Z

    .line 711
    move/from16 v0, p9

    iput-boolean v0, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mDelivery:Z

    .line 713
    if-eqz p5, :cond_2

    .line 714
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mBCC:[Lcom/android/emailcommon/mail/Address;

    .line 716
    :cond_2
    move-object/from16 v0, p7

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mReplyTo:[Lcom/android/emailcommon/mail/Address;

    .line 717
    move-object/from16 v0, p7

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    invoke-static {v4}, Lcom/android/emailcommon/internet/Rfc822Output;->getEmailPriority(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mImportance:Ljava/lang/String;

    .line 719
    const/16 v20, 0x0

    .line 720
    .local v20, "bodyText":Lcom/android/emailcommon/internet/Rfc822Output$BodyTextHtml;
    move-object/from16 v0, p7

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v10

    .line 723
    .local v10, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyText(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v31

    .line 724
    .local v31, "plainText":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/Rfc822Output;->buildBodyTextHtml(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Ljava/lang/String;

    move-result-object v17

    .line 727
    .local v17, "HTMLText":Ljava/lang/String;
    const/4 v14, 0x0

    .line 728
    .local v14, "credentialAccount":Z
    if-eqz v10, :cond_3

    .line 729
    invoke-virtual {v10}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/android/emailcommon/cac/CACManager;->isCredentialAccount(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v4, :cond_4

    const/4 v14, 0x1

    .line 731
    :cond_3
    :goto_0
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v10, v1, v14}, Lcom/android/emailcommon/internet/Rfc822Output;->getSMIMEHelper(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Message;Z)Lcom/android/emailcommon/smime/SMIMEHelper;

    move-result-object v11

    .line 734
    .local v11, "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    if-eqz v17, :cond_5

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 735
    const/4 v4, 0x1

    iput-boolean v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mIsHtml:Z

    .line 736
    move-object/from16 v0, v17

    iput-object v0, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mHtmlBodyText:Ljava/lang/String;

    .line 742
    :goto_1
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2, v12}, Lcom/android/emailcommon/internet/Rfc822Output;->addAttachment(Landroid/content/Context;JLcom/android/emailcommon/smime/SMIMEHelper$Message;)V

    .line 745
    :try_start_0
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 746
    .local v13, "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    move-object/from16 v0, p7

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-eqz v4, :cond_b

    .line 747
    const/4 v4, 0x1

    new-array v0, v4, [Lcom/android/emailcommon/mail/Address;

    move-object/from16 v28, v0

    .line 748
    .local v28, "from":[Lcom/android/emailcommon/mail/Address;
    const/4 v4, 0x0

    iget-object v5, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v5, v28, v4

    .line 749
    const/4 v4, 0x4

    new-array v4, v4, [[Lcom/android/emailcommon/mail/Address;

    const/4 v5, 0x0

    iget-object v7, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mTo:[Lcom/android/emailcommon/mail/Address;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    iget-object v7, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mCC:[Lcom/android/emailcommon/mail/Address;

    aput-object v7, v4, v5

    const/4 v5, 0x2

    iget-object v7, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mBCC:[Lcom/android/emailcommon/mail/Address;

    aput-object v7, v4, v5

    const/4 v5, 0x3

    aput-object v28, v4, v5

    invoke-static {v4}, Lcom/android/emailcommon/smime/SMIMEHelper;->joinAddresses([[Lcom/android/emailcommon/mail/Address;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v18

    .line 752
    .local v18, "addresses":[Lcom/android/emailcommon/mail/Address;
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    .line 753
    .local v32, "selection":Ljava/lang/StringBuilder;
    const/16 v27, 0x1

    .line 754
    .local v27, "first":Z
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_2
    move-object/from16 v0, v18

    array-length v4, v0

    move/from16 v0, v29

    if-ge v0, v4, :cond_7

    .line 755
    if-eqz v27, :cond_6

    .line 756
    const-string v4, "email"

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v18, v29

    invoke-virtual {v5}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 758
    const/16 v27, 0x0

    .line 754
    :goto_3
    add-int/lit8 v29, v29, 0x1

    goto :goto_2

    .line 729
    .end local v11    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    .end local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .end local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v27    # "first":Z
    .end local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .end local v29    # "i":I
    .end local v32    # "selection":Ljava/lang/StringBuilder;
    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 738
    .restart local v11    # "helper":Lcom/android/emailcommon/smime/SMIMEHelper;
    :cond_5
    const/4 v4, 0x0

    iput-boolean v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mIsHtml:Z

    .line 739
    move-object/from16 v0, v31

    iput-object v0, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mBodyText:Ljava/lang/String;

    goto :goto_1

    .line 760
    .restart local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .restart local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .restart local v27    # "first":Z
    .restart local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .restart local v29    # "i":I
    .restart local v32    # "selection":Ljava/lang/StringBuilder;
    :cond_6
    :try_start_1
    const-string v4, " OR "

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "email"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v18, v29

    invoke-virtual {v5}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 809
    .end local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .end local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v27    # "first":Z
    .end local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .end local v29    # "i":I
    .end local v32    # "selection":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v24

    .line 810
    .local v24, "e":Lcom/android/emailcommon/smime/CertificateManagerException;
    if-eqz v14, :cond_0

    .line 811
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/android/emailcommon/cac/CACManager;->setErrorCode(Landroid/content/Context;I)V

    .line 812
    new-instance v4, Lcom/android/emailcommon/cac/CACException;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lcom/android/emailcommon/cac/CACException;-><init>(I)V

    throw v4

    .line 765
    .end local v24    # "e":Lcom/android/emailcommon/smime/CertificateManagerException;
    .restart local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .restart local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .restart local v27    # "first":Z
    .restart local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .restart local v29    # "i":I
    .restart local v32    # "selection":Ljava/lang/StringBuilder;
    :cond_7
    :try_start_2
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_b

    .line 766
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "certificate"

    aput-object v5, v6, v4

    .line 769
    .local v6, "prj":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$CertificateCacheColumns;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v22

    .line 772
    .local v22, "cursor":Landroid/database/Cursor;
    if-eqz v22, :cond_b

    .line 774
    :cond_8
    :goto_4
    :try_start_3
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 775
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 776
    .local v21, "cert":Ljava/lang/String;
    if-eqz v21, :cond_8

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_8

    .line 777
    invoke-static/range {v21 .. v21}, Lcom/android/emailcommon/smime/CertificateUtil;->convertToX509(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 781
    .end local v21    # "cert":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v22, :cond_9

    :try_start_4
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v4
    :try_end_4
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 815
    .end local v6    # "prj":[Ljava/lang/String;
    .end local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .end local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v27    # "first":Z
    .end local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .end local v29    # "i":I
    .end local v32    # "selection":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v25

    .line 816
    .local v25, "ex":Ljava/lang/Exception;
    const-class v4, Lcom/android/emailcommon/internet/Rfc822Output;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V

    .line 819
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4

    .line 781
    .end local v25    # "ex":Ljava/lang/Exception;
    .restart local v6    # "prj":[Ljava/lang/String;
    .restart local v13    # "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    .restart local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .restart local v22    # "cursor":Landroid/database/Cursor;
    .restart local v27    # "first":Z
    .restart local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .restart local v29    # "i":I
    .restart local v32    # "selection":Ljava/lang/StringBuilder;
    :cond_a
    if-eqz v22, :cond_b

    :try_start_5
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .end local v6    # "prj":[Ljava/lang/String;
    .end local v18    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v22    # "cursor":Landroid/database/Cursor;
    .end local v27    # "first":Z
    .end local v28    # "from":[Lcom/android/emailcommon/mail/Address;
    .end local v29    # "i":I
    .end local v32    # "selection":Ljava/lang/StringBuilder;
    :cond_b
    move-object/from16 v7, p0

    move-object/from16 v8, p3

    move-object/from16 v9, p7

    move/from16 v15, p4

    move/from16 v16, p6

    .line 787
    invoke-static/range {v7 .. v16}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoSMIME(Landroid/content/Context;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/smime/SMIMEHelper;Lcom/android/emailcommon/smime/SMIMEHelper$Message;Ljava/util/ArrayList;ZZZ)V

    .line 791
    iget-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mAttachments:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 792
    iget-object v4, v12, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v30

    .local v30, "i$":Ljava/util/Iterator;
    :cond_c
    :goto_5
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;

    .line 793
    .local v19, "attach":Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;
    const-string v4, "Rfc8220Output"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "context.getCacheDir().getName(): "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 796
    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 797
    .local v26, "file":Ljava/io/File;
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 798
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 799
    const-string v4, "Rfc8220Output"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "temp file deleted: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 801
    :cond_d
    const-string v4, "Rfc8220Output"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unable to delete temp file: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_5
.end method

.method private static writetoSMIME(Landroid/content/Context;Ljava/io/OutputStream;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/smime/SMIMEHelper;Lcom/android/emailcommon/smime/SMIMEHelper$Message;Ljava/util/ArrayList;ZZZ)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p4, "helper"    # Lcom/android/emailcommon/smime/SMIMEHelper;
    .param p5, "msg"    # Lcom/android/emailcommon/smime/SMIMEHelper$Message;
    .param p7, "credentialAccount"    # Z
    .param p8, "appendQuotedText"    # Z
    .param p9, "isAttachmentDownloadRequired"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/io/OutputStream;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            "Lcom/android/emailcommon/provider/EmailContent$Account;",
            "Lcom/android/emailcommon/smime/SMIMEHelper;",
            "Lcom/android/emailcommon/smime/SMIMEHelper$Message;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;ZZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateManagerException;,
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 639
    .local p6, "recCertificates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/X509Certificate;>;"
    invoke-static/range {p3 .. p3}, Lcom/android/emailcommon/internet/Rfc822Output;->getSigningAlgorithm(Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;

    move-result-object v21

    .line 640
    .local v21, "signingAlgorithm":Ljava/lang/String;
    invoke-static/range {p2 .. p3}, Lcom/android/emailcommon/internet/Rfc822Output;->getEncryptAlforithm(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;

    move-result-object v19

    .line 642
    .local v19, "encryptionAlgorithm":Ljava/lang/String;
    const/16 v18, 0x0

    .line 644
    .local v18, "certMgr":Lcom/android/emailcommon/smime/CertificateMgr;
    const/4 v2, 0x0

    .line 645
    .local v2, "alias":Ljava/lang/String;
    const/16 v20, 0x0

    .line 646
    .local v20, "privateKey":Ljava/security/PrivateKey;
    const/4 v14, 0x0

    .line 648
    .local v14, "certificateForSignature":Ljava/security/cert/X509Certificate;
    if-eqz p7, :cond_3

    .line 649
    const-string v3, "Rfc8220Output"

    const-string v4, " Using Smartcard credential  "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    invoke-static/range {p0 .. p0}, Lcom/android/emailcommon/cac/CACManager;->getCertificateMgr(Landroid/content/Context;)Lcom/android/emailcommon/smime/CertificateMgr;

    move-result-object v18

    .line 651
    invoke-virtual/range {p3 .. p3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/cac/CACManager;->getAliasForSignature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 652
    if-nez v18, :cond_1

    .line 653
    const-string v3, "Rfc8220Output"

    const-string v4, " [CAC debug] certMgr is null. cannot send mail"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/smime/CertificateMgr;->getPrivateKey(Ljava/lang/String;)Ljava/security/Key;

    move-result-object v20

    .end local v20    # "privateKey":Ljava/security/PrivateKey;
    check-cast v20, Ljava/security/PrivateKey;

    .line 658
    .restart local v20    # "privateKey":Ljava/security/PrivateKey;
    invoke-virtual/range {p3 .. p3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/emailcommon/cac/CACManager;->getAliasForSignature(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/android/emailcommon/smime/CertificateMgr;->getCertificate(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v14

    .line 673
    :cond_2
    :goto_1
    const/4 v6, 0x0

    .line 674
    .local v6, "os":Ljava/io/FileOutputStream;
    const-string v3, "eas_input"

    const-string v4, "tmp"

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-static {v3, v4, v5}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v17

    .line 676
    .local v17, "mimeDataFile":Ljava/io/File;
    new-instance v6, Ljava/io/FileOutputStream;

    .end local v6    # "os":Ljava/io/FileOutputStream;
    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 677
    .restart local v6    # "os":Ljava/io/FileOutputStream;
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v3, p0

    move/from16 v7, p8

    move/from16 v9, p9

    move-object/from16 v10, p2

    invoke-static/range {v3 .. v13}, Lcom/android/emailcommon/internet/Rfc822Output;->writetoPlain(Landroid/content/Context;JLjava/io/OutputStream;ZZZLcom/android/emailcommon/provider/EmailContent$Message;ZZZ)V

    .line 678
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-eqz v3, :cond_5

    .line 679
    move-object/from16 v0, p2

    iget-boolean v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/security/cert/X509Certificate;

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Ljava/security/cert/X509Certificate;

    move-object/from16 v7, p4

    move-object/from16 v8, p0

    move-object/from16 v9, p5

    move-object/from16 v11, v19

    move-object/from16 v12, v21

    move-object/from16 v13, v20

    move-object/from16 v16, p1

    invoke-virtual/range {v7 .. v17}, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptMessage(Landroid/content/Context;Lcom/android/emailcommon/smime/SMIMEHelper$Message;ZLjava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;[Ljava/security/cert/X509Certificate;Ljava/io/OutputStream;Ljava/io/File;)V

    .line 686
    :goto_2
    if-eqz v17, :cond_0

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 687
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 662
    .end local v6    # "os":Ljava/io/FileOutputStream;
    .end local v17    # "mimeDataFile":Ljava/io/File;
    :cond_3
    const-string v3, "Rfc8220Output"

    const-string v4, " Using normal credential  "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    new-instance v18, Lcom/android/emailcommon/smime/CertificateMgr;

    .end local v18    # "certMgr":Lcom/android/emailcommon/smime/CertificateMgr;
    sget-object v3, Lcom/android/emailcommon/internet/Rfc822Output;->mDeviceId:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v3, v1}, Lcom/android/emailcommon/smime/CertificateMgr;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 665
    .restart local v18    # "certMgr":Lcom/android/emailcommon/smime/CertificateMgr;
    if-eqz p3, :cond_4

    .line 666
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSmimeOwnSignCertAlias:Ljava/lang/String;

    .line 667
    :cond_4
    const-string v3, "RFC822Output"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "alias= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v3, :cond_2

    .line 669
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/smime/CertificateMgr;->getPrivateKey(Ljava/lang/String;)Ljava/security/Key;

    move-result-object v20

    .end local v20    # "privateKey":Ljava/security/PrivateKey;
    check-cast v20, Ljava/security/PrivateKey;

    .line 670
    .restart local v20    # "privateKey":Ljava/security/PrivateKey;
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/smime/CertificateMgr;->getCertificate(Ljava/lang/String;)Ljava/security/cert/X509Certificate;

    move-result-object v14

    goto/16 :goto_1

    .restart local v6    # "os":Ljava/io/FileOutputStream;
    .restart local v17    # "mimeDataFile":Ljava/io/File;
    :cond_5
    move-object/from16 v10, p4

    move-object/from16 v11, p0

    move-object/from16 v12, p5

    move-object/from16 v13, v20

    move-object/from16 v15, v21

    move-object/from16 v16, p1

    .line 683
    invoke-virtual/range {v10 .. v17}, Lcom/android/emailcommon/smime/SMIMEHelper;->signMessage(Landroid/content/Context;Lcom/android/emailcommon/smime/SMIMEHelper$Message;Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/io/OutputStream;Ljava/io/File;)V

    goto :goto_2
.end method
