.class public Lcom/android/emailcommon/crypto/AESEncryption;
.super Ljava/lang/Object;
.source "AESEncryption.java"


# static fields
.field public static LOGTAG:Ljava/lang/String;

.field static iv:[B

.field private static salt:Ljava/lang/String;

.field public static str:Ljava/lang/String;


# instance fields
.field private cipher:Ljavax/crypto/Cipher;

.field next_salt:[[I

.field next_str:[[I

.field out_char_salt:[[C

.field out_char_str:[[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    sput-object v0, Lcom/android/emailcommon/crypto/AESEncryption;->salt:Ljava/lang/String;

    .line 43
    sput-object v0, Lcom/android/emailcommon/crypto/AESEncryption;->str:Ljava/lang/String;

    .line 81
    const-string v0, "MEALY_TEST"

    sput-object v0, Lcom/android/emailcommon/crypto/AESEncryption;->LOGTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/16 v1, 0x13

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v5

    new-array v2, v4, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_3

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_4

    aput-object v2, v1, v8

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_5

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_6

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_7

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_8

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_9

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_a

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-array v3, v4, [I

    fill-array-data v3, :array_b

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-array v3, v4, [I

    fill-array-data v3, :array_c

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-array v3, v4, [I

    fill-array-data v3, :array_d

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-array v3, v4, [I

    fill-array-data v3, :array_e

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-array v3, v4, [I

    fill-array-data v3, :array_f

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-array v3, v4, [I

    fill-array-data v3, :array_10

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-array v3, v4, [I

    fill-array-data v3, :array_11

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-array v3, v4, [I

    fill-array-data v3, :array_12

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->next_str:[[I

    .line 47
    const/16 v1, 0x13

    new-array v1, v1, [[C

    new-array v2, v4, [C

    fill-array-data v2, :array_13

    aput-object v2, v1, v5

    new-array v2, v4, [C

    fill-array-data v2, :array_14

    aput-object v2, v1, v6

    new-array v2, v4, [C

    fill-array-data v2, :array_15

    aput-object v2, v1, v4

    new-array v2, v4, [C

    fill-array-data v2, :array_16

    aput-object v2, v1, v7

    new-array v2, v4, [C

    fill-array-data v2, :array_17

    aput-object v2, v1, v8

    const/4 v2, 0x5

    new-array v3, v4, [C

    fill-array-data v3, :array_18

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [C

    fill-array-data v3, :array_19

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [C

    fill-array-data v3, :array_1a

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [C

    fill-array-data v3, :array_1b

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [C

    fill-array-data v3, :array_1c

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [C

    fill-array-data v3, :array_1d

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-array v3, v4, [C

    fill-array-data v3, :array_1e

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-array v3, v4, [C

    fill-array-data v3, :array_1f

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-array v3, v4, [C

    fill-array-data v3, :array_20

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-array v3, v4, [C

    fill-array-data v3, :array_21

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-array v3, v4, [C

    fill-array-data v3, :array_22

    aput-object v3, v1, v2

    const/16 v2, 0x10

    new-array v3, v4, [C

    fill-array-data v3, :array_23

    aput-object v3, v1, v2

    const/16 v2, 0x11

    new-array v3, v4, [C

    fill-array-data v3, :array_24

    aput-object v3, v1, v2

    const/16 v2, 0x12

    new-array v3, v4, [C

    fill-array-data v3, :array_25

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->out_char_str:[[C

    .line 49
    const/16 v1, 0x10

    new-array v1, v1, [[I

    new-array v2, v4, [I

    fill-array-data v2, :array_26

    aput-object v2, v1, v5

    new-array v2, v4, [I

    fill-array-data v2, :array_27

    aput-object v2, v1, v6

    new-array v2, v4, [I

    fill-array-data v2, :array_28

    aput-object v2, v1, v4

    new-array v2, v4, [I

    fill-array-data v2, :array_29

    aput-object v2, v1, v7

    new-array v2, v4, [I

    fill-array-data v2, :array_2a

    aput-object v2, v1, v8

    const/4 v2, 0x5

    new-array v3, v4, [I

    fill-array-data v3, :array_2b

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [I

    fill-array-data v3, :array_2c

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [I

    fill-array-data v3, :array_2d

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [I

    fill-array-data v3, :array_2e

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [I

    fill-array-data v3, :array_2f

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [I

    fill-array-data v3, :array_30

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-array v3, v4, [I

    fill-array-data v3, :array_31

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-array v3, v4, [I

    fill-array-data v3, :array_32

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-array v3, v4, [I

    fill-array-data v3, :array_33

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-array v3, v4, [I

    fill-array-data v3, :array_34

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-array v3, v4, [I

    fill-array-data v3, :array_35

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->next_salt:[[I

    .line 50
    const/16 v1, 0x10

    new-array v1, v1, [[C

    new-array v2, v4, [C

    fill-array-data v2, :array_36

    aput-object v2, v1, v5

    new-array v2, v4, [C

    fill-array-data v2, :array_37

    aput-object v2, v1, v6

    new-array v2, v4, [C

    fill-array-data v2, :array_38

    aput-object v2, v1, v4

    new-array v2, v4, [C

    fill-array-data v2, :array_39

    aput-object v2, v1, v7

    new-array v2, v4, [C

    fill-array-data v2, :array_3a

    aput-object v2, v1, v8

    const/4 v2, 0x5

    new-array v3, v4, [C

    fill-array-data v3, :array_3b

    aput-object v3, v1, v2

    const/4 v2, 0x6

    new-array v3, v4, [C

    fill-array-data v3, :array_3c

    aput-object v3, v1, v2

    const/4 v2, 0x7

    new-array v3, v4, [C

    fill-array-data v3, :array_3d

    aput-object v3, v1, v2

    const/16 v2, 0x8

    new-array v3, v4, [C

    fill-array-data v3, :array_3e

    aput-object v3, v1, v2

    const/16 v2, 0x9

    new-array v3, v4, [C

    fill-array-data v3, :array_3f

    aput-object v3, v1, v2

    const/16 v2, 0xa

    new-array v3, v4, [C

    fill-array-data v3, :array_40

    aput-object v3, v1, v2

    const/16 v2, 0xb

    new-array v3, v4, [C

    fill-array-data v3, :array_41

    aput-object v3, v1, v2

    const/16 v2, 0xc

    new-array v3, v4, [C

    fill-array-data v3, :array_42

    aput-object v3, v1, v2

    const/16 v2, 0xd

    new-array v3, v4, [C

    fill-array-data v3, :array_43

    aput-object v3, v1, v2

    const/16 v2, 0xe

    new-array v3, v4, [C

    fill-array-data v3, :array_44

    aput-object v3, v1, v2

    const/16 v2, 0xf

    new-array v3, v4, [C

    fill-array-data v3, :array_45

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->out_char_salt:[[C

    .line 85
    invoke-direct {p0}, Lcom/android/emailcommon/crypto/AESEncryption;->generateIV()[B

    move-result-object v1

    sput-object v1, Lcom/android/emailcommon/crypto/AESEncryption;->iv:[B

    .line 86
    invoke-virtual {p0}, Lcom/android/emailcommon/crypto/AESEncryption;->loadStr()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/emailcommon/crypto/AESEncryption;->str:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Lcom/android/emailcommon/crypto/AESEncryption;->loadSalt()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/emailcommon/crypto/AESEncryption;->salt:Ljava/lang/String;

    .line 93
    :try_start_0
    const-string v1, "AES/CBC/PKCS7Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    return-void

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    goto :goto_0

    .line 98
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 99
    .local v0, "e":Ljavax/crypto/NoSuchPaddingException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    goto :goto_0

    .line 46
    nop

    :array_0
    .array-data 4
        0x7
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x10
        0x6
    .end array-data

    :array_2
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x0
        0xb
    .end array-data

    :array_7
    .array-data 4
        0xa
        0x0
    .end array-data

    :array_8
    .array-data 4
        0xd
        0x8
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x12
        0xa
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_c
    .array-data 4
        0xe
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x0
        0xc
    .end array-data

    :array_e
    .array-data 4
        0x1
        0x2
    .end array-data

    :array_f
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_10
    .array-data 4
        0xe
        0x0
    .end array-data

    :array_11
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_12
    .array-data 4
        0x10
        0x0
    .end array-data

    .line 47
    :array_13
    .array-data 2
        0x71s
        0x28s
    .end array-data

    :array_14
    .array-data 2
        0x42s
        0x32s
    .end array-data

    :array_15
    .array-data 2
        0x6fs
        0x35s
    .end array-data

    :array_16
    .array-data 2
        0x77s
        0x64s
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x3es
    .end array-data

    :array_18
    .array-data 2
        0x65s
        0x40s
    .end array-data

    :array_19
    .array-data 2
        0x3cs
        0x71s
    .end array-data

    :array_1a
    .array-data 2
        0x6bs
        0x78s
    .end array-data

    :array_1b
    .array-data 2
        0x6cs
        0x71s
    .end array-data

    :array_1c
    .array-data 2
        0x32s
        0x45s
    .end array-data

    :array_1d
    .array-data 2
        0x3fs
        0x66s
    .end array-data

    :array_1e
    .array-data 2
        0x6cs
        0x31s
    .end array-data

    :array_1f
    .array-data 2
        0x78s
        0x67s
    .end array-data

    :array_20
    .array-data 2
        0x48s
        0x42s
    .end array-data

    :array_21
    .array-data 2
        0x6es
        0x72s
    .end array-data

    :array_22
    .array-data 2
        0x3ds
        0x5es
    .end array-data

    :array_23
    .array-data 2
        0x21s
        0x4cs
    .end array-data

    :array_24
    .array-data 2
        0x4es
        0x46s
    .end array-data

    :array_25
    .array-data 2
        0x29s
        0x54s
    .end array-data

    .line 49
    :array_26
    .array-data 4
        0x9
        0x8
    .end array-data

    :array_27
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_28
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_29
    .array-data 4
        0x0
        0xb
    .end array-data

    :array_2a
    .array-data 4
        0x0
        0x6
    .end array-data

    :array_2b
    .array-data 4
        0x7
        0x2
    .end array-data

    :array_2c
    .array-data 4
        0x3
        0x9
    .end array-data

    :array_2d
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_2e
    .array-data 4
        0x6
        0x8
    .end array-data

    :array_2f
    .array-data 4
        0x5
        0x4
    .end array-data

    :array_30
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_31
    .array-data 4
        0x0
        0xe
    .end array-data

    :array_32
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_33
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_34
    .array-data 4
        0x0
        0xc
    .end array-data

    :array_35
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 50
    :array_36
    .array-data 2
        0x65s
        0x73s
    .end array-data

    :array_37
    .array-data 2
        0x70s
        0x31s
    .end array-data

    :array_38
    .array-data 2
        0x67s
        0x63s
    .end array-data

    :array_39
    .array-data 2
        0x5ds
        0x61s
    .end array-data

    :array_3a
    .array-data 2
        0x76s
        0x5fs
    .end array-data

    :array_3b
    .array-data 2
        0x5fs
        0x6es
    .end array-data

    :array_3c
    .array-data 2
        0x73s
        0x73s
    .end array-data

    :array_3d
    .array-data 2
        0x3fs
        0x73s
    .end array-data

    :array_3e
    .array-data 2
        0x6ds
        0x61s
    .end array-data

    :array_3f
    .array-data 2
        0x75s
        0x63s
    .end array-data

    :array_40
    .array-data 2
        0x48s
        0x45s
    .end array-data

    :array_41
    .array-data 2
        0x45s
        0x6cs
    .end array-data

    :array_42
    .array-data 2
        0x49s
        0x60s
    .end array-data

    :array_43
    .array-data 2
        0x55s
        0x67s
    .end array-data

    :array_44
    .array-data 2
        0x75s
        0x74s
    .end array-data

    :array_45
    .array-data 2
        0x78s
        0x6fs
    .end array-data
.end method

.method private final generateIV()[B
    .locals 3

    .prologue
    .line 160
    const/16 v2, 0x10

    :try_start_0
    new-array v1, v2, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 166
    :goto_0
    return-object v1

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "passcode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 140
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-object v3

    .line 143
    :cond_1
    const/4 v0, 0x0

    .line 146
    .local v0, "decrypted":[B
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v4, Lcom/android/emailcommon/crypto/AESEncryption;->iv:[B

    invoke-direct {v2, v4}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 147
    .local v2, "ivspec":Ljavax/crypto/spec/IvParameterSpec;
    iget-object v4, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    const/4 v5, 0x2

    invoke-virtual {p0, p2}, Lcom/android/emailcommon/crypto/AESEncryption;->getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 148
    iget-object v4, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    const/4 v5, 0x0

    invoke-static {p1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 154
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0

    .line 151
    .end local v2    # "ivspec":Ljavax/crypto/spec/IvParameterSpec;
    :catch_0
    move-exception v1

    .line 152
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public encrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "passcode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 119
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move-object v2, v4

    .line 135
    :goto_0
    return-object v2

    .line 122
    :cond_1
    const/4 v1, 0x0

    .line 123
    .local v1, "encrypted":[B
    const/4 v2, 0x0

    .line 126
    .local v2, "encrypted64":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/IvParameterSpec;

    sget-object v5, Lcom/android/emailcommon/crypto/AESEncryption;->iv:[B

    invoke-direct {v3, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 127
    .local v3, "ivspec":Ljavax/crypto/spec/IvParameterSpec;
    iget-object v5, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    const/4 v6, 0x1

    invoke-virtual {p0, p2}, Lcom/android/emailcommon/crypto/AESEncryption;->getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 128
    iget-object v5, p0, Lcom/android/emailcommon/crypto/AESEncryption;->cipher:Ljavax/crypto/Cipher;

    const-string v6, "UTF-8"

    invoke-virtual {p1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 129
    const/4 v5, 0x0

    invoke-static {v1, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 131
    .end local v3    # "ivspec":Ljavax/crypto/spec/IvParameterSpec;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-object v2, v4

    .line 132
    goto :goto_0
.end method

.method getInternalStr(II[[I[[C)Ljava/lang/String;
    .locals 6
    .param p1, "v"    # I
    .param p2, "sz"    # I
    .param p3, "next"    # [[I
    .param p4, "go"    # [[C

    .prologue
    .line 66
    new-array v4, p2, [C

    .line 67
    .local v4, "str":[C
    const/4 v3, 0x0

    .local v3, "state":I
    const/4 v1, 0x0

    .line 69
    .local v1, "len":I
    const/16 v5, 0x64

    if-le p2, v5, :cond_1

    .line 70
    const/4 v5, 0x0

    .line 78
    :goto_0
    return-object v5

    .line 73
    .end local v1    # "len":I
    .local v2, "len":I
    :goto_1
    if-ge v2, p2, :cond_0

    .line 74
    and-int/lit8 v0, p1, 0x1

    .local v0, "input":I
    shr-int/lit8 p1, p1, 0x1

    .line 75
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "len":I
    .restart local v1    # "len":I
    aget-object v5, p4, v3

    aget-char v5, v5, v0

    aput-char v5, v4, v2

    .line 76
    aget-object v5, p3, v3

    aget v3, v5, v0

    move v2, v1

    .line 77
    .end local v1    # "len":I
    .restart local v2    # "len":I
    goto :goto_1

    .line 78
    .end local v0    # "input":I
    :cond_0
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>([C)V

    move v1, v2

    .end local v2    # "len":I
    .restart local v1    # "len":I
    goto :goto_0

    :cond_1
    move v2, v1

    .end local v1    # "len":I
    .restart local v2    # "len":I
    goto :goto_1
.end method

.method public getSecretKey(Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 9
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 106
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    sget-object v6, Lcom/android/emailcommon/crypto/AESEncryption;->salt:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    const/16 v7, 0x64

    const/16 v8, 0x80

    invoke-direct {v2, v5, v6, v7, v8}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C[BII)V

    .line 107
    .local v2, "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    const-string v5, "PBKDF2WithHmacSHA1"

    invoke-static {v5}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v1

    .line 108
    .local v1, "factory":Ljavax/crypto/SecretKeyFactory;
    invoke-virtual {v1, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v4

    .line 109
    .local v4, "tmp":Ljavax/crypto/SecretKey;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {v4}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v5

    const-string v6, "AES"

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    .end local v1    # "factory":Ljavax/crypto/SecretKeyFactory;
    .end local v2    # "pbeKeySpec":Ljavax/crypto/spec/PBEKeySpec;
    .end local v4    # "tmp":Ljavax/crypto/SecretKey;
    :goto_0
    return-object v3

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method final loadSalt()Ljava/lang/String;
    .locals 5

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, "str":Ljava/lang/String;
    const v1, 0xed2b

    const/16 v2, 0x10

    iget-object v3, p0, Lcom/android/emailcommon/crypto/AESEncryption;->next_salt:[[I

    iget-object v4, p0, Lcom/android/emailcommon/crypto/AESEncryption;->out_char_salt:[[C

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/emailcommon/crypto/AESEncryption;->getInternalStr(II[[I[[C)Ljava/lang/String;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method final loadStr()Ljava/lang/String;
    .locals 5

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "str":Ljava/lang/String;
    const v1, 0x128cb

    const/16 v2, 0x13

    iget-object v3, p0, Lcom/android/emailcommon/crypto/AESEncryption;->next_str:[[I

    iget-object v4, p0, Lcom/android/emailcommon/crypto/AESEncryption;->out_char_str:[[C

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/emailcommon/crypto/AESEncryption;->getInternalStr(II[[I[[C)Ljava/lang/String;

    move-result-object v0

    .line 55
    return-object v0
.end method
