.class public Lcom/android/emailcommon/crypto/AESEncryptionUtil;
.super Ljava/lang/Object;
.source "AESEncryptionUtil.java"


# static fields
.field public static LOGTAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "MEALY_TEST"

    sput-object v0, Lcom/android/emailcommon/crypto/AESEncryptionUtil;->LOGTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AESDecryption(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "encryptpassword"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v2, Lcom/android/emailcommon/crypto/AESEncryption;

    invoke-direct {v2}, Lcom/android/emailcommon/crypto/AESEncryption;-><init>()V

    .line 38
    .local v2, "enc":Lcom/android/emailcommon/crypto/AESEncryption;
    move-object v0, p0

    .line 42
    .local v0, "decCode":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/android/emailcommon/crypto/AESEncryption;->str:Ljava/lang/String;

    invoke-virtual {v2, p0, v3}, Lcom/android/emailcommon/crypto/AESEncryption;->decrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public static AESEncryption(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    .line 18
    new-instance v0, Lcom/android/emailcommon/crypto/AESEncryption;

    invoke-direct {v0}, Lcom/android/emailcommon/crypto/AESEncryption;-><init>()V

    .line 25
    .local v0, "enc":Lcom/android/emailcommon/crypto/AESEncryption;
    sget-object v2, Lcom/android/emailcommon/crypto/AESEncryption;->str:Ljava/lang/String;

    invoke-virtual {v0, p0, v2}, Lcom/android/emailcommon/crypto/AESEncryption;->encrypt(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "encCode":Ljava/lang/String;
    return-object v1
.end method
