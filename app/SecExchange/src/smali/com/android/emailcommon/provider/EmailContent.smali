.class public abstract Lcom/android/emailcommon/provider/EmailContent;
.super Ljava/lang/Object;
.source "EmailContent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/provider/EmailContent$1;,
        Lcom/android/emailcommon/provider/EmailContent$TopStories;,
        Lcom/android/emailcommon/provider/EmailContent$TopStoriesColumns;,
        Lcom/android/emailcommon/provider/EmailContent$FavoriteContact;,
        Lcom/android/emailcommon/provider/EmailContent$FavoriteContactColumns;,
        Lcom/android/emailcommon/provider/EmailContent$BlackListMessage;,
        Lcom/android/emailcommon/provider/EmailContent$BlackListMessageColumns;,
        Lcom/android/emailcommon/provider/EmailContent$BlackList;,
        Lcom/android/emailcommon/provider/EmailContent$BlackListColumns;,
        Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;,
        Lcom/android/emailcommon/provider/EmailContent$EmailAddressCacheColumns;,
        Lcom/android/emailcommon/provider/EmailContent$EmailContextualUsageInfoColumns;,
        Lcom/android/emailcommon/provider/EmailContent$FilterListColumns;,
        Lcom/android/emailcommon/provider/EmailContent$VIPListColumns;,
        Lcom/android/emailcommon/provider/EmailContent$LDAPAccountColumns;,
        Lcom/android/emailcommon/provider/EmailContent$HistoryAccountColumns;,
        Lcom/android/emailcommon/provider/EmailContent$AccountCB;,
        Lcom/android/emailcommon/provider/EmailContent$MessageCB;,
        Lcom/android/emailcommon/provider/EmailContent$MailboxCB;,
        Lcom/android/emailcommon/provider/EmailContent$AccountCBColumns;,
        Lcom/android/emailcommon/provider/EmailContent$MessageCBColumns;,
        Lcom/android/emailcommon/provider/EmailContent$MailboxCBColumns;,
        Lcom/android/emailcommon/provider/EmailContent$CertificateCacheColumns;,
        Lcom/android/emailcommon/provider/EmailContent$CRLCacheColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Document;,
        Lcom/android/emailcommon/provider/EmailContent$DocumentColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Policies;,
        Lcom/android/emailcommon/provider/EmailContent$PoliciesColumns;,
        Lcom/android/emailcommon/provider/EmailContent$HostAuth;,
        Lcom/android/emailcommon/provider/EmailContent$HostAuthColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Mailbox;,
        Lcom/android/emailcommon/provider/EmailContent$MailboxColumns;,
        Lcom/android/emailcommon/provider/EmailContent$QuickResponseColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Attachment;,
        Lcom/android/emailcommon/provider/EmailContent$AttachmentColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Account;,
        Lcom/android/emailcommon/provider/EmailContent$AccountColumns;,
        Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;,
        Lcom/android/emailcommon/provider/EmailContent$RecipientInformationColumns;,
        Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;,
        Lcom/android/emailcommon/provider/EmailContent$FollowupFlagColumns;,
        Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;,
        Lcom/android/emailcommon/provider/EmailContent$IRMTemplateColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Message;,
        Lcom/android/emailcommon/provider/EmailContent$MessageColumns;,
        Lcom/android/emailcommon/provider/EmailContent$Body;,
        Lcom/android/emailcommon/provider/EmailContent$BodyColumns;,
        Lcom/android/emailcommon/provider/EmailContent$SyncColumns;
    }
.end annotation


# static fields
.field public static final CONTENT_NOTIFIER_URI:Landroid/net/Uri;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTROLED_SYNC_PROJECTION:[Ljava/lang/String;

.field public static final COUNT_COLUMNS:[Ljava/lang/String;

.field public static DEFAULTAUTORETRYTIMES:I

.field public static DEFAULTRINGTONEURI:Ljava/lang/String;

.field public static DRAFTS_SYNC_PROJECTION:[Ljava/lang/String;

.field public static final EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

.field public static final ID_PROJECTION:[Ljava/lang/String;

.field private static MESSAGEID_TO_ACCOUNTID_COLUMN_ACCOUNTID:I

.field private static MESSAGEID_TO_ACCOUNTID_PROJECTION:[Ljava/lang/String;

.field private static MESSAGEID_TO_MAILBOXID_COLUMN_MAILBOXID:I

.field private static MESSAGEID_TO_MAILBOXID_PROJECTION:[Ljava/lang/String;


# instance fields
.field public mBaseUri:Landroid/net/Uri;

.field public mId:J

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0xe

    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 98
    const-string v0, "content://com.android.email.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    .line 102
    const-string v0, "content://com.android.email.notifier"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_NOTIFIER_URI:Landroid/net/Uri;

    .line 113
    new-array v0, v3, [Ljava/lang/String;

    const-string v2, "count(*)"

    aput-object v2, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->COUNT_COLUMNS:[Ljava/lang/String;

    .line 117
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->DEFAULTRINGTONEURI:Ljava/lang/String;

    .line 127
    invoke-static {}, Lcom/android/emailcommon/utility/SecFeatureWrapper;->getCarrierId()I

    move-result v0

    if-ne v0, v6, :cond_4

    move v0, v1

    :goto_1
    sput v0, Lcom/android/emailcommon/provider/EmailContent;->DEFAULTAUTORETRYTIMES:I

    .line 136
    new-array v0, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    .line 142
    new-array v0, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    const-string v2, "emailAddress"

    aput-object v2, v0, v3

    const-string v2, "displayName"

    aput-object v2, v0, v5

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

    .line 155
    new-array v0, v5, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    const-string v2, "syncInterval"

    aput-object v2, v0, v3

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTROLED_SYNC_PROJECTION:[Ljava/lang/String;

    .line 159
    new-array v0, v5, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    const-string v2, "mailboxKey"

    aput-object v2, v0, v3

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->MESSAGEID_TO_MAILBOXID_PROJECTION:[Ljava/lang/String;

    .line 162
    new-array v0, v5, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    const-string v2, "accountKey"

    aput-object v2, v0, v3

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->MESSAGEID_TO_ACCOUNTID_PROJECTION:[Ljava/lang/String;

    .line 166
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v4

    const-string v2, "syncServerId"

    aput-object v2, v0, v3

    const-string v2, "accountKey"

    aput-object v2, v0, v5

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "dirtyCommit"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent;->DRAFTS_SYNC_PROJECTION:[Ljava/lang/String;

    .line 171
    sput v3, Lcom/android/emailcommon/provider/EmailContent;->MESSAGEID_TO_MAILBOXID_COLUMN_MAILBOXID:I

    .line 173
    sput v3, Lcom/android/emailcommon/provider/EmailContent;->MESSAGEID_TO_ACCOUNTID_COLUMN_ACCOUNTID:I

    return-void

    .line 117
    :cond_0
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_AIO:Z

    if-eqz v0, :cond_1

    const-string v0, "/system/media/audio/notifications/Cricket_Keyboard.ogg"

    goto :goto_0

    :cond_1
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ATT:Z

    if-eqz v0, :cond_2

    const-string v0, "/system/media/audio/notifications/S_Dew_drops.ogg"

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/emailcommon/utility/SecFeatureWrapper;->getCarrierId()I

    move-result v0

    if-ne v0, v6, :cond_3

    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, "/system/media/audio/notifications/S_Postman.ogg"

    goto :goto_0

    .line 127
    :cond_4
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_WHOLE_SPRINT:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x3e8

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/android/emailcommon/utility/SecFeatureWrapper;->getCarrierId()I

    move-result v0

    if-ne v0, v3, :cond_6

    move v0, v1

    goto/16 :goto_1

    :cond_6
    const/16 v0, 0xa

    goto/16 :goto_1
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 386
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mUri:Landroid/net/Uri;

    .line 202
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    .line 387
    return-void
.end method

.method public static count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 302
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->COUNT_COLUMNS:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v7}, Lcom/android/emailcommon/utility/Utility;->getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method

.method public static delete(Landroid/content/Context;Landroid/net/Uri;J)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 286
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/android/emailcommon/provider/EmailContent;",
            ">(",
            "Landroid/database/Cursor;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "klass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent;

    .line 230
    .local v0, "content":Lcom/android/emailcommon/provider/EmailContent;, "TT;"
    const/4 v2, 0x0

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    .line 231
    invoke-virtual {v0, p0}, Lcom/android/emailcommon/provider/EmailContent;->restore(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 238
    .end local v0    # "content":Lcom/android/emailcommon/provider/EmailContent;, "TT;"
    :goto_0
    return-object v0

    .line 233
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 238
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 235
    :catch_1
    move-exception v1

    .line 236
    .local v1, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1
.end method

.method public static isSNCAccount(Landroid/content/Context;Ljava/lang/Long;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acctId"    # Ljava/lang/Long;

    .prologue
    .line 357
    const/4 v2, 0x0

    .line 359
    .local v2, "result":Z
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v1

    .line 362
    .local v1, "localAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v1, :cond_0

    .line 363
    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 364
    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p0, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 367
    .local v0, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    const-string v4, "snc"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 368
    const-string v3, "EmailContent >>"

    const-string v4, "account is SNC "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    const/4 v2, 0x1

    .line 373
    .end local v0    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :cond_0
    return v2
.end method

.method public static update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "id"    # J
    .param p4, "contentValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 281
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p4, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static uriWithLimit(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "limit"    # I

    .prologue
    .line 377
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getUri()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mBaseUri:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mUri:Landroid/net/Uri;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isSaved()Z
    .locals 4

    .prologue
    .line 223
    iget-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract restore(Landroid/database/Cursor;)V
.end method

.method public save(Landroid/content/Context;)Landroid/net/Uri;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/android/emailcommon/provider/EmailContent;->isSaved()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 245
    :cond_0
    if-eqz p1, :cond_1

    .line 246
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent;->mBaseUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/android/emailcommon/provider/EmailContent;->toContentValues()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 247
    .local v0, "res":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/emailcommon/provider/EmailContent;->mId:J

    .line 250
    .end local v0    # "res":Landroid/net/Uri;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract toContentValues()Landroid/content/ContentValues;
.end method

.method public update(Landroid/content/Context;Landroid/content/ContentValues;)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v2, 0x0

    .line 274
    invoke-virtual {p0}, Lcom/android/emailcommon/provider/EmailContent;->isSaved()Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 277
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/emailcommon/provider/EmailContent;->getUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p2, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
