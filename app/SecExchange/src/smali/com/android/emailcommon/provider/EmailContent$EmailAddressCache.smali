.class public Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;
.super Lcom/android/emailcommon/provider/EmailContent;
.source "EmailContent.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/android/emailcommon/provider/EmailContent$EmailAddressCacheColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/provider/EmailContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EmailAddressCache"
.end annotation


# static fields
.field public static final CONTENT_FILTER_URI:Landroid/net/Uri;

.field public static final CONTENT_PROJECTION:[Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;


# instance fields
.field public mDisplayName:Ljava/lang/String;

.field public mEmailAddress:Ljava/lang/String;

.field public mOperationType:I

.field public mPhotoContentBytes:[B

.field public mPrefix:Ljava/lang/String;

.field public mRank:I

.field public mSource:Ljava/lang/String;

.field public mTimestamp:J

.field public mUsagecount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 12126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/emailaddresscache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->CONTENT_URI:Landroid/net/Uri;

    .line 12129
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "filter"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->CONTENT_FILTER_URI:Landroid/net/Uri;

    .line 12154
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "accountAddress"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "operationType"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "accountPrefix"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "accountRank"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "accountSource"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "timeStamp"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "usageCount"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "photocontentbytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->CONTENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 12296
    const/4 v0, 0x0

    return v0
.end method

.method public restore(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 12261
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mId:J

    .line 12262
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mEmailAddress:Ljava/lang/String;

    .line 12263
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mDisplayName:Ljava/lang/String;

    .line 12264
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mSource:Ljava/lang/String;

    .line 12265
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPrefix:Ljava/lang/String;

    .line 12266
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mTimestamp:J

    .line 12267
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mUsagecount:I

    .line 12268
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mOperationType:I

    .line 12269
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mRank:I

    .line 12271
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPhotoContentBytes:[B

    .line 12273
    return-void
.end method

.method public toContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 12278
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 12280
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "accountAddress"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 12281
    const-string v1, "accountName"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 12282
    const-string v1, "operationType"

    iget v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mOperationType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 12283
    const-string v1, "accountPrefix"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 12284
    const-string v1, "accountRank"

    iget v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mRank:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 12285
    const-string v1, "accountSource"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mSource:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 12286
    const-string v1, "timeStamp"

    iget-wide v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 12287
    const-string v1, "usageCount"

    iget v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mUsagecount:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 12289
    const-string v1, "photocontentbytes"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPhotoContentBytes:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 12291
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 12300
    iget-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 12301
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 12302
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 12303
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mSource:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 12304
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPrefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 12305
    iget-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 12306
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mUsagecount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12307
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mOperationType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12308
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mRank:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12310
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPhotoContentBytes:[B

    if-nez v0, :cond_0

    .line 12311
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12316
    :goto_0
    return-void

    .line 12313
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPhotoContentBytes:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 12314
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$EmailAddressCache;->mPhotoContentBytes:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
