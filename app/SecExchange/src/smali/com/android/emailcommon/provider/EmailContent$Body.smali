.class public final Lcom/android/emailcommon/provider/EmailContent$Body;
.super Lcom/android/emailcommon/provider/EmailContent;
.source "EmailContent.java"

# interfaces
.implements Lcom/android/emailcommon/provider/EmailContent$BodyColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/provider/EmailContent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Body"
.end annotation


# static fields
.field public static final COMMON_PROJECTION_HTML:[Ljava/lang/String;

.field public static final COMMON_PROJECTION_INTRO:[Ljava/lang/String;

.field public static final COMMON_PROJECTION_REPLY_HTML:[Ljava/lang/String;

.field public static final COMMON_PROJECTION_REPLY_TEXT:[Ljava/lang/String;

.field public static final COMMON_PROJECTION_SOURCE:[Ljava/lang/String;

.field public static final COMMON_PROJECTION_TEXT:[Ljava/lang/String;

.field public static final CONTENT_PROJECTION:[Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final PROJECTION_FILE_SAVE_FLAGS:[Ljava/lang/String;

.field private static final PROJECTION_SOURCE_KEY:[Ljava/lang/String;


# instance fields
.field public mFileSaveFlags:I

.field public mHtmlContent:Ljava/lang/String;

.field public mHtmlReply:Ljava/lang/String;

.field public mIntroText:Ljava/lang/String;

.field public mMessageKey:J

.field public mSourceKey:J

.field public mTextContent:Ljava/lang/String;

.field public mTextReply:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 434
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/body"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    .line 472
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "messageKey"

    aput-object v1, v0, v4

    const-string v1, "htmlContent"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "textContent"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "htmlReply"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "textReply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sourceMessageKey"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "introText"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "fileSaveFlags"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_PROJECTION:[Ljava/lang/String;

    .line 478
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "textContent"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_TEXT:[Ljava/lang/String;

    .line 482
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "htmlContent"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_HTML:[Ljava/lang/String;

    .line 486
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "textReply"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_REPLY_TEXT:[Ljava/lang/String;

    .line 490
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "htmlReply"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_REPLY_HTML:[Ljava/lang/String;

    .line 494
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "introText"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_INTRO:[Ljava/lang/String;

    .line 498
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "sourceMessageKey"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_SOURCE:[Ljava/lang/String;

    .line 504
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "sourceMessageKey"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->PROJECTION_SOURCE_KEY:[Ljava/lang/String;

    .line 508
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "fileSaveFlags"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->PROJECTION_FILE_SAVE_FLAGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 527
    invoke-direct {p0}, Lcom/android/emailcommon/provider/EmailContent;-><init>()V

    .line 528
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mBaseUri:Landroid/net/Uri;

    .line 529
    return-void
.end method

.method public static bodyFilesRemoved(Landroid/content/Context;JJ)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 981
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 982
    const-string v1, "EmailContent >>"

    const-string v2, "bodyFilesRemoved: no body files insde container"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    const/4 v7, 0x0

    .line 1026
    :cond_0
    :goto_0
    return v7

    .line 985
    :cond_1
    const/4 v7, 0x0

    .line 986
    .local v7, "result":Z
    invoke-static {p0, p3, p4}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreFileSaveFlags(Landroid/content/Context;J)I

    move-result v6

    .line 987
    .local v6, "flags":I
    move v0, v6

    .line 989
    .local v0, "oldFlags":I
    if-lez v6, :cond_6

    .line 990
    and-int/lit8 v1, v6, 0x1

    if-eqz v1, :cond_2

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->isHtmlContentFileExists(Landroid/content/Context;JJ)Z

    move-result v1

    if-nez v1, :cond_2

    .line 992
    and-int/lit8 v6, v6, -0x2

    .line 993
    const/4 v7, 0x1

    .line 996
    :cond_2
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_3

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->isTextContentFileExists(Landroid/content/Context;JJ)Z

    move-result v1

    if-nez v1, :cond_3

    .line 998
    and-int/lit8 v6, v6, -0x3

    .line 999
    const/4 v7, 0x1

    .line 1002
    :cond_3
    and-int/lit8 v1, v6, 0x4

    if-eqz v1, :cond_4

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->isHtmlReplyFileExists(Landroid/content/Context;JJ)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1004
    and-int/lit8 v6, v6, -0x5

    .line 1005
    const/4 v7, 0x1

    .line 1008
    :cond_4
    and-int/lit8 v1, v6, 0x8

    if-eqz v1, :cond_5

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->isTextReplyFileExists(Landroid/content/Context;JJ)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1010
    and-int/lit8 v6, v6, -0x9

    .line 1011
    const/4 v7, 0x1

    .line 1014
    :cond_5
    and-int/lit8 v1, v6, 0x10

    if-eqz v1, :cond_6

    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->isIntroFileExists(Landroid/content/Context;JJ)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1016
    and-int/lit8 v6, v6, -0x11

    .line 1017
    const/4 v7, 0x1

    .line 1021
    :cond_6
    if-eq v6, v0, :cond_0

    .line 1022
    const-string v1, "EmailContent >>"

    const-string v2, "Body.bodyFilesRemoved(): some body files were removed. Update flags in DB"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    .line 1023
    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateTruncatedFlags(Landroid/content/Context;JJI)V

    goto :goto_0
.end method

.method public static lookupBodyIdWithMessageId(Landroid/content/Context;J)J
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    const/4 v6, 0x0

    .line 678
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Body;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "messageKey=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const/4 v5, 0x0

    const-wide/16 v8, -0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/android/emailcommon/utility/Utility;->getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private restoreBodyFromFiles(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 573
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v2

    .line 574
    .local v2, "accountId":J
    iget v7, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 576
    .local v7, "oldFlags":I
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 577
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/BodyUtilites;->readHtmlContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 578
    .local v0, "fileContent":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 579
    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 585
    .end local v0    # "fileContent":Ljava/lang/String;
    :cond_0
    :goto_0
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 586
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/BodyUtilites;->readTextContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 587
    .restart local v0    # "fileContent":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 588
    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 595
    .end local v0    # "fileContent":Ljava/lang/String;
    :cond_1
    :goto_1
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_2

    .line 596
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/BodyUtilites;->readTextReplyFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 597
    .restart local v0    # "fileContent":Ljava/lang/String;
    if-eqz v0, :cond_8

    .line 598
    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    .line 604
    .end local v0    # "fileContent":Ljava/lang/String;
    :cond_2
    :goto_2
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_3

    .line 605
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/BodyUtilites;->readHtmlReplyFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 606
    .restart local v0    # "fileContent":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 607
    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    .line 613
    .end local v0    # "fileContent":Ljava/lang/String;
    :cond_3
    :goto_3
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 614
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/BodyUtilites;->readIntroFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 615
    .restart local v0    # "fileContent":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 616
    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    .line 622
    .end local v0    # "fileContent":Ljava/lang/String;
    :cond_4
    :goto_4
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    if-eq v7, v1, :cond_5

    .line 623
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateTruncatedFlags(Landroid/content/Context;JJI)V

    .line 625
    :cond_5
    return-void

    .line 581
    .restart local v0    # "fileContent":Ljava/lang/String;
    :cond_6
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    goto :goto_0

    .line 591
    :cond_7
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, -0x3

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    goto :goto_1

    .line 600
    :cond_8
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    goto :goto_2

    .line 609
    :cond_9
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    goto :goto_3

    .line 618
    :cond_a
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    and-int/lit8 v1, v1, -0x11

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    goto :goto_4
.end method

.method public static restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    .line 857
    const/4 v0, 0x0

    .line 858
    .local v0, "resultContent":Ljava/lang/String;
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreFileSaveFlags(Landroid/content/Context;J)I

    move-result v6

    .line 859
    .local v6, "flags":I
    and-int/lit8 v1, v6, 0x1

    if-eqz v1, :cond_0

    .line 860
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v2

    .line 861
    .local v2, "accountId":J
    invoke-static {p0, v2, v3, p1, p2}, Lcom/android/emailcommon/utility/BodyUtilites;->readHtmlContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 863
    if-nez v0, :cond_0

    .line 864
    const-string v1, "EmailContent >>"

    const-string v4, "restoreBodyHtmlWithMessageId : content is null, mark message as truncated"

    invoke-static {v1, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    and-int/lit8 v6, v6, -0x2

    move-object v1, p0

    move-wide v4, p1

    .line 866
    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateTruncatedFlags(Landroid/content/Context;JJI)V

    .line 870
    .end local v2    # "accountId":J
    :cond_0
    if-nez v0, :cond_1

    .line 871
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_HTML:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreTextWithMessageId(Landroid/content/Context;J[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 873
    :cond_1
    return-object v0
.end method

.method public static restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    .line 837
    const/4 v0, 0x0

    .line 838
    .local v0, "resultContent":Ljava/lang/String;
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreFileSaveFlags(Landroid/content/Context;J)I

    move-result v6

    .line 839
    .local v6, "flags":I
    and-int/lit8 v1, v6, 0x2

    if-eqz v1, :cond_0

    .line 840
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v2

    .line 841
    .local v2, "accountId":J
    invoke-static {p0, v2, v3, p1, p2}, Lcom/android/emailcommon/utility/BodyUtilites;->readTextContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v0

    .line 843
    if-nez v0, :cond_0

    .line 844
    const-string v1, "EmailContent >>"

    const-string v4, "restoreBodyTextWithMessageId : content is null, mark message as truncated"

    invoke-static {v1, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    and-int/lit8 v6, v6, -0x3

    move-object v1, p0

    move-wide v4, p1

    .line 846
    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateTruncatedFlags(Landroid/content/Context;JJI)V

    .line 850
    .end local v2    # "accountId":J
    :cond_0
    if-nez v0, :cond_1

    .line 851
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->COMMON_PROJECTION_TEXT:[Ljava/lang/String;

    invoke-static {p0, p1, p2, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreTextWithMessageId(Landroid/content/Context;J[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 853
    :cond_1
    return-object v0
.end method

.method private static restoreBodyWithCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/emailcommon/provider/EmailContent$Body;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 629
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 630
    const-class v3, Lcom/android/emailcommon/provider/EmailContent$Body;

    invoke-static {p1, v3}, Lcom/android/emailcommon/provider/EmailContent$Body;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Body;

    .line 631
    .local v0, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    if-eqz v0, :cond_0

    .line 632
    invoke-direct {v0, p0}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyFromFiles(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 642
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 643
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .end local v0    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    :cond_1
    :goto_0
    return-object v0

    .line 642
    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 643
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v2

    goto :goto_0

    .line 638
    :catch_0
    move-exception v1

    .line 639
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "EmailContent >>"

    const-string v4, "Exception in restoreBodyWithCursor"

    invoke-static {v3, v4, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 642
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_4

    .line 643
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    move-object v0, v2

    goto :goto_0

    .line 642
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz p1, :cond_5

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_5

    .line 643
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method public static restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    const/4 v7, 0x0

    .line 659
    const/4 v6, 0x0

    .line 661
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v3, "messageKey=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 667
    :goto_0
    if-eqz v6, :cond_0

    .line 668
    invoke-static {p0, v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v0

    .line 670
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v7

    goto :goto_1

    .line 665
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static restoreFileSaveFlags(Landroid/content/Context;J)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    const/4 v6, 0x0

    .line 808
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Body;->PROJECTION_FILE_SAVE_FLAGS:[Ljava/lang/String;

    const-string v3, "messageKey=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v0, p0

    invoke-static/range {v0 .. v7}, Lcom/android/emailcommon/utility/Utility;->getFirstRowInt(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private static restoreTextWithMessageId(Landroid/content/Context;J[Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 816
    const/4 v6, 0x0

    .line 818
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "messageKey=?"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x0

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 822
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 823
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 831
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 832
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v0

    .line 831
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 832
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v8

    goto :goto_0

    .line 827
    :catch_0
    move-exception v7

    .line 828
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "EmailContent >>"

    const-string v1, "Exception in restoreTextWithMessageId"

    invoke-static {v0, v1, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 831
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 832
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v8

    goto :goto_0

    .line 831
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 832
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private static saveBodyToFilesIfNecessary(Landroid/content/Context;JLandroid/content/ContentValues;)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "cv"    # Landroid/content/ContentValues;

    .prologue
    .line 719
    if-eqz p0, :cond_6

    if-eqz p3, :cond_6

    const-wide/16 v6, 0x0

    cmp-long v3, p1, v6

    if-lez v3, :cond_6

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v3

    if-nez v3, :cond_6

    .line 720
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v4

    .line 721
    .local v4, "accountId":J
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreFileSaveFlags(Landroid/content/Context;J)I

    move-result v2

    .line 723
    .local v2, "flags":I
    const/4 v8, 0x0

    .line 724
    .local v8, "htmlText":Ljava/lang/String;
    const-string v3, "htmlContent"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 725
    const-string v3, "htmlContent"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 726
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    const v6, 0x32000

    if-le v3, v6, :cond_0

    move-object v3, p0

    move-wide/from16 v6, p1

    .line 727
    invoke-static/range {v3 .. v8}, Lcom/android/emailcommon/utility/BodyUtilites;->writeHtmlContentToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 728
    or-int/lit8 v2, v2, 0x1

    .line 729
    const-string v3, "htmlContent"

    const v6, 0x32000

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    invoke-static {v8, v6, v7, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    :cond_0
    const-string v3, "textContent"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 735
    const-string v3, "textContent"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 736
    .local v14, "text":Ljava/lang/String;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const v6, 0x32000

    if-le v3, v6, :cond_2

    move-object v9, p0

    move-wide v10, v4

    move-wide/from16 v12, p1

    .line 737
    invoke-static/range {v9 .. v14}, Lcom/android/emailcommon/utility/BodyUtilites;->writeTextContentToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 738
    or-int/lit8 v2, v2, 0x2

    .line 739
    const-string v3, "textContent"

    const v6, 0x32000

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v14, v6, v7, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    .end local v14    # "text":Ljava/lang/String;
    :cond_2
    const-string v3, "textReply"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 745
    const-string v3, "textReply"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 746
    .restart local v14    # "text":Ljava/lang/String;
    if-eqz v14, :cond_3

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const v6, 0x32000

    if-le v3, v6, :cond_3

    move-object v9, p0

    move-wide v10, v4

    move-wide/from16 v12, p1

    .line 747
    invoke-static/range {v9 .. v14}, Lcom/android/emailcommon/utility/BodyUtilites;->writeTextReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 748
    or-int/lit8 v2, v2, 0x8

    .line 749
    const-string v3, "textReply"

    const v6, 0x32000

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v14, v6, v7, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    .end local v14    # "text":Ljava/lang/String;
    :cond_3
    const-string v3, "htmlReply"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 755
    const-string v3, "htmlReply"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 756
    .restart local v14    # "text":Ljava/lang/String;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const v6, 0x32000

    if-le v3, v6, :cond_4

    move-object v9, p0

    move-wide v10, v4

    move-wide/from16 v12, p1

    .line 757
    invoke-static/range {v9 .. v14}, Lcom/android/emailcommon/utility/BodyUtilites;->writeHtmlReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 758
    or-int/lit8 v2, v2, 0x4

    .line 759
    const-string v3, "htmlReply"

    const v6, 0x32000

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    invoke-static {v14, v6, v7, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    .end local v14    # "text":Ljava/lang/String;
    :cond_4
    const-string v3, "introText"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 765
    const-string v3, "introText"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 766
    .restart local v14    # "text":Ljava/lang/String;
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v3

    const v6, 0x32000

    if-le v3, v6, :cond_5

    move-object v9, p0

    move-wide v10, v4

    move-wide/from16 v12, p1

    .line 767
    invoke-static/range {v9 .. v14}, Lcom/android/emailcommon/utility/BodyUtilites;->writeIntroToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 768
    or-int/lit8 v2, v2, 0x10

    .line 769
    const-string v3, "introText"

    const v6, 0x32000

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v14, v6, v7, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 774
    .end local v14    # "text":Ljava/lang/String;
    :cond_5
    if-lez v2, :cond_6

    .line 775
    const-string v3, "fileSaveFlags"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 778
    .end local v2    # "flags":I
    .end local v4    # "accountId":J
    .end local v8    # "htmlText":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method public static updateBodyWithMessageId(Landroid/content/Context;JLandroid/content/ContentValues;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J
    .param p3, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v6, 0x0

    .line 788
    invoke-static {p0, p1, p2, p3}, Lcom/android/emailcommon/provider/EmailContent$Body;->saveBodyToFilesIfNecessary(Landroid/content/Context;JLandroid/content/ContentValues;)V

    .line 789
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 790
    .local v2, "resolver":Landroid/content/ContentResolver;
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Body;->lookupBodyIdWithMessageId(Landroid/content/Context;J)J

    move-result-wide v0

    .line 791
    .local v0, "bodyId":J
    const-string v4, "messageKey"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 792
    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    .line 793
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4, p3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 798
    :goto_0
    return-void

    .line 795
    :cond_0
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 796
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v3, p3, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static updateTruncatedFlags(Landroid/content/Context;JJI)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "flags"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 950
    const-string v2, "EmailContent >>"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTruncatedFlags start accountId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " messageId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " flags = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 952
    const-string v2, "EmailContent >>"

    const-string v3, "updateTruncatedFlags: Saving message body to file is not supported inside container. Skip it."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :goto_0
    return-void

    .line 955
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 956
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "fileSaveFlags"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 957
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "messageKey = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 960
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 961
    .local v0, "acnt":Lcom/android/emailcommon/provider/EmailContent$Account;
    new-instance v1, Landroid/content/ContentValues;

    .end local v1    # "cv":Landroid/content/ContentValues;
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 962
    .restart local v1    # "cv":Landroid/content/ContentValues;
    if-eqz v0, :cond_2

    .line 963
    invoke-virtual {v0, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->isEasAccount(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 964
    const-string v2, "EmailContent >>"

    const-string v3, "updateTruncatedFlags : set ISTRUNCATED to YES"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 965
    const-string v2, "istruncated"

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 976
    :goto_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, p3, p4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 977
    const-string v2, "EmailContent >>"

    const-string v3, "updateTruncatedFlags finish"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 967
    :cond_1
    const-string v2, "EmailContent >>"

    const-string v3, "updateTruncatedFlags : set FLAG_LOADED to PARTIAL"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    const-string v2, "flagLoaded"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 971
    :cond_2
    const-string v2, "EmailContent >>"

    const-string v3, "updateTruncatedFlags : cannot restore account, set both flags"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    const-string v2, "istruncated"

    invoke-static {v7}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 974
    const-string v2, "flagLoaded"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1
.end method


# virtual methods
.method public restore(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 938
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mBaseUri:Landroid/net/Uri;

    .line 939
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    .line 940
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 941
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 942
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    .line 943
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    .line 944
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mSourceKey:J

    .line 945
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    .line 946
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 947
    return-void
.end method

.method public saveBodyToFilesIfNecessary(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v7, 0x32000

    .line 685
    const-string v0, "EmailContent >>"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "saveBodyToFilesIfNecessary start : messageId = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    if-eqz p1, :cond_5

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v0

    if-nez v0, :cond_5

    .line 687
    iget-wide v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v2

    .line 688
    .local v2, "accountId":J
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v7, :cond_0

    .line 689
    const-string v0, "EmailContent >>"

    const-string v1, "Save HTML content"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget-object v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->writeHtmlContentToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 691
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v7, :cond_2

    .line 695
    const-string v0, "EmailContent >>"

    const-string v1, "Save text content"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget-object v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->writeTextContentToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 697
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 700
    :cond_2
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v7, :cond_3

    .line 701
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget-object v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->writeTextReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 702
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 705
    :cond_3
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v7, :cond_4

    .line 706
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget-object v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->writeHtmlReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 707
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 710
    :cond_4
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v7, :cond_5

    .line 711
    iget-wide v4, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    iget-object v6, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->writeIntroToFile(Landroid/content/Context;JJLjava/lang/String;)V

    .line 712
    iget v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 715
    .end local v2    # "accountId":J
    :cond_5
    const-string v0, "EmailContent >>"

    const-string v1, "saveBodyToFilesIfNecessary end"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    return-void
.end method

.method public toContentValues()Landroid/content/ContentValues;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x32000

    .line 533
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 536
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "messageKey"

    iget-wide v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 537
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 538
    const-string v1, "htmlContent"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3, v6}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :goto_0
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 544
    const-string v1, "textContent"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3, v5}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    :goto_1
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_2

    .line 550
    const-string v1, "htmlReply"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3, v6}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    :goto_2
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_3

    .line 556
    const-string v1, "textReply"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3, v5}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :goto_3
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_4

    .line 562
    const-string v1, "introText"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    invoke-static {}, Lcom/android/emailcommon/utility/BodyUtilites;->getCutText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v4, v3, v5}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    :goto_4
    const-string v1, "sourceMessageKey"

    iget-wide v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mSourceKey:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 568
    const-string v1, "fileSaveFlags"

    iget v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 569
    return-object v0

    .line 541
    :cond_0
    const-string v1, "htmlContent"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 547
    :cond_1
    const-string v1, "textContent"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 553
    :cond_2
    const-string v1, "htmlReply"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 559
    :cond_3
    const-string v1, "textReply"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 565
    :cond_4
    const-string v1, "introText"

    iget-object v2, p0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method
