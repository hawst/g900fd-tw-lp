.class public Lcom/android/emailcommon/LeakTestCursor;
.super Landroid/database/CursorWrapper;
.source "LeakTestCursor.java"


# static fields
.field public static ENABLE_LEAK_MONITOR:Z

.field private static sOpenedCursorList:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/emailcommon/LeakTestCursor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private callStack:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/emailcommon/LeakTestCursor;->sOpenedCursorList:Ljava/util/HashSet;

    .line 13
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/emailcommon/LeakTestCursor;->ENABLE_LEAK_MONITOR:Z

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 19
    sget-boolean v0, Lcom/android/emailcommon/LeakTestCursor;->ENABLE_LEAK_MONITOR:Z

    if-eqz v0, :cond_0

    .line 20
    invoke-direct {p0}, Lcom/android/emailcommon/LeakTestCursor;->CallStack()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/emailcommon/LeakTestCursor;->callStack:Ljava/lang/String;

    .line 21
    sget-object v0, Lcom/android/emailcommon/LeakTestCursor;->sOpenedCursorList:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 23
    :cond_0
    return-void
.end method

.method private CallStack()Ljava/lang/String;
    .locals 6

    .prologue
    .line 26
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 27
    .local v3, "stacktrace":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 28
    .local v2, "stacks":[Ljava/lang/StackTraceElement;
    const/4 v1, 0x7

    .line 29
    .local v1, "maxDepth":I
    array-length v4, v2

    if-ge v4, v1, :cond_0

    .line 30
    array-length v1, v2

    .line 31
    :cond_0
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 38
    sget-boolean v0, Lcom/android/emailcommon/LeakTestCursor;->ENABLE_LEAK_MONITOR:Z

    if-eqz v0, :cond_0

    .line 39
    sget-object v0, Lcom/android/emailcommon/LeakTestCursor;->sOpenedCursorList:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    return-void
.end method
