.class public Lcom/android/emailcommon/smime/CertificateManagerException;
.super Ljava/lang/Exception;
.source "CertificateManagerException.java"


# static fields
.field private static final serialVersionUID:J = 0x443612ead14a7a25L


# instance fields
.field public exception:Ljava/lang/Exception;

.field private mErrorCode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 17
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/emailcommon/smime/CertificateManagerException;->mErrorCode:I

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
    .param p2, "errorCode"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 22
    iput p2, p0, Lcom/android/emailcommon/smime/CertificateManagerException;->mErrorCode:I

    .line 23
    return-void
.end method
