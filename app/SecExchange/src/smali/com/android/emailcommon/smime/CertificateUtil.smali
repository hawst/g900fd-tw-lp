.class public Lcom/android/emailcommon/smime/CertificateUtil;
.super Ljava/lang/Object;
.source "CertificateUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/smime/CertificateUtil$1;,
        Lcom/android/emailcommon/smime/CertificateUtil$revocationInfo;,
        Lcom/android/emailcommon/smime/CertificateUtil$EncryptionAlgorithm;,
        Lcom/android/emailcommon/smime/CertificateUtil$AllowSMIMEEncryptionAlgorithmNegotiation;,
        Lcom/android/emailcommon/smime/CertificateUtil$RecipientCertificate;
    }
.end annotation


# static fields
.field static final CERTIFICATE_SEARCH_URI:Landroid/net/Uri;

.field static final CRL_CONTENT_URI:Landroid/net/Uri;

.field static CRL_DIR_PATH:Ljava/lang/String;

.field static EMAIL_DATABASE_PATH:Ljava/lang/String;

.field private static isOCSPEnabled:Z

.field private static isRevocationEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 56
    sput-object v0, Lcom/android/emailcommon/smime/CertificateUtil;->EMAIL_DATABASE_PATH:Ljava/lang/String;

    .line 57
    sput-object v0, Lcom/android/emailcommon/smime/CertificateUtil;->CRL_DIR_PATH:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/crlCache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/smime/CertificateUtil;->CRL_CONTENT_URI:Landroid/net/Uri;

    .line 63
    const-string v0, "content://com.android.email.directory.provider/certificate/*"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/smime/CertificateUtil;->CERTIFICATE_SEARCH_URI:Landroid/net/Uri;

    .line 713
    sput-boolean v2, Lcom/android/emailcommon/smime/CertificateUtil;->isRevocationEnabled:Z

    .line 714
    sput-boolean v2, Lcom/android/emailcommon/smime/CertificateUtil;->isOCSPEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 678
    return-void
.end method

.method public static convertToX509(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 7
    .param p0, "certificateStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateUtilExcpetion;
        }
    .end annotation

    .prologue
    .line 85
    const/4 v1, 0x0

    .line 87
    .local v1, "certificate":Ljava/security/cert/X509Certificate;
    :try_start_0
    const-string v4, "X.509"

    invoke-static {v4}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 89
    .local v0, "cert":Ljava/security/cert/CertificateFactory;
    const-string v4, "-----BEGIN CERTIFICATE-----"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-----BEGIN CERTIFICATE-----\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n-----END CERTIFICATE-----"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 93
    :cond_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 94
    .local v3, "is":Ljava/io/InputStream;
    invoke-virtual {v0, v3}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v1

    .end local v1    # "certificate":Ljava/security/cert/X509Certificate;
    check-cast v1, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    .restart local v1    # "certificate":Ljava/security/cert/X509Certificate;
    return-object v1

    .line 95
    .end local v0    # "cert":Ljava/security/cert/CertificateFactory;
    .end local v1    # "certificate":Ljava/security/cert/X509Certificate;
    .end local v3    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 96
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/android/emailcommon/smime/CertificateUtilExcpetion;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error while converting certificate. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/emailcommon/smime/CertificateUtilExcpetion;-><init>(Ljava/lang/String;)V

    throw v4
.end method
