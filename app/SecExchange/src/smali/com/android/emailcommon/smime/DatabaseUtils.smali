.class public Lcom/android/emailcommon/smime/DatabaseUtils;
.super Ljava/lang/Object;
.source "DatabaseUtils.java"


# static fields
.field private static final CONTENT_PROJECTION:[Ljava/lang/String;

.field private static CONTENT_URI:Landroid/net/Uri;

.field public static CRL_DIR_PATH:Ljava/lang/String;

.field public static DATABASE_PATH:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/android/emailcommon/smime/DatabaseUtils;->DATABASE_PATH:Ljava/lang/String;

    .line 28
    sput-object v0, Lcom/android/emailcommon/smime/DatabaseUtils;->CRL_DIR_PATH:Ljava/lang/String;

    .line 37
    sput-object v0, Lcom/android/emailcommon/smime/DatabaseUtils;->mContext:Landroid/content/Context;

    .line 47
    sput-object v0, Lcom/android/emailcommon/smime/DatabaseUtils;->CONTENT_URI:Landroid/net/Uri;

    .line 68
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dp"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "crlLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "nextUpdate"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "lastUsed"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "freshestCrl"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/smime/DatabaseUtils;->CONTENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
