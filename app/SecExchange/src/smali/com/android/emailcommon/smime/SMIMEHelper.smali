.class public Lcom/android/emailcommon/smime/SMIMEHelper;
.super Ljava/lang/Object;
.source "SMIMEHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/smime/SMIMEHelper$Message;,
        Lcom/android/emailcommon/smime/SMIMEHelper$Attachment;
    }
.end annotation


# static fields
.field public static final ENCRYPTION_ALGORITHMS:[Ljava/lang/String;

.field public static final SIGNING_ALGORITHMS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;

.field private static mProviderRegistered:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private SMIME_PROVIDER_NAME:Ljava/lang/String;

.field private final encryptLock:Ljava/util/concurrent/locks/Lock;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 56
    const-class v0, Lcom/android/emailcommon/smime/SMIMEHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    .line 74
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "-sha1"

    aput-object v1, v0, v2

    const-string v1, "-md5"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/emailcommon/smime/SMIMEHelper;->SIGNING_ALGORITHMS:[Ljava/lang/String;

    .line 94
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "-des3"

    aput-object v1, v0, v2

    const-string v1, "-des"

    aput-object v1, v0, v3

    const-string v1, "-rc2-128"

    aput-object v1, v0, v4

    const/4 v1, 0x3

    const-string v2, "-rc2-64"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "-rc2-40"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/smime/SMIMEHelper;->ENCRYPTION_ALGORITHMS:[Ljava/lang/String;

    .line 181
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/android/emailcommon/smime/SMIMEHelper;->mProviderRegistered:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    .line 62
    const-string v0, "AndroidOpenSSL"

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    .line 190
    iput-object p1, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    .line 191
    invoke-direct {p0}, Lcom/android/emailcommon/smime/SMIMEHelper;->registerProviderIfNecessary()V

    .line 192
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "providerName"    # Ljava/lang/String;

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    .line 62
    const-string v0, "AndroidOpenSSL"

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    .line 201
    iput-object p1, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    .line 202
    iput-object p2, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    .line 203
    invoke-direct {p0}, Lcom/android/emailcommon/smime/SMIMEHelper;->registerProviderIfNecessary()V

    .line 204
    return-void
.end method

.method private isProviderRegistered(Ljava/lang/String;)Z
    .locals 5
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 215
    sget-object v2, Lcom/android/emailcommon/smime/SMIMEHelper;->mProviderRegistered:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 216
    .local v1, "registeredProvider":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 217
    const/4 v2, 0x1

    .line 220
    .end local v1    # "registeredProvider":Ljava/lang/String;
    :goto_0
    return v2

    .line 219
    :cond_1
    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SMIMEHelper provider is not registered : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static varargs joinAddresses([[Lcom/android/emailcommon/mail/Address;)[Lcom/android/emailcommon/mail/Address;
    .locals 6
    .param p0, "addresses"    # [[Lcom/android/emailcommon/mail/Address;

    .prologue
    .line 244
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v4, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    if-eqz p0, :cond_1

    .line 247
    move-object v1, p0

    .local v1, "arr$":[[Lcom/android/emailcommon/mail/Address;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 248
    .local v0, "addressArray":[Lcom/android/emailcommon/mail/Address;
    if-eqz v0, :cond_0

    .line 249
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 247
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 252
    .end local v0    # "addressArray":[Lcom/android/emailcommon/mail/Address;
    .end local v1    # "arr$":[[Lcom/android/emailcommon/mail/Address;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    const/4 v5, 0x0

    new-array v5, v5, [Lcom/android/emailcommon/mail/Address;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/android/emailcommon/mail/Address;

    return-object v5
.end method

.method private registerProviderIfNecessary()V
    .locals 4

    .prologue
    .line 260
    iget-object v2, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/smime/SMIMEHelper;->isProviderRegistered(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    invoke-static {}, Ljava/security/Security;->getProviders()[Ljava/security/Provider;

    move-result-object v1

    .line 266
    .local v1, "providers":[Ljava/security/Provider;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_2

    .line 267
    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 268
    sget-object v2, Lcom/android/emailcommon/smime/SMIMEHelper;->mProviderRegistered:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 273
    :cond_2
    iget-object v2, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/smime/SMIMEHelper;->isProviderRegistered(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 274
    sget-object v2, Lcom/android/emailcommon/smime/SMIMEHelper;->mProviderRegistered:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->SMIME_PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 266
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private static writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 964
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 965
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 966
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 967
    invoke-static {p2}, Lcom/android/emailcommon/mail/Address;->packedToHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 968
    .local v0, "packedToHeader":Ljava/lang/String;
    if-eqz v0, :cond_1

    .end local v0    # "packedToHeader":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MimeUtility;->fold(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 970
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 972
    :cond_0
    return-void

    .line 968
    .restart local v0    # "packedToHeader":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private static writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 991
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 992
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 993
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 994
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {p2, v1}, Lcom/android/emailcommon/internet/MimeUtility;->foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 995
    .local v0, "foldedString":Ljava/lang/String;
    const-string v1, "\r\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 996
    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 997
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 999
    .end local v0    # "foldedString":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 975
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 976
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 977
    const-string v0, ": "

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 978
    const-string v0, "Content-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 979
    const-string v0, "<"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 981
    :cond_0
    invoke-virtual {p0, p2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 982
    const-string v0, "Content-ID"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 983
    const-string v0, ">"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 985
    :cond_1
    const-string v0, "\r\n"

    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 987
    :cond_2
    return-void
.end method

.method private writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/smime/SMIMEHelper$Message;)V
    .locals 7
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "message"    # Lcom/android/emailcommon/smime/SMIMEHelper$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 929
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 960
    :cond_0
    :goto_0
    return-void

    .line 933
    :cond_1
    const-string v2, "To"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mTo:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    const-string v2, "From"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    const-string v2, "Cc"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mCC:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    const-string v2, "Bcc"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mBCC:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    const-string v2, "Reply-To"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mReplyTo:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    const-string v2, "Subject"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mSubject:Ljava/lang/String;

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    const-string v2, "Message-ID"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mMessageID:Ljava/lang/String;

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 945
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mDate:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 947
    .local v0, "date":Ljava/lang/String;
    const-string v2, "Date"

    invoke-static {p1, v2, v0}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    iget-boolean v2, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mDelivery:Z

    if-eqz v2, :cond_2

    .line 950
    const-string v2, "Return-Receipt-To"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    :cond_2
    iget-boolean v2, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mRead:Z

    if-eqz v2, :cond_3

    .line 955
    const-string v2, "Disposition-Notification-To"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    :cond_3
    const-string v2, "Importance"

    iget-object v3, p2, Lcom/android/emailcommon/smime/SMIMEHelper$Message;->mImportance:Ljava/lang/String;

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public encryptMessage(Landroid/content/Context;Lcom/android/emailcommon/smime/SMIMEHelper$Message;ZLjava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;[Ljava/security/cert/X509Certificate;Ljava/io/OutputStream;Ljava/io/File;)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/smime/SMIMEHelper$Message;
    .param p3, "signMessage"    # Z
    .param p4, "encryptionAlgorithm"    # Ljava/lang/String;
    .param p5, "signingAlgorithm"    # Ljava/lang/String;
    .param p6, "privateKey"    # Ljava/security/PrivateKey;
    .param p7, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p8, "recipientCertificates"    # [Ljava/security/cert/X509Certificate;
    .param p9, "out"    # Ljava/io/OutputStream;
    .param p10, "mimeInputFile"    # Ljava/io/File;

    .prologue
    .line 354
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    const-string v6, "encryptMessage() start"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const/4 v13, 0x0

    .line 356
    .local v13, "encryptedTempFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 357
    const/4 v15, 0x0

    .line 358
    .local v15, "fileInputStrm":Ljava/io/FileInputStream;
    const/16 v17, 0x0

    .line 361
    .local v17, "fos":Ljava/io/FileOutputStream;
    if-nez p4, :cond_0

    .line 363
    :try_start_0
    const-string p4, "-des3"
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    :cond_0
    const/16 v22, 0x0

    .line 367
    .local v22, "res":Z
    if-eqz p3, :cond_b

    .line 368
    const/4 v5, 0x0

    .line 370
    .local v5, "signedTempFile":Ljava/io/File;
    :try_start_1
    const-string v4, "eas_signed"

    const-string v6, "tmp"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v8

    invoke-static {v4, v6, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    .line 373
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/security/cert/Certificate;

    const/4 v6, 0x0

    aput-object p7, v4, v6

    invoke-static {v4}, Landroid/security/Credentials;->convertToPem([Ljava/security/cert/Certificate;)[B

    move-result-object v7

    .line 374
    .local v7, "certBytes":[B
    const/4 v8, 0x0

    move-object/from16 v4, p10

    move-object/from16 v6, p6

    move-object/from16 v9, p5

    invoke-static/range {v4 .. v9}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->openSSLPKCS7Sign(Ljava/io/File;Ljava/io/File;Ljava/security/PrivateKey;[BLjava/util/List;Ljava/lang/String;)Z

    move-result v23

    .line 377
    .local v23, "resSign":Z
    if-eqz v23, :cond_2

    .line 379
    const-string v4, "eas_signed_encrypted"

    const-string v6, "tmp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v8

    invoke-static {v4, v6, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v13

    .line 381
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 382
    .local v21, "recipientCertsBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    move-object/from16 v11, p8

    .local v11, "arr$":[Ljava/security/cert/X509Certificate;
    array-length v0, v11

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    aget-object v25, v11, v18

    .line 383
    .local v25, "x":Ljava/security/cert/X509Certificate;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/security/cert/Certificate;

    const/4 v6, 0x0

    aput-object v25, v4, v6

    invoke-static {v4}, Landroid/security/Credentials;->convertToPem([Ljava/security/cert/Certificate;)[B

    move-result-object v10

    .line 384
    .local v10, "arr":[B
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 386
    .end local v10    # "arr":[B
    .end local v25    # "x":Ljava/security/cert/X509Certificate;
    :cond_1
    move-object/from16 v0, v21

    move-object/from16 v1, p4

    invoke-static {v5, v13, v0, v1}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->openSSLPKCS7encrypt(Ljava/io/File;Ljava/io/File;Ljava/util/List;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v22

    .line 394
    .end local v11    # "arr$":[Ljava/security/cert/X509Certificate;
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v21    # "recipientCertsBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    :cond_2
    if-eqz v5, :cond_3

    :try_start_2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 395
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 409
    .end local v5    # "signedTempFile":Ljava/io/File;
    .end local v7    # "certBytes":[B
    .end local v23    # "resSign":Z
    :cond_3
    :goto_1
    if-nez v22, :cond_d

    .line 410
    new-instance v4, Ljava/lang/Exception;

    const-string v6, "Native Encrypt/Sign Returned False."

    invoke-direct {v4, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 422
    .end local v22    # "res":Z
    :catch_0
    move-exception v20

    .line 423
    .local v20, "oe":Ljava/lang/OutOfMemoryError;
    :goto_2
    :try_start_3
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - Runtime.getRuntime().freeMemory() : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - Runtime.getRuntime().maxMemory() : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - Runtime.getRuntime().totalMemory() : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - getNativeHeapAllocatedSize : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - getNativeHeapFreeSize : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Debug;->getNativeHeapFreeSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "After - getNativeHeapSize : "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Debug;->getNativeHeapSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception caught in encryptMessage() :  "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 443
    throw v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 448
    .end local v20    # "oe":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v15, :cond_4

    .line 450
    :try_start_4
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8

    .line 456
    :cond_4
    :goto_4
    if-eqz v17, :cond_5

    .line 458
    :try_start_5
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9

    .line 465
    :cond_5
    :goto_5
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 466
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 468
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v4

    .line 389
    .restart local v5    # "signedTempFile":Ljava/io/File;
    .restart local v22    # "res":Z
    :catch_1
    move-exception v12

    .line 390
    .local v12, "e":Ljava/io/FileNotFoundException;
    :try_start_6
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 394
    if-eqz v5, :cond_3

    :try_start_7
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 395
    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 444
    .end local v5    # "signedTempFile":Ljava/io/File;
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    .end local v22    # "res":Z
    :catch_2
    move-exception v14

    .line 445
    .local v14, "ex":Ljava/lang/Exception;
    :goto_6
    :try_start_8
    sget-object v4, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception caught: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v14}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 448
    if-eqz v15, :cond_7

    .line 450
    :try_start_9
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 456
    :cond_7
    :goto_7
    if-eqz v17, :cond_8

    .line 458
    :try_start_a
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 465
    :cond_8
    :goto_8
    if-eqz v13, :cond_9

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 466
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 468
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 471
    .end local v14    # "ex":Ljava/lang/Exception;
    :goto_9
    return-void

    .line 391
    .restart local v5    # "signedTempFile":Ljava/io/File;
    .restart local v22    # "res":Z
    :catch_3
    move-exception v12

    .line 392
    .local v12, "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 394
    if-eqz v5, :cond_3

    :try_start_c
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 395
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto/16 :goto_1

    .line 394
    .end local v12    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v4

    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 395
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_a
    throw v4

    .line 399
    .end local v5    # "signedTempFile":Ljava/io/File;
    :cond_b
    const-string v4, "eas_encrypted"

    const-string v6, "tmp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v8

    invoke-static {v4, v6, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v13

    .line 401
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 402
    .restart local v21    # "recipientCertsBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    move-object/from16 v11, p8

    .restart local v11    # "arr$":[Ljava/security/cert/X509Certificate;
    array-length v0, v11

    move/from16 v19, v0

    .restart local v19    # "len$":I
    const/16 v18, 0x0

    .restart local v18    # "i$":I
    :goto_a
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_c

    aget-object v25, v11, v18

    .line 403
    .restart local v25    # "x":Ljava/security/cert/X509Certificate;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/security/cert/Certificate;

    const/4 v6, 0x0

    aput-object v25, v4, v6

    invoke-static {v4}, Landroid/security/Credentials;->convertToPem([Ljava/security/cert/Certificate;)[B

    move-result-object v10

    .line 404
    .restart local v10    # "arr":[B
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    add-int/lit8 v18, v18, 0x1

    goto :goto_a

    .line 406
    .end local v10    # "arr":[B
    .end local v25    # "x":Ljava/security/cert/X509Certificate;
    :cond_c
    move-object/from16 v0, p10

    move-object/from16 v1, v21

    move-object/from16 v2, p4

    invoke-static {v0, v13, v1, v2}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->openSSLPKCS7encrypt(Ljava/io/File;Ljava/io/File;Ljava/util/List;Ljava/lang/String;)Z

    move-result v22

    goto/16 :goto_1

    .line 413
    .end local v11    # "arr$":[Ljava/security/cert/X509Certificate;
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    .end local v21    # "recipientCertsBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    :cond_d
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 414
    .end local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    .local v16, "fileInputStrm":Ljava/io/FileInputStream;
    :try_start_d
    new-instance v24, Ljava/io/OutputStreamWriter;

    move-object/from16 v0, v24

    move-object/from16 v1, p9

    invoke-direct {v0, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 416
    .local v24, "writer":Ljava/io/Writer;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/smime/SMIMEHelper$Message;)V

    .line 417
    invoke-virtual/range {v24 .. v24}, Ljava/io/Writer;->flush()V

    .line 419
    new-instance v4, Ljava/io/BufferedInputStream;

    const v6, 0x8000

    move-object/from16 v0, v16

    invoke-direct {v4, v0, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p9

    invoke-static {v4, v0}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_b
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 448
    if-eqz v16, :cond_e

    .line 450
    :try_start_e
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4

    .line 456
    :cond_e
    :goto_b
    if-eqz v17, :cond_f

    .line 458
    :try_start_f
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_5

    .line 465
    :cond_f
    :goto_c
    if-eqz v13, :cond_10

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 466
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 468
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-object/from16 v15, v16

    .line 470
    .end local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 451
    .end local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_4
    move-exception v12

    .line 452
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_b

    .line 459
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v12

    .line 460
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_c

    .line 451
    .end local v12    # "e":Ljava/io/IOException;
    .end local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v22    # "res":Z
    .end local v24    # "writer":Ljava/io/Writer;
    .restart local v14    # "ex":Ljava/lang/Exception;
    .restart local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_6
    move-exception v12

    .line 452
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_7

    .line 459
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v12

    .line 460
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_8

    .line 451
    .end local v12    # "e":Ljava/io/IOException;
    .end local v14    # "ex":Ljava/lang/Exception;
    :catch_8
    move-exception v12

    .line 452
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 453
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_4

    .line 459
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v12

    .line 460
    .local v12, "e":Ljava/io/IOException;
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    .line 461
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_5

    .line 448
    .end local v12    # "e":Ljava/io/IOException;
    .end local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v22    # "res":Z
    :catchall_2
    move-exception v4

    move-object/from16 v15, v16

    .end local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .line 444
    .end local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_a
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .line 422
    .end local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_b
    move-exception v20

    move-object/from16 v15, v16

    .end local v16    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v15    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method

.method public signMessage(Landroid/content/Context;Lcom/android/emailcommon/smime/SMIMEHelper$Message;Ljava/security/PrivateKey;Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/io/OutputStream;Ljava/io/File;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/smime/SMIMEHelper$Message;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p5, "signingAlgorithm"    # Ljava/lang/String;
    .param p6, "out"    # Ljava/io/OutputStream;
    .param p7, "inputFile"    # Ljava/io/File;

    .prologue
    .line 285
    sget-object v3, Lcom/android/emailcommon/smime/SMIMEHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "signMessage: signingAlgorithm:  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const/4 v10, 0x0

    .line 288
    .local v10, "fileInputStrm":Ljava/io/FileInputStream;
    const/4 v2, 0x0

    .line 289
    .local v2, "signedTempFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 290
    .local v1, "srcMimeTempFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 293
    .local v7, "bos":Ljava/io/BufferedOutputStream;
    :try_start_0
    const-string v3, "eas_signed"

    const-string v5, "tmp"

    iget-object v6, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-static {v3, v5, v6}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 294
    const-string v3, "eas_src"

    const-string v5, "tmp"

    iget-object v6, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    invoke-static {v3, v5, v6}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    .line 296
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/security/cert/Certificate;

    const/4 v5, 0x0

    aput-object p4, v3, v5

    invoke-static {v3}, Landroid/security/Credentials;->convertToPem([Ljava/security/cert/Certificate;)[B

    move-result-object v4

    .line 298
    .local v4, "certBytes":[B
    new-instance v11, Ljava/io/FileInputStream;

    move-object/from16 v0, p7

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .local v11, "fileInputStrm":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v12, Lcom/android/emailcommon/internet/MimeMessage;

    new-instance v3, Ljava/io/BufferedInputStream;

    const v5, 0x8000

    invoke-direct {v3, v11, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v12, v3}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V

    .line 301
    .local v12, "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    new-instance v8, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const v5, 0x8000

    invoke-direct {v8, v3, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1d
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_1a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_17
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 302
    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .local v8, "bos":Ljava/io/BufferedOutputStream;
    :try_start_2
    invoke-virtual {v12, v8}, Lcom/android/emailcommon/internet/MimeMessage;->writeTo(Ljava/io/OutputStream;)V

    .line 303
    invoke-virtual {v8}, Ljava/io/BufferedOutputStream;->close()V

    .line 305
    const/4 v5, 0x0

    move-object/from16 v3, p3

    move-object/from16 v6, p5

    invoke-static/range {v1 .. v6}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->openSSLPKCS7Sign(Ljava/io/File;Ljava/io/File;Ljava/security/PrivateKey;[BLjava/util/List;Ljava/lang/String;)Z

    .line 308
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    .line 310
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1e
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_1b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_18
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 311
    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v13, Ljava/io/OutputStreamWriter;

    move-object/from16 v0, p6

    invoke-direct {v13, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 312
    .local v13, "writer":Ljava/io/Writer;
    move-object/from16 v0, p2

    invoke-direct {p0, v13, v0}, Lcom/android/emailcommon/smime/SMIMEHelper;->writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/smime/SMIMEHelper$Message;)V

    .line 313
    invoke-virtual {v13}, Ljava/io/Writer;->flush()V

    .line 315
    new-instance v3, Ljava/io/BufferedInputStream;

    const v5, 0x8000

    invoke-direct {v3, v10, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p6

    invoke-static {v3, v0}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1f
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3 .. :try_end_3} :catch_1c
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_19
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_16
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_13
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 332
    if-eqz v10, :cond_0

    .line 333
    :try_start_4
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 338
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_2
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    move-object v7, v8

    .line 344
    .end local v4    # "certBytes":[B
    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .end local v13    # "writer":Ljava/io/Writer;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    :goto_1
    return-void

    .line 334
    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v4    # "certBytes":[B
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v13    # "writer":Ljava/io/Writer;
    :catch_0
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 318
    .end local v4    # "certBytes":[B
    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .end local v13    # "writer":Ljava/io/Writer;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    :catch_1
    move-exception v9

    .line 319
    .local v9, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 332
    if-eqz v10, :cond_3

    .line 333
    :try_start_6
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 338
    .end local v9    # "e":Ljava/io/FileNotFoundException;
    :cond_3
    :goto_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_5
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto :goto_1

    .line 334
    .restart local v9    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 320
    .end local v9    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v9

    .line 321
    .local v9, "e":Ljava/security/cert/CertificateEncodingException;
    :goto_4
    :try_start_7
    invoke-virtual {v9}, Ljava/security/cert/CertificateEncodingException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 332
    if-eqz v10, :cond_6

    .line 333
    :try_start_8
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 338
    .end local v9    # "e":Ljava/security/cert/CertificateEncodingException;
    :cond_6
    :goto_5
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_8
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto :goto_1

    .line 334
    .restart local v9    # "e":Ljava/security/cert/CertificateEncodingException;
    :catch_4
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 322
    .end local v9    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v9

    .line 323
    .restart local v9    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_9
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 332
    if-eqz v10, :cond_9

    .line 333
    :try_start_a
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 338
    :cond_9
    :goto_7
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_a
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_b
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto :goto_1

    .line 334
    :catch_6
    move-exception v9

    .line 335
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 324
    .end local v9    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v9

    .line 325
    .local v9, "e":Ljava/security/InvalidKeyException;
    :goto_8
    :try_start_b
    invoke-virtual {v9}, Ljava/security/InvalidKeyException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 332
    if-eqz v10, :cond_c

    .line 333
    :try_start_c
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 338
    .end local v9    # "e":Ljava/security/InvalidKeyException;
    :cond_c
    :goto_9
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_d
    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_e
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto/16 :goto_1

    .line 334
    .restart local v9    # "e":Ljava/security/InvalidKeyException;
    :catch_8
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 326
    .end local v9    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v9

    .line 327
    .local v9, "e":Ljava/lang/NullPointerException;
    :goto_a
    :try_start_d
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 332
    if-eqz v10, :cond_f

    .line 333
    :try_start_e
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 338
    .end local v9    # "e":Ljava/lang/NullPointerException;
    :cond_f
    :goto_b
    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_10
    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_11
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto/16 :goto_1

    .line 334
    .restart local v9    # "e":Ljava/lang/NullPointerException;
    :catch_a
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 328
    .end local v9    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v9

    .line 329
    .local v9, "e":Ljava/lang/Exception;
    :goto_c
    :try_start_f
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 332
    if-eqz v10, :cond_12

    .line 333
    :try_start_10
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c

    .line 338
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_12
    :goto_d
    if-eqz v2, :cond_13

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_13
    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_14

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_14
    iget-object v3, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v3}, Ljava/util/concurrent/locks/Lock;->lock()V

    goto/16 :goto_1

    .line 334
    .restart local v9    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v9

    .line 335
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 331
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 332
    :goto_e
    if-eqz v10, :cond_15

    .line 333
    :try_start_11
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d

    .line 338
    :cond_15
    :goto_f
    if-eqz v2, :cond_16

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 339
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 340
    :cond_16
    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 341
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 342
    :cond_17
    iget-object v5, p0, Lcom/android/emailcommon/smime/SMIMEHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    throw v3

    .line 334
    :catch_d
    move-exception v9

    .line 335
    .restart local v9    # "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 331
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v4    # "certBytes":[B
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v3

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_e

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catchall_2
    move-exception v3

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_e

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catchall_3
    move-exception v3

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_e

    .line 328
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_e
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_c

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_f
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_c

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_10
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto :goto_c

    .line 326
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_11
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_a

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_12
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_a

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_13
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_a

    .line 324
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_14
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_15
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_16
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_8

    .line 322
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_17
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_18
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_6

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_19
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_6

    .line 320
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_1a
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_1b
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_4

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_1c
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_4

    .line 318
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_1d
    move-exception v9

    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_1e
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    move-object v10, v11

    .end local v11    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .end local v7    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v8    # "bos":Ljava/io/BufferedOutputStream;
    :catch_1f
    move-exception v9

    move-object v7, v8

    .end local v8    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "bos":Ljava/io/BufferedOutputStream;
    goto/16 :goto_2
.end method
