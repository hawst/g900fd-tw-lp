.class public Lcom/android/emailcommon/smime/CertificateMgr;
.super Ljava/lang/Object;
.source "CertificateMgr.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static mIsSavingKeyStore:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mKeyStore:Ljava/security/KeyStore;

.field private mKeyStoreType:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field pkc11KestoreAliases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/android/emailcommon/smime/CertificateMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    .line 87
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/emailcommon/smime/CertificateMgr;->mIsSavingKeyStore:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateManagerException;
        }
    .end annotation

    .prologue
    .line 271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/smime/CertificateMgr;->pkc11KestoreAliases:Ljava/util/ArrayList;

    .line 273
    const-string v0, "AndroidKeyStore"

    iput-object v0, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStoreType:Ljava/lang/String;

    .line 274
    iput-object p2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mContext:Landroid/content/Context;

    .line 275
    if-nez p1, :cond_0

    .line 276
    new-instance v0, Lcom/android/emailcommon/smime/CertificateManagerException;

    const-string v1, "CertificateMgr is unable to intialize without password"

    invoke-direct {v0, v1}, Lcom/android/emailcommon/smime/CertificateManagerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_0
    iput-object p1, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mPassword:Ljava/lang/String;

    .line 278
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "keyStoreName"    # Ljava/lang/String;
    .param p4, "provider"    # Ljava/lang/String;
    .param p5, "credentialAccount"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateManagerException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->pkc11KestoreAliases:Ljava/util/ArrayList;

    .line 287
    :try_start_0
    iput-object p2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mContext:Landroid/content/Context;

    .line 288
    iput-object p1, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mPassword:Ljava/lang/String;

    .line 289
    sget-object v4, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Is CAC enabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    if-nez p5, :cond_0

    .line 292
    sget-object v4, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    const-string v5, "Loading Tima KeyStore and CAC is not enabled"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v4, "TimaKeyStore"

    invoke-static {v4}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v3

    .line 294
    .local v3, "timaKeystore":Ljava/security/KeyStore;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 296
    .end local v3    # "timaKeystore":Ljava/security/KeyStore;
    :cond_0
    invoke-static {p3, p4}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v4

    iput-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    .line 297
    sget-object v4, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    const-string v5, "Initializing KeyStore"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iput-object p3, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStoreType:Ljava/lang/String;

    .line 300
    invoke-static {}, Lcom/android/emailcommon/smime/CertificateMgr;->waitingKeyOp()V

    .line 301
    iget-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 302
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->pkc11KestoreAliases:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 324
    :try_start_1
    iget-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v4}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    .line 325
    .local v1, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-nez v4, :cond_1

    .line 326
    sget-object v4, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    const-string v5, "Empty Keystore!!!"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 329
    iget-object v4, p0, Lcom/android/emailcommon/smime/CertificateMgr;->pkc11KestoreAliases:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 331
    .end local v1    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 335
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    return-void

    .line 305
    :catch_1
    move-exception v0

    .line 306
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 307
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 308
    .local v2, "t":Ljava/lang/Throwable;
    if-eqz v2, :cond_3

    instance-of v4, v2, Ljava/security/UnrecoverableKeyException;

    if-eqz v4, :cond_3

    .line 310
    new-instance v4, Lcom/android/emailcommon/smime/CertificateManagerException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " may be wrong pw"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-direct {v4, v5, v6}, Lcom/android/emailcommon/smime/CertificateManagerException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 314
    :cond_3
    new-instance v4, Lcom/android/emailcommon/smime/CertificateManagerException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " may be pw input canceled"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lcom/android/emailcommon/smime/CertificateManagerException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 318
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "t":Ljava/lang/Throwable;
    :catch_2
    move-exception v0

    .line 319
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    const-string v5, " error while loading certificate"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    new-instance v4, Lcom/android/emailcommon/smime/CertificateManagerException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/emailcommon/smime/CertificateManagerException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static waitingKeyOp()V
    .locals 4

    .prologue
    .line 522
    :goto_0
    :try_start_0
    sget-boolean v1, Lcom/android/emailcommon/smime/CertificateMgr;->mIsSavingKeyStore:Z

    if-eqz v1, :cond_0

    .line 523
    const-wide/16 v2, 0x64

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 525
    :catch_0
    move-exception v0

    .line 526
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 528
    :cond_0
    return-void
.end method


# virtual methods
.method public getAliases()Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateManagerException;
        }
    .end annotation

    .prologue
    .line 567
    :try_start_0
    iget-object v1, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    if-eqz v1, :cond_0

    .line 568
    iget-object v1, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v1}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 573
    :goto_0
    return-object v1

    .line 570
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/android/emailcommon/smime/CertificateManagerException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/emailcommon/smime/CertificateManagerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 573
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCertificate(Ljava/lang/String;)Ljava/security/cert/X509Certificate;
    .locals 8
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 583
    const/4 v1, 0x0

    .line 585
    .local v1, "cert":Ljava/security/cert/X509Certificate;
    :try_start_0
    iget-object v6, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStoreType:Ljava/lang/String;

    const-string v7, "AndroidKeyStore"

    if-ne v6, v7, :cond_0

    .line 586
    const-string v6, "PKCS12"

    new-instance v7, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v7}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v6, v7}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object v5

    .line 587
    .local v5, "tempKeyStore":Ljava/security/KeyStore;
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mPassword:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 588
    iget-object v6, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mContext:Landroid/content/Context;

    invoke-static {v6, p1}, Landroid/security/KeyChain;->getCertificateChain(Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;

    move-result-object v2

    .line 589
    .local v2, "certs":[Ljava/security/cert/X509Certificate;
    iget-object v6, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mContext:Landroid/content/Context;

    invoke-static {v6, p1}, Landroid/security/KeyChain;->getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v4

    .line 590
    .local v4, "key":Ljava/security/PrivateKey;
    iget-object v6, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mPassword:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-virtual {v5, p1, v4, v6, v2}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    .line 591
    invoke-virtual {v5, p1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v1, v0

    .line 599
    .end local v2    # "certs":[Ljava/security/cert/X509Certificate;
    .end local v4    # "key":Ljava/security/PrivateKey;
    .end local v5    # "tempKeyStore":Ljava/security/KeyStore;
    :goto_0
    return-object v1

    .line 593
    :cond_0
    iget-object v6, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    invoke-virtual {v6, p1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 595
    :catch_0
    move-exception v3

    .line 596
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/Key;
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 499
    const/4 v1, 0x0

    .line 502
    .local v1, "key":Ljava/security/Key;
    :try_start_0
    iget-object v2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStoreType:Ljava/lang/String;

    const-string v3, "AndroidKeyStore"

    if-ne v2, v3, :cond_1

    .line 503
    iget-object v2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Landroid/security/KeyChain;->getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v1

    .line 517
    :cond_0
    :goto_0
    return-object v1

    .line 505
    :cond_1
    iget-object v2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    if-eqz v2, :cond_0

    .line 506
    iget-object v2, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mKeyStore:Ljava/security/KeyStore;

    iget-object v3, p0, Lcom/android/emailcommon/smime/CertificateMgr;->mPassword:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    .line 507
    instance-of v2, v1, Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    .line 508
    const/4 v1, 0x0

    goto :goto_0

    .line 512
    :catch_0
    move-exception v0

    .line 513
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    const-string v3, "getPrivateKey: exception"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    sget-object v2, Lcom/android/emailcommon/smime/CertificateMgr;->TAG:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
