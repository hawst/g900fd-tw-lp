.class public Lcom/android/sdp/SdpManager;
.super Ljava/lang/Object;
.source "SdpManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/sdp/SdpManager$SensitiveDBPolicy;
    }
.end annotation


# static fields
.field private static _instance:Lcom/android/sdp/SdpManager;


# instance fields
.field private mPm:Landroid/os/PersonaManager;

.field mService:Landroid/service/sdp/ISdpManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/android/sdp/SdpManager;->_instance:Lcom/android/sdp/SdpManager;

    .line 77
    const-string v0, "/system/lib/libsdp_sdk.so"

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/android/sdp/SdpManager;->mPm:Landroid/os/PersonaManager;

    .line 52
    iput-object v0, p0, Lcom/android/sdp/SdpManager;->mService:Landroid/service/sdp/ISdpManagerService;

    .line 74
    return-void
.end method

.method public static native Native_Sdp_SetSensitiveApp(I)I
.end method

.method public static native Native_Sdp_SetSensitiveFile(ILjava/lang/String;)I
.end method

.method public static getSdpManager()Lcom/android/sdp/SdpManager;
    .locals 1

    .prologue
    .line 549
    sget-object v0, Lcom/android/sdp/SdpManager;->_instance:Lcom/android/sdp/SdpManager;

    if-nez v0, :cond_0

    .line 550
    new-instance v0, Lcom/android/sdp/SdpManager;

    invoke-direct {v0}, Lcom/android/sdp/SdpManager;-><init>()V

    sput-object v0, Lcom/android/sdp/SdpManager;->_instance:Lcom/android/sdp/SdpManager;

    .line 552
    :cond_0
    sget-object v0, Lcom/android/sdp/SdpManager;->_instance:Lcom/android/sdp/SdpManager;

    return-object v0
.end method

.method private getSdpService()Landroid/service/sdp/ISdpManagerService;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcom/android/sdp/SdpManager;->mService:Landroid/service/sdp/ISdpManagerService;

    if-nez v0, :cond_0

    .line 540
    const-string v0, "sdp"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/service/sdp/ISdpManagerService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/sdp/ISdpManagerService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/sdp/SdpManager;->mService:Landroid/service/sdp/ISdpManagerService;

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/android/sdp/SdpManager;->mService:Landroid/service/sdp/ISdpManagerService;

    return-object v0
.end method


# virtual methods
.method public SDP_GetState()I
    .locals 5

    .prologue
    .line 94
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 95
    .local v1, "uid":I
    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 97
    .local v2, "userId":I
    :try_start_0
    invoke-direct {p0}, Lcom/android/sdp/SdpManager;->getSdpService()Landroid/service/sdp/ISdpManagerService;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/service/sdp/ISdpManagerService;->getState(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 102
    :goto_0
    return v3

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SDP.SDK"

    const-string v4, "Error- Exception in get SDP state"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const-string v3, "SDP.SDK"

    const-string v4, "Failed to get SDP state!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public SDP_SetSensitiveApp(I)Z
    .locals 7
    .param p1, "pid"    # I

    .prologue
    .line 516
    const/4 v1, 0x0

    .line 517
    .local v1, "res":Z
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "To handle SDP_SetSensitiveApp : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 519
    .local v2, "uid":I
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 520
    .local v3, "userId":I
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "given app info: uid -"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-userid-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :try_start_0
    invoke-direct {p0}, Lcom/android/sdp/SdpManager;->getSdpService()Landroid/service/sdp/ISdpManagerService;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/service/sdp/ISdpManagerService;->isSDPEnabled(I)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 524
    invoke-static {p1}, Lcom/android/sdp/SdpManager;->Native_Sdp_SetSensitiveApp(I)I

    move-result v4

    if-nez v4, :cond_0

    .line 525
    const-string v4, "SDP.SDK"

    const-string v5, "Error to handle SDP_SetSensitiveApp"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :goto_0
    return v1

    .line 527
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 530
    :cond_1
    const-string v4, "SDP.SDK"

    const-string v5, "SDPEnabled false"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SDP.SDK"

    const-string v5, "Error- Exception in setting Policy"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public SDP_SetSensitiveFile(Ljava/lang/String;)Z
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "res":Z
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "$$To handle SDP_SetSensitiveFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 109
    .local v2, "uid":I
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 110
    .local v3, "userId":I
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "given app info: uid -"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-userid-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :try_start_0
    invoke-direct {p0}, Lcom/android/sdp/SdpManager;->getSdpService()Landroid/service/sdp/ISdpManagerService;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/service/sdp/ISdpManagerService;->isSDPEnabled(I)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 114
    invoke-static {v3, p1}, Lcom/android/sdp/SdpManager;->Native_Sdp_SetSensitiveFile(ILjava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 115
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error to handle SDP_SetSensitiveFile : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :goto_0
    return v1

    .line 117
    :cond_0
    const/4 v1, 0x1

    .line 119
    invoke-direct {p0}, Lcom/android/sdp/SdpManager;->getSdpService()Landroid/service/sdp/ISdpManagerService;

    move-result-object v4

    invoke-interface {v4, v3, v2, p1}, Landroid/service/sdp/ISdpManagerService;->setSensitiveFileInfo(IILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SDP.SDK"

    const-string v5, "Error- Exception in setting Policy"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    const-string v4, "SDP.SDK"

    const-string v5, "SDPEnabled false"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public updateSDPStateToDB(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "dbalias"    # Ljava/lang/String;
    .param p3, "sdpState"    # I

    .prologue
    .line 431
    :try_start_0
    const-string v2, "SDP.SDK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateSDPStateToDB called with dbalias = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " sdpState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    if-nez p2, :cond_0

    const-string v0, ""

    .line 433
    .local v0, "dbaliasprefix":Ljava/lang/String;
    :goto_0
    packed-switch p3, :pswitch_data_0

    .line 441
    :goto_1
    const/4 v2, 0x1

    .line 446
    .end local v0    # "dbaliasprefix":Ljava/lang/String;
    :goto_2
    return v2

    .line 432
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 435
    .restart local v0    # "dbaliasprefix":Ljava/lang/String;
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pragma "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sdp_locked;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 442
    .end local v0    # "dbaliasprefix":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 443
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 446
    const/4 v2, 0x0

    goto :goto_2

    .line 438
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "dbaliasprefix":Ljava/lang/String;
    :pswitch_1
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pragma "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sdp_unlocked;"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateSDPStateToDB(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "dbPath"    # Ljava/lang/String;
    .param p2, "sdpState"    # I

    .prologue
    const/4 v3, 0x0

    .line 475
    :try_start_0
    const-string v4, "SDP.SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateSDPStateToDB called with dbPath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sdpState = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 477
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4, p2}, Lcom/android/sdp/SdpManager;->updateSDPStateToDB(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)Z

    move-result v2

    .line 478
    .local v2, "ret":Z
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v2    # "ret":Z
    :goto_0
    return v2

    .line 480
    :catch_0
    move-exception v1

    .line 481
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v2, v3

    .line 484
    goto :goto_0
.end method
