.class public Lcom/android/exchange/EasDownLoadAttachmentSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasDownLoadAttachmentSvc.java"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field private SPLIT:Ljava/lang/String;

.field public mAccountKey:J

.field private mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

.field private mAttInfoLog:Ljava/lang/String;

.field private mDownloadTime:F

.field private mIsAttachmentSizeConflict:Z

.field private mIsCancelledByUser:Z

.field private mIsDeadService:Z

.field private mLastProgress:I

.field private mLastSavedLength:J

.field private mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

.field private mRequest:Lcom/android/exchange/PartRequest;

.field private mResponseTime:F

.field private mSavedLength:J

.field private mSessionRecoveryOutputStream:Ljava/io/OutputStream;

.field private mStartTime:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/PartRequest;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p3, "req"    # Lcom/android/exchange/PartRequest;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 115
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 85
    iput-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 86
    iput-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 91
    const-string v0, "/"

    iput-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->SPLIT:Ljava/lang/String;

    .line 93
    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    .line 94
    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastProgress:I

    .line 100
    iput-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    .line 102
    iput-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAttInfoLog:Ljava/lang/String;

    .line 104
    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    .line 105
    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsDeadService:Z

    .line 107
    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsAttachmentSizeConflict:Z

    .line 109
    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    .line 110
    iput v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mResponseTime:F

    .line 111
    iput v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDownloadTime:F

    .line 112
    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccountKey:J

    .line 116
    iput-object p3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    .line 117
    iget-wide v0, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iput-wide v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccountKey:J

    .line 118
    return-void
.end method

.method private checkAttachmentPolicy()Z
    .locals 9

    .prologue
    const/16 v8, 0x17

    const/4 v1, 0x0

    .line 940
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    if-nez v4, :cond_0

    .line 970
    :goto_0
    return v1

    .line 943
    :cond_0
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v0

    .line 945
    .local v0, "accountPolicies":Lcom/android/emailcommon/service/PolicySet;
    if-eqz v0, :cond_2

    .line 946
    iget-boolean v4, v0, Lcom/android/emailcommon/service/PolicySet;->mAttachmentsEnabled:Z

    if-nez v4, :cond_1

    .line 947
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "info=IT SecurityPolicy: Attachments disabled mAccount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " attachmentId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 950
    const-string v4, "EasDownLoadAttachmentSvc"

    const-string v5, "IT SecurityPolicy: Attachments disabled"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    invoke-virtual {p0, v8}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto :goto_0

    .line 956
    :cond_1
    iget v4, v0, Lcom/android/emailcommon/service/PolicySet;->mMaxAttachmentSize:I

    int-to-long v2, v4

    .line 958
    .local v2, "maxAllowedSize":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    cmp-long v4, v4, v2

    if-lez v4, :cond_2

    .line 959
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "info=IT SecurityPolicy: Attachment Maxsize exceeded. actual size="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", maxAllowedSize="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAccount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " attachmentId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 963
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IT SecurityPolicy: Attachment Maxsize exceeded. request:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", max="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    invoke-virtual {p0, v8}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto/16 :goto_0

    .line 970
    .end local v2    # "maxAllowedSize":J
    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method private doProgressCallback(I)V
    .locals 8
    .param p1, "progress"    # I

    .prologue
    .line 768
    iget-boolean v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsDeadService:Z

    if-eqz v0, :cond_1

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    iget-boolean v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-eqz v0, :cond_2

    .line 773
    const/high16 v0, 0x130000

    invoke-virtual {p0, v0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto :goto_0

    .line 779
    :cond_2
    iget v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastProgress:I

    if-eq v0, p1, :cond_0

    .line 782
    iput p1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastProgress:I

    .line 785
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/4 v6, 0x1

    move v7, p1

    invoke-interface/range {v1 .. v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 787
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getAttInfo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 984
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAttInfoLog:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 985
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " accId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " msgId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " attachmentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FileName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAttInfoLog:Ljava/lang/String;

    .line 988
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAttInfoLog:Ljava/lang/String;

    return-object v0
.end method

.method private getAttachmentDownloadClient(I)Lorg/apache/http/client/HttpClient;
    .locals 7
    .param p1, "timeout"    # I

    .prologue
    .line 228
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 229
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 230
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 231
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 232
    sget v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 234
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 236
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 237
    .local v2, "proxyHost":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 238
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 239
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 240
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getClientConnectionManagerForAttachmentDownloads()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 246
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v4, Lcom/android/exchange/EasDownLoadAttachmentSvc$1;

    invoke-direct {v4, p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc$1;-><init>(Lcom/android/exchange/EasDownLoadAttachmentSvc;)V

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    .line 254
    invoke-static {}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getDefaultRedirectHandler()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 255
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getClientConnectionManagerForAttachment Downloads : client : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return-object v0
.end method

.method private getClientConnectionManagerForAttachmentDownloads()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 2

    .prologue
    .line 265
    const-string v0, "EasDownLoadAttachmentSvc"

    const-string v1, "getClientConnectionManagerForAttachment Downloads : "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-boolean v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSsl:Z

    iget v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPort:I

    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->getClientConnectionManagerForAttachmentDownLoads(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method private getRequestRange()Ljava/lang/String;
    .locals 4

    .prologue
    .line 931
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getSavedLengthInLastSession()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    .line 932
    iget-wide v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 933
    :cond_0
    const/4 v0, 0x0

    .line 935
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSavedLengthInLastSession()J
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x1

    const/4 v7, 0x0

    .line 909
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v4, v5, v10, v11}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentPartUri(JJ)Landroid/net/Uri;

    move-result-object v1

    .line 913
    .local v1, "attachmentPartFileUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContentResolver:Landroid/content/ContentResolver;

    new-array v2, v12, [Ljava/lang/String;

    const-string v4, "_size"

    aput-object v4, v2, v7

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v10, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 919
    .local v6, "cr":Landroid/database/Cursor;
    const-wide/16 v8, -0x1

    .line 921
    .local v8, "partSize":J
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 922
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 924
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 926
    return-wide v8
.end method

.method private isAttachmentSessionRecoveryEnabled()Z
    .locals 4

    .prologue
    .line 875
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide v2, 0x402c333333333333L    # 14.1

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 878
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    iget-object v0, v0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    iget-object v0, v0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    .line 880
    const/4 v0, 0x1

    .line 882
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWifiConnected()Z
    .locals 6

    .prologue
    .line 270
    const/4 v2, 0x0

    .line 272
    .local v2, "isWifiConnected":Z
    :try_start_0
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 274
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 275
    .local v3, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 276
    const/4 v2, 0x1

    .line 284
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "networkInfo":Landroid/net/NetworkInfo;
    :goto_0
    return v2

    .line 278
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 280
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "networkInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 281
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 282
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private shutDownService()V
    .locals 6

    .prologue
    .line 204
    const-string v1, "EasDownLoadAttachmentSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finishing download service of attachment id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    iget-object v3, v3, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-boolean v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-nez v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->clearAttRqFromQueue(Lcom/android/exchange/PartRequest;)V

    .line 208
    :cond_0
    sget-boolean v1, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z

    if-eqz v1, :cond_1

    .line 210
    :try_start_0
    iget-boolean v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-nez v1, :cond_1

    .line 211
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    iget-object v1, v1, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->shutdownConnectionManagerForAttachmentDownLoads(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :cond_1
    :goto_0
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 219
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private writeToServiceLogger(Ljava/lang/Exception;)V
    .locals 4
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 979
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " attachmentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Exception:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 981
    return-void
.end method

.method private writeToServiceLogger(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 974
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "thread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getThreadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/ServiceLogger;->logAttachmentStats(Ljava/lang/String;)V

    .line 976
    return-void
.end method


# virtual methods
.method protected doStatusCallback(I)V
    .locals 10
    .param p1, "status"    # I

    .prologue
    const-wide/16 v8, 0x0

    const/high16 v5, 0x130000

    const/4 v4, 0x1

    .line 794
    const/4 v7, 0x0

    .line 797
    .local v7, "progress":I
    iget-boolean v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsDeadService:Z

    if-eqz v1, :cond_0

    .line 832
    :goto_0
    return-void

    .line 800
    :cond_0
    if-nez p1, :cond_2

    .line 801
    const/16 v7, 0x64

    .line 825
    :cond_1
    :goto_1
    iput v7, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastProgress:I

    .line 828
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move v6, p1

    invoke-interface/range {v1 .. v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 829
    :catch_0
    move-exception v1

    goto :goto_0

    .line 802
    :cond_2
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 806
    iget-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    cmp-long v1, v2, v8

    if-gtz v1, :cond_3

    if-ne v5, p1, :cond_1

    .line 807
    :cond_3
    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsDeadService:Z

    .line 810
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 811
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "encodedSize"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->SPLIT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    sget-object v2, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 814
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 815
    iget-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    div-long/2addr v2, v4

    long-to-int v7, v2

    goto :goto_1

    .line 818
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_4
    if-ne v5, p1, :cond_1

    .line 819
    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsDeadService:Z

    .line 820
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 821
    .restart local v0    # "cv":Landroid/content/ContentValues;
    const-string v1, "contentUri"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 822
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    sget-object v2, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    goto :goto_1
.end method

.method protected fetchAttachment(Lcom/android/exchange/PartRequest;)V
    .locals 24
    .param p1, "req"    # Lcom/android/exchange/PartRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 486
    if-eqz p1, :cond_2

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 488
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v18

    if-nez v18, :cond_0

    .line 489
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V

    .line 492
    :cond_0
    const/4 v15, 0x0

    .line 495
    .local v15, "res":Lcom/android/exchange/EasResponse;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->makeFetchAttachmentRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;

    move-result-object v15

    .line 496
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x447a0000    # 1000.0f

    div-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mResponseTime:F

    .line 497
    if-nez v15, :cond_3

    .line 498
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mAccount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " attachmentId="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " info=response is NULL"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 501
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v18, v0

    if-nez v18, :cond_1

    .line 502
    const/16 v18, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 672
    :cond_1
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    .line 676
    .end local v15    # "res":Lcom/android/exchange/EasResponse;
    :cond_2
    :goto_0
    return-void

    .line 506
    .restart local v15    # "res":Lcom/android/exchange/EasResponse;
    :cond_3
    :try_start_1
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v16

    .line 507
    .local v16, "status":I
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mAccount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " cmd="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mASCmd:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " attachmentId="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " FileName="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " res="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " responseTime="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mResponseTime:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "sec"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 511
    sget-boolean v18, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v18, :cond_4

    .line 512
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getAttachment(): GetAttachment command http response code:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V

    .line 515
    :cond_4
    const/16 v18, 0xc8

    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_5

    .line 516
    const/16 v18, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 521
    :cond_5
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->isSaved()Z

    move-result v18

    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-gtz v18, :cond_6

    .line 526
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 527
    .local v7, "cv":Landroid/content/ContentValues;
    const-string v18, "encodedSize"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "0"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->SPLIT:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    sget-object v19, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 532
    .end local v7    # "cv":Landroid/content/ContentValues;
    :cond_6
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 533
    .local v12, "is":Ljava/io/InputStream;
    new-instance v18, Lcom/android/exchange/adapter/ItemOperationsParser;

    new-instance v19, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v12, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 535
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v18, v0

    if-eqz v18, :cond_7

    .line 536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/ItemOperationsParser;->stopParser()V
    :try_end_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 540
    :cond_7
    :try_start_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 544
    const/4 v6, 0x0

    .line 545
    .local v6, "attachmentUri":Landroid/net/Uri;
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentPartUri(JJ)Landroid/net/Uri;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v6

    .line 554
    :goto_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 559
    :goto_2
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->flush()V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/android/exchange/adapter/ItemOperationsParser;->setOutputStream(Ljava/io/OutputStream;)V

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->setObserver(Ljava/util/Observer;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z

    move-result v11

    .line 566
    .local v11, "iopResult":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I

    move-result v14

    .line 568
    .local v14, "parsedStatus":I
    if-nez v11, :cond_9

    .line 570
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mAccount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " attachmentId="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " info=response is NULL"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 573
    const/16 v18, 0xb

    move/from16 v0, v18

    if-ne v14, v0, :cond_c

    .line 574
    const/high16 v18, 0x120000

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->flush()V

    .line 577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 580
    :try_start_6
    sget-object v18, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentFilename(Landroid/content/Context;JJ)Ljava/io/File;

    move-result-object v17

    .line 582
    .local v17, "tempFile":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    .line 583
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 644
    :try_start_7
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Finishing pending request to download attachment : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 549
    .end local v11    # "iopResult":Z
    .end local v14    # "parsedStatus":I
    .end local v17    # "tempFile":Ljava/io/File;
    :cond_8
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v6

    goto/16 :goto_1

    .line 556
    :catch_0
    move-exception v10

    .line 557
    .local v10, "e1":Ljava/io/FileNotFoundException;
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "Can\'t get attachment; write file not found?"

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_2

    .line 640
    .end local v6    # "attachmentUri":Landroid/net/Uri;
    .end local v10    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v13

    .line 641
    .local v13, "ome":Ljava/lang/OutOfMemoryError;
    :try_start_9
    const-string v18, "EasDownLoadAttachmentSvc"

    const-string v19, "Fetching attachment is Out of Memory"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttInfo()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " Exception:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v13}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 644
    :try_start_a
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Finishing pending request to download attachment : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_a
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 672
    .end local v13    # "ome":Ljava/lang/OutOfMemoryError;
    :goto_3
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 586
    .restart local v6    # "attachmentUri":Landroid/net/Uri;
    .restart local v11    # "iopResult":Z
    .restart local v14    # "parsedStatus":I
    :catch_2
    move-exception v10

    .line 587
    .local v10, "e1":Ljava/lang/IllegalArgumentException;
    :try_start_b
    invoke-virtual {v10}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 600
    .end local v10    # "e1":Ljava/lang/IllegalArgumentException;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->flush()V

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V

    .line 605
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v18

    if-eqz v18, :cond_a

    .line 606
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 607
    .restart local v7    # "cv":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v6, v7, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 610
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v6

    .line 614
    .end local v7    # "cv":Landroid/content/ContentValues;
    :cond_a
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v18

    if-eqz v18, :cond_b

    .line 615
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 616
    .local v8, "cva":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v6, v8, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 619
    .end local v8    # "cva":Landroid/content/ContentValues;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v18, v0

    if-eqz v18, :cond_d

    .line 620
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "info=user Cancelled Attachment Request for attachmentId= mAccount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " attachmentId="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/OutOfMemoryError; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 644
    :try_start_c
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Finishing pending request to download attachment : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_c
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 589
    :cond_c
    const/16 v18, 0x14

    move/from16 v0, v18

    if-ne v14, v0, :cond_9

    .line 590
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->flush()V

    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/io/OutputStream;->close()V

    .line 592
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Size conflict occured for File::  Name : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , Id: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ,and server size: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsAttachmentSizeConflict:Z
    :try_end_d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 644
    :try_start_e
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Finishing pending request to download attachment : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_e
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 627
    :cond_d
    :try_start_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x447a0000    # 1000.0f

    div-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDownloadTime:F

    .line 628
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mAccount="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mProtocol="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " cmd=GetAttachment"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttInfo()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " res="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " downloadTime="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDownloadTime:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "sec"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->isSaved()Z

    move-result v18

    if-eqz v18, :cond_e

    .line 634
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 635
    .restart local v7    # "cv":Landroid/content/ContentValues;
    const-string v18, "contentUri"

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    sget-object v19, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 637
    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_f
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 644
    .end local v7    # "cv":Landroid/content/ContentValues;
    .end local v11    # "iopResult":Z
    .end local v14    # "parsedStatus":I
    :cond_e
    :try_start_10
    const-string v18, "EasDownLoadAttachmentSvc"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Finishing pending request to download attachment : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_10
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_3

    .line 650
    .end local v6    # "attachmentUri":Landroid/net/Uri;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v16    # "status":I
    :catch_3
    move-exception v9

    .line 651
    .local v9, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_11
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/Exception;)V

    .line 652
    const/16 v18, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 644
    .end local v9    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v16    # "status":I
    :catchall_0
    move-exception v18

    :try_start_12
    const-string v19, "EasDownLoadAttachmentSvc"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Finishing pending request to download attachment : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 648
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V

    throw v18
    :try_end_12
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_12 .. :try_end_12} :catch_3
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_4
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 653
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v16    # "status":I
    :catch_4
    move-exception v9

    .line 654
    .local v9, "e":Ljava/io/IOException;
    :try_start_13
    const-string v18, "EasDownLoadAttachmentSvc"

    const-string v19, "get IOException while getAttachment http request"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/Exception;)V

    .line 659
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 661
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v18, v0

    if-nez v18, :cond_11

    .line 662
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v18, v0

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0x4000

    move/from16 v18, v0

    if-eqz v18, :cond_f

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isWifiConnected()Z

    move-result v18

    if-nez v18, :cond_f

    .line 664
    const/16 v18, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 672
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 667
    :cond_f
    const/16 v18, 0x20

    :try_start_14
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 669
    new-instance v18, Ljava/io/IOException;

    invoke-direct/range {v18 .. v18}, Ljava/io/IOException;-><init>()V

    throw v18
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 672
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v18

    if-eqz v15, :cond_10

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    :cond_10
    throw v18

    .line 672
    .restart local v9    # "e":Ljava/io/IOException;
    :cond_11
    if-eqz v15, :cond_2

    .line 673
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0
.end method

.method public getAttachment()Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .locals 1

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    return-object v0
.end method

.method public getCancelledFlag()Z
    .locals 1

    .prologue
    .line 762
    iget-boolean v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    return v0
.end method

.method protected getClient(Ljava/lang/String;IZ)Lorg/apache/http/client/HttpClient;
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p3, "isPingCommand"    # Z

    .prologue
    .line 224
    invoke-direct {p0, p2}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttachmentDownloadClient(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method protected loadAttachment(Lcom/android/exchange/PartRequest;)V
    .locals 26
    .param p1, "req"    # Lcom/android/exchange/PartRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    const-string v22, "EasDownLoadAttachmentSvc"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Start attachment loading for :: Name: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " , Id: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " ,and server size: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v24, v0

    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v22, v0

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0x4000

    move/from16 v22, v0

    if-eqz v22, :cond_1

    .line 298
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isWifiConnected()Z

    move-result v22

    if-nez v22, :cond_1

    .line 299
    const/16 v22, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    const-wide/high16 v24, 0x402c000000000000L    # 14.0

    cmpl-double v22, v22, v24

    if-ltz v22, :cond_2

    .line 306
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->fetchAttachment(Lcom/android/exchange/PartRequest;)V
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 307
    :catch_0
    move-exception v8

    .line 308
    .local v8, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    const-string v22, "info=DeviceAccessException occured while loadAttachment"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 309
    const/16 v22, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto :goto_0

    .line 316
    .end local v8    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :cond_2
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V

    .line 318
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "GetAttachment&AttachmentName="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 319
    .local v6, "cmd":Ljava/lang/String;
    const-string v22, "EasDownLoadAttachmentSvc"

    const-string v23, "getAttachment http request start"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const/16 v18, 0x0

    .line 323
    .local v18, "res":Lcom/android/exchange/EasResponse;
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    .line 324
    const/16 v22, 0x0

    sget v23, Lcom/android/exchange/EasDownLoadAttachmentSvc;->COMMAND_TIMEOUT:I

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v6, v1, v2}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v18

    .line 325
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    move-wide/from16 v0, v22

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mResponseTime:F

    .line 326
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v19

    .line 328
    .local v19, "statusCode":I
    const-string v22, "EasDownLoadAttachmentSvc"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "GetAttachment command http response code:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    sget-boolean v22, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v22, :cond_3

    .line 331
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "getAttachment(): GetAttachment command http response code:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V

    .line 334
    :cond_3
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mAccount="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mProtocol="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " cmd=GetAttachment"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttInfo()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " res="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " responseTime="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mResponseTime:F

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "sec"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 338
    const/16 v22, 0xc8

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_11

    .line 339
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v13

    .line 340
    .local v13, "len":I
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 341
    .local v12, "is":Ljava/io/InputStream;
    const/4 v15, 0x0

    .line 342
    .local v15, "os":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v24, v0

    invoke-static/range {v22 .. v25}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 344
    .local v4, "attachmentUri":Landroid/net/Uri;
    const/4 v10, 0x0

    .line 347
    .local v10, "fileName":Ljava/lang/String;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v15

    .line 348
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "Attachment filename retrieved as: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 354
    :goto_1
    if-eqz v15, :cond_a

    .line 355
    if-eqz v13, :cond_9

    .line 360
    :try_start_3
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 361
    const/16 v22, 0x4000

    move/from16 v0, v22

    new-array v5, v0, [B

    .line 362
    .local v5, "bytes":[B
    move v14, v13

    .line 366
    .local v14, "length":I
    const/16 v21, 0x0

    .line 368
    .local v21, "totalRead":I
    const/16 v20, 0x0

    .line 369
    .local v20, "tempPct":I
    const-string v22, "Attachment content-length: "

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v13}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog(Ljava/lang/String;I)V

    .line 372
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v22, v0

    if-eqz v22, :cond_6

    .line 373
    const/16 v22, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 375
    if-eqz v12, :cond_5

    .line 376
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 378
    :cond_5
    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 379
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 433
    const/16 v22, 0x0

    :try_start_4
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 434
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 479
    if-eqz v18, :cond_0

    .line 480
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 349
    .end local v5    # "bytes":[B
    .end local v14    # "length":I
    .end local v20    # "tempPct":I
    .end local v21    # "totalRead":I
    :catch_1
    move-exception v9

    .line 350
    .local v9, "e1":Ljava/io/FileNotFoundException;
    const/16 v22, 0x1

    :try_start_5
    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const-string v24, "Can\'t get attachment; write file not found?"

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V

    .line 351
    const/16 v22, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 458
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v9    # "e1":Ljava/io/FileNotFoundException;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v15    # "os":Ljava/io/OutputStream;
    .end local v19    # "statusCode":I
    :catch_2
    move-exception v11

    .line 459
    .local v11, "ioe":Ljava/io/IOException;
    :try_start_6
    const-string v22, "EasDownLoadAttachmentSvc"

    const-string v23, "got IOException while getAttachment http request"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mAccount="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mProtocol="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " cmd=GetAttachment"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttInfo()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " res=IOException"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 466
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    .line 468
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v22, v0

    if-nez v22, :cond_14

    .line 469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    move/from16 v22, v0

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0x4000

    move/from16 v22, v0

    if-eqz v22, :cond_13

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isWifiConnected()Z

    move-result v22

    if-nez v22, :cond_13

    .line 470
    const/16 v22, 0x43

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 479
    if-eqz v18, :cond_0

    .line 480
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 383
    .end local v11    # "ioe":Ljava/io/IOException;
    .restart local v4    # "attachmentUri":Landroid/net/Uri;
    .restart local v5    # "bytes":[B
    .restart local v10    # "fileName":Ljava/lang/String;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v14    # "length":I
    .restart local v15    # "os":Ljava/io/OutputStream;
    .restart local v19    # "statusCode":I
    .restart local v20    # "tempPct":I
    .restart local v21    # "totalRead":I
    :cond_6
    const/16 v17, 0x0

    .line 385
    .local v17, "read":I
    if-eqz v12, :cond_7

    .line 386
    const/16 v22, 0x0

    const/16 v23, 0x4000

    :try_start_7
    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v12, v5, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v17

    .line 389
    :cond_7
    if-gez v17, :cond_b

    .line 391
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    move/from16 v22, v0

    if-nez v22, :cond_b

    .line 392
    const-string v22, "Attachment load reached EOF, totalRead: "

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog(Ljava/lang/String;I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 433
    :cond_8
    :goto_3
    const/16 v22, 0x0

    :try_start_8
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 434
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V

    .line 437
    .end local v5    # "bytes":[B
    .end local v14    # "length":I
    .end local v17    # "read":I
    .end local v20    # "tempPct":I
    .end local v21    # "totalRead":I
    :cond_9
    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 438
    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V

    .line 439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    move-wide/from16 v0, v22

    long-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x447a0000    # 1000.0f

    div-float v22, v22, v23

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDownloadTime:F

    .line 440
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "mAccount="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mProtocol="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " cmd=GetAttachment"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttInfo()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " res="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " downloadTime="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDownloadTime:F

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "sec"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->isSaved()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 446
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 447
    .local v7, "cv":Landroid/content/ContentValues;
    const-string v22, "contentUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    sget-object v23, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 449
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 479
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v7    # "cv":Landroid/content/ContentValues;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v15    # "os":Ljava/io/OutputStream;
    :cond_a
    :goto_4
    if-eqz v18, :cond_0

    .line 480
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 397
    .restart local v4    # "attachmentUri":Landroid/net/Uri;
    .restart local v5    # "bytes":[B
    .restart local v10    # "fileName":Ljava/lang/String;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v14    # "length":I
    .restart local v15    # "os":Ljava/io/OutputStream;
    .restart local v17    # "read":I
    .restart local v20    # "tempPct":I
    .restart local v21    # "totalRead":I
    :cond_b
    add-int v21, v21, v17

    .line 400
    const/16 v22, 0x0

    :try_start_9
    move/from16 v0, v22

    move/from16 v1, v17

    invoke-virtual {v15, v5, v0, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 402
    instance-of v0, v12, Ljava/util/zip/GZIPInputStream;

    move/from16 v22, v0

    if-eqz v22, :cond_e

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    long-to-int v14, v0

    .line 405
    move/from16 v0, v21

    if-le v0, v14, :cond_c

    .line 406
    add-int/lit8 v14, v21, 0x1

    .line 408
    :cond_c
    mul-int/lit8 v22, v21, 0x64

    div-int v16, v22, v14

    .line 410
    .local v16, "pct":I
    move/from16 v0, v20

    move/from16 v1, v16

    if-ge v0, v1, :cond_d

    const/16 v22, 0x64

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_d

    .line 411
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V

    .line 413
    :cond_d
    move/from16 v20, v16

    .line 414
    goto/16 :goto_2

    .end local v16    # "pct":I
    :cond_e
    if-lez v14, :cond_4

    .line 415
    move/from16 v0, v21

    if-le v0, v14, :cond_10

    .line 416
    const-string v22, "totalRead is greater than attachment length?"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->errorLog(Ljava/lang/String;)V

    .line 417
    sget-boolean v22, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v22, :cond_8

    .line 419
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/android/exchange/ExchangeService;->shutdownConnectionManagerForAttachmentDownLoads(J)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 421
    :catch_3
    move-exception v8

    .line 422
    .local v8, "e":Ljava/lang/Exception;
    :try_start_b
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 433
    .end local v5    # "bytes":[B
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v14    # "length":I
    .end local v17    # "read":I
    .end local v20    # "tempPct":I
    .end local v21    # "totalRead":I
    :catchall_0
    move-exception v22

    const/16 v23, 0x0

    :try_start_c
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 434
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->abortPendingPost()V

    throw v22
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 479
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v15    # "os":Ljava/io/OutputStream;
    .end local v19    # "statusCode":I
    :catchall_1
    move-exception v22

    if-eqz v18, :cond_f

    .line 480
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    :cond_f
    throw v22

    .line 427
    .restart local v4    # "attachmentUri":Landroid/net/Uri;
    .restart local v5    # "bytes":[B
    .restart local v10    # "fileName":Ljava/lang/String;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v14    # "length":I
    .restart local v15    # "os":Ljava/io/OutputStream;
    .restart local v17    # "read":I
    .restart local v19    # "statusCode":I
    .restart local v20    # "tempPct":I
    .restart local v21    # "totalRead":I
    :cond_10
    mul-int/lit8 v22, v21, 0x64

    :try_start_d
    div-int v16, v22, v14

    .line 428
    .restart local v16    # "pct":I
    const/16 v22, 0x64

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    .line 429
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_2

    .line 452
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v5    # "bytes":[B
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v14    # "length":I
    .end local v15    # "os":Ljava/io/OutputStream;
    .end local v16    # "pct":I
    .end local v17    # "read":I
    .end local v20    # "tempPct":I
    .end local v21    # "totalRead":I
    :cond_11
    const/16 v22, 0x19d

    move/from16 v0, v19

    move/from16 v1, v22

    if-ne v0, v1, :cond_12

    .line 453
    const/16 v22, 0x0

    :try_start_e
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 454
    const/high16 v22, 0x120000

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto/16 :goto_4

    .line 456
    :cond_12
    const/16 v22, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_4

    .line 473
    .end local v19    # "statusCode":I
    .restart local v11    # "ioe":Ljava/io/IOException;
    :cond_13
    const/16 v22, 0x20

    :try_start_f
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 475
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 477
    :cond_14
    throw v11
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1
.end method

.method protected makeFetchAttachmentRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;
    .locals 8
    .param p1, "fileRef"    # Ljava/lang/String;
    .param p2, "username"    # Ljava/lang/String;
    .param p3, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 681
    if-nez p1, :cond_0

    .line 721
    :goto_0
    return-object v1

    .line 684
    :cond_0
    new-instance v2, Lcom/android/emailcommon/utility/Serializer;

    invoke-direct {v2}, Lcom/android/emailcommon/utility/Serializer;-><init>()V

    .line 686
    .local v2, "s":Lcom/android/emailcommon/utility/Serializer;
    const/16 v3, 0x505

    invoke-virtual {v2, v3}, Lcom/android/emailcommon/utility/Serializer;->start(I)Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    const/16 v4, 0x506

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/utility/Serializer;->start(I)Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    const/16 v4, 0x507

    const-string v5, "Mailbox"

    invoke-virtual {v3, v4, v5}, Lcom/android/emailcommon/utility/Serializer;->data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    const/16 v4, 0x451

    invoke-virtual {v3, v4, p1}, Lcom/android/emailcommon/utility/Serializer;->data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    .line 689
    const/16 v3, 0x508

    invoke-virtual {v2, v3}, Lcom/android/emailcommon/utility/Serializer;->start(I)Lcom/android/emailcommon/utility/Serializer;

    .line 691
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 692
    const/16 v3, 0x514

    invoke-virtual {v2, v3, p2}, Lcom/android/emailcommon/utility/Serializer;->data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    const/16 v4, 0x515

    invoke-virtual {v3, v4, p3}, Lcom/android/emailcommon/utility/Serializer;->data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    .line 698
    :cond_1
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 699
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getRequestRange()Ljava/lang/String;

    move-result-object v0

    .line 700
    .local v0, "range":Ljava/lang/String;
    const-string v3, "EasDownloadAttachmentSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Partial file found; partial size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " getRequestRange: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    if-eqz v0, :cond_2

    .line 704
    const/16 v3, 0x509

    invoke-virtual {v2, v3, v0}, Lcom/android/emailcommon/utility/Serializer;->data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    .line 707
    .end local v0    # "range":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Lcom/android/emailcommon/utility/Serializer;->end()Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/emailcommon/utility/Serializer;->end()Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/emailcommon/utility/Serializer;->end()Lcom/android/emailcommon/utility/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/emailcommon/utility/Serializer;->done()V

    .line 709
    iget-boolean v3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-eqz v3, :cond_3

    .line 710
    const/high16 v3, 0x130000

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    goto :goto_0

    .line 714
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mStartTime:J

    .line 715
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cmd=ItemOperations fileRef="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FileName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mAccount="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->writeToServiceLogger(Ljava/lang/String;)V

    .line 718
    const/4 v1, 0x0

    .line 719
    .local v1, "ret_value":Lcom/android/exchange/EasResponse;
    const-string v3, "ItemOperations;"

    invoke-virtual {v2}, Lcom/android/emailcommon/utility/Serializer;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v1

    .line 721
    goto/16 :goto_0
.end method

.method public resetPartFileOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 886
    iput-object p1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    .line 888
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 889
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 890
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    .line 892
    sget-object v1, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentPartFilename(Landroid/content/Context;JJ)Ljava/io/File;

    move-result-object v0

    .line 895
    .local v0, "partFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 896
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 899
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 900
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    .line 903
    .end local v0    # "partFile":Ljava/io/File;
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSessionRecoveryOutputStream:Ljava/io/OutputStream;

    return-object v1
.end method

.method public run()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 121
    iget-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAccountKey:J

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    .line 122
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->setupService()Z

    .line 128
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    if-eqz v4, :cond_0

    .line 129
    iget-object v3, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    .line 133
    .local v3, "req":Lcom/android/exchange/PartRequest;
    iget-object v4, v3, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iput-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 135
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Starting download service for attachment id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 139
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    if-nez v4, :cond_2

    .line 140
    const-string v4, "EasDownLoadAttachmentSvc"

    const-string v5, "run() - msg is null."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    .line 144
    :cond_2
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->checkAttachmentPolicy()Z

    move-result v4

    if-nez v4, :cond_3

    .line 145
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    .line 148
    :cond_3
    :try_start_0
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mDeviceId:Ljava/lang/String;

    .line 150
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {p0, v4, v5}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->checkLoadAttachmentRequest(J)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 151
    iget-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    if-eqz v4, :cond_5

    .line 152
    const/high16 v4, 0x130000

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 153
    const/4 v4, 0x0

    iput v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mExitStatus:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    goto :goto_0

    .line 155
    :cond_5
    :try_start_1
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    if-eqz v4, :cond_a

    .line 156
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getSavedLengthInLastSession()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    .line 158
    const/4 v2, 0x0

    .line 159
    .local v2, "percentageCompletion":I
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_6

    iget-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    cmp-long v4, v4, v8

    if-eqz v4, :cond_6

    .line 160
    iget-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-object v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    div-long/2addr v4, v6

    long-to-int v2, v4

    .line 162
    :cond_6
    invoke-direct {p0, v2}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V

    .line 169
    .end local v2    # "percentageCompletion":I
    :goto_2
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "before call loadAttachment id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->loadAttachment(Lcom/android/exchange/PartRequest;)V

    .line 176
    iget-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsAttachmentSizeConflict:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_7

    .line 178
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsAttachmentSizeConflict:Z

    .line 179
    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->loadAttachment(Lcom/android/exchange/PartRequest;)V

    .line 181
    :cond_7
    const/4 v4, 0x0

    iput v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mExitStatus:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/io/IOException;
    const/16 v4, 0x8

    :try_start_2
    iput v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mExitStatus:I

    .line 186
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "message":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Caught IOException in Get Attachment: "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    if-nez v1, :cond_8

    const-string v1, "No message"

    .end local v1    # "message":Ljava/lang/String;
    :cond_8
    aput-object v1, v4, v5

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userLog([Ljava/lang/String;)V

    .line 189
    iget-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-nez v4, :cond_9

    .line 190
    const/16 v4, 0x20

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V

    .line 192
    :cond_9
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 197
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    goto/16 :goto_0

    .line 164
    .end local v0    # "e":Ljava/io/IOException;
    :cond_a
    :try_start_3
    sget-object v4, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    iget-object v5, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 193
    :catch_1
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v4, "EasDownLoadAttachmentSvc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception Caught "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 197
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    goto/16 :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->shutDownService()V

    throw v4
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 993
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/android/exchange/EasSyncService;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for Attachment : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 997
    :goto_0
    return-object v1

    .line 994
    :catch_0
    move-exception v0

    .line 995
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 997
    invoke-super {p0}, Lcom/android/exchange/EasSyncService;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 8
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const-wide/16 v6, 0x0

    .line 835
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 854
    .end local p2    # "data":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 837
    .restart local p2    # "data":Ljava/lang/Object;
    :cond_1
    instance-of v1, p1, Lcom/android/exchange/adapter/ItemOperationsParser;

    if-eqz v1, :cond_0

    .line 840
    instance-of v1, p2, Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 841
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "data":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    .line 842
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 843
    iget-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    iget-wide v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mLastSavedLength:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    .line 847
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_4

    iget-wide v2, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 849
    .local v0, "progress":I
    :goto_2
    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 852
    invoke-direct {p0, v0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doProgressCallback(I)V

    goto :goto_0

    .line 844
    .end local v0    # "progress":I
    :cond_3
    iget-boolean v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-eqz v1, :cond_2

    .line 845
    iput-wide v6, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mSavedLength:J

    goto :goto_1

    .line 847
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public updateAttachmentActualSize(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 857
    instance-of v1, p1, Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 858
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-object v1, p1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 859
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 860
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "size"

    check-cast p1, Ljava/lang/Long;

    .end local p1    # "data":Ljava/lang/Object;
    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 861
    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    sget-object v2, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 862
    const/4 v1, 0x1

    .line 865
    .end local v0    # "cv":Landroid/content/ContentValues;
    :goto_0
    return v1

    .restart local p1    # "data":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateAttachmentInstance()V
    .locals 4

    .prologue
    .line 1005
    sget-object v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1006
    return-void
.end method

.method public userCancelledAttachmentRequest(Lcom/android/exchange/Request;)Z
    .locals 8
    .param p1, "req"    # Lcom/android/exchange/Request;

    .prologue
    const/4 v7, 0x1

    .line 726
    if-eqz p1, :cond_0

    .line 727
    :try_start_0
    iget-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    if-nez v4, :cond_0

    .line 728
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mIsCancelledByUser:Z

    .line 729
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    if-eqz v4, :cond_3

    .line 730
    const-string v5, "EasDownLoadAttachmentSvc"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Issuing attachment cancel request : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object v0, p1

    check-cast v0, Lcom/android/exchange/PartRequest;

    move-object v4, v0

    iget-object v4, v4, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    invoke-virtual {v4}, Lcom/android/exchange/adapter/ItemOperationsParser;->stopParser()V

    .line 733
    check-cast p1, Lcom/android/exchange/PartRequest;

    .end local p1    # "req":Lcom/android/exchange/Request;
    const/4 v4, 0x1

    invoke-static {p1, v4}, Lcom/android/exchange/ExchangeService;->clearAttRqFromQueue(Lcom/android/exchange/PartRequest;Z)V

    .line 742
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->isAttachmentSessionRecoveryEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 743
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 744
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v4, "contentUri"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 745
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mAtt:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    sget-object v5, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 747
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_1
    sget-boolean v4, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_2

    .line 749
    :try_start_1
    iget-object v4, p0, Lcom/android/exchange/EasDownLoadAttachmentSvc;->mRequest:Lcom/android/exchange/PartRequest;

    iget-object v4, v4, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->shutdownConnectionManagerForAttachmentDownLoads(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 758
    :cond_2
    :goto_1
    return v7

    .line 737
    .restart local p1    # "req":Lcom/android/exchange/Request;
    :cond_3
    const/high16 v4, 0x130000

    :try_start_2
    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->doStatusCallback(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 755
    .end local p1    # "req":Lcom/android/exchange/Request;
    :catch_0
    move-exception v3

    .line 756
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 751
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 752
    .restart local v3    # "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1
.end method
