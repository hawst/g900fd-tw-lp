.class public Lcom/android/exchange/EmailSyncAlarmReceiver;
.super Landroid/content/BroadcastReceiver;
.source "EmailSyncAlarmReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    }
.end annotation


# instance fields
.field final MAILBOX_DATA_PROJECTION:[Ljava/lang/String;

.field uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 51
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/EmailSyncAlarmReceiver;->MAILBOX_DATA_PROJECTION:[Ljava/lang/String;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/EmailSyncAlarmReceiver;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/EmailSyncAlarmReceiver;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/android/exchange/EmailSyncAlarmReceiver;->handleReceive(Landroid/content/Context;)V

    return-void
.end method

.method private handleReceive(Landroid/content/Context;)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    const-string v3, "TAG"

    const-string v4, "EmailSyncAlarmReceiver: handleReceive() called "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v24, "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 101
    .local v2, "cr":Landroid/content/ContentResolver;
    if-nez v2, :cond_1

    .line 102
    const-string v3, "TAG"

    const-string v4, "EmailSyncAlarmReceiver: handleReceive(), ContentResolver is null!! "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    return-void

    .line 106
    :cond_1
    const/16 v25, 0x0

    .line 110
    .local v25, "messageCount":I
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getEasAccountSelector()Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "selector":Ljava/lang/String;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->MAILBOX_DATA_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 118
    .local v18, "c":Landroid/database/Cursor;
    if-eqz v18, :cond_4

    .line 119
    :cond_2
    :goto_0
    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 120
    add-int/lit8 v25, v25, 0x1

    .line 121
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 122
    .local v8, "mailboxId":J
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 123
    .local v10, "messageId":J
    new-instance v6, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;-><init>(Lcom/android/exchange/EmailSyncAlarmReceiver;JJ)V

    .line 124
    .local v6, "notifyItem":Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 125
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 130
    .end local v6    # "notifyItem":Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    .end local v8    # "mailboxId":J
    .end local v10    # "messageId":J
    :catchall_0
    move-exception v3

    if-eqz v18, :cond_3

    .line 131
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3

    .line 130
    :cond_4
    if-eqz v18, :cond_5

    .line 131
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_5
    sget-object v13, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->MAILBOX_DATA_PROJECTION:[Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v12, v2

    move-object v15, v5

    invoke-virtual/range {v12 .. v17}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 139
    if-eqz v18, :cond_8

    .line 140
    :cond_6
    :goto_1
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 141
    add-int/lit8 v25, v25, 0x1

    .line 142
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 143
    .restart local v8    # "mailboxId":J
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 144
    .restart local v10    # "messageId":J
    new-instance v6, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;-><init>(Lcom/android/exchange/EmailSyncAlarmReceiver;JJ)V

    .line 145
    .restart local v6    # "notifyItem":Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 146
    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 151
    .end local v6    # "notifyItem":Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    .end local v8    # "mailboxId":J
    .end local v10    # "messageId":J
    :catchall_1
    move-exception v3

    if-eqz v18, :cond_7

    .line 152
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3

    .line 151
    :cond_8
    if-eqz v18, :cond_9

    .line 152
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_9
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    .line 157
    .local v22, "item":Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
    move-object/from16 v0, v22

    iget-wide v8, v0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->mailboxId:J

    .line 158
    .restart local v8    # "mailboxId":J
    move-object/from16 v0, v22

    iget-wide v10, v0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->messageId:J

    .line 160
    .restart local v10    # "messageId":J
    const/16 v23, 0x0

    .line 162
    .local v23, "mMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const-wide/16 v12, 0x0

    cmp-long v3, v8, v12

    if-ltz v3, :cond_b

    .line 163
    :try_start_2
    move-object/from16 v0, p1

    invoke-static {v0, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v23

    .line 168
    :cond_b
    :goto_3
    if-eqz v23, :cond_e

    move-object/from16 v0, v23

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_c

    move-object/from16 v0, v23

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_e

    .line 170
    :cond_c
    const-string v3, "TAG"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EmailSyncAlarmReceiver: remove trying to upsync for draft box or Outbox.. : mMailbox.mType = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :try_start_3
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    .line 176
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_d

    .line 177
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 184
    :cond_d
    :goto_4
    :try_start_4
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    .line 185
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_a

    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EmailSyncAlarmReceiver;->uri:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 188
    :catch_0
    move-exception v19

    .line 189
    .local v19, "e":Ljava/lang/Exception;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 165
    .end local v19    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v20

    .line 166
    .local v20, "ex":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 179
    .end local v20    # "ex":Ljava/lang/Exception;
    :catch_2
    move-exception v19

    .line 180
    .restart local v19    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 193
    .end local v19    # "e":Ljava/lang/Exception;
    :cond_e
    const/4 v3, 0x0

    invoke-static {v8, v9, v3}, Lcom/android/exchange/ExchangeService;->serviceRequest(JI)V

    goto/16 :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 57
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/exchange/EmailSyncAlarmReceiver$1;

    invoke-direct {v1, p0, p1}, Lcom/android/exchange/EmailSyncAlarmReceiver$1;-><init>(Lcom/android/exchange/EmailSyncAlarmReceiver;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 68
    return-void
.end method
