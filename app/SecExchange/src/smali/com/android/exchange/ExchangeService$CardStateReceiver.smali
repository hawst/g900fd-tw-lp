.class public Lcom/android/exchange/ExchangeService$CardStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CardStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4703
    const-string v4, "ACTION_CURRENT_NETWORK"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4704
    const-string v4, "state"

    const/4 v5, 0x0

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 4705
    .local v0, "activeNetwork":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "activeNetwork = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4706
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentActiveNetwork = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I
    invoke-static {v5}, Lcom/android/exchange/ExchangeService;->access$2500(Lcom/android/exchange/ExchangeService;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4708
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I
    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->access$2500(Lcom/android/exchange/ExchangeService;)I

    move-result v4

    if-eq v0, v4, :cond_1

    .line 4709
    const/4 v2, 0x0

    .line 4710
    .local v2, "oldRoamingType":Z
    const/4 v1, 0x0

    .line 4712
    .local v1, "newRoamingType":Z
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I
    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->access$2500(Lcom/android/exchange/ExchangeService;)I

    move-result v4

    if-nez v4, :cond_2

    .line 4713
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v2

    .line 4718
    :goto_0
    if-nez v0, :cond_3

    .line 4719
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    const-string v5, "phone"

    invoke-virtual {v4, v5}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 4720
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v3, :cond_0

    .line 4721
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    .line 4724
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # setter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v4, v1}, Lcom/android/exchange/ExchangeService;->access$2602(Lcom/android/exchange/ExchangeService;Z)Z

    .line 4736
    .end local v3    # "tm":Landroid/telephony/TelephonyManager;
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # setter for: Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I
    invoke-static {v4, v0}, Lcom/android/exchange/ExchangeService;->access$2502(Lcom/android/exchange/ExchangeService;I)I

    .line 4737
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "oldRoaming = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4738
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "newRoamingType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4739
    if-eq v2, v1, :cond_1

    .line 4740
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # invokes: Lcom/android/exchange/ExchangeService;->updateAccountDb()V
    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->access$2400(Lcom/android/exchange/ExchangeService;)V

    .line 4744
    .end local v0    # "activeNetwork":I
    .end local v1    # "newRoamingType":Z
    .end local v2    # "oldRoamingType":Z
    :cond_1
    return-void

    .line 4715
    .restart local v0    # "activeNetwork":I
    .restart local v1    # "newRoamingType":Z
    .restart local v2    # "oldRoamingType":Z
    :cond_2
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->access$2700(Lcom/android/exchange/ExchangeService;)Z

    move-result v2

    goto :goto_0

    .line 4729
    :cond_3
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/samsung/android/telephony/MultiSimManager;->isNetworkRoaming(I)Z

    move-result v1

    .line 4732
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$CardStateReceiver;->this$0:Lcom/android/exchange/ExchangeService;

    # setter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v4, v1}, Lcom/android/exchange/ExchangeService;->access$2702(Lcom/android/exchange/ExchangeService;Z)Z

    goto :goto_1
.end method
