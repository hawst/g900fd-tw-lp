.class final Lcom/android/exchange/ExchangeService$1;
.super Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;-><init>()V

    return-void
.end method

.method private declared-synchronized broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V
    .locals 5
    .param p1, "wrapper"    # Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;

    .prologue
    .line 608
    monitor-enter p0

    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    .line 610
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 612
    :try_start_1
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/android/emailcommon/service/IEmailServiceCallback;

    invoke-interface {p1, v3}, Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;->call(Lcom/android/emailcommon/service/IEmailServiceCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 610
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 615
    :catch_0
    move-exception v1

    .line 619
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v3, "ExchangeService"

    const-string v4, "Caught RuntimeException in broadcast"

    invoke-static {v3, v4, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 624
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    :try_start_3
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 608
    .end local v0    # "count":I
    .end local v2    # "i":I
    :catchall_1
    move-exception v3

    monitor-exit p0

    throw v3

    .line 624
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_0
    :try_start_4
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 626
    monitor-exit p0

    return-void

    .line 613
    :catch_1
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public clearSSLVerificationDomains()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 825
    new-instance v0, Lcom/android/exchange/ExchangeService$1$15;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$1$15;-><init>(Lcom/android/exchange/ExchangeService$1;)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 837
    return-void
.end method

.method public closeSSLVerificationDialog()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 809
    new-instance v0, Lcom/android/exchange/ExchangeService$1$14;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$1$14;-><init>(Lcom/android/exchange/ExchangeService$1;)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 821
    return-void
.end method

.method public deviceInfoStatus(I)V
    .locals 1
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 757
    new-instance v0, Lcom/android/exchange/ExchangeService$1$11;

    invoke-direct {v0, p0, p1}, Lcom/android/exchange/ExchangeService$1$11;-><init>(Lcom/android/exchange/ExchangeService$1;I)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 763
    return-void
.end method

.method public emptyTrashStatus(JII)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "statusCode"    # I
    .param p4, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 709
    new-instance v0, Lcom/android/exchange/ExchangeService$1$8;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/ExchangeService$1$8;-><init>(Lcom/android/exchange/ExchangeService$1;JII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 715
    return-void
.end method

.method public folderCommandStatus(IJI)V
    .locals 8
    .param p1, "command"    # I
    .param p2, "mailboxId"    # J
    .param p4, "statusCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 694
    new-instance v1, Lcom/android/exchange/ExchangeService$1$7;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/exchange/ExchangeService$1$7;-><init>(Lcom/android/exchange/ExchangeService$1;IJI)V

    invoke-direct {p0, v1}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 703
    return-void
.end method

.method public loadAttachmentStatus(JJII)V
    .locals 9
    .param p1, "messageId"    # J
    .param p3, "attachmentId"    # J
    .param p5, "status"    # I
    .param p6, "progress"    # I

    .prologue
    .line 632
    new-instance v0, Lcom/android/exchange/ExchangeService$1$1;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/exchange/ExchangeService$1$1;-><init>(Lcom/android/exchange/ExchangeService$1;JJII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 638
    return-void
.end method

.method public loadMoreStatus(JII)V
    .locals 7
    .param p1, "messageId"    # J
    .param p3, "statusCode"    # I
    .param p4, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 722
    new-instance v0, Lcom/android/exchange/ExchangeService$1$9;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/ExchangeService$1$9;-><init>(Lcom/android/exchange/ExchangeService$1;JII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 728
    return-void
.end method

.method public moveConvAlwaysStatus([BIII)V
    .locals 0
    .param p1, "convId"    # [B
    .param p2, "statusCode"    # I
    .param p3, "progress"    # I
    .param p4, "ignore"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 733
    return-void
.end method

.method public moveItemStatus(JI)V
    .locals 1
    .param p1, "mailboxId"    # J
    .param p3, "statusCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 670
    new-instance v0, Lcom/android/exchange/ExchangeService$1$5;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService$1$5;-><init>(Lcom/android/exchange/ExchangeService$1;JI)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 676
    return-void
.end method

.method public oOOfStatus(JIILandroid/os/Bundle;)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "statusCode"    # I
    .param p4, "progress"    # I
    .param p5, "oooResults"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 681
    new-instance v0, Lcom/android/exchange/ExchangeService$1$6;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/android/exchange/ExchangeService$1$6;-><init>(Lcom/android/exchange/ExchangeService$1;JIILandroid/os/Bundle;)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 687
    return-void
.end method

.method public refreshIRMTemplatesStatus(JII)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "statusCode"    # I
    .param p4, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 739
    new-instance v0, Lcom/android/exchange/ExchangeService$1$10;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/ExchangeService$1$10;-><init>(Lcom/android/exchange/ExchangeService$1;JII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 745
    return-void
.end method

.method public sendMeetingEditedResponseCallback(ZJJ)V
    .locals 8
    .param p1, "success"    # Z
    .param p2, "messageId"    # J
    .param p4, "draftId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 774
    new-instance v1, Lcom/android/exchange/ExchangeService$1$12;

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/android/exchange/ExchangeService$1$12;-><init>(Lcom/android/exchange/ExchangeService$1;ZJJ)V

    invoke-direct {p0, v1}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 785
    return-void
.end method

.method public sendMessageStatus(JJLjava/lang/String;II)V
    .locals 9
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "subject"    # Ljava/lang/String;
    .param p6, "status"    # I
    .param p7, "progress"    # I

    .prologue
    .line 642
    new-instance v0, Lcom/android/exchange/ExchangeService$1$2;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/android/exchange/ExchangeService$1$2;-><init>(Lcom/android/exchange/ExchangeService$1;JJLjava/lang/String;II)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 648
    return-void
.end method

.method public setBadSyncAccount(JZ)V
    .locals 1
    .param p1, "accountId"    # J
    .param p3, "nowBadSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 843
    new-instance v0, Lcom/android/exchange/ExchangeService$1$16;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService$1$16;-><init>(Lcom/android/exchange/ExchangeService$1;JZ)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 849
    return-void
.end method

.method public setDeviceInfoStatus(JII)V
    .locals 0
    .param p1, "accountId"    # J
    .param p3, "statusCode"    # I
    .param p4, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 752
    return-void
.end method

.method public syncMailboxListStatus(JII)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 651
    new-instance v0, Lcom/android/exchange/ExchangeService$1$3;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/ExchangeService$1$3;-><init>(Lcom/android/exchange/ExchangeService$1;JII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 657
    return-void
.end method

.method public syncMailboxStatus(JII)V
    .locals 7
    .param p1, "mailboxId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 660
    new-instance v0, Lcom/android/exchange/ExchangeService$1$4;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/ExchangeService$1$4;-><init>(Lcom/android/exchange/ExchangeService$1;JII)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 666
    return-void
.end method

.method public verifySSLCertificate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 793
    new-instance v0, Lcom/android/exchange/ExchangeService$1$13;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/exchange/ExchangeService$1$13;-><init>(Lcom/android/exchange/ExchangeService$1;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/android/exchange/ExchangeService$1;->broadcastCallback(Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;)V

    .line 805
    return-void
.end method
