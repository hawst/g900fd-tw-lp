.class Lcom/android/exchange/ExchangeService$SyncStatusError;
.super Lcom/android/exchange/ExchangeService$SyncError;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SyncStatusError"
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;IZ)V
    .locals 1
    .param p2, "_reason"    # I
    .param p3, "_fatal"    # Z

    .prologue
    .line 3093
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->this$0:Lcom/android/exchange/ExchangeService;

    .line 3094
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    .line 3091
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->count:I

    .line 3095
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->count:I

    .line 3096
    return-void
.end method


# virtual methods
.method escalate()V
    .locals 4

    .prologue
    .line 3100
    iget-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->autoRecover:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    const-wide/32 v2, 0x3a980

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 3101
    :cond_0
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    .line 3103
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdEndTime:J

    .line 3104
    iget v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/exchange/ExchangeService$SyncStatusError;->count:I

    .line 3105
    return-void
.end method
