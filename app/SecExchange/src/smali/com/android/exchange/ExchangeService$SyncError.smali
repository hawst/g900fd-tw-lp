.class Lcom/android/exchange/ExchangeService$SyncError;
.super Ljava/lang/Object;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SyncError"
.end annotation


# instance fields
.field DEFAULT_HOLD_DELAY:J

.field autoRecover:Z

.field fatal:Z

.field holdDelay:J

.field holdEndTime:J

.field reason:I

.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;IIZ)V
    .locals 4
    .param p2, "_reason"    # I
    .param p3, "_holdDelay"    # I
    .param p4, "_fatal"    # Z

    .prologue
    const/4 v0, 0x0

    .line 3065
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$SyncError;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3042
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3044
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 3046
    const-wide/16 v0, 0x3a98

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    .line 3048
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3050
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3066
    iput p2, p0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    .line 3067
    iput-boolean p4, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3068
    int-to-long v0, p3

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3069
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3070
    return-void
.end method

.method constructor <init>(Lcom/android/exchange/ExchangeService;IZ)V
    .locals 4
    .param p2, "_reason"    # I
    .param p3, "_fatal"    # Z

    .prologue
    const/4 v0, 0x0

    .line 3052
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$SyncError;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3042
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3044
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 3046
    const-wide/16 v0, 0x3a98

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    .line 3048
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3050
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3053
    iput p2, p0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    .line 3054
    iput-boolean p3, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3055
    return-void
.end method

.method constructor <init>(Lcom/android/exchange/ExchangeService;IZZ)V
    .locals 4
    .param p2, "_reason"    # I
    .param p3, "_fatal"    # Z
    .param p4, "_autoRecover"    # Z

    .prologue
    const/4 v0, 0x0

    .line 3057
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$SyncError;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3042
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3044
    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 3046
    const-wide/16 v0, 0x3a98

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    .line 3048
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3050
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3058
    iput p2, p0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    .line 3059
    iput-boolean p3, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 3060
    iput-boolean p4, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 3061
    const-wide/32 v0, 0xa4cb80

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3062
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3063
    return-void
.end method


# virtual methods
.method escalate()V
    .locals 4

    .prologue
    .line 3076
    iget-boolean v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    const-wide/32 v2, 0x3a980

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 3077
    :cond_0
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 3079
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 3080
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SyncError: \n reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fatal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " holdDelay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " holdEndTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " autoRecover="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
