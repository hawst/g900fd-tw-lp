.class Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;
.super Ljava/lang/Object;
.source "EasSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/EasSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OriginalMsgMoveRequest"
.end annotation


# instance fields
.field mDstMailboxKey:J

.field mFlags:I

.field mId:J

.field mSrcMailboxKey:J


# direct methods
.method public constructor <init>(JJIJ)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "srcMailboxKey"    # J
    .param p5, "flags"    # I
    .param p6, "dstMailboxKey"    # J

    .prologue
    .line 2525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2526
    iput-wide p1, p0, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mId:J

    .line 2527
    iput-wide p3, p0, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mSrcMailboxKey:J

    .line 2528
    iput p5, p0, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mFlags:I

    .line 2529
    iput-wide p6, p0, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mDstMailboxKey:J

    .line 2530
    return-void
.end method
