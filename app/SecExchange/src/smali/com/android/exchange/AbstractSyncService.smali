.class public abstract Lcom/android/exchange/AbstractSyncService;
.super Ljava/lang/Object;
.source "AbstractSyncService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/AbstractSyncService$ValidationResult;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;

.field public static mContext:Landroid/content/Context;


# instance fields
.field protected mASCmd:Ljava/lang/String;

.field protected mASCmdParams:Ljava/lang/String;

.field public mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

.field protected mAlias:Ljava/lang/String;

.field public mChangeCount:I

.field protected final mExceptionString:[Ljava/lang/String;

.field protected mExitStatus:I

.field protected mIsInitialSyncThread:Z

.field public mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

.field protected mMailboxId:J

.field protected mMailboxName:Ljava/lang/String;

.field protected mPendingRequest:Lcom/android/exchange/PartRequest;

.field protected mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/android/exchange/Request;",
            ">;"
        }
    .end annotation
.end field

.field protected volatile mRequestTime:J

.field protected volatile mStop:Z

.field public volatile mSyncReason:I

.field public mSyncRetryCount:I

.field protected final mSynchronizer:Ljava/lang/Object;

.field protected volatile mThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "AbstractSyncService"

    sput-object v0, Lcom/android/exchange/AbstractSyncService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailboxId"    # J

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NONE"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "IO_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOGIN_FAILURE"

    aput-object v2, v0, v1

    const-string v1, "GENERIC_EXCEPTION"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "SECURITY_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RESYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DEVICE_BLOCKED_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DEVICE_QUARANTINED_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GETATTACHMENT_IO_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SQL_IO_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNSUPPORTED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TOOMANY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SERVER_STATUS_5_ERROR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mExceptionString:[Ljava/lang/String;

    .line 122
    iput v5, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 130
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mChangeCount:I

    .line 132
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 134
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 141
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 143
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 147
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    .line 154
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mAlias:Ljava/lang/String;

    .line 157
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    .line 158
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmdParams:Ljava/lang/String;

    .line 162
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    .line 262
    sput-object p1, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    .line 263
    invoke-static {p1, p2, p3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 265
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 267
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    .line 268
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 271
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NONE"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "IO_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOGIN_FAILURE"

    aput-object v2, v0, v1

    const-string v1, "GENERIC_EXCEPTION"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "SECURITY_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RESYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DEVICE_BLOCKED_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DEVICE_QUARANTINED_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GETATTACHMENT_IO_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SQL_IO_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNSUPPORTED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TOOMANY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SERVER_STATUS_5_ERROR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mExceptionString:[Ljava/lang/String;

    .line 122
    iput v5, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 130
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mChangeCount:I

    .line 132
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 134
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 141
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 143
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 147
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    .line 154
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mAlias:Ljava/lang/String;

    .line 157
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    .line 158
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmdParams:Ljava/lang/String;

    .line 162
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    .line 237
    sput-object p1, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    .line 238
    iput-object p2, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 239
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 240
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    const-wide/16 v2, -0x1

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    .line 241
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 242
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    .line 243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NONE"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "IO_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOGIN_FAILURE"

    aput-object v2, v0, v1

    const-string v1, "GENERIC_EXCEPTION"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "SECURITY_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RESYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DEVICE_BLOCKED_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DEVICE_QUARANTINED_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GETATTACHMENT_IO_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SQL_IO_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNSUPPORTED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TOOMANY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SERVER_STATUS_5_ERROR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mExceptionString:[Ljava/lang/String;

    .line 122
    iput v5, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 130
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mChangeCount:I

    .line 132
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 134
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 141
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 143
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 147
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    .line 154
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mAlias:Ljava/lang/String;

    .line 157
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    .line 158
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmdParams:Ljava/lang/String;

    .line 162
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    .line 225
    sput-object p1, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    .line 226
    iput-object p2, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 227
    iget-wide v0, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 228
    iget-object v0, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    .line 229
    iget-wide v0, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 230
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 6
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NONE"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "IO_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOGIN_FAILURE"

    aput-object v2, v0, v1

    const-string v1, "GENERIC_EXCEPTION"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "SECURITY_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RESYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DEVICE_BLOCKED_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DEVICE_QUARANTINED_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GETATTACHMENT_IO_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SQL_IO_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNSUPPORTED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TOOMANY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SERVER_STATUS_5_ERROR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mExceptionString:[Ljava/lang/String;

    .line 122
    iput v5, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 130
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mChangeCount:I

    .line 132
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 134
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 141
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 143
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 147
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    .line 154
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mAlias:Ljava/lang/String;

    .line 157
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    .line 158
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmdParams:Ljava/lang/String;

    .line 162
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    .line 248
    sput-object p1, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    .line 249
    iget-wide v0, p2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 251
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 253
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    .line 254
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 257
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "NONE"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "IO_ERROR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "LOGIN_FAILURE"

    aput-object v2, v0, v1

    const-string v1, "GENERIC_EXCEPTION"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    const-string v2, "SECURITY_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "RESYNC"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DEVICE_BLOCKED_FAILURE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DEVICE_QUARANTINED_FAILURE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GETATTACHMENT_IO_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "SQL_IO_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "UNSUPPORTED"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TOOMANY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SERVER_STATUS_5_ERROR"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mExceptionString:[Ljava/lang/String;

    .line 122
    iput v5, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 130
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mChangeCount:I

    .line 132
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 134
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    .line 137
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    .line 139
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 141
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 143
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mPendingRequest:Lcom/android/exchange/PartRequest;

    .line 147
    iput v3, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    .line 154
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mAlias:Ljava/lang/String;

    .line 157
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    .line 158
    iput-object v4, p0, Lcom/android/exchange/AbstractSyncService;->mASCmdParams:Ljava/lang/String;

    .line 162
    iput-boolean v3, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    .line 292
    return-void
.end method

.method public static activate(Ljava/lang/Class;Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/android/exchange/AbstractSyncService;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 329
    .local p0, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/android/exchange/AbstractSyncService;>;"
    const/4 v1, 0x0

    .line 331
    .local v1, "license":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/exchange/AbstractSyncService;

    .line 332
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    invoke-virtual {v2, p1}, Lcom/android/exchange/AbstractSyncService;->activateDevice(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 338
    return-object v1

    .line 333
    .end local v2    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catch_0
    move-exception v0

    .line 334
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v3, Lcom/android/emailcommon/mail/MessagingException;

    const-string v4, "internal error"

    invoke-direct {v3, v4, v0}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 335
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v3, Lcom/android/emailcommon/mail/MessagingException;

    const-string v4, "internal error"

    invoke-direct {v3, v4, v0}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static validate(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
    .locals 9
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "port"    # I
    .param p5, "ssl"    # Z
    .param p6, "trustCertificates"    # Z
    .param p7, "path"    # Ljava/lang/String;
    .param p8, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/android/exchange/AbstractSyncService;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZZ",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 315
    .local p0, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/android/exchange/AbstractSyncService;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/AbstractSyncService;

    .line 316
    .local v0, "svc":Lcom/android/exchange/AbstractSyncService;
    sput-object p8, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    .line 317
    invoke-virtual/range {v0 .. v8}, Lcom/android/exchange/AbstractSyncService;->validateAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 322
    .end local v0    # "svc":Lcom/android/exchange/AbstractSyncService;
    :goto_0
    return-object v1

    .line 320
    :catch_0
    move-exception v1

    .line 322
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 319
    :catch_1
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public abstract activateDevice(Landroid/content/Context;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation
.end method

.method public addRequest(Lcom/android/exchange/Request;)V
    .locals 1
    .param p1, "req"    # Lcom/android/exchange/Request;

    .prologue
    .line 491
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 492
    return-void
.end method

.method public abstract alarm()Z
.end method

.method public clearRequests()V
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 504
    return-void
.end method

.method public errorLog(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 455
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 456
    .local v0, "tid":J
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/exchange/AbstractSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    return-void
.end method

.method public getCommand()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mASCmd:Ljava/lang/String;

    return-object v0
.end method

.method public getSynchronizer()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mSynchronizer:Ljava/lang/Object;

    return-object v0
.end method

.method public getThreadId()J
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 172
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public hasConnectivity()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    .line 467
    sget-object v6, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    const-string v7, "connectivity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 469
    .local v0, "cm":Landroid/net/ConnectivityManager;
    const/4 v3, 0x0

    .local v3, "tries":I
    move v4, v3

    .line 470
    .end local v3    # "tries":I
    .local v4, "tries":I
    :goto_0
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "tries":I
    .restart local v3    # "tries":I
    if-ge v4, v5, :cond_1

    .line 471
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 472
    .local v1, "info":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 473
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    .line 474
    .local v2, "state":Landroid/net/NetworkInfo$DetailedState;
    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v2, v6, :cond_0

    .line 483
    .end local v1    # "info":Landroid/net/NetworkInfo;
    .end local v2    # "state":Landroid/net/NetworkInfo$DetailedState;
    :goto_1
    return v5

    .line 479
    .restart local v1    # "info":Landroid/net/NetworkInfo;
    :cond_0
    const-wide/16 v6, 0x2710

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    move v4, v3

    .line 482
    .end local v3    # "tries":I
    .restart local v4    # "tries":I
    goto :goto_0

    .line 483
    .end local v1    # "info":Landroid/net/NetworkInfo;
    .end local v4    # "tries":I
    .restart local v3    # "tries":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 480
    .restart local v1    # "info":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v6

    goto :goto_2
.end method

.method public hasPendingRequests()Z
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/android/exchange/AbstractSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInitialSyncThread()Z
    .locals 1

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/android/exchange/AbstractSyncService;->mIsInitialSyncThread:Z

    return v0
.end method

.method public isStopped()Z
    .locals 1

    .prologue
    .line 387
    iget-boolean v0, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    return v0
.end method

.method public abstract reset()V
.end method

.method public abstract stop()V
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 275
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "|"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mailboxId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSyncReason="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mSyncRetryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/AbstractSyncService;->mSyncRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mExitStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mStop="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/AbstractSyncService;->mStop:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 284
    :goto_0
    return-object v1

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 284
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public userLog(Ljava/lang/String;I)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "code"    # I

    .prologue
    .line 405
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 406
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    .line 408
    :cond_0
    return-void
.end method

.method public userLog(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "code"    # I
    .param p3, "string2"    # Ljava/lang/String;

    .prologue
    .line 399
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 400
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/AbstractSyncService;->userLog([Ljava/lang/String;)V

    .line 402
    :cond_0
    return-void
.end method

.method public userLog(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 412
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    .line 413
    .local v0, "tid":J
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_0

    .line 414
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/exchange/AbstractSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 419
    :goto_0
    return-void

    .line 416
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/exchange/AbstractSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public varargs userLog([Ljava/lang/String;)V
    .locals 10
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    .line 429
    sget-boolean v8, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v8, :cond_0

    .line 430
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    .line 433
    .local v6, "tid":J
    array-length v8, p1

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 434
    const/4 v8, 0x0

    aget-object v3, p1, v8

    .line 443
    .local v3, "logText":Ljava/lang/String;
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/AbstractSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    .end local v3    # "logText":Ljava/lang/String;
    .end local v6    # "tid":J
    :cond_0
    return-void

    .line 436
    .restart local v6    # "tid":J
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v8, 0x40

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 437
    .local v4, "sb":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v5, v0, v1

    .line 438
    .local v5, "string":Ljava/lang/String;
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 440
    .end local v5    # "string":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "logText":Ljava/lang/String;
    goto :goto_0
.end method

.method public abstract validateAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
.end method
