.class Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;
.super Ljava/lang/Object;
.source "ExchangeDirectoryProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/provider/ExchangeDirectoryProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecipientProjectionFlags"
.end annotation


# instance fields
.field mAccountIdFlag:Z

.field mAvailabilityStatusFlag:Z

.field mCertificateFlag:Z

.field mCertificateRetrieval:I

.field mCertificateStatusFlag:Z

.field mDisplayNameFlag:Z

.field mEmailFlag:Z

.field mIdFlag:Z

.field mMergedFreeBusyFlag:Z

.field mMiniCertificateFlag:Z

.field mProjectionStrs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mResponseStatusFlag:Z

.field mToFlag:Z

.field final synthetic this$0:Lcom/android/exchange/provider/ExchangeDirectoryProvider;


# direct methods
.method private constructor <init>(Lcom/android/exchange/provider/ExchangeDirectoryProvider;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 728
    iput-object p1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->this$0:Lcom/android/exchange/provider/ExchangeDirectoryProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 729
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateRetrieval:I

    .line 730
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mIdFlag:Z

    .line 731
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAccountIdFlag:Z

    .line 732
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mToFlag:Z

    .line 733
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mResponseStatusFlag:Z

    .line 734
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mDisplayNameFlag:Z

    .line 735
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mEmailFlag:Z

    .line 736
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateStatusFlag:Z

    .line 737
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateFlag:Z

    .line 738
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMiniCertificateFlag:Z

    .line 739
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAvailabilityStatusFlag:Z

    .line 740
    iput-boolean v1, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMergedFreeBusyFlag:Z

    .line 742
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mProjectionStrs:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/exchange/provider/ExchangeDirectoryProvider;Lcom/android/exchange/provider/ExchangeDirectoryProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/exchange/provider/ExchangeDirectoryProvider;
    .param p2, "x1"    # Lcom/android/exchange/provider/ExchangeDirectoryProvider$1;

    .prologue
    .line 728
    invoke-direct {p0, p1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;-><init>(Lcom/android/exchange/provider/ExchangeDirectoryProvider;)V

    return-void
.end method
