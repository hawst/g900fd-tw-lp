.class public Lcom/android/exchange/provider/GalResult;
.super Ljava/lang/Object;
.source "GalResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/provider/GalResult$1;,
        Lcom/android/exchange/provider/GalResult$GalData;
    }
.end annotation


# instance fields
.field public endRange:I

.field public galData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/provider/GalResult$GalData;",
            ">;"
        }
    .end annotation
.end field

.field public protocolVerison:D

.field public startRange:I

.field public total:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v0, p0, Lcom/android/exchange/provider/GalResult;->startRange:I

    .line 34
    iput v0, p0, Lcom/android/exchange/provider/GalResult;->endRange:I

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/provider/GalResult;->galData:Ljava/util/ArrayList;

    .line 47
    return-void
.end method


# virtual methods
.method public addGalData(JLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "displayName"    # Ljava/lang/String;
    .param p4, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/exchange/provider/GalResult;->galData:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/exchange/provider/GalResult$GalData;

    const/4 v6, 0x0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/android/exchange/provider/GalResult$GalData;-><init>(JLjava/lang/String;Ljava/lang/String;Lcom/android/exchange/provider/GalResult$1;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    return-void
.end method

.method public addGalData(Lcom/android/exchange/provider/GalResult$GalData;)V
    .locals 1
    .param p1, "data"    # Lcom/android/exchange/provider/GalResult$GalData;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/exchange/provider/GalResult;->galData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method
