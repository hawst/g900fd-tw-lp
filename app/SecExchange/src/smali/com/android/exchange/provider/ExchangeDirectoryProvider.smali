.class public Lcom/android/exchange/provider/ExchangeDirectoryProvider;
.super Landroid/content/ContentProvider;
.source "ExchangeDirectoryProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/provider/ExchangeDirectoryProvider$1;,
        Lcom/android/exchange/provider/ExchangeDirectoryProvider$MatrixCursorExtras;,
        Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;,
        Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;,
        Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;
    }
.end annotation


# static fields
.field public static final EMAIL_SEARCH_URI:Landroid/net/Uri;

.field public static final RESOLVERECIPIENTS_URI:Landroid/net/Uri;

.field public static final RIC_URI:Landroid/net/Uri;

.field public static final VALIDATE_CERT_URI:Landroid/net/Uri;

.field static final mAccountIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# instance fields
.field final RIC_PROJECTION:[Ljava/lang/String;

.field protected mContentResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 87
    const-string v0, "content://com.android.exchange.directory.provider/emailsearch/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->EMAIL_SEARCH_URI:Landroid/net/Uri;

    .line 91
    const-string v0, "content://com.android.exchange.directory.provider/resolverecipients/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->RESOLVERECIPIENTS_URI:Landroid/net/Uri;

    .line 95
    const-string v0, "content://com.android.exchange.directory.provider/recipientInformation cache/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->RIC_URI:Landroid/net/Uri;

    .line 128
    const-string v0, "content://com.android.exchange.directory.provider/validatecert/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->VALIDATE_CERT_URI:Landroid/net/Uri;

    .line 152
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    .line 155
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "directories"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 156
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "contacts/filter/*"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 157
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "contacts/lookup/*/entities"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "contacts/lookup/*/#/entities"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 161
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "data/emails/filter/*"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 164
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "resolverecipients"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 168
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "validatecert"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 171
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "emailsearch/*/*/*/*/*/*/*/*"

    const/16 v3, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 175
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "recipientInformation cache/*/*"

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 179
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.exchange.directory.provider"

    const-string v2, "contacts/lookup/*/#"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 144
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "alias"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "email_address"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "accountkey"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "fileas"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "weightedrank"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->RIC_PROJECTION:[Ljava/lang/String;

    .line 1105
    return-void
.end method

.method private static addR2DataRow(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZZZZZZZZZZ)V
    .locals 3
    .param p0, "mc"    # Landroid/database/MatrixCursor;
    .param p1, "id"    # J
    .param p3, "accountId"    # Ljava/lang/String;
    .param p4, "to"    # Ljava/lang/String;
    .param p5, "responseStatus"    # B
    .param p6, "displayName"    # Ljava/lang/String;
    .param p7, "email"    # Ljava/lang/String;
    .param p8, "certificateStatus"    # B
    .param p9, "certificate"    # Ljava/lang/String;
    .param p10, "miniCertificate"    # Ljava/lang/String;
    .param p11, "availabilityStatus"    # I
    .param p12, "mergedFreeBusy"    # Ljava/lang/String;
    .param p13, "idFlag"    # Z
    .param p14, "accountIdFlag"    # Z
    .param p15, "toFlag"    # Z
    .param p16, "responseStatusFlag"    # Z
    .param p17, "displayNameFlag"    # Z
    .param p18, "emailFlag"    # Z
    .param p19, "certificateStatusFlag"    # Z
    .param p20, "certificateFlag"    # Z
    .param p21, "miniCertificateFlag"    # Z
    .param p22, "availabilityStatusFlag"    # Z
    .param p23, "mergedFreeBusyFlag"    # Z

    .prologue
    .line 229
    invoke-virtual {p0}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v0

    .line 230
    .local v0, "rb":Landroid/database/MatrixCursor$RowBuilder;
    if-eqz p13, :cond_0

    .line 231
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 232
    :cond_0
    if-eqz p14, :cond_1

    .line 233
    invoke-virtual {v0, p3}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 234
    :cond_1
    if-eqz p15, :cond_2

    .line 235
    invoke-virtual {v0, p4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 236
    :cond_2
    if-eqz p16, :cond_3

    .line 237
    invoke-static {p5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 238
    :cond_3
    if-eqz p17, :cond_4

    .line 239
    invoke-virtual {v0, p6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 240
    :cond_4
    if-eqz p18, :cond_5

    .line 241
    invoke-virtual {v0, p7}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 242
    :cond_5
    if-eqz p19, :cond_6

    .line 243
    invoke-static {p8}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 248
    :cond_6
    if-eqz p20, :cond_7

    .line 249
    invoke-virtual {v0, p9}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 250
    :cond_7
    if-eqz p21, :cond_8

    .line 251
    invoke-virtual {v0, p10}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 252
    :cond_8
    if-eqz p22, :cond_9

    .line 253
    invoke-static {p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 254
    :cond_9
    if-eqz p23, :cond_a

    .line 255
    invoke-virtual {v0, p12}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 256
    :cond_a
    return-void
.end method

.method private buildResolveRecipients([Ljava/lang/String;Ljava/util/ArrayList;ZZZZZZZZZZZLjava/lang/String;)Landroid/database/Cursor;
    .locals 44
    .param p1, "projs"    # [Ljava/lang/String;
    .param p3, "idFlag"    # Z
    .param p4, "accountIdFlag"    # Z
    .param p5, "toFlag"    # Z
    .param p6, "responseStatusFlag"    # Z
    .param p7, "displayNameFlag"    # Z
    .param p8, "emailFlag"    # Z
    .param p9, "certificateStatusFlag"    # Z
    .param p10, "certificateFlag"    # Z
    .param p11, "miniCertificateFlag"    # Z
    .param p12, "availabilityStatusFlag"    # Z
    .param p13, "mergedFreeBusyFlag"    # Z
    .param p14, "accountIdStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/provider/RRResponse;",
            ">;ZZZZZZZZZZZ",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 1018
    .local p2, "r2Result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/provider/RRResponse;>;"
    new-instance v3, Lcom/android/exchange/provider/ExchangeDirectoryProvider$MatrixCursorExtras;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$MatrixCursorExtras;-><init>([Ljava/lang/String;)V

    .line 1020
    .local v3, "c":Lcom/android/exchange/provider/ExchangeDirectoryProvider$MatrixCursorExtras;
    if-eqz p2, :cond_4

    .line 1021
    const/4 v11, 0x0

    .line 1024
    .local v11, "certStatus":B
    const/4 v12, 0x0

    .line 1025
    .local v12, "certificate":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1026
    .local v13, "miniCertificate":Ljava/lang/String;
    const/16 v39, 0x0

    .line 1028
    .local v39, "counter":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_0
    :goto_0
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/android/exchange/provider/RRResponse;

    .line 1029
    .local v43, "responseData":Lcom/android/exchange/provider/RRResponse;
    move-object/from16 v0, v43

    iget-byte v4, v0, Lcom/android/exchange/provider/RRResponse;->status:B

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    .line 1030
    move-object/from16 v0, v43

    iget-object v4, v0, Lcom/android/exchange/provider/RRResponse;->recipientData:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v41

    .local v41, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/android/exchange/provider/RRResponse$RecipientData;

    .line 1032
    .local v42, "recipientData":Lcom/android/exchange/provider/RRResponse$RecipientData;
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mCertificates:Lcom/android/exchange/provider/RecipientCertificates;

    if-eqz v4, :cond_1

    .line 1033
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mCertificates:Lcom/android/exchange/provider/RecipientCertificates;

    iget-byte v11, v4, Lcom/android/exchange/provider/RecipientCertificates;->mStatus:B

    .line 1038
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mCertificates:Lcom/android/exchange/provider/RecipientCertificates;

    iget-object v12, v4, Lcom/android/exchange/provider/RecipientCertificates;->mCertificate:Ljava/lang/String;

    .line 1039
    move-object/from16 v0, v42

    iget-object v4, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mCertificates:Lcom/android/exchange/provider/RecipientCertificates;

    iget-object v13, v4, Lcom/android/exchange/provider/RecipientCertificates;->mMiniCertificate:Ljava/lang/String;

    .line 1048
    :goto_2
    move/from16 v0, v39

    int-to-long v4, v0

    move-object/from16 v0, v43

    iget-object v7, v0, Lcom/android/exchange/provider/RRResponse;->to:Ljava/lang/String;

    move-object/from16 v0, v43

    iget-byte v8, v0, Lcom/android/exchange/provider/RRResponse;->status:B

    move-object/from16 v0, v42

    iget-object v9, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mDisplayName:Ljava/lang/String;

    move-object/from16 v0, v42

    iget-object v10, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mEmail:Ljava/lang/String;

    move-object/from16 v0, v42

    iget v14, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mAvailabilityStatus:I

    move-object/from16 v0, v42

    iget-object v15, v0, Lcom/android/exchange/provider/RRResponse$RecipientData;->mMergedFreeBusy:Ljava/lang/String;

    move-object/from16 v6, p14

    move/from16 v16, p3

    move/from16 v17, p4

    move/from16 v18, p5

    move/from16 v19, p6

    move/from16 v20, p7

    move/from16 v21, p8

    move/from16 v22, p9

    move/from16 v23, p10

    move/from16 v24, p11

    move/from16 v25, p12

    move/from16 v26, p13

    invoke-static/range {v3 .. v26}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->addR2DataRow(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZZZZZZZZZZ)V

    .line 1069
    add-int/lit8 v39, v39, 0x1

    .line 1070
    goto :goto_1

    .line 1041
    :cond_1
    const/4 v11, 0x0

    .line 1044
    const/4 v12, 0x0

    .line 1045
    const/4 v13, 0x0

    goto :goto_2

    .line 1073
    .end local v41    # "i$":Ljava/util/Iterator;
    .end local v42    # "recipientData":Lcom/android/exchange/provider/RRResponse$RecipientData;
    :cond_2
    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/android/exchange/provider/RRResponse;->to:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v43

    iget-byte v0, v0, Lcom/android/exchange/provider/RRResponse;->status:B

    move/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object v15, v3

    move-object/from16 v18, p14

    move/from16 v28, p3

    move/from16 v29, p4

    move/from16 v30, p5

    move/from16 v31, p6

    move/from16 v32, p7

    move/from16 v33, p8

    move/from16 v34, p9

    move/from16 v35, p10

    move/from16 v36, p11

    move/from16 v37, p12

    move/from16 v38, p13

    invoke-static/range {v15 .. v38}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->addR2DataRow(Landroid/database/MatrixCursor;JLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZZZZZZZZZZZ)V

    .line 1090
    add-int/lit8 v39, v39, 0x1

    goto/16 :goto_0

    .line 1094
    .end local v43    # "responseData":Lcom/android/exchange/provider/RRResponse;
    :cond_3
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1095
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "com.android.exchange.provider.TOTAL_RESULTS"

    move/from16 v0, v39

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1096
    invoke-virtual {v3, v2}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$MatrixCursorExtras;->setExtras(Landroid/os/Bundle;)V

    .line 1099
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v11    # "certStatus":B
    .end local v12    # "certificate":Ljava/lang/String;
    .end local v13    # "miniCertificate":Ljava/lang/String;
    .end local v39    # "counter":I
    :cond_4
    return-object v3
.end method

.method public static deleteAccountFromMap(Ljava/lang/String;)V
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 185
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "ExchangeDirectoryProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "==>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from mAccountIdMap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    :cond_0
    return-void
.end method

.method private normalizeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "inVal"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x27

    .line 1350
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 1351
    .local v0, "len":I
    move-object v1, p1

    .line 1352
    .local v1, "retVal":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, v3, :cond_0

    .line 1353
    const/4 v2, 0x1

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1355
    :cond_0
    return-object v1
.end method

.method private parseRRProjectionList(Ljava/util/List;)Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;"
        }
    .end annotation

    .prologue
    .local p1, "projectionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 746
    new-instance v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;-><init>(Lcom/android/exchange/provider/ExchangeDirectoryProvider;Lcom/android/exchange/provider/ExchangeDirectoryProvider$1;)V

    .line 747
    .local v0, "flags":Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 748
    .local v1, "projectionStrs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "_id"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 749
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mIdFlag:Z

    .line 750
    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 752
    :cond_0
    const-string v2, "accountId"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 753
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAccountIdFlag:Z

    .line 754
    const-string v2, "accountId"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 756
    :cond_1
    const-string v2, "to"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 757
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mToFlag:Z

    .line 758
    const-string v2, "to"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 760
    :cond_2
    const-string v2, "responseStatus"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 761
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mResponseStatusFlag:Z

    .line 762
    const-string v2, "responseStatus"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    :cond_3
    const-string v2, "displayName"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 765
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mDisplayNameFlag:Z

    .line 766
    const-string v2, "displayName"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 768
    :cond_4
    const-string v2, "email"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 769
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mEmailFlag:Z

    .line 770
    const-string v2, "email"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    :cond_5
    const-string v2, "certificateStatus"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 773
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateStatusFlag:Z

    .line 774
    const-string v2, "certificateStatus"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    :cond_6
    const-string v2, "certificate"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 777
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateFlag:Z

    .line 778
    const-string v2, "certificate"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    iput v4, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateRetrieval:I

    .line 783
    :cond_7
    const-string v2, "miniCertificate"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 784
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMiniCertificateFlag:Z

    .line 785
    const-string v2, "miniCertificate"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    iget v2, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateRetrieval:I

    if-eq v2, v4, :cond_8

    .line 789
    const/4 v2, 0x3

    iput v2, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateRetrieval:I

    .line 792
    :cond_8
    const-string v2, "availabilityStatus"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 793
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAvailabilityStatusFlag:Z

    .line 794
    const-string v2, "availabilityStatus"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 796
    :cond_9
    const-string v2, "mergedFreeBusy"

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 797
    iput-boolean v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMergedFreeBusyFlag:Z

    .line 798
    const-string v2, "mergedFreeBusy"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    :cond_a
    iput-object v1, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mProjectionStrs:Ljava/util/ArrayList;

    .line 803
    return-object v0
.end method

.method private queryDirectory([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 21
    .param p1, "projection"    # [Ljava/lang/String;

    .prologue
    .line 512
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v19

    const-string v20, "com.android.exchange"

    invoke-virtual/range {v19 .. v20}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    .line 515
    .local v5, "accounts":[Landroid/accounts/Account;
    new-instance v11, Landroid/database/MatrixCursor;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 517
    .local v11, "cursor":Landroid/database/MatrixCursor;
    const-string v19, "[ExchagneDirectoryProvider]"

    const-string v20, "start query GAL_DIRECTORIES "

    invoke-static/range {v19 .. v20}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 524
    .local v10, "context":Landroid/content/Context;
    if-eqz v5, :cond_a

    .line 525
    move-object v6, v5

    .local v6, "arr$":[Landroid/accounts/Account;
    array-length v0, v6

    move/from16 v17, v0

    .local v17, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_0
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_a

    aget-object v2, v6, v16

    .line 551
    .local v2, "account":Landroid/accounts/Account;
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    .line 552
    .local v18, "row":[Ljava/lang/Object;
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v15, v0, :cond_9

    .line 553
    aget-object v9, p1, v15

    .line 554
    .local v9, "column":Ljava/lang/String;
    const-string v19, "accountName"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 555
    iget-object v0, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v18, v15

    .line 552
    :cond_0
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 556
    :cond_1
    const-string v19, "accountType"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 557
    iget-object v0, v2, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v18, v15

    goto :goto_2

    .line 558
    :cond_2
    const-string v19, "typeResourceId"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 559
    const/4 v8, 0x0

    .line 561
    .local v8, "bundle":Landroid/os/Bundle;
    :try_start_0
    const-string v4, "com.android.exchange"

    .line 562
    .local v4, "accountType":Ljava/lang/String;
    new-instance v19, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v19

    invoke-direct {v0, v10}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/android/emailcommon/service/AccountServiceProxy;->getConfigurationData(Ljava/lang/String;)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 572
    .end local v4    # "accountType":Ljava/lang/String;
    :cond_3
    :goto_3
    const v13, 0x7f060001

    .line 573
    .local v13, "exchangeName":I
    if-eqz v8, :cond_4

    const-string v19, "com.android.email.EXCHANGE_CONFIGURATION_USE_ALTERNATE_STRINGS"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    if-nez v19, :cond_4

    .line 578
    const/high16 v13, 0x7f060000

    .line 580
    :cond_4
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v18, v15

    goto :goto_2

    .line 564
    .end local v13    # "exchangeName":I
    :catch_0
    move-exception v12

    .line 565
    .local v12, "e":Landroid/os/RemoteException;
    const-string v19, "ExchangeDirectoryProvider."

    const-string v20, "Exception Caughted"

    invoke-static/range {v19 .. v20}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    if-eqz v12, :cond_3

    .line 567
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 581
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v12    # "e":Landroid/os/RemoteException;
    :cond_5
    const-string v19, "displayName"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 586
    iget-object v3, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 587
    .local v3, "accountName":Ljava/lang/String;
    const/16 v19, 0x40

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 588
    .local v7, "atIndex":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v7, v0, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x2

    move/from16 v0, v19

    if-ge v7, v0, :cond_6

    .line 589
    add-int/lit8 v19, v7, 0x1

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v14

    .line 591
    .local v14, "firstLetter":C
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v20, v7, 0x2

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v18, v15

    goto/16 :goto_2

    .line 593
    .end local v14    # "firstLetter":C
    :cond_6
    iget-object v0, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v18, v15

    goto/16 :goto_2

    .line 595
    .end local v3    # "accountName":Ljava/lang/String;
    .end local v7    # "atIndex":I
    :cond_7
    const-string v19, "exportSupport"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 596
    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v18, v15

    goto/16 :goto_2

    .line 597
    :cond_8
    const-string v19, "shortcutSupport"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 598
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v18, v15

    goto/16 :goto_2

    .line 601
    .end local v9    # "column":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 525
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 604
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v6    # "arr$":[Landroid/accounts/Account;
    .end local v15    # "i":I
    .end local v16    # "i$":I
    .end local v17    # "len$":I
    .end local v18    # "row":[Ljava/lang/Object;
    :cond_a
    return-object v11
.end method

.method private queryGAL(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 23
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 608
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 611
    .local v8, "filter":Ljava/lang/String;
    const-string v5, "[ExchagneDirectoryProvider]"

    const-string v11, "query GAL_EMAIL_FILTER "

    invoke-static {v5, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v11, 0x2

    if-ge v5, v11, :cond_1

    .line 613
    :cond_0
    const/4 v5, 0x0

    .line 671
    :goto_0
    return-object v5

    .line 615
    :cond_1
    const-string v5, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 616
    .local v4, "accountName":Ljava/lang/String;
    if-nez v4, :cond_2

    .line 617
    const/4 v5, 0x0

    goto :goto_0

    .line 620
    :cond_2
    const-string v5, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 621
    .local v21, "limitString":Ljava/lang/String;
    move-object/from16 v22, p3

    .line 622
    .local v22, "startString":Ljava/lang/String;
    const/16 v10, 0x14

    .line 623
    .local v10, "limit":I
    if-eqz v21, :cond_3

    .line 625
    :try_start_0
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 629
    :goto_1
    if-gtz v10, :cond_3

    .line 630
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Limit not valid: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 626
    :catch_0
    move-exception v17

    .line 627
    .local v17, "e":Ljava/lang/NumberFormatException;
    const/4 v10, 0x0

    goto :goto_1

    .line 633
    .end local v17    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v18

    .line 634
    .local v18, "callingId":J
    const/4 v9, 0x0

    .line 635
    .local v9, "startindex":I
    if-eqz p4, :cond_4

    .line 637
    const/4 v5, 0x0

    :try_start_1
    aget-object v5, p4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    .line 644
    :cond_4
    :goto_2
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getAccountIdByName(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v6

    .line 645
    .local v6, "accountId":J
    const-string v5, "ExchangeDirectoryProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "accountName:accountId -->"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 646
    const-wide/16 v12, -0x1

    cmp-long v5, v6, v12

    if-nez v5, :cond_5

    .line 648
    const/4 v5, 0x0

    .line 668
    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 638
    .end local v6    # "accountId":J
    :catch_1
    move-exception v17

    .line 639
    .restart local v17    # "e":Ljava/lang/NumberFormatException;
    const/4 v9, 0x0

    goto :goto_2

    .line 652
    .end local v17    # "e":Ljava/lang/NumberFormatException;
    .restart local v6    # "accountId":J
    :cond_5
    :try_start_3
    const-string v5, "[ExchagneDirectoryProvider]"

    const-string v11, "Before EasSyncService.searchGal "

    invoke-static {v5, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const/16 v20, 0x0

    .line 654
    .local v20, "galResult":Lcom/android/exchange/provider/GalResult;
    if-eqz v22, :cond_6

    if-eqz p4, :cond_6

    .line 655
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static/range {v5 .. v10}, Lcom/android/exchange/EasSyncService;->searchGal(Landroid/content/Context;JLjava/lang/String;II)Lcom/android/exchange/provider/GalResult;

    move-result-object v20

    .line 661
    :goto_3
    if-eqz v20, :cond_7

    .line 662
    const-string v5, "[ExchagneDirectoryProvider]"

    const-string v11, "EasSyncService.searchGal galResult != null "

    invoke-static {v5, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2, v9}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->buildGalResultCursor([Ljava/lang/String;Lcom/android/exchange/provider/GalResult;I)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v5

    .line 668
    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 658
    :cond_6
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v11

    const/4 v15, 0x0

    move-wide v12, v6

    move-object v14, v8

    move/from16 v16, v10

    invoke-static/range {v11 .. v16}, Lcom/android/exchange/EasSyncService;->searchGal(Landroid/content/Context;JLjava/lang/String;II)Lcom/android/exchange/provider/GalResult;

    move-result-object v20

    goto :goto_3

    .line 666
    :cond_7
    const-string v5, "[ExchagneDirectoryProvider]"

    const-string v11, "galResult is null "

    invoke-static {v5, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 668
    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 671
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 668
    .end local v6    # "accountId":J
    .end local v20    # "galResult":Lcom/android/exchange/provider/GalResult;
    :catchall_0
    move-exception v5

    invoke-static/range {v18 .. v19}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v5
.end method

.method private queryGALContact(Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 24
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "match"    # I

    .prologue
    .line 677
    const/16 v19, 0x0

    .line 681
    .local v19, "cursor":Landroid/database/MatrixCursor;
    const-string v9, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 682
    .local v7, "accountName":Ljava/lang/String;
    const-string v9, "[ExchagneDirectoryProvider]"

    const-string v10, "Gal query GAL_CONTACT_WITH_ID "

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    if-nez v7, :cond_0

    .line 684
    const/4 v2, 0x0

    move-object v9, v2

    move-object/from16 v2, v19

    .line 725
    .end local v19    # "cursor":Landroid/database/MatrixCursor;
    .local v2, "cursor":Landroid/database/MatrixCursor;
    :goto_0
    return-object v9

    .line 686
    .end local v2    # "cursor":Landroid/database/MatrixCursor;
    .restart local v19    # "cursor":Landroid/database/MatrixCursor;
    :cond_0
    new-instance v3, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;-><init>([Ljava/lang/String;)V

    .line 688
    .local v3, "galProjection":Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;
    :try_start_0
    new-instance v2, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    .end local v19    # "cursor":Landroid/database/MatrixCursor;
    .restart local v2    # "cursor":Landroid/database/MatrixCursor;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v22

    .line 692
    .local v22, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x2

    move-object/from16 v0, v22

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 693
    .local v6, "lookupKey":Ljava/lang/String;
    const/4 v9, 0x3

    move/from16 v0, p3

    if-ne v0, v9, :cond_1

    const/4 v9, 0x3

    move-object/from16 v0, v22

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 695
    .local v4, "contactId":J
    :goto_1
    new-instance v23, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, v23

    invoke-direct {v0, v6}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 696
    .local v23, "ps":Lcom/android/emailcommon/mail/PackedString;
    const-string v9, "displayName"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 697
    .local v8, "displayName":Ljava/lang/String;
    const-string v9, "emailAddress"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addEmailAddress(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    const/16 v17, 0x1

    const-string v9, "homePhone"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object v10, v2

    move-object v11, v3

    move-wide v12, v4

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v8

    invoke-static/range {v10 .. v18}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addPhoneRow(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 701
    const/16 v17, 0x3

    const-string v9, "workPhone"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object v10, v2

    move-object v11, v3

    move-wide v12, v4

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v8

    invoke-static/range {v10 .. v18}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addPhoneRow(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 703
    const/16 v17, 0x2

    const-string v9, "mobilePhone"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object v10, v2

    move-object v11, v3

    move-wide v12, v4

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v8

    invoke-static/range {v10 .. v18}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addPhoneRow(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 711
    const-string v9, "firstName"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v9, "lastName"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object v10, v2

    move-object v11, v3

    move-wide v12, v4

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v16, v8

    invoke-static/range {v10 .. v18}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addNameRow(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    const-string v9, "firstName"

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object v10, v2

    move-object v11, v3

    move-wide v12, v4

    move-object v14, v7

    move-object v15, v8

    move-object/from16 v17, v23

    invoke-static/range {v10 .. v17}, Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalContactRow;->addDataRow(Landroid/database/MatrixCursor;Lcom/android/exchange/provider/ExchangeDirectoryProvider$GalProjection;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/emailcommon/mail/PackedString;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .end local v4    # "contactId":J
    .end local v6    # "lookupKey":Ljava/lang/String;
    .end local v8    # "displayName":Ljava/lang/String;
    .end local v22    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v23    # "ps":Lcom/android/emailcommon/mail/PackedString;
    :goto_2
    move-object v9, v2

    .line 725
    goto/16 :goto_0

    .line 693
    .restart local v6    # "lookupKey":Ljava/lang/String;
    .restart local v22    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-wide/16 v4, 0x1

    goto/16 :goto_1

    .line 716
    .end local v2    # "cursor":Landroid/database/MatrixCursor;
    .end local v6    # "lookupKey":Ljava/lang/String;
    .end local v22    # "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v19    # "cursor":Landroid/database/MatrixCursor;
    :catch_0
    move-exception v20

    move-object/from16 v2, v19

    .line 718
    .end local v19    # "cursor":Landroid/database/MatrixCursor;
    .restart local v2    # "cursor":Landroid/database/MatrixCursor;
    .local v20, "e1":Ljava/lang/Exception;
    :goto_3
    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Landroid/database/MatrixCursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 722
    :cond_2
    :goto_4
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 719
    :catch_1
    move-exception v21

    .line 720
    .local v21, "e2":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 716
    .end local v20    # "e1":Ljava/lang/Exception;
    .end local v21    # "e2":Ljava/lang/Exception;
    :catch_2
    move-exception v20

    goto :goto_3
.end method

.method private queryRIC(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 17
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 430
    const/4 v15, 0x0

    .line 432
    .local v15, "ric":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 433
    .local v7, "acckey":Ljava/lang/String;
    const-string v4, "accountkey=?"

    .line 434
    .local v4, "selection1":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v7, v5, v1

    .line 437
    .local v5, "selectionArgs1":[Ljava/lang/String;
    const-string v6, "weightedrank DESC "

    .line 438
    .local v6, "sortOrder1":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 440
    .local v12, "filter":Ljava/lang/String;
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-gtz v1, :cond_2

    .line 441
    :cond_0
    const/4 v13, 0x0

    .line 499
    if-eqz v15, :cond_1

    .line 500
    :try_start_1
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7

    .line 503
    .end local v4    # "selection1":Ljava/lang/String;
    .end local v5    # "selectionArgs1":[Ljava/lang/String;
    .end local v6    # "sortOrder1":Ljava/lang/String;
    .end local v7    # "acckey":Ljava/lang/String;
    .end local v12    # "filter":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v13

    .line 443
    .restart local v4    # "selection1":Ljava/lang/String;
    .restart local v5    # "selectionArgs1":[Ljava/lang/String;
    .restart local v6    # "sortOrder1":Ljava/lang/String;
    .restart local v7    # "acckey":Ljava/lang/String;
    .restart local v12    # "filter":Ljava/lang/String;
    :cond_2
    :try_start_2
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v12}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 445
    .local v2, "uri_ric":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 446
    .local v8, "context":Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mContentResolver:Landroid/content/ContentResolver;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 448
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->RIC_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 451
    if-eqz v15, :cond_3

    .line 452
    const-string v1, "[ExchagneDirectoryProvider]"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Ri email address"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 458
    :cond_3
    :goto_1
    if-eqz v15, :cond_b

    :try_start_4
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_b

    .line 459
    new-instance v13, Landroid/database/MatrixCursor;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 460
    .local v13, "newCursor":Landroid/database/MatrixCursor;
    const/4 v1, -0x1

    invoke-interface {v15, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 463
    const/4 v14, 0x0

    .line 466
    .local v14, "record":I
    :goto_2
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0xa

    if-gt v14, v1, :cond_6

    .line 467
    const-string v1, "fileas"

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 470
    .local v9, "displayName":Ljava/lang/String;
    if-eqz v9, :cond_4

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_5

    .line 471
    :cond_4
    const-string v1, "alias"

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 474
    :cond_5
    const-string v1, "email_address"

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 476
    .local v11, "emailAddress":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v9, v1, v3

    const/4 v3, 0x1

    aput-object v11, v1, v3

    invoke-virtual {v13, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 479
    add-int/lit8 v14, v14, 0x1

    .line 480
    goto :goto_2

    .line 454
    .end local v9    # "displayName":Ljava/lang/String;
    .end local v11    # "emailAddress":Ljava/lang/String;
    .end local v13    # "newCursor":Landroid/database/MatrixCursor;
    .end local v14    # "record":I
    :catch_0
    move-exception v10

    .line 455
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 494
    .end local v2    # "uri_ric":Landroid/net/Uri;
    .end local v4    # "selection1":Ljava/lang/String;
    .end local v5    # "selectionArgs1":[Ljava/lang/String;
    .end local v6    # "sortOrder1":Ljava/lang/String;
    .end local v7    # "acckey":Ljava/lang/String;
    .end local v8    # "context":Landroid/content/Context;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v12    # "filter":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 495
    .restart local v10    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 496
    const/4 v13, 0x0

    .line 499
    if-eqz v15, :cond_1

    .line 500
    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 502
    :catch_2
    move-exception v1

    goto/16 :goto_0

    .line 481
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v2    # "uri_ric":Landroid/net/Uri;
    .restart local v4    # "selection1":Ljava/lang/String;
    .restart local v5    # "selectionArgs1":[Ljava/lang/String;
    .restart local v6    # "sortOrder1":Ljava/lang/String;
    .restart local v7    # "acckey":Ljava/lang/String;
    .restart local v8    # "context":Landroid/content/Context;
    .restart local v12    # "filter":Ljava/lang/String;
    .restart local v13    # "newCursor":Landroid/database/MatrixCursor;
    .restart local v14    # "record":I
    :cond_6
    if-lez v14, :cond_7

    if-eqz v13, :cond_7

    :try_start_7
    invoke-virtual {v13}, Landroid/database/MatrixCursor;->getCount()I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v1

    if-gtz v1, :cond_a

    .line 482
    :cond_7
    if-eqz v13, :cond_8

    .line 484
    :try_start_8
    invoke-virtual {v13}, Landroid/database/MatrixCursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 489
    :cond_8
    :goto_3
    const/4 v13, 0x0

    .line 499
    .end local v13    # "newCursor":Landroid/database/MatrixCursor;
    if-eqz v15, :cond_1

    .line 500
    :try_start_9
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 502
    :catch_3
    move-exception v1

    goto/16 :goto_0

    .line 485
    .restart local v13    # "newCursor":Landroid/database/MatrixCursor;
    :catch_4
    move-exception v10

    .line 486
    .restart local v10    # "e":Ljava/lang/Exception;
    :try_start_a
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_3

    .line 498
    .end local v2    # "uri_ric":Landroid/net/Uri;
    .end local v4    # "selection1":Ljava/lang/String;
    .end local v5    # "selectionArgs1":[Ljava/lang/String;
    .end local v6    # "sortOrder1":Ljava/lang/String;
    .end local v7    # "acckey":Ljava/lang/String;
    .end local v8    # "context":Landroid/content/Context;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v12    # "filter":Ljava/lang/String;
    .end local v13    # "newCursor":Landroid/database/MatrixCursor;
    .end local v14    # "record":I
    :catchall_0
    move-exception v1

    .line 499
    if-eqz v15, :cond_9

    .line 500
    :try_start_b
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8

    .line 503
    :cond_9
    :goto_4
    throw v1

    .line 499
    .restart local v2    # "uri_ric":Landroid/net/Uri;
    .restart local v4    # "selection1":Ljava/lang/String;
    .restart local v5    # "selectionArgs1":[Ljava/lang/String;
    .restart local v6    # "sortOrder1":Ljava/lang/String;
    .restart local v7    # "acckey":Ljava/lang/String;
    .restart local v8    # "context":Landroid/content/Context;
    .restart local v12    # "filter":Ljava/lang/String;
    .restart local v13    # "newCursor":Landroid/database/MatrixCursor;
    .restart local v14    # "record":I
    :cond_a
    if-eqz v15, :cond_1

    .line 500
    :try_start_c
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    goto/16 :goto_0

    .line 502
    :catch_5
    move-exception v1

    goto/16 :goto_0

    .line 493
    .end local v13    # "newCursor":Landroid/database/MatrixCursor;
    .end local v14    # "record":I
    :cond_b
    const/4 v13, 0x0

    .line 499
    if-eqz v15, :cond_1

    .line 500
    :try_start_d
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    goto/16 :goto_0

    .line 502
    :catch_6
    move-exception v1

    goto/16 :goto_0

    .end local v2    # "uri_ric":Landroid/net/Uri;
    .end local v8    # "context":Landroid/content/Context;
    :catch_7
    move-exception v1

    goto/16 :goto_0

    .end local v4    # "selection1":Ljava/lang/String;
    .end local v5    # "selectionArgs1":[Ljava/lang/String;
    .end local v6    # "sortOrder1":Ljava/lang/String;
    .end local v7    # "acckey":Ljava/lang/String;
    .end local v12    # "filter":Ljava/lang/String;
    :catch_8
    move-exception v3

    goto :goto_4
.end method

.method private queryRecipientFilter([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 47
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 807
    const-string v3, "[ExchangeDirectoryProvider] "

    const-string v10, "[queryRecipientFilter] start"

    invoke-static {v3, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    if-nez p2, :cond_0

    .line 812
    const/4 v3, 0x0

    .line 1009
    :goto_0
    return-object v3

    .line 816
    :cond_0
    const-wide/16 v4, -0x1

    .line 817
    .local v4, "accountId":J
    const/16 v24, 0x0

    .line 818
    .local v24, "accountIdStr":Ljava/lang/String;
    new-instance v44, Ljava/util/ArrayList;

    invoke-direct/range {v44 .. v44}, Ljava/util/ArrayList;-><init>()V

    .line 819
    .local v44, "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 820
    .local v8, "startTimeStr":Ljava/lang/String;
    const/4 v9, 0x0

    .line 821
    .local v9, "endTimeStr":Ljava/lang/String;
    const/16 v31, 0x1

    .line 823
    .local v31, "isValidWhereClause":Z
    const/4 v7, 0x1

    .line 824
    .local v7, "certificateRetrieval":I
    const/4 v13, 0x0

    .line 825
    .local v13, "idFlag":Z
    const/4 v14, 0x0

    .line 826
    .local v14, "accountIdFlag":Z
    const/4 v15, 0x0

    .line 827
    .local v15, "toFlag":Z
    const/16 v16, 0x0

    .line 828
    .local v16, "responseStatusFlag":Z
    const/16 v17, 0x0

    .line 829
    .local v17, "displayNameFlag":Z
    const/16 v18, 0x0

    .line 830
    .local v18, "emailFlag":Z
    const/16 v19, 0x0

    .line 831
    .local v19, "certificateStatusFlag":Z
    const/16 v20, 0x0

    .line 832
    .local v20, "certificateFlag":Z
    const/16 v21, 0x0

    .line 833
    .local v21, "miniCertificateFlag":Z
    const/16 v22, 0x0

    .line 834
    .local v22, "availabilityStatusFlag":Z
    const/16 v23, 0x0

    .line 836
    .local v23, "mergedFreeBusyFlag":Z
    const/16 v41, 0x0

    .line 837
    .local v41, "projectionStrs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {p1 .. p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v40

    .line 839
    .local v40, "projectionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->parseRRProjectionList(Ljava/util/List;)Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;

    move-result-object v29

    .line 841
    .local v29, "flags":Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;
    if-eqz v29, :cond_1

    .line 842
    move-object/from16 v0, v29

    iget v7, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateRetrieval:I

    .line 843
    move-object/from16 v0, v29

    iget-boolean v13, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mIdFlag:Z

    .line 844
    move-object/from16 v0, v29

    iget-boolean v14, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAccountIdFlag:Z

    .line 845
    move-object/from16 v0, v29

    iget-boolean v15, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mToFlag:Z

    .line 846
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mResponseStatusFlag:Z

    move/from16 v16, v0

    .line 847
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mDisplayNameFlag:Z

    move/from16 v17, v0

    .line 848
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mEmailFlag:Z

    move/from16 v18, v0

    .line 849
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateStatusFlag:Z

    move/from16 v19, v0

    .line 850
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mCertificateFlag:Z

    move/from16 v20, v0

    .line 851
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMiniCertificateFlag:Z

    move/from16 v21, v0

    .line 852
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mAvailabilityStatusFlag:Z

    move/from16 v22, v0

    .line 853
    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mMergedFreeBusyFlag:Z

    move/from16 v23, v0

    .line 854
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider$RecipientProjectionFlags;->mProjectionStrs:Ljava/util/ArrayList;

    move-object/from16 v41, v0

    .line 860
    :cond_1
    if-eqz p3, :cond_3

    .line 861
    const/16 v3, 0x3f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v30

    .line 862
    .local v30, "index":I
    const/16 v26, 0x0

    .line 863
    .local v26, "countReplace":I
    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v25, v0

    .line 864
    .local v25, "argLength":I
    :goto_1
    if-ltz v30, :cond_3

    if-eqz v31, :cond_3

    .line 865
    move/from16 v0, v26

    move/from16 v1, v25

    if-ge v0, v1, :cond_2

    .line 866
    const-string v3, "\\?"

    aget-object v10, p3, v26

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v10}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 867
    add-int/lit8 v26, v26, 0x1

    .line 868
    const/16 v3, 0x3f

    move-object/from16 v0, p2

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v30

    goto :goto_1

    .line 870
    :cond_2
    const/16 v31, 0x0

    goto :goto_1

    .line 878
    .end local v25    # "argLength":I
    .end local v26    # "countReplace":I
    .end local v30    # "index":I
    :cond_3
    const/16 v3, 0x28

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v28

    .line 879
    .local v28, "firstRoundBracket":I
    const/16 v3, 0x29

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v42

    .line 880
    .local v42, "secondRoundBracket":I
    if-gez v28, :cond_4

    if-ltz v42, :cond_a

    if-eqz v31, :cond_a

    .line 881
    :cond_4
    if-ltz v28, :cond_9

    if-lez v42, :cond_9

    move/from16 v0, v42

    move/from16 v1, v28

    if-le v0, v1, :cond_9

    .line 883
    add-int/lit8 v3, v28, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v42

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v43

    .line 885
    .local v43, "substr":Ljava/lang/String;
    const-string v3, "("

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, ")"

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "OR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    move/from16 v0, v28

    if-le v3, v0, :cond_8

    const-string v3, "OR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    move/from16 v0, v42

    if-ge v3, v0, :cond_8

    .line 888
    const-string v3, "AND"

    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 889
    const/16 v31, 0x0

    .line 901
    .end local v43    # "substr":Ljava/lang/String;
    :cond_5
    :goto_2
    const-string v3, "AND"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 902
    .local v2, "andTokens":[Ljava/lang/String;
    array-length v0, v2

    move/from16 v33, v0

    .line 903
    .local v33, "lengthAnd":I
    :cond_6
    :goto_3
    add-int/lit8 v33, v33, -0x1

    if-ltz v33, :cond_13

    if-eqz v31, :cond_13

    .line 904
    aget-object v38, v2, v33

    .line 906
    .local v38, "paramAnd":Ljava/lang/String;
    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v38

    .line 907
    const-string v3, "("

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 908
    const-string v3, ")"

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 909
    const/4 v3, 0x1

    invoke-virtual/range {v38 .. v38}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v3, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    .line 910
    .local v35, "orParse":Ljava/lang/String;
    const-string v3, "OR"

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    .line 911
    .local v36, "orTokens":[Ljava/lang/String;
    move-object/from16 v0, v36

    array-length v0, v0

    move/from16 v34, v0

    .line 912
    .local v34, "lengthOr":I
    :goto_4
    add-int/lit8 v34, v34, -0x1

    if-ltz v34, :cond_6

    if-eqz v31, :cond_6

    .line 913
    aget-object v39, v36, v34

    .line 914
    .local v39, "paramOr":Ljava/lang/String;
    const-string v3, "="

    move-object/from16 v0, v39

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    .line 915
    .local v37, "pair":[Ljava/lang/String;
    move-object/from16 v0, v37

    array-length v3, v0

    const/4 v10, 0x2

    if-eq v3, v10, :cond_7

    .line 916
    const/16 v31, 0x0

    .line 918
    :cond_7
    const/4 v3, 0x0

    aget-object v3, v37, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v32

    .line 919
    .local v32, "key":Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v3, v37, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v45

    .line 920
    .local v45, "val":Ljava/lang/String;
    const-string v3, "to"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 921
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 892
    .end local v2    # "andTokens":[Ljava/lang/String;
    .end local v32    # "key":Ljava/lang/String;
    .end local v33    # "lengthAnd":I
    .end local v34    # "lengthOr":I
    .end local v35    # "orParse":Ljava/lang/String;
    .end local v36    # "orTokens":[Ljava/lang/String;
    .end local v37    # "pair":[Ljava/lang/String;
    .end local v38    # "paramAnd":Ljava/lang/String;
    .end local v39    # "paramOr":Ljava/lang/String;
    .end local v45    # "val":Ljava/lang/String;
    .restart local v43    # "substr":Ljava/lang/String;
    :cond_8
    const/16 v31, 0x0

    goto/16 :goto_2

    .line 895
    .end local v43    # "substr":Ljava/lang/String;
    :cond_9
    const/16 v31, 0x0

    goto/16 :goto_2

    .line 897
    :cond_a
    const-string v3, "AND"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "OR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 898
    const/16 v31, 0x0

    goto/16 :goto_2

    .line 924
    .restart local v2    # "andTokens":[Ljava/lang/String;
    .restart local v32    # "key":Ljava/lang/String;
    .restart local v33    # "lengthAnd":I
    .restart local v34    # "lengthOr":I
    .restart local v35    # "orParse":Ljava/lang/String;
    .restart local v36    # "orTokens":[Ljava/lang/String;
    .restart local v37    # "pair":[Ljava/lang/String;
    .restart local v38    # "paramAnd":Ljava/lang/String;
    .restart local v39    # "paramOr":Ljava/lang/String;
    .restart local v45    # "val":Ljava/lang/String;
    :cond_b
    const/16 v31, 0x0

    goto :goto_4

    .line 928
    .end local v32    # "key":Ljava/lang/String;
    .end local v34    # "lengthOr":I
    .end local v35    # "orParse":Ljava/lang/String;
    .end local v36    # "orTokens":[Ljava/lang/String;
    .end local v37    # "pair":[Ljava/lang/String;
    .end local v39    # "paramOr":Ljava/lang/String;
    .end local v45    # "val":Ljava/lang/String;
    :cond_c
    const/16 v31, 0x0

    goto/16 :goto_3

    .line 933
    :cond_d
    const-string v3, "="

    move-object/from16 v0, v38

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v37

    .line 934
    .restart local v37    # "pair":[Ljava/lang/String;
    move-object/from16 v0, v37

    array-length v3, v0

    const/4 v10, 0x2

    if-eq v3, v10, :cond_e

    .line 935
    const/16 v31, 0x0

    .line 937
    :cond_e
    const/4 v3, 0x0

    aget-object v3, v37, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v32

    .line 938
    .restart local v32    # "key":Ljava/lang/String;
    const/4 v3, 0x1

    aget-object v3, v37, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v45

    .line 939
    .restart local v45    # "val":Ljava/lang/String;
    const-string v3, "accountId"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 940
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_3

    .line 941
    :cond_f
    const-string v3, "to"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 942
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v44

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 943
    :cond_10
    const-string v3, "startTime"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 944
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_3

    .line 945
    :cond_11
    const-string v3, "endTime"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 946
    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-direct {v0, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->normalizeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    .line 947
    :cond_12
    const-string v3, "accoundId"

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 948
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v10, "Use \'AccountId\' instead of \'AccoundId\'"

    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 957
    .end local v32    # "key":Ljava/lang/String;
    .end local v37    # "pair":[Ljava/lang/String;
    .end local v38    # "paramAnd":Ljava/lang/String;
    .end local v45    # "val":Ljava/lang/String;
    :cond_13
    const-string v3, "[ExchangeDirectoryProvider] "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "[queryRecipientFilter] Gal query selection: "

    move-object/from16 v0, v46

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    if-eqz v31, :cond_16

    if-nez v8, :cond_14

    if-nez v9, :cond_16

    :cond_14
    if-nez v9, :cond_15

    if-nez v8, :cond_16

    :cond_15
    if-nez v24, :cond_17

    .line 970
    :cond_16
    const-string v3, "[ExchangeDirectoryProvider] "

    const-string v10, "[queryRecipientFilter] return IllegalArgumentException"

    invoke-static {v3, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v10, "Illegal where clause string"

    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 974
    :cond_17
    const/4 v11, 0x0

    .line 975
    .local v11, "projs":[Ljava/lang/String;
    if-eqz v41, :cond_18

    .line 976
    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v11, v3, [Ljava/lang/String;

    .line 977
    move-object/from16 v0, v41

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 981
    :cond_18
    :try_start_0
    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 987
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Ljava/lang/String;

    .line 988
    .local v6, "toArray":[Ljava/lang/String;
    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 991
    const/4 v12, 0x0

    .line 994
    .local v12, "r2Result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/provider/RRResponse;>;"
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static/range {v3 .. v9}, Lcom/android/exchange/EasSyncService;->doResolveRecipients(Landroid/content/Context;J[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_1
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    .line 1003
    if-eqz v11, :cond_19

    move-object/from16 v10, p0

    .line 1004
    invoke-direct/range {v10 .. v24}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->buildResolveRecipients([Ljava/lang/String;Ljava/util/ArrayList;ZZZZZZZZZZZLjava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_0

    .line 982
    .end local v6    # "toArray":[Ljava/lang/String;
    .end local v12    # "r2Result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/provider/RRResponse;>;"
    :catch_0
    move-exception v27

    .line 983
    .local v27, "e":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 984
    const-string v3, "[ExchangeDirectoryProvider] "

    const-string v10, "[queryRecipientFilter] return IllegalArgumentException"

    invoke-static {v3, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v10, "Illegal value in where clause"

    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 996
    .end local v27    # "e":Ljava/lang/NumberFormatException;
    .restart local v6    # "toArray":[Ljava/lang/String;
    .restart local v12    # "r2Result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/provider/RRResponse;>;"
    :catch_1
    move-exception v27

    .line 997
    .local v27, "e":Lcom/android/emailcommon/mail/MessagingException;
    const-string v3, "[ExchangeDirectoryProvider] "

    const-string v10, "[queryRecipientFilter] return IllegalArgumentException"

    invoke-static {v3, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    invoke-virtual/range {v27 .. v27}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V

    .line 999
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "MessagingException: type:"

    move-object/from16 v0, v46

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v27 .. v27}, Lcom/android/emailcommon/mail/MessagingException;->getExceptionType()I

    move-result v46

    move/from16 v0, v46

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1009
    .end local v27    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :cond_19
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private validateCert(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 20
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1458
    const-string v3, "ExchangeProvider"

    const-string v17, " [validateCert] called"

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    const/4 v10, 0x0

    .line 1460
    .local v10, "cursor":Landroid/database/MatrixCursor;
    const-wide/16 v4, 0x0

    .line 1461
    .local v4, "accountId":J
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1462
    .local v2, "certChain":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1463
    .local v9, "certs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 1465
    .local v8, "isCheckCLR":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    array-length v3, v0

    if-nez v3, :cond_1

    .line 1466
    :cond_0
    const-string v3, "ExchangeProvider"

    const-string v17, " [validateCert] selection == null || selectionArgs == null || selectionArgs.length == 0"

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v11, v10

    .line 1520
    .end local v10    # "cursor":Landroid/database/MatrixCursor;
    .local v11, "cursor":Landroid/database/MatrixCursor;
    :goto_0
    return-object v11

    .line 1469
    .end local v11    # "cursor":Landroid/database/MatrixCursor;
    .restart local v10    # "cursor":Landroid/database/MatrixCursor;
    :cond_1
    const-string v3, "certificates"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1470
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Certificate is not specified."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1471
    :cond_2
    const-string v3, "accountId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1472
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Account ID is not specified."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1474
    :cond_3
    const-string v3, "accountId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/16 v17, 0x2

    move/from16 v0, v17

    if-le v3, v0, :cond_4

    .line 1475
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Account ID is not defined."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1477
    :cond_4
    const-string v3, "checkCRL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/16 v17, 0x2

    move/from16 v0, v17

    if-le v3, v0, :cond_5

    .line 1478
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "CheckCLR is not defined."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1480
    :cond_5
    const-string v3, "OR"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v3, v0, :cond_6

    .line 1481
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Selection has OR statement."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1483
    :cond_6
    const-string v3, "="

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 1484
    .local v14, "params":[Ljava/lang/String;
    if-eqz v14, :cond_d

    array-length v3, v14

    const/16 v17, 0x1

    move/from16 v0, v17

    if-le v3, v0, :cond_d

    move-object/from16 v0, p2

    array-length v3, v0

    array-length v0, v14

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ne v3, v0, :cond_d

    .line 1485
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p2

    array-length v3, v0

    if-ge v12, v3, :cond_e

    .line 1486
    aget-object v3, v14, v12

    const-string v17, "certificates"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1487
    aget-object v3, p2, v12

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1485
    :goto_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 1488
    :cond_7
    aget-object v3, v14, v12

    const-string v17, "certificateChain"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1489
    aget-object v3, p2, v12

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1490
    :cond_8
    aget-object v3, v14, v12

    const-string v17, "checkCRL"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1491
    const-string v3, "true"

    aget-object v17, p2, v12

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v3, "1"

    aget-object v17, p2, v12

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_9
    const/4 v8, 0x1

    :goto_3
    goto :goto_2

    :cond_a
    const/4 v8, 0x0

    goto :goto_3

    .line 1493
    :cond_b
    aget-object v3, v14, v12

    const-string v17, "accountId"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1494
    aget-object v3, p2, v12

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_2

    .line 1496
    :cond_c
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Can\'t parse statement."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1499
    .end local v12    # "i":I
    :cond_d
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Wrong selection in query."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1501
    .restart local v12    # "i":I
    :cond_e
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v7, v3, [Ljava/lang/String;

    .line 1502
    .local v7, "certChainArray":[Ljava/lang/String;
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1503
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v6, v3, [Ljava/lang/String;

    .line 1504
    .local v6, "certArray":[Ljava/lang/String;
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1505
    const-wide/16 v18, 0x0

    cmp-long v3, v4, v18

    if-gtz v3, :cond_f

    array-length v3, v6

    if-nez v3, :cond_f

    .line 1506
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v17, "Account ID or certificate is not specified."

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1507
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static/range {v3 .. v8}, Lcom/android/exchange/EasSyncService;->doValidateCert(Landroid/content/Context;J[Ljava/lang/String;[Ljava/lang/String;Z)Ljava/util/ArrayList;

    move-result-object v15

    .line 1509
    .local v15, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v15, :cond_10

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_10

    .line 1510
    new-instance v10, Landroid/database/MatrixCursor;

    .end local v10    # "cursor":Landroid/database/MatrixCursor;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/16 v17, 0x0

    const-string v18, "status"

    aput-object v18, v3, v17

    invoke-direct {v10, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1513
    .restart local v10    # "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    .line 1514
    .local v16, "status":Ljava/lang/Integer;
    const-string v3, "ExchangeProvider"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, " [validateCert] status : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    invoke-virtual {v10}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    goto :goto_4

    .line 1518
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "status":Ljava/lang/Integer;
    :cond_10
    const-string v3, "ExchangeProvider"

    const-string v17, " [validateCert] result == null || result.size() <= 0"

    move-object/from16 v0, v17

    invoke-static {v3, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    move-object v11, v10

    .line 1520
    .end local v10    # "cursor":Landroid/database/MatrixCursor;
    .restart local v11    # "cursor":Landroid/database/MatrixCursor;
    goto/16 :goto_0
.end method


# virtual methods
.method buildGalResultCursor([Ljava/lang/String;Lcom/android/exchange/provider/GalResult;I)Landroid/database/Cursor;
    .locals 32
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "galResult"    # Lcom/android/exchange/provider/GalResult;
    .param p3, "startindex"    # I

    .prologue
    .line 1125
    const/4 v7, -0x1

    .line 1126
    .local v7, "displayNameIndex":I
    const/4 v2, -0x1

    .line 1127
    .local v2, "alternateDisplayNameIndex":I
    const/4 v8, -0x1

    .line 1128
    .local v8, "emailIndex":I
    const/16 v24, -0x1

    .line 1129
    .local v24, "idIndex":I
    const/16 v26, -0x1

    .line 1131
    .local v26, "lookupIndex":I
    const/16 v20, -0x1

    .line 1132
    .local v20, "gal_pictureIndex":I
    const/4 v13, -0x1

    .line 1133
    .local v13, "gal_displayname":I
    const/4 v14, -0x1

    .line 1134
    .local v14, "gal_emailaddress":I
    const/16 v22, -0x1

    .line 1135
    .local v22, "gal_workphone":I
    const/16 v16, -0x1

    .line 1136
    .local v16, "gal_homephone":I
    const/16 v18, -0x1

    .line 1137
    .local v18, "gal_mobilephone":I
    const/4 v15, -0x1

    .line 1138
    .local v15, "gal_firstname":I
    const/16 v17, -0x1

    .line 1139
    .local v17, "gal_lastname":I
    const/4 v12, -0x1

    .line 1140
    .local v12, "gal_company":I
    const/16 v21, -0x1

    .line 1141
    .local v21, "gal_title":I
    const/16 v19, -0x1

    .line 1142
    .local v19, "gal_office":I
    const/4 v11, -0x1

    .line 1144
    .local v11, "gal_alias":I
    const/16 v23, 0x0

    .local v23, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v23

    move/from16 v1, v28

    if-ge v0, v1, :cond_12

    .line 1145
    aget-object v3, p1, v23

    .line 1146
    .local v3, "column":Ljava/lang/String;
    const-string v28, "display_name"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_0

    const-string v28, "display_name"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 1148
    :cond_0
    move/from16 v7, v23

    .line 1144
    :cond_1
    :goto_1
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .line 1149
    :cond_2
    const-string v28, "display_name_alt"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 1150
    move/from16 v2, v23

    goto :goto_1

    .line 1151
    :cond_3
    const-string v28, "data1"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 1152
    move/from16 v8, v23

    goto :goto_1

    .line 1153
    :cond_4
    const-string v28, "_id"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 1154
    move/from16 v24, v23

    goto :goto_1

    .line 1155
    :cond_5
    const-string v28, "lookup"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 1156
    move/from16 v26, v23

    goto :goto_1

    .line 1157
    :cond_6
    const-string v28, "pictureData"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 1160
    move/from16 v20, v23

    goto :goto_1

    .line 1161
    :cond_7
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 1162
    move/from16 v13, v23

    goto :goto_1

    .line 1163
    :cond_8
    const-string v28, "emailAddress"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 1164
    move/from16 v14, v23

    goto :goto_1

    .line 1165
    :cond_9
    const-string v28, "workPhone"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 1166
    move/from16 v22, v23

    goto :goto_1

    .line 1167
    :cond_a
    const-string v28, "homePhone"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 1168
    move/from16 v16, v23

    goto :goto_1

    .line 1169
    :cond_b
    const-string v28, "mobilePhone"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 1170
    move/from16 v18, v23

    goto/16 :goto_1

    .line 1171
    :cond_c
    const-string v28, "firstName"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 1172
    move/from16 v15, v23

    goto/16 :goto_1

    .line 1173
    :cond_d
    const-string v28, "lastName"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 1174
    move/from16 v17, v23

    goto/16 :goto_1

    .line 1175
    :cond_e
    const-string v28, "company"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 1176
    move/from16 v12, v23

    goto/16 :goto_1

    .line 1177
    :cond_f
    const-string v28, "title"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 1178
    move/from16 v21, v23

    goto/16 :goto_1

    .line 1179
    :cond_10
    const-string v28, "office"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 1180
    move/from16 v19, v23

    goto/16 :goto_1

    .line 1181
    :cond_11
    const-string v28, "alias"

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 1182
    move/from16 v11, v23

    goto/16 :goto_1

    .line 1185
    .end local v3    # "column":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    .line 1190
    .local v27, "row":[Ljava/lang/Object;
    new-instance v5, Landroid/database/MatrixCursor;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1191
    .local v5, "cursor":Landroid/database/MatrixCursor;
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/exchange/provider/GalResult;->galData:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1192
    .local v4, "count":I
    const/16 v23, 0x0

    :goto_2
    move/from16 v0, v23

    if-ge v0, v4, :cond_29

    .line 1194
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/exchange/provider/GalResult;->protocolVerison:D

    move-wide/from16 v28, v0

    const-wide/high16 v30, 0x4028000000000000L    # 12.0

    cmpg-double v28, v28, v30

    if-gez v28, :cond_13

    move/from16 v0, v23

    move/from16 v1, p3

    if-ge v0, v1, :cond_13

    .line 1192
    :goto_3
    add-int/lit8 v23, v23, 0x1

    goto :goto_2

    .line 1200
    :cond_13
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/exchange/provider/GalResult;->galData:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/exchange/provider/GalResult$GalData;

    .line 1201
    .local v10, "galDataRow":Lcom/android/exchange/provider/GalResult$GalData;
    const-string v28, "firstName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1202
    .local v9, "firstName":Ljava/lang/String;
    const-string v28, "lastName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 1203
    .local v25, "lastName":Ljava/lang/String;
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1206
    .local v6, "displayName":Ljava/lang/String;
    if-nez v6, :cond_14

    .line 1207
    if-eqz v9, :cond_26

    if-eqz v25, :cond_26

    .line 1208
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1215
    :cond_14
    :goto_4
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0, v6}, Lcom/android/exchange/provider/GalResult$GalData;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v7, v0, :cond_15

    .line 1217
    aput-object v6, v27, v7

    .line 1219
    :cond_15
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v2, v0, :cond_16

    .line 1224
    if-eqz v9, :cond_28

    if-eqz v25, :cond_28

    .line 1225
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v2

    .line 1230
    :cond_16
    :goto_5
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v8, v0, :cond_17

    .line 1231
    const-string v28, "emailAddress"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v8

    .line 1233
    :cond_17
    const/16 v28, -0x1

    move/from16 v0, v24

    move/from16 v1, v28

    if-eq v0, v1, :cond_18

    .line 1234
    add-int/lit8 v28, v23, 0x1

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v27, v24

    .line 1236
    :cond_18
    const/16 v28, -0x1

    move/from16 v0, v26

    move/from16 v1, v28

    if-eq v0, v1, :cond_19

    .line 1241
    invoke-virtual {v10}, Lcom/android/exchange/provider/GalResult$GalData;->toPackedString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v26

    .line 1245
    :cond_19
    const/16 v28, -0x1

    move/from16 v0, v20

    move/from16 v1, v28

    if-eq v0, v1, :cond_1a

    .line 1246
    const-string v28, "picture"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v20

    .line 1251
    :cond_1a
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v13, v0, :cond_1b

    .line 1252
    const-string v28, "displayName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v13

    .line 1254
    :cond_1b
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v14, v0, :cond_1c

    .line 1255
    const-string v28, "emailAddress"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v14

    .line 1257
    :cond_1c
    const/16 v28, -0x1

    move/from16 v0, v22

    move/from16 v1, v28

    if-eq v0, v1, :cond_1d

    .line 1258
    const-string v28, "workPhone"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v22

    .line 1260
    :cond_1d
    const/16 v28, -0x1

    move/from16 v0, v16

    move/from16 v1, v28

    if-eq v0, v1, :cond_1e

    .line 1261
    const-string v28, "homePhone"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v16

    .line 1263
    :cond_1e
    const/16 v28, -0x1

    move/from16 v0, v18

    move/from16 v1, v28

    if-eq v0, v1, :cond_1f

    .line 1264
    const-string v28, "mobilePhone"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v18

    .line 1266
    :cond_1f
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v15, v0, :cond_20

    .line 1267
    const-string v28, "firstName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v15

    .line 1269
    :cond_20
    const/16 v28, -0x1

    move/from16 v0, v17

    move/from16 v1, v28

    if-eq v0, v1, :cond_21

    .line 1270
    const-string v28, "lastName"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v17

    .line 1272
    :cond_21
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v12, v0, :cond_22

    .line 1273
    const-string v28, "company"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v12

    .line 1275
    :cond_22
    const/16 v28, -0x1

    move/from16 v0, v21

    move/from16 v1, v28

    if-eq v0, v1, :cond_23

    .line 1276
    const-string v28, "title"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v21

    .line 1278
    :cond_23
    const/16 v28, -0x1

    move/from16 v0, v19

    move/from16 v1, v28

    if-eq v0, v1, :cond_24

    .line 1279
    const-string v28, "office"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v19

    .line 1281
    :cond_24
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v11, v0, :cond_25

    .line 1282
    const-string v28, "alias"

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Lcom/android/exchange/provider/GalResult$GalData;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    aput-object v28, v27, v11

    .line 1285
    :cond_25
    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 1209
    :cond_26
    if-eqz v9, :cond_27

    .line 1210
    move-object v6, v9

    goto/16 :goto_4

    .line 1211
    :cond_27
    if-eqz v25, :cond_14

    .line 1212
    move-object/from16 v6, v25

    goto/16 :goto_4

    .line 1227
    :cond_28
    aput-object v6, v27, v2

    goto/16 :goto_5

    .line 1289
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v9    # "firstName":Ljava/lang/String;
    .end local v10    # "galDataRow":Lcom/android/exchange/provider/GalResult$GalData;
    .end local v25    # "lastName":Ljava/lang/String;
    :cond_29
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/exchange/provider/GalResult;->endRange:I

    move/from16 v28, v0

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/exchange/provider/GalResult;->total:I

    move/from16 v29, v0

    add-int/lit8 v29, v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_30

    .line 1291
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v7, v0, :cond_2a

    .line 1292
    const-string v28, "gal_search_show_more"

    aput-object v28, v27, v7

    .line 1294
    :cond_2a
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v2, v0, :cond_2b

    .line 1295
    const-string v28, ""

    aput-object v28, v27, v2

    .line 1297
    :cond_2b
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v8, v0, :cond_2c

    .line 1298
    const-string v28, ""

    aput-object v28, v27, v8

    .line 1300
    :cond_2c
    const/16 v28, -0x1

    move/from16 v0, v24

    move/from16 v1, v28

    if-eq v0, v1, :cond_2d

    .line 1301
    const/16 v28, -0x1

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v27, v24

    .line 1303
    :cond_2d
    const/16 v28, -0x1

    move/from16 v0, v26

    move/from16 v1, v28

    if-eq v0, v1, :cond_2e

    .line 1304
    const-string v28, ""

    aput-object v28, v27, v26

    .line 1308
    :cond_2e
    const/16 v28, -0x1

    move/from16 v0, v20

    move/from16 v1, v28

    if-eq v0, v1, :cond_2f

    .line 1309
    const-string v28, "gal_search_show_more"

    aput-object v28, v27, v20

    .line 1313
    :cond_2f
    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1316
    :cond_30
    return-object v5
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1335
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method getAccountIdByName(Landroid/content/Context;Ljava/lang/String;)J
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, -0x1

    const/4 v6, 0x0

    .line 361
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    .line 362
    .local v8, "accountId":Ljava/lang/Long;
    if-nez v8, :cond_0

    .line 363
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "emailAddress=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p2, v4, v6

    const/4 v5, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object v0, p1

    invoke-static/range {v0 .. v7}, Lcom/android/emailcommon/utility/Utility;->getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;)Ljava/lang/Long;

    move-result-object v8

    .line 367
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-eqz v0, :cond_0

    .line 368
    sget-object v0, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->mAccountIdMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    :cond_0
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1321
    sget-object v1, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 1322
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 1330
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 1324
    :sswitch_0
    const-string v1, "vnd.android.cursor.item/contact"

    goto :goto_0

    .line 1327
    :sswitch_1
    const-string v1, "vnd.android.cursor.dir/resolverecipients-entry"

    goto :goto_0

    .line 1322
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1340
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 377
    sget-object v3, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->sURIMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 380
    .local v1, "match":I
    sget-object v3, Landroid/os/StrictMode$ThreadPolicy;->LAX:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v3}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 381
    sget-object v3, Landroid/os/StrictMode$VmPolicy;->LAX:Landroid/os/StrictMode$VmPolicy;

    invoke-static {v3}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 383
    const-string v3, "[ExchagneDirectoryProvider]"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "query match: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    sparse-switch v1, :sswitch_data_0

    .line 418
    const/16 v3, 0x65

    if-ne v1, v3, :cond_0

    .line 419
    const-string v3, "[ExchagneDirectoryProvider]"

    const-string v4, "match is GAL_CONTACT_EXCEPTIONAL_CASE"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :goto_0
    :sswitch_0
    return-object v2

    .line 387
    :sswitch_1
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->queryRIC(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 393
    :sswitch_2
    invoke-direct {p0, p2}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->queryDirectory([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 397
    :sswitch_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->queryGAL(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 401
    :sswitch_4
    invoke-direct {p0, p1, p2, v1}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->queryGALContact(Landroid/net/Uri;[Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 405
    :sswitch_5
    :try_start_0
    invoke-direct {p0, p3, p4}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->validateCert(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e1":Lcom/android/emailcommon/mail/MessagingException;
    invoke-virtual {v0}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V

    .line 409
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessagingException: type:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/emailcommon/mail/MessagingException;->getExceptionType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 414
    .end local v0    # "e1":Lcom/android/emailcommon/mail/MessagingException;
    :sswitch_6
    invoke-direct {p0, p2, p3, p4}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->queryRecipientFilter([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 422
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown URI "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 385
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x4 -> :sswitch_3
        0x64 -> :sswitch_6
        0xc8 -> :sswitch_5
        0x12c -> :sswitch_0
        0x190 -> :sswitch_1
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1345
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
