.class public Lcom/android/exchange/PasswordRecoveryService;
.super Lcom/android/exchange/EasSyncService;
.source "PasswordRecoveryService.java"


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field mRecoveryPassword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/android/exchange/PasswordRecoveryService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {p1, p2}, Lcom/android/exchange/PasswordRecoveryService;->getMailboxForAccount(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 37
    sget-object v1, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/PasswordRecoveryService;->mDeviceId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    iput-object p3, p0, Lcom/android/exchange/PasswordRecoveryService;->mRecoveryPassword:Ljava/lang/String;

    .line 46
    iget-object v1, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/PasswordRecoveryService;->mProtocolVersion:Ljava/lang/String;

    .line 47
    iget-object v1, p0, Lcom/android/exchange/PasswordRecoveryService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/PasswordRecoveryService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 48
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception caught while getting device id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getMailboxForAccount(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/16 v4, 0x44

    .line 52
    const/4 v0, 0x0

    .line 53
    .local v0, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz p1, :cond_1

    .line 54
    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {p0, v2, v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 56
    if-nez v0, :cond_0

    .line 57
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .end local v0    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const-string v1, "dummy"

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>(JLjava/lang/String;I)V

    .line 58
    .restart local v0    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    sget-object v1, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    const-string v2, "restoreMailboxOfType returned null"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 61
    :cond_1
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .end local v0    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const-wide/16 v2, 0x1

    const-string v1, "dummy"

    invoke-direct {v0, v2, v3, v1, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>(JLjava/lang/String;I)V

    .restart local v0    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    goto :goto_0
.end method

.method private logResponse(Lcom/android/exchange/EasResponse;)V
    .locals 9
    .param p1, "httpResponse"    # Lcom/android/exchange/EasResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 118
    sget-object v6, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    const-string v7, "logResponse"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    if-nez p1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 123
    .local v5, "resp":I
    const/16 v6, 0xc8

    if-ne v5, v6, :cond_2

    .line 126
    iget-object v6, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v7, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    const v8, -0x8001

    and-int/2addr v7, v8

    iput v7, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    .line 128
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 130
    .local v2, "inputStream":Ljava/io/InputStream;
    sget-object v6, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    const-string v7, "creating parser"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v3, 0x0

    .line 133
    .local v3, "parser":Lcom/android/exchange/adapter/Parser;
    :try_start_0
    new-instance v4, Lcom/android/exchange/adapter/SettingsParser;

    invoke-direct {v4, v2}, Lcom/android/exchange/adapter/SettingsParser;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "parser":Lcom/android/exchange/adapter/Parser;
    .local v4, "parser":Lcom/android/exchange/adapter/Parser;
    move-object v3, v4

    .line 138
    .end local v4    # "parser":Lcom/android/exchange/adapter/Parser;
    .restart local v3    # "parser":Lcom/android/exchange/adapter/Parser;
    :goto_1
    if-eqz v3, :cond_1

    .line 140
    :try_start_1
    invoke-virtual {v3}, Lcom/android/exchange/adapter/Parser;->parse()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/exchange/EasException; {:try_start_1 .. :try_end_1} :catch_2

    .line 150
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "parser":Lcom/android/exchange/adapter/Parser;
    :cond_1
    :goto_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 151
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v6, "flags"

    iget-object v7, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    iget-object v6, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v7, Lcom/android/exchange/PasswordRecoveryService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v0}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    goto :goto_0

    .line 134
    .end local v0    # "cv":Landroid/content/ContentValues;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "parser":Lcom/android/exchange/adapter/Parser;
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 141
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 142
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 143
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 144
    .local v1, "e":Lcom/android/exchange/EasException;
    invoke-virtual {v1}, Lcom/android/exchange/EasException;->printStackTrace()V

    goto :goto_2

    .line 148
    .end local v1    # "e":Lcom/android/exchange/EasException;
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "parser":Lcom/android/exchange/adapter/Parser;
    :cond_2
    iget-object v6, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v7, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    const v8, 0x8000

    or-int/2addr v7, v8

    iput v7, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    goto :goto_2
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 69
    sget-object v6, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    const-string v7, "run"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/android/exchange/PasswordRecoveryService;->setupService()Z

    .line 73
    new-instance v5, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v5}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 75
    .local v5, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v6, 0x485

    :try_start_0
    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 76
    const/16 v6, 0x494

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 77
    const/16 v6, 0x488

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 78
    const/16 v6, 0x495

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/PasswordRecoveryService;->mRecoveryPassword:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 79
    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 80
    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 81
    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/exchange/adapter/Serializer;->done()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    sget-object v6, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sending http post with recovery password, mUsername:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/PasswordRecoveryService;->mUserName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :try_start_1
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v6, :cond_0

    .line 91
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "PasswordRecoverySerice:run(): Wbxml:"

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/android/exchange/PasswordRecoveryService;->userLog([Ljava/lang/String;)V

    .line 92
    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v0

    .line 93
    .local v0, "b":[B
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 94
    .local v1, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v4, Lcom/android/exchange/adapter/LogAdapter;

    invoke-direct {v4, p0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 95
    .local v4, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v4, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 99
    .end local v0    # "b":[B
    .end local v1    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v4    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_0
    const-string v6, "Settings"

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/android/exchange/PasswordRecoveryService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    .line 101
    .local v3, "httpResponse":Lcom/android/exchange/EasResponse;
    :try_start_2
    invoke-direct {p0, v3}, Lcom/android/exchange/PasswordRecoveryService;->logResponse(Lcom/android/exchange/EasResponse;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    if-eqz v3, :cond_1

    .line 104
    :try_start_3
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 115
    .end local v3    # "httpResponse":Lcom/android/exchange/EasResponse;
    :cond_1
    :goto_1
    return-void

    .line 82
    :catch_0
    move-exception v2

    .line 83
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v3    # "httpResponse":Lcom/android/exchange/EasResponse;
    :catchall_0
    move-exception v6

    if-eqz v3, :cond_2

    .line 104
    :try_start_4
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    :cond_2
    throw v6
    :try_end_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 106
    .end local v3    # "httpResponse":Lcom/android/exchange/EasResponse;
    :catch_1
    move-exception v2

    .line 107
    .local v2, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    const-string v6, "DeviceAccessPermission"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Caught Exceptoin, Device is blocked or quarantined "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/android/emailcommon/utility/DeviceAccessException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v6, 0x6

    iput v6, p0, Lcom/android/exchange/PasswordRecoveryService;->mExitStatus:I

    .line 110
    iget-object v6, p0, Lcom/android/exchange/PasswordRecoveryService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v8, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v6, v7, v8}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_1

    .line 111
    .end local v2    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catch_2
    move-exception v2

    .line 112
    .local v2, "e":Ljava/io/IOException;
    sget-object v6, Lcom/android/exchange/PasswordRecoveryService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Caught IO Exception: message :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
