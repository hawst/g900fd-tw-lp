.class public Lcom/android/exchange/ServiceLogger;
.super Ljava/lang/Object;
.source "ServiceLogger.java"


# static fields
.field private static mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

.field private static mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

.field private static mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

.field private static sNotificationLogger:Lcom/android/exchange/DiscourseLogger;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/android/exchange/ServiceLogger;->initLogging()V

    .line 25
    return-void
.end method

.method public static initLogging()V
    .locals 2

    .prologue
    const/16 v1, 0x32

    .line 27
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    .line 30
    :cond_0
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_1

    .line 31
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    .line 33
    :cond_1
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_2

    .line 34
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    .line 36
    :cond_2
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_3

    .line 37
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    .line 39
    :cond_3
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_4

    .line 40
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    .line 42
    :cond_4
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_5

    .line 43
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    .line 45
    :cond_5
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_6

    .line 46
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    .line 48
    :cond_6
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_7

    .line 49
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    .line 51
    :cond_7
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    if-nez v0, :cond_8

    .line 52
    new-instance v0, Lcom/android/exchange/DiscourseLogger;

    invoke-direct {v0, v1}, Lcom/android/exchange/DiscourseLogger;-><init>(I)V

    sput-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    .line 54
    :cond_8
    return-void
.end method


# virtual methods
.method public dumpLogStats(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 169
    const-string v0, "BEGIN: DUMP OF ExchangeService"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 170
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 171
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 175
    :goto_0
    const-string v0, "DUMP OF ExchangeService Ping Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 176
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_1

    .line 177
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 181
    :goto_1
    const-string v0, "DUMP OF ExchangeService Sync Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_2

    .line 183
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 187
    :goto_2
    const-string v0, "DUMP OF EasOutboxService Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 188
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_3

    .line 189
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 193
    :goto_3
    const-string v0, "DUMP OF Attachments Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 194
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_4

    .line 195
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 199
    :goto_4
    const-string v0, "DUMP OF FolderOperation Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 200
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_5

    .line 201
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 205
    :goto_5
    const-string v0, "DUMP OF Notification Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 206
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_6

    .line 207
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 211
    :goto_6
    const-string v0, "DUMP OF GALSearch Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 212
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_7

    .line 213
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 217
    :goto_7
    const-string v0, "DUMP OF Network Stats"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 218
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_8

    .line 219
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logLastDiscourse(Ljava/io/PrintWriter;)V

    .line 224
    :goto_8
    const-string v0, "END: DUMP OF ExchangeService"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 225
    return-void

    .line 173
    :cond_0
    const-string v0, "DUMP OF ExchangeService NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :cond_1
    const-string v0, "DUMP OF ExchangeService NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 185
    :cond_2
    const-string v0, "DUMP OF ExchangeService NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 191
    :cond_3
    const-string v0, "DUMP OF EasOutboxService NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 197
    :cond_4
    const-string v0, "DUMP OF Attachments NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_4

    .line 203
    :cond_5
    const-string v0, "DUMP OF FolderOperation NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    .line 209
    :cond_6
    const-string v0, "DUMP OF Notification NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_6

    .line 215
    :cond_7
    const-string v0, "DUMP OF GALSearch NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_7

    .line 221
    :cond_8
    const-string v0, "DUMP OF Network NOTHING TO DUMP!!!"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public logAttachmentStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "attachmentStat"    # Ljava/lang/String;

    .prologue
    .line 81
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 82
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sAttachmentLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method

.method public logConnectivityStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "connectivityStat"    # Ljava/lang/String;

    .prologue
    .line 105
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 106
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sConnectivityStateLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 108
    :cond_0
    return-void
.end method

.method public logEASServiceStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "serviceStat"    # Ljava/lang/String;

    .prologue
    .line 69
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 70
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mEASServiceLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 72
    :cond_0
    return-void
.end method

.method public logEasOutboxServiceStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "outboxServiceStat"    # Ljava/lang/String;

    .prologue
    .line 75
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 76
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sEasOutboxServiceLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 78
    :cond_0
    return-void
.end method

.method public logFolderOperationStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "folderOperationStat"    # Ljava/lang/String;

    .prologue
    .line 87
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sFolderOperationLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 90
    :cond_0
    return-void
.end method

.method public logGALSearchStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "galSearchStat"    # Ljava/lang/String;

    .prologue
    .line 99
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sGALSearchLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 102
    :cond_0
    return-void
.end method

.method public logNotificationStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "notificationStat"    # Ljava/lang/String;

    .prologue
    .line 93
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 94
    sget-object v0, Lcom/android/exchange/ServiceLogger;->sNotificationLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method

.method public logPingStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "pingStat"    # Ljava/lang/String;

    .prologue
    .line 63
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServicePingLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method

.method public logSyncStats(Ljava/lang/String;)V
    .locals 1
    .param p1, "syncStat"    # Ljava/lang/String;

    .prologue
    .line 57
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    if-eqz v0, :cond_0

    .line 58
    sget-object v0, Lcom/android/exchange/ServiceLogger;->mServiceSyncLogger:Lcom/android/exchange/DiscourseLogger;

    invoke-virtual {v0, p1}, Lcom/android/exchange/DiscourseLogger;->logString(Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method
