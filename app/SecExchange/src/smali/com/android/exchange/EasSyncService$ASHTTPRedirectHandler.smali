.class Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;
.super Ljava/lang/Object;
.source "EasSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/EasSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ASHTTPRedirectHandler"
.end annotation


# instance fields
.field private mIsRedirected:Z

.field final synthetic this$0:Lcom/android/exchange/EasSyncService;


# direct methods
.method private constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 1

    .prologue
    .line 3486
    iput-object p1, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3491
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/exchange/EasSyncService;Lcom/android/exchange/EasSyncService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/exchange/EasSyncService;
    .param p2, "x1"    # Lcom/android/exchange/EasSyncService$1;

    .prologue
    .line 3486
    invoke-direct {p0, p1}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;-><init>(Lcom/android/exchange/EasSyncService;)V

    return-void
.end method

.method private checkUpdateRedirectUri(Lcom/android/exchange/EasResponse;Lorg/apache/http/client/methods/HttpRequestBase;)Z
    .locals 20
    .param p1, "response"    # Lcom/android/exchange/EasResponse;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpRequestBase;

    .prologue
    .line 3569
    const/4 v12, 0x0

    .line 3570
    .local v12, "continueRedirection":Z
    const-string v4, "X-MS-Location"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v11

    .line 3572
    .local v11, "commands":Lorg/apache/http/Header;
    invoke-direct/range {p0 .. p1}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->isHttp451RedirectionNeeded(Lcom/android/exchange/EasResponse;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Http status 451 recieved, Server redirected request to new URI : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3574
    invoke-interface {v11}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v17

    .line 3575
    .local v17, "redirectedUri":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v18

    .line 3576
    .local v18, "updateUri":Ljava/lang/String;
    const-string v4, "\\?"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 3577
    .local v16, "old_uri_parts":[Ljava/lang/String;
    const-string v4, "\\?"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 3578
    .local v15, "new_uri_parts":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v16, v4

    const/4 v5, 0x0

    aget-object v5, v15, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 3579
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # getter for: Lcom/android/exchange/EasSyncService;->mTrustSsl:Z
    invoke-static {v4}, Lcom/android/exchange/EasSyncService;->access$000(Lcom/android/exchange/EasSyncService;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3580
    const-string v4, "https"

    const-string v5, "httpts"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 3582
    :cond_0
    invoke-static/range {v18 .. v18}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 3583
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z

    .line 3584
    const/4 v12, 0x1

    .line 3627
    .end local v15    # "new_uri_parts":[Ljava/lang/String;
    .end local v16    # "old_uri_parts":[Ljava/lang/String;
    .end local v17    # "redirectedUri":Ljava/lang/String;
    .end local v18    # "updateUri":Ljava/lang/String;
    :cond_1
    :goto_0
    return v12

    .line 3587
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v4, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/android/exchange/AutoDiscoverHandler;->isAutoDiscoverNeeded(Lcom/android/exchange/EasResponse;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3589
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    .line 3590
    .local v3, "userName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mDomain:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # getter for: Lcom/android/exchange/EasSyncService;->mTrustSsl:Z
    invoke-static {v6}, Lcom/android/exchange/EasSyncService;->access$000(Lcom/android/exchange/EasSyncService;)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v7, v7, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v8, v8, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget-object v10, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    invoke-static/range {v3 .. v10}, Lcom/android/exchange/AutoDiscoverHandler;->tryAutodiscoverWithLock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLandroid/content/Context;)Landroid/os/Bundle;

    move-result-object v2

    .line 3591
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "autodiscover_host_auth"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 3592
    .local v14, "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v14, :cond_3

    .line 3593
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 3594
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthSend:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 3596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v5, v14, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 3597
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v5, v14, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 3598
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v5, v14, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v5, v4, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 3601
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 3602
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 3604
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->makeUriString()Ljava/lang/String;

    move-result-object v19

    .line 3605
    .local v19, "uri":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 3610
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3611
    const/4 v12, 0x1

    .line 3619
    .end local v19    # "uri":Ljava/lang/String;
    :cond_3
    :try_start_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->clearSSLVerificationDomains()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 3621
    :catch_0
    move-exception v13

    .line 3622
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 3613
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "userName":Ljava/lang/String;
    .end local v13    # "e":Landroid/os/RemoteException;
    .end local v14    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :catch_1
    move-exception v13

    .line 3614
    .restart local v13    # "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3619
    :try_start_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->clearSSLVerificationDomains()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 3621
    :catch_2
    move-exception v13

    .line 3622
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 3615
    .end local v13    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v13

    .line 3616
    .local v13, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3619
    :try_start_5
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    invoke-interface {v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->clearSSLVerificationDomains()V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 3621
    :catch_4
    move-exception v13

    .line 3622
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 3618
    .end local v13    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v4

    .line 3619
    :try_start_6
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    invoke-interface {v5}, Lcom/android/emailcommon/service/IEmailServiceCallback;->clearSSLVerificationDomains()V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_5

    .line 3623
    :goto_1
    throw v4

    .line 3621
    :catch_5
    move-exception v13

    .line 3622
    .restart local v13    # "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private executeHttpOption(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;)Lcom/android/exchange/EasResponse;
    .locals 7
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 3860
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v2}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3863
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3864
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->setAliasInMap(Ljava/lang/String;)V

    .line 3868
    :goto_0
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # invokes: Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    invoke-static {v2}, Lcom/android/exchange/EasSyncService;->access$300(Lcom/android/exchange/EasSyncService;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/android/exchange/EasResponse;->fromHttpRequest(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/android/exchange/EasResponse;

    move-result-object v1

    .line 3870
    .local v1, "response":Lcom/android/exchange/EasResponse;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->removeAliasFromMap()V

    .line 3874
    .end local v1    # "response":Lcom/android/exchange/EasResponse;
    :goto_1
    return-object v1

    .line 3866
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mCbaCertificateAlias:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->setAliasInMap(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 3877
    :catch_0
    move-exception v0

    .line 3878
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "sendHttpClientOptions(): IllegalStateException from HTTP Client handled"

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3879
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 3880
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 3874
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # invokes: Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    invoke-static {v2}, Lcom/android/exchange/EasSyncService;->access$300(Lcom/android/exchange/EasSyncService;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/android/exchange/EasResponse;->fromHttpRequest(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/android/exchange/EasResponse;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_1

    .line 3881
    :catch_1
    move-exception v0

    .line 3882
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "executePostWithTimeout(): Unexpected exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3883
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private extractRedirectURI(Ljava/lang/String;)V
    .locals 7
    .param p1, "urlString"    # Ljava/lang/String;

    .prologue
    .line 3632
    const/4 v0, 0x0

    .line 3633
    .local v0, "recvAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    const/4 v1, 0x0

    .line 3634
    .local v1, "sendAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v2, :cond_0

    .line 3635
    sget-object v2, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 3636
    sget-object v2, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeySend:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v1

    .line 3638
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3639
    if-eqz v0, :cond_2

    .line 3640
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # getter for: Lcom/android/exchange/EasSyncService;->mTrustSsl:Z
    invoke-static {v2}, Lcom/android/exchange/EasSyncService;->access$000(Lcom/android/exchange/EasSyncService;)Z

    move-result v2

    invoke-direct {p0, v0, p1, v2}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->setFlags(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;Z)V

    .line 3641
    invoke-direct {p0, v0, p1}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->setRedirectedURI(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;)V

    .line 3642
    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->isSaved()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3643
    sget-object v2, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->toContentValues()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 3644
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Saved redirected URI due to status 451 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->getUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3646
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->isSaved()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3648
    sget-object v2, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->toContentValues()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 3652
    :cond_2
    return-void
.end method

.method private isHttp451RedirectionNeeded(Lcom/android/exchange/EasResponse;)Z
    .locals 4
    .param p1, "response"    # Lcom/android/exchange/EasResponse;

    .prologue
    .line 3557
    const/4 v2, 0x0

    .line 3558
    .local v2, "is451RedirectionNeeded":Z
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 3559
    .local v0, "code":I
    const-string v3, "X-MS-Location"

    invoke-virtual {p1, v3}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 3560
    .local v1, "commands":Lorg/apache/http/Header;
    const/16 v3, 0x1c3

    if-ne v0, v3, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3562
    const/4 v2, 0x1

    .line 3564
    :cond_0
    return v2
.end method

.method private isRedirected()Z
    .locals 1

    .prologue
    .line 3493
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z

    return v0
.end method

.method private setFlags(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;Z)V
    .locals 5
    .param p1, "recvAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "urlString"    # Ljava/lang/String;
    .param p3, "isTrustAll"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3674
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3675
    const/16 v2, 0x3a

    invoke-virtual {p2, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 3676
    .local v1, "protocolIndex":I
    invoke-virtual {p2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 3677
    .local v0, "protocol":Ljava/lang/String;
    const-string v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "httpts"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3679
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iput-boolean v4, v2, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 3680
    if-eqz p1, :cond_1

    .line 3681
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 3696
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 3697
    if-eqz p3, :cond_5

    .line 3698
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 3704
    .end local v0    # "protocol":Ljava/lang/String;
    .end local v1    # "protocolIndex":I
    :cond_2
    :goto_1
    return-void

    .line 3683
    .restart local v0    # "protocol":Ljava/lang/String;
    .restart local v1    # "protocolIndex":I
    :cond_3
    const-string v2, "tls"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3684
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iput-boolean v4, v2, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 3685
    if-eqz p1, :cond_1

    .line 3687
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 3688
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_0

    .line 3691
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iput-boolean v3, v2, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 3692
    if-eqz p1, :cond_1

    .line 3693
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v2, v2, -0xc

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_0

    .line 3700
    :cond_5
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_1
.end method

.method private setRedirectedURI(Lcom/android/emailcommon/provider/EmailContent$HostAuth;Ljava/lang/String;)V
    .locals 5
    .param p1, "recvAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "urlString"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 3655
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3656
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    .line 3657
    .local v0, "uri":Ljava/net/URI;
    iget-object v3, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 3658
    if-eqz p1, :cond_1

    .line 3659
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    .line 3660
    invoke-virtual {v0}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    .line 3661
    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3662
    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mDomain:Ljava/lang/String;

    .line 3664
    :cond_0
    iget v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_2

    move v1, v2

    .line 3665
    .local v1, "useSSL":Z
    :goto_0
    iget-object v3, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget v3, v3, Lcom/android/exchange/EasSyncService;->mPort:I

    iput v3, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 3666
    const-string v3, "eas"

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget v3, v3, Lcom/android/exchange/EasSyncService;->mPort:I

    if-ge v3, v2, :cond_1

    .line 3667
    if-eqz v1, :cond_3

    const/16 v2, 0x1bb

    :goto_1
    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 3671
    .end local v0    # "uri":Ljava/net/URI;
    .end local v1    # "useSSL":Z
    :cond_1
    return-void

    .line 3664
    .restart local v0    # "uri":Ljava/net/URI;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 3667
    .restart local v1    # "useSSL":Z
    :cond_3
    const/16 v2, 0x50

    goto :goto_1
.end method


# virtual methods
.method public execOption(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;)Lcom/android/exchange/EasResponse;
    .locals 7
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3524
    const/4 v2, 0x0

    .line 3525
    .local v2, "count":I
    const/4 v4, 0x0

    .line 3526
    .local v4, "response":Lcom/android/exchange/EasResponse;
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z

    .line 3527
    const/4 v1, 0x0

    .line 3529
    .local v1, "continueRedirection":Z
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->executeHttpOption(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;)Lcom/android/exchange/EasResponse;

    move-result-object v4

    .line 3530
    if-eqz v4, :cond_0

    .line 3531
    invoke-direct {p0, v4, p2}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->checkUpdateRedirectUri(Lcom/android/exchange/EasResponse;Lorg/apache/http/client/methods/HttpRequestBase;)Z

    move-result v1

    .line 3534
    :cond_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "count":I
    .local v3, "count":I
    const/16 v5, 0xa

    if-ge v2, v5, :cond_1

    if-nez v1, :cond_5

    .line 3536
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_2

    invoke-direct {p0}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->isRedirected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3539
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpOptions;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->extractRedirectURI(Ljava/lang/String;)V

    .line 3541
    :cond_2
    if-eqz v4, :cond_4

    .line 3543
    invoke-virtual {v4}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 3544
    .local v0, "code":I
    const/16 v5, 0x191

    if-eq v0, v5, :cond_3

    const/16 v5, 0x193

    if-ne v0, v5, :cond_4

    .line 3546
    :cond_3
    const-string v5, "ExchangeService"

    const-string v6, "Authentication Failed So aborting HTTP Session."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3547
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpOptions;->abort()V

    .line 3551
    .end local v0    # "code":I
    :cond_4
    return-object v4

    :cond_5
    move v2, v3

    .end local v3    # "count":I
    .restart local v2    # "count":I
    goto :goto_0
.end method

.method public execPost(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;
    .locals 6
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpPost;
    .param p3, "timeout"    # I
    .param p4, "isPingCommand"    # Z
    .param p5, "isAbortNotNeeded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3499
    const/4 v2, 0x0

    .line 3500
    .local v2, "count":I
    const/4 v4, 0x0

    .line 3501
    .local v4, "response":Lcom/android/exchange/EasResponse;
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->mIsRedirected:Z

    .line 3502
    const/4 v1, 0x0

    .line 3504
    .local v1, "continueRedirection":Z
    :goto_0
    invoke-virtual/range {p0 .. p5}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->executePostWithTimeout(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;

    move-result-object v4

    .line 3506
    if-eqz v4, :cond_0

    .line 3507
    invoke-direct {p0, v4, p2}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->checkUpdateRedirectUri(Lcom/android/exchange/EasResponse;Lorg/apache/http/client/methods/HttpRequestBase;)Z

    move-result v1

    .line 3510
    :cond_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "count":I
    .local v3, "count":I
    const/16 v5, 0xa

    if-ge v2, v5, :cond_1

    if-nez v1, :cond_4

    .line 3512
    :cond_1
    if-eqz v4, :cond_3

    .line 3513
    invoke-virtual {v4}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 3515
    .local v0, "code":I
    const/16 v5, 0xc8

    if-eq v0, v5, :cond_2

    const/16 v5, 0x1c1

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->isRedirected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3517
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->extractRedirectURI(Ljava/lang/String;)V

    .line 3520
    .end local v0    # "code":I
    :cond_3
    return-object v4

    :cond_4
    move v2, v3

    .end local v3    # "count":I
    .restart local v2    # "count":I
    goto :goto_0
.end method

.method protected executePostWithTimeout(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;
    .locals 16
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpPost;
    .param p3, "timeout"    # I
    .param p4, "isPingCommand"    # Z
    .param p5, "isAbortNotNeeded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3710
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-boolean v8, v8, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-eqz v8, :cond_0

    # getter for: Lcom/android/exchange/EasSyncService;->flagRemoteWipe:Z
    invoke-static {}, Lcom/android/exchange/EasSyncService;->access$100()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3714
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "executePostWithTimeout(): mStop == true, throw exception to finish immediately"

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3715
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Thread was stopped. Don\'t open connection to server"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 3718
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 3720
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    move-object/from16 v0, p2

    # setter for: Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v8, v0}, Lcom/android/exchange/EasSyncService;->access$202(Lcom/android/exchange/EasSyncService;Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/client/methods/HttpPost;

    .line 3722
    move/from16 v0, p3

    add-int/lit16 v8, v0, 0x2710

    int-to-long v4, v8

    .line 3724
    .local v4, "alarmTime":J
    if-eqz p4, :cond_2

    .line 3726
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11, v4, v5}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 3734
    :goto_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3741
    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 3744
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v8, v8, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    if-eqz v8, :cond_3

    .line 3745
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v8, v8, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->setAliasInMap(Ljava/lang/String;)V

    .line 3750
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # invokes: Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    invoke-static {v8}, Lcom/android/exchange/EasSyncService;->access$300(Lcom/android/exchange/EasSyncService;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v8, v0, v1}, Lcom/android/exchange/EasResponse;->fromHttpRequest(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/android/exchange/EasResponse;

    move-result-object v7

    .line 3751
    .local v7, "response":Lcom/android/exchange/EasResponse;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->removeAliasFromMap()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3792
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 3794
    :try_start_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-boolean v8, v8, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_7

    .line 3820
    if-eqz p4, :cond_6

    .line 3822
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 3836
    :goto_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 3840
    if-eqz p5, :cond_1

    .line 3844
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x0

    # setter for: Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v8, v10}, Lcom/android/exchange/EasSyncService;->access$202(Lcom/android/exchange/EasSyncService;Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/client/methods/HttpPost;

    .line 3846
    :cond_1
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .end local v7    # "response":Lcom/android/exchange/EasResponse;
    :goto_3
    return-object v7

    .line 3730
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11, v4, v5}, Lcom/android/exchange/ExchangeService;->setWatchdogAlarm(JJ)V

    goto :goto_0

    .line 3734
    .end local v4    # "alarmTime":J
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v8

    .line 3747
    .restart local v4    # "alarmTime":J
    :cond_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-object v8, v8, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mCbaCertificateAlias:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->setAliasInMap(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 3762
    :catch_0
    move-exception v6

    .line 3767
    .local v6, "ie":Ljava/io/IOException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "executePostWithTimeout(): IOException "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3769
    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Timeout waiting for connection"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 3771
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "executePostWithTimeout(): Timeout Waiting for Connection. Shutting down ConnectionManager"

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3772
    invoke-static {}, Lcom/android/exchange/ExchangeService;->shutdownConnectionManager()V

    .line 3774
    :cond_4
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3792
    .end local v6    # "ie":Ljava/io/IOException;
    :catchall_1
    move-exception v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v9}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 3794
    :try_start_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-boolean v10, v10, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v10, :cond_d

    .line 3820
    if-eqz p4, :cond_c

    .line 3822
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v10, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 3836
    :goto_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 3840
    if-eqz p5, :cond_5

    .line 3844
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v11, 0x0

    # setter for: Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v10, v11}, Lcom/android/exchange/EasSyncService;->access$202(Lcom/android/exchange/EasSyncService;Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/client/methods/HttpPost;

    .line 3846
    :cond_5
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw v8

    .line 3826
    .restart local v7    # "response":Lcom/android/exchange/EasResponse;
    :cond_6
    :try_start_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->clearWatchdogAlarm(J)V

    goto/16 :goto_2

    .line 3846
    :catchall_2
    move-exception v8

    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v8

    .line 3832
    :cond_7
    :try_start_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "executePostWithTimeout():Thread stopped mailbox:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v14, v13, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " Watchdog not cleared again"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v10}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_2

    .line 3754
    .end local v7    # "response":Lcom/android/exchange/EasResponse;
    :cond_8
    :try_start_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    # invokes: Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    invoke-static {v8}, Lcom/android/exchange/EasSyncService;->access$300(Lcom/android/exchange/EasSyncService;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v8, v0, v1}, Lcom/android/exchange/EasResponse;->fromHttpRequest(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/android/exchange/EasResponse;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/AssertionError; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v7

    .line 3792
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 3794
    :try_start_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-boolean v8, v8, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_b

    .line 3820
    if-eqz p4, :cond_a

    .line 3822
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 3836
    :goto_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 3840
    if-eqz p5, :cond_9

    .line 3844
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x0

    # setter for: Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v8, v10}, Lcom/android/exchange/EasSyncService;->access$202(Lcom/android/exchange/EasSyncService;Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/client/methods/HttpPost;

    .line 3846
    :cond_9
    monitor-exit v9

    goto/16 :goto_3

    :catchall_3
    move-exception v8

    monitor-exit v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v8

    .line 3826
    :cond_a
    :try_start_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v8, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->clearWatchdogAlarm(J)V

    goto :goto_5

    .line 3832
    :cond_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "executePostWithTimeout():Thread stopped mailbox:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v14, v13, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " Watchdog not cleared again"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v10}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_5

    .line 3776
    :catch_1
    move-exception v6

    .line 3777
    .local v6, "ie":Ljava/lang/IllegalStateException;
    :try_start_c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "executePostWithTimeout(): IllegalStateException "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3778
    new-instance v8, Ljava/io/IOException;

    invoke-direct {v8}, Ljava/io/IOException;-><init>()V

    throw v8

    .line 3780
    .end local v6    # "ie":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v3

    .line 3781
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "executePostWithTimeout(): Unexpected exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3782
    new-instance v8, Ljava/io/IOException;

    invoke-direct {v8, v3}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 3783
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v2

    .line 3784
    .local v2, "ae":Ljava/lang/AssertionError;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "executePostWithTimeout(): AssertionError "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/AssertionError;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3785
    new-instance v8, Ljava/io/IOException;

    invoke-direct {v8, v2}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 3826
    .end local v2    # "ae":Ljava/lang/AssertionError;
    :cond_c
    :try_start_d
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v10, v10, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->clearWatchdogAlarm(J)V

    goto/16 :goto_4

    .line 3846
    :catchall_4
    move-exception v8

    monitor-exit v9
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v8

    .line 3832
    :cond_d
    :try_start_e
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "executePostWithTimeout():Thread stopped mailbox:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->this$0:Lcom/android/exchange/EasSyncService;

    iget-wide v14, v14, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " Watchdog not cleared again"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    goto/16 :goto_4
.end method
