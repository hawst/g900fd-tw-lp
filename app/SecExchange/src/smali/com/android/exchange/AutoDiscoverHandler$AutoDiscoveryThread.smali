.class Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
.super Ljava/lang/Thread;
.source "AutoDiscoverHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/AutoDiscoverHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AutoDiscoveryThread"
.end annotation


# instance fields
.field private mAuthString:Ljava/lang/String;

.field private mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

.field private mEmailAddress:Ljava/lang/String;

.field private final mExecutionStep:I

.field private mHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field private mHttpPost:Lorg/apache/http/client/methods/HttpPost;

.field private mServerAddress:Ljava/lang/String;

.field private mUri:Ljava/lang/String;

.field private mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

.field final synthetic this$0:Lcom/android/exchange/AutoDiscoverHandler;


# direct methods
.method public constructor <init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)V
    .locals 4
    .param p2, "_uri"    # Ljava/lang/String;
    .param p3, "_userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p4, "_authToken"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 272
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 236
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    .line 238
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    .line 239
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 242
    const-string v3, ""

    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mEmailAddress:Ljava/lang/String;

    .line 274
    const/4 v3, 0x1

    iput v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mExecutionStep:I

    .line 276
    iput-object p4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    .line 277
    iput-object p2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUri:Ljava/lang/String;

    .line 278
    const/4 v2, 0x0

    .line 279
    .local v2, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 280
    .local v1, "passWord":Ljava/lang/String;
    const/4 v0, 0x0

    .line 282
    .local v0, "domain":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    .end local v2    # "userName":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 283
    .restart local v2    # "userName":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    .end local v1    # "passWord":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 285
    .restart local v1    # "passWord":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 286
    new-instance v0, Ljava/lang/String;

    .end local v0    # "domain":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 288
    .restart local v0    # "domain":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    invoke-direct {v3, v2, v1, v0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .line 289
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->init()V

    .line 291
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;Z)V
    .locals 4
    .param p2, "_serverAddress"    # Ljava/lang/String;
    .param p3, "_userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p4, "_authToken"    # Ljava/lang/String;
    .param p5, "stepsForHttpGet"    # Z

    .prologue
    const/4 v3, 0x0

    .line 295
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 236
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    .line 238
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    .line 239
    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 242
    const-string v3, ""

    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mEmailAddress:Ljava/lang/String;

    .line 296
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mExecutionStep:I

    .line 297
    iput-object p4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    .line 298
    iput-object p2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mServerAddress:Ljava/lang/String;

    .line 299
    const/4 v2, 0x0

    .line 300
    .local v2, "userName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 301
    .local v1, "passWord":Ljava/lang/String;
    const/4 v0, 0x0

    .line 303
    .local v0, "domain":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    .end local v2    # "userName":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 304
    .restart local v2    # "userName":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    .end local v1    # "passWord":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 306
    .restart local v1    # "passWord":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 307
    new-instance v0, Ljava/lang/String;

    .end local v0    # "domain":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v0    # "domain":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    invoke-direct {v3, v2, v1, v0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .line 310
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->init()V

    .line 312
    return-void
.end method

.method static synthetic access$2500(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpPost;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpGet;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 268
    new-instance v0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {v0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .line 269
    return-void
.end method

.method private isRedirected()Z
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v0

    return v0
.end method

.method private isSuccessful()Z
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "autodiscover_host_auth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327
    const/4 v0, 0x1

    .line 329
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private postAutodiscover(Lorg/apache/http/HttpResponse;ZLcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)V
    .locals 8
    .param p1, "resp"    # Lorg/apache/http/HttpResponse;
    .param p2, "canRetry"    # Z
    .param p3, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p4, "mAutoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p5, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 528
    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isInterrupted()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 534
    .local v2, "httpCode":I
    const/16 v4, 0xc8

    if-ne v2, v4, :cond_2

    .line 535
    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseAutoDiscoverResponse(Lorg/apache/http/HttpResponse;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lorg/apache/http/client/methods/HttpPost;
    invoke-static {p1, p3, p4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->access$1700(Lorg/apache/http/HttpResponse;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lorg/apache/http/client/methods/HttpPost;

    goto :goto_0

    .line 543
    :cond_2
    const/16 v4, 0x1c3

    if-ne v2, v4, :cond_3

    .line 545
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->getRedirect(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    invoke-static {v4, p1}, Lcom/android/exchange/AutoDiscoverHandler;->access$1800(Lcom/android/exchange/AutoDiscoverHandler;Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v3

    .line 547
    .local v3, "newURL":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 549
    new-array v4, v6, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Posting autodiscover to redirect: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    .line 553
    invoke-direct {p0, v3, p3, p4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    goto :goto_0

    .line 561
    .end local v3    # "newURL":Ljava/lang/String;
    :cond_3
    const/16 v4, 0x12e

    if-eq v2, v4, :cond_4

    const/16 v4, 0x12d

    if-ne v2, v4, :cond_9

    .line 565
    :cond_4
    const-string v4, "Location"

    invoke-interface {p1, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 566
    .local v1, "header":Lorg/apache/http/Header;
    const/4 v3, 0x0

    .line 567
    .restart local v3    # "newURL":Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 568
    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 571
    :cond_5
    if-nez v3, :cond_6

    .line 573
    new-instance v4, Ljava/io/IOException;

    const-string v5, "No location header"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 575
    :cond_6
    invoke-virtual {v3, p5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 577
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Same location from prev"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 579
    :cond_7
    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 581
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Prefer to html page"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 585
    :cond_8
    invoke-direct {p0, v3, p3, p4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    goto :goto_0

    .line 594
    .end local v1    # "header":Lorg/apache/http/Header;
    .end local v3    # "newURL":Ljava/lang/String;
    :cond_9
    const/16 v4, 0x191

    if-ne v2, v4, :cond_c

    .line 596
    if-eqz p2, :cond_b

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 600
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 602
    .local v0, "atSignIndex":I
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 607
    const-string v4, "outlook"

    invoke-virtual {p5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 608
    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mOriginalUserName:Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/AutoDiscoverHandler;->access$2000()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 610
    :cond_a
    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->getAuthToken(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler;->access$2100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    .line 612
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "401 received; trying username: "

    aput-object v5, v4, v7

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    .line 618
    invoke-direct {p0, p5, p3, p4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    goto/16 :goto_0

    .line 624
    .end local v0    # "atSignIndex":I
    :cond_b
    new-instance v4, Lcom/android/emailcommon/mail/MessagingException;

    const/4 v5, 0x5

    invoke-direct {v4, v5}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v4

    .line 633
    :cond_c
    new-array v4, v6, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", throwing IOException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    .line 635
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4
.end method

.method private tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    .locals 12
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p3, "mAutoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->TIMEOUT:I
    invoke-static {}, Lcom/android/exchange/AutoDiscoverHandler;->access$1200()I

    move-result v2

    const/4 v3, 0x1

    const/16 v4, 0x1bb

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->getHttpClient(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-static {v0, v2, v3, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1300(Lcom/android/exchange/AutoDiscoverHandler;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v6

    .line 413
    .local v6, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 416
    const-string v10, "<?xml version=\'1.0\' encoding=\'UTF-8\' standalone=\'no\' ?><Autodiscover xmlns=\"http://schemas.microsoft.com/exchange/autodiscover/mobilesync/requestschema/2006\"><Request><EMailAddress>%s</EMailAddress><AcceptableResponseSchema>http://schemas.microsoft.com/exchange/autodiscover/mobilesync/responseschema/2006</AcceptableResponseSchema></Request></Autodiscover>"

    .line 417
    .local v10, "reqTempl":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 418
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mEmailAddress:Ljava/lang/String;

    .line 421
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mEmailAddress:Ljava/lang/String;

    aput-object v3, v0, v2

    invoke-static {v10, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 423
    .local v9, "req":Ljava/lang/String;
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    .line 425
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "Authorization"

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAuthString:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "MS-ASProtocolVersion"

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mProtocolVersion:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/exchange/AutoDiscoverHandler;->access$1400(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "Connection"

    const-string v3, "keep-alive"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "User-Agent"

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "Content-Type"

    const-string v3, "text/xml"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "Accept"

    const-string v3, "text/xml, text/html"

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 433
    new-instance v0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread$1;

    invoke-direct {v0, p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread$1;-><init>(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)V

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 445
    :try_start_0
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost to URL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 448
    .local v1, "resp":Lorg/apache/http/HttpResponse;
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost successfull : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljavax/net/ssl/SSLProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 492
    :goto_1
    if-eqz v1, :cond_0

    .line 493
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parsing response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->postAutodiscover(Lorg/apache/http/HttpResponse;ZLcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)V

    .line 495
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Parsing response successful: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 498
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HttpPost aborted forcefully: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 449
    .end local v1    # "resp":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v7

    .line 450
    .local v7, "e":Ljavax/net/ssl/SSLProtocolException;
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost SSLProtocolException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 453
    .end local v7    # "e":Ljavax/net/ssl/SSLProtocolException;
    :catch_1
    move-exception v7

    .line 454
    .local v7, "e":Ljavax/net/ssl/SSLException;
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost got SSLError, trying to verifying ssl cert : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-class v2, Lcom/android/exchange/AutoDiscoverHandler;

    monitor-enter v2

    .line 458
    :try_start_1
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got the lock, verifying SSL Certificate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v0}, Lcom/android/exchange/AutoDiscoverHandler;->access$900(Lcom/android/exchange/AutoDiscoverHandler;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsAutoDiscoverCancelledByUser:Z
    invoke-static {v0}, Lcom/android/exchange/AutoDiscoverHandler;->access$1500(Lcom/android/exchange/AutoDiscoverHandler;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 462
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/exchange/AutoDiscoverHandler;->access$1600(Lcom/android/exchange/AutoDiscoverHandler;)Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, p1, v3}, Lcom/android/exchange/utility/SSLCertVerificationHandler;->verify(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    move-result-object v11

    .line 465
    .local v11, "result":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_VERIFIED_CONTINUE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    if-eq v11, v0, :cond_3

    .line 466
    const-string v0, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Verifying SSL Certificate is canceled by user or timmed out : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 470
    :try_start_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->closeSSLVerificationDialog()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 475
    :goto_2
    :try_start_3
    monitor-exit v2

    goto/16 :goto_0

    .line 478
    .end local v11    # "result":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 472
    .restart local v11    # "result":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :catch_2
    move-exception v8

    .line 473
    .local v8, "e1":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 478
    .end local v8    # "e1":Landroid/os/RemoteException;
    .end local v11    # "result":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :cond_3
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 480
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost ignoring SSL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->TIMEOUT:I
    invoke-static {}, Lcom/android/exchange/AutoDiscoverHandler;->access$1200()I

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x1bb

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->getHttpClient(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-static {v0, v2, v3, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1300(Lcom/android/exchange/AutoDiscoverHandler;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v6

    .line 482
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v6, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 483
    .restart local v1    # "resp":Lorg/apache/http/HttpResponse;
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost ignoring SSL successful : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 486
    .end local v1    # "resp":Lorg/apache/http/HttpResponse;
    .end local v7    # "e":Ljavax/net/ssl/SSLException;
    :catch_3
    move-exception v7

    .line 487
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending HttpPost IOException : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private tryRedirectedUri(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    .locals 10
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p3, "mAutoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 650
    const-string v0, "/autodiscover/autodiscover.xml"

    .line 652
    .local v0, "AUTO_DISCOVER_PAGE":Ljava/lang/String;
    :try_start_0
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sending httpGet : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http://autodiscover."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/autodiscover/autodiscover.xml"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    iput-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 655
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-static {v4}, Lcom/android/exchange/utility/HttpRedirect;->getRedirectURI(Lorg/apache/http/client/methods/HttpGet;)Ljava/net/URI;

    move-result-object v3

    .line 656
    .local v3, "redirectUri":Ljava/net/URI;
    if-eqz v3, :cond_1

    .line 657
    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    .line 658
    .local v2, "newUri":Ljava/lang/String;
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sending httpGet successfull, trying autodiscover with httpPost on new redirected URL : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 662
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    const/4 v5, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 664
    invoke-direct {p0, v2, p2, p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    .end local v2    # "newUri":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v4

    if-nez v4, :cond_0

    .line 684
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 685
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpGet aborted forcefully: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v4, v9}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 688
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 689
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 692
    .end local v3    # "redirectUri":Ljava/net/URI;
    :cond_0
    :goto_1
    return-void

    .line 666
    .restart local v3    # "redirectUri":Ljava/net/URI;
    :cond_1
    :try_start_1
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 670
    .end local v3    # "redirectUri":Ljava/net/URI;
    :catch_0
    move-exception v1

    .line 671
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 683
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v4

    if-nez v4, :cond_0

    .line 684
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 685
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpGet aborted forcefully: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v4, v9}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 688
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 689
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_1

    .line 674
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 675
    .local v1, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_3
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 683
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v4

    if-nez v4, :cond_0

    .line 684
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 685
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpGet aborted forcefully: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v4, v9}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 688
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 689
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto/16 :goto_1

    .line 678
    .end local v1    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :catch_2
    move-exception v1

    .line 679
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 683
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v4

    if-nez v4, :cond_0

    .line 684
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 685
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpGet aborted forcefully: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v4, v9}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 688
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 689
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto/16 :goto_1

    .line 683
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v5

    if-nez v5, :cond_2

    .line 684
    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 685
    const-string v5, "AutoDiscoverHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HttpGet aborted forcefully: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v5, v9}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 688
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 689
    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v5

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v5, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    :cond_2
    throw v4
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x1

    .line 338
    const-string v2, "AutoDiscoverHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Running: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :try_start_0
    iget v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mExecutionStep:I

    if-ne v2, v5, :cond_3

    .line 341
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUri:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {p0, v2, v3, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryAutoDiscoverPost(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_f

    .line 363
    :goto_0
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_1
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 365
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 367
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_1
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 389
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_2
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removed from autodiscoverThreads "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 395
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_3
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 397
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_2
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    .line 402
    :goto_2
    return-void

    .line 342
    :cond_3
    :try_start_4
    iget v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mExecutionStep:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    .line 343
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mServerAddress:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {p0, v2, v3, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->tryRedirectedUri(Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_f

    goto/16 :goto_0

    .line 349
    :catch_0
    move-exception v1

    .line 350
    .local v1, "ie":Ljava/io/IOException;
    :try_start_5
    const-string v2, "Email"

    const-string v3, "AutoDiscoverHandler: IOException"

    invoke-static {v2, v3, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 351
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "autodiscover_error_code"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_f

    .line 363
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_6
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 365
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 367
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_5
    :goto_3
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    .line 389
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_7
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removed from autodiscoverThreads "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_8

    .line 395
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_8
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 397
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_6
    monitor-exit v3

    goto/16 :goto_2

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v2

    .line 363
    .end local v1    # "ie":Ljava/io/IOException;
    :cond_7
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_9
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 365
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 367
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_8
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_9
    :goto_4
    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 389
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_a
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removed from autodiscoverThreads "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 395
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_b
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 397
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_a
    monitor-exit v3

    goto/16 :goto_2

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    throw v2

    .line 371
    :cond_b
    :try_start_c
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 372
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_4

    .line 387
    :catchall_2
    move-exception v2

    monitor-exit v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v2

    .line 377
    :cond_c
    :try_start_d
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 378
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_9

    .line 380
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autodiscover_error_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto/16 :goto_4

    .line 392
    :catchall_3
    move-exception v2

    :try_start_e
    monitor-exit v3
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    throw v2

    .line 371
    :cond_d
    :try_start_f
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 372
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto/16 :goto_1

    .line 387
    :catchall_4
    move-exception v2

    monitor-exit v3
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    throw v2

    .line 377
    :cond_e
    :try_start_10
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 378
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_1

    .line 380
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autodiscover_error_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    goto/16 :goto_1

    .line 392
    :catchall_5
    move-exception v2

    :try_start_11
    monitor-exit v3
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    throw v2

    .line 399
    :catchall_6
    move-exception v2

    :try_start_12
    monitor-exit v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    throw v2

    .line 371
    .restart local v1    # "ie":Ljava/io/IOException;
    :cond_f
    :try_start_13
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 372
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto/16 :goto_3

    .line 387
    :catchall_7
    move-exception v2

    monitor-exit v3
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_7

    throw v2

    .line 377
    :cond_10
    :try_start_14
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 378
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_5

    .line 380
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autodiscover_error_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    goto/16 :goto_3

    .line 392
    :catchall_8
    move-exception v2

    :try_start_15
    monitor-exit v3
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    throw v2

    .line 354
    .end local v1    # "ie":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 355
    .local v0, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_16
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "autodiscover_error_code"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_f

    .line 363
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_17
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 365
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 367
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_11
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_12
    :goto_5
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_a

    .line 389
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_18
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removed from autodiscoverThreads "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    .line 395
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_19
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 397
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_13
    monitor-exit v3

    goto/16 :goto_2

    :catchall_9
    move-exception v2

    monitor-exit v3
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_9

    throw v2

    .line 371
    :cond_14
    :try_start_1a
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 372
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_5

    .line 387
    :catchall_a
    move-exception v2

    monitor-exit v3
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_a

    throw v2

    .line 377
    :cond_15
    :try_start_1b
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 378
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_12

    .line 380
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autodiscover_error_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_a

    goto/16 :goto_5

    .line 392
    :catchall_b
    move-exception v2

    :try_start_1c
    monitor-exit v3
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_b

    throw v2

    .line 358
    .end local v0    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :catch_2
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1d
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "autodiscover_error_code"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_f

    .line 363
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_1e
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_19

    .line 365
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 367
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_16
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v4, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_17
    :goto_6
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_d

    .line 389
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_1f
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "removed from autodiscoverThreads "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_e

    .line 395
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_20
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 397
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_18
    monitor-exit v3

    goto/16 :goto_2

    :catchall_c
    move-exception v2

    monitor-exit v3
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_c

    throw v2

    .line 371
    :cond_19
    :try_start_21
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 372
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v2, v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_6

    .line 387
    :catchall_d
    move-exception v2

    monitor-exit v3
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_d

    throw v2

    .line 377
    :cond_1a
    :try_start_22
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 378
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v6, :cond_17

    .line 380
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "autodiscover_error_code"

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "autodiscover_error_code"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_d

    goto/16 :goto_6

    .line 392
    :catchall_e
    move-exception v2

    :try_start_23
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_e

    throw v2

    .line 363
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_f
    move-exception v2

    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v3}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 364
    :try_start_24
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 365
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 366
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 367
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v5

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 368
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v5

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 370
    :cond_1b
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    const/4 v5, 0x1

    # setter for: Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler;->access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z

    .line 387
    :cond_1c
    :goto_7
    monitor-exit v3
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_10

    .line 389
    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v3

    monitor-enter v3

    .line 390
    :try_start_25
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 391
    const-string v4, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removed from autodiscoverThreads "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    monitor-exit v3
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_11

    .line 395
    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v3}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v3

    monitor-enter v3

    .line 396
    :try_start_26
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isSuccessful()Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 397
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 399
    :cond_1d
    monitor-exit v3
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_12

    throw v2

    .line 371
    :cond_1e
    :try_start_27
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->isRedirected()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 372
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$600(Lcom/android/exchange/AutoDiscoverHandler;)V

    .line 373
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 374
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v5

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {v4, v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_7

    .line 387
    :catchall_10
    move-exception v2

    monitor-exit v3
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_10

    throw v2

    .line 377
    :cond_1f
    :try_start_28
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 378
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v6, :cond_1c

    .line 380
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->this$0:Lcom/android/exchange/AutoDiscoverHandler;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    move-result-object v4

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "autodiscover_error_code"

    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "autodiscover_error_code"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_10

    goto/16 :goto_7

    .line 392
    :catchall_11
    move-exception v2

    :try_start_29
    monitor-exit v3
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_11

    throw v2

    .line 399
    :catchall_12
    move-exception v2

    :try_start_2a
    monitor-exit v3
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_12

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "  AutoDiscoverThread [ Context "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    if-eqz v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUserInfoThread:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    if-eqz v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUri:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 258
    const-string v1, " url "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_2
    const-string v1, " execution step "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mExecutionStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
