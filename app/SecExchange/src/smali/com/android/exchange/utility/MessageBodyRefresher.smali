.class public Lcom/android/exchange/utility/MessageBodyRefresher;
.super Lcom/android/exchange/EasLoadMoreSvc;
.source "MessageBodyRefresher.java"


# static fields
.field private static final dateFormat:Ljava/text/SimpleDateFormat;

.field private static final downloadDir:Ljava/io/File;


# instance fields
.field private isMIMEDataEnabled:Z

.field private isSaveDataEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/exchange/utility/MessageBodyRefresher;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 45
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/utility/MessageBodyRefresher;->downloadDir:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZZ)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3, "withMIMEData"    # Z
    .param p4, "saveAsFile"    # Z

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasLoadMoreSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 48
    iput-boolean v0, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isMIMEDataEnabled:Z

    .line 49
    iput-boolean v0, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isSaveDataEnabled:Z

    .line 54
    iput-boolean p3, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isMIMEDataEnabled:Z

    .line 55
    iput-boolean p4, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isSaveDataEnabled:Z

    .line 56
    return-void
.end method


# virtual methods
.method public getIsSaveMode()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isSaveDataEnabled:Z

    return v0
.end method

.method public getSaveFilePath()Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v7, 0x32

    .line 178
    const/16 v2, 0x32

    .line 180
    .local v2, "max_email_subject_len":I
    sget-object v5, Lcom/android/exchange/utility/MessageBodyRefresher;->downloadDir:Ljava/io/File;

    if-nez v5, :cond_0

    .line 181
    const/4 v5, 0x0

    .line 208
    :goto_0
    return-object v5

    .line 183
    :cond_0
    sget-object v5, Lcom/android/exchange/utility/MessageBodyRefresher;->downloadDir:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 185
    iget-object v5, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    if-nez v5, :cond_2

    sget-object v5, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    const v6, 0x7f060023

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 188
    .local v3, "subject":Ljava/lang/String;
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v7, :cond_1

    .line 189
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 193
    :cond_1
    const-string v5, "[\\|\\?\\*\\\"\\:\\<\\>\\.\\&\\,\\\\]"

    const-string v6, "_"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 195
    const-string v4, ""

    .line 197
    .local v4, "tempDate":Ljava/lang/String;
    :try_start_0
    sget-object v5, Lcom/android/exchange/utility/MessageBodyRefresher;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v6, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 203
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 204
    .local v1, "filename":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v6, 0x2f

    const/16 v7, 0x20

    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v5, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isMIMEDataEnabled:Z

    if-eqz v5, :cond_3

    const-string v5, ".mime"

    :goto_3
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 206
    const-string v5, "MessageBodyRefresher"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FORMAT : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/android/exchange/utility/MessageBodyRefresher;->downloadDir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 185
    .end local v1    # "filename":Ljava/lang/String;
    .end local v3    # "subject":Ljava/lang/String;
    .end local v4    # "tempDate":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v3, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_1

    .line 198
    .restart local v3    # "subject":Ljava/lang/String;
    .restart local v4    # "tempDate":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "MessageBodyRefresher"

    const-string v6, "DateFormat error?"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 204
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "filename":Ljava/lang/String;
    :cond_3
    const-string v5, ".txt"

    goto :goto_3
.end method

.method protected prepareCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;
    .locals 7
    .param p1, "login"    # Ljava/lang/String;
    .param p2, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x22

    const/16 v5, 0x12

    const/16 v4, 0xd

    .line 61
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 63
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mIs2007support:Z

    if-eqz v1, :cond_7

    .line 67
    const/16 v1, 0x505

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 68
    const/16 v1, 0x506

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x507

    const-string v3, "Mailbox"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 70
    iget-object v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 71
    iget-object v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 72
    iget-object v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 77
    :cond_0
    :goto_0
    const/16 v1, 0x508

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v6, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 80
    const/16 v1, 0x445

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x446

    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isMIMEDataEnabled:Z

    if-eqz v1, :cond_6

    const-string v1, "4"

    :goto_1
    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 84
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 86
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mIs2010support:Z

    if-eqz v1, :cond_1

    .line 87
    const/16 v1, 0x514

    invoke-virtual {v0, v1, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 88
    const/16 v1, 0x515

    invoke-virtual {v0, v1, p2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 90
    :cond_1
    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mIs2010EXsupport:Z

    if-eqz v1, :cond_2

    .line 91
    const/16 v1, 0x605

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 93
    :cond_2
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 95
    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mIs2010EXsupport:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mRemoveIrmProtectionFlag:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 96
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_3

    .line 97
    const-string v1, "MessageBodyRefresher"

    const-string v2, "Removing irm, adding tag "

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_3
    const/16 v1, 0x618

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 100
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 102
    :cond_4
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 126
    :goto_2
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 127
    return-object v0

    .line 73
    :cond_5
    iget-object v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 74
    const/16 v1, 0x3d8

    iget-object v2, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_0

    .line 80
    :cond_6
    const-string v1, "2"

    goto :goto_1

    .line 107
    :cond_7
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 108
    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 109
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x10

    const-string v3, "Email"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 113
    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x23

    const-string v3, "8"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    iget-boolean v1, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->isMIMEDataEnabled:Z

    if-eqz v1, :cond_8

    const-string v1, "2"

    :goto_3
    invoke-virtual {v2, v6, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 118
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 120
    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 121
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/utility/MessageBodyRefresher;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 122
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 124
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_2

    .line 113
    :cond_8
    const-string v1, "0"

    goto :goto_3
.end method

.method public saveAsText(Ljava/lang/String;)V
    .locals 10
    .param p1, "msgData"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/android/exchange/utility/MessageBodyRefresher;->getSaveFilePath()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "_path":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v3, "file":Ljava/io/File;
    const/4 v4, 0x0

    .line 143
    .local v4, "in":Ljava/io/ByteArrayInputStream;
    const/4 v6, 0x0

    .line 147
    .local v6, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 149
    .local v1, "byteArr":[B
    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .local v5, "in":Ljava/io/ByteArrayInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    invoke-direct {v7, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 152
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .local v7, "out":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {v5, v7}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 154
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 155
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 161
    if-eqz v5, :cond_2

    .line 162
    :try_start_3
    invoke-virtual {v5}, Ljava/io/ByteArrayInputStream;->close()V

    .line 163
    :cond_2
    if-eqz v7, :cond_3

    .line 164
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 171
    .end local v1    # "byteArr":[B
    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    :cond_4
    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 172
    sget-object v8, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Utility;->registerSensitiveFileWithSdpIfNecessary(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 165
    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "byteArr":[B
    .restart local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v2

    .line 167
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 169
    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    goto :goto_1

    .line 157
    .end local v1    # "byteArr":[B
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 158
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 161
    if-eqz v4, :cond_5

    .line 162
    :try_start_5
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->close()V

    .line 163
    :cond_5
    if-eqz v6, :cond_4

    .line 164
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 165
    :catch_2
    move-exception v2

    .line 167
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 161
    :goto_3
    if-eqz v4, :cond_6

    .line 162
    :try_start_6
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->close()V

    .line 163
    :cond_6
    if-eqz v6, :cond_7

    .line 164
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 168
    :cond_7
    :goto_4
    throw v8

    .line 165
    :catch_3
    move-exception v2

    .line 167
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 160
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v1    # "byteArr":[B
    .restart local v5    # "in":Ljava/io/ByteArrayInputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    goto :goto_3

    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    goto :goto_3

    .line 157
    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "in":Ljava/io/ByteArrayInputStream;
    :catch_4
    move-exception v2

    move-object v4, v5

    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    goto :goto_2

    .end local v4    # "in":Ljava/io/ByteArrayInputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v2

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "in":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "in":Ljava/io/ByteArrayInputStream;
    goto :goto_2
.end method

.method public updateMessageBody(Lcom/android/emailcommon/provider/EmailContent$Body;)V
    .locals 13
    .param p1, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v12, 0x7f06001a

    const/4 v11, 0x1

    const v10, 0x32000

    const/4 v9, 0x0

    .line 213
    const-string v6, "MessageBodyRefresher"

    const-string v7, "updateMessageBody start"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 218
    .local v4, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 220
    .local v1, "cvBody":Landroid/content/ContentValues;
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 221
    :cond_0
    const-string v6, "MessageBodyRefresher"

    const-string v7, "updateEmail empty HTML content"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v6, "htmlContent"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v11, :cond_2

    .line 226
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 228
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    const-string v7, "\r\n"

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    .line 229
    .local v5, "value":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    .line 230
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v6, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 233
    :cond_1
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v6, :cond_4

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v10, :cond_4

    .line 235
    iget v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 236
    const-string v6, "textContent"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    sget-object v8, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v10, v8, v9}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v6, "fileSaveFlags"

    iget v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 243
    :goto_0
    const/4 v0, 0x0

    .line 244
    .local v0, "_nullStr":Ljava/lang/String;
    const-string v6, "htmlContent"

    invoke-virtual {v1, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .end local v0    # "_nullStr":Ljava/lang/String;
    .end local v5    # "value":I
    :cond_2
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-nez v6, :cond_3

    .line 249
    const-string v6, "MessageBodyRefresher"

    const-string v7, "There is no MIME parsed Bodyparts."

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v6, ""

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 272
    :cond_3
    :goto_1
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mId:J

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    invoke-virtual {p0}, Lcom/android/exchange/utility/MessageBodyRefresher;->getSynchronizer()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 277
    :try_start_0
    invoke-virtual {p0}, Lcom/android/exchange/utility/MessageBodyRefresher;->isStopped()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 278
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 292
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 241
    .restart local v5    # "value":I
    :cond_4
    const-string v6, "textContent"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    .end local v5    # "value":I
    :cond_5
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 257
    .local v3, "lastChar":C
    if-nez v3, :cond_6

    .line 258
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 260
    :cond_6
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v10, :cond_7

    .line 261
    iget v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 262
    const-string v6, "htmlContent"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    sget-object v8, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    invoke-virtual {v8, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v10, v8, v11}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const-string v6, "fileSaveFlags"

    iget v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 269
    :goto_2
    const/4 v6, 0x0

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    goto :goto_1

    .line 267
    :cond_7
    const-string v6, "htmlContent"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 282
    .end local v3    # "lastChar":C
    :cond_8
    :try_start_1
    sget-object v6, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    invoke-virtual {p1, v6}, Lcom/android/emailcommon/provider/EmailContent$Body;->saveBodyToFilesIfNecessary(Landroid/content/Context;)V

    .line 283
    sget-object v6, Lcom/android/exchange/utility/MessageBodyRefresher;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "com.android.email.provider"

    invoke-virtual {v6, v8, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 292
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 293
    const-string v6, "MessageBodyRefresher"

    const-string v7, "updateMessageBody finish"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    return-void

    .line 285
    :catch_0
    move-exception v2

    .line 286
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v6, "MessageBodyRefresher"

    invoke-static {v6, v2}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 287
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 288
    .end local v2    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v2

    .line 289
    .local v2, "e":Landroid/content/OperationApplicationException;
    const-string v6, "MessageBodyRefresher"

    invoke-static {v6, v2}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 290
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method
