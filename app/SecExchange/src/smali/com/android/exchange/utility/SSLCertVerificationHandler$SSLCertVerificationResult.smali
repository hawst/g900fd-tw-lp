.class public final enum Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
.super Ljava/lang/Enum;
.source "SSLCertVerificationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/utility/SSLCertVerificationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SSLCertVerificationResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

.field public static final enum SSL_VALIDATION_IO_ERROR:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

.field public static final enum SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

.field public static final enum SSL_VALIDATION_VERIFIED_CONTINUE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;


# instance fields
.field mvalue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 77
    new-instance v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    const-string v1, "SSL_VALIDATION_NOT_DONE"

    invoke-direct {v0, v1, v2, v2}, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    new-instance v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    const-string v1, "SSL_VALIDATION_VERIFIED_CONTINUE"

    invoke-direct {v0, v1, v3, v3}, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_VERIFIED_CONTINUE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    new-instance v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    const-string v1, "SSL_VALIDATION_IO_ERROR"

    invoke-direct {v0, v1, v4, v4}, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_IO_ERROR:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .line 76
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    sget-object v1, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_VERIFIED_CONTINUE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_IO_ERROR:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->$VALUES:[Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "_value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->mvalue:I

    .line 83
    iput p3, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->mvalue:I

    .line 84
    return-void
.end method

.method public static get(I)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 87
    packed-switch p0, :pswitch_data_0

    .line 95
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    :goto_0
    return-object v0

    .line 89
    :pswitch_0
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    goto :goto_0

    .line 91
    :pswitch_1
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_VERIFIED_CONTINUE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    goto :goto_0

    .line 93
    :pswitch_2
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_IO_ERROR:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    const-class v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    return-object v0
.end method

.method public static values()[Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->$VALUES:[Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    invoke-virtual {v0}, [Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    return-object v0
.end method
