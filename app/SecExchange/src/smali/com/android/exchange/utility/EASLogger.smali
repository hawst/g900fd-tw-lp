.class public Lcom/android/exchange/utility/EASLogger;
.super Ljava/lang/Object;
.source "EASLogger.java"


# static fields
.field public static VIEW_EAS_LOADMORE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "VIEW_EAS_LOADMORE_TIME"

    sput-object v0, Lcom/android/exchange/utility/EASLogger;->VIEW_EAS_LOADMORE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static debugStartTime(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/android/emailcommon/Logging;->prevTime:J

    sput-wide v0, Lcom/android/emailcommon/Logging;->startTime:J

    .line 14
    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " => debugStartTime()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/android/exchange/ExchangeService;->alwaysTagLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public static debugTime(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 19
    .local v0, "curTime":J
    sget-wide v6, Lcom/android/emailcommon/Logging;->prevTime:J

    sub-long v2, v0, v6

    .line 20
    .local v2, "debugTime":J
    sget-wide v6, Lcom/android/emailcommon/Logging;->startTime:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double v4, v6, v8

    .line 21
    .local v4, "totalTime":D
    sput-wide v0, Lcom/android/emailcommon/Logging;->prevTime:J

    .line 22
    const-wide/16 v6, 0x64

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    const-wide/16 v6, 0x12c

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    const/4 v6, 0x2

    :goto_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " => debugTime("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "), totalTime("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, p0, v7}, Lcom/android/exchange/ExchangeService;->alwaysTagLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 24
    return-void

    .line 22
    :cond_0
    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method
