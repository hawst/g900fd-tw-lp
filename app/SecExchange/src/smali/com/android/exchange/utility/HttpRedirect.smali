.class public Lcom/android/exchange/utility/HttpRedirect;
.super Ljava/lang/Object;
.source "HttpRedirect.java"


# static fields
.field private static redirectedURI:Ljava/net/URI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/utility/HttpRedirect;->redirectedURI:Ljava/net/URI;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Ljava/net/URI;)Ljava/net/URI;
    .locals 0
    .param p0, "x0"    # Ljava/net/URI;

    .prologue
    .line 14
    sput-object p0, Lcom/android/exchange/utility/HttpRedirect;->redirectedURI:Ljava/net/URI;

    return-object p0
.end method

.method public static getRedirectURI(Lorg/apache/http/client/methods/HttpGet;)Ljava/net/URI;
    .locals 3
    .param p0, "get"    # Lorg/apache/http/client/methods/HttpGet;

    .prologue
    .line 36
    const/4 v2, 0x0

    sput-object v2, Lcom/android/exchange/utility/HttpRedirect;->redirectedURI:Ljava/net/URI;

    .line 37
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 38
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v1, Lcom/android/exchange/utility/HttpRedirect$1CustomRedirectHanlder;

    invoke-direct {v1}, Lcom/android/exchange/utility/HttpRedirect$1CustomRedirectHanlder;-><init>()V

    .line 39
    .local v1, "handler":Lcom/android/exchange/utility/HttpRedirect$1CustomRedirectHanlder;
    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 41
    :try_start_0
    invoke-virtual {v0, p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    sget-object v2, Lcom/android/exchange/utility/HttpRedirect;->redirectedURI:Ljava/net/URI;

    return-object v2

    .line 42
    :catch_0
    move-exception v2

    goto :goto_0
.end method
