.class public Lcom/android/exchange/utility/CalendarUtilities;
.super Ljava/lang/Object;
.source "CalendarUtilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/utility/CalendarUtilities$RRule;,
        Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    }
.end annotation


# static fields
.field private static final UTC_TIMEZONE:Ljava/util/TimeZone;

.field private static mMinutes:I

.field static final sCurrentYear:I

.field static final sDayTokens:[Ljava/lang/String;

.field static final sGmtTimeZone:Ljava/util/TimeZone;

.field private static sTimeZoneCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field static final sTwoCharacterNumbers:[Ljava/lang/String;

.field static final sTypeToFreq:[Ljava/lang/String;

.field private static sTziStringCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/TimeZone;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sTimeZoneCache:Ljava/util/HashMap;

    .line 161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sTziStringCache:Ljava/util/HashMap;

    .line 163
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    .line 174
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "DAILY"

    aput-object v1, v0, v3

    const-string v1, "WEEKLY"

    aput-object v1, v0, v4

    const-string v1, "MONTHLY"

    aput-object v1, v0, v5

    const-string v1, "MONTHLY"

    aput-object v1, v0, v6

    const-string v1, ""

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "YEARLY"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "YEARLY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sTypeToFreq:[Ljava/lang/String;

    .line 178
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "SU"

    aput-object v1, v0, v3

    const-string v1, "MO"

    aput-object v1, v0, v4

    const-string v1, "TU"

    aput-object v1, v0, v5

    const-string v1, "WE"

    aput-object v1, v0, v6

    const-string v1, "TH"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "FR"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sDayTokens:[Ljava/lang/String;

    .line 182
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "00"

    aput-object v1, v0, v3

    const-string v1, "01"

    aput-object v1, v0, v4

    const-string v1, "02"

    aput-object v1, v0, v5

    const-string v1, "03"

    aput-object v1, v0, v6

    const-string v1, "04"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "05"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "06"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "07"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "08"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "09"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "10"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "11"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "12"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sTwoCharacterNumbers:[Ljava/lang/String;

    .line 199
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sput v0, Lcom/android/exchange/utility/CalendarUtilities;->sCurrentYear:I

    .line 201
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    .line 254
    sput v3, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    return-void
.end method

.method private static addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 4
    .param p0, "ics"    # Lcom/android/exchange/utility/SimpleIcsWriter;
    .param p2, "attendeeName"    # Ljava/lang/String;
    .param p3, "attendeeEmail"    # Ljava/lang/String;
    .param p4, "messageFlag"    # I
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/utility/SimpleIcsWriter;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/mail/Address;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/android/emailcommon/provider/EmailContent$Account;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2111
    .local p1, "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    and-int/lit8 v1, p4, 0x30

    if-eqz v1, :cond_5

    .line 2112
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2113
    .local v0, "icalTag":Ljava/lang/StringBuffer;
    and-int/lit8 v1, p4, 0x20

    if-eqz v1, :cond_0

    .line 2114
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2116
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_0
    if-eqz p2, :cond_1

    .line 2117
    const-string v1, ";CN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2119
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAILTO:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2120
    if-nez p2, :cond_3

    new-instance v1, Lcom/android/emailcommon/mail/Address;

    invoke-direct {v1, p3}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2122
    const-string v2, "STATUS"

    const/16 v1, 0x20

    if-ne p4, v1, :cond_4

    const-string v1, "CANCELLED"

    :goto_1
    invoke-virtual {p0, v2, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2145
    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_2
    :goto_2
    return-void

    .line 2120
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_3
    new-instance v1, Lcom/android/emailcommon/mail/Address;

    invoke-direct {v1, p3, p2}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2122
    :cond_4
    const-string v1, "TENTATIVE"

    goto :goto_1

    .line 2125
    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_5
    iget-object v1, p5, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2126
    const/4 v0, 0x0

    .line 2127
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    sparse-switch p4, :sswitch_data_0

    .line 2138
    :goto_3
    if-eqz v0, :cond_2

    .line 2139
    if-eqz p2, :cond_6

    .line 2140
    const-string v1, ";CN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p2}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2142
    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAILTO:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2129
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2130
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_3

    .line 2132
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=DECLINED"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2133
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_3

    .line 2135
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=TENTATIVE"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_3

    .line 2127
    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
        0x100 -> :sswitch_2
    .end sparse-switch
.end method

.method private static addByDay(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    .locals 4
    .param p0, "byDay"    # Ljava/lang/String;
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1470
    if-nez p0, :cond_0

    .line 1485
    :goto_0
    return-void

    .line 1472
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 1475
    .local v1, "weekOfMonth":I
    const/16 v2, 0x2d

    if-ne v1, v2, :cond_1

    .line 1477
    const/4 v1, 0x5

    .line 1478
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1483
    .local v0, "bareByDay":Ljava/lang/String;
    :goto_1
    const/16 v2, 0x122

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1484
    const/16 v2, 0x120

    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->generateEasDayOfWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_0

    .line 1480
    .end local v0    # "bareByDay":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, -0x30

    .line 1481
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "bareByDay":Ljava/lang/String;
    goto :goto_1
.end method

.method static addByDay(Ljava/lang/StringBuilder;II)V
    .locals 4
    .param p0, "rrule"    # Ljava/lang/StringBuilder;
    .param p1, "dow"    # I
    .param p2, "wom"    # I

    .prologue
    .line 1308
    const-string v2, ";BYDAY="

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1309
    const/4 v0, 0x0

    .line 1310
    .local v0, "addComma":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_4

    .line 1311
    and-int/lit8 v2, p1, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 1312
    if-eqz v0, :cond_0

    .line 1313
    const/16 v2, 0x2c

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1315
    :cond_0
    if-lez p2, :cond_1

    .line 1318
    const/4 v2, 0x5

    if-ne p2, v2, :cond_3

    const/4 v2, -0x1

    :goto_1
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1320
    :cond_1
    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sDayTokens:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1321
    const/4 v0, 0x1

    .line 1323
    :cond_2
    shr-int/lit8 p1, p1, 0x1

    .line 1310
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    move v2, p2

    .line 1318
    goto :goto_1

    .line 1325
    :cond_4
    return-void
.end method

.method private static addByDaySetpos(Ljava/lang/String;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    .locals 3
    .param p0, "byDay"    # Ljava/lang/String;
    .param p1, "bySetpos"    # Ljava/lang/String;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1490
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1491
    .local v0, "weekOfMonth":I
    const/16 v1, 0x2d

    if-ne v0, v1, :cond_0

    .line 1493
    const/4 v0, 0x5

    .line 1497
    :goto_0
    const/16 v1, 0x122

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1498
    const/16 v1, 0x120

    invoke-static {p0}, Lcom/android/exchange/utility/CalendarUtilities;->generateEasDayOfWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1499
    return-void

    .line 1495
    :cond_0
    add-int/lit8 v0, v0, -0x30

    goto :goto_0
.end method

.method static addByMonthDay(Ljava/lang/StringBuilder;I)V
    .locals 2
    .param p0, "rrule"    # Ljava/lang/StringBuilder;
    .param p1, "dom"    # I

    .prologue
    .line 1338
    const/16 v0, 0x7f

    if-ne p1, v0, :cond_0

    .line 1339
    const/4 p1, -0x1

    .line 1341
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ";BYMONTHDAY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1342
    return-void
.end method

.method static addBySetpos(Ljava/lang/StringBuilder;II)V
    .locals 1
    .param p0, "rrule"    # Ljava/lang/StringBuilder;
    .param p1, "dow"    # I
    .param p2, "wom"    # I

    .prologue
    .line 1331
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/StringBuilder;II)V

    .line 1332
    const-string v0, ";BYSETPOS="

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1333
    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    const-string v0, "-1"

    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1334
    return-void

    .line 1333
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private static addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    .locals 5
    .param p0, "rrule"    # Ljava/lang/String;
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1454
    const-string v3, "COUNT="

    invoke-static {p0, v3}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1455
    .local v0, "count":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1456
    const/16 v3, 0x11e

    invoke-virtual {p1, v3, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1458
    :cond_0
    const-string v3, "INTERVAL="

    invoke-static {p0, v3}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1459
    .local v1, "interval":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1460
    const/16 v3, 0x11f

    invoke-virtual {p1, v3, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1462
    :cond_1
    const-string v3, "UNTIL="

    invoke-static {p0, v3}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1463
    .local v2, "until":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 1464
    const/16 v3, 0x11d

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->recurrenceUntilToEasUntil(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1466
    :cond_2
    return-void
.end method

.method private static addForwardedAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 4
    .param p0, "ics"    # Lcom/android/exchange/utility/SimpleIcsWriter;
    .param p1, "fwdAttendeeName"    # Ljava/lang/String;
    .param p2, "fwdAttendeeEmail"    # Ljava/lang/String;
    .param p3, "messageFlag"    # I
    .param p4, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 2905
    and-int/lit8 v1, p3, 0x30

    if-eqz v1, :cond_3

    .line 2906
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2907
    .local v0, "icalTag":Ljava/lang/StringBuffer;
    and-int/lit8 v1, p3, 0x20

    if-eqz v1, :cond_0

    .line 2908
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2910
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_0
    if-eqz p1, :cond_1

    .line 2911
    const-string v1, ";CN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2913
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAILTO:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2934
    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    :cond_2
    :goto_0
    return-void

    .line 2914
    :cond_3
    iget-object v1, p4, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2915
    const/4 v0, 0x0

    .line 2916
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    sparse-switch p3, :sswitch_data_0

    .line 2927
    :goto_1
    if-eqz v0, :cond_2

    .line 2928
    if-eqz p1, :cond_4

    .line 2929
    const-string v1, ";CN="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-static {p1}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2931
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MAILTO:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2918
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2919
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_1

    .line 2921
    :sswitch_1
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=DECLINED"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2922
    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_1

    .line 2924
    :sswitch_2
    new-instance v0, Ljava/lang/StringBuffer;

    .end local v0    # "icalTag":Ljava/lang/StringBuffer;
    const-string v1, "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=TENTATIVE"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local v0    # "icalTag":Ljava/lang/StringBuffer;
    goto :goto_1

    .line 2916
    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
        0x100 -> :sswitch_2
    .end sparse-switch
.end method

.method static addUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    .locals 4
    .param p0, "rrule"    # Ljava/lang/String;
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1433
    const-string v2, "UNTIL="

    invoke-static {p0, v2}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1434
    .local v1, "until":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1435
    const/16 v2, 0x11d

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->recurrenceUntilToEasUntil(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1439
    :cond_0
    const-string v2, "COUNT="

    invoke-static {p0, v2}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1440
    .local v0, "occur":Ljava/lang/String;
    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    .line 1441
    const/16 v2, 0x11e

    invoke-virtual {p1, v2, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1443
    :cond_1
    return-void
.end method

.method static asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 1780
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static attendeeStatusFromBusyStatus(I)I
    .locals 1
    .param p0, "busyStatus"    # I

    .prologue
    .line 1838
    packed-switch p0, :pswitch_data_0

    .line 1848
    const/4 v0, 0x0

    .line 1850
    .local v0, "attendeeStatus":I
    :goto_0
    return v0

    .line 1840
    .end local v0    # "attendeeStatus":I
    :pswitch_0
    const/4 v0, 0x1

    .line 1841
    .restart local v0    # "attendeeStatus":I
    goto :goto_0

    .line 1843
    .end local v0    # "attendeeStatus":I
    :pswitch_1
    const/4 v0, 0x4

    .line 1844
    .restart local v0    # "attendeeStatus":I
    goto :goto_0

    .line 1838
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static buildMessageTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;Z)Ljava/lang/String;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entityValues"    # Landroid/content/ContentValues;
    .param p2, "sb"    # Ljava/lang/StringBuilder;
    .param p3, "meetingForward"    # Z

    .prologue
    .line 1992
    if-nez p2, :cond_0

    .line 1993
    new-instance p2, Ljava/lang/StringBuilder;

    .end local p2    # "sb":Ljava/lang/StringBuilder;
    invoke-direct/range {p2 .. p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1995
    .restart local p2    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 2016
    .local v10, "resources":Landroid/content/res/Resources;
    const/4 v3, 0x0

    .line 2017
    .local v3, "allDayEvent":Z
    const-string v11, "allDay"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 2018
    const-string v11, "allDay"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 2019
    .local v2, "ade":Ljava/lang/Integer;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    const/4 v14, 0x1

    if-ne v11, v14, :cond_4

    const/4 v3, 0x1

    .line 2021
    .end local v2    # "ade":Ljava/lang/Integer;
    :cond_1
    :goto_0
    const-string v11, "original_sync_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5

    const-string v11, "rrule"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    const/4 v8, 0x1

    .line 2026
    .local v8, "recurringEvent":Z
    :goto_1
    const-string v11, "dtstart"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 2027
    .local v12, "startTime":J
    if-eqz v3, :cond_7

    .line 2028
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v11

    invoke-static {v12, v13, v11}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v14

    invoke-direct {v4, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 2029
    .local v4, "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 2030
    .local v5, "dateTimeString":Ljava/lang/String;
    if-eqz v8, :cond_6

    const v9, 0x7f06000d

    .line 2035
    .end local v4    # "date":Ljava/util/Date;
    .local v9, "res":I
    :goto_2
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v5, v11, v14

    invoke-virtual {v10, v9, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2037
    const/4 v7, 0x0

    .line 2038
    .local v7, "location":Ljava/lang/String;
    const-string v11, "eventLocation"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2039
    const-string v11, "eventLocation"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2040
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 2041
    const/16 v11, 0xa

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2042
    const v11, 0x7f06000a

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-virtual {v10, v11, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2046
    :cond_2
    if-nez p3, :cond_3

    .line 2047
    const-string v11, "description"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2048
    .local v6, "desc":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 2049
    const-string v11, "\n--\n"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2050
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2053
    .end local v6    # "desc":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 2019
    .end local v5    # "dateTimeString":Ljava/lang/String;
    .end local v7    # "location":Ljava/lang/String;
    .end local v8    # "recurringEvent":Z
    .end local v9    # "res":I
    .end local v12    # "startTime":J
    .restart local v2    # "ade":Ljava/lang/Integer;
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2021
    .end local v2    # "ade":Ljava/lang/Integer;
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 2030
    .restart local v4    # "date":Ljava/util/Date;
    .restart local v5    # "dateTimeString":Ljava/lang/String;
    .restart local v8    # "recurringEvent":Z
    .restart local v12    # "startTime":J
    :cond_6
    const v9, 0x7f06000c

    goto :goto_2

    .line 2032
    .end local v4    # "date":Ljava/util/Date;
    .end local v5    # "dateTimeString":Ljava/lang/String;
    :cond_7
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v11

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14, v12, v13}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v11, v14}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 2033
    .restart local v5    # "dateTimeString":Ljava/lang/String;
    if-eqz v8, :cond_8

    const v9, 0x7f06000b

    .restart local v9    # "res":I
    :goto_3
    goto :goto_2

    .end local v9    # "res":I
    :cond_8
    const v9, 0x7f060009

    goto :goto_3
.end method

.method public static buildProposeNewTimeTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entityValues"    # Landroid/content/ContentValues;
    .param p2, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v8, 0xa

    .line 2059
    if-nez p2, :cond_0

    .line 2060
    new-instance p2, Ljava/lang/StringBuilder;

    .end local p2    # "sb":Ljava/lang/StringBuilder;
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2062
    .restart local p2    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2063
    .local v4, "resources":Landroid/content/res/Resources;
    new-instance v0, Ljava/util/Date;

    const-string v6, "PROPOSED_START_TIME"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 2065
    .local v0, "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2066
    .local v1, "dateTimeString":Ljava/lang/String;
    const v6, 0x7f060007

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2067
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2068
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2069
    const-string v6, " - "

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2071
    new-instance v0, Ljava/util/Date;

    .end local v0    # "date":Ljava/util/Date;
    const-string v6, "PROPOSED_END_TIME"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 2072
    .restart local v0    # "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2073
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2074
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    .line 2075
    .local v5, "timeZone":Ljava/util/TimeZone;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2077
    const/4 v3, 0x0

    .line 2078
    .local v3, "location":Ljava/lang/String;
    const-string v6, "LOC"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2079
    const-string v6, "LOC"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2080
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2081
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2082
    const v6, 0x7f06000a

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2086
    :cond_1
    const-string v6, "description"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2087
    .local v2, "desc":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 2088
    const-string v6, "\n--\n"

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2089
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2091
    :cond_2
    const-string v6, "\n\n"

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2092
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static busyStatusFromAvailabilityStatus(I)I
    .locals 1
    .param p0, "availabilityStatus"    # I

    .prologue
    .line 1968
    packed-switch p0, :pswitch_data_0

    .line 1982
    const/4 v0, 0x2

    .line 1985
    .local v0, "busyStatus":I
    :goto_0
    return v0

    .line 1970
    .end local v0    # "busyStatus":I
    :pswitch_0
    const/4 v0, 0x0

    .line 1971
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1973
    .end local v0    # "busyStatus":I
    :pswitch_1
    const/4 v0, 0x1

    .line 1974
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1976
    .end local v0    # "busyStatus":I
    :pswitch_2
    const/4 v0, 0x2

    .line 1977
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1979
    .end local v0    # "busyStatus":I
    :pswitch_3
    const/4 v0, 0x3

    .line 1980
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1968
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static busyStatusFromSelfAttendeeStatus(I)I
    .locals 1
    .param p0, "selfAttendeeStatus"    # I

    .prologue
    .line 1893
    packed-switch p0, :pswitch_data_0

    .line 1904
    :pswitch_0
    const/4 v0, 0x0

    .line 1906
    .local v0, "busyStatus":I
    :goto_0
    return v0

    .line 1895
    .end local v0    # "busyStatus":I
    :pswitch_1
    const/4 v0, 0x2

    .line 1896
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1898
    .end local v0    # "busyStatus":I
    :pswitch_2
    const/4 v0, 0x1

    .line 1899
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1901
    .end local v0    # "busyStatus":I
    :pswitch_3
    const/4 v0, 0x0

    .line 1902
    .restart local v0    # "busyStatus":I
    goto :goto_0

    .line 1893
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static calendarToBirthdayString(Ljava/util/GregorianCalendar;)Ljava/lang/String;
    .locals 3
    .param p0, "cal"    # Ljava/util/GregorianCalendar;

    .prologue
    const/16 v2, 0x2d

    .line 1128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1129
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1130
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1131
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1133
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1134
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static createCalendar(Lcom/android/exchange/EasSyncService;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)J
    .locals 9
    .param p0, "service"    # Lcom/android/exchange/EasSyncService;
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 1718
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1726
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v4, "calendar_displayName"

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1727
    const-string v4, "account_name"

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    const-string v4, "account_type"

    const-string v5, "com.android.exchange"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1730
    const-string v4, "sync_events"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1734
    const-string v4, "visible"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1738
    const-string v4, "canOrganizerRespond"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1739
    const-string v4, "canModifyTimeZone"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1740
    const-string v4, "maxReminders"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1741
    const-string v4, "allowedReminders"

    const-string v5, "0,1"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    const-string v4, "allowedAttendeeTypes"

    const-string v5, "0,1,2"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    const-string v4, "allowedAvailability"

    const-string v5, "0,1,2"

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    const v0, -0xffff01

    .line 1750
    .local v0, "color":I
    :try_start_0
    new-instance v4, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    iget-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v4, v6, v7}, Lcom/android/emailcommon/service/AccountServiceProxy;->getAccountColor(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1757
    :goto_0
    const-string v4, "calendar_color"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1758
    const-string v4, "calendar_timezone"

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    const-string v4, "calendar_access_level"

    const/16 v5, 0x2bc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1761
    const-string v4, "ownerAccount"

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v7, "com.android.exchange"

    invoke-static {v5, v6, v7}, Lcom/android/exchange/utility/CalendarUtilities;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 1770
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 1771
    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1772
    .local v2, "stringId":Ljava/lang/String;
    iput-object v2, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncStatus:Ljava/lang/String;

    .line 1773
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 1775
    .end local v2    # "stringId":Ljava/lang/String;
    :goto_1
    return-wide v4

    :cond_0
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 1751
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "messageFlag"    # I
    .param p3, "uid"    # Ljava/lang/String;
    .param p4, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 2161
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method public static createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "messageFlag"    # I
    .param p3, "uid"    # Ljava/lang/String;
    .param p4, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p5, "msgId"    # J

    .prologue
    .line 2167
    invoke-static {p0, p5, p6}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v1

    .line 2168
    .local v1, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v1, :cond_0

    iget-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 2169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2172
    :cond_0
    if-eqz v1, :cond_1

    .line 2173
    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v7

    .line 2174
    .local v7, "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v7, :cond_1

    .line 2175
    const/4 v8, 0x0

    .local v8, "itr":I
    :goto_0
    array-length v0, v7

    if-ge v8, v0, :cond_1

    .line 2176
    aget-object v0, v7, v8

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 2177
    iget-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    aget-object v2, v7, v8

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2175
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 2181
    .end local v7    # "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v8    # "itr":I
    :cond_1
    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v6}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method public static createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "messageFlag"    # I
    .param p3, "uid"    # Ljava/lang/String;
    .param p4, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p5, "specifiedAttendee"    # Ljava/lang/String;

    .prologue
    .line 2188
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method public static createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J
    .param p3, "messageFlag"    # I
    .param p4, "uid"    # Ljava/lang/String;
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3444
    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v1 .. v7}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    return-object v0
.end method

.method public static createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J
    .param p3, "messageFlag"    # I
    .param p4, "uid"    # Ljava/lang/String;
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p6, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 3450
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 3452
    .local v2, "cr":Landroid/content/ContentResolver;
    move-wide v0, p1

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getReminder(JLandroid/content/ContentResolver;)V

    .line 3453
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-wide v0, p1

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v11

    .line 3457
    .local v11, "eventIterator":Landroid/content/EntityIterator;
    move-wide/from16 v0, p6

    invoke-static {p0, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v4

    .line 3458
    .local v4, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v4, :cond_0

    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v3, :cond_0

    .line 3459
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 3464
    :cond_0
    if-eqz v4, :cond_2

    .line 3465
    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 3466
    move-wide/from16 v0, p6

    invoke-static {p0, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    .line 3467
    .local v12, "html":Ljava/lang/String;
    iput-object v12, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 3471
    .end local v12    # "html":Ljava/lang/String;
    :cond_1
    iget-wide v6, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {p0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v10

    .line 3472
    .local v10, "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v10, :cond_2

    .line 3473
    const/4 v13, 0x0

    .local v13, "itr":I
    :goto_0
    array-length v3, v10

    if-ge v13, v3, :cond_2

    .line 3474
    aget-object v3, v10, v13

    const-wide/16 v6, 0x0

    iput-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 3475
    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    aget-object v6, v10, v13

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3473
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 3480
    .end local v10    # "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v13    # "itr":I
    :cond_2
    :try_start_0
    invoke-interface {v11}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3481
    invoke-interface {v11}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Entity;

    .line 3482
    .local v5, "entity":Landroid/content/Entity;
    const/4 v9, 0x0

    move-object v3, p0

    move/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-static/range {v3 .. v9}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 3485
    invoke-interface {v11}, Landroid/content/EntityIterator;->close()V

    .line 3487
    .end local v5    # "entity":Landroid/content/Entity;
    :goto_1
    return-object v3

    .line 3485
    :cond_3
    invoke-interface {v11}, Landroid/content/EntityIterator;->close()V

    .line 3487
    const/4 v3, 0x0

    goto :goto_1

    .line 3485
    :catchall_0
    move-exception v3

    invoke-interface {v11}, Landroid/content/EntityIterator;->close()V

    throw v3
.end method

.method public static createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J
    .param p3, "messageFlag"    # I
    .param p4, "uid"    # Ljava/lang/String;
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p6, "specifiedAttendee"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3494
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3496
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-static {p1, p2, v0}, Lcom/android/exchange/utility/CalendarUtilities;->getReminder(JLandroid/content/ContentResolver;)V

    .line 3506
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v7

    .line 3511
    .local v7, "eventIterator":Landroid/content/EntityIterator;
    :try_start_0
    invoke-interface {v7}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3512
    invoke-interface {v7}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Entity;

    .local v2, "entity":Landroid/content/Entity;
    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 3513
    invoke-static/range {v1 .. v6}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 3517
    .end local v2    # "entity":Landroid/content/Entity;
    invoke-interface {v7}, Landroid/content/EntityIterator;->close()V

    .line 3519
    :goto_0
    return-object v2

    .line 3517
    :cond_0
    invoke-interface {v7}, Landroid/content/EntityIterator;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/content/EntityIterator;->close()V

    throw v1
.end method

.method static findNextTransition(J[Ljava/util/GregorianCalendar;)J
    .locals 8
    .param p0, "startingMillis"    # J
    .param p2, "transitions"    # [Ljava/util/GregorianCalendar;

    .prologue
    .line 790
    move-object v0, p2

    .local v0, "arr$":[Ljava/util/GregorianCalendar;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 791
    .local v3, "transition":Ljava/util/GregorianCalendar;
    invoke-virtual {v3}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    .line 792
    .local v4, "transitionMillis":J
    cmp-long v6, v4, p0

    if-lez v6, :cond_0

    .line 796
    .end local v3    # "transition":Ljava/util/GregorianCalendar;
    .end local v4    # "transitionMillis":J
    :goto_1
    return-wide v4

    .line 790
    .restart local v3    # "transition":Ljava/util/GregorianCalendar;
    .restart local v4    # "transitionMillis":J
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 796
    .end local v3    # "transition":Ljava/util/GregorianCalendar;
    .end local v4    # "transitionMillis":J
    :cond_1
    const-wide/16 v4, 0x0

    goto :goto_1
.end method

.method static findTransitionDate(Ljava/util/TimeZone;JJZ)Ljava/util/GregorianCalendar;
    .locals 15
    .param p0, "tz"    # Ljava/util/TimeZone;
    .param p1, "startTime"    # J
    .param p3, "endTime"    # J
    .param p5, "startInDaylightTime"    # Z

    .prologue
    .line 428
    move-wide/from16 v8, p3

    .line 429
    .local v8, "startingEndTime":J
    const/4 v3, 0x0

    .line 432
    .local v3, "date":Ljava/util/Date;
    :goto_0
    sub-long v10, p3, p1

    const-wide/32 v12, 0xea60

    cmp-long v7, v10, v12

    if-lez v7, :cond_1

    .line 433
    add-long v10, p1, p3

    const-wide/16 v12, 0x2

    div-long/2addr v10, v12

    const-wide/16 v12, 0x1

    add-long v4, v10, v12

    .line 434
    .local v4, "checkTime":J
    new-instance v3, Ljava/util/Date;

    .end local v3    # "date":Ljava/util/Date;
    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 435
    .restart local v3    # "date":Ljava/util/Date;
    invoke-virtual {p0, v3}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v6

    .line 436
    .local v6, "inDaylightTime":Z
    move/from16 v0, p5

    if-eq v6, v0, :cond_0

    .line 437
    move-wide/from16 p3, v4

    goto :goto_0

    .line 439
    :cond_0
    move-wide/from16 p1, v4

    goto :goto_0

    .line 444
    .end local v4    # "checkTime":J
    .end local v6    # "inDaylightTime":Z
    :cond_1
    cmp-long v7, p3, v8

    if-nez v7, :cond_2

    .line 445
    const/4 v2, 0x0

    .line 451
    :goto_1
    return-object v2

    .line 449
    :cond_2
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2, p0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 450
    .local v2, "calendar":Ljava/util/GregorianCalendar;
    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    goto :goto_1
.end method

.method public static formatDateTime(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 4
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    const/16 v3, 0x3a

    const/16 v2, 0x2d

    .line 3594
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3596
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 3597
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3598
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3599
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3600
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3601
    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3602
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3603
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3604
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3605
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3606
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3607
    const-string v1, ".000Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3608
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static formatTwo(I)Ljava/lang/String;
    .locals 1
    .param p0, "num"    # I

    .prologue
    .line 1105
    const/16 v0, 0xc

    if-gt p0, v0, :cond_0

    .line 1106
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sTwoCharacterNumbers:[Ljava/lang/String;

    aget-object v0, v0, p0

    .line 1108
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static generateEasDayOfWeek(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "dow"    # Ljava/lang/String;

    .prologue
    .line 1352
    const/4 v2, 0x0

    .line 1353
    .local v2, "bits":I
    const/4 v1, 0x1

    .line 1354
    .local v1, "bit":I
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sDayTokens:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 1357
    .local v5, "token":Ljava/lang/String;
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_0

    .line 1358
    or-int/2addr v2, v1

    .line 1360
    :cond_0
    shl-int/lit8 v1, v1, 0x1

    .line 1354
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1362
    .end local v5    # "token":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public static getAvailabilityStatus(Landroid/content/Context;J)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2983
    const/4 v6, 0x2

    .line 2984
    .local v6, "busyStatus":I
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "availabilityStatus"

    aput-object v0, v2, v8

    .line 2987
    .local v2, "AVAILABILITY_STATUS_PROJECTION":[Ljava/lang/String;
    const-string v3, "_id = ?"

    .line 2988
    .local v3, "CONDITION_FOR_AVAILABILITY":Ljava/lang/String;
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2990
    .local v1, "EVENTS_URI":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v4, v9, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2995
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 2997
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2998
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->busyStatusFromAvailabilityStatus(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 3002
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 3005
    :cond_1
    return v6

    .line 3002
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static getDSTCalendars(Ljava/util/TimeZone;[Ljava/util/GregorianCalendar;[Ljava/util/GregorianCalendar;)Z
    .locals 18
    .param p0, "tz"    # Ljava/util/TimeZone;
    .param p1, "toDaylightCalendars"    # [Ljava/util/GregorianCalendar;
    .param p2, "toStandardCalendars"    # [Ljava/util/GregorianCalendar;

    .prologue
    .line 650
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    .line 651
    .local v17, "maxYears":I
    move-object/from16 v0, p2

    array-length v3, v0

    move/from16 v0, v17

    if-eq v3, v0, :cond_0

    .line 652
    const/4 v3, 0x0

    .line 682
    :goto_0
    return v3

    .line 655
    :cond_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_6

    .line 656
    new-instance v2, Ljava/util/GregorianCalendar;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 657
    .local v2, "cal":Ljava/util/GregorianCalendar;
    sget v3, Lcom/android/exchange/utility/CalendarUtilities;->sCurrentYear:I

    add-int v3, v3, v16

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 658
    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    .line 660
    .local v4, "startTime":J
    const-wide v10, 0x757b12c00L

    add-long/2addr v10, v4

    const-wide/32 v12, 0x1499700

    add-long v6, v10, v12

    .line 661
    .local v6, "endOfYearTime":J
    new-instance v15, Ljava/util/Date;

    invoke-direct {v15, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 662
    .local v15, "date":Ljava/util/Date;
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v8

    .local v8, "startInDaylightTime":Z
    move-object/from16 v3, p0

    .line 664
    invoke-static/range {v3 .. v8}, Lcom/android/exchange/utility/CalendarUtilities;->findTransitionDate(Ljava/util/TimeZone;JJZ)Ljava/util/GregorianCalendar;

    move-result-object v2

    .line 665
    if-nez v2, :cond_1

    .line 666
    const/4 v3, 0x0

    goto :goto_0

    .line 667
    :cond_1
    if-eqz v8, :cond_2

    .line 668
    aput-object v2, p2, v16

    .line 673
    :goto_2
    if-nez v8, :cond_3

    const/4 v14, 0x1

    :goto_3
    move-object/from16 v9, p0

    move-wide v10, v4

    move-wide v12, v6

    invoke-static/range {v9 .. v14}, Lcom/android/exchange/utility/CalendarUtilities;->findTransitionDate(Ljava/util/TimeZone;JJZ)Ljava/util/GregorianCalendar;

    move-result-object v2

    .line 674
    if-nez v2, :cond_4

    .line 675
    const/4 v3, 0x0

    goto :goto_0

    .line 670
    :cond_2
    aput-object v2, p1, v16

    goto :goto_2

    .line 673
    :cond_3
    const/4 v14, 0x0

    goto :goto_3

    .line 676
    :cond_4
    if-eqz v8, :cond_5

    .line 677
    aput-object v2, p1, v16

    .line 655
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 679
    :cond_5
    aput-object v2, p2, v16

    goto :goto_4

    .line 682
    .end local v2    # "cal":Ljava/util/GregorianCalendar;
    .end local v4    # "startTime":J
    .end local v6    # "endOfYearTime":J
    .end local v8    # "startInDaylightTime":Z
    .end local v15    # "date":Ljava/util/Date;
    :cond_6
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public static getIntegerValueAsBoolean(Landroid/content/ContentValues;Ljava/lang/String;)Z
    .locals 2
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 3583
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 3584
    .local v0, "intValue":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J
    .locals 2
    .param p0, "time"    # J
    .param p2, "localTimeZone"    # Ljava/util/TimeZone;

    .prologue
    .line 1291
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    invoke-static {p0, p1, v0, p2}, Lcom/android/exchange/utility/CalendarUtilities;->transposeAllDayTime(JLjava/util/TimeZone;Ljava/util/TimeZone;)J

    move-result-wide v0

    return-wide v0
.end method

.method static getLong([BI)I
    .locals 3
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 260
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 p1, v0, 0x1

    .end local v0    # "offset":I
    .restart local p1    # "offset":I
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .restart local v0    # "offset":I
    aget-byte v2, p0, p1

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v1, v2

    return v1
.end method

.method static getMillisAtTimeZoneDateTransition(Ljava/util/TimeZone;Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;)J
    .locals 4
    .param p0, "timeZone"    # Ljava/util/TimeZone;
    .param p1, "tzd"    # Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;

    .prologue
    .line 404
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 405
    .local v0, "testCalendar":Ljava/util/GregorianCalendar;
    const/4 v1, 0x1

    sget v2, Lcom/android/exchange/utility/CalendarUtilities;->sCurrentYear:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 406
    const/4 v1, 0x2

    iget v2, p1, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->month:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 407
    const/4 v1, 0x7

    iget v2, p1, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->dayOfWeek:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 408
    const/16 v1, 0x8

    iget v2, p1, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->day:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 409
    const/16 v1, 0xb

    iget v2, p1, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->hour:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 410
    const/16 v1, 0xc

    iget v2, p1, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->minute:I

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 411
    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->set(II)V

    .line 412
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private static getReminder(JLandroid/content/ContentResolver;)V
    .locals 8
    .param p0, "eventId"    # J
    .param p2, "mContentResolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3395
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "minutes"

    aput-object v0, v2, v5

    const-string v3, "event_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v5, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3402
    .local v6, "mRemindersCursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 3403
    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I

    .line 3404
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3405
    const-string v0, "minutes"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sput v0, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3415
    :goto_0
    if-eqz v6, :cond_0

    .line 3416
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 3420
    :cond_0
    return-void

    .line 3408
    :cond_1
    :try_start_1
    const-string v0, "CalendarUtility"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRemindersCursor is empty for eventId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3409
    const/4 v0, -0x1

    sput v0, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3415
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 3416
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 3412
    :cond_3
    :try_start_2
    const-string v0, "CalendarUtility"

    const-string v1, "mRemindersCursor is null"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static getString([BII)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "size"    # I

    .prologue
    .line 284
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 285
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_0

    .line 286
    add-int v3, p1, v1

    aget-byte v0, p0, v3

    .line 287
    .local v0, "ch":I
    if-nez v0, :cond_1

    .line 293
    .end local v0    # "ch":I
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 290
    .restart local v0    # "ch":I
    :cond_1
    int-to-char v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 285
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static getTimeZoneDateFromSystemTime([BI)Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 359
    new-instance v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;

    invoke-direct {v3}, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;-><init>()V

    .line 362
    .local v3, "tzd":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    add-int/lit8 v4, p1, 0x0

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v2

    .line 363
    .local v2, "num":I
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->year:Ljava/lang/String;

    .line 367
    add-int/lit8 v4, p1, 0x2

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v2

    .line 368
    if-nez v2, :cond_0

    .line 369
    const/4 v3, 0x0

    .line 393
    .end local v3    # "tzd":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    :goto_0
    return-object v3

    .line 371
    .restart local v3    # "tzd":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    :cond_0
    add-int/lit8 v4, v2, -0x1

    iput v4, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->month:I

    .line 375
    add-int/lit8 v4, p1, 0x4

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->dayOfWeek:I

    .line 378
    add-int/lit8 v4, p1, 0x6

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v2

    .line 380
    const/4 v4, 0x5

    if-ne v2, v4, :cond_1

    .line 381
    const/4 v4, -0x1

    iput v4, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->day:I

    .line 387
    :goto_1
    add-int/lit8 v4, p1, 0x8

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v0

    .line 388
    .local v0, "hour":I
    iput v0, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->hour:I

    .line 389
    add-int/lit8 v4, p1, 0xa

    invoke-static {p0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->getWord([BI)I

    move-result v1

    .line 390
    .local v1, "minute":I
    iput v1, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->minute:I

    .line 391
    const v4, 0x36ee80

    mul-int/2addr v4, v0

    const v5, 0xea60

    mul-int/2addr v5, v1

    add-int/2addr v4, v5

    iput v4, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->time:I

    goto :goto_0

    .line 383
    .end local v0    # "hour":I
    .end local v1    # "minute":I
    :cond_1
    iput v2, v3, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->day:I

    goto :goto_1
.end method

.method static getTrueTransitionHour(Ljava/util/GregorianCalendar;)I
    .locals 2
    .param p0, "calendar"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 1242
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 1243
    .local v0, "hour":I
    add-int/lit8 v0, v0, 0x1

    .line 1244
    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    .line 1245
    const/4 v0, 0x0

    .line 1247
    :cond_0
    return v0
.end method

.method static getTrueTransitionMinute(Ljava/util/GregorianCalendar;)I
    .locals 2
    .param p0, "calendar"    # Ljava/util/GregorianCalendar;

    .prologue
    .line 1227
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    .line 1228
    .local v0, "minute":I
    const/16 v1, 0x3b

    if-ne v0, v1, :cond_0

    .line 1229
    const/4 v0, 0x0

    .line 1231
    :cond_0
    return v0
.end method

.method public static getUidFromGlobalObjId(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "globalObjId"    # Ljava/lang/String;

    .prologue
    .line 1794
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1797
    .local v8, "sb":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    :try_start_0
    invoke-static {p0, v9}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    .line 1798
    .local v4, "idBytes":[B
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>([B)V

    .line 1804
    .local v5, "idString":Ljava/lang/String;
    const-string v9, "vCal-Uid"

    invoke-virtual {v5, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 1805
    .local v6, "index":I
    if-lez v6, :cond_0

    .line 1809
    add-int/lit8 v9, v6, 0xc

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v5, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1824
    .end local v4    # "idBytes":[B
    .end local v5    # "idString":Ljava/lang/String;
    .end local v6    # "index":I
    .end local p0    # "globalObjId":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 1816
    .restart local v4    # "idBytes":[B
    .restart local v5    # "idString":Ljava/lang/String;
    .restart local v6    # "index":I
    .restart local p0    # "globalObjId":Ljava/lang/String;
    :cond_0
    move-object v0, v4

    .local v0, "arr$":[B
    array-length v7, v0

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v7, :cond_1

    aget-byte v1, v0, v3

    .line 1817
    .local v1, "b":B
    invoke-static {v8, v1}, Lcom/android/emailcommon/utility/Utility;->byteToHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;

    .line 1816
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1819
    .end local v1    # "b":B
    :cond_1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 1821
    .end local v0    # "arr$":[B
    .end local v3    # "i$":I
    .end local v4    # "idBytes":[B
    .end local v5    # "idString":Ljava/lang/String;
    .end local v6    # "index":I
    .end local v7    # "len$":I
    :catch_0
    move-exception v2

    .line 1824
    .local v2, "e":Ljava/lang/RuntimeException;
    goto :goto_0
.end method

.method public static getUtcAllDayCalendarTime(JLjava/util/TimeZone;)J
    .locals 2
    .param p0, "time"    # J
    .param p2, "localTimeZone"    # Ljava/util/TimeZone;

    .prologue
    .line 1280
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    invoke-static {p0, p1, p2, v0}, Lcom/android/exchange/utility/CalendarUtilities;->transposeAllDayTime(JLjava/util/TimeZone;Ljava/util/TimeZone;)J

    move-result-wide v0

    return-wide v0
.end method

.method static getWord([BI)I
    .locals 3
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 274
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    return v1
.end method

.method static inferRRuleFromCalendars([Ljava/util/GregorianCalendar;)Lcom/android/exchange/utility/CalendarUtilities$RRule;
    .locals 14
    .param p0, "calendars"    # [Ljava/util/GregorianCalendar;

    .prologue
    .line 549
    const/4 v12, 0x0

    aget-object v1, p0, v12

    .line 550
    .local v1, "calendar":Ljava/util/GregorianCalendar;
    if-nez v1, :cond_0

    .line 551
    const/4 v12, 0x0

    .line 602
    :goto_0
    return-object v12

    .line 552
    :cond_0
    const/4 v12, 0x2

    invoke-virtual {v1, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v8

    .line 553
    .local v8, "month":I
    const/4 v12, 0x5

    invoke-virtual {v1, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    .line 554
    .local v2, "date":I
    const/4 v12, 0x7

    invoke-virtual {v1, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    .line 555
    .local v4, "dayOfWeek":I
    const/16 v12, 0x8

    invoke-virtual {v1, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v11

    .line 556
    .local v11, "week":I
    const/16 v12, 0x8

    invoke-virtual {v1, v12}, Ljava/util/GregorianCalendar;->getActualMaximum(I)I

    move-result v7

    .line 557
    .local v7, "maxWeek":I
    const/4 v3, 0x0

    .line 558
    .local v3, "dateRule":Z
    const/4 v5, 0x0

    .line 559
    .local v5, "dayOfWeekRule":Z
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_1
    array-length v12, p0

    if-ge v6, v12, :cond_a

    .line 560
    aget-object v0, p0, v6

    .line 561
    .local v0, "cal":Ljava/util/GregorianCalendar;
    if-nez v0, :cond_1

    .line 562
    const/4 v12, 0x0

    goto :goto_0

    .line 564
    :cond_1
    const/4 v12, 0x2

    invoke-virtual {v0, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v12

    if-eq v12, v8, :cond_2

    .line 565
    const/4 v12, 0x0

    goto :goto_0

    .line 566
    :cond_2
    const/4 v12, 0x7

    invoke-virtual {v0, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v12

    if-ne v4, v12, :cond_7

    .line 568
    if-eqz v3, :cond_3

    .line 569
    const/4 v12, 0x0

    goto :goto_0

    .line 571
    :cond_3
    const/4 v5, 0x1

    .line 572
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v10

    .line 573
    .local v10, "thisWeek":I
    if-eq v11, v10, :cond_5

    .line 574
    if-ltz v11, :cond_4

    if-ne v11, v7, :cond_6

    .line 575
    :cond_4
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, Ljava/util/GregorianCalendar;->getActualMaximum(I)I

    move-result v9

    .line 576
    .local v9, "thisMaxWeek":I
    if-ne v10, v9, :cond_6

    .line 578
    const/4 v11, -0x1

    .line 559
    .end local v9    # "thisMaxWeek":I
    .end local v10    # "thisWeek":I
    :cond_5
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 582
    .restart local v10    # "thisWeek":I
    :cond_6
    const/4 v12, 0x0

    goto :goto_0

    .line 584
    .end local v10    # "thisWeek":I
    :cond_7
    const/4 v12, 0x5

    invoke-virtual {v0, v12}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v12

    if-ne v2, v12, :cond_9

    .line 586
    if-eqz v5, :cond_8

    .line 587
    const/4 v12, 0x0

    goto :goto_0

    .line 589
    :cond_8
    const/4 v3, 0x1

    goto :goto_2

    .line 591
    :cond_9
    const/4 v12, 0x0

    goto :goto_0

    .line 595
    .end local v0    # "cal":Ljava/util/GregorianCalendar;
    :cond_a
    if-eqz v3, :cond_b

    .line 596
    new-instance v12, Lcom/android/exchange/utility/CalendarUtilities$RRule;

    add-int/lit8 v13, v8, 0x1

    invoke-direct {v12, v13, v2}, Lcom/android/exchange/utility/CalendarUtilities$RRule;-><init>(II)V

    goto :goto_0

    .line 602
    :cond_b
    new-instance v12, Lcom/android/exchange/utility/CalendarUtilities$RRule;

    add-int/lit8 v13, v8, 0x1

    invoke-direct {v12, v13, v4, v11}, Lcom/android/exchange/utility/CalendarUtilities$RRule;-><init>(III)V

    goto :goto_0
.end method

.method public static isMeetingForwardWithInline(JLandroid/content/Context;)I
    .locals 18
    .param p0, "msgId"    # J
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v14, 0x1

    .line 2946
    const/4 v10, 0x0

    .line 2947
    .local v10, "isMeetingForwardEmail":Z
    const/4 v9, 0x0

    .line 2948
    .local v9, "isMeetingForwardCalendar":Z
    move-object/from16 v0, p2

    move-wide/from16 v1, p0

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v13

    .line 2949
    .local v13, "tempMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v13, :cond_1

    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    if-eqz v15, :cond_1

    .line 2950
    new-instance v12, Lcom/android/emailcommon/mail/PackedString;

    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    invoke-direct {v12, v15}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 2951
    .local v12, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    const-string v15, "MEETING_FORWARD"

    invoke-virtual {v12, v15}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2952
    .local v6, "frwd":Ljava/lang/String;
    if-eqz v6, :cond_0

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    if-ne v15, v14, :cond_0

    .line 2953
    const/4 v9, 0x1

    .line 2955
    :cond_0
    iget v15, v13, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit8 v15, v15, 0x2

    if-eqz v15, :cond_1

    if-nez v9, :cond_1

    .line 2956
    const/4 v10, 0x1

    .line 2959
    .end local v6    # "frwd":Ljava/lang/String;
    .end local v12    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    :cond_1
    const/4 v8, 0x0

    .line 2960
    .local v8, "isInline":Z
    move-object/from16 v0, p2

    move-wide/from16 v1, p0

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v5

    .line 2961
    .local v5, "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    move-object v3, v5

    .local v3, "arr$":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    array-length v11, v3

    .local v11, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v11, :cond_2

    aget-object v4, v3, v7

    .line 2962
    .local v4, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v15, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v15, :cond_3

    .line 2963
    const/4 v8, 0x1

    .line 2967
    .end local v4    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_2
    if-eqz v10, :cond_4

    if-eqz v8, :cond_4

    .line 2972
    :goto_1
    return v14

    .line 2961
    .restart local v4    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 2969
    .end local v4    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_4
    if-eqz v9, :cond_5

    if-eqz v8, :cond_5

    .line 2970
    const/4 v14, 0x2

    goto :goto_1

    .line 2972
    :cond_5
    const/4 v14, -0x1

    goto :goto_1
.end method

.method public static millisToEasDateTime(J)Ljava/lang/String;
    .locals 2
    .param p0, "millis"    # J

    .prologue
    .line 1116
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 4
    .param p0, "millis"    # J
    .param p2, "tz"    # Ljava/util/TimeZone;
    .param p3, "withTime"    # Z

    .prologue
    .line 1156
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1157
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1158
    .local v0, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1159
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1160
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1161
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1162
    if-eqz p3, :cond_1

    .line 1163
    const/16 v2, 0x54

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1164
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1165
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1166
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1167
    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1172
    :cond_0
    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1175
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static millisToEasInstanaceId(JLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 6
    .param p0, "millis"    # J
    .param p2, "tz"    # Ljava/util/TimeZone;
    .param p3, "withTime"    # Z

    .prologue
    const/16 v4, 0x3a

    const/16 v3, 0x2d

    .line 1191
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1192
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1193
    .local v0, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1194
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1195
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1196
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1197
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1198
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1199
    if-eqz p3, :cond_1

    .line 1200
    const/16 v2, 0x54

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1201
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1202
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1203
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1204
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1205
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1207
    :cond_0
    const-string v2, ".000Z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1210
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static millisToInstanceId(J)Ljava/lang/String;
    .locals 2
    .param p0, "millis"    # J

    .prologue
    .line 1139
    sget-object v0, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasInstanaceId(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static putRuleIntoTimeZoneInformation([BILcom/android/exchange/utility/CalendarUtilities$RRule;II)V
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "rrule"    # Lcom/android/exchange/utility/CalendarUtilities$RRule;
    .param p3, "hour"    # I
    .param p4, "minute"    # I

    .prologue
    .line 324
    add-int/lit8 v0, p1, 0x2

    iget v1, p2, Lcom/android/exchange/utility/CalendarUtilities$RRule;->month:I

    invoke-static {p0, v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 326
    add-int/lit8 v0, p1, 0x4

    iget v1, p2, Lcom/android/exchange/utility/CalendarUtilities$RRule;->dayOfWeek:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 328
    add-int/lit8 v1, p1, 0x6

    iget v0, p2, Lcom/android/exchange/utility/CalendarUtilities$RRule;->week:I

    if-gez v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    invoke-static {p0, v1, v0}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 330
    add-int/lit8 v0, p1, 0x8

    invoke-static {p0, v0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 331
    add-int/lit8 v0, p1, 0xa

    invoke-static {p0, v0, p4}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 332
    return-void

    .line 328
    :cond_0
    iget v0, p2, Lcom/android/exchange/utility/CalendarUtilities$RRule;->week:I

    goto :goto_0
.end method

.method static putTransitionMillisIntoSystemTime([BIJ)V
    .locals 4
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "millis"    # J

    .prologue
    .line 337
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 339
    .local v0, "cal":Ljava/util/GregorianCalendar;
    const-wide/16 v2, 0x7530

    add-long/2addr v2, p2

    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 342
    add-int/lit8 v2, p1, 0x2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {p0, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 344
    add-int/lit8 v2, p1, 0x4

    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {p0, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 347
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    .line 349
    .local v1, "wom":I
    add-int/lit8 v2, p1, 0x6

    if-gez v1, :cond_0

    const/4 v1, 0x5

    .end local v1    # "wom":I
    :cond_0
    invoke-static {p0, v2, v1}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 352
    add-int/lit8 v2, p1, 0x8

    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionHour(Ljava/util/GregorianCalendar;)I

    move-result v3

    invoke-static {p0, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 353
    add-int/lit8 v2, p1, 0xa

    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionMinute(Ljava/util/GregorianCalendar;)I

    move-result v3

    invoke-static {p0, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->setWord([BII)V

    .line 354
    return-void
.end method

.method public static recurrenceFromRrule(Ljava/lang/String;JLcom/android/exchange/adapter/Serializer;)V
    .locals 11
    .param p0, "rrule"    # Ljava/lang/String;
    .param p1, "startTime"    # J
    .param p3, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1516
    sget-boolean v8, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v8, :cond_0

    .line 1517
    const-string v8, "CalendarUtility"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RRULE: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    :cond_0
    const-string v8, "FREQ="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1524
    .local v6, "freq":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 1525
    const-string v8, "DAILY"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1526
    const/16 v8, 0x11b

    invoke-virtual {p3, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1527
    const/16 v8, 0x11c

    const-string v9, "0"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1536
    const-string v8, "INTERVAL="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1537
    .local v7, "interval":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 1538
    const/16 v8, 0x11f

    invoke-virtual {p3, v8, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1540
    :cond_1
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1541
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1628
    .end local v7    # "interval":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 1542
    :cond_3
    const-string v8, "WEEKLY"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1543
    const/16 v8, 0x11b

    invoke-virtual {p3, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1544
    const/16 v8, 0x11c

    const-string v9, "1"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1546
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1547
    const-string v8, "BYDAY="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1548
    .local v0, "byDay":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 1549
    const/16 v8, 0x120

    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->generateEasDayOfWeek(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1553
    const-string v8, "-1"

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1554
    const/16 v8, 0x122

    const-string v9, "5"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1562
    :cond_4
    :goto_1
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_0

    .line 1556
    :cond_5
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 1557
    .local v4, "c":C
    const/16 v8, 0x31

    if-lt v4, v8, :cond_4

    const/16 v8, 0x34

    if-gt v4, v8, :cond_4

    .line 1558
    const/16 v8, 0x122

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_1

    .line 1563
    .end local v0    # "byDay":Ljava/lang/String;
    .end local v4    # "c":C
    :cond_6
    const-string v8, "MONTHLY"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1564
    const-string v8, "BYMONTHDAY="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1565
    .local v2, "byMonthDay":Ljava/lang/String;
    if-eqz v2, :cond_8

    .line 1567
    const/16 v8, 0x11b

    invoke-virtual {p3, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1571
    const-string v8, "-1"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1572
    const/16 v8, 0x11c

    const-string v9, "3"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1573
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1574
    const/16 v8, 0x120

    const-string v9, "127"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1582
    :goto_2
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_0

    .line 1578
    :cond_7
    const/16 v8, 0x11c

    const-string v9, "2"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1579
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1580
    const/16 v8, 0x121

    invoke-virtual {p3, v8, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_2

    .line 1584
    :cond_8
    const-string v8, "BYDAY="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1585
    .restart local v0    # "byDay":Ljava/lang/String;
    const-string v8, "BYSETPOS="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1587
    .local v3, "bySetpos":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1588
    const/16 v8, 0x11b

    invoke-virtual {p3, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1589
    const/16 v8, 0x11c

    const-string v9, "3"

    invoke-virtual {p3, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1590
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1592
    if-eqz v3, :cond_9

    .line 1593
    invoke-static {v0, v3, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addByDaySetpos(Ljava/lang/String;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1597
    :goto_3
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_0

    .line 1595
    :cond_9
    invoke-static {v0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    goto :goto_3

    .line 1600
    .end local v0    # "byDay":Ljava/lang/String;
    .end local v2    # "byMonthDay":Ljava/lang/String;
    .end local v3    # "bySetpos":Ljava/lang/String;
    :cond_a
    const-string v8, "YEARLY"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1601
    const-string v8, "BYMONTH="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1602
    .local v1, "byMonth":Ljava/lang/String;
    const-string v8, "BYMONTHDAY="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1603
    .restart local v2    # "byMonthDay":Ljava/lang/String;
    const-string v8, "BYDAY="

    invoke-static {p0, v8}, Lcom/android/exchange/utility/CalendarUtilities;->tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1604
    .restart local v0    # "byDay":Ljava/lang/String;
    if-nez v1, :cond_b

    if-nez v2, :cond_b

    .line 1606
    new-instance v5, Ljava/util/GregorianCalendar;

    invoke-direct {v5}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1607
    .local v5, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v5, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1608
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1609
    const/4 v8, 0x2

    invoke-virtual {v5, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 1610
    const/4 v8, 0x5

    invoke-virtual {v5, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 1612
    .end local v5    # "cal":Ljava/util/GregorianCalendar;
    :cond_b
    if-eqz v1, :cond_2

    if-nez v2, :cond_c

    if-eqz v0, :cond_2

    .line 1613
    :cond_c
    const/16 v8, 0x11b

    invoke-virtual {p3, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1614
    const/16 v9, 0x11c

    if-nez v0, :cond_d

    const-string v8, "5"

    :goto_4
    invoke-virtual {p3, v9, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1615
    invoke-static {p0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addCountIntervalAndUntil(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 1616
    const/16 v8, 0x123

    invoke-virtual {p3, v8, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1619
    if-eqz v2, :cond_e

    .line 1620
    const/16 v8, 0x121

    invoke-virtual {p3, v8, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1624
    :goto_5
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_0

    .line 1614
    :cond_d
    const-string v8, "6"

    goto :goto_4

    .line 1622
    :cond_e
    invoke-static {v0, p3}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    goto :goto_5
.end method

.method static recurrenceUntilToEasUntil(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "until"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x8

    const/4 v10, 0x6

    const/4 v6, 0x4

    const/4 v4, 0x0

    .line 1402
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1403
    .local v8, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-gt v5, v11, :cond_0

    .line 1405
    invoke-virtual {p0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1406
    .local v1, "year":I
    invoke-virtual {p0, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v2, v5, -0x1

    .line 1407
    .local v2, "month":I
    invoke-virtual {p0, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1408
    .local v3, "day":I
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    .line 1409
    .local v7, "localTimeZone":Ljava/util/TimeZone;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, v7}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .local v0, "localCalendar":Ljava/util/GregorianCalendar;
    move v5, v4

    move v6, v4

    .line 1410
    invoke-virtual/range {v0 .. v6}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 1411
    new-instance v9, Ljava/util/GregorianCalendar;

    sget-object v4, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    invoke-direct {v9, v4}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1412
    .local v9, "utcCalendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1413
    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    sget-object v6, Lcom/android/exchange/utility/CalendarUtilities;->UTC_TIMEZONE:Ljava/util/TimeZone;

    const/4 v10, 0x1

    invoke-static {v4, v5, v6, v10}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v4

    .line 1422
    .end local v0    # "localCalendar":Ljava/util/GregorianCalendar;
    .end local v1    # "year":I
    .end local v2    # "month":I
    .end local v3    # "day":I
    .end local v7    # "localTimeZone":Ljava/util/TimeZone;
    .end local v9    # "utcCalendar":Ljava/util/GregorianCalendar;
    :goto_0
    return-object v4

    .line 1416
    :cond_0
    invoke-virtual {p0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1417
    invoke-virtual {p0, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1418
    invoke-virtual {p0, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1420
    const/16 v4, 0xd

    invoke-virtual {p0, v11, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1421
    const-string v4, "00Z"

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1422
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static rruleFromRecurrence(IIIIIIILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # I
    .param p1, "occurrences"    # I
    .param p2, "interval"    # I
    .param p3, "dow"    # I
    .param p4, "dom"    # I
    .param p5, "wom"    # I
    .param p6, "moy"    # I
    .param p7, "until"    # Ljava/lang/String;

    .prologue
    .line 1645
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FREQ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sTypeToFreq:[Ljava/lang/String;

    aget-object v2, v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1648
    .local v0, "rrule":Ljava/lang/StringBuilder;
    if-lez p1, :cond_0

    .line 1649
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ";COUNT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1651
    :cond_0
    if-lez p2, :cond_1

    .line 1652
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ";INTERVAL="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1656
    :cond_1
    packed-switch p0, :pswitch_data_0

    .line 1700
    :cond_2
    :goto_0
    :pswitch_0
    if-eqz p7, :cond_3

    .line 1701
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ";UNTIL="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1704
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1659
    :pswitch_1
    if-lez p3, :cond_2

    .line 1661
    invoke-static {v0, p3, p5}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/StringBuilder;II)V

    goto :goto_0

    .line 1665
    :pswitch_2
    if-lez p4, :cond_2

    .line 1666
    invoke-static {v0, p4}, Lcom/android/exchange/utility/CalendarUtilities;->addByMonthDay(Ljava/lang/StringBuilder;I)V

    goto :goto_0

    .line 1671
    :pswitch_3
    const/16 v1, 0x7f

    if-ne p3, v1, :cond_4

    .line 1672
    const-string v1, ";BYMONTHDAY=-1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1674
    :cond_4
    const/4 v1, 0x5

    if-eq p5, v1, :cond_5

    const/4 v1, 0x1

    if-ne p5, v1, :cond_7

    :cond_5
    const/16 v1, 0x3e

    if-eq p3, v1, :cond_6

    const/16 v1, 0x41

    if-ne p3, v1, :cond_7

    .line 1675
    :cond_6
    invoke-static {v0, p3, p5}, Lcom/android/exchange/utility/CalendarUtilities;->addBySetpos(Ljava/lang/StringBuilder;II)V

    goto :goto_0

    .line 1676
    :cond_7
    if-lez p3, :cond_2

    .line 1677
    invoke-static {v0, p3, p5}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/StringBuilder;II)V

    goto :goto_0

    .line 1680
    :pswitch_4
    if-lez p4, :cond_8

    .line 1681
    invoke-static {v0, p4}, Lcom/android/exchange/utility/CalendarUtilities;->addByMonthDay(Ljava/lang/StringBuilder;I)V

    .line 1682
    :cond_8
    if-lez p6, :cond_2

    .line 1683
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ";BYMONTH="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1687
    :pswitch_5
    if-lez p3, :cond_9

    .line 1688
    invoke-static {v0, p3, p5}, Lcom/android/exchange/utility/CalendarUtilities;->addByDay(Ljava/lang/StringBuilder;II)V

    .line 1689
    :cond_9
    if-lez p4, :cond_a

    .line 1690
    invoke-static {v0, p4}, Lcom/android/exchange/utility/CalendarUtilities;->addByMonthDay(Ljava/lang/StringBuilder;I)V

    .line 1691
    :cond_a
    if-lez p6, :cond_2

    .line 1692
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ";BYMONTH="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 1656
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static setLong([BII)V
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # I

    .prologue
    .line 266
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    and-int/lit16 v1, p2, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    .line 267
    add-int/lit8 p1, v0, 0x1

    .end local v0    # "offset":I
    .restart local p1    # "offset":I
    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 268
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .restart local v0    # "offset":I
    shr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    .line 269
    shr-int/lit8 v1, p2, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 270
    return-void
.end method

.method static setWord([BII)V
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "value"    # I

    .prologue
    .line 279
    add-int/lit8 v0, p1, 0x1

    .end local p1    # "offset":I
    .local v0, "offset":I
    and-int/lit16 v1, p2, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, p1

    .line 280
    shr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 281
    return-void
.end method

.method public static timeZoneToTziString(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 5
    .param p0, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 464
    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sTziStringCache:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 465
    .local v0, "tziString":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 466
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_0

    .line 467
    const-string v2, "CalendarUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TZI string for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " found in cache."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v1, v0

    .line 474
    .end local v0    # "tziString":Ljava/lang/String;
    .local v1, "tziString":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 472
    .end local v1    # "tziString":Ljava/lang/String;
    .restart local v0    # "tziString":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Lcom/android/exchange/utility/CalendarUtilities;->timeZoneToTziStringImpl(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    .line 473
    sget-object v2, Lcom/android/exchange/utility/CalendarUtilities;->sTziStringCache:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 474
    .end local v0    # "tziString":Ljava/lang/String;
    .restart local v1    # "tziString":Ljava/lang/String;
    goto :goto_0
.end method

.method static timeZoneToTziStringImpl(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 22
    .param p0, "tz"    # Ljava/util/TimeZone;

    .prologue
    .line 811
    const/16 v19, 0xac

    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v16, v0

    .line 812
    .local v16, "tziBytes":[B
    invoke-virtual/range {p0 .. p0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v19

    move/from16 v0, v19

    neg-int v10, v0

    .line 813
    .local v10, "standardBias":I
    const v19, 0xea60

    div-int v10, v10, v19

    .line 814
    const/16 v19, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v0, v1, v10}, Lcom/android/exchange/utility/CalendarUtilities;->setLong([BII)V

    .line 816
    invoke-virtual/range {p0 .. p0}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 817
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v14, v0, [Ljava/util/GregorianCalendar;

    .line 818
    .local v14, "toDaylightCalendars":[Ljava/util/GregorianCalendar;
    const/16 v19, 0x3

    move/from16 v0, v19

    new-array v15, v0, [Ljava/util/GregorianCalendar;

    .line 822
    .local v15, "toStandardCalendars":[Ljava/util/GregorianCalendar;
    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Lcom/android/exchange/utility/CalendarUtilities;->getDSTCalendars(Ljava/util/TimeZone;[Ljava/util/GregorianCalendar;[Ljava/util/GregorianCalendar;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 824
    invoke-static {v14}, Lcom/android/exchange/utility/CalendarUtilities;->inferRRuleFromCalendars([Ljava/util/GregorianCalendar;)Lcom/android/exchange/utility/CalendarUtilities$RRule;

    move-result-object v4

    .line 825
    .local v4, "daylightRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    invoke-static {v15}, Lcom/android/exchange/utility/CalendarUtilities;->inferRRuleFromCalendars([Ljava/util/GregorianCalendar;)Lcom/android/exchange/utility/CalendarUtilities$RRule;

    move-result-object v11

    .line 826
    .local v11, "standardRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    if-eqz v4, :cond_2

    iget v0, v4, Lcom/android/exchange/utility/CalendarUtilities$RRule;->type:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    if-eqz v11, :cond_2

    iget v0, v11, Lcom/android/exchange/utility/CalendarUtilities$RRule;->type:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 830
    const/16 v19, 0x44

    const/16 v20, 0x0

    aget-object v20, v15, v20

    invoke-static/range {v20 .. v20}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionHour(Ljava/util/GregorianCalendar;)I

    move-result v20

    const/16 v21, 0x0

    aget-object v21, v15, v21

    invoke-static/range {v21 .. v21}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionMinute(Ljava/util/GregorianCalendar;)I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v11, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->putRuleIntoTimeZoneInformation([BILcom/android/exchange/utility/CalendarUtilities$RRule;II)V

    .line 833
    const/16 v19, 0x98

    const/16 v20, 0x0

    aget-object v20, v14, v20

    invoke-static/range {v20 .. v20}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionHour(Ljava/util/GregorianCalendar;)I

    move-result v20

    const/16 v21, 0x0

    aget-object v21, v14, v21

    invoke-static/range {v21 .. v21}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionMinute(Ljava/util/GregorianCalendar;)I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-static {v0, v1, v4, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->putRuleIntoTimeZoneInformation([BILcom/android/exchange/utility/CalendarUtilities$RRule;II)V

    .line 852
    .end local v4    # "daylightRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    .end local v11    # "standardRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v5

    .line 853
    .local v5, "dstOffset":I
    const/16 v19, 0xa8

    neg-int v0, v5

    move/from16 v20, v0

    const v21, 0xea60

    div-int v20, v20, v21

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->setLong([BII)V

    .line 855
    .end local v5    # "dstOffset":I
    .end local v14    # "toDaylightCalendars":[Ljava/util/GregorianCalendar;
    .end local v15    # "toStandardCalendars":[Ljava/util/GregorianCalendar;
    :cond_1
    const/16 v19, 0x2

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v0, v1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v17

    .line 856
    .local v17, "tziEncodedBytes":[B
    new-instance v18, Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 857
    .local v18, "tziString":Ljava/lang/String;
    return-object v18

    .line 840
    .end local v17    # "tziEncodedBytes":[B
    .end local v18    # "tziString":Ljava/lang/String;
    .restart local v4    # "daylightRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    .restart local v11    # "standardRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    .restart local v14    # "toDaylightCalendars":[Ljava/util/GregorianCalendar;
    .restart local v15    # "toStandardCalendars":[Ljava/util/GregorianCalendar;
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 841
    .local v8, "now":J
    invoke-static {v8, v9, v15}, Lcom/android/exchange/utility/CalendarUtilities;->findNextTransition(J[Ljava/util/GregorianCalendar;)J

    move-result-wide v12

    .line 842
    .local v12, "standardTransition":J
    invoke-static {v8, v9, v14}, Lcom/android/exchange/utility/CalendarUtilities;->findNextTransition(J[Ljava/util/GregorianCalendar;)J

    move-result-wide v6

    .line 844
    .local v6, "daylightTransition":J
    const-wide/16 v20, 0x0

    cmp-long v19, v12, v20

    if-eqz v19, :cond_0

    const-wide/16 v20, 0x0

    cmp-long v19, v6, v20

    if-eqz v19, :cond_0

    .line 845
    const/16 v19, 0x44

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v0, v1, v12, v13}, Lcom/android/exchange/utility/CalendarUtilities;->putTransitionMillisIntoSystemTime([BIJ)V

    .line 847
    const/16 v19, 0x98

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-static {v0, v1, v6, v7}, Lcom/android/exchange/utility/CalendarUtilities;->putTransitionMillisIntoSystemTime([BIJ)V

    goto :goto_0
.end method

.method static timeZoneToVTimezone(Ljava/util/TimeZone;Lcom/android/exchange/utility/SimpleIcsWriter;)V
    .locals 14
    .param p0, "tz"    # Ljava/util/TimeZone;
    .param p1, "writer"    # Lcom/android/exchange/utility/SimpleIcsWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 714
    invoke-virtual {p0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v10

    const v11, 0xea60

    div-int v5, v10, v11

    .line 715
    .local v5, "rawOffsetMinutes":I
    invoke-static {v5}, Lcom/android/exchange/utility/CalendarUtilities;->utcOffsetString(I)Ljava/lang/String;

    move-result-object v6

    .line 718
    .local v6, "standardOffsetString":Ljava/lang/String;
    const-string v10, "BEGIN"

    const-string v11, "VTIMEZONE"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    const-string v10, "TZID"

    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    const-string v10, "X-LIC-LOCATION"

    invoke-virtual {p0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    invoke-virtual {p0}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v10

    if-nez v10, :cond_0

    .line 724
    invoke-static {p1, p0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->writeNoDST(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/TimeZone;Ljava/lang/String;)V

    .line 781
    :goto_0
    return-void

    .line 728
    :cond_0
    const/4 v4, 0x3

    .line 729
    .local v4, "maxYears":I
    new-array v8, v4, [Ljava/util/GregorianCalendar;

    .line 730
    .local v8, "toDaylightCalendars":[Ljava/util/GregorianCalendar;
    new-array v9, v4, [Ljava/util/GregorianCalendar;

    .line 731
    .local v9, "toStandardCalendars":[Ljava/util/GregorianCalendar;
    invoke-static {p0, v8, v9}, Lcom/android/exchange/utility/CalendarUtilities;->getDSTCalendars(Ljava/util/TimeZone;[Ljava/util/GregorianCalendar;[Ljava/util/GregorianCalendar;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 732
    invoke-static {p1, p0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->writeNoDST(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/TimeZone;Ljava/lang/String;)V

    goto :goto_0

    .line 736
    :cond_1
    invoke-static {v8}, Lcom/android/exchange/utility/CalendarUtilities;->inferRRuleFromCalendars([Ljava/util/GregorianCalendar;)Lcom/android/exchange/utility/CalendarUtilities$RRule;

    move-result-object v1

    .line 737
    .local v1, "daylightRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    invoke-static {v9}, Lcom/android/exchange/utility/CalendarUtilities;->inferRRuleFromCalendars([Ljava/util/GregorianCalendar;)Lcom/android/exchange/utility/CalendarUtilities$RRule;

    move-result-object v7

    .line 738
    .local v7, "standardRule":Lcom/android/exchange/utility/CalendarUtilities$RRule;
    invoke-virtual {p0}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v10

    const v11, 0xea60

    div-int/2addr v10, v11

    add-int/2addr v10, v5

    invoke-static {v10}, Lcom/android/exchange/utility/CalendarUtilities;->utcOffsetString(I)Ljava/lang/String;

    move-result-object v0

    .line 742
    .local v0, "daylightOffsetString":Ljava/lang/String;
    if-eqz v1, :cond_4

    if-eqz v7, :cond_4

    const/4 v2, 0x1

    .line 745
    .local v2, "hasRule":Z
    :goto_1
    const-string v10, "BEGIN"

    const-string v11, "DAYLIGHT"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    const-string v10, "TZOFFSETFROM"

    invoke-virtual {p1, v10, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    const-string v10, "TZOFFSETTO"

    invoke-virtual {p1, v10, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    const-string v10, "DTSTART"

    const/4 v11, 0x0

    aget-object v11, v8, v11

    invoke-virtual {v11}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    const/4 v11, 0x1

    invoke-static {v12, v13, p0, v11}, Lcom/android/exchange/utility/CalendarUtilities;->transitionMillisToVCalendarTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    if-eqz v2, :cond_5

    .line 751
    const-string v10, "RRULE"

    invoke-virtual {v1}, Lcom/android/exchange/utility/CalendarUtilities$RRule;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    :cond_2
    const-string v10, "END"

    const-string v11, "DAYLIGHT"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const-string v10, "BEGIN"

    const-string v11, "STANDARD"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    const-string v10, "TZOFFSETFROM"

    invoke-virtual {p1, v10, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    const-string v10, "TZOFFSETTO"

    invoke-virtual {p1, v10, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    const-string v10, "DTSTART"

    const/4 v11, 0x0

    aget-object v11, v9, v11

    invoke-virtual {v11}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    const/4 v11, 0x0

    invoke-static {v12, v13, p0, v11}, Lcom/android/exchange/utility/CalendarUtilities;->transitionMillisToVCalendarTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    if-eqz v2, :cond_6

    .line 769
    const-string v10, "RRULE"

    invoke-virtual {v7}, Lcom/android/exchange/utility/CalendarUtilities$RRule;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    :cond_3
    const-string v10, "END"

    const-string v11, "STANDARD"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    const-string v10, "END"

    const-string v11, "VTIMEZONE"

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 742
    .end local v2    # "hasRule":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 753
    .restart local v2    # "hasRule":Z
    :cond_5
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_2
    if-ge v3, v4, :cond_2

    .line 754
    const-string v10, "RDATE"

    aget-object v11, v8, v3

    invoke-virtual {v11}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    const/4 v11, 0x1

    invoke-static {v12, v13, p0, v11}, Lcom/android/exchange/utility/CalendarUtilities;->transitionMillisToVCalendarTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 771
    .end local v3    # "i":I
    :cond_6
    const/4 v3, 0x1

    .restart local v3    # "i":I
    :goto_3
    if-ge v3, v4, :cond_3

    .line 772
    const-string v10, "RDATE"

    aget-object v11, v9, v3

    invoke-virtual {v11}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v12

    const/4 v11, 0x1

    invoke-static {v12, v13, p0, v11}, Lcom/android/exchange/utility/CalendarUtilities;->transitionMillisToVCalendarTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v10, v11}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method

.method static tokenFromRrule(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "rrule"    # Ljava/lang/String;
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 1373
    invoke-virtual {p0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1374
    .local v4, "start":I
    if-gez v4, :cond_0

    .line 1375
    const/4 v5, 0x0

    .line 1385
    :goto_0
    return-object v5

    .line 1376
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 1377
    .local v3, "len":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    .line 1378
    move v1, v4

    .line 1381
    .local v1, "end":I
    :goto_1
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "end":I
    .local v2, "end":I
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1382
    .local v0, "c":C
    const/16 v5, 0x3b

    if-eq v0, v5, :cond_1

    if-ne v2, v3, :cond_3

    .line 1383
    :cond_1
    if-ne v2, v3, :cond_2

    .line 1384
    add-int/lit8 v1, v2, 0x1

    .line 1385
    .end local v2    # "end":I
    .restart local v1    # "end":I
    :goto_2
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .end local v1    # "end":I
    .restart local v2    # "end":I
    :cond_2
    move v1, v2

    .end local v2    # "end":I
    .restart local v1    # "end":I
    goto :goto_2

    .end local v1    # "end":I
    .restart local v2    # "end":I
    :cond_3
    move v1, v2

    .end local v2    # "end":I
    .restart local v1    # "end":I
    goto :goto_1
.end method

.method static transitionMillisToVCalendarTime(JLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 4
    .param p0, "millis"    # J
    .param p2, "tz"    # Ljava/util/TimeZone;
    .param p3, "dst"    # Z

    .prologue
    .line 1259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1260
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1261
    .local v0, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1262
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1263
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1264
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1265
    const/16 v2, 0x54

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1266
    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionHour(Ljava/util/GregorianCalendar;)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->getTrueTransitionMinute(Ljava/util/GregorianCalendar;)I

    move-result v2

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1268
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/android/exchange/utility/CalendarUtilities;->formatTwo(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1269
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static transposeAllDayTime(JLjava/util/TimeZone;Ljava/util/TimeZone;)J
    .locals 8
    .param p0, "time"    # J
    .param p2, "fromTimeZone"    # Ljava/util/TimeZone;
    .param p3, "toTimeZone"    # Ljava/util/TimeZone;

    .prologue
    const/4 v4, 0x0

    .line 1295
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7, p2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1296
    .local v7, "fromCalendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v7, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 1297
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0, p3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1300
    .local v0, "toCalendar":Ljava/util/GregorianCalendar;
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v7, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v7, v3}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v6}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 1303
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v4}, Ljava/util/GregorianCalendar;->set(II)V

    .line 1304
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public static tziStringToTimeZone(Landroid/content/Context;Ljava/lang/String;)Ljava/util/TimeZone;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeZoneString"    # Ljava/lang/String;

    .prologue
    .line 900
    const v0, 0xea60

    invoke-static {p0, p1, v0}, Lcom/android/exchange/utility/CalendarUtilities;->tziStringToTimeZone(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method static tziStringToTimeZone(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/TimeZone;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeZoneString"    # Ljava/lang/String;
    .param p2, "precision"    # I

    .prologue
    .line 872
    sget-object v1, Lcom/android/exchange/utility/CalendarUtilities;->sTimeZoneCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    .line 873
    .local v0, "timeZone":Ljava/util/TimeZone;
    if-eqz v0, :cond_1

    .line 874
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_0

    .line 875
    const-string v1, "CalendarUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Using cached TimeZone "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    const-string v1, "CalendarUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Using cached TimeZone -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    :cond_0
    :goto_0
    return-object v0

    .line 881
    :cond_1
    invoke-static {p0, p1, p2}, Lcom/android/exchange/utility/CalendarUtilities;->tziStringToTimeZoneImpl(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/TimeZone;

    move-result-object v0

    .line 882
    if-nez v0, :cond_2

    .line 886
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TimeZone not found using default: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 887
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 889
    :cond_2
    sget-object v1, Lcom/android/exchange/utility/CalendarUtilities;->sTimeZoneCache:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static tziStringToTimeZoneImpl(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/TimeZone;
    .locals 33
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeZoneString"    # Ljava/lang/String;
    .param p2, "precision"    # I

    .prologue
    .line 914
    const/16 v22, 0x0

    .line 916
    .local v22, "timeZone":Ljava/util/TimeZone;
    const/16 v30, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v23

    .line 917
    .local v23, "timeZoneBytes":[B
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const/high16 v31, 0x7f050000

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    .line 926
    .local v25, "timeZoneIds":[Ljava/lang/String;
    const/16 v30, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getLong([BI)I

    move-result v30

    mul-int/lit8 v30, v30, -0x1

    const v31, 0xea60

    mul-int v8, v30, v31

    .line 931
    .local v8, "bias":I
    invoke-static {v8}, Ljava/util/TimeZone;->getAvailableIDs(I)[Ljava/lang/String;

    move-result-object v29

    .line 932
    .local v29, "zoneIds":[Ljava/lang/String;
    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v30, v0

    if-lez v30, :cond_11

    .line 936
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v18

    .line 937
    .local v18, "localTimeZone":Ljava/util/TimeZone;
    if-eqz v18, :cond_2

    .line 938
    move-object/from16 v5, v29

    .local v5, "arr$":[Ljava/lang/String;
    array-length v15, v5

    .local v15, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_0
    if-ge v13, v15, :cond_2

    aget-object v28, v5, v13

    .line 939
    .local v28, "zoneId":Ljava/lang/String;
    if-eqz v28, :cond_1

    invoke-virtual/range {v18 .. v18}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 940
    sget-boolean v30, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v30, :cond_0

    .line 941
    const-string v30, "CalendarUtility"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "TimeZone is "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v18 .. v18}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    :cond_0
    invoke-static/range {v28 .. v28}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    move-object/from16 v30, v22

    .line 1094
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    .end local v18    # "localTimeZone":Ljava/util/TimeZone;
    .end local v28    # "zoneId":Ljava/lang/String;
    :goto_1
    return-object v30

    .line 938
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v13    # "i$":I
    .restart local v15    # "len$":I
    .restart local v18    # "localTimeZone":Ljava/util/TimeZone;
    .restart local v28    # "zoneId":Ljava/lang/String;
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 953
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    .end local v28    # "zoneId":Ljava/lang/String;
    :cond_2
    const/16 v30, 0x44

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getTimeZoneDateFromSystemTime([BI)Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;

    move-result-object v9

    .line 955
    .local v9, "dstEnd":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    if-nez v9, :cond_7

    .line 961
    move-object/from16 v5, v29

    .restart local v5    # "arr$":[Ljava/lang/String;
    array-length v15, v5

    .restart local v15    # "len$":I
    const/4 v13, 0x0

    .restart local v13    # "i$":I
    move v14, v13

    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    .local v14, "i$":I
    :goto_2
    if-ge v14, v15, :cond_5

    aget-object v28, v5, v14

    .line 962
    .restart local v28    # "zoneId":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    .line 963
    if-eqz v22, :cond_4

    .line 964
    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->useDaylightTime()Z

    move-result v30

    if-nez v30, :cond_4

    .line 965
    move-object/from16 v6, v25

    .local v6, "arr$":[Ljava/lang/String;
    array-length v0, v6

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v13, 0x0

    .end local v14    # "i$":I
    .restart local v13    # "i$":I
    :goto_3
    move/from16 v0, v16

    if-ge v13, v0, :cond_4

    aget-object v24, v6, v13

    .line 966
    .local v24, "timeZoneId":Ljava/lang/String;
    if-eqz v24, :cond_3

    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    move-object/from16 v30, v22

    .line 967
    goto :goto_1

    .line 965
    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 961
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v16    # "len$":I
    .end local v24    # "timeZoneId":Ljava/lang/String;
    :cond_4
    add-int/lit8 v13, v14, 0x1

    .restart local v13    # "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .restart local v14    # "i$":I
    goto :goto_2

    .line 974
    .end local v28    # "zoneId":Ljava/lang/String;
    :cond_5
    sget-boolean v30, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v30, :cond_6

    if-eqz v22, :cond_6

    .line 975
    const-string v30, "CalendarUtility"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "TimeZone without DST found by offset: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move-object/from16 v30, v22

    .line 978
    goto :goto_1

    .line 980
    .end local v14    # "i$":I
    :cond_7
    const/16 v30, 0x98

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getTimeZoneDateFromSystemTime([BI)Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;

    move-result-object v12

    .line 982
    .local v12, "dstStart":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    if-nez v12, :cond_8

    .line 983
    const/16 v30, 0x0

    goto :goto_1

    .line 986
    :cond_8
    const/16 v30, 0xa8

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getLong([BI)I

    move-result v30

    mul-int/lit8 v30, v30, -0x1

    const v31, 0xea60

    mul-int v30, v30, v31

    move/from16 v0, v30

    int-to-long v10, v0

    .line 992
    .local v10, "dstSavings":J
    move-object/from16 v5, v29

    .restart local v5    # "arr$":[Ljava/lang/String;
    array-length v15, v5

    .restart local v15    # "len$":I
    const/4 v13, 0x0

    .restart local v13    # "i$":I
    move v14, v13

    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    .restart local v14    # "i$":I
    :goto_4
    if-ge v14, v15, :cond_c

    aget-object v28, v5, v14

    .line 994
    .restart local v28    # "zoneId":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    .line 1008
    if-eqz v22, :cond_9

    .line 1009
    move-object/from16 v0, v22

    invoke-static {v0, v12}, Lcom/android/exchange/utility/CalendarUtilities;->getMillisAtTimeZoneDateTransition(Ljava/util/TimeZone;Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;)J

    move-result-wide v20

    .line 1014
    .local v20, "millisAtTransition":J
    new-instance v7, Ljava/util/Date;

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v30, v0

    sub-long v30, v20, v30

    move-wide/from16 v0, v30

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 1015
    .local v7, "before":Ljava/util/Date;
    new-instance v4, Ljava/util/Date;

    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v30, v30, v20

    move-wide/from16 v0, v30

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 1016
    .local v4, "after":Ljava/util/Date;
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 992
    .end local v4    # "after":Ljava/util/Date;
    .end local v7    # "before":Ljava/util/Date;
    .end local v14    # "i$":I
    .end local v20    # "millisAtTransition":J
    :cond_9
    add-int/lit8 v13, v14, 0x1

    .restart local v13    # "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .restart local v14    # "i$":I
    goto :goto_4

    .line 1018
    .restart local v4    # "after":Ljava/util/Date;
    .restart local v7    # "before":Ljava/util/Date;
    .restart local v20    # "millisAtTransition":J
    :cond_a
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1022
    move-object/from16 v0, v22

    invoke-static {v0, v9}, Lcom/android/exchange/utility/CalendarUtilities;->getMillisAtTimeZoneDateTransition(Ljava/util/TimeZone;Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;)J

    move-result-wide v20

    .line 1032
    new-instance v7, Ljava/util/Date;

    .end local v7    # "before":Ljava/util/Date;
    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v30, v30, v10

    sub-long v30, v20, v30

    move-wide/from16 v0, v30

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 1033
    .restart local v7    # "before":Ljava/util/Date;
    new-instance v4, Ljava/util/Date;

    .end local v4    # "after":Ljava/util/Date;
    move/from16 v0, p2

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v30, v30, v20

    move-wide/from16 v0, v30

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 1034
    .restart local v4    # "after":Ljava/util/Date;
    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1036
    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v30

    if-nez v30, :cond_9

    .line 1040
    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v30, v0

    cmp-long v30, v10, v30

    if-nez v30, :cond_9

    .line 1042
    move-object/from16 v6, v25

    .restart local v6    # "arr$":[Ljava/lang/String;
    array-length v0, v6

    move/from16 v16, v0

    .restart local v16    # "len$":I
    const/4 v13, 0x0

    .end local v14    # "i$":I
    .restart local v13    # "i$":I
    :goto_5
    move/from16 v0, v16

    if-ge v13, v0, :cond_9

    aget-object v24, v6, v13

    .line 1043
    .restart local v24    # "timeZoneId":Ljava/lang/String;
    if-eqz v24, :cond_b

    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    move-object/from16 v30, v22

    .line 1044
    goto/16 :goto_1

    .line 1042
    :cond_b
    add-int/lit8 v13, v13, 0x1

    goto :goto_5

    .line 1056
    .end local v4    # "after":Ljava/util/Date;
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v7    # "before":Ljava/util/Date;
    .end local v13    # "i$":I
    .end local v16    # "len$":I
    .end local v20    # "millisAtTransition":J
    .end local v24    # "timeZoneId":Ljava/lang/String;
    .end local v28    # "zoneId":Ljava/lang/String;
    .restart local v14    # "i$":I
    :cond_c
    const/16 v17, 0x0

    .line 1057
    .local v17, "lenient":Z
    const/16 v19, 0x0

    .line 1058
    .local v19, "name":Z
    iget v0, v12, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->hour:I

    move/from16 v30, v0

    iget v0, v9, Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;->hour:I

    move/from16 v31, v0

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_e

    const v30, 0xea60

    move/from16 v0, p2

    move/from16 v1, v30

    if-ne v0, v1, :cond_e

    .line 1059
    const v30, 0xdbba00

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->tziStringToTimeZoneImpl(Landroid/content/Context;Ljava/lang/String;I)Ljava/util/TimeZone;

    move-result-object v22

    .line 1061
    const/16 v17, 0x1

    .line 1083
    :goto_6
    sget-boolean v30, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v30, :cond_d

    if-eqz v22, :cond_d

    .line 1084
    const-string v30, "CalendarUtility"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "No TimeZone with correct DST settings; using first: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 1086
    const-string v30, "CalendarUtility"

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "No TimeZone with correct DST settings; using first:-> "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v22 .. v22}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object/from16 v30, v22

    .line 1091
    goto/16 :goto_1

    .line 1069
    :cond_e
    const/16 v30, 0x4

    const/16 v31, 0x20

    move-object/from16 v0, v23

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getString([BII)Ljava/lang/String;

    move-result-object v27

    .line 1071
    .local v27, "tzName":Ljava/lang/String;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_10

    .line 1072
    invoke-static/range {v27 .. v27}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v26

    .line 1073
    .local v26, "tz":Ljava/util/TimeZone;
    if-eqz v26, :cond_f

    .line 1074
    move-object/from16 v22, v26

    .line 1075
    const/16 v19, 0x1

    goto :goto_6

    .line 1077
    :cond_f
    const/16 v30, 0x0

    aget-object v30, v29, v30

    invoke-static/range {v30 .. v30}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    goto :goto_6

    .line 1080
    .end local v26    # "tz":Ljava/util/TimeZone;
    :cond_10
    const/16 v30, 0x0

    aget-object v30, v29, v30

    invoke-static/range {v30 .. v30}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    goto :goto_6

    .line 1094
    .end local v9    # "dstEnd":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    .end local v10    # "dstSavings":J
    .end local v12    # "dstStart":Lcom/android/exchange/utility/CalendarUtilities$TimeZoneDate;
    .end local v14    # "i$":I
    .end local v17    # "lenient":Z
    .end local v18    # "localTimeZone":Ljava/util/TimeZone;
    .end local v19    # "name":Z
    .end local v27    # "tzName":Ljava/lang/String;
    :cond_11
    const/16 v30, 0x0

    goto/16 :goto_1
.end method

.method public static updateMessageBodyforEventId(Landroid/content/Context;JJ)V
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "eventId"    # J
    .param p3, "msgId"    # J

    .prologue
    .line 3532
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v10

    .line 3538
    .local v10, "eventIterator":Landroid/content/EntityIterator;
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3539
    invoke-interface {v10}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Entity;

    .line 3540
    .local v9, "entity":Landroid/content/Entity;
    invoke-virtual {v9}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0, v2, v3, v4}, Lcom/android/exchange/utility/CalendarUtilities;->buildMessageTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v13

    .line 3542
    .local v13, "text":Ljava/lang/String;
    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    .line 3543
    .local v12, "msgText":Ljava/lang/String;
    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .line 3544
    .local v11, "msgHtml":Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    .line 3546
    .local v14, "tmpHtmlContent":Ljava/lang/StringBuffer;
    if-eqz v11, :cond_0

    .line 3547
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "</body>"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3548
    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, "</body>"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3550
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<br>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<br>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3551
    const-string v2, "</body></html>"

    invoke-virtual {v14, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3552
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 3556
    :cond_0
    if-eqz v12, :cond_1

    .line 3557
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\r \n\r"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3558
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3562
    :goto_1
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 3563
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v2, "htmlContent"

    invoke-virtual {v8, v2, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3564
    const-string v2, "textContent"

    invoke-virtual {v8, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3565
    move-wide/from16 v0, p3

    invoke-static {p0, v0, v1, v8}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateBodyWithMessageId(Landroid/content/Context;JLandroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 3568
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v9    # "entity":Landroid/content/Entity;
    .end local v11    # "msgHtml":Ljava/lang/String;
    .end local v12    # "msgText":Ljava/lang/String;
    .end local v13    # "text":Ljava/lang/String;
    .end local v14    # "tmpHtmlContent":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/content/EntityIterator;->close()V

    throw v2

    .line 3560
    .restart local v9    # "entity":Landroid/content/Entity;
    .restart local v11    # "msgHtml":Ljava/lang/String;
    .restart local v12    # "msgText":Ljava/lang/String;
    .restart local v13    # "text":Ljava/lang/String;
    .restart local v14    # "tmpHtmlContent":Ljava/lang/StringBuffer;
    :cond_1
    move-object v12, v13

    goto :goto_1

    .line 3568
    .end local v9    # "entity":Landroid/content/Entity;
    .end local v11    # "msgHtml":Ljava/lang/String;
    .end local v12    # "msgText":Ljava/lang/String;
    .end local v13    # "text":Ljava/lang/String;
    .end local v14    # "tmpHtmlContent":Ljava/lang/StringBuffer;
    :cond_2
    invoke-interface {v10}, Landroid/content/EntityIterator;->close()V

    .line 3570
    return-void
.end method

.method public static updateMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 62
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "entity"    # Landroid/content/Entity;
    .param p3, "messageFlag"    # I
    .param p4, "uid"    # Ljava/lang/String;
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p6, "specifiedAttendee"    # Ljava/lang/String;

    .prologue
    .line 2197
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v27

    .line 2198
    .local v27, "entityValues":Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v51

    .line 2201
    .local v51, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    const-string v8, "original_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v34

    .line 2202
    .local v34, "isException":Z
    const/16 v35, 0x0

    .line 2205
    .local v35, "isReply":Z
    if-nez p1, :cond_0

    .line 2206
    new-instance p1, Lcom/android/emailcommon/provider/EmailContent$Message;

    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    invoke-direct/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 2207
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v8, 0x1

    move-object/from16 v0, p1

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    .line 2209
    :cond_0
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    or-int v8, v8, p3

    move-object/from16 v0, p1

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 2211
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    move-object/from16 v0, p1

    iput-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    .line 2214
    and-int/lit8 v8, p3, 0x10

    if-eqz v8, :cond_17

    .line 2215
    const-string v38, "REQUEST"

    .line 2225
    .local v38, "method":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v4, Lcom/android/exchange/utility/SimpleIcsWriter;

    invoke-direct {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;-><init>()V

    .line 2226
    .local v4, "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    const-string v8, "BEGIN"

    const-string v9, "VCALENDAR"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2227
    const-string v8, "METHOD"

    move-object/from16 v0, v38

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2228
    const-string v8, "PRODID"

    const-string v9, "AndroidEmail"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229
    const-string v8, "VERSION"

    const-string v9, "2.0"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2234
    sget-object v60, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    .line 2235
    .local v60, "vCalendarTimeZone":Ljava/util/TimeZone;
    const-string v59, ""

    .line 2238
    .local v59, "vCalendarDateSuffix":Ljava/lang/String;
    const/16 v17, 0x0

    .line 2239
    .local v17, "allDayEvent":Z
    const-string v8, "allDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2240
    const-string v8, "allDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v16

    .line 2241
    .local v16, "ade":Ljava/lang/Integer;
    if-eqz v16, :cond_19

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_19

    const/16 v17, 0x1

    .line 2242
    :goto_1
    if-eqz v17, :cond_1

    .line 2255
    .end local v16    # "ade":Ljava/lang/Integer;
    :cond_1
    if-nez v35, :cond_4

    if-eqz v17, :cond_2

    if-eqz v17, :cond_4

    const-string v8, "rrule"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_2
    const-string v8, "rrule"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "original_sync_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2260
    :cond_3
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v60

    .line 2262
    move-object/from16 v0, v60

    invoke-static {v0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->timeZoneToVTimezone(Ljava/util/TimeZone;Lcom/android/exchange/utility/SimpleIcsWriter;)V

    .line 2264
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ";TZID="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v60 .. v60}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v59

    .line 2267
    :cond_4
    const-string v8, "BEGIN"

    const-string v9, "VEVENT"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2268
    if-nez p4, :cond_5

    .line 2271
    const-string v8, "sync_data2"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 2273
    :cond_5
    if-eqz p4, :cond_6

    .line 2274
    const-string v8, "UID"

    move-object/from16 v0, p4

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2277
    :cond_6
    const-string v8, "DTSTAMP"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1a

    .line 2278
    const-string v8, "DTSTAMP"

    const-string v9, "DTSTAMP"

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2285
    :goto_2
    const-string v8, "dtstart"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v52

    .line 2286
    .local v52, "startTime":J
    const-wide/16 v28, -0x1

    .line 2287
    .local v28, "endTime":J
    const-string v8, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 2288
    const-string v8, "dtend"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2289
    const-string v8, "dtend"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 2293
    :cond_7
    if-eqz v17, :cond_8

    .line 2294
    const-string v59, ""

    .line 2295
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v58

    .line 2296
    .local v58, "tz":Ljava/util/TimeZone;
    move-wide/from16 v0, v52

    move-object/from16 v2, v58

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v52

    .line 2297
    const-wide/16 v8, -0x1

    cmp-long v8, v28, v8

    if-eqz v8, :cond_8

    .line 2298
    move-wide/from16 v0, v28

    move-object/from16 v2, v58

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v28

    .line 2302
    .end local v58    # "tz":Ljava/util/TimeZone;
    :cond_8
    const-wide/16 v8, 0x0

    cmp-long v8, v52, v8

    if-eqz v8, :cond_9

    .line 2303
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DTSTART"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v59

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    move-wide/from16 v0, v52

    move-object/from16 v2, v60

    invoke-static {v0, v1, v2, v9}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2312
    :cond_9
    if-eqz v34, :cond_a

    .line 2313
    const-string v8, "originalInstanceTime"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v44

    .line 2314
    .local v44, "originalTime":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RECURRENCE-ID"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v59

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    if-nez v17, :cond_1b

    const/4 v8, 0x1

    :goto_3
    move-wide/from16 v0, v44

    move-object/from16 v2, v60

    invoke-static {v0, v1, v2, v8}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v9, v8}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318
    .end local v44    # "originalTime":J
    :cond_a
    const-string v8, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1c

    .line 2319
    const-string v8, "dtend"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 2320
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DTEND"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v59

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    move-wide/from16 v0, v28

    move-object/from16 v2, v60

    invoke-static {v0, v1, v2, v9}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2338
    :cond_b
    :goto_4
    const/16 v37, 0x0

    .line 2339
    .local v37, "location":Ljava/lang/String;
    const-string v8, "eventLocation"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2340
    const-string v8, "eventLocation"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 2341
    const-string v8, "LOCATION"

    move-object/from16 v0, v37

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346
    :cond_c
    const-string v8, "sync_data4"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 2347
    .local v50, "sequence":Ljava/lang/String;
    if-nez v50, :cond_d

    .line 2348
    const-string v50, "0"

    .line 2352
    :cond_d
    const/16 v56, 0x0

    .line 2353
    .local v56, "titleId":I
    sparse-switch p3, :sswitch_data_0

    .line 2372
    :cond_e
    :goto_5
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v47

    .line 2373
    .local v47, "resources":Landroid/content/res/Resources;
    const-string v8, "title"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    .line 2374
    .local v55, "title":Ljava/lang/String;
    if-nez v55, :cond_f

    .line 2375
    const-string v55, ""

    .line 2380
    :cond_f
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    if-eqz v8, :cond_10

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 2382
    :cond_10
    if-nez v56, :cond_1d

    .line 2383
    move-object/from16 v0, v55

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    .line 2389
    :cond_11
    :goto_6
    const-string v8, "SUMMARY"

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2393
    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    .line 2394
    .local v49, "sb":Ljava/lang/StringBuilder;
    if-eqz v34, :cond_12

    if-nez v35, :cond_12

    .line 2397
    new-instance v21, Ljava/util/Date;

    const-string v8, "originalInstanceTime"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, v21

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 2398
    .local v21, "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v22

    .line 2399
    .local v22, "dateString":Ljava/lang/String;
    const v8, 0x7f060005

    move/from16 v0, v56

    if-ne v0, v8, :cond_1e

    .line 2400
    const v8, 0x7f060010

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v22, v9, v10

    move-object/from16 v0, v47

    invoke-virtual {v0, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2404
    :goto_7
    const-string v8, "\n\n"

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2406
    .end local v21    # "date":Ljava/util/Date;
    .end local v22    # "dateString":Ljava/lang/String;
    :cond_12
    const/4 v8, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v49

    invoke-static {v0, v1, v2, v8}, Lcom/android/exchange/utility/CalendarUtilities;->buildMessageTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v54

    .line 2409
    .local v54, "text":Ljava/lang/String;
    invoke-virtual/range {v54 .. v54}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_13

    .line 2410
    const-string v8, "DESCRIPTION"

    move-object/from16 v0, v54

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413
    :cond_13
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2415
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v8, :cond_1f

    .line 2416
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2417
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v54

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2422
    :goto_8
    const-string v8, ""

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    .line 2425
    if-nez v35, :cond_15

    .line 2426
    const-string v8, "allDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 2427
    const-string v8, "allDay"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v16

    .line 2428
    .restart local v16    # "ade":Ljava/lang/Integer;
    const-string v9, "X-MICROSOFT-CDO-ALLDAYEVENT"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-nez v8, :cond_20

    const-string v8, "FALSE"

    :goto_9
    invoke-virtual {v4, v9, v8}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2431
    .end local v16    # "ade":Ljava/lang/Integer;
    :cond_14
    const-string v8, "rrule"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 2432
    .local v48, "rrule":Ljava/lang/String;
    if-eqz v48, :cond_15

    .line 2433
    const-string v8, "RRULE"

    move-object/from16 v0, v48

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443
    .end local v48    # "rrule":Ljava/lang/String;
    :cond_15
    const/16 v43, 0x0

    .line 2444
    .local v43, "organizerName":Ljava/lang/String;
    const/16 v42, 0x0

    .line 2445
    .local v42, "organizerEmail":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2446
    .local v7, "attendeeEmail":Ljava/lang/String;
    const/4 v6, 0x0

    .line 2447
    .local v6, "attendeeName":Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2448
    .local v5, "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    invoke-virtual/range {v51 .. v51}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v32

    .local v32, "i$":Ljava/util/Iterator;
    :cond_16
    :goto_a
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_23

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Landroid/content/Entity$NamedContentValues;

    .line 2449
    .local v39, "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v39

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v40, v0

    .line 2450
    .local v40, "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v39

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v41, v0

    .line 2451
    .local v41, "ncvValues":Landroid/content/ContentValues;
    sget-object v8, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v40

    invoke-virtual {v0, v8}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 2452
    const-string v8, "attendeeRelationship"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v46

    .line 2455
    .local v46, "relationship":Ljava/lang/Integer;
    if-eqz v46, :cond_16

    const-string v8, "attendeeEmail"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 2457
    invoke-virtual/range {v46 .. v46}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const/4 v9, 0x2

    if-ne v8, v9, :cond_21

    .line 2458
    const-string v8, "attendeeName"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 2459
    const-string v8, "attendeeEmail"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v42

    .line 2460
    goto :goto_a

    .line 2216
    .end local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .end local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v7    # "attendeeEmail":Ljava/lang/String;
    .end local v17    # "allDayEvent":Z
    .end local v28    # "endTime":J
    .end local v32    # "i$":Ljava/util/Iterator;
    .end local v37    # "location":Ljava/lang/String;
    .end local v38    # "method":Ljava/lang/String;
    .end local v39    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v40    # "ncvUri":Landroid/net/Uri;
    .end local v41    # "ncvValues":Landroid/content/ContentValues;
    .end local v42    # "organizerEmail":Ljava/lang/String;
    .end local v43    # "organizerName":Ljava/lang/String;
    .end local v46    # "relationship":Ljava/lang/Integer;
    .end local v47    # "resources":Landroid/content/res/Resources;
    .end local v49    # "sb":Ljava/lang/StringBuilder;
    .end local v50    # "sequence":Ljava/lang/String;
    .end local v52    # "startTime":J
    .end local v54    # "text":Ljava/lang/String;
    .end local v55    # "title":Ljava/lang/String;
    .end local v56    # "titleId":I
    .end local v59    # "vCalendarDateSuffix":Ljava/lang/String;
    .end local v60    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :cond_17
    and-int/lit8 v8, p3, 0x20

    if-eqz v8, :cond_18

    .line 2217
    const-string v38, "CANCEL"

    .restart local v38    # "method":Ljava/lang/String;
    goto/16 :goto_0

    .line 2219
    .end local v38    # "method":Ljava/lang/String;
    :cond_18
    const-string v38, "REPLY"

    .line 2220
    .restart local v38    # "method":Ljava/lang/String;
    const/16 v35, 0x1

    goto/16 :goto_0

    .line 2241
    .restart local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .restart local v16    # "ade":Ljava/lang/Integer;
    .restart local v17    # "allDayEvent":Z
    .restart local v59    # "vCalendarDateSuffix":Ljava/lang/String;
    .restart local v60    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :cond_19
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 2280
    .end local v16    # "ade":Ljava/lang/Integer;
    :cond_1a
    :try_start_1
    const-string v8, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 2653
    .end local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .end local v17    # "allDayEvent":Z
    .end local v59    # "vCalendarDateSuffix":Ljava/lang/String;
    .end local v60    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :catch_0
    move-exception v26

    .line 2654
    .local v26, "e":Ljava/io/IOException;
    const-string v8, "CalendarUtility"

    const-string v9, "IOException in createMessageForEntity"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2655
    const/16 p1, 0x0

    .line 2659
    .end local v26    # "e":Ljava/io/IOException;
    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_b
    return-object p1

    .line 2314
    .restart local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .restart local v17    # "allDayEvent":Z
    .restart local v28    # "endTime":J
    .restart local v44    # "originalTime":J
    .restart local v52    # "startTime":J
    .restart local v59    # "vCalendarDateSuffix":Ljava/lang/String;
    .restart local v60    # "vCalendarTimeZone":Ljava/util/TimeZone;
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_1b
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 2326
    .end local v44    # "originalTime":J
    :cond_1c
    const-wide/32 v24, 0x36ee80

    .line 2327
    .local v24, "durationMillis":J
    :try_start_2
    new-instance v23, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct/range {v23 .. v23}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2329
    .local v23, "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_3
    const-string v8, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 2330
    invoke-virtual/range {v23 .. v23}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-wide v24

    .line 2334
    :goto_c
    :try_start_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "DTEND"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v59

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    add-long v10, v52, v24

    const/4 v9, 0x1

    move-object/from16 v0, v60

    invoke-static {v10, v11, v0, v9}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2355
    .end local v23    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v24    # "durationMillis":J
    .restart local v37    # "location":Ljava/lang/String;
    .restart local v50    # "sequence":Ljava/lang/String;
    .restart local v56    # "titleId":I
    :sswitch_0
    const-string v8, "0"

    move-object/from16 v0, v50

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 2356
    const v56, 0x7f060008

    goto/16 :goto_5

    .line 2360
    :sswitch_1
    const v56, 0x7f060002

    .line 2361
    goto/16 :goto_5

    .line 2363
    :sswitch_2
    const v56, 0x7f060003

    .line 2364
    goto/16 :goto_5

    .line 2366
    :sswitch_3
    const v56, 0x7f060004

    .line 2367
    goto/16 :goto_5

    .line 2369
    :sswitch_4
    const v56, 0x7f060005

    goto/16 :goto_5

    .line 2386
    .restart local v47    # "resources":Landroid/content/res/Resources;
    .restart local v55    # "title":Ljava/lang/String;
    :cond_1d
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v55, v8, v9

    move-object/from16 v0, v47

    move/from16 v1, v56

    invoke-virtual {v0, v1, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_6

    .line 2402
    .restart local v21    # "date":Ljava/util/Date;
    .restart local v22    # "dateString":Ljava/lang/String;
    .restart local v49    # "sb":Ljava/lang/StringBuilder;
    :cond_1e
    const v8, 0x7f060011

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v22, v9, v10

    move-object/from16 v0, v47

    invoke-virtual {v0, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2419
    .end local v21    # "date":Ljava/util/Date;
    .end local v22    # "dateString":Ljava/lang/String;
    .restart local v54    # "text":Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, v54

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto/16 :goto_8

    .line 2428
    .restart local v16    # "ade":Ljava/lang/Integer;
    :cond_20
    const-string v8, "TRUE"

    goto/16 :goto_9

    .line 2462
    .end local v16    # "ade":Ljava/lang/Integer;
    .restart local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .restart local v6    # "attendeeName":Ljava/lang/String;
    .restart local v7    # "attendeeEmail":Ljava/lang/String;
    .restart local v32    # "i$":Ljava/util/Iterator;
    .restart local v39    # "ncv":Landroid/content/Entity$NamedContentValues;
    .restart local v40    # "ncvUri":Landroid/net/Uri;
    .restart local v41    # "ncvValues":Landroid/content/ContentValues;
    .restart local v42    # "organizerEmail":Ljava/lang/String;
    .restart local v43    # "organizerName":Ljava/lang/String;
    .restart local v46    # "relationship":Ljava/lang/Integer;
    :cond_21
    const-string v8, "attendeeEmail"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2463
    const-string v8, "attendeeName"

    move-object/from16 v0, v41

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2466
    if-eqz v7, :cond_16

    .line 2470
    if-eqz p6, :cond_22

    move-object/from16 v0, p6

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    :cond_22
    move/from16 v8, p3

    move-object/from16 v9, p5

    .line 2475
    invoke-static/range {v4 .. v9}, Lcom/android/exchange/utility/CalendarUtilities;->addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    goto/16 :goto_a

    .line 2482
    .end local v39    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v40    # "ncvUri":Landroid/net/Uri;
    .end local v41    # "ncvValues":Landroid/content/ContentValues;
    .end local v46    # "relationship":Ljava/lang/Integer;
    :cond_23
    if-nez v42, :cond_24

    .line 2483
    const-string v8, "organizer"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    .line 2485
    :cond_24
    if-nez v7, :cond_25

    if-nez p6, :cond_25

    .line 2486
    move-object/from16 v0, p5

    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    .line 2487
    move-object/from16 v0, v42

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_25

    move/from16 v8, p3

    move-object/from16 v9, p5

    .line 2488
    invoke-static/range {v4 .. v9}, Lcom/android/exchange/utility/CalendarUtilities;->addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 2496
    :cond_25
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_26

    .line 2497
    if-eqz p6, :cond_2a

    .line 2498
    const/4 v10, 0x0

    move-object v8, v4

    move-object v9, v5

    move-object/from16 v11, p6

    move/from16 v12, p3

    move-object/from16 v13, p5

    invoke-static/range {v8 .. v13}, Lcom/android/exchange/utility/CalendarUtilities;->addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 2510
    .end local v32    # "i$":Ljava/util/Iterator;
    :cond_26
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    .line 2511
    .local v61, "visibility":Ljava/lang/Integer;
    const-string v8, "accessLevel"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_27

    .line 2512
    const-string v8, "accessLevel"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v61

    .line 2513
    invoke-virtual/range {v61 .. v61}, Ljava/lang/Integer;->intValue()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 2527
    const-string v8, "CLASS"

    const-string v9, "DEFAULT"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2534
    :cond_27
    :goto_d
    if-eqz v42, :cond_29

    .line 2535
    new-instance v33, Ljava/lang/StringBuffer;

    const-string v8, "ORGANIZER"

    move-object/from16 v0, v33

    invoke-direct {v0, v8}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2539
    .local v33, "icalTag":Ljava/lang/StringBuffer;
    if-eqz v43, :cond_28

    .line 2540
    const-string v8, ";CN="

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-static/range {v43 .. v43}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2542
    :cond_28
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MAILTO:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v42

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2543
    if-eqz v35, :cond_29

    .line 2544
    if-nez v43, :cond_2b

    new-instance v8, Lcom/android/emailcommon/mail/Address;

    move-object/from16 v0, v42

    invoke-direct {v8, v0}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;)V

    :goto_e
    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2550
    .end local v33    # "icalTag":Ljava/lang/StringBuffer;
    :cond_29
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2c

    .line 2551
    const/16 p1, 0x0

    goto/16 :goto_b

    .line 2500
    .end local v61    # "visibility":Ljava/lang/Integer;
    .restart local v32    # "i$":Ljava/util/Iterator;
    :cond_2a
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v15

    .line 2501
    .local v15, "addresses":[Lcom/android/emailcommon/mail/Address;
    move-object/from16 v18, v15

    .local v18, "arr$":[Lcom/android/emailcommon/mail/Address;
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v36, v0

    .local v36, "len$":I
    const/16 v32, 0x0

    .local v32, "i$":I
    :goto_f
    move/from16 v0, v32

    move/from16 v1, v36

    if-ge v0, v1, :cond_26

    aget-object v14, v18, v32

    .line 2502
    .local v14, "address":Lcom/android/emailcommon/mail/Address;
    const/4 v10, 0x0

    invoke-virtual {v14}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v11

    move-object v8, v4

    move-object v9, v5

    move/from16 v12, p3

    move-object/from16 v13, p5

    invoke-static/range {v8 .. v13}, Lcom/android/exchange/utility/CalendarUtilities;->addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 2501
    add-int/lit8 v32, v32, 0x1

    goto :goto_f

    .line 2515
    .end local v14    # "address":Lcom/android/emailcommon/mail/Address;
    .end local v15    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v18    # "arr$":[Lcom/android/emailcommon/mail/Address;
    .end local v32    # "i$":I
    .end local v36    # "len$":I
    .restart local v61    # "visibility":Ljava/lang/Integer;
    :pswitch_0
    const-string v8, "CLASS"

    const-string v9, "DEFAULT"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 2518
    :pswitch_1
    const-string v8, "CLASS"

    const-string v9, "PUBLIC"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 2521
    :pswitch_2
    const-string v8, "CLASS"

    const-string v9, "PRIVATE"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 2524
    :pswitch_3
    const-string v8, "CLASS"

    const-string v9, "CONFIDENTIAL"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 2544
    .restart local v33    # "icalTag":Ljava/lang/StringBuffer;
    :cond_2b
    new-instance v8, Lcom/android/emailcommon/mail/Address;

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-direct {v8, v0, v1}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e

    .line 2554
    .end local v33    # "icalTag":Ljava/lang/StringBuffer;
    :cond_2c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v0, v8, [Lcom/android/emailcommon/mail/Address;

    move-object/from16 v57, v0

    .line 2555
    .local v57, "toArray":[Lcom/android/emailcommon/mail/Address;
    const/16 v30, 0x0

    .line 2556
    .local v30, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v32

    .local v32, "i$":Ljava/util/Iterator;
    move/from16 v31, v30

    .end local v30    # "i":I
    .local v31, "i":I
    :goto_10
    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2d

    invoke-interface/range {v32 .. v32}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/android/emailcommon/mail/Address;

    .line 2557
    .restart local v14    # "address":Lcom/android/emailcommon/mail/Address;
    add-int/lit8 v30, v31, 0x1

    .end local v31    # "i":I
    .restart local v30    # "i":I
    aput-object v14, v57, v31

    move/from16 v31, v30

    .line 2558
    .end local v30    # "i":I
    .restart local v31    # "i":I
    goto :goto_10

    .line 2560
    .end local v14    # "address":Lcom/android/emailcommon/mail/Address;
    :cond_2d
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    if-eqz v8, :cond_2e

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2f

    .line 2562
    :cond_2e
    invoke-static/range {v57 .. v57}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    .line 2574
    :cond_2f
    const/16 v20, 0x2

    .line 2575
    .local v20, "busyStatus":I
    const-string v8, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_30

    .line 2576
    const-string v8, "_id"

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/android/exchange/utility/CalendarUtilities;->getAvailabilityStatus(Landroid/content/Context;J)I

    move-result v20

    .line 2578
    :cond_30
    if-nez v20, :cond_33

    .line 2579
    const-string v8, "TRANSP"

    const-string v9, "TRANSPARENT"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2584
    :goto_11
    packed-switch v20, :pswitch_data_1

    .line 2598
    const-string v8, "X-MICROSOFT-CDO-BUSYSTATUS"

    const-string v9, "BUSY"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2604
    :goto_12
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    if-nez v8, :cond_34

    .line 2605
    const-string v8, "PRIORITY"

    const-string v9, "9"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612
    :cond_31
    :goto_13
    const-string v8, "SEQUENCE"

    move-object/from16 v0, v50

    invoke-virtual {v4, v8, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2614
    sget v8, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_36

    .line 2615
    invoke-static {v4}, Lcom/android/exchange/utility/CalendarUtilities;->writeVAlarm(Lcom/android/exchange/utility/SimpleIcsWriter;)V

    .line 2619
    :goto_14
    const-string v8, "END"

    const-string v9, "VEVENT"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2620
    const-string v8, "END"

    const-string v9, "VCALENDAR"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2624
    const-string v8, "CalendarUtility"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Encoded iCalendar follows below. \n "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 2628
    new-instance v19, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct/range {v19 .. v19}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 2629
    .local v19, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    invoke-virtual {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;->getBytes()[B

    move-result-object v8

    move-object/from16 v0, v19

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    .line 2630
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "text/calendar; method="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v38

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v19

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 2631
    const-string v8, "invite.ics"

    move-object/from16 v0, v19

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 2632
    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    array-length v8, v8

    int-to-long v8, v8

    move-object/from16 v0, v19

    iput-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 2634
    const/4 v8, 0x1

    move-object/from16 v0, v19

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    .line 2638
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v8, :cond_32

    .line 2639
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2642
    :cond_32
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2643
    const/4 v8, 0x1

    move-object/from16 v0, p1

    iput-boolean v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 2649
    const/4 v8, 0x1

    move-object/from16 v0, p1

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 2650
    const/4 v8, 0x1

    move-object/from16 v0, p1

    iput-boolean v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    goto/16 :goto_b

    .line 2581
    .end local v19    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_33
    const-string v8, "TRANSP"

    const-string v9, "OPAQUE"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 2586
    :pswitch_4
    const-string v8, "X-MICROSOFT-CDO-BUSYSTATUS"

    const-string v9, "FREE"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 2589
    :pswitch_5
    const-string v8, "X-MICROSOFT-CDO-BUSYSTATUS"

    const-string v9, "TENTATIVE"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 2592
    :pswitch_6
    const-string v8, "X-MICROSOFT-CDO-BUSYSTATUS"

    const-string v9, "BUSY"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 2595
    :pswitch_7
    const-string v8, "X-MICROSOFT-CDO-BUSYSTATUS"

    const-string v9, "OOF"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 2606
    :cond_34
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_35

    .line 2607
    const-string v8, "PRIORITY"

    const-string v9, "5"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2608
    :cond_35
    move-object/from16 v0, p1

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_31

    .line 2609
    const-string v8, "PRIORITY"

    const-string v9, "1"

    invoke-virtual {v4, v8, v9}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2617
    :cond_36
    const-string v8, "CalendarUtility"

    const-string v9, "None selected for Alarm"

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_14

    .line 2331
    .end local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v7    # "attendeeEmail":Ljava/lang/String;
    .end local v20    # "busyStatus":I
    .end local v31    # "i":I
    .end local v32    # "i$":Ljava/util/Iterator;
    .end local v37    # "location":Ljava/lang/String;
    .end local v42    # "organizerEmail":Ljava/lang/String;
    .end local v43    # "organizerName":Ljava/lang/String;
    .end local v47    # "resources":Landroid/content/res/Resources;
    .end local v49    # "sb":Ljava/lang/StringBuilder;
    .end local v50    # "sequence":Ljava/lang/String;
    .end local v54    # "text":Ljava/lang/String;
    .end local v55    # "title":Ljava/lang/String;
    .end local v56    # "titleId":I
    .end local v57    # "toArray":[Lcom/android/emailcommon/mail/Address;
    .end local v61    # "visibility":Ljava/lang/Integer;
    .restart local v23    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .restart local v24    # "durationMillis":J
    :catch_1
    move-exception v8

    goto/16 :goto_c

    .line 2353
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_4
        0x40 -> :sswitch_1
        0x80 -> :sswitch_2
        0x100 -> :sswitch_3
    .end sparse-switch

    .line 2513
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 2584
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static updateMessageForForwardEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 48
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "entity"    # Landroid/content/Entity;
    .param p3, "messageFlag"    # I
    .param p4, "uid"    # Ljava/lang/String;
    .param p5, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 2679
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v16

    .line 2680
    .local v16, "entityValues":Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v39

    .line 2681
    .local v39, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    const-string v44, "original_sync_id"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v23

    .line 2682
    .local v23, "isException":Z
    const/16 v24, 0x0

    .line 2683
    .local v24, "isReply":Z
    if-nez p1, :cond_0

    .line 2684
    new-instance p1, Lcom/android/emailcommon/provider/EmailContent$Message;

    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    invoke-direct/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 2685
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/16 v44, 0x1

    move/from16 v0, v44

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    .line 2687
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v44, v0

    or-int v44, v44, p3

    move/from16 v0, v44

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 2688
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v44

    move-wide/from16 v0, v44

    move-object/from16 v2, p1

    iput-wide v0, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    .line 2690
    and-int/lit8 v44, p3, 0x10

    if-eqz v44, :cond_c

    .line 2691
    const-string v26, "REQUEST"

    .line 2701
    .local v26, "method":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v22, Lcom/android/exchange/utility/SimpleIcsWriter;

    invoke-direct/range {v22 .. v22}, Lcom/android/exchange/utility/SimpleIcsWriter;-><init>()V

    .line 2702
    .local v22, "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    const-string v44, "BEGIN"

    const-string v45, "VCALENDAR"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2703
    const-string v44, "METHOD"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2704
    const-string v44, "PRODID"

    const-string v45, "AndroidEmail"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2705
    const-string v44, "VERSION"

    const-string v45, "2.0"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2706
    sget-object v42, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    .line 2707
    .local v42, "vCalendarTimeZone":Ljava/util/TimeZone;
    const-string v41, ""

    .line 2708
    .local v41, "vCalendarDateSuffix":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2709
    .local v7, "allDayEvent":Z
    const-string v44, "allDay"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_1

    .line 2710
    const-string v44, "allDay"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 2711
    .local v6, "ade":Ljava/lang/Integer;
    if-eqz v6, :cond_e

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v44

    const/16 v45, 0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_e

    const/4 v7, 0x1

    .line 2714
    .end local v6    # "ade":Ljava/lang/Integer;
    :cond_1
    :goto_1
    if-nez v24, :cond_4

    if-eqz v7, :cond_2

    if-eqz v7, :cond_4

    const-string v44, "rrule"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_4

    :cond_2
    const-string v44, "rrule"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-nez v44, :cond_3

    const-string v44, "original_sync_id"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_4

    .line 2716
    :cond_3
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v42

    .line 2717
    move-object/from16 v0, v42

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->timeZoneToVTimezone(Ljava/util/TimeZone;Lcom/android/exchange/utility/SimpleIcsWriter;)V

    .line 2718
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, ";TZID="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v42 .. v42}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v45

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    .line 2721
    :cond_4
    const-string v44, "BEGIN"

    const-string v45, "VEVENT"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2722
    if-nez p4, :cond_5

    .line 2723
    const-string v44, "sync_data2"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    .line 2725
    :cond_5
    if-eqz p4, :cond_6

    .line 2726
    const-string v44, "UID"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, p4

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2729
    :cond_6
    const-string v44, "DTSTAMP"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_f

    .line 2730
    const-string v44, "DTSTAMP"

    const-string v45, "DTSTAMP"

    move-object/from16 v0, v16

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2735
    :goto_2
    const-string v44, "dtstart"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Long;->longValue()J

    move-result-wide v36

    .line 2736
    .local v36, "startTime":J
    const-wide/16 v14, -0x1

    .line 2737
    .local v14, "endTime":J
    const-string v44, "duration"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-nez v44, :cond_7

    .line 2738
    const-string v44, "dtend"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_7

    .line 2739
    const-string v44, "dtend"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 2743
    :cond_7
    const-wide/16 v44, 0x0

    cmp-long v44, v36, v44

    if-eqz v44, :cond_8

    .line 2744
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "DTSTART"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    const/16 v45, 0x1

    move-wide/from16 v0, v36

    move-object/from16 v2, v42

    move/from16 v3, v45

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2747
    :cond_8
    if-eqz v23, :cond_9

    .line 2748
    const-string v44, "originalInstanceTime"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Long;->longValue()J

    move-result-wide v32

    .line 2749
    .local v32, "originalTime":J
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "RECURRENCE-ID"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    if-nez v7, :cond_10

    const/16 v44, 0x1

    :goto_3
    move-wide/from16 v0, v32

    move-object/from16 v2, v42

    move/from16 v3, v44

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v22

    move-object/from16 v1, v45

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2752
    .end local v32    # "originalTime":J
    :cond_9
    const-string v44, "duration"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-nez v44, :cond_11

    .line 2753
    const-string v44, "dtend"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_a

    .line 2754
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "DTEND"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    const/16 v45, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v45

    invoke-static {v14, v15, v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2769
    :cond_a
    :goto_4
    const-string v44, "DTSTAMP"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_12

    .line 2770
    const-string v44, "LAST-MODIFIED"

    const-string v45, "DTSTAMP"

    move-object/from16 v0, v16

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2775
    :goto_5
    const/16 v30, 0x0

    .local v30, "organizerEmail":Ljava/lang/String;
    const/4 v9, 0x0

    .line 2776
    .local v9, "attendeeEmail":Ljava/lang/String;
    new-instance v21, Ljava/lang/StringBuffer;

    const-string v44, "ORGANIZER;SENT-BY="

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2777
    .local v21, "icalTag":Ljava/lang/StringBuffer;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 2778
    .local v17, "fwdAttendeeEmail":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 2780
    .local v18, "fwdAttendeeName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_6
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v44

    if-eqz v44, :cond_15

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/content/Entity$NamedContentValues;

    .line 2781
    .local v27, "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v28, v0

    .line 2782
    .local v28, "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v27

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v29, v0

    .line 2783
    .local v29, "ncvValues":Landroid/content/ContentValues;
    sget-object v44, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v28

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 2784
    const-string v44, "attendeeRelationship"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v31

    .line 2785
    .local v31, "relationship":Ljava/lang/Integer;
    if-eqz v31, :cond_b

    const-string v44, "attendeeEmail"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_b

    .line 2786
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v44

    const/16 v45, 0x2

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_14

    .line 2787
    const-string v44, "attendeeStatus"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Integer;->intValue()I

    move-result v38

    .line 2788
    .local v38, "status":I
    if-nez v38, :cond_13

    .line 2789
    const-string v44, "attendeeEmail"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v30

    .line 2790
    goto :goto_6

    .line 2692
    .end local v7    # "allDayEvent":Z
    .end local v9    # "attendeeEmail":Ljava/lang/String;
    .end local v14    # "endTime":J
    .end local v17    # "fwdAttendeeEmail":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v18    # "fwdAttendeeName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v21    # "icalTag":Ljava/lang/StringBuffer;
    .end local v22    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .end local v26    # "method":Ljava/lang/String;
    .end local v27    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v28    # "ncvUri":Landroid/net/Uri;
    .end local v29    # "ncvValues":Landroid/content/ContentValues;
    .end local v30    # "organizerEmail":Ljava/lang/String;
    .end local v31    # "relationship":Ljava/lang/Integer;
    .end local v36    # "startTime":J
    .end local v38    # "status":I
    .end local v41    # "vCalendarDateSuffix":Ljava/lang/String;
    .end local v42    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :cond_c
    and-int/lit8 v44, p3, 0x20

    if-eqz v44, :cond_d

    .line 2693
    const-string v26, "CANCEL"

    .restart local v26    # "method":Ljava/lang/String;
    goto/16 :goto_0

    .line 2695
    .end local v26    # "method":Ljava/lang/String;
    :cond_d
    const-string v26, "REPLY"

    .line 2696
    .restart local v26    # "method":Ljava/lang/String;
    const/16 v24, 0x1

    goto/16 :goto_0

    .line 2711
    .restart local v6    # "ade":Ljava/lang/Integer;
    .restart local v7    # "allDayEvent":Z
    .restart local v22    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .restart local v41    # "vCalendarDateSuffix":Ljava/lang/String;
    .restart local v42    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 2732
    .end local v6    # "ade":Ljava/lang/Integer;
    :cond_f
    :try_start_1
    const-string v44, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v46

    invoke-static/range {v46 .. v47}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 2886
    .end local v7    # "allDayEvent":Z
    .end local v22    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .end local v41    # "vCalendarDateSuffix":Ljava/lang/String;
    .end local v42    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :catch_0
    move-exception v11

    .line 2887
    .local v11, "e":Ljava/io/IOException;
    const-string v44, "CalendarUtility"

    const-string v45, "IOException in updateMessageForForwardEntity"

    invoke-static/range {v44 .. v45}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2888
    const/16 p1, 0x0

    .line 2890
    .end local v11    # "e":Ljava/io/IOException;
    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_7
    return-object p1

    .line 2749
    .restart local v7    # "allDayEvent":Z
    .restart local v14    # "endTime":J
    .restart local v22    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .restart local v32    # "originalTime":J
    .restart local v36    # "startTime":J
    .restart local v41    # "vCalendarDateSuffix":Ljava/lang/String;
    .restart local v42    # "vCalendarTimeZone":Ljava/util/TimeZone;
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_10
    const/16 v44, 0x0

    goto/16 :goto_3

    .line 2758
    .end local v32    # "originalTime":J
    :cond_11
    const-wide/32 v12, 0x36ee80

    .line 2759
    .local v12, "durationMillis":J
    :try_start_2
    new-instance v10, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct {v10}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2761
    .local v10, "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_3
    const-string v44, "duration"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v10, v0}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 2762
    invoke-virtual {v10}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-wide v12

    .line 2766
    :goto_8
    :try_start_4
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "DTEND"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    add-long v46, v36, v12

    const/16 v45, 0x1

    move-wide/from16 v0, v46

    move-object/from16 v2, v42

    move/from16 v3, v45

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2763
    :catch_1
    move-exception v11

    .line 2764
    .local v11, "e":Ljava/text/ParseException;
    const-string v44, "CalendarUtility"

    const-string v45, "ParseException in updateMessageForForwardEntity"

    invoke-static/range {v44 .. v45}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 2772
    .end local v10    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v11    # "e":Ljava/text/ParseException;
    .end local v12    # "durationMillis":J
    :cond_12
    const-string v44, "LAST-MODIFIED"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v46

    invoke-static/range {v46 .. v47}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2792
    .restart local v9    # "attendeeEmail":Ljava/lang/String;
    .restart local v17    # "fwdAttendeeEmail":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v18    # "fwdAttendeeName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v20    # "i$":Ljava/util/Iterator;
    .restart local v21    # "icalTag":Ljava/lang/StringBuffer;
    .restart local v27    # "ncv":Landroid/content/Entity$NamedContentValues;
    .restart local v28    # "ncvUri":Landroid/net/Uri;
    .restart local v29    # "ncvValues":Landroid/content/ContentValues;
    .restart local v30    # "organizerEmail":Ljava/lang/String;
    .restart local v31    # "relationship":Ljava/lang/Integer;
    .restart local v38    # "status":I
    :cond_13
    const-string v44, "attendeeEmail"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2793
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "MAILTO:"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    invoke-static/range {v44 .. v44}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v21

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_6

    .line 2796
    .end local v38    # "status":I
    :cond_14
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v44

    const/16 v45, 0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_b

    .line 2797
    const-string v44, "attendeeEmail"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v17

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2798
    const-string v44, "attendeeName"

    move-object/from16 v0, v29

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v18

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    .line 2803
    .end local v27    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v28    # "ncvUri":Landroid/net/Uri;
    .end local v29    # "ncvValues":Landroid/content/ContentValues;
    .end local v31    # "relationship":Ljava/lang/Integer;
    :cond_15
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v44

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "MAILTO:"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2805
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_9
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v44

    move/from16 v0, v19

    move/from16 v1, v44

    if-ge v0, v1, :cond_16

    .line 2806
    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Ljava/lang/String;

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    move/from16 v3, p3

    move-object/from16 v4, p5

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/exchange/utility/CalendarUtilities;->addForwardedAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 2805
    add-int/lit8 v19, v19, 0x1

    goto :goto_9

    .line 2808
    :cond_16
    const-string v44, "SUMMARY"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    move-object/from16 v45, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2809
    const/16 v25, 0x0

    .line 2810
    .local v25, "location":Ljava/lang/String;
    const-string v44, "eventLocation"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_17

    .line 2811
    const-string v44, "eventLocation"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 2812
    const-string v44, "LOCATION"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2814
    :cond_17
    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    .line 2815
    .local v35, "sb":Ljava/lang/StringBuilder;
    const/16 v44, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, v35

    move/from16 v3, v44

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->buildMessageTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;Z)Ljava/lang/String;

    move-result-object v40

    .line 2816
    .local v40, "text":Ljava/lang/String;
    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->length()I

    move-result v44

    if-lez v44, :cond_18

    .line 2817
    const-string v44, "DESCRIPTION"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2819
    :cond_18
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v44, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v44

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2820
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v44, v0

    if-eqz v44, :cond_1e

    .line 2821
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    const-string v45, "\n\n"

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2822
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v45, v0

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2826
    :goto_a
    const/16 v44, 0x0

    invoke-static/range {v44 .. v44}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v43

    .line 2827
    .local v43, "visibility":Ljava/lang/Integer;
    const-string v44, "accessLevel"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_19

    .line 2828
    const-string v44, "accessLevel"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v43

    .line 2829
    invoke-virtual/range {v43 .. v43}, Ljava/lang/Integer;->intValue()I

    move-result v44

    packed-switch v44, :pswitch_data_0

    .line 2843
    const-string v44, "CLASS"

    const-string v45, "DEFAULT"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2847
    :cond_19
    :goto_b
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    move/from16 v44, v0

    if-nez v44, :cond_1f

    .line 2848
    const-string v44, "PRIORITY"

    const-string v45, "9"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2854
    :cond_1a
    :goto_c
    if-nez v24, :cond_1c

    .line 2855
    const-string v44, "allDay"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v44

    if-eqz v44, :cond_1b

    .line 2856
    const-string v44, "allDay"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 2857
    .restart local v6    # "ade":Ljava/lang/Integer;
    const-string v45, "X-MICROSOFT-CDO-ALLDAYEVENT"

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v44

    if-nez v44, :cond_21

    const-string v44, "FALSE"

    :goto_d
    move-object/from16 v0, v22

    move-object/from16 v1, v45

    move-object/from16 v2, v44

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2859
    .end local v6    # "ade":Ljava/lang/Integer;
    :cond_1b
    const-string v44, "rrule"

    move-object/from16 v0, v16

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 2860
    .local v34, "rrule":Ljava/lang/String;
    if-eqz v34, :cond_1c

    .line 2861
    const-string v44, "RRULE"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2864
    .end local v34    # "rrule":Ljava/lang/String;
    :cond_1c
    sget v44, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I

    const/16 v45, -0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-eq v0, v1, :cond_22

    .line 2865
    invoke-static/range {v22 .. v22}, Lcom/android/exchange/utility/CalendarUtilities;->writeVAlarm(Lcom/android/exchange/utility/SimpleIcsWriter;)V

    .line 2868
    :goto_e
    const-string v44, "END"

    const-string v45, "VEVENT"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2869
    const-string v44, "END"

    const-string v45, "VCALENDAR"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 2871
    const-string v44, "CalendarUtility"

    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    const-string v46, "Encoded iCalendar follows below. \n "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/utility/SimpleIcsWriter;->toString()Ljava/lang/String;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    invoke-static/range {v44 .. v45}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 2872
    new-instance v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v8}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 2873
    .local v8, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/utility/SimpleIcsWriter;->getBytes()[B

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    .line 2874
    new-instance v44, Ljava/lang/StringBuilder;

    invoke-direct/range {v44 .. v44}, Ljava/lang/StringBuilder;-><init>()V

    const-string v45, "text/calendar; method="

    invoke-virtual/range {v44 .. v45}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    move-object/from16 v0, v44

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v44

    invoke-virtual/range {v44 .. v44}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, v44

    iput-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 2875
    const-string v44, "invite.ics"

    move-object/from16 v0, v44

    iput-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 2876
    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    array-length v0, v0

    move/from16 v44, v0

    move/from16 v0, v44

    int-to-long v0, v0

    move-wide/from16 v44, v0

    move-wide/from16 v0, v44

    iput-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 2877
    const/16 v44, 0x1

    move/from16 v0, v44

    iput v0, v8, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    .line 2879
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    if-nez v44, :cond_1d

    .line 2880
    new-instance v44, Ljava/util/ArrayList;

    invoke-direct/range {v44 .. v44}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2882
    :cond_1d
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    move-object/from16 v44, v0

    move-object/from16 v0, v44

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2883
    const/16 v44, 0x1

    move/from16 v0, v44

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 2884
    const/16 v44, 0x1

    move/from16 v0, v44

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 2885
    const/16 v44, 0x1

    move/from16 v0, v44

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    goto/16 :goto_7

    .line 2824
    .end local v8    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v43    # "visibility":Ljava/lang/Integer;
    :cond_1e
    move-object/from16 v0, v40

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto/16 :goto_a

    .line 2831
    .restart local v43    # "visibility":Ljava/lang/Integer;
    :pswitch_0
    const-string v44, "CLASS"

    const-string v45, "DEFAULT"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2834
    :pswitch_1
    const-string v44, "CLASS"

    const-string v45, "PUBLIC"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2837
    :pswitch_2
    const-string v44, "CLASS"

    const-string v45, "PRIVATE"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2840
    :pswitch_3
    const-string v44, "CLASS"

    const-string v45, "CONFIDENTIAL"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 2849
    :cond_1f
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    move/from16 v44, v0

    const/16 v45, 0x1

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_20

    .line 2850
    const-string v44, "PRIORITY"

    const-string v45, "5"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 2851
    :cond_20
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    move/from16 v44, v0

    const/16 v45, 0x2

    move/from16 v0, v44

    move/from16 v1, v45

    if-ne v0, v1, :cond_1a

    .line 2852
    const-string v44, "PRIORITY"

    const-string v45, "1"

    move-object/from16 v0, v22

    move-object/from16 v1, v44

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c

    .line 2857
    .restart local v6    # "ade":Ljava/lang/Integer;
    :cond_21
    const-string v44, "TRUE"

    goto/16 :goto_d

    .line 2867
    .end local v6    # "ade":Ljava/lang/Integer;
    :cond_22
    const-string v44, "CalendarUtility"

    const-string v45, "None selected for Alarm"

    invoke-static/range {v44 .. v45}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_e

    .line 2829
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static updateProposeNewTimeMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 57
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "entity"    # Landroid/content/Entity;
    .param p3, "uid"    # Ljava/lang/String;
    .param p4, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p5, "specifiedAttendee"    # Ljava/lang/String;

    .prologue
    .line 3013
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v23

    .line 3014
    .local v23, "entityValues":Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v47

    .line 3017
    .local v47, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    const-string v6, "original_sync_id"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v28

    .line 3018
    .local v28, "isException":Z
    const/16 v29, 0x0

    .line 3021
    .local v29, "isReply":Z
    if-nez p1, :cond_0

    .line 3022
    new-instance p1, Lcom/android/emailcommon/provider/EmailContent$Message;

    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    invoke-direct/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 3023
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v6, 0x1

    move-object/from16 v0, p1

    iput v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    .line 3025
    :cond_0
    move-object/from16 v0, p1

    iget v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    or-int/lit16 v6, v6, 0x100

    move-object/from16 v0, p1

    iput v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 3027
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p1

    iput-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    .line 3030
    const-string v32, "COUNTER"

    .line 3031
    .local v32, "method":Ljava/lang/String;
    const/16 v29, 0x1

    .line 3035
    :try_start_0
    new-instance v4, Lcom/android/exchange/utility/SimpleIcsWriter;

    invoke-direct {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;-><init>()V

    .line 3036
    .local v4, "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    const-string v6, "BEGIN"

    const-string v7, "VCALENDAR"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3037
    const-string v6, "METHOD"

    move-object/from16 v0, v32

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3038
    const-string v6, "PRODID"

    const-string v7, "AndroidEmail"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3039
    const-string v6, "VERSION"

    const-string v7, "2.0"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3044
    sget-object v55, Lcom/android/exchange/utility/CalendarUtilities;->sGmtTimeZone:Ljava/util/TimeZone;

    .line 3045
    .local v55, "vCalendarTimeZone":Ljava/util/TimeZone;
    const-string v54, ""

    .line 3048
    .local v54, "vCalendarDateSuffix":Ljava/lang/String;
    const/4 v13, 0x0

    .line 3049
    .local v13, "allDayEvent":Z
    const-string v6, "allDay"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3050
    const-string v6, "allDay"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    .line 3051
    .local v12, "ade":Ljava/lang/Integer;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    const/4 v13, 0x1

    .line 3052
    :goto_0
    if-eqz v13, :cond_1

    .line 3054
    const-string v54, ";VALUE=DATE"

    .line 3065
    .end local v12    # "ade":Ljava/lang/Integer;
    :cond_1
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v55

    .line 3067
    move-object/from16 v0, v55

    invoke-static {v0, v4}, Lcom/android/exchange/utility/CalendarUtilities;->timeZoneToVTimezone(Ljava/util/TimeZone;Lcom/android/exchange/utility/SimpleIcsWriter;)V

    .line 3069
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ";TZID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v55 .. v55}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v54

    .line 3071
    const-string v6, "BEGIN"

    const-string v7, "VEVENT"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3075
    const/16 v37, 0x0

    .line 3076
    .local v37, "organizerName":Ljava/lang/String;
    const/16 v36, 0x0

    .line 3077
    .local v36, "organizerEmail":Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3078
    .local v5, "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    .local v26, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Entity$NamedContentValues;

    .line 3079
    .local v33, "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v34, v0

    .line 3080
    .local v34, "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v35, v0

    .line 3081
    .local v35, "ncvValues":Landroid/content/ContentValues;
    sget-object v6, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3082
    const-string v6, "attendeeRelationship"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    .line 3085
    .local v42, "relationship":Ljava/lang/Integer;
    if-eqz v42, :cond_2

    const-string v6, "attendeeEmail"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3087
    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 3088
    const-string v6, "attendeeName"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 3089
    const-string v6, "attendeeEmail"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3090
    goto :goto_1

    .line 3051
    .end local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    .end local v36    # "organizerEmail":Ljava/lang/String;
    .end local v37    # "organizerName":Ljava/lang/String;
    .end local v42    # "relationship":Ljava/lang/Integer;
    .restart local v12    # "ade":Ljava/lang/Integer;
    :cond_3
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 3092
    .end local v12    # "ade":Ljava/lang/Integer;
    .restart local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .restart local v34    # "ncvUri":Landroid/net/Uri;
    .restart local v35    # "ncvValues":Landroid/content/ContentValues;
    .restart local v36    # "organizerEmail":Ljava/lang/String;
    .restart local v37    # "organizerName":Ljava/lang/String;
    .restart local v42    # "relationship":Ljava/lang/Integer;
    :cond_4
    const-string v6, "attendeeEmail"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 3093
    .local v15, "attendeeEmail":Ljava/lang/String;
    const-string v6, "attendeeName"

    move-object/from16 v0, v35

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 3096
    .local v16, "attendeeName":Ljava/lang/String;
    if-eqz v15, :cond_2

    .line 3100
    if-eqz p5, :cond_5

    move-object/from16 v0, p5

    invoke-virtual {v15, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 3105
    :cond_5
    const-string v6, "ATTENDEE;PARTSTAT=TENTATIVE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MAILTO:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 3373
    .end local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .end local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .end local v13    # "allDayEvent":Z
    .end local v15    # "attendeeEmail":Ljava/lang/String;
    .end local v16    # "attendeeName":Ljava/lang/String;
    .end local v26    # "i$":Ljava/util/Iterator;
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    .end local v36    # "organizerEmail":Ljava/lang/String;
    .end local v37    # "organizerName":Ljava/lang/String;
    .end local v42    # "relationship":Ljava/lang/Integer;
    .end local v54    # "vCalendarDateSuffix":Ljava/lang/String;
    .end local v55    # "vCalendarTimeZone":Ljava/util/TimeZone;
    :catch_0
    move-exception v22

    .line 3374
    .local v22, "e":Ljava/io/IOException;
    const-string v6, "CalendarUtility"

    const-string v7, "IOException in createMessageForEntity"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 3375
    const/16 p1, 0x0

    .line 3379
    .end local v22    # "e":Ljava/io/IOException;
    .end local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_2
    return-object p1

    .line 3112
    .restart local v4    # "ics":Lcom/android/exchange/utility/SimpleIcsWriter;
    .restart local v5    # "toList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Address;>;"
    .restart local v13    # "allDayEvent":Z
    .restart local v26    # "i$":Ljava/util/Iterator;
    .restart local v36    # "organizerEmail":Ljava/lang/String;
    .restart local v37    # "organizerName":Ljava/lang/String;
    .restart local v54    # "vCalendarDateSuffix":Ljava/lang/String;
    .restart local v55    # "vCalendarTimeZone":Ljava/util/TimeZone;
    .restart local p1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_6
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_7

    if-eqz p5, :cond_7

    .line 3113
    const/4 v6, 0x0

    const/16 v8, 0x100

    move-object/from16 v7, p5

    move-object/from16 v9, p4

    invoke-static/range {v4 .. v9}, Lcom/android/exchange/utility/CalendarUtilities;->addAttendeeToMessage(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;ILcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 3117
    :cond_7
    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    .line 3118
    .local v56, "visibility":Ljava/lang/Integer;
    const-string v6, "accessLevel"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 3119
    const-string v6, "accessLevel"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v56

    .line 3120
    invoke-virtual/range {v56 .. v56}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 3134
    const-string v6, "CLASS"

    const-string v7, "DEFAULT"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3141
    :cond_8
    :goto_3
    if-eqz v36, :cond_a

    .line 3142
    new-instance v27, Ljava/lang/StringBuffer;

    const-string v6, "ORGANIZER"

    move-object/from16 v0, v27

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3146
    .local v27, "icalTag":Ljava/lang/StringBuffer;
    if-eqz v37, :cond_9

    .line 3147
    const-string v6, ";CN="

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-static/range {v37 .. v37}, Lcom/android/exchange/utility/SimpleIcsWriter;->quoteParamValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3149
    :cond_9
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MAILTO:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3150
    if-eqz v29, :cond_a

    .line 3151
    if-nez v37, :cond_16

    new-instance v6, Lcom/android/emailcommon/mail/Address;

    move-object/from16 v0, v36

    invoke-direct {v6, v0}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3156
    .end local v27    # "icalTag":Ljava/lang/StringBuffer;
    :cond_a
    const-string v6, "dtstart"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v48

    .line 3157
    .local v48, "startTime":J
    const-string v6, "PROPOSED_START_TIME"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v40

    .line 3159
    .local v40, "proposedStartTime":J
    const-wide/16 v6, 0x0

    cmp-long v6, v48, v6

    if-eqz v6, :cond_b

    .line 3160
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "X-MS-OLK-ORIGINALSTART"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-nez v13, :cond_17

    const/4 v6, 0x1

    :goto_5
    move-wide/from16 v0, v48

    move-object/from16 v2, v55

    invoke-static {v0, v1, v2, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3162
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DTSTART"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-nez v13, :cond_18

    const/4 v6, 0x1

    :goto_6
    move-wide/from16 v0, v40

    move-object/from16 v2, v55

    invoke-static {v0, v1, v2, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3169
    :cond_b
    if-eqz v28, :cond_c

    .line 3170
    const-string v6, "originalInstanceTime"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    .line 3171
    .local v38, "originalTime":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RECURRENCE-ID"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-nez v13, :cond_19

    const/4 v6, 0x1

    :goto_7
    move-wide/from16 v0, v38

    move-object/from16 v2, v55

    invoke-static {v0, v1, v2, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3175
    .end local v38    # "originalTime":J
    :cond_c
    const-string v6, "duration"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1c

    .line 3176
    const-string v6, "dtend"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 3177
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "X-MS-OLK-ORIGINALEND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v6, "dtend"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    if-nez v13, :cond_1a

    const/4 v6, 0x1

    :goto_8
    move-object/from16 v0, v55

    invoke-static {v8, v9, v0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3181
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DTEND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v6, "PROPOSED_END_TIME"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    if-nez v13, :cond_1b

    const/4 v6, 0x1

    :goto_9
    move-object/from16 v0, v55

    invoke-static {v8, v9, v0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3208
    :cond_d
    :goto_a
    const-string v6, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3210
    const-string v6, "title"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 3211
    .local v51, "title":Ljava/lang/String;
    if-nez v51, :cond_e

    .line 3212
    const-string v51, ""

    .line 3214
    :cond_e
    const-string v6, "SUMMARY"

    move-object/from16 v0, v51

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3216
    const-string v6, "LOC"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 3217
    .local v31, "location":Ljava/lang/String;
    if-eqz v31, :cond_1f

    .line 3218
    const-string v6, "LOCATION"

    move-object/from16 v0, v31

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3226
    :cond_f
    :goto_b
    const-string v6, "sync_data4"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 3227
    .local v46, "sequence":Ljava/lang/String;
    if-nez v46, :cond_10

    .line 3228
    const-string v46, "0"

    .line 3231
    :cond_10
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v43

    .line 3235
    .local v43, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    if-nez v6, :cond_11

    .line 3237
    const v6, 0x7f060006

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v51, v7, v8

    move-object/from16 v0, v43

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    .line 3242
    :cond_11
    const/16 v52, 0x0

    .line 3244
    .local v52, "titleId":I
    new-instance v45, Ljava/lang/StringBuilder;

    invoke-direct/range {v45 .. v45}, Ljava/lang/StringBuilder;-><init>()V

    .line 3245
    .local v45, "sb":Ljava/lang/StringBuilder;
    if-eqz v28, :cond_12

    if-nez v29, :cond_12

    .line 3248
    new-instance v17, Ljava/util/Date;

    const-string v6, "originalInstanceTime"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 3249
    .local v17, "date":Ljava/util/Date;
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v18

    .line 3250
    .local v18, "dateString":Ljava/lang/String;
    const v6, 0x7f060005

    move/from16 v0, v52

    if-ne v0, v6, :cond_20

    .line 3251
    const v6, 0x7f060010

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v18, v7, v8

    move-object/from16 v0, v43

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3255
    :goto_c
    const-string v6, "\n\n"

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3257
    .end local v17    # "date":Ljava/util/Date;
    .end local v18    # "dateString":Ljava/lang/String;
    :cond_12
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->buildProposeNewTimeTextFromEntityValues(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v50

    .line 3260
    .local v50, "text":Ljava/lang/String;
    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_13

    .line 3261
    const-string v6, "DESCRIPTION"

    move-object/from16 v0, v50

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3264
    :cond_13
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 3266
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v6, :cond_21

    .line 3267
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 3268
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v50

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 3273
    :goto_d
    const-string v6, ""

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    .line 3276
    if-nez v29, :cond_15

    .line 3277
    const-string v6, "allDay"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 3278
    const-string v6, "allDay"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    .line 3279
    .restart local v12    # "ade":Ljava/lang/Integer;
    const-string v7, "X-MICROSOFT-CDO-ALLDAYEVENT"

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_22

    const-string v6, "FALSE"

    :goto_e
    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3282
    .end local v12    # "ade":Ljava/lang/Integer;
    :cond_14
    const-string v6, "rrule"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 3283
    .local v44, "rrule":Ljava/lang/String;
    if-eqz v44, :cond_15

    .line 3284
    const-string v6, "RRULE"

    move-object/from16 v0, v44

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3293
    .end local v44    # "rrule":Ljava/lang/String;
    :cond_15
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_23

    .line 3294
    const/16 p1, 0x0

    goto/16 :goto_2

    .line 3122
    .end local v31    # "location":Ljava/lang/String;
    .end local v40    # "proposedStartTime":J
    .end local v43    # "resources":Landroid/content/res/Resources;
    .end local v45    # "sb":Ljava/lang/StringBuilder;
    .end local v46    # "sequence":Ljava/lang/String;
    .end local v48    # "startTime":J
    .end local v50    # "text":Ljava/lang/String;
    .end local v51    # "title":Ljava/lang/String;
    .end local v52    # "titleId":I
    :pswitch_0
    const-string v6, "CLASS"

    const-string v7, "DEFAULT"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3125
    :pswitch_1
    const-string v6, "CLASS"

    const-string v7, "PUBLIC"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3128
    :pswitch_2
    const-string v6, "CLASS"

    const-string v7, "PRIVATE"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3131
    :pswitch_3
    const-string v6, "CLASS"

    const-string v7, "CONFIDENTIAL"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 3151
    .restart local v27    # "icalTag":Ljava/lang/StringBuffer;
    :cond_16
    new-instance v6, Lcom/android/emailcommon/mail/Address;

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-direct {v6, v0, v1}, Lcom/android/emailcommon/mail/Address;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 3160
    .end local v27    # "icalTag":Ljava/lang/StringBuffer;
    .restart local v40    # "proposedStartTime":J
    .restart local v48    # "startTime":J
    :cond_17
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 3162
    :cond_18
    const/4 v6, 0x0

    goto/16 :goto_6

    .line 3171
    .restart local v38    # "originalTime":J
    :cond_19
    const/4 v6, 0x0

    goto/16 :goto_7

    .line 3177
    .end local v38    # "originalTime":J
    :cond_1a
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 3181
    :cond_1b
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 3190
    :cond_1c
    const-wide/32 v20, 0x36ee80

    .line 3191
    .local v20, "durationMillis":J
    new-instance v19, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct/range {v19 .. v19}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 3193
    .local v19, "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_2
    const-string v6, "duration"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 3194
    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-wide v20

    .line 3198
    :goto_f
    :try_start_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "X-MS-OLK-ORIGINALEND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    add-long v8, v40, v20

    if-nez v13, :cond_1d

    const/4 v6, 0x1

    :goto_10
    move-object/from16 v0, v55

    invoke-static {v8, v9, v0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3202
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DTEND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    add-long v8, v48, v20

    if-nez v13, :cond_1e

    const/4 v6, 0x1

    :goto_11
    move-object/from16 v0, v55

    invoke-static {v8, v9, v0, v6}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(JLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v7, v6}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 3198
    :cond_1d
    const/4 v6, 0x0

    goto :goto_10

    .line 3202
    :cond_1e
    const/4 v6, 0x0

    goto :goto_11

    .line 3219
    .end local v19    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v20    # "durationMillis":J
    .restart local v31    # "location":Ljava/lang/String;
    .restart local v51    # "title":Ljava/lang/String;
    :cond_1f
    const-string v6, "eventLocation"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 3220
    const-string v6, "eventLocation"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 3221
    const-string v6, "LOCATION"

    move-object/from16 v0, v31

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 3253
    .restart local v17    # "date":Ljava/util/Date;
    .restart local v18    # "dateString":Ljava/lang/String;
    .restart local v43    # "resources":Landroid/content/res/Resources;
    .restart local v45    # "sb":Ljava/lang/StringBuilder;
    .restart local v46    # "sequence":Ljava/lang/String;
    .restart local v52    # "titleId":I
    :cond_20
    const v6, 0x7f060011

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v18, v7, v8

    move-object/from16 v0, v43

    invoke-virtual {v0, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 3270
    .end local v17    # "date":Ljava/util/Date;
    .end local v18    # "dateString":Ljava/lang/String;
    .restart local v50    # "text":Ljava/lang/String;
    :cond_21
    move-object/from16 v0, v50

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto/16 :goto_d

    .line 3279
    .restart local v12    # "ade":Ljava/lang/Integer;
    :cond_22
    const-string v6, "TRUE"

    goto/16 :goto_e

    .line 3297
    .end local v12    # "ade":Ljava/lang/Integer;
    :cond_23
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v0, v6, [Lcom/android/emailcommon/mail/Address;

    move-object/from16 v53, v0

    .line 3298
    .local v53, "toArray":[Lcom/android/emailcommon/mail/Address;
    const/16 v24, 0x0

    .line 3299
    .local v24, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    move/from16 v25, v24

    .end local v24    # "i":I
    .local v25, "i":I
    :goto_12
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_24

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/emailcommon/mail/Address;

    .line 3300
    .local v11, "address":Lcom/android/emailcommon/mail/Address;
    add-int/lit8 v24, v25, 0x1

    .end local v25    # "i":I
    .restart local v24    # "i":I
    aput-object v11, v53, v25

    move/from16 v25, v24

    .line 3301
    .end local v24    # "i":I
    .restart local v25    # "i":I
    goto :goto_12

    .line 3303
    .end local v11    # "address":Lcom/android/emailcommon/mail/Address;
    :cond_24
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    if-nez v6, :cond_25

    .line 3305
    invoke-static/range {v53 .. v53}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    .line 3325
    :cond_25
    if-nez p3, :cond_26

    .line 3328
    const-string v6, "sync_data2"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 3330
    :cond_26
    if-eqz p3, :cond_27

    .line 3331
    const-string v6, "UID"

    move-object/from16 v0, p3

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3334
    :cond_27
    const-string v6, "SEQUENCE"

    move-object/from16 v0, v46

    invoke-virtual {v4, v6, v0}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3341
    const-string v6, "END"

    const-string v7, "VEVENT"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3342
    const-string v6, "END"

    const-string v7, "VCALENDAR"

    invoke-virtual {v4, v6, v7}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3346
    const-string v6, "CalendarUtility"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Encoded iCalendar follows below. \n "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 3350
    new-instance v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v14}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 3351
    .local v14, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    invoke-virtual {v4}, Lcom/android/exchange/utility/SimpleIcsWriter;->getBytes()[B

    move-result-object v6

    iput-object v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    .line 3352
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "text/calendar; method="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 3353
    const-string v6, "invite.ics"

    iput-object v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 3354
    iget-object v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    array-length v6, v6

    int-to-long v6, v6

    iput-wide v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 3356
    const/4 v6, 0x1

    iput v6, v14, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    .line 3360
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v6, :cond_28

    .line 3361
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 3362
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v10

    .line 3363
    .local v10, "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v10, :cond_28

    .line 3364
    const/16 v30, 0x0

    .local v30, "itr":I
    :goto_13
    array-length v6, v10

    move/from16 v0, v30

    if-ge v0, v6, :cond_28

    .line 3365
    aget-object v6, v10, v30

    const-wide/16 v8, 0x0

    iput-wide v8, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 3366
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    aget-object v7, v10, v30

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3364
    add-int/lit8 v30, v30, 0x1

    goto :goto_13

    .line 3371
    .end local v10    # "aAtts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v30    # "itr":I
    :cond_28
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3372
    const/4 v6, 0x1

    move-object/from16 v0, p1

    iput-boolean v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    .line 3195
    .end local v14    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v25    # "i":I
    .end local v31    # "location":Ljava/lang/String;
    .end local v43    # "resources":Landroid/content/res/Resources;
    .end local v45    # "sb":Ljava/lang/StringBuilder;
    .end local v46    # "sequence":Ljava/lang/String;
    .end local v50    # "text":Ljava/lang/String;
    .end local v51    # "title":Ljava/lang/String;
    .end local v52    # "titleId":I
    .end local v53    # "toArray":[Lcom/android/emailcommon/mail/Address;
    .restart local v19    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .restart local v20    # "durationMillis":J
    :catch_1
    move-exception v6

    goto/16 :goto_f

    .line 3120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static userResponsefromSelfAttendeeStatus(I)I
    .locals 1
    .param p0, "selfAttendeeStatus"    # I

    .prologue
    .line 1945
    packed-switch p0, :pswitch_data_0

    .line 1956
    :pswitch_0
    const/4 v0, 0x0

    .line 1958
    .local v0, "userResponse":I
    :goto_0
    return v0

    .line 1947
    .end local v0    # "userResponse":I
    :pswitch_1
    const/4 v0, 0x1

    .line 1948
    .restart local v0    # "userResponse":I
    goto :goto_0

    .line 1950
    .end local v0    # "userResponse":I
    :pswitch_2
    const/4 v0, 0x2

    .line 1951
    .restart local v0    # "userResponse":I
    goto :goto_0

    .line 1953
    .end local v0    # "userResponse":I
    :pswitch_3
    const/4 v0, 0x3

    .line 1954
    .restart local v0    # "userResponse":I
    goto :goto_0

    .line 1945
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static utcOffsetString(I)Ljava/lang/String;
    .locals 6
    .param p0, "offsetMinutes"    # I

    .prologue
    const/16 v5, 0x30

    const/16 v4, 0xa

    .line 614
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 615
    .local v2, "sb":Ljava/lang/StringBuilder;
    div-int/lit8 v0, p0, 0x3c

    .line 616
    .local v0, "hours":I
    if-gez v0, :cond_2

    .line 617
    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 618
    rsub-int/lit8 v0, v0, 0x0

    .line 622
    :goto_0
    rem-int/lit8 v1, p0, 0x3c

    .line 623
    .local v1, "minutes":I
    if-ge v0, v4, :cond_0

    .line 624
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 626
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 627
    if-ge v1, v4, :cond_1

    .line 628
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 630
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 631
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 620
    .end local v1    # "minutes":I
    :cond_2
    const/16 v3, 0x2b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static writeNoDST(Lcom/android/exchange/utility/SimpleIcsWriter;Ljava/util/TimeZone;Ljava/lang/String;)V
    .locals 4
    .param p0, "writer"    # Lcom/android/exchange/utility/SimpleIcsWriter;
    .param p1, "tz"    # Ljava/util/TimeZone;
    .param p2, "offsetString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 695
    const-string v0, "BEGIN"

    const-string v1, "STANDARD"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v0, "TZOFFSETFROM"

    invoke-virtual {p0, v0, p2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    const-string v0, "TZOFFSETTO"

    invoke-virtual {p0, v0, p2}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 699
    const-string v0, "DTSTART"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    const-string v0, "END"

    const-string v1, "STANDARD"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    const-string v0, "END"

    const-string v1, "VTIMEZONE"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    return-void
.end method

.method private static writeVAlarm(Lcom/android/exchange/utility/SimpleIcsWriter;)V
    .locals 3
    .param p0, "writer"    # Lcom/android/exchange/utility/SimpleIcsWriter;

    .prologue
    .line 3387
    const-string v0, "BEGIN"

    const-string v1, "VALARM"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3388
    const-string v0, "ACTION"

    const-string v1, "DISPLAY"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3389
    const-string v0, "DESCRIPTION"

    const-string v1, "REMINDER"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3390
    const-string v0, "TRIGGER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-PT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/exchange/utility/CalendarUtilities;->mMinutes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "M"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3391
    const-string v0, "END"

    const-string v1, "VALARM"

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/utility/SimpleIcsWriter;->writeTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 3392
    return-void
.end method
