.class Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;
.super Ljava/lang/Object;
.source "SSLCertVerificationHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/utility/SSLCertVerificationHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field private mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    iput-object v0, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/exchange/utility/SSLCertVerificationHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$1;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;
    .param p1, "x1"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->reset()V

    return-void
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    iput-object v0, p0, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .line 73
    return-void
.end method
