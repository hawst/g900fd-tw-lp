.class public Lcom/android/exchange/utility/ProxyUtils;
.super Ljava/lang/Object;
.source "ProxyUtils.java"


# static fields
.field private static final PREFERAPN_URI:Landroid/net/Uri;

.field private static cm:Landroid/net/ConnectivityManager;

.field private static lp:Landroid/net/LinkProperties;

.field private static final where:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 18
    sput-object v0, Lcom/android/exchange/utility/ProxyUtils;->cm:Landroid/net/ConnectivityManager;

    .line 19
    sput-object v0, Lcom/android/exchange/utility/ProxyUtils;->lp:Landroid/net/LinkProperties;

    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "numeric=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "gsm.sim.operator.numeric"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND current=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/utility/ProxyUtils;->where:Ljava/lang/String;

    .line 24
    const-string v0, "content://telephony/carriers/preferapn"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/utility/ProxyUtils;->PREFERAPN_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHost(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    .local v0, "host":Ljava/lang/String;
    const/4 v1, 0x0

    .line 40
    .local v1, "normalizedHost":Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/utility/ProxyUtils;->getProxy(Landroid/content/Context;)Landroid/net/ProxyInfo;

    move-result-object v2

    .line 41
    .local v2, "proxy":Landroid/net/ProxyInfo;
    if-eqz v2, :cond_0

    .line 42
    invoke-virtual {v2}, Landroid/net/ProxyInfo;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 47
    :goto_0
    if-eqz v0, :cond_1

    .line 48
    invoke-static {v0}, Lcom/android/exchange/utility/ProxyUtils;->normalizeProxy(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 53
    :goto_1
    const-string v3, "EmailProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "host = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-object v1

    .line 44
    :cond_0
    const/4 v3, 0x0

    invoke-static {p0, v3}, Lcom/android/exchange/utility/ProxyUtils;->readFromDB(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 50
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getPort(Landroid/content/Context;)I
    .locals 5
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, -0x1

    .line 66
    .local v0, "port":I
    invoke-static {p0}, Lcom/android/exchange/utility/ProxyUtils;->getProxy(Landroid/content/Context;)Landroid/net/ProxyInfo;

    move-result-object v1

    .line 67
    .local v1, "proxy":Landroid/net/ProxyInfo;
    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v1}, Landroid/net/ProxyInfo;->getPort()I

    move-result v0

    .line 72
    :goto_0
    const-string v2, "EmailProxy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "port = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return v0

    .line 70
    :cond_0
    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/android/exchange/utility/ProxyUtils;->readFromDB(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static getProxy(Landroid/content/Context;)Landroid/net/ProxyInfo;
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 80
    const/4 v1, 0x0

    .line 81
    .local v1, "proxy":Landroid/net/ProxyInfo;
    const/4 v0, 0x0

    .line 82
    .local v0, "activeNetwork":Landroid/net/NetworkInfo;
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    sput-object v2, Lcom/android/exchange/utility/ProxyUtils;->cm:Landroid/net/ConnectivityManager;

    .line 83
    sget-object v2, Lcom/android/exchange/utility/ProxyUtils;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveLinkProperties()Landroid/net/LinkProperties;

    move-result-object v2

    sput-object v2, Lcom/android/exchange/utility/ProxyUtils;->lp:Landroid/net/LinkProperties;

    .line 84
    sget-object v2, Lcom/android/exchange/utility/ProxyUtils;->cm:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 85
    sget-object v2, Lcom/android/exchange/utility/ProxyUtils;->lp:Landroid/net/LinkProperties;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 87
    sget-object v2, Lcom/android/exchange/utility/ProxyUtils;->lp:Landroid/net/LinkProperties;

    invoke-virtual {v2}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v1

    .line 89
    :cond_0
    return-object v1
.end method

.method private static getSelectedApnKey(Landroid/content/Context;)I
    .locals 9
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 154
    const/4 v8, 0x0

    .line 155
    .local v8, "key":Ljava/lang/String;
    const/4 v6, -0x1

    .line 156
    .local v6, "apn_key":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/exchange/utility/ProxyUtils;->PREFERAPN_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v2, v5

    const-string v5, "name ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 159
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 161
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 162
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 163
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 166
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 169
    :cond_1
    if-eqz v8, :cond_2

    .line 171
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 172
    :cond_2
    return v6

    .line 166
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static normalizeProxy(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "proxyAddress"    # Ljava/lang/String;

    .prologue
    .line 176
    const-string v5, "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 177
    .local v1, "ipPattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    .line 178
    .local v2, "matched":Z
    if-nez v2, :cond_0

    .line 196
    .end local p0    # "proxyAddress":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 182
    .restart local p0    # "proxyAddress":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 183
    .local v4, "sb":Ljava/lang/StringBuffer;
    const-string v5, "\\."

    invoke-virtual {p0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 185
    .local v0, "ipAry":[Ljava/lang/String;
    const/4 v5, 0x0

    :try_start_0
    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 186
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 187
    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 188
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 189
    const/4 v5, 0x2

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 190
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 191
    const/4 v5, 0x3

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 192
    :catch_0
    move-exception v3

    .line 194
    .local v3, "nfe":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method

.method private static readFromDB(Landroid/content/Context;I)Ljava/lang/String;
    .locals 13
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "type"    # I

    .prologue
    .line 96
    const/4 v9, 0x0

    .line 97
    .local v9, "host":Ljava/lang/String;
    const/4 v11, -0x1

    .line 98
    .local v11, "port":I
    const/4 v12, -0x1

    .line 99
    .local v12, "posOfApn":I
    const/4 v10, -0x1

    .line 100
    .local v10, "key":I
    const/4 v7, 0x0

    .line 102
    .local v7, "cursorApn":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    sget-object v3, Lcom/android/exchange/utility/ProxyUtils;->where:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 107
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 109
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_1

    .line 110
    invoke-static {p0}, Lcom/android/exchange/utility/ProxyUtils;->getSelectedApnKey(Landroid/content/Context;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 112
    :cond_0
    if-ne v12, v10, :cond_5

    .line 122
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 123
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 125
    :cond_2
    :goto_1
    const/4 v3, -0x1

    if-eq v12, v3, :cond_4

    .line 126
    sget-object v3, Landroid/provider/Telephony$Carriers;->CONTENT_URI:Landroid/net/Uri;

    int-to-long v4, v12

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, "url":Landroid/net/Uri;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "proxy"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "port"

    aput-object v4, v2, v3

    .line 130
    .local v2, "prj":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 131
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 133
    .local v6, "c":Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_1
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 134
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 135
    const/4 v3, 0x0

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 136
    const/4 v3, 0x1

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v11

    .line 141
    :cond_3
    if-eqz v6, :cond_4

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 145
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "url":Landroid/net/Uri;
    .end local v2    # "prj":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_4
    :goto_2
    if-nez p1, :cond_8

    .line 148
    .end local v9    # "host":Ljava/lang/String;
    :goto_3
    return-object v9

    .line 115
    .restart local v9    # "host":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x0

    :try_start_2
    invoke-interface {v7, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 116
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 119
    :catch_0
    move-exception v8

    .line 120
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 122
    if-eqz v7, :cond_2

    .line 123
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 122
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v7, :cond_6

    .line 123
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v3

    .line 138
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v1    # "url":Landroid/net/Uri;
    .restart local v2    # "prj":[Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_1
    move-exception v8

    .line 139
    .restart local v8    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 141
    if-eqz v6, :cond_4

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 141
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    if-eqz v6, :cond_7

    .line 142
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3

    .line 148
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "url":Landroid/net/Uri;
    .end local v2    # "prj":[Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_8
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_3
.end method
