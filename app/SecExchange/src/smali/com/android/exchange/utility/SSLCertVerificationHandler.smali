.class public Lcom/android/exchange/utility/SSLCertVerificationHandler;
.super Ljava/lang/Object;
.source "SSLCertVerificationHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/utility/SSLCertVerificationHandler$1;,
        Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;,
        Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;
    }
.end annotation


# static fields
.field public static final TIMEOUT:I

.field static final sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;-><init>(Lcom/android/exchange/utility/SSLCertVerificationHandler$1;)V

    sput-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    .line 57
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/android/exchange/utility/SSLCertVerificationHandler;->TIMEOUT:I

    return-void

    :cond_0
    const/16 v0, 0x50

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method public static finished(Ljava/lang/String;Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;)V
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "resultvalue"    # Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .prologue
    .line 143
    sget-object v1, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    monitor-enter v1

    .line 144
    :try_start_0
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    # setter for: Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    invoke-static {v0, p1}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->access$102(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    .line 145
    sget-object v0, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 146
    monitor-exit v1

    .line 147
    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static verify(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "username"    # Ljava/lang/String;

    .prologue
    .line 115
    sget-object v3, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    monitor-enter v3

    .line 116
    :try_start_0
    sget-object v1, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->SSL_VALIDATION_NOT_DONE:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    .local v1, "returnResult":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :try_start_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v2

    invoke-interface {v2, p1, p2}, Lcom/android/emailcommon/service/IEmailServiceCallback;->verifySSLCertificate(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    sget-object v2, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    sget v4, Lcom/android/exchange/utility/SSLCertVerificationHandler;->TIMEOUT:I

    int-to-long v4, v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 120
    sget-object v2, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    # getter for: Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->mValue:Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    invoke-static {v2}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->access$100(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    move-result-object v1

    .line 121
    sget-object v2, Lcom/android/exchange/utility/SSLCertVerificationHandler;->sResult:Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;

    # invokes: Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->reset()V
    invoke-static {v2}, Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;->access$200(Lcom/android/exchange/utility/SSLCertVerificationHandler$Result;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 131
    :goto_0
    :try_start_2
    monitor-exit v3

    return-object v1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "SSLCertVerificationHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "autodiscover ssl certificate verification interrupted, url : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " username : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "returnResult":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 126
    .restart local v1    # "returnResult":Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;
    :catch_1
    move-exception v0

    .line 127
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v2, "SSLCertVerificationHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "autodiscover ssl certificate verification remote exception, url : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " username : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
