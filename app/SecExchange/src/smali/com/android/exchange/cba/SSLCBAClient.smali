.class public Lcom/android/exchange/cba/SSLCBAClient;
.super Ljava/lang/Object;
.source "SSLCBAClient.java"


# static fields
.field private static final INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager;

.field private static sStaticContext:Landroid/content/Context;


# instance fields
.field mAlias:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mInsecure:Z

.field mKeyStorePassword:Ljava/lang/String;

.field mSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

.field mTempKeyStorePassword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    const/4 v1, 0x0

    new-instance v2, Lcom/android/exchange/cba/SSLCBAClient$1;

    invoke-direct {v2}, Lcom/android/exchange/cba/SSLCBAClient$1;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/cba/SSLCBAClient;->INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/android/exchange/cba/SSLCBAClient;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method public static getSSLSocketFactory(Landroid/content/Context;Ljava/lang/String;Z)Ljavax/net/ssl/SSLSocketFactory;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "insecure"    # Z

    .prologue
    .line 160
    new-instance v0, Lcom/android/exchange/cba/SSLCBAClient;

    invoke-direct {v0, p0}, Lcom/android/exchange/cba/SSLCBAClient;-><init>(Landroid/content/Context;)V

    .line 161
    .local v0, "client":Lcom/android/exchange/cba/SSLCBAClient;
    invoke-virtual {v0, p1, p2}, Lcom/android/exchange/cba/SSLCBAClient;->init(Ljava/lang/String;Z)V

    .line 162
    iget-object v1, v0, Lcom/android/exchange/cba/SSLCBAClient;->mSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    return-object v1
.end method

.method public static getStaticContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/android/exchange/cba/SSLCBAClient;->sStaticContext:Landroid/content/Context;

    return-object v0
.end method

.method public static setStaticContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "staticContext"    # Landroid/content/Context;

    .prologue
    .line 166
    sput-object p0, Lcom/android/exchange/cba/SSLCBAClient;->sStaticContext:Landroid/content/Context;

    .line 167
    return-void
.end method

.method private setupKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;
    .locals 1
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method private setupSSLContext(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;)Ljavax/net/ssl/SSLContext;
    .locals 7
    .param p1, "keyStore"    # Ljava/security/KeyStore;
    .param p2, "keyStorePassword"    # Ljava/lang/String;
    .param p3, "trustStore"    # Ljava/security/KeyStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/KeyManagementException;,
            Ljava/security/KeyStoreException;,
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 99
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v3

    .line 101
    .local v3, "trustManagerFactory":Ljavax/net/ssl/TrustManagerFactory;
    invoke-virtual {v3, p3}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    .line 103
    const/4 v0, 0x0

    .line 106
    .local v0, "keyManager":[Ljavax/net/ssl/KeyManager;
    if-nez p1, :cond_0

    .line 107
    const/4 v4, 0x1

    new-array v0, v4, [Ljavax/net/ssl/KeyManager;

    .end local v0    # "keyManager":[Ljavax/net/ssl/KeyManager;
    const/4 v4, 0x0

    new-instance v5, Lcom/android/exchange/CBAEmailKeyManager;

    invoke-direct {v5, p0}, Lcom/android/exchange/CBAEmailKeyManager;-><init>(Lcom/android/exchange/cba/SSLCBAClient;)V

    aput-object v5, v0, v4

    .line 118
    .restart local v0    # "keyManager":[Ljavax/net/ssl/KeyManager;
    :goto_0
    const-string v4, "TLS"

    invoke-static {v4}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    .line 120
    .local v2, "sslCtx":Ljavax/net/ssl/SSLContext;
    iget-boolean v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mInsecure:Z

    if-nez v4, :cond_1

    .line 121
    invoke-virtual {v3}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v4

    invoke-virtual {v2, v0, v4, v6}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 126
    :goto_1
    return-object v2

    .line 111
    .end local v2    # "sslCtx":Ljavax/net/ssl/SSLContext;
    :cond_0
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v1

    .line 113
    .local v1, "keyManagerFactory":Ljavax/net/ssl/KeyManagerFactory;
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    invoke-virtual {v1, p1, v4}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    .line 114
    invoke-virtual {v1}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object v0

    goto :goto_0

    .line 123
    .end local v1    # "keyManagerFactory":Ljavax/net/ssl/KeyManagerFactory;
    .restart local v2    # "sslCtx":Ljavax/net/ssl/SSLContext;
    :cond_1
    sget-object v4, Lcom/android/exchange/cba/SSLCBAClient;->INSECURE_TRUST_MANAGER:[Ljavax/net/ssl/TrustManager;

    invoke-virtual {v2, v0, v4, v6}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    goto :goto_1
.end method

.method private declared-synchronized setupTrustStore()Ljava/security/KeyStore;
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method


# virtual methods
.method public chooseAlias()Ljava/lang/String;
    .locals 4

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 139
    .local v0, "alias":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->getAliasFromMap(J)Ljava/lang/String;

    move-result-object v0

    .line 146
    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/android/exchange/cba/SSLCBAClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public init(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "insecure"    # Z

    .prologue
    .line 59
    :try_start_0
    iput-object p1, p0, Lcom/android/exchange/cba/SSLCBAClient;->mAlias:Ljava/lang/String;

    .line 61
    iget-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mKeyStorePassword:Ljava/lang/String;

    .line 62
    iput-boolean p2, p0, Lcom/android/exchange/cba/SSLCBAClient;->mInsecure:Z

    .line 63
    iput-object p1, p0, Lcom/android/exchange/cba/SSLCBAClient;->mTempKeyStorePassword:Ljava/lang/String;

    .line 65
    iget-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mAlias:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/cba/SSLCBAClient;->mTempKeyStorePassword:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/cba/SSLCBAClient;->setupKeyStore(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 66
    .local v1, "keyStore":Ljava/security/KeyStore;
    invoke-direct {p0}, Lcom/android/exchange/cba/SSLCBAClient;->setupTrustStore()Ljava/security/KeyStore;

    move-result-object v3

    .line 67
    .local v3, "trustStore":Ljava/security/KeyStore;
    iget-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mTempKeyStorePassword:Ljava/lang/String;

    invoke-direct {p0, v1, v4, v3}, Lcom/android/exchange/cba/SSLCBAClient;->setupSSLContext(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;)Ljavax/net/ssl/SSLContext;

    move-result-object v2

    .line 69
    .local v2, "sslCtx":Ljavax/net/ssl/SSLContext;
    invoke-virtual {v2}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mSocketFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v1    # "keyStore":Ljava/security/KeyStore;
    .end local v2    # "sslCtx":Ljavax/net/ssl/SSLContext;
    .end local v3    # "trustStore":Ljava/security/KeyStore;
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 72
    const-string v4, "SSL"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v4

    check-cast v4, Ljavax/net/ssl/SSLSocketFactory;

    iput-object v4, p0, Lcom/android/exchange/cba/SSLCBAClient;->mSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    goto :goto_0
.end method
