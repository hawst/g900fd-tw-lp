.class public Lcom/android/exchange/cba/SSLUtils;
.super Ljava/lang/Object;
.source "SSLUtils.java"


# static fields
.field private static sInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private static sSecureFactory:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final declared-synchronized getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;
    .locals 3
    .param p0, "insecure"    # Z

    .prologue
    .line 46
    const-class v1, Lcom/android/exchange/cba/SSLUtils;

    monitor-enter v1

    if-eqz p0, :cond_1

    .line 47
    :try_start_0
    sget-object v0, Lcom/android/exchange/cba/SSLUtils;->sInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_0

    .line 49
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, p0}, Lcom/android/exchange/cba/SSLCBAClient;->getSSLSocketFactory(Landroid/content/Context;Ljava/lang/String;Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/cba/SSLUtils;->sInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 52
    :cond_0
    sget-object v0, Lcom/android/exchange/cba/SSLUtils;->sInsecureFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_0
    monitor-exit v1

    return-object v0

    .line 54
    :cond_1
    :try_start_1
    sget-object v0, Lcom/android/exchange/cba/SSLUtils;->sSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v0, :cond_2

    .line 56
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, p0}, Lcom/android/exchange/cba/SSLCBAClient;->getSSLSocketFactory(Landroid/content/Context;Ljava/lang/String;Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/cba/SSLUtils;->sSecureFactory:Ljavax/net/ssl/SSLSocketFactory;

    .line 59
    :cond_2
    sget-object v0, Lcom/android/exchange/cba/SSLUtils;->sSecureFactory:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
