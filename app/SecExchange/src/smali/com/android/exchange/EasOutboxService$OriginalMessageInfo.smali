.class public Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
.super Ljava/lang/Object;
.source "EasOutboxService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/EasOutboxService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "OriginalMessageInfo"
.end annotation


# instance fields
.field mCollectionId:Ljava/lang/String;

.field mForward:Z

.field mItemId:Ljava/lang/String;

.field mReply:Z

.field mSmartSend:Z

.field mSubject:Ljava/lang/String;

.field mcalRecEventInstanceId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 1
    .param p1, "subject"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "collectionId"    # Ljava/lang/String;
    .param p4, "calRecEventInstanceId"    # Ljava/lang/String;
    .param p5, "reply"    # Z
    .param p6, "forward"    # Z
    .param p7, "smartsend"    # Z

    .prologue
    const/4 v0, 0x0

    .line 550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 545
    iput-boolean v0, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mReply:Z

    .line 546
    iput-boolean v0, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mForward:Z

    .line 547
    iput-boolean v0, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mSmartSend:Z

    .line 551
    iput-object p1, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mSubject:Ljava/lang/String;

    .line 552
    iput-object p2, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mItemId:Ljava/lang/String;

    .line 553
    iput-object p3, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mCollectionId:Ljava/lang/String;

    .line 554
    iput-object p4, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mcalRecEventInstanceId:Ljava/lang/String;

    .line 555
    iput-boolean p5, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mReply:Z

    .line 556
    iput-boolean p6, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mForward:Z

    .line 557
    iput-boolean p7, p0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mSmartSend:Z

    .line 558
    return-void
.end method
