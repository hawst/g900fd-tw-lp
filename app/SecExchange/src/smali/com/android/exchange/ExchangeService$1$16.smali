.class Lcom/android/exchange/ExchangeService$1$16;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService$1;->setBadSyncAccount(JZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService$1;

.field final synthetic val$accountId:J

.field final synthetic val$nowBadSync:Z


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService$1;JZ)V
    .locals 0

    .prologue
    .line 843
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$1$16;->this$0:Lcom/android/exchange/ExchangeService$1;

    iput-wide p2, p0, Lcom/android/exchange/ExchangeService$1$16;->val$accountId:J

    iput-boolean p4, p0, Lcom/android/exchange/ExchangeService$1$16;->val$nowBadSync:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/android/emailcommon/service/IEmailServiceCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/android/emailcommon/service/IEmailServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 846
    iget-wide v0, p0, Lcom/android/exchange/ExchangeService$1$16;->val$accountId:J

    iget-boolean v2, p0, Lcom/android/exchange/ExchangeService$1$16;->val$nowBadSync:Z

    invoke-interface {p1, v0, v1, v2}, Lcom/android/emailcommon/service/IEmailServiceCallback;->setBadSyncAccount(JZ)V

    .line 847
    return-void
.end method
