.class Lcom/android/exchange/ExchangeService$9;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;)V
    .locals 0

    .prologue
    .line 5034
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$9;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 5038
    # getter for: Lcom/android/exchange/ExchangeService;->sStartingUp:Z
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$2900()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5058
    :goto_0
    return-void

    .line 5040
    :cond_0
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 5041
    :try_start_0
    const-string v1, "!!! EAS ExchangeService, onCreate"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 5046
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.exchange"

    const-class v3, Lcom/android/exchange/ExchangeService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5049
    .local v0, "compName":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$9;->this$0:Lcom/android/exchange/ExchangeService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.email.EXCHANGE_INTENT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/exchange/ExchangeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5051
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$9;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 5052
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$9;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/android/emailcommon/service/AccountServiceProxy;->mBindable:Z

    .line 5054
    :cond_1
    # getter for: Lcom/android/exchange/ExchangeService;->sStop:Z
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3100()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5055
    monitor-exit v2

    goto :goto_0

    .line 5057
    .end local v0    # "compName":Landroid/content/ComponentName;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .restart local v0    # "compName":Landroid/content/ComponentName;
    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
