.class public Lcom/android/exchange/EasAccountService;
.super Lcom/android/exchange/EasSyncService;
.source "EasAccountService.java"


# instance fields
.field private final mBindArguments:[Ljava/lang/String;

.field private mPingChangeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPingForceHeartbeat:I

.field mPingHeartbeat:I

.field mPingHeartbeatDropped:Z

.field private mPingHighWaterMark:I

.field mPingMaxHeartbeat:I

.field mPingMinHeartbeat:I

.field volatile mReset:Z

.field private mSyncHeartbeat:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 3
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    const/16 v2, 0x1d6

    const/4 v1, 0x0

    .line 165
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 136
    const/16 v0, 0x78

    iput v0, p0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    .line 140
    iput v2, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    .line 145
    const/16 v0, 0xdca

    iput v0, p0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    .line 150
    iput v2, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    .line 153
    iput v2, p0, Lcom/android/exchange/EasAccountService;->mSyncHeartbeat:I

    .line 154
    iput v1, p0, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    .line 156
    iput-boolean v1, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeatDropped:Z

    .line 158
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasAccountService;->mBindArguments:[Ljava/lang/String;

    .line 160
    iput-boolean v1, p0, Lcom/android/exchange/EasAccountService;->mReset:Z

    .line 166
    return-void
.end method

.method private allowNormalPing(D)Z
    .locals 3
    .param p1, "protocolVersion"    # D

    .prologue
    .line 918
    const-wide/high16 v0, 0x402c000000000000L    # 14.0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private isLikelyNatFailure(Ljava/lang/String;JI)Z
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "commandOutstandingTime"    # J
    .param p4, "heartBeatTime"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 864
    if-nez p1, :cond_1

    .line 879
    :cond_0
    :goto_0
    return v0

    .line 866
    :cond_1
    const-string v2, "reset by peer"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 867
    goto :goto_0

    .line 872
    :cond_2
    const-string v2, "ead timed out"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    int-to-long v2, p4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    .line 874
    new-array v2, v1, [Ljava/lang/String;

    const-string v3, "Read timed out for long heartbeat. Decrease heartbeat"

    aput-object v3, v2, v0

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    move v0, v1

    .line 875
    goto :goto_0
.end method

.method private parsePingResult(Ljava/io/InputStream;Landroid/content/ContentResolver;Ljava/util/HashMap;ILjava/util/ArrayList;)I
    .locals 19
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p4, "code"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Landroid/content/ContentResolver;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/StaleFolderListException;,
            Lcom/android/exchange/IllegalHeartbeatException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 1495
    .local p3, "errorMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p5, "pingMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v15, Lcom/android/exchange/adapter/PingParser;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v15, v0, v1}, Lcom/android/exchange/adapter/PingParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 1497
    .local v15, "pp":Lcom/android/exchange/adapter/PingParser;
    invoke-virtual {v15}, Lcom/android/exchange/adapter/PingParser;->parse()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasAccountService;->mBindArguments:[Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1502
    invoke-virtual {v15}, Lcom/android/exchange/adapter/PingParser;->getSyncList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/exchange/EasAccountService;->mPingChangeList:Ljava/util/ArrayList;

    .line 1503
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "thread="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ChangesFound="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasAccountService;->mPingChangeList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 1506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasAccountService;->mPingChangeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1507
    .local v16, "serverId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasAccountService;->mBindArguments:[Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v16, v2, v3

    .line 1508
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v5, "accountKey=? and serverId=?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mBindArguments:[Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1511
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_2

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1527
    const/16 v2, 0xf

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1528
    .local v17, "status":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/exchange/ExchangeService;->getStatusType(Ljava/lang/String;)I

    move-result v18

    .line 1530
    .local v18, "type":I
    const/4 v2, 0x3

    move/from16 v0, v18

    if-ne v0, v2, :cond_1

    .line 1531
    invoke-static/range {v17 .. v17}, Lcom/android/exchange/ExchangeService;->getStatusChangeCount(Ljava/lang/String;)I

    move-result v9

    .line 1532
    .local v9, "changeCount":I
    if-lez v9, :cond_3

    .line 1533
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1560
    .end local v9    # "changeCount":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v13

    .line 1561
    .local v13, "masterAutoSync":Z
    const/4 v2, 0x5

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 1564
    .local v12, "mailboxType":I
    if-eqz v13, :cond_2

    .line 1565
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1572
    .end local v12    # "mailboxType":I
    .end local v13    # "masterAutoSync":Z
    .end local v17    # "status":Ljava/lang/String;
    .end local v18    # "type":I
    :cond_2
    if-eqz v8, :cond_0

    .line 1573
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1534
    .restart local v9    # "changeCount":I
    .restart local v17    # "status":Ljava/lang/String;
    .restart local v18    # "type":I
    :cond_3
    if-nez v9, :cond_1

    .line 1538
    const/4 v2, 0x1

    :try_start_1
    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1539
    .local v14, "name":Ljava/lang/String;
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 1540
    .local v10, "failures":Ljava/lang/Integer;
    if-nez v10, :cond_5

    .line 1541
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Last ping reported changes in error for: "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v14, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1542
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1572
    .end local v9    # "changeCount":I
    .end local v10    # "failures":Ljava/lang/Integer;
    .end local v14    # "name":Ljava/lang/String;
    .end local v17    # "status":Ljava/lang/String;
    .end local v18    # "type":I
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_4

    .line 1573
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 1543
    .restart local v9    # "changeCount":I
    .restart local v10    # "failures":Ljava/lang/Integer;
    .restart local v14    # "name":Ljava/lang/String;
    .restart local v17    # "status":Ljava/lang/String;
    .restart local v18    # "type":I
    :cond_5
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_6

    .line 1545
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/android/exchange/EasAccountService;->pushFallback(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1572
    if-eqz v8, :cond_0

    .line 1573
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1548
    :cond_6
    const/4 v2, 0x2

    :try_start_3
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Last ping reported changes in error for: "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v14, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1549
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 1579
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "changeCount":I
    .end local v10    # "failures":Ljava/lang/Integer;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v14    # "name":Ljava/lang/String;
    .end local v16    # "serverId":Ljava/lang/String;
    .end local v17    # "status":Ljava/lang/String;
    .end local v18    # "type":I
    :cond_7
    invoke-virtual {v15}, Lcom/android/exchange/adapter/PingParser;->getSyncStatus()I

    move-result v2

    return v2
.end method

.method private parseSyncWithHBIResult(Ljava/io/InputStream;)V
    .locals 10
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 1588
    new-instance v5, Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-direct {v5, p0}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 1589
    .local v5, "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-virtual {v5, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->parse(Ljava/io/InputStream;)Z

    .line 1590
    iget-object v6, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    .line 1592
    .local v4, "masterAutoSync":Z
    iget-object v6, v5, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMoreAvailableSyncHBI:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1593
    iget-object v6, v5, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMoreAvailableSyncHBI:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1594
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v2

    .line 1599
    .local v2, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v2, :cond_0

    .line 1600
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 1601
    .local v3, "mailboxType":I
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    if-eqz v4, :cond_0

    .line 1603
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x3

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    goto :goto_0

    .line 1612
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v3    # "mailboxType":I
    :cond_1
    return-void
.end method

.method private pushFallback(J)V
    .locals 7
    .param p1, "mailboxId"    # J

    .prologue
    const/4 v5, 0x0

    .line 836
    sget-object v3, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-static {v3, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    .line 837
    .local v1, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v1, :cond_0

    .line 851
    :goto_0
    return-void

    .line 840
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 841
    .local v0, "cv":Landroid/content/ContentValues;
    const/16 v2, 0x19

    .line 842
    .local v2, "mins":I
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v3, :cond_1

    .line 843
    const/4 v2, 0x5

    .line 845
    :cond_1
    const-string v3, "syncInterval"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 846
    iget-object v3, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 848
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*** PING ERROR LOOP: Set "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " min sync"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    .line 850
    const-string v3, "push fallback"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private runPingLoop()V
    .locals 68
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/StaleFolderListException;,
            Lcom/android/exchange/IllegalHeartbeatException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 923
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    move/from16 v46, v0

    .line 924
    .local v46, "pingHeartbeat":I
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "runPingLoop"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 926
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x1b7740

    add-long v28, v6, v8

    .line 927
    .local v28, "endTime":J
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 928
    .local v15, "pingErrorMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 929
    .local v17, "readyMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v44, Ljava/util/ArrayList;

    invoke-direct/range {v44 .. v44}, Ljava/util/ArrayList;-><init>()V

    .line 930
    .local v44, "notReadyMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v51, 0x0

    .line 931
    .local v51, "pingWaitCount":I
    const-wide/16 v32, -0x1

    .line 933
    .local v32, "inboxId":J
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v6, v6, v28

    if-gez v6, :cond_3e

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v6

    if-nez v6, :cond_3e

    .line 935
    const/16 v56, 0x0

    .line 937
    .local v56, "pushCount":I
    const/16 v22, 0x0

    .line 939
    .local v22, "canPushCount":I
    const/16 v65, 0x0

    .line 941
    .local v65, "uninitCount":I
    const/16 v59, 0x0

    .line 942
    .local v59, "sPHeartBeatInterval":I
    new-instance v45, Ljava/util/HashMap;

    invoke-direct/range {v45 .. v45}, Ljava/util/HashMap;-><init>()V

    .line 943
    .local v45, "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    sget-object v6, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_6

    sget-object v6, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v59

    .line 946
    :goto_1
    new-instance v58, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v58 .. v58}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 947
    .local v58, "s":Lcom/android/exchange/adapter/Serializer;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "accountKey="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v66, v0

    move-wide/from16 v0, v66

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " AND syncInterval IN (-3,-2) AND type!=\"68\""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 950
    .local v21, "c":Landroid/database/Cursor;
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->clear()V

    .line 951
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 952
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mReset:Z

    .line 953
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v6}, Lcom/android/emailcommon/EasRefs;->getProtocolVersionDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v54

    .line 954
    .local v54, "protocolVersion":D
    const/16 v64, 0x0

    .line 956
    .local v64, "type":I
    const-wide/16 v6, -0x1

    cmp-long v6, v32, v6

    if-nez v6, :cond_1

    .line 957
    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v7, 0x0

    invoke-static {v6, v8, v9, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v32

    .line 962
    :cond_1
    if-eqz v21, :cond_f

    move/from16 v23, v22

    .line 966
    .end local v22    # "canPushCount":I
    .local v23, "canPushCount":I
    :cond_2
    :goto_2
    :try_start_0
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 967
    add-int/lit8 v56, v56, 0x1

    .line 972
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v42

    .line 973
    .local v42, "masterAutoSync":Z
    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v38

    .line 974
    .local v38, "mailboxId":J
    const/4 v6, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 975
    .local v41, "mailboxserver_id":Ljava/lang/String;
    invoke-static/range {v38 .. v39}, Lcom/android/exchange/ExchangeService;->pingStatus(J)I

    move-result v50

    .line 976
    .local v50, "pingStatus":I
    const/4 v6, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 978
    .local v40, "mailboxName":Ljava/lang/String;
    new-instance v62, Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v7, "com.android.exchange"

    move-object/from16 v0, v62

    invoke-direct {v0, v6, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    .local v62, "syncAccount":Landroid/accounts/Account;
    const/4 v6, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v64

    .line 981
    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v6, v54, v6

    if-ltz v6, :cond_3

    const/16 v6, 0x40

    move/from16 v0, v64

    if-ge v0, v6, :cond_3

    const/4 v6, 0x4

    move/from16 v0, v64

    if-eq v0, v6, :cond_3

    const/16 v6, 0x44

    move/from16 v0, v64

    if-eq v0, v6, :cond_3

    .line 983
    if-eqz v62, :cond_3

    const-string v6, "com.android.email.provider"

    move-object/from16 v0, v62

    invoke-static {v0, v6}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 985
    const/16 v50, 0x3

    .line 988
    :cond_3
    if-nez v50, :cond_b

    .line 989
    const/4 v6, 0x7

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v63

    .line 993
    .local v63, "syncKey":Ljava/lang/String;
    if-eqz v63, :cond_4

    const-string v6, "0"

    move-object/from16 v0, v63

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_4
    const/16 v19, 0x1

    .line 996
    .local v19, "bInitSync":Z
    :goto_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v27

    check-cast v27, Lcom/android/exchange/ExchangeService;

    .line 998
    .local v27, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v19, :cond_5

    if-eqz v42, :cond_5

    if-eqz v27, :cond_8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v0, v27

    move/from16 v1, v64

    invoke-virtual {v0, v6, v1}, Lcom/android/exchange/ExchangeService;->isMailboxSyncable(Lcom/android/emailcommon/provider/EmailContent$Account;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_8

    .line 1006
    :cond_5
    add-int/lit8 v56, v56, -0x1

    .line 1007
    if-eqz v19, :cond_2

    if-eqz v42, :cond_2

    .line 1008
    add-int/lit8 v65, v65, 0x1

    goto/16 :goto_2

    .line 943
    .end local v19    # "bInitSync":Z
    .end local v21    # "c":Landroid/database/Cursor;
    .end local v23    # "canPushCount":I
    .end local v27    # "exchangeService":Lcom/android/exchange/ExchangeService;
    .end local v38    # "mailboxId":J
    .end local v40    # "mailboxName":Ljava/lang/String;
    .end local v41    # "mailboxserver_id":Ljava/lang/String;
    .end local v42    # "masterAutoSync":Z
    .end local v50    # "pingStatus":I
    .end local v54    # "protocolVersion":D
    .end local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v62    # "syncAccount":Landroid/accounts/Account;
    .end local v63    # "syncKey":Ljava/lang/String;
    .end local v64    # "type":I
    .restart local v22    # "canPushCount":I
    :cond_6
    const/16 v59, 0x0

    goto/16 :goto_1

    .line 993
    .end local v22    # "canPushCount":I
    .restart local v21    # "c":Landroid/database/Cursor;
    .restart local v23    # "canPushCount":I
    .restart local v38    # "mailboxId":J
    .restart local v40    # "mailboxName":Ljava/lang/String;
    .restart local v41    # "mailboxserver_id":Ljava/lang/String;
    .restart local v42    # "masterAutoSync":Z
    .restart local v50    # "pingStatus":I
    .restart local v54    # "protocolVersion":D
    .restart local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v62    # "syncAccount":Landroid/accounts/Account;
    .restart local v63    # "syncKey":Ljava/lang/String;
    .restart local v64    # "type":I
    :cond_7
    const/16 v19, 0x0

    goto :goto_3

    .line 1012
    .restart local v19    # "bInitSync":Z
    .restart local v27    # "exchangeService":Lcom/android/exchange/ExchangeService;
    :cond_8
    add-int/lit8 v22, v23, 0x1

    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    if-nez v23, :cond_9

    .line 1022
    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasAccountService;->getTargetCollectionClassFromCursor(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v30

    .line 1026
    .local v30, "folderClass":Ljava/lang/String;
    const/4 v6, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    .line 1027
    .local v37, "mType":I
    const/4 v6, 0x4

    move/from16 v0, v37

    if-ne v0, v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v6, v6, 0x800

    if-nez v6, :cond_a

    move/from16 v23, v22

    .line 1029
    .end local v22    # "canPushCount":I
    .restart local v23    # "canPushCount":I
    goto/16 :goto_2

    .line 1033
    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    :cond_a
    const/4 v6, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v30, v7, v8

    const/4 v8, 0x1

    aput-object v63, v7, v8

    const/4 v8, 0x2

    aput-object v41, v7, v8

    const/4 v8, 0x3

    invoke-static/range {v37 .. v37}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-static/range {v38 .. v39}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    move-object/from16 v0, v45

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1044
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .end local v19    # "bInitSync":Z
    .end local v27    # "exchangeService":Lcom/android/exchange/ExchangeService;
    .end local v30    # "folderClass":Ljava/lang/String;
    .end local v37    # "mType":I
    .end local v63    # "syncKey":Ljava/lang/String;
    :goto_4
    move/from16 v23, v22

    .line 1053
    .end local v22    # "canPushCount":I
    .restart local v23    # "canPushCount":I
    goto/16 :goto_2

    .line 1045
    :cond_b
    const/4 v6, 0x1

    move/from16 v0, v50

    if-eq v0, v6, :cond_c

    const/4 v6, 0x2

    move/from16 v0, v50

    if-ne v0, v6, :cond_d

    .line 1047
    :cond_c
    :try_start_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v41

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v44

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v22, v23

    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    goto :goto_4

    .line 1048
    .end local v22    # "canPushCount":I
    .restart local v23    # "canPushCount":I
    :cond_d
    const/4 v6, 0x3

    move/from16 v0, v50

    if-ne v0, v6, :cond_3f

    .line 1049
    add-int/lit8 v56, v56, -0x1

    .line 1050
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v40, v6, v7

    const/4 v7, 0x1

    const-string v8, " in error state; ignore"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 1055
    .end local v38    # "mailboxId":J
    .end local v40    # "mailboxName":Ljava/lang/String;
    .end local v41    # "mailboxserver_id":Ljava/lang/String;
    .end local v42    # "masterAutoSync":Z
    .end local v50    # "pingStatus":I
    .end local v62    # "syncAccount":Landroid/accounts/Account;
    :catchall_0
    move-exception v6

    move/from16 v22, v23

    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    :goto_5
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    throw v6

    .end local v22    # "canPushCount":I
    .restart local v23    # "canPushCount":I
    :cond_e
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    move/from16 v22, v23

    .line 1059
    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    :cond_f
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_11

    .line 1060
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_10

    .line 1061
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Ping not ready for: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v44

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1063
    :cond_10
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_11

    .line 1064
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Ping ready for: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1070
    :cond_11
    invoke-virtual/range {v44 .. v44}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_14

    const/4 v6, 0x5

    move/from16 v0, v51

    if-le v0, v6, :cond_14

    const/4 v11, 0x1

    .line 1072
    .local v11, "forcePing":Z
    :goto_6
    if-lez v22, :cond_39

    move/from16 v0, v22

    move/from16 v1, v56

    if-eq v0, v1, :cond_12

    if-eqz v11, :cond_39

    .line 1076
    :cond_12
    const/16 v34, 0x0

    .line 1077
    .local v34, "isStickyPing":Z
    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->allowNormalPing(D)Z

    move-result v6

    if-eqz v6, :cond_15

    move-object/from16 v6, p0

    move-object/from16 v7, v58

    move-object/from16 v8, v45

    move/from16 v9, v46

    move/from16 v10, v59

    .line 1078
    invoke-virtual/range {v6 .. v11}, Lcom/android/exchange/EasAccountService;->createPingRequest(Lcom/android/exchange/adapter/Serializer;Ljava/util/HashMap;IIZ)Z

    move-result v34

    .line 1085
    :goto_7
    const/16 v51, 0x0

    .line 1086
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPostAborted:Z

    .line 1087
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPostReset:Z

    .line 1090
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1326
    .end local v11    # "forcePing":Z
    .end local v21    # "c":Landroid/database/Cursor;
    .end local v22    # "canPushCount":I
    .end local v34    # "isStickyPing":Z
    .end local v45    # "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v54    # "protocolVersion":D
    .end local v56    # "pushCount":I
    .end local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v59    # "sPHeartBeatInterval":I
    .end local v64    # "type":I
    .end local v65    # "uninitCount":I
    :cond_13
    :goto_8
    return-void

    .line 1070
    .restart local v21    # "c":Landroid/database/Cursor;
    .restart local v22    # "canPushCount":I
    .restart local v45    # "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local v54    # "protocolVersion":D
    .restart local v56    # "pushCount":I
    .restart local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v59    # "sPHeartBeatInterval":I
    .restart local v64    # "type":I
    .restart local v65    # "uninitCount":I
    :cond_14
    const/4 v11, 0x0

    goto :goto_6

    .line 1081
    .restart local v11    # "forcePing":Z
    .restart local v34    # "isStickyPing":Z
    :cond_15
    move-object/from16 v0, p0

    move-object/from16 v1, v58

    move-object/from16 v2, v45

    move/from16 v3, v46

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/EasAccountService;->createSyncHBIRequest(Lcom/android/exchange/adapter/Serializer;Ljava/util/HashMap;I)V

    goto :goto_7

    .line 1092
    :cond_16
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v52

    .line 1095
    .local v52, "pingTime":J
    if-eqz v11, :cond_17

    .line 1096
    const/4 v6, 0x1

    :try_start_3
    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Forcing ping after waiting for all boxes to be ready"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1100
    :cond_17
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_19

    if-nez v34, :cond_19

    .line 1101
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "runPingLoop:Wbxml"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1102
    invoke-virtual/range {v58 .. v58}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v18

    .line 1103
    .local v18, "b":[B
    new-instance v20, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1104
    .local v20, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v36, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 1105
    .local v36, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v36

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 1117
    .end local v18    # "b":[B
    .end local v20    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v36    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_18
    :goto_9
    const/16 v57, 0x0

    .line 1118
    .local v57, "res":Lcom/android/exchange/EasResponse;
    const/16 v45, 0x0

    .line 1119
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mReset:Z

    if-eqz v6, :cond_1a

    .line 1120
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "ExchangeService called stopPing(). New sync for some mailbox was started. Skip current Ping."

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 1235
    .end local v57    # "res":Lcom/android/exchange/EasResponse;
    :catch_0
    move-exception v60

    .line 1236
    .local v60, "se":Ljava/net/SocketTimeoutException;
    sget-object v6, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v7, " [runPingLoop] SocketTimeOutException Caught...simply reissue ping"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1106
    .end local v60    # "se":Ljava/net/SocketTimeoutException;
    :cond_19
    :try_start_4
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_18

    .line 1107
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "runPingLoop:Wbxml"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1108
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Sticky ping Request"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_9

    .line 1239
    :catch_1
    move-exception v25

    .line 1241
    .local v25, "cse":Lcom/android/exchange/CommandStatusException;
    move-object/from16 v0, v25

    iget v0, v0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    move/from16 v61, v0

    .line 1242
    .local v61, "status":I
    invoke-static/range {v61 .. v61}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isServerError(I)Z

    move-result v6

    if-eqz v6, :cond_30

    .line 1243
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " [runPingLoop] Sync with HBI Got Server error Status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v61

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    .line 1244
    const-string v6, " [runPingLoop] Ping loop sleeping for 5m"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    .line 1245
    const-wide/32 v6, 0x493e0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    goto/16 :goto_0

    .line 1125
    .end local v25    # "cse":Lcom/android/exchange/CommandStatusException;
    .end local v61    # "status":I
    .restart local v57    # "res":Lcom/android/exchange/EasResponse;
    :cond_1a
    :try_start_5
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " pingFolders="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " pingHeartbeat="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v46

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 1130
    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->allowNormalPing(D)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 1131
    if-eqz v34, :cond_1f

    .line 1132
    const/4 v7, 0x0

    if-eqz v11, :cond_1e

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    :goto_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v6}, Lcom/android/exchange/EasAccountService;->sendPing([BI)Lcom/android/exchange/EasResponse;
    :try_end_5
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    move-result-object v57

    .line 1146
    :goto_b
    :try_start_6
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v24

    .line 1147
    .local v24, "code":I
    const-string v6, "Ping response: "

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;I)V

    .line 1148
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " res="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 1153
    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->canAutoSync(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v6

    if-nez v6, :cond_1b

    .line 1154
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->stop()V

    .line 1158
    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v6

    if-eqz v6, :cond_22

    .line 1159
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Stopping pingLoop"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1232
    if-eqz v57, :cond_13

    .line 1233
    :try_start_7
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_8

    .line 1252
    .end local v24    # "code":I
    .end local v57    # "res":Lcom/android/exchange/EasResponse;
    :catch_2
    move-exception v26

    .line 1253
    .local v26, "e":Ljava/io/IOException;
    invoke-virtual/range {v26 .. v26}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v43

    .line 1256
    .local v43, "message":Ljava/lang/String;
    if-eqz v43, :cond_32

    const/16 v31, 0x1

    .line 1257
    .local v31, "hasMessage":Z
    :goto_c
    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException runPingLoop: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v31, :cond_33

    move-object/from16 v6, v43

    :goto_d
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1260
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v48, v6, v52

    .line 1261
    .local v48, "pingLength":J
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPostReset:Z

    if-nez v6, :cond_0

    .line 1264
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPostAborted:Z

    if-nez v6, :cond_1c

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    move-wide/from16 v2, v48

    move/from16 v4, v46

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasAccountService;->isLikelyNatFailure(Ljava/lang/String;JI)Z

    move-result v6

    if-eqz v6, :cond_37

    .line 1266
    :cond_1c
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    move/from16 v0, v46

    if-le v0, v6, :cond_34

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    move/from16 v0, v46

    if-le v0, v6, :cond_34

    .line 1268
    move/from16 v0, v46

    add-int/lit16 v0, v0, -0xb4

    move/from16 v46, v0

    .line 1269
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPingHeartbeatDropped:Z

    .line 1270
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    move/from16 v0, v46

    if-ge v0, v6, :cond_1d

    .line 1271
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    move/from16 v46, v0

    .line 1273
    :cond_1d
    const-string v6, "Decreased ping heartbeat to "

    const-string v7, "s"

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v6, v1, v7}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .end local v26    # "e":Ljava/io/IOException;
    .end local v31    # "hasMessage":Z
    .end local v43    # "message":Ljava/lang/String;
    .end local v48    # "pingLength":J
    .restart local v57    # "res":Lcom/android/exchange/EasResponse;
    :cond_1e
    move/from16 v6, v46

    .line 1132
    goto/16 :goto_a

    .line 1134
    :cond_1f
    :try_start_8
    invoke-virtual/range {v58 .. v58}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v7

    if-eqz v11, :cond_20

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    :goto_e
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v6}, Lcom/android/exchange/EasAccountService;->sendPing([BI)Lcom/android/exchange/EasResponse;

    move-result-object v57

    goto/16 :goto_b

    :cond_20
    move/from16 v6, v46

    goto :goto_e

    .line 1138
    :cond_21
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mIsSyncWithHBICmd:Z

    .line 1140
    const-string v6, "Sync"

    new-instance v7, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual/range {v58 .. v58}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    add-int/lit8 v8, v46, 0x5

    mul-int/lit16 v8, v8, 0x3e8

    const/4 v9, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7, v8, v9}, Lcom/android/exchange/EasAccountService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;IZ)Lcom/android/exchange/EasResponse;
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    move-result-object v57

    goto/16 :goto_b

    .line 1162
    .restart local v24    # "code":I
    :cond_22
    const/16 v47, -0x1

    .line 1163
    .local v47, "pingResult":I
    const/16 v6, 0xc8

    move/from16 v0, v24

    if-ne v0, v6, :cond_2c

    .line 1166
    :try_start_9
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->removeFromSyncErrorMap(J)V

    .line 1168
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v35

    .line 1170
    .local v35, "len":I
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    .line 1172
    .local v13, "is":Ljava/io/InputStream;
    move/from16 v16, v24

    .line 1173
    .local v16, "rescode":I
    if-eqz v35, :cond_2b

    .line 1176
    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->allowNormalPing(D)Z

    move-result v6

    if-eqz v6, :cond_29

    .line 1177
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v12, p0

    invoke-direct/range {v12 .. v17}, Lcom/android/exchange/EasAccountService;->parsePingResult(Ljava/io/InputStream;Landroid/content/ContentResolver;Ljava/util/HashMap;ILjava/util/ArrayList;)I

    move-result v47

    .line 1191
    :cond_23
    :goto_f
    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->allowNormalPing(D)Z

    move-result v6

    if-eqz v6, :cond_24

    if-eqz v35, :cond_24

    const/4 v6, 0x1

    move/from16 v0, v47

    if-ne v0, v6, :cond_24

    if-eqz v11, :cond_25

    :cond_24
    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v6, v54, v6

    if-ltz v6, :cond_28

    if-nez v35, :cond_28

    if-nez v11, :cond_28

    .line 1195
    :cond_25
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    move/from16 v0, v46

    if-le v0, v6, :cond_26

    .line 1196
    move/from16 v0, v46

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    .line 1197
    const-string v6, "Setting high water mark at: "

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;I)V

    .line 1199
    :cond_26
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    move/from16 v0, v46

    if-ge v0, v6, :cond_28

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPingHeartbeatDropped:Z

    if-nez v6, :cond_28

    .line 1200
    move/from16 v0, v46

    add-int/lit16 v0, v0, 0xb4

    move/from16 v46, v0

    .line 1201
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    move/from16 v0, v46

    if-le v0, v6, :cond_27

    .line 1202
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    move/from16 v46, v0

    .line 1204
    :cond_27
    const-string v6, "Increase ping heartbeat to "

    const-string v7, "s"

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v6, v1, v7}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1232
    .end local v13    # "is":Ljava/io/InputStream;
    .end local v16    # "rescode":I
    .end local v35    # "len":I
    :cond_28
    :goto_10
    if-eqz v57, :cond_0

    .line 1233
    :try_start_a
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    goto/16 :goto_0

    .line 1179
    .restart local v13    # "is":Ljava/io/InputStream;
    .restart local v16    # "rescode":I
    .restart local v35    # "len":I
    :cond_29
    :try_start_b
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/android/exchange/EasAccountService;->parseSyncWithHBIResult(Ljava/io/InputStream;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_f

    .line 1232
    .end local v13    # "is":Ljava/io/InputStream;
    .end local v16    # "rescode":I
    .end local v24    # "code":I
    .end local v35    # "len":I
    .end local v47    # "pingResult":I
    :catchall_1
    move-exception v6

    if-eqz v57, :cond_2a

    .line 1233
    :try_start_c
    invoke-virtual/range {v57 .. v57}, Lcom/android/exchange/EasResponse;->close()V

    :cond_2a
    throw v6
    :try_end_c
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    .line 1182
    .restart local v13    # "is":Ljava/io/InputStream;
    .restart local v16    # "rescode":I
    .restart local v24    # "code":I
    .restart local v35    # "len":I
    .restart local v47    # "pingResult":I
    :cond_2b
    :try_start_d
    move-object/from16 v0, p0

    move-wide/from16 v1, v54

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->allowNormalPing(D)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 1183
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Ping returned empty result; throwing IOException"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1184
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 1209
    .end local v13    # "is":Ljava/io/InputStream;
    .end local v16    # "rescode":I
    .end local v35    # "len":I
    :cond_2c
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasAccountService;->isProvisionError(I)Z

    move-result v6

    if-eqz v6, :cond_2d

    .line 1210
    const-string v6, "AT&T TEST"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "try Provisioin for Ping isProvisionError code : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    const/4 v6, 0x4

    move-object/from16 v0, p0

    iput v6, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 1213
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 1214
    :cond_2d
    invoke-static/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 1215
    const-string v6, "AT&T TEST"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mExitStatus = EXIT_LOGIN_FAILURE for Ping isAuthError code : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    const/4 v6, 0x2

    move-object/from16 v0, p0

    iput v6, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 1219
    const-string v6, "Authorization error during Ping: "

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v6, v1}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;I)V

    .line 1220
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6

    .line 1223
    :cond_2e
    const/16 v6, 0x1f4

    move/from16 v0, v24

    if-eq v0, v6, :cond_2f

    const/16 v6, 0x1f7

    move/from16 v0, v24

    if-ne v0, v6, :cond_28

    .line 1226
    :cond_2f
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPingOnHold:Z

    .line 1227
    const-string v6, " [runPingLoop] Ping loop sleeping for 5m"

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    .line 1228
    const-wide/32 v6, 0x493e0

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_10

    .line 1246
    .end local v24    # "code":I
    .end local v47    # "pingResult":I
    .end local v57    # "res":Lcom/android/exchange/EasResponse;
    .restart local v25    # "cse":Lcom/android/exchange/CommandStatusException;
    .restart local v61    # "status":I
    :cond_30
    invoke-static/range {v61 .. v61}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v6

    if-eqz v6, :cond_31

    .line 1247
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " [runPingLoop] Sync with HBI Got transient error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v61

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1249
    :cond_31
    throw v25

    .line 1256
    .end local v25    # "cse":Lcom/android/exchange/CommandStatusException;
    .end local v61    # "status":I
    .restart local v26    # "e":Ljava/io/IOException;
    .restart local v43    # "message":Ljava/lang/String;
    :cond_32
    const/16 v31, 0x0

    goto/16 :goto_c

    .line 1257
    .restart local v31    # "hasMessage":Z
    :cond_33
    const-string v6, "[no message]"

    goto/16 :goto_d

    .line 1274
    .restart local v48    # "pingLength":J
    :cond_34
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mPostAborted:Z

    if-eqz v6, :cond_35

    .line 1280
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Ping aborted; retry"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1281
    :cond_35
    const-wide/16 v6, 0x7d0

    cmp-long v6, v48, v6

    if-gez v6, :cond_36

    .line 1282
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Abort or NAT type return < 2 seconds; throwing IOException"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1283
    throw v26

    .line 1285
    :cond_36
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "NAT type IOException"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1287
    :cond_37
    if-eqz v31, :cond_38

    const-string v6, "roken pipe"

    move-object/from16 v0, v43

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1292
    :cond_38
    throw v26

    .line 1295
    .end local v26    # "e":Ljava/io/IOException;
    .end local v31    # "hasMessage":Z
    .end local v34    # "isStickyPing":Z
    .end local v43    # "message":Ljava/lang/String;
    .end local v48    # "pingLength":J
    .end local v52    # "pingTime":J
    :cond_39
    if-eqz v11, :cond_3a

    .line 1298
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "pingLoop waiting 60s for any pingable boxes"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1299
    const-wide/32 v6, 0xea60

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    goto/16 :goto_0

    .line 1300
    :cond_3a
    if-lez v56, :cond_3b

    .line 1303
    const-wide/16 v6, 0x7d0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    .line 1304
    add-int/lit8 v51, v51, 0x1

    goto/16 :goto_0

    .line 1306
    :cond_3b
    if-lez v65, :cond_3c

    .line 1310
    const-string v6, "pingLoop waiting for initial sync of "

    const-string v7, " box(es)"

    move-object/from16 v0, p0

    move/from16 v1, v65

    invoke-virtual {v0, v6, v1, v7}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 1311
    const-wide/16 v6, 0x2710

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    goto/16 :goto_0

    .line 1312
    :cond_3c
    const-wide/16 v6, -0x1

    cmp-long v6, v32, v6

    if-nez v6, :cond_3d

    .line 1314
    const-wide/32 v6, 0xafc8

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    goto/16 :goto_0

    .line 1319
    :cond_3d
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Account mailbox sleeping for 20m"

    aput-object v8, v6, v7

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 1320
    const-wide/32 v6, 0x124f80

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v8}, Lcom/android/exchange/EasAccountService;->sleep(JZ)V

    goto/16 :goto_0

    .line 1325
    .end local v11    # "forcePing":Z
    .end local v21    # "c":Landroid/database/Cursor;
    .end local v22    # "canPushCount":I
    .end local v45    # "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v54    # "protocolVersion":D
    .end local v56    # "pushCount":I
    .end local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v59    # "sPHeartBeatInterval":I
    .end local v64    # "type":I
    .end local v65    # "uninitCount":I
    :cond_3e
    move/from16 v0, v46

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    goto/16 :goto_8

    .line 1055
    .restart local v19    # "bInitSync":Z
    .restart local v21    # "c":Landroid/database/Cursor;
    .restart local v22    # "canPushCount":I
    .restart local v27    # "exchangeService":Lcom/android/exchange/ExchangeService;
    .restart local v38    # "mailboxId":J
    .restart local v40    # "mailboxName":Ljava/lang/String;
    .restart local v41    # "mailboxserver_id":Ljava/lang/String;
    .restart local v42    # "masterAutoSync":Z
    .restart local v45    # "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    .restart local v50    # "pingStatus":I
    .restart local v54    # "protocolVersion":D
    .restart local v56    # "pushCount":I
    .restart local v58    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v59    # "sPHeartBeatInterval":I
    .restart local v62    # "syncAccount":Landroid/accounts/Account;
    .restart local v63    # "syncKey":Ljava/lang/String;
    .restart local v64    # "type":I
    .restart local v65    # "uninitCount":I
    :catchall_2
    move-exception v6

    goto/16 :goto_5

    .end local v19    # "bInitSync":Z
    .end local v22    # "canPushCount":I
    .end local v27    # "exchangeService":Lcom/android/exchange/ExchangeService;
    .end local v63    # "syncKey":Ljava/lang/String;
    .restart local v23    # "canPushCount":I
    :cond_3f
    move/from16 v22, v23

    .end local v23    # "canPushCount":I
    .restart local v22    # "canPushCount":I
    goto/16 :goto_4
.end method

.method private sleep(JZ)V
    .locals 5
    .param p1, "ms"    # J
    .param p3, "runAsleep"    # Z

    .prologue
    .line 883
    if-eqz p3, :cond_0

    .line 884
    iget-wide v0, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v2, 0x1388

    add-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 887
    :cond_0
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 891
    if-eqz p3, :cond_1

    iget-boolean v0, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v0, :cond_1

    .line 892
    iget-wide v0, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 895
    :cond_1
    :goto_0
    return-void

    .line 888
    :catch_0
    move-exception v0

    .line 891
    if-eqz p3, :cond_1

    iget-boolean v0, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v0, :cond_1

    .line 892
    iget-wide v0, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    goto :goto_0

    .line 891
    :catchall_0
    move-exception v0

    if-eqz p3, :cond_2

    iget-boolean v1, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v1, :cond_2

    .line 892
    iget-wide v2, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    :cond_2
    throw v0
.end method


# virtual methods
.method createPingRequest(Lcom/android/exchange/adapter/Serializer;Ljava/util/HashMap;IIZ)Z
    .locals 16
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "pingHeartbeat"    # I
    .param p4, "sPHeartBeatInterval"    # I
    .param p5, "forcePing"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/adapter/Serializer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;IIZ)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1332
    .local p2, "pingFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 1334
    .local v10, "isStickyPing":Z
    const/4 v7, 0x0

    .line 1335
    .local v7, "hasChangeFolders":I
    const/4 v5, 0x0

    .line 1336
    .local v5, "folderClassIndex":I
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashSet;

    .line 1337
    .local v6, "folderMap":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 1339
    .local v11, "newFolderMap":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    move/from16 v0, p3

    move/from16 v1, p4

    if-eq v0, v1, :cond_0

    .line 1340
    const/16 v12, 0x345

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    const/16 v13, 0x348

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1344
    :cond_0
    if-eqz v6, :cond_7

    .line 1345
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1347
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 1350
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "hasChangeFolders":I
    .local v8, "hasChangeFolders":I
    if-nez v7, :cond_5

    move/from16 v0, p3

    move/from16 v1, p4

    if-eq v0, v1, :cond_5

    .line 1351
    const/16 v12, 0x349

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move v7, v8

    .line 1361
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v8    # "hasChangeFolders":I
    .restart local v7    # "hasChangeFolders":I
    :cond_2
    :goto_0
    if-nez v7, :cond_3

    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->size()I

    move-result v12

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v13

    if-eq v12, v13, :cond_3

    .line 1362
    add-int/lit8 v7, v7, 0x1

    .line 1363
    move/from16 v0, p3

    move/from16 v1, p4

    if-eq v0, v1, :cond_6

    .line 1364
    const/16 v12, 0x349

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1376
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    const/4 v12, -0x1

    if-ne v7, v12, :cond_4

    .line 1377
    const/16 v12, 0x349

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1380
    :cond_4
    if-eqz v7, :cond_a

    .line 1381
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1382
    .restart local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1383
    const/16 v12, 0x34a

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    const/16 v14, 0x34b

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v14, v12}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    const/16 v14, 0x34c

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    aget-object v12, v12, v5

    invoke-virtual {v13, v14, v12}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_2

    .line 1354
    .end local v7    # "hasChangeFolders":I
    .restart local v8    # "hasChangeFolders":I
    :cond_5
    const/16 v12, 0x345

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    const/16 v13, 0x349

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move v7, v8

    .line 1356
    .end local v8    # "hasChangeFolders":I
    .restart local v7    # "hasChangeFolders":I
    goto :goto_0

    .line 1367
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_6
    const/16 v12, 0x345

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    const/16 v13, 0x349

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto :goto_1

    .line 1371
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_7
    const/4 v7, -0x1

    goto :goto_1

    .line 1386
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1395
    .end local v9    # "i$":Ljava/util/Iterator;
    :goto_3
    if-eqz p5, :cond_c

    .line 1398
    if-eqz v7, :cond_9

    if-eqz v11, :cond_9

    invoke-virtual {v11}, Ljava/util/HashSet;->size()I

    move-result v12

    if-lez v12, :cond_9

    .line 1399
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1402
    :cond_9
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1413
    :goto_4
    const-string v12, "Sticky Ping"

    const-string v13, " start--"

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_e

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 1416
    .local v3, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/HashSet<Ljava/lang/String;>;>;"
    const-string v12, "Sticky Ping"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mPingFoldersMaps key["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1387
    .end local v3    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/HashSet<Ljava/lang/String;>;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_a
    move/from16 v0, p3

    move/from16 v1, p4

    if-eq v0, v1, :cond_b

    .line 1390
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->done()V

    goto/16 :goto_3

    .line 1393
    :cond_b
    const/4 v10, 0x1

    goto/16 :goto_3

    .line 1407
    :cond_c
    if-eqz v7, :cond_d

    if-eqz v11, :cond_d

    invoke-virtual {v11}, Ljava/util/HashSet;->size()I

    move-result v12

    if-lez v12, :cond_d

    .line 1408
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1411
    :cond_d
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 1419
    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_e
    sget-object v12, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1421
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    const-string v12, "Sticky Ping"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "mPingHeartBeatIntervalMap key["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "] = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1424
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/Integer;>;"
    :cond_f
    const-string v12, "Sticky Ping"

    const-string v13, " end--"

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    const/4 v7, 0x0

    .line 1426
    const/4 v11, 0x0

    .line 1428
    return v10
.end method

.method createSyncHBIRequest(Lcom/android/exchange/adapter/Serializer;Ljava/util/HashMap;I)V
    .locals 10
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "pingHeartbeat"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/adapter/Serializer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1433
    .local p2, "syncHBIFolderMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 1434
    .local v1, "folderClassIndex":I
    const/4 v6, 0x1

    .line 1435
    .local v6, "syncKeyIndex":I
    const/4 v4, 0x2

    .line 1436
    .local v4, "mailboxServerIDIndex":I
    const/4 v5, 0x3

    .line 1437
    .local v5, "mailboxTypeIndex":I
    const/4 v3, 0x4

    .line 1439
    .local v3, "mailboxIdIndex":I
    const/4 v7, 0x5

    invoke-virtual {p1, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    const/16 v8, 0x1c

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1441
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1442
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    const/16 v7, 0xf

    invoke-virtual {p1, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v8

    const/16 v9, 0xb

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    aget-object v7, v7, v6

    invoke-virtual {v8, v9, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v8

    const/16 v9, 0x12

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v8, v9, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1445
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    aget-object v7, v7, v3

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {p0, p1, v8, v9}, Lcom/android/exchange/EasAccountService;->syncHBIFolderDetail(Lcom/android/exchange/adapter/Serializer;J)V

    .line 1446
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_0

    .line 1449
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1450
    const/16 v7, 0x29

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1451
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1452
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 170
    invoke-super {p0}, Lcom/android/exchange/EasSyncService;->reset()V

    .line 171
    iput-boolean v0, p0, Lcom/android/exchange/EasAccountService;->mReset:Z

    .line 172
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EasAccountService.reset()"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method resetHeartbeats(I)V
    .locals 4
    .param p1, "legalHeartbeat"    # I

    .prologue
    const/4 v3, 0x0

    .line 802
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resetting min/max heartbeat, legal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 806
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    if-le p1, v0, :cond_3

    .line 810
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    if-ge v0, p1, :cond_0

    .line 811
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    .line 813
    :cond_0
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    if-ge v0, p1, :cond_1

    .line 814
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingForceHeartbeat:I

    .line 817
    :cond_1
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    iget v1, p0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    if-le v0, v1, :cond_2

    .line 818
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    .line 830
    :cond_2
    :goto_0
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    .line 832
    iput-boolean v3, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeatDropped:Z

    .line 833
    return-void

    .line 820
    :cond_3
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingHeartbeat:I

    if-ge p1, v0, :cond_2

    .line 823
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    .line 825
    iget v0, p0, Lcom/android/exchange/EasAccountService;->mPingMaxHeartbeat:I

    iget v1, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    if-ge v0, v1, :cond_2

    .line 826
    iput p1, p0, Lcom/android/exchange/EasAccountService;->mPingMinHeartbeat:I

    goto :goto_0
.end method

.method public run()V
    .locals 13

    .prologue
    const/4 v12, -0x2

    const/4 v7, 0x0

    .line 177
    iput v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 178
    const/4 v6, 0x0

    .line 182
    .local v6, "status":I
    :try_start_0
    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->setupService()Z

    move-result v7

    if-nez v7, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_0

    .line 186
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;
    :try_end_0
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v7, :cond_0

    .line 189
    :try_start_1
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/EasAccountService;->mDeviceId:Ljava/lang/String;

    .line 191
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v7, :cond_8

    .line 217
    :cond_2
    :try_start_2
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_3

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .local v3, "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_3
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_6

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_0

    .line 277
    :pswitch_0
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_2 .. :try_end_2} :catch_0

    .line 324
    :cond_4
    :goto_1
    :try_start_3
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_5

    const/16 v7, 0x20

    if-ne v6, v7, :cond_5

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_5
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_d
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_3 .. :try_end_3} :catch_0

    .line 338
    :goto_2
    :try_start_4
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 340
    :catch_0
    move-exception v2

    .line 341
    .local v2, "e":Lcom/android/emailcommon/provider/ProviderUnavailableException;
    sget-object v7, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v8, "EmailProvider unavailable; sync ended prematurely"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    .end local v2    # "e":Lcom/android/emailcommon/provider/ProviderUnavailableException;
    :pswitch_1
    if-nez v6, :cond_4

    .line 235
    const/16 v6, 0x20

    goto :goto_1

    .line 239
    :pswitch_2
    const/4 v6, 0x0

    .line 240
    :try_start_5
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .local v5, "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_1

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_3
    const/16 v6, 0x16

    .line 250
    goto/16 :goto_1

    .line 254
    :pswitch_4
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_1

    .line 257
    :pswitch_5
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_1

    .line 261
    :pswitch_6
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_1

    .line 265
    :pswitch_7
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_1

    .line 270
    :pswitch_8
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_1

    .line 274
    :pswitch_9
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_1

    .line 282
    :cond_6
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_7

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_7

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_7
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_1

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 291
    :pswitch_a
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 297
    :pswitch_b
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 305
    :pswitch_c
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 194
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_8
    :try_start_6
    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->sync()V
    :try_end_6
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 217
    :try_start_7
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_9

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_9
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_c

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_2

    .line 277
    :pswitch_d
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_7 .. :try_end_7} :catch_0

    .line 324
    :cond_a
    :goto_3
    :try_start_8
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_b

    const/16 v7, 0x20

    if-ne v6, v7, :cond_b

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_b
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_c
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_8 .. :try_end_8} :catch_0

    .line 338
    :goto_4
    :try_start_9
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_e
    if-nez v6, :cond_a

    .line 235
    const/16 v6, 0x20

    goto :goto_3

    .line 239
    :pswitch_f
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_3

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_10
    const/16 v6, 0x16

    .line 250
    goto :goto_3

    .line 254
    :pswitch_11
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_3

    .line 257
    :pswitch_12
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_3

    .line 261
    :pswitch_13
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_3

    .line 265
    :pswitch_14
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_3

    .line 270
    :pswitch_15
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_3

    .line 274
    :pswitch_16
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_3

    .line 282
    :cond_c
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_d

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_d

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_d
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_3

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 291
    :pswitch_17
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 297
    :pswitch_18
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 305
    :pswitch_19
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_3

    .line 196
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :catch_1
    move-exception v2

    .line 197
    .local v2, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_a
    const-string v7, "DeviceAccessPermission"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Caught Exceptoin, Device is blocked or quarantined "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/android/emailcommon/utility/DeviceAccessException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/4 v7, 0x6

    iput v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 200
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 217
    :try_start_b
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_e

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_e
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_11

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_4

    .line 277
    :pswitch_1a
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_b
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_b .. :try_end_b} :catch_0

    .line 324
    :cond_f
    :goto_5
    :try_start_c
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_10

    const/16 v7, 0x20

    if-ne v6, v7, :cond_10

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_10
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_b
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_c .. :try_end_c} :catch_0

    .line 338
    :goto_6
    :try_start_d
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_1b
    if-nez v6, :cond_f

    .line 235
    const/16 v6, 0x20

    goto :goto_5

    .line 239
    :pswitch_1c
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_5

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_1d
    const/16 v6, 0x16

    .line 250
    goto :goto_5

    .line 254
    :pswitch_1e
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_5

    .line 257
    :pswitch_1f
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_5

    .line 261
    :pswitch_20
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_5

    .line 265
    :pswitch_21
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_5

    .line 270
    :pswitch_22
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_5

    .line 274
    :pswitch_23
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_5

    .line 282
    :cond_11
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_12

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_12

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_12
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_5

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 291
    :pswitch_24
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 297
    :pswitch_25
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 305
    :pswitch_26
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_d .. :try_end_d} :catch_0

    goto/16 :goto_5

    .line 201
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catch_2
    move-exception v2

    .line 202
    .local v2, "e":Lcom/android/exchange/EasAuthenticationException;
    const/4 v7, 0x1

    :try_start_e
    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Caught authentication error"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 203
    const/4 v7, 0x2

    iput v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 217
    :try_start_f
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_13

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_13
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_16

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_6

    .line 277
    :pswitch_27
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_f
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_f .. :try_end_f} :catch_0

    .line 324
    :cond_14
    :goto_7
    :try_start_10
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_15

    const/16 v7, 0x20

    if-ne v6, v7, :cond_15

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_15
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_a
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_10 .. :try_end_10} :catch_0

    .line 338
    :goto_8
    :try_start_11
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_28
    if-nez v6, :cond_14

    .line 235
    const/16 v6, 0x20

    goto :goto_7

    .line 239
    :pswitch_29
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_7

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_2a
    const/16 v6, 0x16

    .line 250
    goto :goto_7

    .line 254
    :pswitch_2b
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_7

    .line 257
    :pswitch_2c
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_7

    .line 261
    :pswitch_2d
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_7

    .line 265
    :pswitch_2e
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_7

    .line 270
    :pswitch_2f
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_7

    .line 274
    :pswitch_30
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_7

    .line 282
    :cond_16
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_17

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_17

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_17
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_7

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_7

    .line 291
    :pswitch_31
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 297
    :pswitch_32
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 305
    :pswitch_33
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_11 .. :try_end_11} :catch_0

    goto/16 :goto_7

    .line 204
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "e":Lcom/android/exchange/EasAuthenticationException;
    :catch_3
    move-exception v2

    .line 205
    .local v2, "e":Ljava/io/IOException;
    :try_start_12
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 206
    .local v4, "message":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Caught IOException: "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    if-nez v4, :cond_18

    const-string v4, "No message"

    .end local v4    # "message":Ljava/lang/String;
    :cond_18
    aput-object v4, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 207
    const/4 v7, 0x1

    iput v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 217
    :try_start_13
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_19

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_19
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_1c

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_8

    .line 277
    :pswitch_34
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_13
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_13 .. :try_end_13} :catch_0

    .line 324
    :cond_1a
    :goto_9
    :try_start_14
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_1b

    const/16 v7, 0x20

    if-ne v6, v7, :cond_1b

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_1b
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_14} :catch_9
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_14 .. :try_end_14} :catch_0

    .line 338
    :goto_a
    :try_start_15
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_35
    if-nez v6, :cond_1a

    .line 235
    const/16 v6, 0x20

    goto :goto_9

    .line 239
    :pswitch_36
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_9

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_37
    const/16 v6, 0x16

    .line 250
    goto :goto_9

    .line 254
    :pswitch_38
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_9

    .line 257
    :pswitch_39
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_9

    .line 261
    :pswitch_3a
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_9

    .line 265
    :pswitch_3b
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_9

    .line 270
    :pswitch_3c
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_9

    .line 274
    :pswitch_3d
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_9

    .line 282
    :cond_1c
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_1d

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_1d

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_1d
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_9

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 291
    :pswitch_3e
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 297
    :pswitch_3f
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 305
    :pswitch_40
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_15 .. :try_end_15} :catch_0

    goto/16 :goto_9

    .line 209
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 210
    .local v2, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :try_start_16
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDiskIOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 211
    .restart local v4    # "message":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Caught SQLiteDisk IOException: "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    if-nez v4, :cond_1e

    const-string v4, "No message"

    .end local v4    # "message":Ljava/lang/String;
    :cond_1e
    aput-object v4, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 212
    const/16 v7, 0x9

    iput v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 217
    :try_start_17
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_1f

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_1f
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_22

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_a

    .line 277
    :pswitch_41
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_17
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_17 .. :try_end_17} :catch_0

    .line 324
    :cond_20
    :goto_b
    :try_start_18
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_21

    const/16 v7, 0x20

    if-ne v6, v7, :cond_21

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_21
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_8
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_18 .. :try_end_18} :catch_0

    .line 338
    :goto_c
    :try_start_19
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_42
    if-nez v6, :cond_20

    .line 235
    const/16 v6, 0x20

    goto :goto_b

    .line 239
    :pswitch_43
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_b

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_44
    const/16 v6, 0x16

    .line 250
    goto :goto_b

    .line 254
    :pswitch_45
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_b

    .line 257
    :pswitch_46
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_b

    .line 261
    :pswitch_47
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_b

    .line 265
    :pswitch_48
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_b

    .line 270
    :pswitch_49
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_b

    .line 274
    :pswitch_4a
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_b

    .line 282
    :cond_22
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_23

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_23

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_23
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_b

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_b

    .line 291
    :pswitch_4b
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 297
    :pswitch_4c
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 305
    :pswitch_4d
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_19
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_19 .. :try_end_19} :catch_0

    goto/16 :goto_b

    .line 214
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :catch_5
    move-exception v2

    .line 215
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1a
    const-string v7, "Uncaught exception in AccountMailboxService"

    invoke-virtual {p0, v7, v2}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 217
    :try_start_1b
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v7, :cond_24

    .line 218
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "thread="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mAccount="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mMailbox="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " cmd="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mSyncReason="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " syncKey="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exception="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " mStop="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_24
    iget-boolean v7, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v7, :cond_27

    .line 226
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Sync finished"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync finished exit status :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_c

    .line 277
    :pswitch_4e
    const/16 v6, 0x15

    .line 278
    const-string v7, "Sync ended due to an exception."

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_1b
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_1b .. :try_end_1b} :catch_0

    .line 324
    :cond_25
    :goto_d
    :try_start_1c
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v8, 0x5

    if-ge v7, v8, :cond_26

    const/16 v7, 0x20

    if-ne v6, v7, :cond_26

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_26
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v7

    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v10, 0x0

    invoke-interface {v7, v8, v9, v6, v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_7
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_1c .. :try_end_1c} :catch_0

    .line 338
    :goto_e
    :try_start_1d
    const-string v7, "sync finished"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :pswitch_4f
    if-nez v6, :cond_25

    .line 235
    const/16 v6, 0x20

    goto :goto_d

    .line 239
    :pswitch_50
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v7, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "S"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x3a

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v7, "syncStatus"

    invoke-virtual {v1, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v7, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v7}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_d

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_51
    const/16 v6, 0x16

    .line 250
    goto :goto_d

    .line 254
    :pswitch_52
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_d

    .line 257
    :pswitch_53
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_d

    .line 261
    :pswitch_54
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_d

    .line 265
    :pswitch_55
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_d

    .line 270
    :pswitch_56
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_d

    .line 274
    :pswitch_57
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_d

    .line 282
    :cond_27
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Stopped sync finished."

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_28

    iget v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v7, v12, :cond_28

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_28
    iget v7, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v7, :pswitch_data_d

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_d

    .line 291
    :pswitch_58
    const/4 v6, 0x3

    .line 292
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server block this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 297
    :pswitch_59
    const/4 v6, 0x4

    .line 298
    const-string v7, "DeviceAccessPermission"

    const-string v8, "Service is stopped as server qurantined this device"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 305
    :pswitch_5a
    const/16 v6, 0x20

    .line 306
    const-string v7, "Connection Error "

    const-string v8, "Service is stopped as Network Connectivity Failed"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 217
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    iget v8, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    if-eqz v8, :cond_29

    .line 218
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 222
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 224
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_29
    iget-boolean v8, p0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v8, :cond_2c

    .line 226
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 227
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 229
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget v8, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v8, :pswitch_data_e

    .line 277
    :pswitch_5b
    const/16 v6, 0x15

    .line 278
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V
    :try_end_1d
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_1d .. :try_end_1d} :catch_0

    .line 324
    :cond_2a
    :goto_f
    :try_start_1e
    iget v8, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_2b

    const/16 v8, 0x20

    if-ne v6, v8, :cond_2b

    .line 326
    const/4 v6, 0x0

    .line 331
    :cond_2b
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_6
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_1e .. :try_end_1e} :catch_0

    .line 338
    :goto_10
    :try_start_1f
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    throw v7

    .line 234
    :pswitch_5c
    if-nez v6, :cond_2a

    .line 235
    const/16 v6, 0x20

    goto :goto_f

    .line 239
    :pswitch_5d
    const/4 v6, 0x0

    .line 240
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 241
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 242
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasAccountService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 243
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 246
    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto :goto_f

    .line 249
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_5e
    const/16 v6, 0x16

    .line 250
    goto :goto_f

    .line 254
    :pswitch_5f
    sget-object v8, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_f

    .line 257
    :pswitch_60
    const/4 v6, 0x3

    .line 258
    goto/16 :goto_f

    .line 261
    :pswitch_61
    const/4 v6, 0x4

    .line 262
    goto/16 :goto_f

    .line 265
    :pswitch_62
    const/16 v6, 0xad

    .line 266
    goto/16 :goto_f

    .line 270
    :pswitch_63
    const/high16 v6, 0x110000

    .line 271
    goto/16 :goto_f

    .line 274
    :pswitch_64
    const/16 v6, 0x56

    .line 275
    goto/16 :goto_f

    .line 282
    :cond_2c
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 284
    sget-object v8, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 285
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_2d

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-eq v8, v12, :cond_2d

    .line 287
    iget-wide v8, p0, Lcom/android/exchange/EasAccountService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 289
    :cond_2d
    iget v8, p0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    packed-switch v8, :pswitch_data_f

    .line 313
    const/4 v6, 0x0

    goto/16 :goto_f

    .line 291
    :pswitch_65
    const/4 v6, 0x3

    .line 292
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 297
    :pswitch_66
    const/4 v6, 0x4

    .line 298
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    .line 305
    :pswitch_67
    const/16 v6, 0x20

    .line 306
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1f
    .catch Lcom/android/emailcommon/provider/ProviderUnavailableException; {:try_start_1f .. :try_end_1f} :catch_0

    goto/16 :goto_f

    .line 334
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :catch_6
    move-exception v8

    goto/16 :goto_10

    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v7

    goto/16 :goto_e

    .local v2, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :catch_8
    move-exception v7

    goto/16 :goto_c

    .local v2, "e":Ljava/io/IOException;
    :catch_9
    move-exception v7

    goto/16 :goto_a

    .local v2, "e":Lcom/android/exchange/EasAuthenticationException;
    :catch_a
    move-exception v7

    goto/16 :goto_8

    .local v2, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catch_b
    move-exception v7

    goto/16 :goto_6

    .end local v2    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catch_c
    move-exception v7

    goto/16 :goto_4

    :catch_d
    move-exception v7

    goto/16 :goto_2

    .line 232
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_9
    .end packed-switch

    .line 289
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 232
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_10
        :pswitch_d
        :pswitch_11
        :pswitch_d
        :pswitch_12
        :pswitch_13
        :pswitch_d
        :pswitch_15
        :pswitch_d
        :pswitch_14
        :pswitch_16
    .end packed-switch

    .line 289
    :pswitch_data_3
    .packed-switch 0x6
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch

    .line 232
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1b
        :pswitch_1d
        :pswitch_1a
        :pswitch_1e
        :pswitch_1a
        :pswitch_1f
        :pswitch_20
        :pswitch_1a
        :pswitch_22
        :pswitch_1a
        :pswitch_21
        :pswitch_23
    .end packed-switch

    .line 289
    :pswitch_data_5
    .packed-switch 0x6
        :pswitch_24
        :pswitch_25
        :pswitch_26
    .end packed-switch

    .line 232
    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_29
        :pswitch_28
        :pswitch_2a
        :pswitch_27
        :pswitch_2b
        :pswitch_27
        :pswitch_2c
        :pswitch_2d
        :pswitch_27
        :pswitch_2f
        :pswitch_27
        :pswitch_2e
        :pswitch_30
    .end packed-switch

    .line 289
    :pswitch_data_7
    .packed-switch 0x6
        :pswitch_31
        :pswitch_32
        :pswitch_33
    .end packed-switch

    .line 232
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_36
        :pswitch_35
        :pswitch_37
        :pswitch_34
        :pswitch_38
        :pswitch_34
        :pswitch_39
        :pswitch_3a
        :pswitch_34
        :pswitch_3c
        :pswitch_34
        :pswitch_3b
        :pswitch_3d
    .end packed-switch

    .line 289
    :pswitch_data_9
    .packed-switch 0x6
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
    .end packed-switch

    .line 232
    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_43
        :pswitch_42
        :pswitch_44
        :pswitch_41
        :pswitch_45
        :pswitch_41
        :pswitch_46
        :pswitch_47
        :pswitch_41
        :pswitch_49
        :pswitch_41
        :pswitch_48
        :pswitch_4a
    .end packed-switch

    .line 289
    :pswitch_data_b
    .packed-switch 0x6
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
    .end packed-switch

    .line 232
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_50
        :pswitch_4f
        :pswitch_51
        :pswitch_4e
        :pswitch_52
        :pswitch_4e
        :pswitch_53
        :pswitch_54
        :pswitch_4e
        :pswitch_56
        :pswitch_4e
        :pswitch_55
        :pswitch_57
    .end packed-switch

    .line 289
    :pswitch_data_d
    .packed-switch 0x6
        :pswitch_58
        :pswitch_59
        :pswitch_5a
    .end packed-switch

    .line 232
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_5d
        :pswitch_5c
        :pswitch_5e
        :pswitch_5b
        :pswitch_5f
        :pswitch_5b
        :pswitch_60
        :pswitch_61
        :pswitch_5b
        :pswitch_63
        :pswitch_5b
        :pswitch_62
        :pswitch_64
    .end packed-switch

    .line 289
    :pswitch_data_f
    .packed-switch 0x6
        :pswitch_65
        :pswitch_66
        :pswitch_67
    .end packed-switch
.end method

.method protected sendPing([BI)Lcom/android/exchange/EasResponse;
    .locals 4
    .param p1, "bytes"    # [B
    .param p2, "heartbeat"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 901
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 902
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Send ping, timeout: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "s, high: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/EasAccountService;->mPingHighWaterMark:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x73

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 903
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Ping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 909
    :goto_0
    if-eqz p1, :cond_1

    .line 910
    const-string v0, "Ping"

    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    add-int/lit8 v2, p2, 0x5

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/exchange/EasAccountService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v0

    .line 913
    :goto_1
    return-object v0

    .line 905
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Ping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 913
    :cond_1
    const-string v0, "Ping"

    const/4 v1, 0x0

    add-int/lit8 v2, p2, 0x5

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/exchange/EasAccountService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v0

    goto :goto_1
.end method

.method public sync()V
    .locals 38
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/adapter/Parser$EasParserException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 379
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 382
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 389
    :goto_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit8 v5, v5, 0x20

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v5, v5, 0x4000

    if-eqz v5, :cond_2

    .line 391
    :cond_0
    sget-object v5, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v6, "runAccountMailbox returns due to SECURITY HOLD"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const/4 v5, 0x4

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_1
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 394
    :try_start_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v8, 0x17

    const/4 v9, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 795
    :cond_1
    :goto_1
    return-void

    .line 402
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    if-nez v5, :cond_3

    .line 403
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    const-string v6, "0"

    iput-object v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 404
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Account syncKey INIT to 0"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 405
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 406
    .local v15, "cv":Landroid/content/ContentValues;
    const-string v5, "syncKey"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v15}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 410
    .end local v15    # "cv":Landroid/content/ContentValues;
    :cond_3
    const-string v5, "0"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    .line 411
    .local v21, "firstSync":Z
    if-eqz v21, :cond_4

    .line 412
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Initial FolderSync"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 416
    :cond_4
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 417
    .restart local v15    # "cv":Landroid/content/ContentValues;
    const-string v5, "syncInterval"

    const/4 v6, -0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 418
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "accountKey=? and syncInterval=-3"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v15, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_5

    .line 422
    const-string v5, "change ping boxes to push"

    invoke-static {v5}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 427
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncTime:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x5265c00

    cmp-long v5, v6, v8

    if-lez v5, :cond_6

    .line 428
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Determine EAS protocol version"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 429
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->sendHttpClientOptions()Lcom/android/exchange/EasResponse;
    :try_end_3
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v30

    .line 431
    .local v30, "resp":Lcom/android/exchange/EasResponse;
    :try_start_4
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v14

    .line 432
    .local v14, "code":I
    const-string v5, "OPTIONS response: "

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;I)V

    .line 433
    const/16 v5, 0xc8

    if-ne v14, v5, :cond_a

    .line 434
    const-string v5, "MS-ASProtocolCommands"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v22

    .line 435
    .local v22, "header":Lorg/apache/http/Header;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 436
    const-string v5, "ms-asprotocolversions"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v22

    .line 438
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasAccountService;->setupProtocolVersion(Lcom/android/exchange/EasSyncService;Lorg/apache/http/Header;)V

    .line 441
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Using version "

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mProtocolVersion:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 448
    :try_start_6
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 450
    const-string v5, "protocolVersion"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    const-string v5, "deviceBlockedType"

    sget v6, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 454
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v15}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 455
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 457
    const-string v5, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 458
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6, v15}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->update(Landroid/content/Context;Landroid/content/ContentValues;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 470
    if-eqz v30, :cond_6

    .line 471
    :try_start_7
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 476
    .end local v14    # "code":I
    .end local v22    # "header":Lorg/apache/http/Header;
    .end local v30    # "resp":Lcom/android/exchange/EasResponse;
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v6, -0x2

    if-ne v5, v6, :cond_7

    .line 478
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const/4 v7, -0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static/range {v5 .. v10}, Lcom/android/exchange/ExchangeService;->setEasSyncIntervals(Landroid/content/Context;Ljava/lang/String;IJZ)V

    .line 482
    :cond_7
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v5

    if-nez v5, :cond_1

    .line 490
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    const/16 v6, 0xa

    if-ne v5, v6, :cond_d

    .line 499
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "runAccountMailbox(): Provisioning needed. Sending Provision..."

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 500
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->tryProvision(Lcom/android/exchange/EasSyncService;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 502
    const/4 v5, 0x4

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_7
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_1

    .line 736
    .end local v15    # "cv":Landroid/content/ContentValues;
    .end local v21    # "firstSync":Z
    :catch_0
    move-exception v20

    .line 742
    .local v20, "e":Lcom/android/exchange/CommandStatusException;
    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    move/from16 v32, v0

    .line 743
    .local v32, "status":I
    invoke-static/range {v32 .. v32}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 746
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-nez v5, :cond_1

    .line 755
    const/4 v5, 0x4

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    goto/16 :goto_1

    .line 442
    .end local v20    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v32    # "status":I
    .restart local v14    # "code":I
    .restart local v15    # "cv":Landroid/content/ContentValues;
    .restart local v21    # "firstSync":Z
    .restart local v22    # "header":Lorg/apache/http/Header;
    .restart local v30    # "resp":Lcom/android/exchange/EasResponse;
    :catch_1
    move-exception v20

    .line 445
    .local v20, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_8
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5}, Ljava/io/IOException;-><init>()V

    throw v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 470
    .end local v14    # "code":I
    .end local v20    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .end local v22    # "header":Lorg/apache/http/Header;
    :catchall_0
    move-exception v5

    if-eqz v30, :cond_8

    .line 471
    :try_start_9
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    :cond_8
    throw v5
    :try_end_9
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    .line 776
    .end local v15    # "cv":Landroid/content/ContentValues;
    .end local v21    # "firstSync":Z
    .end local v30    # "resp":Lcom/android/exchange/EasResponse;
    :catch_2
    move-exception v20

    .line 781
    .local v20, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v5

    if-nez v5, :cond_9

    .line 785
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_7

    .line 792
    :cond_9
    :goto_3
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "runAccountMailbox(): Caught IO exception. Sending to run()"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 793
    throw v20

    .line 460
    .end local v20    # "e":Ljava/io/IOException;
    .restart local v14    # "code":I
    .restart local v15    # "cv":Landroid/content/ContentValues;
    .restart local v21    # "firstSync":Z
    .restart local v30    # "resp":Lcom/android/exchange/EasResponse;
    :cond_a
    :try_start_b
    invoke-static {v14}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 461
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "OPTIONS: Authentication Error - 401"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 462
    const/4 v5, 0x2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 470
    if-eqz v30, :cond_1

    .line 471
    :try_start_c
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_c
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    goto/16 :goto_1

    .line 466
    :cond_b
    :try_start_d
    const-string v5, "OPTIONS command failed; throwing IOException"

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->errorLog(Ljava/lang/String;)V

    .line 467
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5}, Ljava/io/IOException;-><init>()V

    throw v5
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 508
    .end local v14    # "code":I
    .end local v30    # "resp":Lcom/android/exchange/EasResponse;
    :cond_c
    const/4 v5, 0x2

    :try_start_e
    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    .line 509
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    .line 511
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/exchange/EasAccountService;->mEasNeedsProvisioning:Z

    .line 512
    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    const/4 v7, 0x4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v5, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z

    goto/16 :goto_1

    .line 522
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->refresh(Landroid/content/Context;)V

    .line 524
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Sending Account syncKey: "

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 525
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    .line 526
    const/4 v14, 0x0

    .line 527
    .restart local v14    # "code":I
    new-instance v31, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 528
    .local v31, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v5, 0x1d6

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x1d2

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 532
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_e

    .line 533
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "runAccountMailbox(): Wbxml:"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 534
    invoke-virtual/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v4

    .line 535
    .local v4, "b":[B
    new-instance v11, Ljava/io/ByteArrayInputStream;

    invoke-direct {v11, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 536
    .local v11, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v26, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 537
    .local v26, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_e
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2

    .line 541
    .end local v4    # "b":[B
    .end local v11    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v26    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_e
    const/16 v30, 0x0

    .line 542
    .restart local v30    # "resp":Lcom/android/exchange/EasResponse;
    const/16 v28, 0x0

    .line 546
    .local v28, "mInputStream":Ljava/io/InputStream;
    :try_start_f
    const-string v5, "FolderSync"

    invoke-virtual/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/android/exchange/EasAccountService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v30

    .line 547
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    move-result v5

    if-eqz v5, :cond_10

    .line 666
    if-eqz v30, :cond_f

    .line 667
    :try_start_10
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;
    :try_end_10
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2

    goto/16 :goto_1

    .line 550
    :cond_10
    :try_start_11
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v28

    .line 551
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v14

    .line 554
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_11

    .line 555
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "runAccountMailbox(): FolderSync command http response code:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 560
    :cond_11
    const/16 v5, 0xc8

    if-ne v14, v5, :cond_14

    .line 564
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-static {v5}, Lcom/android/exchange/ExchangeService;->releaseSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 566
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v25

    .line 567
    .local v25, "len":I
    if-eqz v25, :cond_14

    .line 568
    move-object/from16 v23, v28

    .line 570
    .local v23, "is":Ljava/io/InputStream;
    new-instance v5, Lcom/android/exchange/adapter/FolderSyncParser;

    new-instance v6, Lcom/android/exchange/adapter/AccountSyncAdapter;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/android/exchange/adapter/AccountSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v23

    invoke-direct {v5, v0, v6}, Lcom/android/exchange/adapter/FolderSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    invoke-virtual {v5}, Lcom/android/exchange/adapter/FolderSyncParser;->parse()Z

    move-result v33

    .line 573
    .local v33, "syncAgain":Z
    if-eqz v21, :cond_12

    .line 574
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    invoke-static/range {v5 .. v10}, Lcom/android/exchange/ExchangeService;->setEasSyncIntervals(Landroid/content/Context;Ljava/lang/String;IJZ)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 577
    :cond_12
    if-eqz v33, :cond_14

    .line 666
    if-eqz v30, :cond_13

    .line 667
    :try_start_12
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_13
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;
    :try_end_12
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_12 .. :try_end_12} :catch_0
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_2

    goto/16 :goto_2

    .line 586
    .end local v23    # "is":Ljava/io/InputStream;
    .end local v25    # "len":I
    .end local v33    # "syncAgain":Z
    :cond_14
    :try_start_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->isStopped()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 587
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "WARNING!!! Thread execution stopped. Exit runAccountMailbox!"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    .line 666
    if-eqz v30, :cond_15

    .line 667
    :try_start_14
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;
    :try_end_14
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_14 .. :try_end_14} :catch_0
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_2

    goto/16 :goto_1

    .line 592
    :cond_16
    :try_start_15
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/EasAccountService;->isProvisionError(I)Z

    move-result v5

    if-nez v5, :cond_17

    .line 599
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDeviceInfoSent()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_19

    const/16 v18, 0x1

    .line 601
    .local v18, "deviceInfoSent":Z
    :goto_4
    if-nez v18, :cond_17

    .line 602
    const-string v5, "EasSyncService"

    const-string v6, "Device info not yet sent"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 604
    :try_start_16
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpl-double v5, v6, v8

    if-ltz v5, :cond_17

    .line 605
    new-instance v17, Lcom/android/exchange/DeviceInformation;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    move-object/from16 v0, v17

    invoke-direct {v0, v6, v7}, Lcom/android/exchange/DeviceInformation;-><init>(D)V
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 608
    .local v17, "deviceInfo":Lcom/android/exchange/DeviceInformation;
    :try_start_17
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/exchange/EasAccountService;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6, v7}, Lcom/android/exchange/DeviceInformation;->prepareDeviceInformation(Landroid/content/Context;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 610
    sget-object v5, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v6, "INIT_ERROR Device Information"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_17 .. :try_end_17} :catch_4
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 641
    .end local v17    # "deviceInfo":Lcom/android/exchange/DeviceInformation;
    .end local v18    # "deviceInfoSent":Z
    :cond_17
    :goto_5
    :try_start_18
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/EasAccountService;->isProvisionError(I)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 645
    const/16 v5, 0xa

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mSyncReason:I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .line 666
    if-eqz v30, :cond_18

    .line 667
    :try_start_19
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;
    :try_end_19
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_19 .. :try_end_19} :catch_0
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_2

    goto/16 :goto_2

    .line 599
    :cond_19
    const/16 v18, 0x0

    goto :goto_4

    .line 612
    .restart local v17    # "deviceInfo":Lcom/android/exchange/DeviceInformation;
    .restart local v18    # "deviceInfoSent":Z
    :cond_1a
    :try_start_1a
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/DeviceInformation;->buildCommand()Lcom/android/exchange/adapter/Serializer;

    move-result-object v19

    .line 613
    .local v19, "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    if-eqz v19, :cond_1d

    .line 614
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/EasAccountService;->sendDeviceInformation(Landroid/content/Context;Lcom/android/exchange/adapter/Serializer;)I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_17

    .line 616
    const-string v5, "EasSyncService"

    const-string v6, "Device Info sent, set to 1 in db"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->setDeviceInfoSent(I)V

    .line 619
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 620
    .local v16, "cvx":Landroid/content/ContentValues;
    const-string v5, "deviceInfoSent"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDeviceInfoSent()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 622
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v6, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-virtual {v5, v6, v0}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1a .. :try_end_1a} :catch_4
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    goto :goto_5

    .line 628
    .end local v16    # "cvx":Landroid/content/ContentValues;
    .end local v19    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :catch_3
    move-exception v20

    .line 629
    .local v20, "e":Ljava/lang/Exception;
    :try_start_1b
    sget-object v5, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v6, "INIT_ERROR Device Information"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1b .. :try_end_1b} :catch_4
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto/16 :goto_5

    .line 633
    .end local v17    # "deviceInfo":Lcom/android/exchange/DeviceInformation;
    .end local v20    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v20

    .line 634
    .local v20, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    goto/16 :goto_5

    .line 666
    .end local v18    # "deviceInfoSent":Z
    .end local v20    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_1
    move-exception v5

    if-eqz v30, :cond_1b

    .line 667
    :try_start_1d
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_1b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v6, :cond_1c

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    :cond_1c
    throw v5
    :try_end_1d
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_1d .. :try_end_1d} :catch_0
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_2

    .line 625
    .restart local v17    # "deviceInfo":Lcom/android/exchange/DeviceInformation;
    .restart local v18    # "deviceInfoSent":Z
    .restart local v19    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :cond_1d
    :try_start_1e
    sget-object v5, Lcom/android/exchange/EasAccountService;->TAG:Ljava/lang/String;

    const-string v6, "deviceSerializer is null"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1e .. :try_end_1e} :catch_4
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    goto/16 :goto_5

    .line 648
    .end local v17    # "deviceInfo":Lcom/android/exchange/DeviceInformation;
    .end local v18    # "deviceInfoSent":Z
    .end local v19    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :cond_1e
    :try_start_1f
    invoke-static {v14}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v5

    if-eqz v5, :cond_20

    .line 649
    const/4 v5, 0x2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    .line 651
    :try_start_20
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    const/4 v9, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_20} :catch_9
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    .line 666
    :goto_6
    if-eqz v30, :cond_1f

    .line 667
    :try_start_21
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_1f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;
    :try_end_21
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_21 .. :try_end_21} :catch_0
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_2

    goto/16 :goto_1

    .line 657
    :cond_20
    :try_start_22
    const-string v5, "FolderSync response error: "

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v14}, Lcom/android/exchange/EasAccountService;->userLog(Ljava/lang/String;I)V

    .line 659
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "thread="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->getThreadId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mAccount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mMailbox="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " cmd="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mSyncReason="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasAccountService;->mSyncReason:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " syncKey="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " exception="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mExceptionString:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mStop="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasAccountService;->mStop:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 663
    .local v27, "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    .line 666
    if-eqz v30, :cond_21

    .line 667
    :try_start_23
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/EasResponse;->close()V

    .line 670
    :cond_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    if-eqz v5, :cond_22

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasAccountService;->mASCmd:Ljava/lang/String;

    .line 674
    :cond_22
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 675
    const-string v5, "syncInterval"

    const/4 v6, -0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 676
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "accountKey=? and syncInterval=-4"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v15, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_23

    .line 680
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Set push/hold boxes to push..."

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_23
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_23 .. :try_end_23} :catch_0
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_2

    .line 684
    :cond_23
    :try_start_24
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    const/4 v9, 0x0

    invoke-interface {v5, v6, v7, v8, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_24
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_24} :catch_8
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_24 .. :try_end_24} :catch_0
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_2

    .line 692
    :goto_7
    :try_start_25
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/exchange/EasAccountService;->mStop:Z

    if-eqz v5, :cond_24

    .line 693
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "WARNING!!! Thread execution stopped. Exit runAccountMailbox!"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 700
    :cond_24
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSecuritySyncKey:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 701
    .local v24, "key":Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_25

    .line 706
    new-instance v29, Lcom/android/emailcommon/service/PolicySet;

    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v0, v29

    invoke-direct {v0, v5, v6}, Lcom/android/emailcommon/service/PolicySet;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 707
    .local v29, "ps":Lcom/android/emailcommon/service/PolicySet;
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-static {v5, v0}, Lcom/android/exchange/SecurityPolicyDelegate;->isActive(Landroid/content/Context;Lcom/android/emailcommon/service/PolicySet;)Z

    move-result v5

    if-nez v5, :cond_25

    .line 708
    invoke-virtual {v15}, Landroid/content/ContentValues;->clear()V

    .line 709
    const-string v5, "securityFlags"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v15, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 710
    const-string v5, "securitySyncKey"

    invoke-virtual {v15, v5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 711
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    .line 712
    .local v12, "accountId":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasAccountService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v15, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 715
    sget-object v5, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-static {v5, v12, v13}, Lcom/android/exchange/SecurityPolicyDelegate;->policiesRequired(Landroid/content/Context;J)V

    .line 720
    .end local v12    # "accountId":J
    .end local v29    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_25
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;
    :try_end_25
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_25 .. :try_end_25} :catch_0
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_25} :catch_2

    move-result-object v34

    .line 722
    .local v34, "threadName":Ljava/lang/String;
    :try_start_26
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasAccountService;->runPingLoop()V
    :try_end_26
    .catch Lcom/android/exchange/StaleFolderListException; {:try_start_26 .. :try_end_26} :catch_5
    .catch Lcom/android/exchange/IllegalHeartbeatException; {:try_start_26 .. :try_end_26} :catch_6
    .catchall {:try_start_26 .. :try_end_26} :catchall_2

    .line 731
    :try_start_27
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V
    :try_end_27
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_27 .. :try_end_27} :catch_0
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_27} :catch_2

    goto/16 :goto_2

    .line 723
    :catch_5
    move-exception v20

    .line 725
    .local v20, "e":Lcom/android/exchange/StaleFolderListException;
    const/4 v5, 0x1

    :try_start_28
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "Ping interrupted; folder list requires sync..."

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_2

    .line 731
    :try_start_29
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V
    :try_end_29
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_29 .. :try_end_29} :catch_0
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_29} :catch_2

    goto/16 :goto_2

    .line 726
    .end local v20    # "e":Lcom/android/exchange/StaleFolderListException;
    :catch_6
    move-exception v20

    .line 729
    .local v20, "e":Lcom/android/exchange/IllegalHeartbeatException;
    :try_start_2a
    move-object/from16 v0, v20

    iget v5, v0, Lcom/android/exchange/IllegalHeartbeatException;->mLegalHeartbeat:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->resetHeartbeats(I)V
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_2

    .line 731
    :try_start_2b
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    goto/16 :goto_2

    .end local v20    # "e":Lcom/android/exchange/IllegalHeartbeatException;
    :catchall_2
    move-exception v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v5
    :try_end_2b
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2b .. :try_end_2b} :catch_0
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_2b} :catch_2

    .line 761
    .end local v14    # "code":I
    .end local v15    # "cv":Landroid/content/ContentValues;
    .end local v21    # "firstSync":Z
    .end local v24    # "key":Ljava/lang/String;
    .end local v27    # "loggingPhrase":Ljava/lang/String;
    .end local v28    # "mInputStream":Ljava/io/InputStream;
    .end local v30    # "resp":Lcom/android/exchange/EasResponse;
    .end local v31    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v34    # "threadName":Ljava/lang/String;
    .local v20, "e":Lcom/android/exchange/CommandStatusException;
    .restart local v32    # "status":I
    :cond_26
    invoke-static/range {v32 .. v32}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTooManyPartnerships(I)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 762
    const/16 v5, 0xb

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    goto/16 :goto_1

    .line 764
    :cond_27
    invoke-static/range {v32 .. v32}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isServerError(I)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 765
    const/16 v5, 0xc

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    goto/16 :goto_1

    .line 766
    :cond_28
    invoke-static/range {v32 .. v32}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 767
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    goto/16 :goto_1

    .line 770
    :cond_29
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unexpected status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static/range {v32 .. v32}, Lcom/android/exchange/CommandStatusException$CommandStatus;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasAccountService;->userLog([Ljava/lang/String;)V

    .line 771
    const/4 v5, 0x3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/EasAccountService;->mExitStatus:I

    goto/16 :goto_1

    .line 788
    .end local v32    # "status":I
    .local v20, "e":Ljava/io/IOException;
    :catch_7
    move-exception v5

    goto/16 :goto_3

    .line 685
    .end local v20    # "e":Ljava/io/IOException;
    .restart local v14    # "code":I
    .restart local v15    # "cv":Landroid/content/ContentValues;
    .restart local v21    # "firstSync":Z
    .restart local v27    # "loggingPhrase":Ljava/lang/String;
    .restart local v28    # "mInputStream":Ljava/io/InputStream;
    .restart local v30    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v31    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_8
    move-exception v5

    goto/16 :goto_7

    .line 652
    .end local v27    # "loggingPhrase":Ljava/lang/String;
    :catch_9
    move-exception v5

    goto/16 :goto_6

    .line 396
    .end local v14    # "code":I
    .end local v15    # "cv":Landroid/content/ContentValues;
    .end local v21    # "firstSync":Z
    .end local v28    # "mInputStream":Ljava/io/InputStream;
    .end local v30    # "resp":Lcom/android/exchange/EasResponse;
    .end local v31    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_a
    move-exception v5

    goto/16 :goto_1

    .line 384
    :catch_b
    move-exception v5

    goto/16 :goto_0
.end method

.method syncHBIFolderDetail(Lcom/android/exchange/adapter/Serializer;J)V
    .locals 10
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "mailboxId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1455
    const/4 v6, 0x0

    .line 1456
    .local v6, "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/EasAccountService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1459
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-static {v7, p2, p3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 1460
    .local v3, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v3, :cond_1

    .line 1488
    :cond_0
    :goto_0
    return-void

    .line 1463
    :cond_1
    new-instance v4, Lcom/android/exchange/EasSyncService;

    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    invoke-direct {v4, v7, v3}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 1464
    .local v4, "service":Lcom/android/exchange/EasSyncService;
    if-eqz v4, :cond_3

    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->loadServiceData()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1465
    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-eqz v7, :cond_2

    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_4

    .line 1466
    :cond_2
    new-instance v6, Lcom/android/exchange/adapter/EmailSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 1482
    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_3
    :goto_1
    if-eqz v6, :cond_0

    .line 1483
    invoke-virtual {v6}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v1

    .line 1484
    .local v1, "className":Ljava/lang/String;
    invoke-virtual {v6}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v5

    .line 1485
    .local v5, "syncKey":Ljava/lang/String;
    const-string v7, "0"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1486
    .local v2, "initialSync":Z
    iget-object v7, p0, Lcom/android/exchange/EasAccountService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v6, v7, p1, v2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V

    goto :goto_0

    .line 1467
    .end local v1    # "className":Ljava/lang/String;
    .end local v2    # "initialSync":Z
    .end local v5    # "syncKey":Ljava/lang/String;
    :cond_4
    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x42

    if-eq v7, v8, :cond_5

    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x53

    if-ne v7, v8, :cond_6

    .line 1469
    :cond_5
    new-instance v6, Lcom/android/exchange/adapter/ContactsSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/ContactsSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_1

    .line 1470
    :cond_6
    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x41

    if-eq v7, v8, :cond_7

    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x52

    if-ne v7, v8, :cond_8

    .line 1472
    :cond_7
    new-instance v6, Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_1

    .line 1473
    :cond_8
    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x43

    if-ne v7, v8, :cond_9

    .line 1474
    new-instance v6, Lcom/android/exchange/adapter/TasksSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_1

    .line 1475
    :cond_9
    iget-object v7, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x61

    if-ne v7, v8, :cond_a

    .line 1476
    new-instance v6, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_1

    .line 1477
    :cond_a
    sget-object v7, Lcom/android/exchange/EasAccountService;->mContext:Landroid/content/Context;

    iget-object v8, v4, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    invoke-static {v7, v8}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->isPrivateSyncOptionTypeByType(Landroid/content/Context;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1478
    new-instance v6, Lcom/android/exchange/adapter/EmailSyncAdapter;

    .end local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    invoke-direct {v6, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v6    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_1
.end method
