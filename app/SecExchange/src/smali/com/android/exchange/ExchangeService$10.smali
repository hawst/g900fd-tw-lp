.class Lcom/android/exchange/ExchangeService$10;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;)V
    .locals 0

    .prologue
    .line 5068
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 5072
    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5083
    :try_start_1
    # getter for: Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3200()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    if-nez v2, :cond_2

    .line 5085
    :try_start_2
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/utility/Utility;->isNonPhone(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5087
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 5088
    .local v0, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 5089
    # setter for: Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$3202(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5104
    .end local v0    # "deviceId":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_3
    # getter for: Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3200()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    .line 5105
    const-string v2, "!!! deviceId unknown; stopping self and retrying"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 5106
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService;->stopSelf()V

    .line 5108
    new-instance v2, Lcom/android/exchange/ExchangeService$10$1;

    invoke-direct {v2, p0}, Lcom/android/exchange/ExchangeService$10$1;-><init>(Lcom/android/exchange/ExchangeService$10;)V

    invoke-static {v2}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 5125
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5162
    # setter for: Lcom/android/exchange/ExchangeService;->sStartingUp:Z
    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->access$2902(Z)Z

    .line 5164
    :goto_1
    return-void

    .line 5092
    :cond_1
    :try_start_4
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "phone"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 5094
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5095
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 5096
    .restart local v0    # "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 5097
    # setter for: Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$3202(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 5101
    .end local v0    # "deviceId":Ljava/lang/String;
    .end local v1    # "tm":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v2

    goto :goto_0

    .line 5151
    :cond_2
    :try_start_5
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService;->maybeStartExchangeServiceThread()V

    .line 5152
    # getter for: Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3300()Ljava/lang/Thread;

    move-result-object v2

    if-nez v2, :cond_4

    .line 5153
    const-string v2, "!!! EAS ExchangeService, stopping self"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 5154
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService;->stopSelf()V

    .line 5160
    :cond_3
    :goto_2
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 5162
    # setter for: Lcom/android/exchange/ExchangeService;->sStartingUp:Z
    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->access$2902(Z)Z

    goto :goto_1

    .line 5155
    :cond_4
    :try_start_6
    # getter for: Lcom/android/exchange/ExchangeService;->sStop:Z
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3100()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5158
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x1388

    # invokes: Lcom/android/exchange/ExchangeService;->setAlarm(JJ)V
    invoke-static {v2, v4, v5, v6, v7}, Lcom/android/exchange/ExchangeService;->access$3400(Lcom/android/exchange/ExchangeService;JJ)V

    goto :goto_2

    .line 5160
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 5162
    :catchall_1
    move-exception v2

    # setter for: Lcom/android/exchange/ExchangeService;->sStartingUp:Z
    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->access$2902(Z)Z

    throw v2
.end method
