.class Lcom/android/exchange/ExchangeService$8;
.super Landroid/telephony/PhoneStateListener;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;)V
    .locals 0

    .prologue
    .line 4997
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 3
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 4999
    const-string v1, "onServiceStateChanged"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5000
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v0

    .line 5002
    .local v0, "oldRoamingType":Z
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    # setter for: Lcom/android/exchange/ExchangeService;->mNetworkState:I
    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->access$2802(Lcom/android/exchange/ExchangeService;I)I

    .line 5003
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-nez v1, :cond_0

    .line 5004
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v2

    # setter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->access$2602(Lcom/android/exchange/ExchangeService;Z)Z

    .line 5019
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 5021
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Roaming State changed: mIsNetworkRoaming:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5025
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$8;->this$0:Lcom/android/exchange/ExchangeService;

    # invokes: Lcom/android/exchange/ExchangeService;->updateAccountDb()V
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$2400(Lcom/android/exchange/ExchangeService;)V

    .line 5028
    :cond_0
    return-void
.end method
