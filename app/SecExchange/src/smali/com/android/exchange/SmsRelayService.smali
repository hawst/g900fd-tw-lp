.class public Lcom/android/exchange/SmsRelayService;
.super Landroid/app/Service;
.source "SmsRelayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/SmsRelayService$1;,
        Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;,
        Lcom/android/exchange/SmsRelayService$SmsPayload;
    }
.end annotation


# instance fields
.field private mRelayThread:Ljava/lang/Thread;

.field private mSmsQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/SmsRelayService$SmsPayload;",
            ">;"
        }
    .end annotation
.end field

.field private mStop:Z

.field private mSyncObject:Ljava/lang/Object;

.field private mTm:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSyncObject:Ljava/lang/Object;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    .line 37
    iput-object v1, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/SmsRelayService;->mStop:Z

    .line 41
    iput-object v1, p0, Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;

    .line 124
    return-void
.end method

.method static synthetic access$100(Lcom/android/exchange/SmsRelayService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSyncObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/exchange/SmsRelayService;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/exchange/SmsRelayService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/exchange/SmsRelayService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/android/exchange/SmsRelayService;->mStop:Z

    return v0
.end method

.method static synthetic access$500(Lcom/android/exchange/SmsRelayService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/android/exchange/SmsRelayService;->getMobileAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/exchange/SmsRelayService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/SmsRelayService;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/exchange/SmsRelayService;->onDone()V

    return-void
.end method

.method private getMobileAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" [MOBILE:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private onDone()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    .line 118
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 113
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 55
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/android/exchange/SmsRelayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;

    .line 56
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/SmsRelayService;->mStop:Z

    .line 104
    iput-object v1, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    .line 105
    iget-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 108
    :cond_0
    iput-object v1, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    .line 109
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Service;->onLowMemory()V

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/SmsRelayService;->mStop:Z

    .line 98
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v2

    .line 62
    .local v2, "result":I
    const-string v3, "SmsRelayService"

    const-string v4, "Received SMS"

    invoke-static {v3, v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    if-nez p1, :cond_1

    .line 66
    const-string v3, "SmsRelayService"

    const-string v4, "SmsRelayService.onStartCommand: intent is null"

    invoke-static {v3, v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    :goto_0
    return v2

    .line 71
    :cond_1
    const-string v3, "smsRelay"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    iget-object v4, p0, Lcom/android/exchange/SmsRelayService;->mSyncObject:Ljava/lang/Object;

    monitor-enter v4

    .line 77
    :try_start_0
    invoke-static {p1}, Landroid/provider/Telephony$Sms$Intents;->getMessagesFromIntent(Landroid/content/Intent;)[Landroid/telephony/SmsMessage;

    move-result-object v1

    .line 78
    .local v1, "messages":[Landroid/telephony/SmsMessage;
    iget-object v3, p0, Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;

    new-instance v5, Lcom/android/exchange/SmsRelayService$SmsPayload;

    invoke-direct {v5, p0, v1}, Lcom/android/exchange/SmsRelayService$SmsPayload;-><init>(Lcom/android/exchange/SmsRelayService;[Landroid/telephony/SmsMessage;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    const-string v3, "SmsRelayService"

    const-string v5, "SMS Added to relay que"

    invoke-static {v3, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    .end local v1    # "messages":[Landroid/telephony/SmsMessage;
    :goto_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    iget-object v3, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    if-nez v3, :cond_0

    .line 87
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;-><init>(Lcom/android/exchange/SmsRelayService;Lcom/android/exchange/SmsRelayService$1;)V

    const-string v5, "smsRelay"

    invoke-direct {v3, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    .line 88
    iget-object v3, p0, Lcom/android/exchange/SmsRelayService;->mRelayThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 84
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method
