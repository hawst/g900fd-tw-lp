.class public Lcom/android/exchange/AutoDiscoverHandler;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;,
        Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;,
        Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;,
        Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT:I

.field private static final TIMEOUT:I

.field private static mOriginalUserName:Ljava/lang/String;

.field private static final sLocks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/exchange/AutoDiscoverHandler;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final autodiscoverThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIsAutoDiscoverCancelledByUser:Z

.field private mIsCancelled:Z

.field private mProtocolVersion:Ljava/lang/String;

.field private mRedirectCount:I

.field private final mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

.field private mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/android/exchange/AutoDiscoverHandler;->TIMEOUT:I

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    .line 119
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_1
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/android/exchange/AutoDiscoverHandler;->CONNECTION_TIMEOUT:I

    .line 125
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/AutoDiscoverHandler;->mOriginalUserName:Ljava/lang/String;

    return-void

    .line 99
    :cond_0
    const/16 v0, 0x50

    goto :goto_0

    .line 119
    :cond_1
    const/16 v0, 0x28

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "svc"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput v1, p0, Lcom/android/exchange/AutoDiscoverHandler;->mRedirectCount:I

    .line 108
    new-instance v0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {v0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .line 111
    const-string v0, "2.5"

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mProtocolVersion:Ljava/lang/String;

    .line 113
    iput-boolean v1, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z

    .line 116
    iput-boolean v1, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsAutoDiscoverCancelledByUser:Z

    .line 123
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    .line 697
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler;->mContext:Landroid/content/Context;

    .line 698
    return-void
.end method

.method static synthetic access$1000(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1200()I
    .locals 1

    .prologue
    .line 94
    sget v0, Lcom/android/exchange/AutoDiscoverHandler;->TIMEOUT:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/exchange/AutoDiscoverHandler;IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/AutoDiscoverHandler;->getHttpClient(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/exchange/AutoDiscoverHandler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mProtocolVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/exchange/AutoDiscoverHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsAutoDiscoverCancelledByUser:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/android/exchange/AutoDiscoverHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/exchange/AutoDiscoverHandler;Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;
    .param p1, "x1"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/android/exchange/AutoDiscoverHandler;->getRedirect(Lorg/apache/http/HttpResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900([Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # [Ljava/lang/String;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lcom/android/exchange/AutoDiscoverHandler;->mOriginalUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler;->getAuthToken(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/exchange/AutoDiscoverHandler;)Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/exchange/AutoDiscoverHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V

    return-void
.end method

.method static synthetic access$900(Lcom/android/exchange/AutoDiscoverHandler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/exchange/AutoDiscoverHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z

    return p1
.end method

.method private static clearLock(Ljava/lang/String;)V
    .locals 4
    .param p0, "smtpAddress"    # Ljava/lang/String;

    .prologue
    .line 754
    sget-object v1, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    monitor-enter v1

    .line 755
    :try_start_0
    sget-object v0, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    const-string v0, "AutoDiscoverHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "successfully cleared lock for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    sget-object v0, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 759
    :cond_0
    monitor-exit v1

    .line 760
    return-void

    .line 759
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static getAuthToken(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    .locals 5
    .param p0, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .prologue
    const/16 v4, 0x3a

    .line 702
    const/4 v1, 0x0

    .line 704
    .local v1, "cs":Ljava/lang/String;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 706
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 715
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Basic "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 719
    .local v0, "authToken":Ljava/lang/String;
    return-object v0

    .line 711
    .end local v0    # "authToken":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;
    invoke-static {p0}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getHttpClient(IZI)Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 7
    .param p1, "timeout"    # I
    .param p2, "withSSL"    # Z
    .param p3, "port"    # I

    .prologue
    .line 1222
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 1224
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/AutoDiscoverHandler;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1226
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 1228
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 1232
    sget v4, Lcom/android/exchange/AutoDiscoverHandler;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 1240
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1242
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 1244
    .local v2, "proxyHost":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/exchange/AutoDiscoverHandler;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 1246
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 1248
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 1251
    const-string v4, "PROXY"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-static {p2, p3}, Lcom/android/exchange/ExchangeService;->getClientConnectionManagerForSSLValidation(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 1268
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    return-object v0
.end method

.method public static getLock(Ljava/lang/String;)Lcom/android/exchange/AutoDiscoverHandler;
    .locals 2
    .param p0, "smtpAddress"    # Ljava/lang/String;

    .prologue
    .line 744
    sget-object v1, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    monitor-enter v1

    .line 745
    :try_start_0
    sget-object v0, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/AutoDiscoverHandler;

    monitor-exit v1

    return-object v0

    .line 746
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static getOrCreateLock(Ljava/lang/String;Landroid/content/Context;)Lcom/android/exchange/AutoDiscoverHandler;
    .locals 7
    .param p0, "smtpAddress"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 731
    const/4 v1, 0x0

    .line 732
    .local v1, "lock":Lcom/android/exchange/AutoDiscoverHandler;
    sget-object v4, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    monitor-enter v4

    .line 733
    :try_start_0
    sget-object v3, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/exchange/AutoDiscoverHandler;

    move-object v1, v0

    .line 734
    if-nez v1, :cond_0

    .line 735
    const-string v3, "AutoDiscoverHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "creating autodiscover instance lock"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    new-instance v2, Lcom/android/exchange/AutoDiscoverHandler;

    invoke-direct {v2, p1}, Lcom/android/exchange/AutoDiscoverHandler;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 737
    .end local v1    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    .local v2, "lock":Lcom/android/exchange/AutoDiscoverHandler;
    :try_start_1
    sget-object v3, Lcom/android/exchange/AutoDiscoverHandler;->sLocks:Ljava/util/HashMap;

    invoke-virtual {v3, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v1, v2

    .line 739
    .end local v2    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    .restart local v1    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    :cond_0
    :try_start_2
    monitor-exit v4

    .line 740
    return-object v1

    .line 739
    :catchall_0
    move-exception v3

    :goto_0
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .end local v1    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    .restart local v2    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    .restart local v1    # "lock":Lcom/android/exchange/AutoDiscoverHandler;
    goto :goto_0
.end method

.method private getRedirect(Lorg/apache/http/HttpResponse;)Ljava/lang/String;
    .locals 3
    .param p1, "resp"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1156
    const-string v2, "X-MS-Location"

    invoke-interface {p1, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 1157
    .local v0, "header":Lorg/apache/http/Header;
    const/4 v1, 0x0

    .line 1158
    .local v1, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1159
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1161
    :cond_0
    return-object v1
.end method

.method private getSubDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1199
    const/4 v2, 0x0

    .line 1200
    .local v2, "newDomain":Ljava/lang/String;
    const-string v4, "\\."

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1201
    .local v1, "arg":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v4, v1

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    :cond_0
    move-object v3, v2

    .line 1209
    .end local v2    # "newDomain":Ljava/lang/String;
    .local v3, "newDomain":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 1204
    .end local v3    # "newDomain":Ljava/lang/String;
    .restart local v2    # "newDomain":Ljava/lang/String;
    :cond_1
    const/16 v4, 0x2e

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1205
    .local v0, "amp":I
    if-lez v0, :cond_2

    .line 1206
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 1207
    .end local v2    # "newDomain":Ljava/lang/String;
    .restart local v3    # "newDomain":Ljava/lang/String;
    goto :goto_0

    .end local v3    # "newDomain":Ljava/lang/String;
    .restart local v2    # "newDomain":Ljava/lang/String;
    :cond_2
    move-object v3, v2

    .line 1209
    .end local v2    # "newDomain":Ljava/lang/String;
    .restart local v3    # "newDomain":Ljava/lang/String;
    goto :goto_0
.end method

.method private interruptAllThreads()V
    .locals 6

    .prologue
    .line 1077
    iget-object v3, p0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1079
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .line 1080
    .local v1, "thread":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2500(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2500(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->isAborted()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1081
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpPost:Lorg/apache/http/client/methods/HttpPost;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2500(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 1082
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "httpPost aborted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    :cond_0
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2600(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v2

    if-eqz v2, :cond_1

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2600(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpGet;->isAborted()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1085
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->mHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->access$2600(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpGet;->abort()V

    .line 1086
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "httpGet aborted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    :cond_1
    invoke-virtual {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->interrupt()V

    .line 1089
    const-string v2, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Interrupted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1092
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "thread":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    :catch_0
    move-exception v2

    .line 1094
    :cond_2
    :try_start_1
    monitor-exit v3

    .line 1095
    return-void

    .line 1094
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public static isAutoDiscoverNeeded(Lcom/android/exchange/EasResponse;)Z
    .locals 4
    .param p0, "response"    # Lcom/android/exchange/EasResponse;

    .prologue
    .line 769
    const/4 v2, 0x0

    .line 770
    .local v2, "tryAutodiscover":Z
    invoke-virtual {p0}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 771
    .local v0, "code":I
    const/16 v3, 0x12e

    if-ne v0, v3, :cond_1

    .line 772
    const/4 v2, 0x1

    .line 782
    :cond_0
    :goto_0
    return v2

    .line 773
    :cond_1
    const/16 v3, 0x1c3

    if-ne v0, v3, :cond_3

    .line 774
    const/4 v1, 0x0

    .line 775
    .local v1, "commands":Lorg/apache/http/Header;
    const-string v3, "X-MS-Location"

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 776
    if-eqz v1, :cond_2

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 777
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 779
    .end local v1    # "commands":Lorg/apache/http/Header;
    :cond_3
    const/16 v3, 0x193

    if-ne v0, v3, :cond_0

    .line 780
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static saveAutodiscoveredInfo(JLandroid/os/Bundle;Landroid/content/Context;)V
    .locals 8
    .param p0, "accId"    # J
    .param p2, "bundle"    # Landroid/os/Bundle;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 1041
    invoke-static {p3, p0, p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1042
    .local v0, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_0

    .line 1043
    const-string v3, "autodiscover_host_auth"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 1045
    .local v2, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v2, :cond_0

    .line 1046
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1047
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "password"

    iget-object v4, v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/crypto/AESEncryptionUtil;->AESEncryption(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const-string v3, "protocol"

    iget-object v4, v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    const-string v3, "flags"

    iget v4, v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1051
    const-string v3, "login"

    iget-object v4, v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    const-string v3, "address"

    iget-object v4, v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->CONTENT_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "accountKey="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1055
    const-string v3, "AutoDiscoverHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updated autodiscover result in db for account "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v2    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :cond_0
    return-void
.end method

.method private shutdownConnectionManager()V
    .locals 6

    .prologue
    .line 1277
    sget-object v4, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    monitor-enter v4

    .line 1278
    :try_start_0
    sget-object v3, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1280
    const-string v3, "AutoDiscoverHandler"

    const-string v5, "Shutting down sAutoDiscClientConnectionManagers"

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1283
    :try_start_1
    sget-object v3, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1285
    .local v2, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-eqz v2, :cond_0

    .line 1287
    :try_start_2
    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1288
    const/4 v2, 0x0

    goto :goto_0

    .line 1289
    :catch_0
    move-exception v0

    .line 1290
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1294
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :catch_1
    move-exception v0

    .line 1295
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1297
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v3, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1299
    :cond_2
    monitor-exit v4

    .line 1301
    return-void

    .line 1299
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3
.end method

.method private startAutodiscoveryThread(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)V
    .locals 22
    .param p1, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p2, "mAutoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p3, "_serverdomain"    # Ljava/lang/String;

    .prologue
    .line 956
    const-string v5, "AutoDiscoverHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " Starting threads for autodiscover for user  "

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v19, " , autodiscoverInfo  "

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    const/4 v6, 0x0

    .line 961
    .local v6, "serverdomain":Ljava/lang/String;
    if-eqz p3, :cond_4

    .line 962
    move-object/from16 v6, p3

    .line 971
    :goto_0
    const-string v5, "AutoDiscoverHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " autodiscover server domain : "

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    const-string v10, "/autodiscover/autodiscover.xml"

    .line 975
    .local v10, "AUTO_DISCOVER_PAGE":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/exchange/AutoDiscoverHandler;->getSubDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 977
    .local v13, "subDomain":Ljava/lang/String;
    const/4 v14, 0x0

    .line 978
    .local v14, "t1":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    const/4 v15, 0x0

    .line 979
    .local v15, "t2":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    const/16 v16, 0x0

    .line 980
    .local v16, "t3":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    const/16 v17, 0x0

    .line 981
    .local v17, "t4":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    const/4 v4, 0x0

    .line 983
    .local v4, "t5":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    invoke-static/range {p1 .. p1}, Lcom/android/exchange/AutoDiscoverHandler;->getAuthToken(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v8

    .line 985
    .local v8, "authToken":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "https://autodiscover."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/autodiscover/autodiscover.xml"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 988
    .local v18, "url":Ljava/lang/String;
    new-instance v14, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .end local v14    # "t1":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v14, v0, v1, v2, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;-><init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)V

    .line 989
    .restart local v14    # "t1":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    invoke-virtual {v5, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 990
    invoke-virtual {v14}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->start()V

    .line 992
    if-eqz v13, :cond_0

    .line 993
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "https://autodiscover."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/autodiscover/autodiscover.xml"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 994
    new-instance v15, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .end local v15    # "t2":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p1

    invoke-direct {v15, v0, v1, v2, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;-><init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)V

    .line 995
    .restart local v15    # "t2":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    invoke-virtual {v15}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->start()V

    .line 999
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "https://"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/autodiscover/autodiscover.xml"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1000
    new-instance v16, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .end local v16    # "t3":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;-><init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)V

    .line 1001
    .restart local v16    # "t3":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->start()V

    .line 1004
    if-eqz v13, :cond_1

    .line 1005
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "https://"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/autodiscover/autodiscover.xml"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1006
    new-instance v17, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .end local v17    # "t4":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;-><init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)V

    .line 1007
    .restart local v17    # "t4":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->start()V

    .line 1011
    :cond_1
    const/4 v9, 0x1

    .line 1012
    .local v9, "stepsForHttpGet":Z
    new-instance v4, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;

    .end local v4    # "t5":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v5, p0

    move-object/from16 v7, p1

    invoke-direct/range {v4 .. v9}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;-><init>(Lcom/android/exchange/AutoDiscoverHandler;Ljava/lang/String;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;Z)V

    .line 1014
    .restart local v4    # "t5":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1015
    invoke-virtual {v4}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;->start()V

    .line 1017
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    monitor-enter v7

    .line 1018
    :cond_2
    :goto_1
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z

    if-nez v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->autodiscoverThreads:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_5

    .line 1020
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    const-wide/16 v20, 0x2710

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 1021
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    if-eqz v5, :cond_2

    .line 1022
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1031
    .end local v4    # "t5":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .end local v8    # "authToken":Ljava/lang/String;
    .end local v9    # "stepsForHttpGet":Z
    .end local v10    # "AUTO_DISCOVER_PAGE":Ljava/lang/String;
    .end local v13    # "subDomain":Ljava/lang/String;
    .end local v14    # "t1":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .end local v15    # "t2":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .end local v16    # "t3":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .end local v17    # "t4":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .end local v18    # "url":Ljava/lang/String;
    :cond_3
    :goto_2
    return-void

    .line 964
    :cond_4
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v5

    const/16 v7, 0x40

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    .line 965
    .local v11, "amp":I
    if-lez v11, :cond_3

    .line 966
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v7, v11, 0x1

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 1024
    .end local v11    # "amp":I
    .restart local v4    # "t5":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .restart local v8    # "authToken":Ljava/lang/String;
    .restart local v9    # "stepsForHttpGet":Z
    .restart local v10    # "AUTO_DISCOVER_PAGE":Ljava/lang/String;
    .restart local v13    # "subDomain":Ljava/lang/String;
    .restart local v14    # "t1":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .restart local v15    # "t2":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .restart local v16    # "t3":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .restart local v17    # "t4":Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoveryThread;
    .restart local v18    # "url":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 1025
    .local v12, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v12}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 1028
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    :cond_5
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1030
    const-string v5, "AutoDiscoverHandler"

    const-string v7, "All the autoDiscovery threads finished"

    invoke-static {v5, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private tryAutodiscover(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 9
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "domain_user"    # Ljava/lang/String;
    .param p4, "bTrustCert"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 901
    new-instance v6, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    invoke-direct {v6, p1, p2, p3}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .line 902
    const/4 v2, 0x0

    .line 903
    .local v2, "bundle":Landroid/os/Bundle;
    const/4 v4, 0x1

    .line 904
    .local v4, "redirected":Z
    const/4 v5, 0x0

    .line 906
    .local v5, "serverdomain":Ljava/lang/String;
    const/4 v0, 0x0

    .line 907
    .local v0, "alreadyPerformedDNSQuery":Z
    :cond_0
    :goto_0
    iget v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mRedirectCount:I

    const/16 v7, 0xa

    if-ge v6, v7, :cond_5

    if-eqz v4, :cond_5

    .line 908
    const-string v6, "AutoDiscoverHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Starting threads for autodiscover for user  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " redirect count "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/AutoDiscoverHandler;->mRedirectCount:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    iget-object v7, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {p0, v6, v7, v5}, Lcom/android/exchange/AutoDiscoverHandler;->startAutodiscoveryThread(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)V

    .line 912
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 913
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v2

    .line 914
    const-string v6, "autodiscover_error_code"

    const/4 v7, -0x1

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 917
    const/4 v4, 0x0

    goto :goto_0

    .line 918
    :cond_1
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->isRedirected()Z
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2200(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 919
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->init()V
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    .line 920
    iget v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mRedirectCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mRedirectCount:I

    .line 922
    const/4 v4, 0x1

    goto :goto_0

    .line 923
    :cond_2
    if-nez v0, :cond_4

    .line 924
    const/4 v3, 0x0

    .line 925
    .local v3, "domainforDns":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x40

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 926
    .local v1, "amp":I
    if-lez v1, :cond_0

    .line 927
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultUserInfo:Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 928
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    invoke-direct {p0, v3}, Lcom/android/exchange/AutoDiscoverHandler;->tryDNS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 929
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Ljava/lang/String;

    move-result-object v5

    .line 930
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # invokes: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->init()V
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    .line 931
    const/4 v0, 0x1

    .line 932
    if-eqz v5, :cond_3

    .line 933
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 935
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 939
    .end local v1    # "amp":I
    .end local v3    # "domainforDns":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 942
    :cond_5
    const-string v6, "AutoDiscoverHandler"

    const-string v7, "AutoDiscovery finished"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    iget-object v6, p0, Lcom/android/exchange/AutoDiscoverHandler;->mResultAutoDiscInfo:Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v6

    return-object v6
.end method

.method public static tryAutodiscoverWithLock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLandroid/content/Context;)Landroid/os/Bundle;
    .locals 15
    .param p0, "userName"    # Ljava/lang/String;
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "domain_user"    # Ljava/lang/String;
    .param p3, "bTrustCert"    # Z
    .param p4, "currentAddress"    # Ljava/lang/String;
    .param p5, "accId"    # J
    .param p7, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 802
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 803
    .local v5, "bundle":Landroid/os/Bundle;
    sput-object p0, Lcom/android/exchange/AutoDiscoverHandler;->mOriginalUserName:Ljava/lang/String;

    .line 804
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p7

    invoke-static {v10, v0}, Lcom/android/exchange/AutoDiscoverHandler;->getOrCreateLock(Ljava/lang/String;Landroid/content/Context;)Lcom/android/exchange/AutoDiscoverHandler;

    move-result-object v8

    .line 805
    .local v8, "handlerlock":Lcom/android/exchange/AutoDiscoverHandler;
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "trying autodisocover waiting for lock for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    monitor-enter v8

    .line 810
    :try_start_0
    invoke-direct {v8}, Lcom/android/exchange/AutoDiscoverHandler;->shutdownConnectionManager()V

    .line 811
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "autodiscover lock acquired for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    const-wide/16 v10, -0x1

    cmp-long v10, p5, v10

    if-eqz v10, :cond_1

    .line 821
    move-object/from16 v0, p7

    move-wide/from16 v1, p5

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 822
    .local v4, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v4, :cond_1

    .line 823
    iget-wide v10, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-object/from16 v0, p7

    invoke-static {v0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v9

    .line 825
    .local v9, "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v9, :cond_1

    .line 826
    iget-object v10, v9, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    iget-object v10, v9, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    invoke-virtual {v10, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 830
    :cond_0
    const-string v10, "autodiscover_host_auth"

    invoke-virtual {v5, v10, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 833
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "skipping autodiscover and returning just finished result [  "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, v9, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ] for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 855
    :try_start_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    invoke-interface {v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->closeSSLVerificationDialog()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 860
    :goto_0
    :try_start_2
    invoke-direct {v8}, Lcom/android/exchange/AutoDiscoverHandler;->shutdownConnectionManager()V

    .line 861
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/AutoDiscoverHandler;->clearLock(Ljava/lang/String;)V

    .line 862
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "autodiscover lock cleared for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v8

    move-object v6, v5

    .line 883
    .end local v4    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v5    # "bundle":Landroid/os/Bundle;
    .end local v9    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .local v6, "bundle":Ljava/lang/Object;
    :goto_1
    return-object v6

    .line 856
    .end local v6    # "bundle":Ljava/lang/Object;
    .restart local v4    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v5    # "bundle":Landroid/os/Bundle;
    .restart local v9    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :catch_0
    move-exception v7

    .line 857
    .local v7, "e":Landroid/os/RemoteException;
    const-string v10, "AutoDiscoverHandler"

    const-string v11, "remote Exception in finish SSL Activity"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 865
    .end local v4    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v9    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :catchall_0
    move-exception v10

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .line 846
    :cond_1
    :try_start_3
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v8, p0, v0, v1, v2}, Lcom/android/exchange/AutoDiscoverHandler;->tryAutodiscover(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v5

    .line 848
    move-wide/from16 v0, p5

    move-object/from16 v2, p7

    invoke-static {v0, v1, v5, v2}, Lcom/android/exchange/AutoDiscoverHandler;->saveAutodiscoveredInfo(JLandroid/os/Bundle;Landroid/content/Context;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 855
    :try_start_4
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    invoke-interface {v10}, Lcom/android/emailcommon/service/IEmailServiceCallback;->closeSSLVerificationDialog()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 860
    :goto_2
    :try_start_5
    invoke-direct {v8}, Lcom/android/exchange/AutoDiscoverHandler;->shutdownConnectionManager()V

    .line 861
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/AutoDiscoverHandler;->clearLock(Ljava/lang/String;)V

    .line 862
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "autodiscover lock cleared for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 867
    const-string v10, "autodiscover_host_auth"

    invoke-virtual {v5, v10}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 869
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "returning autodiscover result [ "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "autodiscover_host_auth"

    invoke-virtual {v5, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ] for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " result "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    :goto_3
    const/4 v10, 0x1

    iget-boolean v11, v8, Lcom/android/exchange/AutoDiscoverHandler;->mIsAutoDiscoverCancelledByUser:Z

    if-ne v10, v11, :cond_2

    .line 878
    const-string v10, "autodiscover_error_code"

    const/16 v11, 0x74

    invoke-virtual {v5, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    move-object v6, v5

    .line 883
    .restart local v6    # "bundle":Ljava/lang/Object;
    goto/16 :goto_1

    .line 856
    .end local v6    # "bundle":Ljava/lang/Object;
    :catch_1
    move-exception v7

    .line 857
    .restart local v7    # "e":Landroid/os/RemoteException;
    :try_start_6
    const-string v10, "AutoDiscoverHandler"

    const-string v11, "remote Exception in finish SSL Activity"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    .line 854
    .end local v7    # "e":Landroid/os/RemoteException;
    :catchall_1
    move-exception v10

    .line 855
    :try_start_7
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v11

    invoke-interface {v11}, Lcom/android/emailcommon/service/IEmailServiceCallback;->closeSSLVerificationDialog()V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 860
    :goto_4
    :try_start_8
    invoke-direct {v8}, Lcom/android/exchange/AutoDiscoverHandler;->shutdownConnectionManager()V

    .line 861
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/android/exchange/AutoDiscoverHandler;->clearLock(Ljava/lang/String;)V

    .line 862
    const-string v11, "AutoDiscoverHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "autodiscover lock cleared for user "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v10

    .line 856
    :catch_2
    move-exception v7

    .line 857
    .restart local v7    # "e":Landroid/os/RemoteException;
    const-string v11, "AutoDiscoverHandler"

    const-string v12, "remote Exception in finish SSL Activity"

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    .line 874
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_3
    const-string v10, "AutoDiscoverHandler"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " autodiscover failed for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private tryDNS(Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 1104
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "_autodiscover._tcp."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1105
    .local v3, "dnsQuery":Ljava/lang/String;
    const-string v18, "AutoDiscoverHandler"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending dnsQuery : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const/4 v5, 0x0

    .line 1108
    .local v5, "host":Ljava/lang/String;
    const/4 v9, -0x1

    .line 1109
    .local v9, "maxPriority":I
    const/4 v10, -0x1

    .line 1111
    .local v10, "maxweight":I
    :try_start_0
    new-instance v18, Lorg/xbill/DNS/Lookup;

    const/16 v19, 0x21

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v3, v1}, Lorg/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    invoke-virtual/range {v18 .. v18}, Lorg/xbill/DNS/Lookup;->run()[Lorg/xbill/DNS/Record;

    move-result-object v13

    .line 1112
    .local v13, "records":[Lorg/xbill/DNS/Record;
    if-eqz v13, :cond_2

    .line 1113
    move-object v2, v13

    .local v2, "arr$":[Lorg/xbill/DNS/Record;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v12, v2, v7

    .line 1114
    .local v12, "record":Lorg/xbill/DNS/Record;
    move-object v0, v12

    check-cast v0, Lorg/xbill/DNS/SRVRecord;

    move-object v14, v0

    .line 1115
    .local v14, "srv":Lorg/xbill/DNS/SRVRecord;
    invoke-virtual {v14}, Lorg/xbill/DNS/SRVRecord;->getTarget()Lorg/xbill/DNS/Name;

    move-result-object v6

    .line 1116
    .local v6, "hostname":Lorg/xbill/DNS/Name;
    if-eqz v6, :cond_1

    .line 1117
    invoke-virtual {v6}, Lorg/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1118
    .local v15, "tmphost":Ljava/lang/String;
    invoke-virtual {v14}, Lorg/xbill/DNS/SRVRecord;->getPort()I

    move-result v16

    .line 1119
    .local v16, "tmpport":I
    invoke-virtual {v14}, Lorg/xbill/DNS/SRVRecord;->getPriority()I

    move-result v11

    .line 1120
    .local v11, "priority":I
    invoke-virtual {v14}, Lorg/xbill/DNS/SRVRecord;->getWeight()I
    :try_end_0
    .catch Lorg/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_2

    move-result v17

    .line 1121
    .local v17, "weight":I
    const/16 v18, 0x1bb

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    if-le v9, v11, :cond_0

    move/from16 v0, v17

    if-gt v10, v0, :cond_1

    .line 1123
    :cond_0
    move v9, v11

    .line 1124
    move/from16 v10, v17

    .line 1125
    move-object v5, v15

    .line 1113
    .end local v11    # "priority":I
    .end local v15    # "tmphost":Ljava/lang/String;
    .end local v16    # "tmpport":I
    .end local v17    # "weight":I
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1130
    .end local v2    # "arr$":[Lorg/xbill/DNS/Record;
    .end local v6    # "hostname":Lorg/xbill/DNS/Name;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v12    # "record":Lorg/xbill/DNS/Record;
    .end local v13    # "records":[Lorg/xbill/DNS/Record;
    .end local v14    # "srv":Lorg/xbill/DNS/SRVRecord;
    :catch_0
    move-exception v4

    .line 1131
    .local v4, "e":Lorg/xbill/DNS/TextParseException;
    invoke-virtual {v4}, Lorg/xbill/DNS/TextParseException;->printStackTrace()V

    .line 1137
    .end local v4    # "e":Lorg/xbill/DNS/TextParseException;
    :cond_2
    :goto_1
    if-eqz v5, :cond_3

    .line 1138
    const-string v18, "AutoDiscoverHandler"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Sending dnsQuery successfull, host : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1141
    const/16 v18, 0x0

    const-string v19, "."

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 1144
    :cond_3
    return-object v5

    .line 1132
    :catch_1
    move-exception v4

    .line 1133
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1134
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v4

    .line 1135
    .local v4, "e":Ljava/lang/ExceptionInInitializerError;
    invoke-virtual {v4}, Ljava/lang/ExceptionInInitializerError;->printStackTrace()V

    goto :goto_1
.end method

.method private static varargs userLog([Ljava/lang/String;)V
    .locals 10
    .param p0, "strings"    # [Ljava/lang/String;

    .prologue
    .line 1173
    sget-boolean v8, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v8, :cond_0

    .line 1174
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    .line 1177
    .local v6, "tid":J
    array-length v8, p0

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 1178
    const/4 v8, 0x0

    aget-object v3, p0, v8

    .line 1187
    .local v3, "logText":Ljava/lang/String;
    :goto_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AutoDiscoverHandler<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    .end local v3    # "logText":Ljava/lang/String;
    .end local v6    # "tid":J
    :cond_0
    return-void

    .line 1180
    .restart local v6    # "tid":J
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v8, 0x40

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1181
    .local v4, "sb":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v5, v0, v1

    .line 1182
    .local v5, "string":Ljava/lang/String;
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1181
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1184
    .end local v5    # "string":Ljava/lang/String;
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "logText":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public cancelAutodiscover()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1066
    iput-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsAutoDiscoverCancelledByUser:Z

    .line 1067
    iput-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler;->mIsCancelled:Z

    .line 1069
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler;->interruptAllThreads()V

    .line 1070
    const-string v0, "AutoDiscoverHandler"

    const-string v1, "Canceling autodiscover done "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    return-void
.end method
