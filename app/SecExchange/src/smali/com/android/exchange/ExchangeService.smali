.class public Lcom/android/exchange/ExchangeService;
.super Landroid/app/Service;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;,
        Lcom/android/exchange/ExchangeService$UpdateHistory;,
        Lcom/android/exchange/ExchangeService$CardStateReceiver;,
        Lcom/android/exchange/ExchangeService$ConnectivityReceiver;,
        Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;,
        Lcom/android/exchange/ExchangeService$SyncStatusError;,
        Lcom/android/exchange/ExchangeService$SyncError;,
        Lcom/android/exchange/ExchangeService$SyncStatus;,
        Lcom/android/exchange/ExchangeService$SyncedMessageObserver;,
        Lcom/android/exchange/ExchangeService$MailboxObserver;,
        Lcom/android/exchange/ExchangeService$CalendarObserver;,
        Lcom/android/exchange/ExchangeService$TaskObserver;,
        Lcom/android/exchange/ExchangeService$AccountObserver;,
        Lcom/android/exchange/ExchangeService$AccountList;,
        Lcom/android/exchange/ExchangeService$ServiceCallbackWrapper;
    }
.end annotation


# static fields
.field private static final CALENDAR_SUBFOLDER_MAP_LOCK:Ljava/lang/Object;

.field private static final CONTACT_SUBFOLDER_MAP_LOCK:Ljava/lang/Object;

.field private static final EMPTY_ACCOUNT_LIST:Lcom/android/exchange/ExchangeService$AccountList;

.field public static HAS_SENABLE_MESSAGE_IN_OUTBOX_NO:J

.field public static HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

.field protected static INSTANCE:Lcom/android/exchange/ExchangeService;

.field public static MAILBOX_DUMMY_GALSEARCH:J

.field public static MAILBOX_DUMMY_MAX:J

.field public static MAILBOX_DUMMY_MIN:J

.field public static MAILBOX_DUMMY_OoO:J

.field private static THREAD_MAP_LOCK:Ljava/lang/Object;

.field public static adClientConnectionManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile adShutdownCount:I

.field public static elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

.field public static elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

.field public static isInitSyncCalendar:Z

.field public static isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static isInitSyncContact:Z

.field public static isInitSyncContactSubFolerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static isInitSyncNote:Z

.field public static isInitSyncTask:Z

.field public static lmClientConnectionManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field

.field public static final mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

.field private static mCallbackList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/android/emailcommon/service/IEmailServiceCallback;",
            ">;"
        }
    .end annotation
.end field

.field public static mClientConnectionManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field

.field static mEasDownloadInProgress:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/exchange/EasDownLoadAttachmentSvc;",
            ">;"
        }
    .end annotation
.end field

.field public static mPingFoldersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static mPingHeartBeatIntervalMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mServiceLogger:Lcom/android/exchange/ServiceLogger;

.field private static mSubFolderCalendarAccountObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static mSubFolderContactAccountObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static mThreadIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static sAutoDiscClientConnectionManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field

.field private static final sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

.field private static volatile sClientConnectionManagerShutdownCount:I

.field public static sClientConnectionManagers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field

.field public static sConnPerPing:Lorg/apache/http/conn/params/ConnPerRoute;

.field public static sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

.field public static sConnectivityHold:Z

.field public static final sConnectivityLock:Ljava/lang/Object;

.field private static sDeviceId:Ljava/lang/String;

.field private static sLicenseKey:Ljava/lang/String;

.field private static sServiceThread:Ljava/lang/Thread;

.field private static volatile sStartingUp:Z

.field private static volatile sStop:Z

.field private static final sSyncLock:Ljava/lang/Object;

.field private static sUpdateHistoryThread:Ljava/lang/Thread;

.field public static smClientConnectionManager:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lorg/apache/http/conn/ClientConnectionManager;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isWiFiConnected:Z

.field private mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

.field private mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;

.field private volatile mBackgroundData:Z

.field private final mBinder:Lcom/android/emailcommon/service/IEmailService$Stub;

.field private mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/exchange/ExchangeService$CalendarObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mCardReceiver:Lcom/android/exchange/ExchangeService$CardStateReceiver;

.field private mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

.field private mCurrentActiveNetwork:I

.field private mHandler:Landroid/os/Handler;

.field private mIsNetworkRoaming:Z

.field private mIsNetworkRoaming2:Z

.field private mKicked:Z

.field private mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field private mNetworkState:I

.field private mNetworkState2:I

.field private mNextWaitReason:Ljava/lang/String;

.field private mPendingIntents:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/app/PendingIntent;",
            ">;"
        }
    .end annotation
.end field

.field private mPhone:Landroid/telephony/TelephonyManager;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPolicyManager:Landroid/net/NetworkPolicyManager;

.field private mPreferences:Lcom/android/exchange/Preferences;

.field private mProvisionErrorExist:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/emailcommon/provider/EmailContent$Account;",
            ">;"
        }
    .end annotation
.end field

.field private mResolver:Landroid/content/ContentResolver;

.field private mServiceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/exchange/AbstractSyncService;",
            ">;"
        }
    .end annotation
.end field

.field private mStatusChangeListener:Ljava/lang/Object;

.field mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/exchange/ExchangeService$SyncError;",
            ">;"
        }
    .end annotation
.end field

.field mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/exchange/ExchangeService$SyncStatusError;",
            ">;"
        }
    .end annotation
.end field

.field private mSyncStatusObserver:Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;

.field private mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLocks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 387
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    .line 390
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    .line 392
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    .line 401
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;

    .line 403
    const-wide/32 v0, 0xf4240

    sput-wide v0, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_MIN:J

    .line 405
    const-wide/32 v0, 0xf4241

    sput-wide v0, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_OoO:J

    .line 407
    const-wide/32 v0, 0xf4242

    sput-wide v0, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_GALSEARCH:J

    .line 409
    const-wide/32 v0, 0xf4243

    sput-wide v0, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_MAX:J

    .line 415
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z

    .line 416
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    .line 417
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    .line 418
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    .line 420
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    .line 424
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    .line 427
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NO:J

    .line 428
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

    .line 446
    new-instance v0, Lcom/android/exchange/ExchangeService$AccountList;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$AccountList;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    .line 471
    sput-object v3, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    .line 474
    sput-object v3, Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;

    .line 477
    sput-object v3, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;

    .line 481
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    .line 486
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    .line 489
    sput v2, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    .line 494
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    .line 496
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    .line 499
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    .line 500
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    .line 505
    sput v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    .line 510
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    .line 514
    sput-object v3, Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;

    .line 516
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    .line 517
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sStop:Z

    .line 539
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;

    .line 549
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    .line 550
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    .line 554
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->CONTACT_SUBFOLDER_MAP_LOCK:Ljava/lang/Object;

    .line 555
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mSubFolderContactAccountObjectMap:Ljava/util/HashMap;

    .line 559
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->CALENDAR_SUBFOLDER_MAP_LOCK:Ljava/lang/Object;

    .line 560
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mSubFolderCalendarAccountObjectMap:Ljava/util/HashMap;

    .line 564
    sput-object v3, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    .line 566
    sput-object v3, Lcom/android/exchange/ExchangeService;->elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

    .line 573
    new-instance v0, Lcom/android/exchange/ExchangeService$AccountList;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$AccountList;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->EMPTY_ACCOUNT_LIST:Lcom/android/exchange/ExchangeService$AccountList;

    .line 579
    new-instance v0, Lcom/android/exchange/ServiceLogger;

    invoke-direct {v0}, Lcom/android/exchange/ServiceLogger;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    .line 598
    new-instance v0, Lcom/android/exchange/ExchangeService$1;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$1;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    .line 3487
    new-instance v0, Lcom/android/exchange/ExchangeService$4;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$4;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    .line 3496
    new-instance v0, Lcom/android/exchange/ExchangeService$5;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$5;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sConnPerPing:Lorg/apache/http/conn/params/ConnPerRoute;

    .line 3502
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 166
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 369
    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z

    .line 371
    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z

    .line 383
    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->isWiFiConnected:Z

    .line 399
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    .line 432
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 434
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 437
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    .line 440
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    .line 443
    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 449
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mHandler:Landroid/os/Handler;

    .line 464
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 523
    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    .line 526
    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    .line 529
    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mCardReceiver:Lcom/android/exchange/ExchangeService$CardStateReceiver;

    .line 534
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/ExchangeService;->mBackgroundData:Z

    .line 544
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    .line 583
    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;

    .line 890
    new-instance v0, Lcom/android/exchange/ExchangeService$2;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$2;-><init>(Lcom/android/exchange/ExchangeService;)V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService;->mBinder:Lcom/android/emailcommon/service/IEmailService$Stub;

    .line 7684
    return-void
.end method

.method static synthetic access$000()Landroid/os/RemoteCallbackList;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/exchange/ExchangeService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/exchange/ExchangeService;Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/android/exchange/Request;

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/exchange/ExchangeService;JZ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/exchange/ExchangeService;)Lcom/android/exchange/Preferences;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/exchange/ExchangeService;Lcom/android/exchange/Preferences;)Lcom/android/exchange/Preferences;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Lcom/android/exchange/Preferences;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/android/exchange/ExchangeService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # [Ljava/lang/String;
    .param p6, "x6"    # Ljava/lang/String;
    .param p7, "x7"    # Ljava/lang/String;
    .param p8, "x8"    # I

    .prologue
    .line 166
    invoke-direct/range {p0 .. p8}, Lcom/android/exchange/ExchangeService;->buildSearchRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/android/exchange/ExchangeService$AccountList;

    .prologue
    .line 166
    invoke-static {p0, p1}, Lcom/android/exchange/ExchangeService;->collectEasAccounts(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/exchange/ExchangeService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/exchange/ExchangeService;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/exchange/ExchangeService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/ExchangeService;->stopPing(J)V

    return-void
.end method

.method static synthetic access$200()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/exchange/ExchangeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->checkPIMSyncSettings()V

    return-void
.end method

.method static synthetic access$2100(Lcom/android/exchange/ExchangeService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/android/exchange/ExchangeService;->runAccountReconcilerSync(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$2200()Lcom/android/exchange/ServiceLogger;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/exchange/ExchangeService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/android/exchange/ExchangeService;->isWiFiConnected:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/android/exchange/ExchangeService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/android/exchange/ExchangeService;->isWiFiConnected:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/android/exchange/ExchangeService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->updateAccountDb()V

    return-void
.end method

.method static synthetic access$2500(Lcom/android/exchange/ExchangeService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget v0, p0, Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I

    return v0
.end method

.method static synthetic access$2502(Lcom/android/exchange/ExchangeService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/android/exchange/ExchangeService;->mCurrentActiveNetwork:I

    return p1
.end method

.method static synthetic access$2600(Lcom/android/exchange/ExchangeService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/android/exchange/ExchangeService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/android/exchange/ExchangeService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/android/exchange/ExchangeService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/android/exchange/ExchangeService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget v0, p0, Lcom/android/exchange/ExchangeService;->mNetworkState:I

    return v0
.end method

.method static synthetic access$2802(Lcom/android/exchange/ExchangeService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/android/exchange/ExchangeService;->mNetworkState:I

    return p1
.end method

.method static synthetic access$2900()Z
    .locals 1

    .prologue
    .line 166
    sget-boolean v0, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    return v0
.end method

.method static synthetic access$2902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 166
    sput-boolean p0, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    return p0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/android/exchange/ExchangeService;Lcom/android/emailcommon/service/AccountServiceProxy;)Lcom/android/emailcommon/service/AccountServiceProxy;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Lcom/android/emailcommon/service/AccountServiceProxy;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;

    return-object p1
.end method

.method static synthetic access$302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 166
    sput-object p0, Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3100()Z
    .locals 1

    .prologue
    .line 166
    sget-boolean v0, Lcom/android/exchange/ExchangeService;->sStop:Z

    return v0
.end method

.method static synthetic access$3102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 166
    sput-boolean p0, Lcom/android/exchange/ExchangeService;->sStop:Z

    return p0
.end method

.method static synthetic access$3200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 166
    sput-object p0, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3300()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/exchange/ExchangeService;JJ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/exchange/ExchangeService;->setAlarm(JJ)V

    return-void
.end method

.method static synthetic access$3500()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$3502(Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Thread;

    .prologue
    .line 166
    sput-object p0, Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;

    return-object p0
.end method

.method static synthetic access$3600(Lcom/android/exchange/ExchangeService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;

    .prologue
    .line 166
    iget v0, p0, Lcom/android/exchange/ExchangeService;->mNetworkState2:I

    return v0
.end method

.method static synthetic access$3602(Lcom/android/exchange/ExchangeService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # I

    .prologue
    .line 166
    iput p1, p0, Lcom/android/exchange/ExchangeService;->mNetworkState2:I

    return p1
.end method

.method static synthetic access$400()Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/exchange/ExchangeService;Landroid/content/Context;IJ)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/exchange/ExchangeService;->releaseSyncHoldMailbox(Landroid/content/Context;IJ)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/exchange/ExchangeService;J)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/ExchangeService;->getMailBoxIdRIC(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/exchange/ExchangeService;J)Lcom/android/exchange/EasDownLoadAttachmentSvc;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J

    .prologue
    .line 166
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/ExchangeService;->getDownloadServiceFromMap(J)Lcom/android/exchange/EasDownLoadAttachmentSvc;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/exchange/ExchangeService;JLcom/android/exchange/EasDownLoadAttachmentSvc;)V
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService;
    .param p1, "x1"    # J
    .param p3, "x2"    # Lcom/android/exchange/EasDownLoadAttachmentSvc;

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->setDownloadServiceInMap(JLcom/android/exchange/EasDownLoadAttachmentSvc;)V

    return-void
.end method

.method private acquireWakeLock(J)V
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 4270
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    monitor-enter v3

    .line 4271
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 4272
    .local v0, "lock":Ljava/lang/Boolean;
    if-nez v0, :cond_1

    .line 4273
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_0

    .line 4274
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 4275
    .local v1, "pm":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v4, "MAIL_SERVICE"

    invoke-virtual {v1, v2, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 4276
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 4279
    .end local v1    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4281
    :cond_1
    monitor-exit v3

    .line 4282
    return-void

    .line 4281
    .end local v0    # "lock":Ljava/lang/Boolean;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static alert(Landroid/content/Context;J)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J

    .prologue
    .line 4394
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4395
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->checkExchangeServiceServiceRunning()V

    .line 4396
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    .line 4397
    const-string v3, "ExchangeService alert"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4398
    const-string v3, "ping ExchangeService"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 4474
    :cond_0
    :goto_0
    return-void

    .line 4399
    :cond_1
    if-nez v0, :cond_2

    .line 4400
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/android/exchange/ExchangeService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 4403
    :cond_2
    sget-wide v4, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_MIN:J

    cmp-long v3, p1, v4

    if-lez v3, :cond_3

    sget-wide v4, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_MAX:J

    cmp-long v3, p1, v4

    if-gez v3, :cond_3

    .line 4404
    const-string v2, "ExchangeService Alert: Out of Band"

    .line 4405
    .local v2, "threadName":Ljava/lang/String;
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/android/exchange/ExchangeService$6;

    invoke-direct {v4, p1, p2, v0}, Lcom/android/exchange/ExchangeService$6;-><init>(JLcom/android/exchange/ExchangeService;)V

    invoke-direct {v3, v4, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 4418
    .end local v2    # "threadName":Ljava/lang/String;
    :cond_3
    iget-object v3, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/exchange/AbstractSyncService;

    .line 4419
    .local v1, "service":Lcom/android/exchange/AbstractSyncService;
    if-eqz v1, :cond_0

    .line 4424
    const-string v2, "ExchangeService Alert: "

    .line 4425
    .restart local v2    # "threadName":Ljava/lang/String;
    iget-object v3, v1, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v3, :cond_4

    .line 4426
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4428
    :cond_4
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/android/exchange/ExchangeService$7;

    invoke-direct {v4, v0, p1, p2, v1}, Lcom/android/exchange/ExchangeService$7;-><init>(Lcom/android/exchange/ExchangeService;JLcom/android/exchange/AbstractSyncService;)V

    invoke-direct {v3, v4, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static alwaysLog(Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 3415
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-nez v0, :cond_0

    .line 3416
    const-string v0, "ExchangeService"

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3420
    :goto_0
    return-void

    .line 3418
    :cond_0
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static alwaysTagLog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "type"    # I
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 3406
    if-nez p0, :cond_1

    .line 3407
    invoke-static {p1, p2}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3412
    :cond_0
    :goto_0
    return-void

    .line 3408
    :cond_1
    const/4 v0, 0x1

    if-ne p0, v0, :cond_2

    .line 3409
    invoke-static {p1, p2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3410
    :cond_2
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    .line 3411
    invoke-static {p1, p2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static blockDevice(JI)V
    .locals 6
    .param p0, "acctId"    # J
    .param p2, "blockType"    # I

    .prologue
    .line 3965
    sget-object v2, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3966
    .local v2, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v2, :cond_0

    .line 3967
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-static {v3, p0, p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 3968
    .local v0, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_0

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mdeviceBlockedType:I

    if-eq v3, p2, :cond_0

    .line 3969
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3970
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "deviceBlockedType"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3971
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v0, v3, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 3974
    .end local v0    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private buildSearchRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;
    .locals 18
    .param p1, "accountIdString"    # Ljava/lang/String;
    .param p2, "searchTextString"    # Ljava/lang/String;
    .param p3, "greaterThanDateString"    # Ljava/lang/String;
    .param p4, "lessThanDateString"    # Ljava/lang/String;
    .param p5, "mailboxIdStrings"    # [Ljava/lang/String;
    .param p6, "optionsDeepTraversalStr"    # Ljava/lang/String;
    .param p7, "conversationIdStr"    # Ljava/lang/String;
    .param p8, "maxRange"    # I

    .prologue
    .line 2048
    new-instance v3, Lcom/android/exchange/SearchRequest$Builder;

    invoke-direct {v3}, Lcom/android/exchange/SearchRequest$Builder;-><init>()V

    .line 2049
    .local v3, "builder":Lcom/android/exchange/SearchRequest$Builder;
    const-string v15, "ExchangeProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " search mailbox num ="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p5

    array-length v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2052
    const/4 v2, 0x0

    .line 2053
    .local v2, "a":Lcom/android/emailcommon/provider/EmailContent$Account;
    const-wide/16 v4, -0x1

    .line 2054
    .local v4, "accId":J
    const/4 v14, 0x0

    .line 2055
    .local v14, "truncation_size":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2056
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 2057
    if-eqz v2, :cond_5

    .line 2058
    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailSize()B

    move-result v15

    invoke-static {v15}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v15

    sget-object v16, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_4

    .line 2059
    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailSize()B

    move-result v15

    invoke-static {v15}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Value()I

    move-result v14

    .line 2060
    const-string v15, "ExchangeProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Jane: Email Size = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailSize()B

    move-result v17

    invoke-static/range {v17 .. v17}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Text()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2069
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Lcom/android/exchange/SearchRequest$Builder;->accountId(J)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    sget-object v16, Lcom/android/exchange/SearchRequest$StoreName;->MAILBOX:Lcom/android/exchange/SearchRequest$StoreName;

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/SearchRequest$Builder;->storeName(Lcom/android/exchange/SearchRequest$StoreName;)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    sget-object v16, Lcom/android/exchange/SearchRequest$QueryClass;->EMAIL:Lcom/android/exchange/SearchRequest$QueryClass;

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/SearchRequest$Builder;->queryClass(Lcom/android/exchange/SearchRequest$QueryClass;)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lcom/android/exchange/SearchRequest$Builder;->queryFreeText(Ljava/lang/String;)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/SearchRequest$Builder;->optionsRebuildResults()Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v0, v16

    move/from16 v1, p8

    invoke-virtual {v15, v0, v1}, Lcom/android/exchange/SearchRequest$Builder;->optionsRange(II)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    sget-object v16, Lcom/android/exchange/SearchRequest$BodyPreferenceType;->PLAIN_TEXT:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/SearchRequest$Builder;->optionsBodyPreferenceType(Lcom/android/exchange/SearchRequest$BodyPreferenceType;)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    invoke-virtual {v15, v14}, Lcom/android/exchange/SearchRequest$Builder;->optionsBodyPreferenceTruncationSize(I)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    sget-object v16, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->SEND_DATA_FOR_ALL_MSGS:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/SearchRequest$Builder;->optionsMIMESupport(Lcom/android/exchange/SearchRequest$OptionsMIMESupport;)Lcom/android/exchange/SearchRequest$Builder;

    move-result-object v15

    move-object/from16 v0, p7

    invoke-virtual {v15, v0}, Lcom/android/exchange/SearchRequest$Builder;->queryConvId(Ljava/lang/String;)Lcom/android/exchange/SearchRequest$Builder;

    .line 2084
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, "True"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-nez v15, :cond_0

    .line 2085
    invoke-virtual {v3}, Lcom/android/exchange/SearchRequest$Builder;->optionsDeepTraversal()Lcom/android/exchange/SearchRequest$Builder;

    .line 2087
    :cond_0
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, "NoneDate"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_1

    .line 2088
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v15, "dd/MM/yyyy hh:mm:ss a"

    invoke-direct {v11, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2089
    .local v11, "formatter":Ljava/text/SimpleDateFormat;
    const/4 v9, 0x0

    .line 2091
    .local v9, "date":Ljava/util/Date;
    :try_start_1
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    .line 2092
    const-string v15, "ExchangeProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "reaterThanDate = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2096
    :try_start_2
    invoke-virtual {v3, v9}, Lcom/android/exchange/SearchRequest$Builder;->queryGreaterThan(Ljava/util/Date;)Lcom/android/exchange/SearchRequest$Builder;

    .line 2098
    .end local v9    # "date":Ljava/util/Date;
    .end local v11    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_1
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, "NoneDate"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_2

    .line 2099
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v15, "dd/MM/yyyy hh:mm:ss a"

    invoke-direct {v11, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2100
    .restart local v11    # "formatter":Ljava/text/SimpleDateFormat;
    const/4 v9, 0x0

    .line 2102
    .restart local v9    # "date":Ljava/util/Date;
    :try_start_3
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v9

    .line 2105
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 2106
    .local v6, "cal":Ljava/util/Calendar;
    invoke-virtual {v6, v9}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 2107
    const-string v15, "ExchangeService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " LESS Hours "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0xb

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2108
    const-string v15, "ExchangeService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " LESS mIN "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0xc

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2109
    const-string v15, "ExchangeProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "lessThanDate = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    .line 2113
    :try_start_4
    invoke-virtual {v3, v9}, Lcom/android/exchange/SearchRequest$Builder;->queryLessThan(Ljava/util/Date;)Lcom/android/exchange/SearchRequest$Builder;

    .line 2116
    .end local v6    # "cal":Ljava/util/Calendar;
    .end local v9    # "date":Ljava/util/Date;
    .end local v11    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2117
    .local v8, "collectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    move-object/from16 v0, p5

    array-length v15, v0

    if-ge v12, v15, :cond_6

    .line 2118
    aget-object v15, p5, v12

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    const-string v16, "NoneId"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_3

    .line 2119
    const-string v15, "ExchangeProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, " MailboxString of search ="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    aget-object v17, p5, v12

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2120
    aget-object v15, p5, v12

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v8, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2117
    :cond_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 2064
    .end local v8    # "collectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v12    # "i":I
    :cond_4
    const/4 v14, -0x1

    goto/16 :goto_0

    .line 2066
    :cond_5
    const/16 v14, 0x400

    goto/16 :goto_0

    .line 2093
    .restart local v9    # "date":Ljava/util/Date;
    .restart local v11    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v10

    .line 2094
    .local v10, "e":Ljava/lang/Exception;
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Illegal value in Uri Date"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1

    .line 2131
    .end local v9    # "date":Ljava/util/Date;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_1
    move-exception v10

    .line 2132
    .local v10, "e":Ljava/lang/NumberFormatException;
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Illegal value in URI"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2110
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    .restart local v9    # "date":Ljava/util/Date;
    .restart local v11    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_2
    move-exception v10

    .line 2111
    .local v10, "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v15, Ljava/lang/IllegalArgumentException;

    const-string v16, "Illegal value in Uri Date"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 2124
    .end local v9    # "date":Ljava/util/Date;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "formatter":Ljava/text/SimpleDateFormat;
    .restart local v8    # "collectionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v12    # "i":I
    :cond_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v15

    new-array v7, v15, [J

    .line 2125
    .local v7, "collectionIds":[J
    const/4 v12, 0x0

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v12, v15, :cond_7

    .line 2126
    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    aput-wide v16, v7, v12

    .line 2125
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 2129
    :cond_7
    invoke-virtual {v3, v7}, Lcom/android/exchange/SearchRequest$Builder;->queryCollectionIds([J)Lcom/android/exchange/SearchRequest$Builder;
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_1

    .line 2134
    invoke-virtual {v3}, Lcom/android/exchange/SearchRequest$Builder;->build()Lcom/android/exchange/SearchRequest;

    move-result-object v13

    .line 2135
    .local v13, "searchRequest":Lcom/android/exchange/SearchRequest;
    return-object v13
.end method

.method private calculateAlignedTime(JJJ)J
    .locals 9
    .param p1, "now"    # J
    .param p3, "interval"    # J
    .param p5, "lastSync"    # J

    .prologue
    const/16 v7, 0xc

    const/4 v6, 0x0

    .line 6195
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 6196
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/32 v2, 0xea60

    mul-long/2addr v2, p3

    add-long/2addr v2, p5

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 6197
    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x5

    int-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v7, v1}, Ljava/util/Calendar;->set(II)V

    .line 6198
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 6199
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v6}, Ljava/util/Calendar;->set(II)V

    .line 6201
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v2, p1

    return-wide v2
.end method

.method public static callback()Lcom/android/emailcommon/service/IEmailServiceCallback;
    .locals 1

    .prologue
    .line 2982
    sget-object v0, Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    return-object v0
.end method

.method static canAutoSync(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5630
    if-nez p1, :cond_1

    .line 5651
    :cond_0
    :goto_0
    return v2

    .line 5633
    :cond_1
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 5634
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 5639
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v1

    .line 5641
    .local v1, "isRoaming":Z
    if-eqz v1, :cond_4

    .line 5642
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-nez v4, :cond_2

    .line 5643
    iget-wide v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v0, v4, v5}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v4

    iput-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 5645
    :cond_2
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget-boolean v4, v4, Lcom/android/emailcommon/service/PolicySet;->mRequireManualSyncWhenRoaming:Z

    if-nez v4, :cond_0

    :cond_3
    move v2, v3

    .line 5648
    goto :goto_0

    :cond_4
    move v2, v3

    .line 5651
    goto :goto_0
.end method

.method private canSyncEmail(Landroid/accounts/Account;)Z
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 5664
    const-string v0, "com.android.email.provider"

    invoke-static {p1, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static checkExchangeServiceServiceRunning()V
    .locals 3

    .prologue
    .line 5256
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 5257
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v0, :cond_1

    .line 5263
    :cond_0
    :goto_0
    return-void

    .line 5259
    :cond_1
    sget-object v1, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    if-nez v1, :cond_0

    .line 5260
    const-string v1, "!!! checkExchangeServiceServiceRunning; starting service..."

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5261
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/exchange/ExchangeService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/android/exchange/ExchangeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private checkMailboxes()J
    .locals 62

    .prologue
    .line 5746
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 5747
    .local v25, "deletedMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 5748
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    .line 5749
    .local v30, "mailboxId":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v29

    .line 5750
    .local v29, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v29, :cond_0

    .line 5751
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5771
    .end local v28    # "i$":Ljava/util/Iterator;
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v30    # "mailboxId":J
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 5755
    .restart local v28    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v28

    :goto_1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Long;

    .line 5756
    .local v30, "mailboxId":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Lcom/android/exchange/AbstractSyncService;

    .line 5757
    .local v48, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v48, :cond_2

    move-object/from16 v0, v48

    iget-object v4, v0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    if-nez v4, :cond_3

    .line 5758
    :cond_2
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    goto :goto_1

    .line 5761
    :cond_3
    move-object/from16 v0, v48

    iget-object v4, v0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isAlive()Z

    move-result v17

    .line 5762
    .local v17, "alive":Z
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleted mailbox: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v48

    iget-object v6, v0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5763
    if-eqz v17, :cond_4

    .line 5764
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->stopManualSync(J)V

    goto :goto_1

    .line 5766
    :cond_4
    const-string v4, "Removing from serviceMap"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5767
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    goto :goto_1

    .line 5771
    .end local v17    # "alive":Z
    .end local v30    # "mailboxId":Ljava/lang/Long;
    .end local v48    # "svc":Lcom/android/exchange/AbstractSyncService;
    :cond_5
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5772
    const-wide/32 v38, 0xdbba0

    .line 5773
    .local v38, "nextWait":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v42

    .line 5778
    .local v42, "now":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    if-nez v4, :cond_6

    .line 5779
    const-string v4, "mAccountObserver null; service died??"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    move-wide/from16 v40, v38

    .line 6191
    .end local v38    # "nextWait":J
    .local v40, "nextWait":J
    :goto_2
    return-wide v40

    .line 5783
    .end local v40    # "nextWait":J
    .restart local v38    # "nextWait":J
    :cond_6
    const/16 v21, 0x0

    .line 5786
    .local v21, "c":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    invoke-virtual {v7}, Lcom/android/exchange/ExchangeService$AccountObserver;->getSyncableEasMailboxWhere()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 5791
    new-instance v47, Landroid/content/ContentValues;

    invoke-direct/range {v47 .. v47}, Landroid/content/ContentValues;-><init>()V

    .line 5792
    .local v47, "subFolderContactCV":Landroid/content/ContentValues;
    const-string v4, "syncInterval"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v47

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5793
    const-string v4, "syncKey"

    const-string v5, "0"

    move-object/from16 v0, v47

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5797
    new-instance v46, Landroid/content/ContentValues;

    invoke-direct/range {v46 .. v46}, Landroid/content/ContentValues;-><init>()V

    .line 5798
    .local v46, "subFolderCalendarCV":Landroid/content/ContentValues;
    const-string v4, "syncInterval"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 5799
    const-string v4, "syncKey"

    const-string v5, "0"

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5802
    :cond_7
    :goto_3
    if-eqz v21, :cond_31

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_31

    .line 5803
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v33

    .line 5804
    .local v33, "masterAutoSync":Z
    const/4 v4, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v34

    .line 5806
    .local v34, "mid":J
    const/4 v4, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v18, v0

    .line 5807
    .local v18, "acctId":J
    const/16 v4, 0x9

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v8, v4

    .line 5808
    .local v8, "interval":J
    const/4 v4, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v59

    .line 5809
    .local v59, "type":I
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 5810
    .local v26, "displayName":Ljava/lang/String;
    const/16 v37, 0x0

    .line 5811
    .local v37, "service":Lcom/android/exchange/AbstractSyncService;
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 5812
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/android/exchange/AbstractSyncService;

    move-object/from16 v37, v0

    .line 5813
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 5818
    const/16 v4, 0x44

    move/from16 v0, v59

    if-ne v0, v4, :cond_8

    const-wide/16 v4, -0x1

    cmp-long v4, v8, v4

    if-nez v4, :cond_8

    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 5823
    :cond_8
    if-nez v37, :cond_2a

    .line 5824
    const/4 v4, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v12

    .line 5825
    .local v12, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v12, :cond_7

    .line 5833
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 5838
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 5839
    .local v16, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/16 v4, 0x44

    move/from16 v0, v59

    if-eq v0, v4, :cond_9

    if-eqz v16, :cond_9

    .line 5840
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncErrorExist for account:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ". Put hold on mailbox:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5842
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v7, Lcom/android/exchange/ExchangeService$SyncError;

    const/16 v60, 0x4

    const/16 v61, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v60

    move/from16 v2, v61

    invoke-direct {v7, v0, v1, v2}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5845
    :cond_9
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 5850
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/ExchangeService;->mBackgroundData:Z

    if-nez v4, :cond_a

    const/4 v4, 0x4

    move/from16 v0, v59

    if-ne v0, v4, :cond_7

    .line 5856
    :cond_a
    const/4 v4, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v32

    .line 5858
    .local v32, "mailboxType":I
    const/16 v4, 0x42

    move/from16 v0, v59

    if-eq v0, v4, :cond_b

    const/16 v4, 0x41

    move/from16 v0, v59

    if-eq v0, v4, :cond_b

    const/16 v4, 0x43

    move/from16 v0, v59

    if-eq v0, v4, :cond_b

    const/16 v4, 0x45

    move/from16 v0, v59

    if-eq v0, v4, :cond_b

    const/16 v4, 0x53

    move/from16 v0, v59

    if-eq v0, v4, :cond_b

    const/16 v4, 0x52

    move/from16 v0, v59

    if-ne v0, v4, :cond_f

    .line 5866
    :cond_b
    const/16 v4, 0x53

    move/from16 v0, v59

    if-ne v0, v4, :cond_e

    .line 5867
    const-string v4, "Suggested Contacts"

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 5870
    const/4 v4, 0x6

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2, v4}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v58

    .line 5872
    .local v58, "trashServerId":Ljava/lang/String;
    const/4 v4, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 5874
    .local v23, "contactSubFolderParentServerId":Ljava/lang/String;
    if-eqz v58, :cond_e

    move-object/from16 v0, v58

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 5877
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Contact SubFolder, Updating synckey zero and interval as manual for mailbox id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v34

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " act id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5880
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "accountKey=? and serverId=?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/16 v60, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v61

    aput-object v61, v7, v60

    const/16 v60, 0x1

    const/16 v61, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v61

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v61

    aput-object v61, v7, v60

    move-object/from16 v0, v47

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto/16 :goto_3

    .line 6181
    .end local v8    # "interval":J
    .end local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v16    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v18    # "acctId":J
    .end local v23    # "contactSubFolderParentServerId":Ljava/lang/String;
    .end local v26    # "displayName":Ljava/lang/String;
    .end local v32    # "mailboxType":I
    .end local v33    # "masterAutoSync":Z
    .end local v34    # "mid":J
    .end local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .end local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .end local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .end local v58    # "trashServerId":Ljava/lang/String;
    .end local v59    # "type":I
    :catch_0
    move-exception v27

    .line 6182
    .local v27, "e":Ljava/lang/IllegalStateException;
    :try_start_7
    const-string v4, "IllegalStateException exception in checkMailboxes() loop"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6183
    const-string v4, "ExchangeService"

    move-object/from16 v0, v27

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 6188
    if-eqz v21, :cond_c

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_c

    .line 6189
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .end local v27    # "e":Ljava/lang/IllegalStateException;
    :cond_c
    :goto_4
    move-wide/from16 v40, v38

    .line 6191
    .end local v38    # "nextWait":J
    .restart local v40    # "nextWait":J
    goto/16 :goto_2

    .line 5813
    .end local v40    # "nextWait":J
    .restart local v8    # "interval":J
    .restart local v18    # "acctId":J
    .restart local v26    # "displayName":Ljava/lang/String;
    .restart local v33    # "masterAutoSync":Z
    .restart local v34    # "mid":J
    .restart local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .restart local v38    # "nextWait":J
    .restart local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .restart local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .restart local v59    # "type":I
    :catchall_1
    move-exception v4

    :try_start_8
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v4
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 6184
    .end local v8    # "interval":J
    .end local v18    # "acctId":J
    .end local v26    # "displayName":Ljava/lang/String;
    .end local v33    # "masterAutoSync":Z
    .end local v34    # "mid":J
    .end local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .end local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .end local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .end local v59    # "type":I
    :catch_1
    move-exception v27

    .line 6185
    .local v27, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_a
    const-string v4, "SQLiteException exception in checkMailboxes() loop"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6186
    const-string v4, "ExchangeService"

    move-object/from16 v0, v27

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 6188
    if-eqz v21, :cond_c

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_c

    .line 6189
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto :goto_4

    .line 5845
    .end local v27    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v8    # "interval":J
    .restart local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v18    # "acctId":J
    .restart local v26    # "displayName":Ljava/lang/String;
    .restart local v33    # "masterAutoSync":Z
    .restart local v34    # "mid":J
    .restart local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .restart local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .restart local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .restart local v59    # "type":I
    :catchall_2
    move-exception v4

    :try_start_b
    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v4
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 6188
    .end local v8    # "interval":J
    .end local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v18    # "acctId":J
    .end local v26    # "displayName":Ljava/lang/String;
    .end local v33    # "masterAutoSync":Z
    .end local v34    # "mid":J
    .end local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .end local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .end local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .end local v59    # "type":I
    :catchall_3
    move-exception v4

    if-eqz v21, :cond_d

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_d

    .line 6189
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v4

    .line 5898
    .restart local v8    # "interval":J
    .restart local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v16    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v18    # "acctId":J
    .restart local v26    # "displayName":Ljava/lang/String;
    .restart local v32    # "mailboxType":I
    .restart local v33    # "masterAutoSync":Z
    .restart local v34    # "mid":J
    .restart local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .restart local v46    # "subFolderCalendarCV":Landroid/content/ContentValues;
    .restart local v47    # "subFolderContactCV":Landroid/content/ContentValues;
    .restart local v59    # "type":I
    :cond_e
    const/16 v4, 0x52

    move/from16 v0, v59

    if-ne v0, v4, :cond_f

    .line 5900
    const/4 v4, 0x6

    :try_start_d
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2, v4}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v58

    .line 5902
    .restart local v58    # "trashServerId":Ljava/lang/String;
    const/4 v4, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 5904
    .local v22, "calendarSubFolderParentServerId":Ljava/lang/String;
    if-eqz v58, :cond_f

    move-object/from16 v0, v58

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 5907
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Calendar SubFolder, Updating synckey zero and interval as manual for mailbox id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v34

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " act id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5910
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "accountKey=? and serverId=?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/16 v60, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v61

    aput-object v61, v7, v60

    const/16 v60, 0x1

    const/16 v61, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v61

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v61

    aput-object v61, v7, v60

    move-object/from16 v0, v46

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_3

    .line 5926
    .end local v22    # "calendarSubFolderParentServerId":Ljava/lang/String;
    .end local v58    # "trashServerId":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v12, v1}, Lcom/android/exchange/ExchangeService;->isMailboxSyncable(Lcom/android/emailcommon/provider/EmailContent$Account;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 5936
    if-nez v33, :cond_10

    const/16 v4, 0x44

    move/from16 v0, v59

    if-eq v0, v4, :cond_10

    const/4 v4, 0x4

    move/from16 v0, v59

    if-ne v0, v4, :cond_7

    .line 5940
    :cond_10
    new-instance v13, Landroid/accounts/Account;

    iget-object v4, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v5, "com.android.exchange"

    invoke-direct {v13, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5942
    .local v13, "accountManagerAccount":Landroid/accounts/Account;
    const/16 v4, 0x42

    move/from16 v0, v59

    if-eq v0, v4, :cond_11

    const/16 v4, 0x41

    move/from16 v0, v59

    if-eq v0, v4, :cond_11

    const/16 v4, 0x43

    move/from16 v0, v59

    if-eq v0, v4, :cond_11

    const/16 v4, 0x45

    move/from16 v0, v59

    if-eq v0, v4, :cond_11

    const/16 v4, 0x53

    move/from16 v0, v59

    if-eq v0, v4, :cond_11

    const/16 v4, 0x52

    move/from16 v0, v59

    if-ne v0, v4, :cond_19

    .line 5948
    :cond_11
    const/16 v4, 0x42

    move/from16 v0, v59

    if-eq v0, v4, :cond_12

    const/16 v4, 0x53

    move/from16 v0, v59

    if-ne v0, v4, :cond_16

    .line 5952
    :cond_12
    const-string v20, "com.android.contacts"

    .line 5969
    .local v20, "authority":Ljava/lang/String;
    :cond_13
    :goto_5
    move-object/from16 v0, v20

    invoke-static {v13, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 5972
    const/16 v4, 0x41

    move/from16 v0, v59

    if-eq v0, v4, :cond_14

    const/16 v4, 0x52

    move/from16 v0, v59

    if-ne v0, v4, :cond_15

    :cond_14
    iget-wide v4, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/ExchangeService;->isCalendarEnabled(J)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 5996
    .end local v20    # "authority":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v49

    check-cast v49, Lcom/android/exchange/ExchangeService$SyncError;

    .line 5997
    .local v49, "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v49, :cond_1e

    .line 5999
    move-object/from16 v0, v49

    iget-boolean v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    if-eqz v4, :cond_1c

    .line 6001
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Mailbox: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") in fatal error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v49

    iget v5, v0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Nothing we can do about fatal errors"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6006
    if-eqz v12, :cond_7

    .line 6007
    iget v4, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit8 v4, v4, 0x20

    if-nez v4, :cond_1b

    .line 6008
    const-string v4, "if mSyncErrorMap of __eas exists, remove it"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6009
    invoke-static/range {v18 .. v19}, Lcom/android/exchange/ExchangeService;->releaseSecurityHoldForEasMailbox(J)V

    goto/16 :goto_3

    .line 5953
    .end local v49    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    :cond_16
    const/16 v4, 0x43

    move/from16 v0, v59

    if-ne v0, v4, :cond_17

    .line 5954
    const-string v20, "tasks"

    .restart local v20    # "authority":Ljava/lang/String;
    goto/16 :goto_5

    .line 5955
    .end local v20    # "authority":Ljava/lang/String;
    :cond_17
    const/16 v4, 0x45

    move/from16 v0, v59

    if-ne v0, v4, :cond_18

    .line 5956
    const-string v20, "com.android.notes"

    .restart local v20    # "authority":Ljava/lang/String;
    goto/16 :goto_5

    .line 5959
    .end local v20    # "authority":Ljava/lang/String;
    :cond_18
    const-string v20, "com.android.calendar"

    .line 5960
    .restart local v20    # "authority":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v6, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 5965
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/android/exchange/ExchangeService;->registerCalendarObserver(Lcom/android/emailcommon/provider/EmailContent$Account;)V

    goto/16 :goto_5

    .line 5976
    .end local v20    # "authority":Ljava/lang/String;
    :cond_19
    const/4 v4, 0x6

    move/from16 v0, v59

    if-eq v0, v4, :cond_7

    .line 5980
    const/16 v4, 0x8

    move/from16 v0, v59

    if-eq v0, v4, :cond_7

    .line 5982
    const/16 v4, 0x40

    move/from16 v0, v59

    if-ge v0, v4, :cond_1a

    const-string v4, "com.android.email.provider"

    invoke-static {v13, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 5990
    :cond_1a
    const/16 v4, 0x62

    move/from16 v0, v59

    if-ne v0, v4, :cond_15

    goto/16 :goto_3

    .line 6011
    .restart local v49    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    :cond_1b
    const-string v4, "put account in mProvisionErrorExist"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6012
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v6, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 6019
    :cond_1c
    move-object/from16 v0, v49

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    cmp-long v4, v42, v4

    if-gez v4, :cond_1d

    .line 6022
    move-object/from16 v0, v49

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    add-long v6, v42, v38

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 6023
    move-object/from16 v0, v49

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    sub-long v38, v4, v42

    .line 6024
    const-string v4, "Release hold"

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    goto/16 :goto_3

    .line 6029
    :cond_1d
    const-wide/16 v4, 0x0

    move-object/from16 v0, v49

    iput-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 6034
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Lcom/android/exchange/ExchangeService$SyncError;

    .line 6035
    .local v50, "syncStatusError":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v50, :cond_20

    .line 6036
    move-object/from16 v0, v50

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    cmp-long v4, v42, v4

    if-gez v4, :cond_1f

    .line 6039
    move-object/from16 v0, v50

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    add-long v6, v42, v38

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 6040
    move-object/from16 v0, v50

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    sub-long v38, v4, v42

    .line 6041
    const-string v4, "Release hold"

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    goto/16 :goto_3

    .line 6046
    :cond_1f
    const-wide/16 v4, 0x0

    move-object/from16 v0, v50

    iput-wide v4, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    .line 6051
    :cond_20
    const/4 v4, 0x4

    move/from16 v0, v59

    if-ne v0, v4, :cond_24

    .line 6052
    const-string v4, "ExchangeService"

    const-string v5, "check outbox"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6053
    iget v4, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mAutoRetryTimes:I

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v4}, Lcom/android/exchange/ExchangeService;->nextWaitTimeForSendableMessages(Landroid/database/Cursor;I)J

    move-result-wide v54

    .line 6057
    .local v54, "toNextCall":J
    sget-wide v4, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

    cmp-long v4, v54, v4

    if-nez v4, :cond_21

    .line 6058
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "run EasOutboxService() now. toNextCall: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v54

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6060
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lcom/android/emailcommon/provider/EmailContent;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v29

    check-cast v29, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 6061
    .restart local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    new-instance v4, Lcom/android/exchange/EasOutboxService;

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v4, v0, v1}, Lcom/android/exchange/EasOutboxService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/exchange/ExchangeService;->startServiceThread(Lcom/android/exchange/AbstractSyncService;)V

    goto/16 :goto_3

    .line 6064
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_21
    sget-wide v4, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

    cmp-long v4, v54, v4

    if-lez v4, :cond_22

    .line 6065
    cmp-long v4, v54, v38

    if-gez v4, :cond_22

    .line 6066
    move-wide/from16 v38, v54

    .line 6067
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "run EasOutboxService() later. toNextCall = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v54

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6073
    :cond_22
    const-wide/16 v4, -0x2

    cmp-long v4, v8, v4

    if-nez v4, :cond_7

    .line 6074
    if-eqz v12, :cond_23

    iget v4, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v4, v4, 0x800

    if-nez v4, :cond_23

    .line 6076
    const-string v4, "Outbox sync interval is PUSH and sms sync is OFF...this is wrong, so, set Outbox sync interval to never"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 6078
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 6079
    .local v24, "cv":Landroid/content/ContentValues;
    const-string v4, "syncInterval"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6080
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/16 v60, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v61

    aput-object v61, v7, v60

    move-object/from16 v0, v24

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_3

    .line 6085
    .end local v24    # "cv":Landroid/content/ContentValues;
    :cond_23
    const-string v4, "requestSync for Outbox"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 6086
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lcom/android/emailcommon/provider/EmailContent;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v29

    check-cast v29, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 6087
    .restart local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4, v5}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    goto/16 :goto_3

    .line 6091
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v54    # "toNextCall":J
    :cond_24
    const-wide/16 v4, -0x2

    cmp-long v4, v8, v4

    if-nez v4, :cond_25

    .line 6092
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestSync for boxtype = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v59

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 6094
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lcom/android/emailcommon/provider/EmailContent;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v29

    check-cast v29, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 6095
    .restart local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4, v5}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    goto/16 :goto_3

    .line 6102
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_25
    const/16 v4, 0x44

    move/from16 v0, v59

    if-ne v0, v4, :cond_26

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 6104
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lcom/android/emailcommon/provider/EmailContent;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v29

    check-cast v29, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 6105
    .restart local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const/16 v4, 0xa

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4, v5}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    goto/16 :goto_3

    .line 6106
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_26
    const-wide/16 v4, 0x0

    cmp-long v4, v8, v4

    if-lez v4, :cond_7

    const-wide/16 v4, 0x5a0

    cmp-long v4, v8, v4

    if-gtz v4, :cond_7

    .line 6107
    const/16 v4, 0xa

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .local v10, "lastSync":J
    move-object/from16 v5, p0

    move-wide/from16 v6, v42

    .line 6109
    invoke-direct/range {v5 .. v11}, Lcom/android/exchange/ExchangeService;->calculateAlignedTime(JJJ)J

    move-result-wide v56

    .line 6110
    .local v56, "toNextSync":J
    const/4 v4, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v36

    .line 6111
    .local v36, "name":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v4, v56, v4

    if-gtz v4, :cond_27

    .line 6112
    const-class v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Lcom/android/emailcommon/provider/EmailContent;->getContent(Landroid/database/Cursor;Ljava/lang/Class;)Lcom/android/emailcommon/provider/EmailContent;

    move-result-object v29

    check-cast v29, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 6113
    .restart local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v4, v5}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    goto/16 :goto_3

    .line 6114
    .end local v29    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_27
    cmp-long v4, v56, v38

    if-gez v4, :cond_29

    .line 6115
    move-wide/from16 v38, v56

    .line 6116
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v4, :cond_28

    .line 6117
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Next sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v6, 0x3e8

    div-long v6, v38, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6119
    :cond_28
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Scheduled sync, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    goto/16 :goto_3

    .line 6120
    :cond_29
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v4, :cond_7

    .line 6121
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Next sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v6, 0x3e8

    div-long v6, v56, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 6125
    .end local v10    # "lastSync":J
    .end local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v13    # "accountManagerAccount":Landroid/accounts/Account;
    .end local v16    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v32    # "mailboxType":I
    .end local v36    # "name":Ljava/lang/String;
    .end local v49    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v50    # "syncStatusError":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v56    # "toNextSync":J
    :cond_2a
    move-object/from16 v0, v37

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    move-object/from16 v51, v0

    .line 6128
    .local v51, "thread":Ljava/lang/Thread;
    if-eqz v51, :cond_2c

    invoke-virtual/range {v51 .. v51}, Ljava/lang/Thread;->isAlive()Z

    move-result v4

    if-nez v4, :cond_2c

    .line 6129
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v4, :cond_2b

    .line 6130
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dead thread, mailbox released: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6133
    :cond_2b
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    .line 6134
    :try_start_e
    move-object/from16 v0, p0

    move-wide/from16 v1, v34

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    .line 6135
    monitor-exit v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 6137
    const-wide/16 v4, 0xbb8

    cmp-long v4, v38, v4

    if-lez v4, :cond_7

    .line 6138
    const-wide/16 v38, 0xbb8

    .line 6139
    :try_start_f
    const-string v4, "Clean up dead thread(s)"

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_f .. :try_end_f} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto/16 :goto_3

    .line 6135
    :catchall_4
    move-exception v4

    :try_start_10
    monitor-exit v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    throw v4

    .line 6142
    :cond_2c
    move-object/from16 v0, v37

    iget-wide v0, v0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    move-wide/from16 v44, v0

    .line 6143
    .local v44, "requestTime":J
    const-wide/16 v4, 0x0

    cmp-long v4, v44, v4

    if-lez v4, :cond_7

    .line 6144
    sub-long v52, v44, v42

    .line 6145
    .local v52, "timeToRequest":J
    const-wide/16 v4, 0x0

    cmp-long v4, v52, v4

    if-gtz v4, :cond_2e

    .line 6146
    const-wide/16 v4, 0x0

    move-object/from16 v0, v37

    iput-wide v4, v0, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 6147
    invoke-virtual/range {v37 .. v37}, Lcom/android/exchange/AbstractSyncService;->isInitialSyncThread()Z

    move-result v4

    if-nez v4, :cond_2d

    .line 6148
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkMailboxes(): call alarm() for mailbox "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    iget-wide v6, v0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 6149
    invoke-virtual/range {v37 .. v37}, Lcom/android/exchange/AbstractSyncService;->alarm()Z

    goto/16 :goto_3

    .line 6151
    :cond_2d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkMailboxes(): don\'t interrupt initial sync for mailbox "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    iget-wide v6, v0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 6153
    :cond_2e
    const-wide/16 v4, 0x0

    cmp-long v4, v44, v4

    if-lez v4, :cond_7

    cmp-long v4, v52, v38

    if-gez v4, :cond_7

    .line 6154
    const-wide/32 v4, 0xa1220

    cmp-long v4, v52, v4

    if-gez v4, :cond_30

    .line 6155
    const-wide/16 v4, 0xfa

    cmp-long v4, v52, v4

    if-gez v4, :cond_2f

    const-wide/16 v38, 0xfa

    .line 6156
    :goto_6
    const-string v4, "Sync data change"

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    goto/16 :goto_3

    :cond_2f
    move-wide/from16 v38, v52

    .line 6155
    goto :goto_6

    .line 6158
    :cond_30
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Illegal timeToRequest: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v52

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 6167
    .end local v8    # "interval":J
    .end local v18    # "acctId":J
    .end local v26    # "displayName":Ljava/lang/String;
    .end local v33    # "masterAutoSync":Z
    .end local v34    # "mid":J
    .end local v37    # "service":Lcom/android/exchange/AbstractSyncService;
    .end local v44    # "requestTime":J
    .end local v51    # "thread":Ljava/lang/Thread;
    .end local v52    # "timeToRequest":J
    .end local v59    # "type":I
    :cond_31
    if-eqz v21, :cond_32

    .line 6168
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->requery()Z

    .line 6170
    :cond_32
    :goto_7
    if-eqz v21, :cond_33

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_33

    .line 6171
    const/4 v4, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v12

    .line 6172
    .restart local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v12, :cond_32

    .line 6173
    invoke-virtual {v12}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v4

    invoke-static {v4}, Lcom/android/emailcommon/utility/SyncScheduler;->getIsPeakAndNextAlarm(Lcom/android/emailcommon/utility/SyncScheduleData;)Landroid/util/Pair;

    move-result-object v4

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_11
    .catch Ljava/lang/IllegalStateException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_1
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    move-result-wide v14

    .line 6175
    .local v14, "accountNextAlarm":J
    cmp-long v4, v14, v38

    if-gez v4, :cond_32

    .line 6176
    move-wide/from16 v38, v14

    goto :goto_7

    .line 6188
    .end local v12    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "accountNextAlarm":J
    :cond_33
    if-eqz v21, :cond_c

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_c

    .line 6189
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto/16 :goto_4
.end method

.method private checkPIMSyncSettings()V
    .locals 5

    .prologue
    .line 4597
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v3

    .line 4598
    :try_start_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 4599
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/16 v2, 0x42

    const-string v4, "com.android.contacts"

    invoke-direct {p0, v0, v2, v4}, Lcom/android/exchange/ExchangeService;->updatePIMSyncSettings(Lcom/android/emailcommon/provider/EmailContent$Account;ILjava/lang/String;)V

    .line 4602
    const/16 v2, 0x41

    const-string v4, "com.android.calendar"

    invoke-direct {p0, v0, v2, v4}, Lcom/android/exchange/ExchangeService;->updatePIMSyncSettings(Lcom/android/emailcommon/provider/EmailContent$Account;ILjava/lang/String;)V

    .line 4604
    const/4 v2, 0x0

    const-string v4, "com.android.email.provider"

    invoke-direct {p0, v0, v2, v4}, Lcom/android/exchange/ExchangeService;->updatePIMSyncSettings(Lcom/android/emailcommon/provider/EmailContent$Account;ILjava/lang/String;)V

    .line 4606
    const/16 v2, 0x43

    const-string v4, "tasks"

    invoke-direct {p0, v0, v2, v4}, Lcom/android/exchange/ExchangeService;->updatePIMSyncSettings(Lcom/android/emailcommon/provider/EmailContent$Account;ILjava/lang/String;)V

    goto :goto_0

    .line 4608
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4609
    return-void
.end method

.method public static checkServiceExist(J)Z
    .locals 6
    .param p0, "mailboxId"    # J

    .prologue
    const/4 v1, 0x0

    .line 6461
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6462
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v0, :cond_0

    .line 6470
    :goto_0
    return v1

    .line 6464
    :cond_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v2

    .line 6465
    :try_start_0
    iget-object v3, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 6466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkServiceExist: Service for mailboxID:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " already exist"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6467
    const/4 v1, 0x1

    monitor-exit v2

    goto :goto_0

    .line 6469
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private clearAlarm(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 4324
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    monitor-enter v3

    .line 4325
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 4326
    .local v1, "pi":Landroid/app/PendingIntent;
    if-eqz v1, :cond_0

    .line 4327
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 4328
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 4330
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4332
    .end local v0    # "alarmManager":Landroid/app/AlarmManager;
    :cond_0
    monitor-exit v3

    .line 4333
    return-void

    .line 4332
    .end local v1    # "pi":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private clearAlarms()V
    .locals 5

    .prologue
    .line 4354
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 4355
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    monitor-enter v4

    .line 4356
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    .line 4357
    .local v2, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 4360
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pi":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 4359
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 4360
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4361
    return-void
.end method

.method public static clearAttRqFromQueue(Lcom/android/exchange/PartRequest;)V
    .locals 1
    .param p0, "req"    # Lcom/android/exchange/PartRequest;

    .prologue
    .line 7629
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/exchange/ExchangeService;->clearAttRqFromQueue(Lcom/android/exchange/PartRequest;Z)V

    .line 7630
    return-void
.end method

.method public static clearAttRqFromQueue(Lcom/android/exchange/PartRequest;Z)V
    .locals 11
    .param p0, "req"    # Lcom/android/exchange/PartRequest;
    .param p1, "userRequest"    # Z

    .prologue
    .line 7633
    if-eqz p0, :cond_0

    iget-object v1, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    if-nez v1, :cond_1

    .line 7654
    :cond_0
    :goto_0
    return-void

    .line 7636
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 7640
    iget-object v1, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    .line 7641
    .local v8, "attachmentId":J
    sget-object v10, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    monitor-enter v10

    .line 7642
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7643
    sget-object v1, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7644
    if-eqz p1, :cond_2

    .line 7646
    :try_start_1
    sget-object v1, Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    iget-wide v2, p0, Lcom/android/exchange/PartRequest;->mMessageId:J

    iget-object v4, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v6, 0x130000

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->loadAttachmentStatus(JJII)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7653
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 7648
    :catch_0
    move-exception v0

    .line 7649
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public static clearWatchdogAlarm(J)V
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 4380
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4381
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 4382
    invoke-direct {v0, p0, p1}, Lcom/android/exchange/ExchangeService;->clearAlarm(J)V

    .line 4384
    :cond_0
    return-void
.end method

.method private static collectEasAccounts(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accounts"    # Lcom/android/exchange/ExchangeService$AccountList;

    .prologue
    const/4 v13, 0x0

    .line 2163
    const/4 v7, 0x0

    .line 2165
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 2170
    :goto_0
    const-string v0, "collect eas accounts"

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2175
    if-nez v7, :cond_1

    .line 2176
    const-string v0, "DB returned null cursor! Return null accounts list to prevent deleting accounts from AccountManager by mistake."

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    move-object p1, v13

    .line 2220
    .end local p1    # "accounts":Lcom/android/exchange/ExchangeService$AccountList;
    :cond_0
    :goto_1
    return-object p1

    .line 2167
    .restart local p1    # "accounts":Lcom/android/exchange/ExchangeService$AccountList;
    :catch_0
    move-exception v8

    .line 2168
    .local v8, "e":Ljava/lang/RuntimeException;
    const-string v0, "ExchangeService"

    const-string v1, "RuntimeException in collectEasAccounts"

    invoke-static {v0, v1, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2185
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :cond_1
    const/4 v12, 0x0

    .line 2187
    .local v12, "nullHostAuth":Z
    :cond_2
    :goto_2
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2188
    const/4 v0, 0x6

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 2189
    .local v10, "hostAuthId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-lez v0, :cond_2

    .line 2190
    invoke-static {p0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    .line 2191
    .local v9, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-nez v9, :cond_5

    .line 2192
    const/4 v12, 0x1

    .line 2207
    .end local v9    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v10    # "hostAuthId":J
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2208
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2212
    :cond_4
    if-eqz v12, :cond_0

    .line 2213
    const-string v0, "collect eas accounts: null HostAuth! This indicates DB error. Return null account list to prevent deleting accounts from AccountManager by mistake."

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    move-object p1, v13

    .line 2214
    goto :goto_1

    .line 2194
    .restart local v9    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v10    # "hostAuthId":J
    :cond_5
    :try_start_2
    iget-object v0, v9, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    const-string v1, "eas"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2195
    new-instance v6, Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-direct {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;-><init>()V

    .line 2196
    .local v6, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    invoke-virtual {v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Account;->restore(Landroid/database/Cursor;)V

    .line 2198
    iput-object v9, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 2199
    invoke-virtual {p1, v6}, Lcom/android/exchange/ExchangeService$AccountList;->add(Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2207
    .end local v6    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v9    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v10    # "hostAuthId":J
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2208
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public static deleteAccountPIMData(J)V
    .locals 6
    .param p0, "accountId"    # J

    .prologue
    .line 2272
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 2273
    .local v1, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v1, :cond_1

    .line 2288
    :cond_0
    :goto_0
    return-void

    .line 2275
    :cond_1
    const/16 v4, 0x42

    invoke-static {v1, p0, p1, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v2

    .line 2277
    .local v2, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v2, :cond_2

    .line 2278
    invoke-static {v1, v2}, Lcom/android/exchange/EasSyncService;->getServiceForMailbox(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)Lcom/android/exchange/EasSyncService;

    move-result-object v3

    .line 2279
    .local v3, "service":Lcom/android/exchange/EasSyncService;
    new-instance v0, Lcom/android/exchange/adapter/ContactsSyncAdapter;

    invoke-direct {v0, v3}, Lcom/android/exchange/adapter/ContactsSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 2280
    .local v0, "adapter":Lcom/android/exchange/adapter/ContactsSyncAdapter;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/ContactsSyncAdapter;->wipe()V

    .line 2282
    .end local v0    # "adapter":Lcom/android/exchange/adapter/ContactsSyncAdapter;
    .end local v3    # "service":Lcom/android/exchange/EasSyncService;
    :cond_2
    const/16 v4, 0x41

    invoke-static {v1, p0, p1, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v2

    .line 2283
    if-eqz v2, :cond_0

    .line 2284
    invoke-static {v1, v2}, Lcom/android/exchange/EasSyncService;->getServiceForMailbox(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)Lcom/android/exchange/EasSyncService;

    move-result-object v3

    .line 2285
    .restart local v3    # "service":Lcom/android/exchange/EasSyncService;
    new-instance v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;

    invoke-direct {v0, v3}, Lcom/android/exchange/adapter/CalendarSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 2286
    .local v0, "adapter":Lcom/android/exchange/adapter/CalendarSyncAdapter;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->wipe()V

    goto :goto_0
.end method

.method public static done(Lcom/android/exchange/AbstractSyncService;)V
    .locals 26
    .param p0, "svc"    # Lcom/android/exchange/AbstractSyncService;

    .prologue
    .line 6715
    if-nez p0, :cond_0

    .line 6716
    const-string v18, "ExchangeService : warning!! done() - svc is null.."

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6915
    :goto_0
    return-void

    .line 6720
    :cond_0
    sget-object v5, Lcom/android/exchange/AbstractSyncService;->mContext:Landroid/content/Context;

    .line 6721
    .local v5, "context":Landroid/content/Context;
    sget-object v10, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6723
    .local v10, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v10, :cond_1

    .line 6724
    const-string v18, "ExchangeService : warning!! done() - exchangeService is null.."

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6727
    :cond_1
    sget-object v19, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v19

    .line 6728
    :try_start_0
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 6729
    .local v14, "mailboxId":J
    iget-object v9, v10, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 6730
    .local v9, "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/exchange/ExchangeService$SyncError;

    .line 6731
    .local v16, "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    iget-object v13, v10, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 6732
    .local v13, "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/android/exchange/ExchangeService$SyncStatusError;

    .line 6733
    .local v17, "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    invoke-direct {v10, v14, v15}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    .line 6734
    move-object/from16 v0, p0

    iget v11, v0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 6735
    .local v11, "exitStatus":I
    invoke-static {v10, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v12

    .line 6736
    .local v12, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v12, :cond_2

    .line 6737
    monitor-exit v19

    goto :goto_0

    .line 6914
    .end local v9    # "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    .end local v11    # "exitStatus":I
    .end local v12    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v13    # "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    .end local v14    # "mailboxId":J
    .end local v16    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v17    # "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v18

    .line 6739
    .restart local v9    # "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    .restart local v11    # "exitStatus":I
    .restart local v12    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v13    # "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    .restart local v14    # "mailboxId":J
    .restart local v16    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    .restart local v17    # "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    :cond_2
    const/16 v18, 0x2

    move/from16 v0, v18

    if-eq v11, v0, :cond_4

    .line 6740
    :try_start_1
    iget-wide v6, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 6741
    .local v6, "accountId":J
    invoke-static {v10, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 6742
    .local v4, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v4, :cond_3

    .line 6743
    monitor-exit v19

    goto :goto_0

    .line 6744
    :cond_3
    const/16 v18, 0x2

    move/from16 v0, v18

    invoke-virtual {v10, v10, v0, v4}, Lcom/android/exchange/ExchangeService;->releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    if-eqz v18, :cond_4

    .line 6747
    :try_start_2
    new-instance v18, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v18

    invoke-direct {v0, v10}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyLoginSucceeded(J)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 6754
    .end local v4    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v6    # "accountId":J
    :cond_4
    :goto_1
    packed-switch v11, :pswitch_data_0

    .line 6913
    :cond_5
    :goto_2
    :pswitch_0
    :try_start_3
    const-string v18, "sync completed"

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 6914
    monitor-exit v19

    goto :goto_0

    .line 6756
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/AbstractSyncService;->hasPendingRequests()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 6759
    :cond_6
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6760
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6762
    const-class v20, Lcom/android/exchange/ExchangeService;

    monitor-enter v20
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6763
    const/16 v18, 0x0

    :try_start_4
    sput v18, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    .line 6764
    monitor-exit v20
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 6765
    if-eqz p0, :cond_5

    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v5, v0}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->isPrivateSyncOptionTypeByType(Landroid/content/Context;I)Z

    move-result v18

    if-nez v18, :cond_5

    .line 6770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move/from16 v18, v0

    const/16 v20, -0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v18, v20, v22

    if-lez v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v18, v0

    const/16 v20, 0x44

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v20, "0"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 6781
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mSyncInterval mismatch mailbox.syncInterval:"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " account.syncInterval:"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move/from16 v20, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6784
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 6785
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v18, "syncInterval"

    const/16 v20, -0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6786
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    sget-object v20, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v21, "_id=?"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 6764
    .end local v8    # "cv":Landroid/content/ContentValues;
    :catchall_1
    move-exception v18

    :try_start_6
    monitor-exit v20
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v18

    .line 6799
    :pswitch_2
    if-eqz v16, :cond_8

    .line 6802
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    move/from16 v18, v0

    if-eqz v18, :cond_7

    .line 6803
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6804
    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6806
    :cond_7
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6807
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " held for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "ms"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6809
    :cond_8
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v10, v11, v1}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6810
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " added to syncErrorMap, hold for 15s"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6815
    :pswitch_3
    if-eqz v17, :cond_a

    .line 6818
    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/android/exchange/ExchangeService$SyncStatusError;->autoRecover:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    .line 6819
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/android/exchange/ExchangeService$SyncStatusError;->autoRecover:Z

    .line 6820
    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncStatusError;->DEFAULT_HOLD_DELAY:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    .line 6822
    :cond_9
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/ExchangeService$SyncStatusError;->escalate()V

    .line 6823
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " held for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "ms"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6825
    :cond_a
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncStatusError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v10, v11, v1}, Lcom/android/exchange/ExchangeService$SyncStatusError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6826
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " added to syncStatusErrorMap, hold for 15s"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 6838
    :pswitch_4
    :try_start_8
    new-instance v18, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v18

    invoke-direct {v0, v10}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    iget-wide v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyLoginFailed(J)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 6845
    :goto_3
    if-eqz v16, :cond_c

    .line 6846
    :try_start_9
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    move/from16 v18, v0

    if-nez v18, :cond_b

    .line 6847
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6848
    const-wide/32 v20, 0xa4cb80

    move-wide/from16 v0, v20

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6850
    :cond_b
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6851
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " held for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "ms"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "due to exception: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v20, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6854
    :cond_c
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v10, v11, v1, v2}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6856
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " added to syncErrorMap, hold for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v20, 0xa4cb80

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6861
    :pswitch_5
    if-eqz v16, :cond_d

    .line 6862
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6867
    :goto_4
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " added to syncErrorMap, hold for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v20, 0x1b7740

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "due to exception: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v20, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6864
    :cond_d
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v21, 0x1b7740

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v10, v11, v1, v2}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IIZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 6875
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 6876
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "done(): setting the provisioning error for mAccount: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6878
    sget-object v18, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6883
    :cond_e
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v10, v11, v1}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 6888
    :pswitch_7
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v21, 0x75300

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v10, v11, v1, v2}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IIZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6890
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Account is blocked account Id:"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " Sync is put on hold for : "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v20, 0x75300

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " minutes"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6896
    :pswitch_8
    if-eqz v16, :cond_10

    .line 6897
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    move/from16 v18, v0

    if-nez v18, :cond_f

    .line 6898
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6899
    const-wide/32 v20, 0xa4cb80

    move-wide/from16 v0, v20

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6901
    :cond_f
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6902
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " held for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "ms"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "due to exception: "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v20, 0xb

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6905
    :cond_10
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    new-instance v20, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v0, v10, v11, v1, v2}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZZ)V

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6907
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, " added to syncErrorMap for AbstractSyncService.EXIT_TOOMANY, hold for "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v20, 0xa4cb80

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2

    .line 6839
    :catch_0
    move-exception v18

    goto/16 :goto_3

    .line 6748
    .restart local v4    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v6    # "accountId":J
    :catch_1
    move-exception v18

    goto/16 :goto_1

    .line 6754
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V
    .locals 15
    .param p0, "svc"    # Lcom/android/exchange/AbstractSyncService;

    .prologue
    .line 6533
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6534
    .local v1, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v1, :cond_0

    .line 6704
    :goto_0
    return-void

    .line 6536
    :cond_0
    sget-object v11, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v11

    .line 6537
    :try_start_0
    iget-wide v6, p0, Lcom/android/exchange/AbstractSyncService;->mMailboxId:J

    .line 6538
    .local v6, "mailboxId":J
    iget-object v0, v1, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 6539
    .local v0, "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/exchange/ExchangeService$SyncError;

    .line 6540
    .local v8, "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    iget-object v5, v1, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 6541
    .local v5, "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/exchange/ExchangeService$SyncStatusError;

    .line 6546
    .local v9, "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    iget v2, p0, Lcom/android/exchange/AbstractSyncService;->mExitStatus:I

    .line 6547
    .local v2, "exitStatus":I
    invoke-static {v1, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 6548
    .local v3, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v3, :cond_1

    .line 6549
    monitor-exit v11

    goto :goto_0

    .line 6703
    .end local v0    # "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    .end local v2    # "exitStatus":I
    .end local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v5    # "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    .end local v6    # "mailboxId":J
    .end local v8    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v9    # "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 6551
    .restart local v0    # "errorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    .restart local v2    # "exitStatus":I
    .restart local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v5    # "statuserrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncStatusError;>;"
    .restart local v6    # "mailboxId":J
    .restart local v8    # "syncError":Lcom/android/exchange/ExchangeService$SyncError;
    .restart local v9    # "syncStatusError":Lcom/android/exchange/ExchangeService$SyncStatusError;
    :cond_1
    :try_start_1
    sget-wide v12, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_GALSEARCH:J

    cmp-long v10, v6, v12

    if-eqz v10, :cond_d

    .line 6552
    packed-switch v2, :pswitch_data_0

    .line 6694
    :cond_2
    :goto_1
    :pswitch_0
    const-string v10, "sync completed"

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 6703
    :cond_3
    :goto_2
    monitor-exit v11

    goto :goto_0

    .line 6557
    :pswitch_1
    const-class v12, Lcom/android/exchange/ExchangeService;

    monitor-enter v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 6558
    const/4 v10, 0x0

    :try_start_2
    sput v10, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    .line 6559
    monitor-exit v12

    goto :goto_1

    :catchall_1
    move-exception v10

    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v10

    .line 6563
    :pswitch_2
    if-eqz v8, :cond_5

    .line 6566
    iget-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    if-eqz v10, :cond_4

    .line 6567
    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6568
    iget-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->DEFAULT_HOLD_DELAY:J

    iput-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6570
    :cond_4
    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6571
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " held for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "ms"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto :goto_1

    .line 6573
    :cond_5
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v13, 0x0

    invoke-direct {v12, v1, v2, v13}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6575
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " added to syncErrorMap, hold for 15s"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto :goto_1

    .line 6579
    :pswitch_3
    if-eqz v9, :cond_7

    .line 6582
    iget-boolean v10, v9, Lcom/android/exchange/ExchangeService$SyncStatusError;->autoRecover:Z

    if-eqz v10, :cond_6

    .line 6583
    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/android/exchange/ExchangeService$SyncStatusError;->autoRecover:Z

    .line 6584
    iget-wide v12, v9, Lcom/android/exchange/ExchangeService$SyncStatusError;->DEFAULT_HOLD_DELAY:J

    iput-wide v12, v9, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    .line 6586
    :cond_6
    invoke-virtual {v9}, Lcom/android/exchange/ExchangeService$SyncStatusError;->escalate()V

    .line 6587
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " held for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v9, Lcom/android/exchange/ExchangeService$SyncStatusError;->holdDelay:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "ms"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6589
    :cond_7
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncStatusError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v13, 0x0

    invoke-direct {v12, v1, v2, v13}, Lcom/android/exchange/ExchangeService$SyncStatusError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    invoke-virtual {v5, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6590
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " added to syncStatusErrorMap, hold for 15s"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6597
    :pswitch_4
    iget-object v10, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v10, :cond_2

    .line 6598
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "setting the provisioning error for mAccount: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6599
    sget-object v10, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-object v10, v10, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    iget-object v13, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v10, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6600
    const-wide/16 v12, 0x0

    cmp-long v10, v6, v12

    if-lez v10, :cond_2

    .line 6603
    invoke-static {v1, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v4

    .line 6605
    .local v4, "m1":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v4, :cond_2

    iget v10, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x44

    if-eq v10, v12, :cond_2

    .line 6606
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v13, 0x1

    invoke-direct {v12, v1, v2, v13}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6608
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mailbox: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " put to hold due to security failure"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 6629
    .end local v4    # "m1":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :pswitch_5
    :try_start_4
    new-instance v10, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v10, v1}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v10, v12, v13}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyLoginFailed(J)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 6637
    :goto_3
    if-eqz v8, :cond_9

    .line 6638
    :try_start_5
    iget-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    if-nez v10, :cond_8

    .line 6639
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6640
    const-wide/32 v12, 0xa4cb80

    iput-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6642
    :cond_8
    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6643
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " held for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "ms"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "due to exception: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v12, 0x2

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6646
    :cond_9
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-direct {v12, v1, v2, v13, v14}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6648
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " added to syncErrorMap, hold for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v12, 0xa4cb80

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6653
    :pswitch_6
    if-eqz v8, :cond_a

    .line 6654
    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6659
    :goto_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " added to syncErrorMap, hold for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v12, 0x1b7740

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "due to exception: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v12, 0x3

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6656
    :cond_a
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v13, 0x1b7740

    const/4 v14, 0x0

    invoke-direct {v12, v1, v2, v13, v14}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IIZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 6666
    :pswitch_7
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const v13, 0x75300

    const/4 v14, 0x0

    invoke-direct {v12, v1, v2, v13, v14}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IIZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6669
    iget-object v10, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v10, :cond_2

    .line 6670
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Account is blocked account Id:"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, p0, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " Sync is put on hold for : "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v12, 0x75300

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " minutes"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6677
    :pswitch_8
    if-eqz v8, :cond_c

    .line 6678
    iget-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    if-nez v10, :cond_b

    .line 6679
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/android/exchange/ExchangeService$SyncError;->autoRecover:Z

    .line 6680
    const-wide/32 v12, 0xa4cb80

    iput-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    .line 6682
    :cond_b
    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$SyncError;->escalate()V

    .line 6683
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " held for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v8, Lcom/android/exchange/ExchangeService$SyncError;->holdDelay:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "ms"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "due to exception: "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v12, 0xb

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6686
    :cond_c
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    new-instance v12, Lcom/android/exchange/ExchangeService$SyncError;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-direct {v12, v1, v2, v13, v14}, Lcom/android/exchange/ExchangeService$SyncError;-><init>(Lcom/android/exchange/ExchangeService;IZZ)V

    invoke-virtual {v0, v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6688
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " added to syncErrorMap for AbstractSyncService.EXIT_TOOMANY, hold for "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const v12, 0xa4cb80

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6695
    :cond_d
    sget-wide v12, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_GALSEARCH:J

    cmp-long v10, v6, v12

    if-nez v10, :cond_3

    if-nez v2, :cond_3

    .line 6698
    const-class v12, Lcom/android/exchange/ExchangeService;

    monitor-enter v12
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 6699
    const/4 v10, 0x0

    :try_start_6
    sput v10, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    .line 6700
    monitor-exit v12

    goto/16 :goto_2

    :catchall_2
    move-exception v10

    monitor-exit v12
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    throw v10
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 6631
    :catch_0
    move-exception v10

    goto/16 :goto_3

    .line 6552
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static errorlog(Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 3424
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 3425
    const-string v0, "ExchangeService"

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3427
    :cond_0
    return-void
.end method

.method public static getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;
    .locals 4
    .param p0, "accountId"    # J

    .prologue
    .line 3000
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3001
    .local v1, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v1, :cond_0

    .line 3002
    sget-object v0, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    .line 3003
    .local v0, "accountList":Lcom/android/exchange/ExchangeService$AccountList;
    monitor-enter v0

    .line 3004
    :try_start_0
    invoke-virtual {v0, p0, p1}, Lcom/android/exchange/ExchangeService$AccountList;->getById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    monitor-exit v0

    .line 3007
    .end local v0    # "accountList":Lcom/android/exchange/ExchangeService$AccountList;
    :goto_0
    return-object v2

    .line 3005
    .restart local v0    # "accountList":Lcom/android/exchange/ExchangeService$AccountList;
    :catchall_0
    move-exception v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 3007
    .end local v0    # "accountList":Lcom/android/exchange/ExchangeService$AccountList;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getAccountList()Lcom/android/exchange/ExchangeService$AccountList;
    .locals 2

    .prologue
    .line 2986
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v0, :cond_0

    .line 2989
    sget-object v1, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v1

    .line 2990
    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    sget-object v0, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-exit v1

    .line 2995
    :goto_0
    return-object v0

    .line 2992
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2995
    :cond_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->EMPTY_ACCOUNT_LIST:Lcom/android/exchange/ExchangeService$AccountList;

    goto :goto_0
.end method

.method public static getAliasFromMap(J)Ljava/lang/String;
    .locals 6
    .param p0, "tId"    # J

    .prologue
    .line 7334
    const/4 v1, 0x0

    .line 7335
    .local v1, "alias":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;

    monitor-enter v4

    .line 7337
    :try_start_0
    sget-object v3, Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7341
    :goto_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 7342
    const-string v3, "SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Returning Alias "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 7343
    return-object v1

    .line 7338
    :catch_0
    move-exception v2

    .line 7339
    .local v2, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0

    .line 7341
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method private getAppRestrictBackground()Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 4620
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/net/NetworkPolicyManager;->from(Landroid/content/Context;)Landroid/net/NetworkPolicyManager;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/ExchangeService;->mPolicyManager:Landroid/net/NetworkPolicyManager;

    .line 4622
    :try_start_0
    const-string v2, "com.android.exchange"

    .line 4623
    .local v2, "package_name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 4624
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v7, 0x0

    invoke-virtual {v3, v2, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4630
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget v4, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 4631
    .local v4, "uid":I
    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mPolicyManager:Landroid/net/NetworkPolicyManager;

    invoke-virtual {v7, v4}, Landroid/net/NetworkPolicyManager;->getUidPolicy(I)I

    move-result v5

    .line 4632
    .local v5, "uidPolicy":I
    and-int/lit8 v7, v5, 0x1

    if-eqz v7, :cond_0

    const/4 v6, 0x1

    .end local v2    # "package_name":Ljava/lang/String;
    .end local v4    # "uid":I
    .end local v5    # "uidPolicy":I
    :cond_0
    :goto_0
    return v6

    .line 4625
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 4626
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v3, 0x0

    .line 4627
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 4628
    .restart local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    goto :goto_0
.end method

.method public static declared-synchronized getClientConnectionManager(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 13
    .param p0, "ssl"    # Z
    .param p1, "port"    # I

    .prologue
    const/16 v7, 0x50

    const/4 v12, 0x1

    const/4 v8, 0x0

    const/16 v6, 0x1bb

    .line 3552
    const-class v9, Lcom/android/exchange/ExchangeService;

    monitor-enter v9

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getClientConnectionManager ssl = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", port = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3554
    if-eqz p0, :cond_0

    const/high16 v8, 0x10000

    :cond_0
    add-int v0, v8, p1

    .line 3555
    .local v0, "key":I
    sget-object v8, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3556
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-nez v1, :cond_3

    .line 3560
    sget v8, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    if-le v8, v12, :cond_1

    .line 3561
    const-string v8, "Shutting down process to unblock threads"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3562
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    invoke-static {v8}, Landroid/os/Process;->killProcess(I)V

    .line 3567
    :cond_1
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3568
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v5, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3570
    .local v5, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3571
    .local v4, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3573
    if-lez p1, :cond_2

    const v8, 0xffff

    if-le p1, v8, :cond_4

    .line 3574
    :cond_2
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v10, 0x50

    invoke-direct {v6, v7, v8, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3575
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3577
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "httpts"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3587
    .end local p1    # "port":I
    :goto_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3588
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    const-string v6, "http.conn-manager.max-total"

    const/16 v7, 0x19

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3589
    const-string v6, "http.conn-manager.max-per-route"

    sget-object v7, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3590
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3591
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3594
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_3
    monitor-exit v9

    return-object v1

    .line 3579
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_4
    :try_start_1
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v11

    if-eqz p0, :cond_5

    :goto_1
    invoke-direct {v8, v10, v11, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3581
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "https"

    if-eqz p0, :cond_6

    move v7, p1

    :goto_2
    invoke-direct {v8, v10, v5, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3583
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "httpts"

    if-eqz p0, :cond_7

    .end local p1    # "port":I
    :goto_3
    invoke-direct {v7, v8, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3552
    .end local v0    # "key":I
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6

    .restart local v0    # "key":I
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_5
    move v7, p1

    .line 3579
    goto :goto_1

    :cond_6
    move v7, v6

    .line 3581
    goto :goto_2

    :cond_7
    move p1, v6

    .line 3583
    goto :goto_3
.end method

.method public static declared-synchronized getClientConnectionManagerForAttachmentDownLoads(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 12
    .param p0, "ssl"    # Z
    .param p1, "port"    # I

    .prologue
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v6, 0x1bb

    .line 3736
    const-class v9, Lcom/android/exchange/ExchangeService;

    monitor-enter v9

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getClientConnectionManagerForAttachmentDownLoads ssl = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", port = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3738
    if-eqz p0, :cond_0

    const/high16 v8, 0x10000

    :cond_0
    add-int v0, v8, p1

    .line 3739
    .local v0, "key":I
    sget-object v8, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3740
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-nez v1, :cond_2

    .line 3741
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3742
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v5, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3744
    .local v5, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3745
    .local v4, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3747
    if-lez p1, :cond_1

    const v8, 0xffff

    if-le p1, v8, :cond_4

    .line 3748
    :cond_1
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v10, 0x50

    invoke-direct {v6, v7, v8, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3749
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3751
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "httpts"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3761
    .end local p1    # "port":I
    :goto_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3762
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    const-string v6, "http.conn-manager.max-total"

    const/16 v7, 0x19

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3763
    const-string v6, "http.conn-manager.max-per-route"

    sget-object v7, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3765
    const-string v6, "http.conn-manager.timeout"

    const/16 v7, 0x4e20

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3766
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3767
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v6, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3769
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_2
    sget-boolean v6, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z

    if-eqz v6, :cond_3

    .line 3770
    sget v6, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    .line 3771
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "adShutdownCount ADD = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3774
    :cond_3
    monitor-exit v9

    return-object v1

    .line 3753
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_4
    :try_start_1
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v11

    if-eqz p0, :cond_5

    :goto_1
    invoke-direct {v8, v10, v11, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3755
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "https"

    if-eqz p0, :cond_6

    move v7, p1

    :goto_2
    invoke-direct {v8, v10, v5, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3757
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "httpts"

    if-eqz p0, :cond_7

    .end local p1    # "port":I
    :goto_3
    invoke-direct {v7, v8, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3736
    .end local v0    # "key":I
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6

    .restart local v0    # "key":I
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_5
    move v7, p1

    .line 3753
    goto :goto_1

    :cond_6
    move v7, v6

    .line 3755
    goto :goto_2

    :cond_7
    move p1, v6

    .line 3757
    goto :goto_3
.end method

.method public static declared-synchronized getClientConnectionManagerForLoadMore(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 12
    .param p0, "ssl"    # Z
    .param p1, "port"    # I

    .prologue
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v6, 0x1bb

    .line 3600
    const-class v9, Lcom/android/exchange/ExchangeService;

    monitor-enter v9

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getClientConnectionManagerForLoadMore ssl = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", port = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3602
    if-eqz p0, :cond_0

    const/high16 v8, 0x10000

    :cond_0
    add-int v0, v8, p1

    .line 3603
    .local v0, "key":I
    sget-object v8, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3604
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-nez v1, :cond_2

    .line 3625
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3626
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v5, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3628
    .local v5, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3629
    .local v4, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3631
    if-lez p1, :cond_1

    const v8, 0xffff

    if-le p1, v8, :cond_3

    .line 3632
    :cond_1
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v10, 0x50

    invoke-direct {v6, v7, v8, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3633
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3635
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "httpts"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3645
    .end local p1    # "port":I
    :goto_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3646
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    const-string v6, "http.conn-manager.max-total"

    const/16 v7, 0x19

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3647
    const-string v6, "http.conn-manager.max-per-route"

    sget-object v7, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3649
    const-string v6, "http.conn-manager.timeout"

    const/16 v7, 0x4e20

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3650
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3651
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v6, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3654
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_2
    monitor-exit v9

    return-object v1

    .line 3637
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_3
    :try_start_1
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v11

    if-eqz p0, :cond_4

    :goto_1
    invoke-direct {v8, v10, v11, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3639
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "https"

    if-eqz p0, :cond_5

    move v7, p1

    :goto_2
    invoke-direct {v8, v10, v5, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3641
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "httpts"

    if-eqz p0, :cond_6

    .end local p1    # "port":I
    :goto_3
    invoke-direct {v7, v8, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3600
    .end local v0    # "key":I
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6

    .restart local v0    # "key":I
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_4
    move v7, p1

    .line 3637
    goto :goto_1

    :cond_5
    move v7, v6

    .line 3639
    goto :goto_2

    :cond_6
    move p1, v6

    .line 3641
    goto :goto_3
.end method

.method public static declared-synchronized getClientConnectionManagerForSSLValidation(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 12
    .param p0, "ssl"    # Z
    .param p1, "port"    # I

    .prologue
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v6, 0x1bb

    .line 3810
    const-class v9, Lcom/android/exchange/ExchangeService;

    monitor-enter v9

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getClientConnectionManager ssl = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", port = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3812
    if-eqz p0, :cond_0

    const/high16 v8, 0x10000

    :cond_0
    add-int v0, v8, p1

    .line 3813
    .local v0, "key":I
    sget-object v8, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3814
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-nez v1, :cond_2

    .line 3818
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3819
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v5, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3821
    .local v5, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3822
    .local v4, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3824
    if-lez p1, :cond_1

    const v8, 0xffff

    if-le p1, v8, :cond_4

    .line 3825
    :cond_1
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v10, 0x50

    invoke-direct {v6, v7, v8, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3827
    if-eqz p0, :cond_3

    .line 3828
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3847
    .end local p1    # "port":I
    :goto_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3848
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    const-string v6, "http.conn-manager.max-total"

    const/16 v7, 0x19

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3849
    const-string v6, "http.conn-manager.max-per-route"

    sget-object v7, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3850
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3851
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v6, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3854
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_2
    monitor-exit v9

    return-object v1

    .line 3830
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_3
    :try_start_1
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3810
    .end local v0    # "key":I
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local p1    # "port":I
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6

    .line 3834
    .restart local v0    # "key":I
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_4
    :try_start_2
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v11

    if-eqz p0, :cond_5

    :goto_1
    invoke-direct {v8, v10, v11, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3837
    if-eqz p0, :cond_7

    .line 3838
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "https"

    if-eqz p0, :cond_6

    .end local p1    # "port":I
    :goto_2
    invoke-direct {v7, v8, v5, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    goto :goto_0

    .restart local p1    # "port":I
    :cond_5
    move v7, p1

    .line 3834
    goto :goto_1

    :cond_6
    move p1, v6

    .line 3838
    goto :goto_2

    .line 3840
    :cond_7
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "https"

    if-eqz p0, :cond_8

    .end local p1    # "port":I
    :goto_3
    invoke-direct {v7, v8, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .restart local p1    # "port":I
    :cond_8
    move p1, v6

    goto :goto_3
.end method

.method public static declared-synchronized getClientConnectionManagerForSendMessage(JZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 12
    .param p0, "accountId"    # J
    .param p2, "ssl"    # Z
    .param p3, "port"    # I

    .prologue
    const/16 v6, 0x50

    const/16 v5, 0x1bb

    .line 3661
    const-class v7, Lcom/android/exchange/ExchangeService;

    monitor-enter v7

    :try_start_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getClientConnectionManagerForSendMessage ssl = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", port = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3662
    const/4 v2, 0x0

    .line 3663
    .local v2, "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v8, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    if-nez v8, :cond_0

    .line 3664
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    sput-object v8, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    .line 3667
    :cond_0
    sget-object v8, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    if-eqz v8, :cond_2

    .line 3668
    sget-object v8, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 3693
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3694
    .local v1, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3696
    .local v4, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v3, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v3, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3697
    .local v3, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v3, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3699
    if-lez p3, :cond_1

    const v8, 0xffff

    if-le p3, v8, :cond_3

    .line 3700
    :cond_1
    new-instance v5, Lorg/apache/http/conn/scheme/Scheme;

    const-string v6, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v9, 0x50

    invoke-direct {v5, v6, v8, v9}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v5}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3701
    new-instance v5, Lorg/apache/http/conn/scheme/Scheme;

    const-string v6, "https"

    const/16 v8, 0x1bb

    invoke-direct {v5, v6, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v5}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3703
    new-instance v5, Lorg/apache/http/conn/scheme/Scheme;

    const-string v6, "httpts"

    const/16 v8, 0x1bb

    invoke-direct {v5, v6, v3, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v5}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3713
    .end local p3    # "port":I
    :goto_0
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3714
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const-string v5, "http.conn-manager.max-total"

    const/16 v6, 0x19

    invoke-interface {v0, v5, v6}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3715
    const-string v5, "http.conn-manager.max-per-route"

    sget-object v6, Lcom/android/exchange/ExchangeService;->sConnPerRoute:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v0, v5, v6}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3717
    const-string v5, "http.conn-manager.timeout"

    const/16 v6, 0x4e20

    invoke-interface {v0, v5, v6}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3718
    new-instance v2, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v2, v0, v1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3719
    .restart local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v5, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3725
    .end local v0    # "params":Lorg/apache/http/params/HttpParams;
    .end local v1    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v3    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v4    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_2
    :goto_1
    monitor-exit v7

    return-object v2

    .line 3705
    .restart local v1    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v3    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v4    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p3    # "port":I
    :cond_3
    :try_start_1
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v9, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v10

    if-eqz p2, :cond_4

    :goto_2
    invoke-direct {v8, v9, v10, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3707
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v9, "https"

    if-eqz p2, :cond_5

    move v6, p3

    :goto_3
    invoke-direct {v8, v9, v4, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3709
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "httpts"

    if-eqz p2, :cond_6

    .end local p3    # "port":I
    :goto_4
    invoke-direct {v6, v8, v3, p3}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3661
    .end local v1    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v4    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :catchall_0
    move-exception v5

    monitor-exit v7

    throw v5

    .restart local v1    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v4    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p3    # "port":I
    :cond_4
    move v6, p3

    .line 3705
    goto :goto_2

    :cond_5
    move v6, v5

    .line 3707
    goto :goto_3

    :cond_6
    move p3, v5

    .line 3709
    goto :goto_4

    .line 3721
    .end local v1    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v3    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v4    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_7
    :try_start_2
    sget-object v5, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    check-cast v2, Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v2    # "returnManager":Lorg/apache/http/conn/ClientConnectionManager;
    goto :goto_1
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 6950
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    return-object v0
.end method

.method public static declared-synchronized getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3446
    const-class v2, Lcom/android/exchange/ExchangeService;

    monitor-enter v2

    if-nez p0, :cond_0

    .line 3447
    :try_start_0
    sget-object p0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3448
    :cond_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_5

    .line 3451
    :try_start_1
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isNonPhone(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3453
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "hardware_id"

    invoke-static {v1, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3455
    .local v0, "hardwareId":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 3456
    const-string v1, "ro.serialno"

    const-string v3, "unknown"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3457
    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3458
    const/4 v0, 0x0

    .line 3460
    :cond_1
    if-nez v0, :cond_3

    .line 3461
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_2

    .line 3462
    const-string v1, "EAS"

    const-string v3, "Wifi Model, hardwareId : null "

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3463
    :cond_2
    const/4 v1, 0x0

    .line 3479
    .end local v0    # "hardwareId":Ljava/lang/String;
    :goto_0
    monitor-exit v2

    return-object v1

    .line 3465
    .restart local v0    # "hardwareId":Ljava/lang/String;
    :cond_3
    :try_start_2
    new-instance v1, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v1, p0}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/android/emailcommon/service/AccountServiceProxy;->getDeviceIdForWifi(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;

    .line 3466
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_4

    .line 3467
    const-string v1, "EAS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wifi Model, hardware_id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3473
    .end local v0    # "hardwareId":Ljava/lang/String;
    :cond_4
    :goto_1
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_5

    .line 3474
    const-string v1, "EAS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Receive deviceId from Email app: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3479
    :cond_5
    :goto_2
    :try_start_3
    sget-object v1, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 3471
    :cond_6
    :try_start_4
    new-instance v1, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v1, p0}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/android/emailcommon/service/AccountServiceProxy;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/exchange/ExchangeService;->sDeviceId:Ljava/lang/String;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 3475
    :catch_0
    move-exception v1

    goto :goto_2

    .line 3446
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private getDownloadServiceFromMap(J)Lcom/android/exchange/EasDownLoadAttachmentSvc;
    .locals 3
    .param p1, "attachmentId"    # J

    .prologue
    .line 7609
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 7610
    const/4 v0, 0x0

    .line 7613
    :goto_0
    return-object v0

    .line 7612
    :cond_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    monitor-enter v1

    .line 7613
    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;

    monitor-exit v1

    goto :goto_0

    .line 7614
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getEasAccountSelector()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3022
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3023
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    if-eqz v1, :cond_0

    .line 3024
    iget-object v1, v0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    invoke-virtual {v1}, Lcom/android/exchange/ExchangeService$AccountObserver;->getAccountKeyWhere()Ljava/lang/String;

    move-result-object v1

    .line 3026
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getElmSvc()Lcom/android/exchange/EasLoadMoreSvc;
    .locals 1

    .prologue
    .line 7324
    sget-object v0, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    return-object v0
.end method

.method public static getElmSvcMsg()J
    .locals 2

    .prologue
    .line 7328
    sget-object v0, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    iget-object v0, v0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    return-wide v0
.end method

.method public static getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxType"    # I

    .prologue
    const/4 v0, 0x0

    .line 7560
    const/4 v6, 0x0

    .line 7561
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 7562
    .local v7, "serverId":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 7563
    const-string v1, "ExchangeService"

    const-string v2, "getFolderServerId context is NULL"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 7579
    :goto_0
    return-object v0

    .line 7567
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "serverId"

    aput-object v4, v2, v3

    const-string v3, "accountKey=? AND type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 7573
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7574
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 7578
    :cond_1
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 7579
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    goto :goto_0

    .line 7578
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 7579
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getMailBoxIdRIC(J)Ljava/lang/Long;
    .locals 13
    .param p1, "mAccountKey"    # J

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v5, 0x1

    .line 855
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 856
    .local v7, "RicBoxId":Ljava/lang/Long;
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "accountKey"

    aput-object v0, v2, v10

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "type"

    aput-object v0, v2, v11

    .line 860
    .local v2, "MAILBOX_PROJECTION":[Ljava/lang/String;
    const-string v3, "accountKey=? AND type=?"

    .line 861
    .local v3, "selection":Ljava/lang/String;
    const/16 v6, 0x61

    .line 862
    .local v6, "RiCBoxtype":I
    new-array v4, v11, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 866
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 869
    .local v9, "ric":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 871
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 872
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 879
    :cond_0
    if-eqz v9, :cond_1

    .line 880
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 883
    :cond_1
    :goto_0
    return-object v7

    .line 875
    :catch_0
    move-exception v8

    .line 876
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "MailboxIdRIC"

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 879
    if-eqz v9, :cond_1

    .line 880
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 879
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_2

    .line 880
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getRetryCount(JI)I
    .locals 2
    .param p0, "mailboxId"    # J
    .param p2, "reason"    # I

    .prologue
    .line 3147
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3148
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 3149
    invoke-virtual {v0, p0, p1, p2}, Lcom/android/exchange/ExchangeService;->getStatusErrorCount(JI)I

    move-result v1

    .line 3151
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getServiceLogger()Lcom/android/exchange/ServiceLogger;
    .locals 1

    .prologue
    .line 7366
    sget-object v0, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    return-object v0
.end method

.method public static getStatusChangeCount(Ljava/lang/String;)I
    .locals 3
    .param p0, "status"    # Ljava/lang/String;

    .prologue
    .line 6942
    const/4 v2, 0x5

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 6943
    .local v1, "s":Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 6945
    .end local v1    # "s":Ljava/lang/String;
    :goto_0
    return v2

    .line 6944
    :catch_0
    move-exception v0

    .line 6945
    .local v0, "e":Ljava/lang/RuntimeException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getStatusType(Ljava/lang/String;)I
    .locals 1
    .param p0, "status"    # Ljava/lang/String;

    .prologue
    .line 6925
    if-nez p0, :cond_0

    .line 6926
    const/4 v0, -0x1

    .line 6928
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    goto :goto_0
.end method

.method public static getSubFolderContactAccountObjectFromMap(J)Ljava/lang/Object;
    .locals 6
    .param p0, "actId"    # J

    .prologue
    .line 7542
    const-string v2, "ExchangeService"

    const-string v3, "getSubFolderContactAccountObjectFromMap"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 7543
    sget-object v3, Lcom/android/exchange/ExchangeService;->CONTACT_SUBFOLDER_MAP_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 7544
    :try_start_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->mSubFolderContactAccountObjectMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 7548
    .local v1, "obj":Ljava/lang/Object;
    if-nez v1, :cond_0

    .line 7549
    const-string v2, "ExchangeService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Contact SubFolder, Creating object for act "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 7550
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 7552
    .local v0, "newObj":Ljava/lang/Object;
    sget-object v2, Lcom/android/exchange/ExchangeService;->mSubFolderContactAccountObjectMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7553
    monitor-exit v3

    .line 7555
    .end local v0    # "newObj":Ljava/lang/Object;
    :goto_0
    return-object v0

    :cond_0
    monitor-exit v3

    move-object v0, v1

    goto :goto_0

    .line 7556
    .end local v1    # "obj":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private isAlreadyDownloding(J)Z
    .locals 5
    .param p1, "attachmentId"    # J

    .prologue
    .line 7657
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 7658
    const/4 v0, 0x0

    .line 7662
    :goto_0
    return v0

    .line 7660
    :cond_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    monitor-enter v2

    .line 7661
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 7662
    .local v0, "ret":Z
    monitor-exit v2

    goto :goto_0

    .line 7663
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private isCalendarEnabled(J)Z
    .locals 7
    .param p1, "accountId"    # J

    .prologue
    const/4 v1, 0x1

    .line 2739
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$CalendarObserver;

    .line 2740
    .local v0, "observer":Lcom/android/exchange/ExchangeService$CalendarObserver;
    if-eqz v0, :cond_0

    .line 2741
    iget-wide v2, v0, Lcom/android/exchange/ExchangeService$CalendarObserver;->mSyncEvents:J

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 2746
    :cond_0
    :goto_0
    return v1

    .line 2741
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static kick(Ljava/lang/String;)V
    .locals 3
    .param p0, "reason"    # Ljava/lang/String;

    .prologue
    .line 6479
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6480
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 6481
    monitor-enter v0

    .line 6483
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, v0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    .line 6484
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 6485
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6487
    :cond_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 6488
    sget-object v2, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    monitor-enter v2

    .line 6489
    :try_start_1
    sget-object v1, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 6490
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 6492
    :cond_1
    return-void

    .line 6485
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 6490
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 3392
    const-string v0, "ExchangeService"

    invoke-static {v0, p0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 3393
    return-void
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 3396
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 3397
    invoke-static {p0, p1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3399
    :cond_0
    return-void
.end method

.method private logSyncHolds()V
    .locals 12

    .prologue
    .line 3109
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v3, :cond_2

    .line 3110
    const-string v3, "Sync holds:"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 3112
    .local v6, "time":J
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3113
    .local v4, "mailboxId":J
    invoke-static {p0, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v2

    .line 3114
    .local v2, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v2, :cond_1

    .line 3115
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Mailbox "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " no longer exists"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3117
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncError;

    .line 3118
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v0, :cond_0

    .line 3119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Mailbox "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v8, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ", error = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v8, v0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ", fatal = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v8, v0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3121
    iget-wide v8, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-lez v3, :cond_0

    .line 3122
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Hold ends in "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v8, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    sub-long/2addr v8, v6

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, "s"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3128
    .end local v0    # "error":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v4    # "mailboxId":J
    .end local v6    # "time":J
    :cond_2
    return-void
.end method

.method public static declared-synchronized newClientConnectionManager(ZI)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 12
    .param p0, "ssl"    # Z
    .param p1, "port"    # I

    .prologue
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v6, 0x1bb

    .line 3507
    const-class v9, Lcom/android/exchange/ExchangeService;

    monitor-enter v9

    :try_start_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "newClientConnectionManager ssl = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", port = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3509
    if-eqz p0, :cond_0

    const/high16 v8, 0x10000

    :cond_0
    add-int v0, v8, p1

    .line 3510
    .local v0, "key":I
    sget-object v8, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3511
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    if-nez v1, :cond_2

    .line 3515
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 3516
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v5, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x0

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3518
    .local v5, "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    new-instance v4, Lcom/android/emailcommon/utility/SSLSocketFactory;

    const/4 v8, 0x1

    invoke-static {v8}, Lcom/android/exchange/cba/SSLUtils;->getSSLSocketFactory(Z)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 3519
    .local v4, "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    sget-object v8, Lcom/android/emailcommon/utility/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v4, v8}, Lcom/android/emailcommon/utility/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    .line 3521
    if-lez p1, :cond_1

    const v8, 0xffff

    if-le p1, v8, :cond_3

    .line 3522
    :cond_1
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v8

    const/16 v10, 0x50

    invoke-direct {v6, v7, v8, v10}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3523
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "https"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v5, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3525
    new-instance v6, Lorg/apache/http/conn/scheme/Scheme;

    const-string v7, "httpts"

    const/16 v8, 0x1bb

    invoke-direct {v6, v7, v4, v8}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v6}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3535
    .end local p1    # "port":I
    :goto_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3536
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    const-string v6, "http.conn-manager.max-total"

    const/16 v7, 0x19

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3537
    const-string v6, "http.conn-manager.max-per-route"

    sget-object v7, Lcom/android/exchange/ExchangeService;->sConnPerPing:Lorg/apache/http/conn/params/ConnPerRoute;

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 3538
    const-string v6, "http.conn-manager.timeout"

    const/16 v7, 0x4e20

    invoke-interface {v2, v6, v7}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    .line 3539
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 3540
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    sget-object v6, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3544
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :cond_2
    monitor-exit v9

    return-object v1

    .line 3527
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_3
    :try_start_1
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v11

    if-eqz p0, :cond_4

    :goto_1
    invoke-direct {v8, v10, v11, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3529
    new-instance v8, Lorg/apache/http/conn/scheme/Scheme;

    const-string v10, "https"

    if-eqz p0, :cond_5

    move v7, p1

    :goto_2
    invoke-direct {v8, v10, v5, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v8}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 3531
    new-instance v7, Lorg/apache/http/conn/scheme/Scheme;

    const-string v8, "httpts"

    if-eqz p0, :cond_6

    .end local p1    # "port":I
    :goto_3
    invoke-direct {v7, v8, v4, p1}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v7}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3507
    .end local v0    # "key":I
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .end local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .end local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    :catchall_0
    move-exception v6

    monitor-exit v9

    throw v6

    .restart local v0    # "key":I
    .restart local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    .restart local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    .restart local v4    # "sf":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local v5    # "sf1":Lcom/android/emailcommon/utility/SSLSocketFactory;
    .restart local p1    # "port":I
    :cond_4
    move v7, p1

    .line 3527
    goto :goto_1

    :cond_5
    move v7, v6

    .line 3529
    goto :goto_2

    :cond_6
    move p1, v6

    .line 3531
    goto :goto_3
.end method

.method private nextWaitTimeForSendableMessages(Landroid/database/Cursor;I)J
    .locals 28
    .param p1, "outboxCursor"    # Landroid/database/Cursor;
    .param p2, "maxAutoRetryTimes"    # I

    .prologue
    .line 5551
    const-wide/16 v16, -0x1

    .line 5552
    .local v16, "msgId":J
    const/16 v22, 0x0

    .line 5553
    .local v22, "retrySendTimes":I
    const-wide/16 v24, 0x0

    .line 5554
    .local v24, "timestamp":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 5555
    .local v12, "currentmilliTime":J
    sget-wide v18, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NO:J

    .line 5557
    .local v18, "nextwaittime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->ID_COLUMN_PROJECTION:[Ljava/lang/String;

    const-string v7, "mailboxKey=? and (syncServerId is null or (syncServerId!=-1 and syncServerId!=-6 and syncServerId!=-2 and syncServerId!=-5 and syncServerId!=-3))"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 5562
    .local v10, "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    if-eqz v10, :cond_5

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 5563
    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    .line 5565
    const/4 v15, 0x0

    .line 5566
    .local v15, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-eqz v4, :cond_1

    .line 5568
    :try_start_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-static {v4, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v15

    .line 5569
    if-eqz v15, :cond_1

    .line 5570
    iget v0, v15, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v22, v0

    .line 5571
    iget-wide v0, v15, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    move-wide/from16 v24, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5580
    :cond_1
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility;->hasUnloadedAttachments(Landroid/content/Context;J)Z

    move-result v4

    if-nez v4, :cond_9

    .line 5581
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextWaitTimeForSendableMessages(). msgId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5582
    const/16 v23, 0x0

    .line 5584
    .local v23, "serverId":I
    if-eqz v15, :cond_2

    :try_start_3
    iget-object v4, v15, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v15, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 5585
    iget-object v4, v15, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v23

    .line 5594
    :cond_2
    :goto_2
    if-gez v23, :cond_4

    if-lez p2, :cond_4

    move/from16 v0, v22

    move/from16 v1, p2

    if-le v0, v1, :cond_4

    .line 5595
    :try_start_4
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextWaitTimeForSendableMessages(). serverId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 5613
    .end local v15    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v23    # "serverId":I
    :catchall_0
    move-exception v4

    .line 5614
    if-eqz v10, :cond_3

    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 5615
    :cond_3
    :goto_3
    throw v4

    .line 5573
    .restart local v15    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catch_0
    move-exception v14

    .line 5574
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    .line 5575
    const/16 v22, 0x0

    .line 5576
    const-wide/16 v24, 0x0

    goto :goto_1

    .line 5587
    .end local v14    # "ex":Ljava/lang/Exception;
    .restart local v23    # "serverId":I
    :catch_1
    move-exception v11

    .line 5588
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 5589
    const/16 v23, 0x0

    goto :goto_2

    .line 5597
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_4
    move/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2, v12, v13}, Lcom/android/emailcommon/utility/Utility;->remainedTimeUntilNextSendDuration(IJJ)J

    move-result-wide v20

    .line 5599
    .local v20, "remainedTime":J
    sget-wide v4, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

    cmp-long v4, v20, v4

    if-gtz v4, :cond_7

    .line 5600
    sget-wide v18, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 5614
    .end local v15    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v20    # "remainedTime":J
    .end local v23    # "serverId":I
    :cond_5
    if-eqz v10, :cond_6

    :try_start_7
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 5617
    :cond_6
    :goto_4
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextWaitTimeForSendableMessages(). expected maximum nextwaittime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5618
    return-wide v18

    .line 5602
    .restart local v15    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v20    # "remainedTime":J
    .restart local v23    # "serverId":I
    :cond_7
    :try_start_8
    sget-wide v4, Lcom/android/exchange/ExchangeService;->HAS_SENABLE_MESSAGE_IN_OUTBOX_NOW:J

    cmp-long v4, v18, v4

    if-lez v4, :cond_8

    cmp-long v4, v18, v20

    if-lez v4, :cond_0

    .line 5604
    :cond_8
    move-wide/from16 v18, v20

    goto/16 :goto_0

    .line 5608
    .end local v20    # "remainedTime":J
    .end local v23    # "serverId":I
    :cond_9
    const-string v4, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextWaitTimeForSendableMessages(). Send later. unloadedAttchment remained. msgId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 5615
    .end local v15    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catch_2
    move-exception v4

    goto :goto_4

    :catch_3
    move-exception v5

    goto :goto_3
.end method

.method public static pingStatus(J)I
    .locals 10
    .param p0, "mailboxId"    # J

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 6381
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6382
    .local v1, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v1, :cond_1

    .line 6405
    :cond_0
    :goto_0
    return v3

    .line 6385
    :cond_1
    iget-object v5, v1, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 6386
    const/4 v3, 0x1

    goto :goto_0

    .line 6389
    :cond_2
    iget-object v5, v1, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncError;

    .line 6390
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v0, :cond_4

    .line 6391
    iget-boolean v5, v0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    if-eqz v5, :cond_3

    .line 6392
    const/4 v3, 0x3

    goto :goto_0

    .line 6393
    :cond_3
    iget-wide v6, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    move v3, v4

    .line 6394
    goto :goto_0

    .line 6398
    :cond_4
    iget-object v5, v1, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/exchange/ExchangeService$SyncError;

    .line 6399
    .local v2, "syncStatuserror":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v2, :cond_0

    .line 6400
    iget-wide v6, v2, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    move v3, v4

    .line 6401
    goto :goto_0
.end method

.method public static reconcileAccounts(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3303
    new-instance v0, Lcom/android/exchange/ExchangeService$3;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$3;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 3323
    return-void
.end method

.method private registerCalendarObserver(Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 6
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 2705
    new-instance v0, Lcom/android/exchange/ExchangeService$CalendarObserver;

    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/exchange/ExchangeService$CalendarObserver;-><init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 2706
    .local v0, "observer":Lcom/android/exchange/ExchangeService$CalendarObserver;
    iget-wide v2, v0, Lcom/android/exchange/ExchangeService$CalendarObserver;->mCalendarId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2709
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2710
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v0, Lcom/android/exchange/ExchangeService$CalendarObserver;->mCalendarId:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2714
    :cond_0
    return-void
.end method

.method private releaseMailbox(J)V
    .locals 3
    .param p1, "mailboxId"    # J

    .prologue
    .line 5538
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 5539
    invoke-virtual {p0, p1, p2}, Lcom/android/exchange/ExchangeService;->releaseWakeLock(J)V

    .line 5540
    return-void
.end method

.method public static releaseSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 3
    .param p0, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 3225
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3226
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 3227
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p0}, Lcom/android/exchange/ExchangeService;->releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z

    .line 3230
    :cond_0
    return-void
.end method

.method public static releaseSecurityHoldForEasMailbox(J)V
    .locals 4
    .param p0, "accountId"    # J

    .prologue
    .line 3132
    const-string v1, "releaseSecurityHoldForEasMailbox(): Entry"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3133
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3134
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 3135
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p0, p1}, Lcom/android/exchange/ExchangeService;->releaseSyncHoldsForEasMailbox(Landroid/content/Context;IJ)Z

    .line 3138
    :cond_0
    return-void
.end method

.method private releaseSyncHoldMailbox(Landroid/content/Context;IJ)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # I
    .param p3, "mailboxId"    # J

    .prologue
    .line 3277
    sget-object v2, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v2

    .line 3278
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncError;

    .line 3279
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    if-ne v1, p2, :cond_0

    .line 3280
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3282
    :cond_0
    monitor-exit v2

    .line 3283
    return-void

    .line 3282
    .end local v0    # "error":Lcom/android/exchange/ExchangeService$SyncError;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private releaseSyncHoldsImpl(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # I
    .param p3, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 3249
    const/4 v1, 0x0

    .line 3250
    .local v1, "holdWasReleased":Z
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3251
    .local v4, "mailboxId":J
    if-eqz p3, :cond_1

    .line 3252
    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 3253
    .local v3, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v3, :cond_2

    .line 3254
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3259
    .end local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncError;

    .line 3260
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v0, :cond_0

    iget v6, v0, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    if-ne v6, p2, :cond_0

    .line 3261
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3262
    const/4 v1, 0x1

    goto :goto_0

    .line 3255
    .end local v0    # "error":Lcom/android/exchange/ExchangeService$SyncError;
    .restart local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-wide v8, p3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    goto :goto_0

    .line 3267
    .end local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v4    # "mailboxId":J
    :cond_3
    if-eqz p3, :cond_4

    .line 3268
    sget-object v7, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v7

    .line 3269
    :try_start_0
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v8, p3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3270
    monitor-exit v7

    .line 3271
    :cond_4
    return v1

    .line 3270
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private releaseSyncHoldsImplForEasMailbox(Landroid/content/Context;IJ)Z
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # I
    .param p3, "accountId"    # J

    .prologue
    .line 3170
    const-string v4, "releaseSyncHoldsImplForEasMailbox(): Entry"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3171
    const/4 v14, 0x0

    .line 3172
    .local v14, "holdWasReleased":Z
    const-wide/16 v12, -0x1

    .line 3174
    .local v12, "easMailboxId":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v7, "accountKey=? AND type=?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v8, v9

    const/4 v9, 0x1

    const-wide/16 v22, 0x44

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 3179
    .local v10, "c":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3180
    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    .line 3183
    :cond_0
    if-eqz v10, :cond_1

    .line 3184
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 3187
    :cond_1
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 3188
    :try_start_1
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 3189
    .local v20, "mailboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 3190
    .local v18, "mailboxId":J
    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v17

    .line 3191
    .local v17, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v17, :cond_5

    .line 3192
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3198
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/exchange/ExchangeService$SyncError;

    .line 3199
    .local v11, "error":Lcom/android/exchange/ExchangeService$SyncError;
    if-eqz v11, :cond_2

    iget v4, v11, Lcom/android/exchange/ExchangeService$SyncError;->reason:I

    move/from16 v0, p2

    if-ne v4, v0, :cond_2

    .line 3200
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "releaseSyncHoldsImplForEasMailbox(): mSyncErrorMap: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " is removed"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3202
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3204
    const/4 v14, 0x1

    goto :goto_0

    .line 3183
    .end local v11    # "error":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v18    # "mailboxId":J
    .end local v20    # "mailboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v4

    if-eqz v10, :cond_4

    .line 3184
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4

    .line 3194
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v17    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v18    # "mailboxId":J
    .restart local v20    # "mailboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_5
    :try_start_2
    move-object/from16 v0, v17

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    cmp-long v4, v6, p3

    if-nez v4, :cond_2

    cmp-long v4, v18, v12

    if-eqz v4, :cond_3

    goto :goto_0

    .line 3207
    .end local v17    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v18    # "mailboxId":J
    :cond_6
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .line 3208
    .local v16, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 3209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3211
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v20    # "mailboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v16    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .restart local v20    # "mailboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_7
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3213
    return v14
.end method

.method public static reloadFolderList(Landroid/content/Context;JZ)V
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "force"    # Z

    .prologue
    .line 4170
    sget-object v9, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4171
    .local v9, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v9, :cond_1

    .line 4252
    :cond_0
    :goto_0
    return-void

    .line 4173
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v3, "accountKey=? AND type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    const/4 v5, 0x1

    const-wide/16 v16, 0x44

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4179
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_7

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4180
    sget-object v1, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 4181
    :try_start_1
    new-instance v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v12}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 4182
    .local v12, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    invoke-virtual {v12, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restore(Landroid/database/Cursor;)V

    .line 4183
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v6

    .line 4184
    .local v6, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v6, :cond_2

    .line 4185
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->reloadFolderListFailed(J)V

    .line 4186
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4248
    if-eqz v7, :cond_0

    .line 4249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4188
    :cond_2
    :try_start_2
    iget-object v14, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 4190
    .local v14, "syncKey":Ljava/lang/String;
    if-nez p3, :cond_4

    if-eqz v14, :cond_3

    const-string v0, "0"

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4191
    :cond_3
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->reloadFolderListFailed(J)V

    .line 4192
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4248
    if-eqz v7, :cond_0

    .line 4249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4196
    :cond_4
    :try_start_3
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 4197
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v0, "syncInterval"

    const/4 v2, -0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v8, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4198
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "accountKey=? and type!=68 and syncInterval IN (-3,-2)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v4, v5

    invoke-virtual {v0, v2, v8, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4202
    const-string v0, "Set push/ping boxes to push/hold"

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4204
    iget-wide v10, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    .line 4205
    .local v10, "id":J
    iget-object v0, v9, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/exchange/AbstractSyncService;

    .line 4207
    .local v13, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v13, :cond_9

    iget v0, v13, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    const/16 v2, 0xa

    if-eq v0, v2, :cond_9

    const-string v0, "FolderSync"

    invoke-virtual {v13}, Lcom/android/exchange/AbstractSyncService;->getCommand()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 4210
    invoke-virtual {v13}, Lcom/android/exchange/AbstractSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4211
    :try_start_4
    invoke-virtual {v13}, Lcom/android/exchange/AbstractSyncService;->stop()V

    .line 4214
    iget-object v15, v13, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    .line 4215
    .local v15, "thread":Ljava/lang/Thread;
    if-eqz v15, :cond_5

    .line 4216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " (Stopped)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v15, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 4217
    invoke-virtual {v15}, Ljava/lang/Thread;->interrupt()V

    .line 4219
    :cond_5
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 4221
    :try_start_5
    invoke-direct {v9, v10, v11}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    .line 4223
    const-string v0, "reload folder list"

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 4245
    .end local v15    # "thread":Ljava/lang/Thread;
    :cond_6
    :goto_1
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 4248
    .end local v6    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v10    # "id":J
    .end local v12    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v13    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v14    # "syncKey":Ljava/lang/String;
    :cond_7
    if-eqz v7, :cond_0

    .line 4249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4219
    .restart local v6    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v8    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "id":J
    .restart local v12    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v13    # "svc":Lcom/android/exchange/AbstractSyncService;
    .restart local v14    # "syncKey":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v0

    .line 4245
    .end local v6    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v10    # "id":J
    .end local v12    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v13    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v14    # "syncKey":Ljava/lang/String;
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 4248
    :catchall_2
    move-exception v0

    if-eqz v7, :cond_8

    .line 4249
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 4235
    .restart local v6    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v8    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "id":J
    .restart local v12    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v13    # "svc":Lcom/android/exchange/AbstractSyncService;
    .restart local v14    # "syncKey":Ljava/lang/String;
    :cond_9
    if-nez v13, :cond_6

    :try_start_9
    iget v0, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_6

    iget-object v0, v9, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v2, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v9, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v2, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 4238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Account in Manual mode. Starting Sync forcefully.. for mailbox:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4240
    const/4 v0, 0x5

    const/4 v2, 0x0

    invoke-direct {v9, v12, v0, v2}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_1
.end method

.method private static reloadFolderListFailed(J)V
    .locals 4
    .param p0, "accountId"    # J

    .prologue
    .line 4162
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    const/16 v1, 0x18

    const/4 v2, 0x0

    invoke-interface {v0, p0, p1, v1, v2}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxListStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4167
    :goto_0
    return-void

    .line 4164
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static removeAliasFromMap()V
    .locals 4

    .prologue
    .line 7353
    sget-object v1, Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 7354
    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7355
    monitor-exit v1

    .line 7356
    return-void

    .line 7355
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static removeFromSyncErrorMap(J)V
    .locals 4
    .param p0, "mailboxId"    # J

    .prologue
    .line 6514
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6515
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 6516
    iget-object v1, v0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6518
    :cond_0
    return-void
.end method

.method public static removeSyncError(J)V
    .locals 4
    .param p0, "mailboxId"    # J

    .prologue
    .line 3163
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3164
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 3165
    iget-object v1, v0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3167
    :cond_0
    return-void
.end method

.method private requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    .locals 8
    .param p1, "m"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "reason"    # I
    .param p3, "req"    # Lcom/android/exchange/Request;

    .prologue
    .line 4818
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    sget-boolean v4, Lcom/android/exchange/ExchangeService;->sStop:Z

    if-eqz v4, :cond_1

    .line 4845
    :cond_0
    :goto_0
    return-void

    .line 4820
    :cond_1
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 4821
    :try_start_0
    iget-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {p0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 4822
    .local v2, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v2, :cond_4

    .line 4825
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    iget-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/exchange/AbstractSyncService;

    .line 4826
    .local v3, "service":Lcom/android/exchange/AbstractSyncService;
    if-nez v3, :cond_4

    .line 4827
    invoke-static {p0, p1}, Lcom/android/exchange/EasSyncService;->getServiceForMailbox(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)Lcom/android/exchange/EasSyncService;

    move-result-object v3

    .line 4828
    move-object v0, v3

    check-cast v0, Lcom/android/exchange/EasSyncService;

    move-object v4, v0

    iget-boolean v4, v4, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    if-nez v4, :cond_2

    .line 4829
    monitor-exit v5

    goto :goto_0

    .line 4844
    .end local v2    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v3    # "service":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 4831
    .restart local v2    # "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v3    # "service":Lcom/android/exchange/AbstractSyncService;
    :cond_2
    :try_start_1
    iget v4, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v6, 0x44

    if-ne v4, v6, :cond_5

    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 4833
    const-string v4, "Provision Error exist. Setting syncReason = SYNC_PROVISION for account sync"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4834
    const/16 v4, 0xa

    iput v4, v3, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    .line 4838
    :goto_1
    if-eqz p3, :cond_3

    .line 4839
    invoke-virtual {v3, p3}, Lcom/android/exchange/AbstractSyncService;->addRequest(Lcom/android/exchange/Request;)V

    .line 4841
    :cond_3
    invoke-direct {p0, v3}, Lcom/android/exchange/ExchangeService;->startServiceThread(Lcom/android/exchange/AbstractSyncService;)V

    .line 4844
    .end local v3    # "service":Lcom/android/exchange/AbstractSyncService;
    :cond_4
    monitor-exit v5

    goto :goto_0

    .line 4836
    .restart local v3    # "service":Lcom/android/exchange/AbstractSyncService;
    :cond_5
    iput p2, v3, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private runAccountReconcilerSync(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3333
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_1

    .line 3334
    const-string v3, "!!! EAS ExchangeService  -----------  Quit thread : null INSTANCE(1)"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3389
    :cond_0
    :goto_0
    return-void

    .line 3338
    :cond_1
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 3345
    .local v1, "accountMgrList":[Landroid/accounts/Account;
    new-instance v3, Lcom/android/exchange/ExchangeService$AccountList;

    invoke-direct {v3}, Lcom/android/exchange/ExchangeService$AccountList;-><init>()V

    invoke-static {p1, v3}, Lcom/android/exchange/ExchangeService;->collectEasAccounts(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;

    move-result-object v0

    .line 3350
    .local v0, "accountList":Lcom/android/exchange/ExchangeService$AccountList;
    if-nez v0, :cond_2

    .line 3351
    const-string v3, "onAccountChanged(): empty accounts list was returned from collectEasAccounts()! Exit immediately."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto :goto_0

    .line 3359
    :cond_2
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_3

    .line 3360
    const-string v3, "!!! EAS EAS ExchangeService  -----------  Quit thread : null INSTANCE(2)"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto :goto_0

    .line 3364
    :cond_3
    const-string v3, "Reconciling accounts..."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3365
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {p1, v0, v1, v3}, Lcom/android/emailcommon/utility/AccountReconciler;->reconcileAccounts(Landroid/content/Context;Ljava/util/List;[Landroid/accounts/Account;Landroid/content/ContentResolver;)Z

    move-result v2

    .line 3367
    .local v2, "accountsDeleted":Z
    if-eqz v2, :cond_4

    .line 3369
    :try_start_0
    new-instance v3, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v3, p1}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/android/emailcommon/service/AccountServiceProxy;->accountDeleted()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3376
    :cond_4
    :goto_1
    invoke-static {p1}, Lcom/android/emailcommon/utility/Utility;->isSamsungSingleuser(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3377
    const-string v3, "Reconciling Single accounts..."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3378
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {p1, v0, v1, v3}, Lcom/android/emailcommon/utility/AccountReconciler;->reconcileSingleAccounts(Landroid/content/Context;Ljava/util/List;[Landroid/accounts/Account;Landroid/content/ContentResolver;)Z

    move-result v2

    .line 3380
    if-eqz v2, :cond_0

    .line 3382
    :try_start_1
    new-instance v3, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v3, p1}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/android/emailcommon/service/AccountServiceProxy;->accountDeleted()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 3383
    :catch_0
    move-exception v3

    goto :goto_0

    .line 3370
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public static runAsleep(JJ)V
    .locals 2
    .param p0, "id"    # J
    .param p2, "millis"    # J

    .prologue
    .line 4372
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4373
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 4374
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->setAlarm(JJ)V

    .line 4375
    invoke-virtual {v0, p0, p1}, Lcom/android/exchange/ExchangeService;->releaseWakeLock(J)V

    .line 4377
    :cond_0
    return-void
.end method

.method public static runAwake(J)V
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 4364
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4365
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 4366
    invoke-direct {v0, p0, p1}, Lcom/android/exchange/ExchangeService;->acquireWakeLock(J)V

    .line 4367
    invoke-direct {v0, p0, p1}, Lcom/android/exchange/ExchangeService;->clearAlarm(J)V

    .line 4369
    :cond_0
    return-void
.end method

.method public static sendMessageRequest(Lcom/android/exchange/Request;)V
    .locals 8
    .param p0, "req"    # Lcom/android/exchange/Request;

    .prologue
    .line 6317
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6318
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v0, :cond_1

    .line 6333
    :cond_0
    :goto_0
    return-void

    .line 6320
    :cond_1
    iget-wide v6, p0, Lcom/android/exchange/Request;->mMessageId:J

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v1

    .line 6321
    .local v1, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v1, :cond_0

    .line 6324
    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 6325
    .local v2, "mailboxId":J
    iget-object v5, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/AbstractSyncService;

    .line 6327
    .local v4, "service":Lcom/android/exchange/AbstractSyncService;
    if-nez v4, :cond_2

    .line 6328
    const/4 v5, 0x7

    invoke-static {v2, v3, v5, p0}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    .line 6329
    const-string v5, "part request"

    invoke-static {v5}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto :goto_0

    .line 6331
    :cond_2
    invoke-virtual {v4, p0}, Lcom/android/exchange/AbstractSyncService;->addRequest(Lcom/android/exchange/Request;)V

    goto :goto_0
.end method

.method public static sendMessageRequestCancel(Lcom/android/exchange/PartRequest;)V
    .locals 14
    .param p0, "req"    # Lcom/android/exchange/PartRequest;

    .prologue
    .line 6337
    sget-object v10, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6338
    .local v10, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v10, :cond_1

    .line 6368
    :cond_0
    :goto_0
    return-void

    .line 6341
    :cond_1
    if-eqz p0, :cond_0

    .line 6342
    iget-object v1, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    .line 6344
    .local v8, "attachmentId":J
    invoke-direct {v10, v8, v9}, Lcom/android/exchange/ExchangeService;->isAlreadyDownloding(J)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 6345
    invoke-direct {v10, v8, v9}, Lcom/android/exchange/ExchangeService;->getDownloadServiceFromMap(J)Lcom/android/exchange/EasDownLoadAttachmentSvc;

    move-result-object v13

    .line 6347
    .local v13, "service":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    if-eqz v13, :cond_2

    .line 6348
    invoke-virtual {v13, p0}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userCancelledAttachmentRequest(Lcom/android/exchange/Request;)Z

    move-result v12

    .line 6349
    .local v12, "ret":Z
    const-string v1, "ExchangeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cancel request issued for attachment id:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 6351
    .end local v12    # "ret":Z
    :cond_2
    iget-wide v2, p0, Lcom/android/exchange/PartRequest;->mMessageId:J

    invoke-static {v10, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v11

    .line 6352
    .local v11, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v11, :cond_0

    .line 6354
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    iget-wide v2, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    iget-object v4, p0, Lcom/android/exchange/PartRequest;->mAttachment:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v6, 0x130000

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6360
    :goto_1
    const-string v1, "ExchangeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Service is null, unable to cancel attachment download of : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 6356
    :catch_0
    move-exception v0

    .line 6357
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 6365
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v13    # "service":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    :cond_3
    const-string v1, "ExchangeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Attachment id :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not downloading"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static serviceRequest(JI)V
    .locals 2
    .param p0, "mailboxId"    # J
    .param p2, "reason"    # I

    .prologue
    .line 6205
    const-wide/16 v0, 0x1388

    invoke-static {p0, p1, v0, v1, p2}, Lcom/android/exchange/ExchangeService;->serviceRequest(JJI)V

    .line 6206
    return-void
.end method

.method public static serviceRequest(JJI)V
    .locals 14
    .param p0, "mailboxId"    # J
    .param p2, "ms"    # J
    .param p4, "reason"    # I

    .prologue
    const/4 v12, 0x4

    .line 6223
    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6224
    .local v4, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v4, :cond_1

    .line 6298
    :cond_0
    :goto_0
    return-void

    .line 6226
    :cond_1
    invoke-static {v4, p0, p1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v5

    .line 6230
    .local v5, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v5, :cond_2

    .line 6231
    const-string v7, "Ignoring serviceRequest for null mailbox"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6237
    :cond_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 6239
    .local v2, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v2, :cond_5

    .line 6247
    iget-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 6249
    iget-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    const-wide v10, 0x4028333333333333L    # 12.1

    cmpg-double v7, v8, v10

    if-gtz v7, :cond_3

    iget v7, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-ne v7, v12, :cond_3

    .line 6251
    const-string v7, "Ignoring serviceRequest for outbox mailbox"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6253
    :cond_3
    iget v7, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-ne v7, v12, :cond_4

    iget-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    const-wide/high16 v10, 0x402c000000000000L    # 14.0

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_4

    iget v7, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v7, v7, 0x800

    if-nez v7, :cond_4

    .line 6256
    const-string v7, "Ignoring serviceRequest for outbox mailbox as SMS Sync is not enabled"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6258
    :cond_4
    if-nez p4, :cond_6

    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-object v7, v7, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v8, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 6260
    const-string v7, "Ignoring serviceRequest for SYNC_UPSYNC due to ProvisionErrorExist"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6264
    :cond_5
    const-string v7, "Ignoring serviceRequest for null account"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6270
    :cond_6
    iget v7, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x8

    if-ne v7, v8, :cond_7

    .line 6272
    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const-string v7, "Ignoring serviceRequest for search"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 6276
    :cond_7
    iget v7, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x62

    if-ne v7, v8, :cond_8

    .line 6277
    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const-string v7, "Ignoring serviceRequest for document search"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6283
    :cond_8
    :try_start_0
    iget-object v7, v4, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/exchange/AbstractSyncService;

    .line 6284
    .local v6, "service":Lcom/android/exchange/AbstractSyncService;
    if-eqz v6, :cond_a

    .line 6285
    instance-of v7, v6, Lcom/android/exchange/EasOutboxService;

    if-nez v7, :cond_9

    .line 6286
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    add-long v8, v8, p2

    iput-wide v8, v6, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    .line 6291
    :goto_1
    const-string v7, "service request"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 6295
    .end local v6    # "service":Lcom/android/exchange/AbstractSyncService;
    :catch_0
    move-exception v3

    .line 6296
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 6288
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v6    # "service":Lcom/android/exchange/AbstractSyncService;
    :cond_9
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "serviceRequest() : service.mRequestTime = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, v6, Lcom/android/exchange/AbstractSyncService;->mRequestTime:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "service.mRequestTime should not be set for EasOutboxService."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 6293
    :cond_a
    const/4 v7, 0x0

    move/from16 v0, p4

    invoke-static {p0, p1, v0, v7}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private setAlarm(JJ)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "millis"    # J

    .prologue
    .line 4336
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    monitor-enter v4

    .line 4337
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    .line 4338
    .local v2, "pi":Landroid/app/PendingIntent;
    if-nez v2, :cond_0

    .line 4339
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/android/exchange/MailboxAlarmReceiver;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4340
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "mailbox"

    invoke-virtual {v1, v3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4341
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Box"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4342
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static {p0, v3, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 4343
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPendingIntents:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4345
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 4346
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, p3

    invoke-virtual {v0, v3, v6, v7, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 4350
    .end local v0    # "alarmManager":Landroid/app/AlarmManager;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    monitor-exit v4

    .line 4351
    return-void

    .line 4350
    .end local v2    # "pi":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public static setAliasInMap(Ljava/lang/String;)V
    .locals 4
    .param p0, "alias"    # Ljava/lang/String;

    .prologue
    .line 7347
    sget-object v1, Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 7348
    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7349
    monitor-exit v1

    .line 7350
    return-void

    .line 7349
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setDownloadServiceInMap(JLcom/android/exchange/EasDownLoadAttachmentSvc;)V
    .locals 3
    .param p1, "attachmentId"    # J
    .param p3, "svc"    # Lcom/android/exchange/EasDownLoadAttachmentSvc;

    .prologue
    .line 7618
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 7626
    :cond_0
    :goto_0
    return-void

    .line 7621
    :cond_1
    sget-object v1, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    monitor-enter v1

    .line 7622
    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 7623
    sget-object v0, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 7625
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setEasSyncIntervals(Landroid/content/Context;Ljava/lang/String;IJZ)V
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "syncInterval"    # I
    .param p3, "accountId"    # J
    .param p5, "modifyInbox"    # Z

    .prologue
    .line 6956
    new-instance v5, Landroid/accounts/Account;

    const-string v14, "com.android.exchange"

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v14}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6958
    .local v5, "acct":Landroid/accounts/Account;
    const-string v14, "com.android.contacts"

    invoke-static {v5, v14}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v10

    .line 6962
    .local v10, "syncContacts":Z
    const-string v14, "com.android.calendar"

    invoke-static {v5, v14}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v9

    .line 6967
    .local v9, "syncCalendar":Z
    const-string v14, "tasks"

    invoke-static {v5, v14}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v12

    .line 6972
    .local v12, "syncTasks":Z
    const-string v14, "com.android.notes"

    invoke-static {v5, v14}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v11

    .line 6974
    .local v11, "syncNotes":Z
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 6975
    .local v6, "cv":Landroid/content/ContentValues;
    if-eqz p5, :cond_1

    .line 6977
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 6978
    .local v4, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/4 v7, 0x1

    .line 6979
    .local v7, "errorOccured":Z
    if-eqz v4, :cond_0

    .line 6984
    :try_start_0
    iget-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    const-wide/high16 v16, 0x402c000000000000L    # 14.0

    cmpl-double v14, v14, v16

    if-ltz v14, :cond_0

    .line 6987
    iget v14, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v14, v14, 0x800

    if-eqz v14, :cond_4

    .line 6988
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6989
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (0,4,97)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7002
    :goto_0
    const-string v14, "ExchangeService"

    const-string v15, "exchange service - seteassync intervals,inbox,outbox,RIC"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7004
    const/4 v7, 0x0

    .line 7012
    :cond_0
    :goto_1
    if-eqz v7, :cond_1

    .line 7013
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7014
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (0)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7022
    .end local v4    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v7    # "errorOccured":Z
    :cond_1
    if-eqz v10, :cond_5

    .line 7023
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7024
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (66)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7029
    const/4 v14, 0x6

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v14}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v13

    .line 7030
    .local v13, "trashServerId":Ljava/lang/String;
    if-eqz v13, :cond_2

    .line 7031
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type = 83 and parentServerId!=? AND displayName != \'Suggested Contacts\'"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7054
    :cond_2
    :goto_2
    if-eqz v9, :cond_6

    .line 7055
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7056
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (65)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7061
    const/4 v14, 0x6

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v14}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v13

    .line 7062
    if-eqz v13, :cond_3

    .line 7063
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type = 82 and parentServerId!=? AND displayName != \'Suggested Contacts\'"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7087
    :cond_3
    :goto_3
    if-eqz v11, :cond_7

    .line 7089
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7090
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (69)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7104
    :goto_4
    if-eqz v12, :cond_8

    .line 7106
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7107
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (67)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7119
    :goto_5
    return-void

    .line 6994
    .end local v13    # "trashServerId":Ljava/lang/String;
    .restart local v4    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v7    # "errorOccured":Z
    :cond_4
    :try_start_1
    const-string v14, "syncInterval"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 6995
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (0,97)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 7007
    :catch_0
    move-exception v8

    .line 7008
    .local v8, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v8}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_1

    .line 7038
    .end local v4    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v7    # "errorOccured":Z
    .end local v8    # "npe":Ljava/lang/NullPointerException;
    :cond_5
    const-string v14, "syncInterval"

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7039
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (66)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7044
    const/4 v14, 0x6

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v14}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v13

    .line 7045
    .restart local v13    # "trashServerId":Ljava/lang/String;
    if-eqz v13, :cond_2

    .line 7046
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type = 83 and parentServerId!=? AND displayName != \'Suggested Contacts\'"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_2

    .line 7070
    :cond_6
    const-string v14, "syncInterval"

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7071
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (65)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 7076
    const/4 v14, 0x6

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2, v14}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v13

    .line 7077
    if-eqz v13, :cond_3

    .line 7078
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type = 82 and parentServerId!=? AND displayName != \'Suggested Contacts\'"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    aput-object v13, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_3

    .line 7095
    :cond_7
    const-string v14, "syncInterval"

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7096
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (69)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_4

    .line 7112
    :cond_8
    const-string v14, "syncInterval"

    const/4 v15, -0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v6, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7113
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v16, "accountKey=? and type in (67)"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v15, v6, v0, v1}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_5
.end method

.method public static setWatchdogAlarm(JJ)V
    .locals 2
    .param p0, "id"    # J
    .param p2, "millis"    # J

    .prologue
    .line 4387
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4388
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 4389
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->setAlarm(JJ)V

    .line 4391
    :cond_0
    return-void
.end method

.method private shutdown()V
    .locals 8

    .prologue
    .line 5424
    sget-object v2, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v2

    .line 5426
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v1, :cond_7

    .line 5427
    const-string v1, "ExchangeService shutting down..."

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5428
    sget-object v3, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ServiceThread=shutdown sStop="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v4, Lcom/android/exchange/ExchangeService;->sStop:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " sServiceThread="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v1, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    if-eqz v1, :cond_8

    sget-object v1, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " Service="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/android/exchange/ServiceLogger;->logEASServiceStats(Ljava/lang/String;)V

    .line 5433
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->stopServiceThreads()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 5436
    :try_start_1
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    if-eqz v1, :cond_0

    .line 5437
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    invoke-virtual {p0, v1}, Lcom/android/exchange/ExchangeService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5463
    :cond_0
    :goto_1
    :try_start_2
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 5464
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    if-eqz v1, :cond_1

    .line 5465
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 5466
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    .line 5468
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    if-eqz v1, :cond_2

    .line 5469
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 5470
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    .line 5472
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

    if-eqz v1, :cond_3

    .line 5473
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 5474
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

    .line 5483
    :cond_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->unregisterCalendarObservers()V

    .line 5487
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mStatusChangeListener:Ljava/lang/Object;

    if-eqz v1, :cond_4

    .line 5488
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mStatusChangeListener:Ljava/lang/Object;

    invoke-static {v1}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 5489
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mStatusChangeListener:Ljava/lang/Object;

    .line 5490
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusObserver:Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;

    .line 5510
    :cond_4
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "phone"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 5512
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-eqz v1, :cond_5

    .line 5513
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 5514
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 5519
    :cond_5
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->clearAlarms()V

    .line 5522
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 5523
    :try_start_3
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_6

    .line 5524
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 5525
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 5527
    :cond_6
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 5529
    const/4 v1, 0x0

    :try_start_4
    sput-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 5530
    const/4 v1, 0x0

    sput-object v1, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    .line 5531
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/exchange/ExchangeService;->sStop:Z

    .line 5532
    const-string v1, "Goodbye"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5534
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_7
    monitor-exit v2

    .line 5535
    return-void

    .line 5428
    :cond_8
    const-string v1, "Died"

    goto/16 :goto_0

    .line 5441
    :catchall_0
    move-exception v1

    throw v1

    .line 5534
    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    .line 5527
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    :catchall_2
    move-exception v1

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5439
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :catch_0
    move-exception v1

    goto/16 :goto_1
.end method

.method public static declared-synchronized shutdownConnectionManager()V
    .locals 9

    .prologue
    .line 3882
    const-class v7, Lcom/android/exchange/ExchangeService;

    monitor-enter v7

    const/4 v0, 0x0

    .line 3883
    .local v0, "bShutdownConnectionManager":Z
    :try_start_0
    sget-object v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 3884
    sget-object v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3885
    .local v5, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    const-string v6, "Shutting down ClientConnectionManager"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3886
    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 3882
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    .line 3889
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    sget-object v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 3891
    const/4 v0, 0x1

    .line 3894
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    sget-object v6, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 3895
    sget-object v6, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3896
    .restart local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    const-string v6, "Shutting down ClientConnectionManager for loadMore"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3897
    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_1

    .line 3900
    .end local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_2
    sget-object v6, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 3909
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    sget-object v6, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    if-eqz v6, :cond_6

    .line 3910
    const-string v6, "Shutting down smClientConnectionManager"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3912
    :try_start_2
    sget-object v6, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 3914
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lorg/apache/http/conn/ClientConnectionManager;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/conn/ClientConnectionManager;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3915
    .local v4, "manager":Lorg/apache/http/conn/ClientConnectionManager;
    if-eqz v4, :cond_4

    .line 3917
    :try_start_3
    invoke-interface {v4}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 3918
    :catch_0
    move-exception v1

    .line 3919
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 3923
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Lorg/apache/http/conn/ClientConnectionManager;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "manager":Lorg/apache/http/conn/ClientConnectionManager;
    :catch_1
    move-exception v1

    .line 3924
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 3926
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    sget-object v6, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 3929
    :cond_6
    sget-object v6, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_8

    .line 3930
    sget-object v6, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3931
    .restart local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    const-string v6, "Shutting down Ping mClientConnectionManager"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3932
    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_3

    .line 3935
    .end local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_7
    sget-object v6, Lcom/android/exchange/ExchangeService;->mClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 3937
    const/4 v0, 0x1

    .line 3941
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_8
    if-eqz v0, :cond_9

    .line 3943
    sget v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    .line 3945
    const-string v6, "shutdownConnectionManager(): sClientConnectionManagerShutdownCount is increased"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3947
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "shutdownConnectionManager(): sClientConnectionManagerShutdownCount= "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v8, Lcom/android/exchange/ExchangeService;->sClientConnectionManagerShutdownCount:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3952
    :cond_9
    sget-object v6, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_b

    .line 3953
    sget-object v6, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3954
    .restart local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    const-string v6, "Shutting down ClientConnectionManager for AutoDiscover"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3955
    invoke-interface {v5}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_4

    .line 3958
    .end local v5    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_a
    sget-object v6, Lcom/android/exchange/ExchangeService;->sAutoDiscClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3962
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_b
    monitor-exit v7

    return-void
.end method

.method public static declared-synchronized shutdownConnectionManagerForAttachmentDownLoads(J)V
    .locals 6
    .param p0, "attachmentId"    # J

    .prologue
    .line 3778
    const-class v3, Lcom/android/exchange/ExchangeService;

    monitor-enter v3

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shutdownConnectionManagerForAttachmentDownLoads() called. attachmentId = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3779
    sget-object v2, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3780
    :cond_0
    const-string v2, "adClientConnectionManagers is Empty!! "

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3781
    const/4 v2, 0x0

    sput v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3803
    :cond_1
    :goto_0
    monitor-exit v3

    return-void

    .line 3785
    :cond_2
    :try_start_1
    sget-boolean v2, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z

    if-eqz v2, :cond_3

    .line 3786
    sget v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    add-int/lit8 v2, v2, -0x1

    sput v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    .line 3787
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adShutdownCount SUB = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v4, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3788
    sget v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    if-gtz v2, :cond_1

    .line 3793
    :cond_3
    sget-object v2, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3794
    sget-object v2, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3795
    .local v1, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    const-string v2, "Shutting down adClientConnectionManagers"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3796
    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3778
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 3799
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    :try_start_2
    sget-object v2, Lcom/android/exchange/ExchangeService;->adClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 3800
    const/4 v2, 0x0

    sput v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static shutdownSendMessageConnection(J)V
    .locals 4
    .param p0, "accountId"    # J

    .prologue
    .line 3860
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutdownSendMessageConnection() called. accountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3861
    sget-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 3862
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    .line 3864
    :cond_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3865
    sget-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/conn/ClientConnectionManager;

    .line 3866
    .local v1, "manager":Lorg/apache/http/conn/ClientConnectionManager;
    if-eqz v1, :cond_1

    .line 3868
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutdownSendMessageConnection() manager clear OK. accountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3870
    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3877
    :cond_1
    :goto_0
    sget-object v2, Lcom/android/exchange/ExchangeService;->smClientConnectionManager:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3879
    .end local v1    # "manager":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_2
    return-void

    .line 3871
    .restart local v1    # "manager":Lorg/apache/http/conn/ClientConnectionManager;
    :catch_0
    move-exception v0

    .line 3872
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "shutdownSendMessageConnection() exception caughted while manager clear. accountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 3874
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static startManualSync(JILcom/android/exchange/Request;)V
    .locals 10
    .param p0, "mailboxId"    # J
    .param p2, "reason"    # I
    .param p3, "req"    # Lcom/android/exchange/Request;

    .prologue
    .line 6409
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6410
    .local v3, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v3, :cond_0

    .line 6440
    :goto_0
    return-void

    .line 6412
    :cond_0
    sget-object v7, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v7

    .line 6413
    :try_start_0
    iget-object v6, v3, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/exchange/AbstractSyncService;

    .line 6414
    .local v5, "svc":Lcom/android/exchange/AbstractSyncService;
    invoke-static {v3, p0, p1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v4

    .line 6415
    .local v4, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v4, :cond_1

    .line 6416
    monitor-exit v7

    goto :goto_0

    .line 6439
    .end local v4    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v5    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 6417
    .restart local v4    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v5    # "svc":Lcom/android/exchange/AbstractSyncService;
    :cond_1
    :try_start_1
    iget-wide v8, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v3, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 6418
    .local v2, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v5, :cond_3

    .line 6419
    iget-object v6, v3, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 6420
    if-eqz v4, :cond_2

    .line 6421
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting sync for "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v8, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6422
    invoke-direct {v3, v4, p2, p3}, Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    .line 6439
    :cond_2
    :goto_1
    monitor-exit v7

    goto :goto_0

    .line 6427
    :cond_3
    if-eqz v2, :cond_2

    .line 6428
    move-object v0, v5

    check-cast v0, Lcom/android/exchange/EasSyncService;

    move-object v6, v0

    iget-boolean v6, v6, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    if-nez v6, :cond_4

    .line 6429
    monitor-exit v7

    goto :goto_0

    .line 6430
    :cond_4
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x44

    if-ne v6, v8, :cond_5

    iget-object v6, v3, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    iget-wide v8, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 6432
    const-string v6, "Provision Error exist. Setting syncReason = SYNC_PROVISION for account sync"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6433
    const/16 v6, 0xa

    iput v6, v5, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I

    goto :goto_1

    .line 6435
    :cond_5
    iput p2, v5, Lcom/android/exchange/AbstractSyncService;->mSyncReason:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private startServiceThread(Lcom/android/exchange/AbstractSyncService;)V
    .locals 10
    .param p1, "service"    # Lcom/android/exchange/AbstractSyncService;

    .prologue
    .line 4755
    iget-object v0, p1, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 4757
    .local v0, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v0, :cond_0

    .line 4790
    :goto_0
    return-void

    .line 4759
    :cond_0
    sget-object v7, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v7

    .line 4760
    :try_start_0
    iget-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 4763
    .local v1, "mailboxName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->getNameForThread()Ljava/lang/String;

    move-result-object v2

    .line 4765
    .local v2, "mailboxThreadName":Ljava/lang/String;
    iget v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v8, 0x4

    if-ne v6, v8, :cond_1

    .line 4766
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/AbstractSyncService;

    .line 4767
    .local v4, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v4, :cond_1

    .line 4768
    const-string v6, "ExchangeService"

    const-string v8, "startServiceThread: warning. mServiceMap for Outbox is already running."

    invoke-static {v6, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4771
    iget-object v3, p1, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    .line 4772
    .local v3, "outboxthread":Ljava/lang/Thread;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4773
    const-string v6, "ExchangeService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "startServiceThread: outboxthread should not be duplicated. return  mailboxName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4776
    monitor-exit v7

    goto :goto_0

    .line 4789
    .end local v1    # "mailboxName":Ljava/lang/String;
    .end local v2    # "mailboxThreadName":Ljava/lang/String;
    .end local v3    # "outboxthread":Ljava/lang/Thread;
    .end local v4    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 4780
    .restart local v1    # "mailboxName":Ljava/lang/String;
    .restart local v2    # "mailboxThreadName":Ljava/lang/String;
    :cond_1
    :try_start_1
    new-instance v5, Ljava/lang/Thread;

    invoke-direct {v5, p1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 4781
    .local v5, "thread":Ljava/lang/Thread;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting thread "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " for mailbox "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4782
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 4783
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4784
    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 4785
    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    const-string v8, "__eas"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 4787
    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-direct {p0, v8, v9}, Lcom/android/exchange/ExchangeService;->stopPing(J)V

    .line 4789
    :cond_2
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private stopAccountAttachmentDownloads(J)V
    .locals 11
    .param p1, "acctId"    # J

    .prologue
    .line 3992
    const-string v5, "ExchangeService"

    const-string v6, " Exchange Service  : stopAccountAttachmentDownloads called "

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3993
    sget-object v6, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    monitor-enter v6

    .line 3994
    :try_start_0
    sget-object v5, Lcom/android/exchange/ExchangeService;->mEasDownloadInProgress:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 3996
    .local v4, "tmpDownloadSet":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/android/exchange/EasDownLoadAttachmentSvc;>;"
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 3997
    .local v2, "mId":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {p0, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v0

    .line 3998
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v0, :cond_0

    .line 3999
    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    cmp-long v5, v8, p1

    if-nez v5, :cond_0

    .line 4000
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {p0, v8, v9}, Lcom/android/exchange/ExchangeService;->getDownloadServiceFromMap(J)Lcom/android/exchange/EasDownLoadAttachmentSvc;

    move-result-object v3

    .line 4001
    .local v3, "svc":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    if-eqz v3, :cond_0

    .line 4002
    const-string v5, "ExchangeService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Exchange Service  : stopAccountAttachmentDownloads called for attachment_id :  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4005
    new-instance v5, Lcom/android/exchange/PartRequest;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, v0, v7, v8}, Lcom/android/exchange/PartRequest;-><init>(Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->userCancelledAttachmentRequest(Lcom/android/exchange/Request;)Z

    goto :goto_0

    .line 4010
    .end local v0    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mId":Ljava/lang/Long;
    .end local v3    # "svc":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    .end local v4    # "tmpDownloadSet":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/android/exchange/EasDownLoadAttachmentSvc;>;"
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v4    # "tmpDownloadSet":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/android/exchange/EasDownLoadAttachmentSvc;>;"
    :cond_1
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4011
    return-void
.end method

.method public static stopAccountSyncs(J)V
    .locals 2
    .param p0, "acctId"    # J

    .prologue
    .line 3977
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 3978
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 3979
    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V

    .line 3981
    :cond_0
    return-void
.end method

.method private stopAccountSyncs(JZ)V
    .locals 15
    .param p1, "acctId"    # J
    .param p3, "includeAccountMailbox"    # Z

    .prologue
    .line 4054
    sget-object v11, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v11

    .line 4055
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4056
    .local v4, "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v10, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 4057
    .local v6, "mid":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {p0, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 4058
    .local v3, "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v3, :cond_0

    .line 4059
    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    cmp-long v10, v12, p1

    if-nez v10, :cond_0

    .line 4060
    if-nez p3, :cond_1

    iget v10, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x44

    if-ne v10, v12, :cond_1

    .line 4061
    iget-object v10, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/exchange/AbstractSyncService;

    .line 4062
    .local v7, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v7, :cond_0

    .line 4063
    invoke-virtual {v7}, Lcom/android/exchange/AbstractSyncService;->stop()V

    goto :goto_0

    .line 4157
    .end local v3    # "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v4    # "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "mid":Ljava/lang/Long;
    .end local v7    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 4067
    .restart local v3    # "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v4    # "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "mid":Ljava/lang/Long;
    :cond_1
    :try_start_1
    iget-object v10, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v10, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/exchange/AbstractSyncService;

    .line 4068
    .restart local v7    # "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v7, :cond_e

    .line 4070
    instance-of v10, v7, Lcom/android/exchange/EasSyncService;

    if-eqz v10, :cond_d

    .line 4071
    move-object v0, v7

    check-cast v0, Lcom/android/exchange/EasSyncService;

    move-object v8, v0

    .line 4072
    .local v8, "svc1":Lcom/android/exchange/EasSyncService;
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x41

    if-eq v10, v12, :cond_2

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x42

    if-eq v10, v12, :cond_2

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x43

    if-eq v10, v12, :cond_2

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x45

    if-eq v10, v12, :cond_2

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x53

    if-eq v10, v12, :cond_2

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x52

    if-ne v10, v12, :cond_4

    :cond_2
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    if-eqz v10, :cond_3

    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    const-string v12, "0"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 4080
    :cond_3
    const-string v10, "stopAccountSyncs(): PIMS doing initial sync with null synckey. Continuing without stopping..."

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4085
    :cond_4
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x41

    if-ne v10, v12, :cond_5

    sget-boolean v10, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z

    const/4 v12, 0x1

    if-ne v10, v12, :cond_5

    .line 4087
    const-string v10, "stopAccountSyncs(): Calendar doing initial sync. Continuing without stopping..."

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4089
    :cond_5
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x42

    if-ne v10, v12, :cond_6

    sget-boolean v10, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    const/4 v12, 0x1

    if-ne v10, v12, :cond_6

    .line 4091
    const-string v10, "stopAccountSyncs(): Contacts doing initial sync. Continuing without stopping..."

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4093
    :cond_6
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x43

    if-ne v10, v12, :cond_7

    sget-boolean v10, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    const/4 v12, 0x1

    if-ne v10, v12, :cond_7

    .line 4095
    const-string v10, "stopAccountSyncs(): Tasks doing initial sync. Continuing without stopping..."

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4097
    :cond_7
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x45

    if-ne v10, v12, :cond_8

    sget-boolean v10, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    const/4 v12, 0x1

    if-ne v10, v12, :cond_8

    .line 4099
    const-string v10, "stopAccountSyncs(): Notes doing initial sync. Continuing without stopping..."

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4108
    :cond_8
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x44

    if-ne v10, v12, :cond_b

    .line 4109
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 4110
    .local v2, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v2, :cond_9

    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    if-eqz v10, :cond_a

    :cond_9
    iget-object v10, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    const-string v12, "0"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 4112
    :cond_a
    const-string v10, "stopAccountSyncs | Initial FolderSync in progress; Do not stop!"

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4115
    .end local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_b
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x53

    if-ne v10, v12, :cond_c

    sget-object v10, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_c

    sget-object v10, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    const/4 v12, 0x1

    if-ne v10, v12, :cond_c

    .line 4119
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "stopAccountSyncs(): Contacts SubFolder ID "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " doing initial sync. Continuing without stopping..."

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4123
    :cond_c
    iget-object v10, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v12, 0x52

    if-ne v10, v12, :cond_d

    sget-object v10, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_d

    sget-object v10, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    const/4 v12, 0x1

    if-ne v10, v12, :cond_d

    .line 4127
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "stopAccountSyncs(): Calendar SubFolder ID "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v12, v8, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, " doing initial sync. Continuing without stopping..."

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4134
    .end local v8    # "svc1":Lcom/android/exchange/EasSyncService;
    :cond_d
    invoke-virtual {v7}, Lcom/android/exchange/AbstractSyncService;->stop()V

    .line 4135
    iget-object v9, v7, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    .line 4136
    .local v9, "t":Ljava/lang/Thread;
    if-eqz v9, :cond_e

    .line 4137
    invoke-virtual {v9}, Ljava/lang/Thread;->interrupt()V

    .line 4140
    .end local v9    # "t":Ljava/lang/Thread;
    :cond_e
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 4144
    .end local v3    # "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v6    # "mid":Ljava/lang/Long;
    .end local v7    # "svc":Lcom/android/exchange/AbstractSyncService;
    :cond_f
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    .line 4145
    .restart local v6    # "mid":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-direct {p0, v12, v13}, Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V

    goto :goto_1

    .line 4149
    .end local v6    # "mid":Ljava/lang/Long;
    :cond_10
    sget-object v10, Lcom/android/exchange/ExchangeService;->mPingFoldersMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4150
    sget-object v10, Lcom/android/exchange/ExchangeService;->mPingHeartBeatIntervalMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4152
    sget-object v10, Lcom/android/exchange/ExchangeService;->mSubFolderContactAccountObjectMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4154
    invoke-direct/range {p0 .. p2}, Lcom/android/exchange/ExchangeService;->stopAccountAttachmentDownloads(J)V

    .line 4156
    sget-object v10, Lcom/android/exchange/ExchangeService;->mSubFolderCalendarAccountObjectMap:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4157
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4158
    return-void
.end method

.method public static stopManualSync(J)V
    .locals 6
    .param p0, "mailboxId"    # J

    .prologue
    .line 6444
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 6445
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v0, :cond_0

    .line 6457
    :goto_0
    return-void

    .line 6447
    :cond_0
    sget-object v3, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v3

    .line 6448
    :try_start_0
    iget-object v2, v0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/exchange/AbstractSyncService;

    .line 6449
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v1, :cond_2

    .line 6450
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stopping sync for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v1, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 6451
    invoke-virtual {v1}, Lcom/android/exchange/AbstractSyncService;->stop()V

    .line 6452
    iget-object v2, v1, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v2, :cond_1

    .line 6453
    iget-object v2, v1, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 6454
    :cond_1
    invoke-virtual {v0, p0, p1}, Lcom/android/exchange/ExchangeService;->releaseWakeLock(J)V

    .line 6456
    :cond_2
    monitor-exit v3

    goto :goto_0

    .end local v1    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static stopNonAccountMailboxSyncsForAccount(J)V
    .locals 2
    .param p0, "acctId"    # J

    .prologue
    .line 4262
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 4263
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-eqz v0, :cond_0

    .line 4264
    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V

    .line 4265
    const-string v1, "reload folder list"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 4267
    :cond_0
    return-void
.end method

.method private stopPing(J)V
    .locals 11
    .param p1, "accountId"    # J

    .prologue
    .line 4799
    sget-object v7, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v7

    .line 4800
    :try_start_0
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 4801
    .local v2, "mailboxId":J
    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    .line 4802
    .local v1, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v1, :cond_0

    .line 4803
    iget-object v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 4804
    .local v4, "serverId":Ljava/lang/String;
    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    cmp-long v6, v8, p1

    if-nez v6, :cond_0

    if-eqz v4, :cond_0

    const-string v6, "__eas"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4808
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/exchange/AbstractSyncService;

    .line 4809
    .local v5, "svc":Lcom/android/exchange/AbstractSyncService;
    invoke-virtual {v5}, Lcom/android/exchange/AbstractSyncService;->reset()V

    goto :goto_0

    .line 4813
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v2    # "mailboxId":J
    .end local v4    # "serverId":Ljava/lang/String;
    .end local v5    # "svc":Lcom/android/exchange/AbstractSyncService;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4814
    return-void
.end method

.method private stopServiceThreads()V
    .locals 8

    .prologue
    .line 4848
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 4849
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4851
    .local v3, "toStop":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 4852
    .local v1, "mailboxId":Ljava/lang/Long;
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 4869
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mailboxId":Ljava/lang/Long;
    .end local v3    # "toStop":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 4855
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "toStop":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 4856
    .restart local v1    # "mailboxId":Ljava/lang/Long;
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/exchange/AbstractSyncService;

    .line 4857
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v2, :cond_2

    .line 4858
    iget-object v4, v2, Lcom/android/exchange/AbstractSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/android/exchange/AbstractSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_1

    .line 4862
    invoke-virtual {v2}, Lcom/android/exchange/AbstractSyncService;->stop()V

    .line 4863
    :cond_1
    iget-object v4, v2, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v4, :cond_2

    .line 4864
    iget-object v4, v2, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    .line 4867
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lcom/android/exchange/ExchangeService;->releaseWakeLock(J)V

    goto :goto_1

    .line 4869
    .end local v1    # "mailboxId":Ljava/lang/Long;
    .end local v2    # "svc":Lcom/android/exchange/AbstractSyncService;
    :cond_3
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4870
    return-void
.end method

.method public static unregisterCalendarObservers()V
    .locals 5

    .prologue
    .line 2720
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 2721
    .local v0, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v0, :cond_0

    .line 2728
    :goto_0
    return-void

    .line 2723
    :cond_0
    iget-object v3, v0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    .line 2724
    .local v3, "resolver":Landroid/content/ContentResolver;
    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/exchange/ExchangeService$CalendarObserver;

    .line 2725
    .local v2, "observer":Lcom/android/exchange/ExchangeService$CalendarObserver;
    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_1

    .line 2727
    .end local v2    # "observer":Lcom/android/exchange/ExchangeService$CalendarObserver;
    :cond_1
    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    goto :goto_0
.end method

.method private updateAccountDb()V
    .locals 36

    .prologue
    .line 7127
    invoke-static/range {p0 .. p0}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v25

    .line 7129
    .local v25, "isRoaming":Z
    const-string v2, "ExchangeService"

    const-string v3, "updateAccountDb start"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 7130
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getAccountList()Lcom/android/exchange/ExchangeService$AccountList;

    move-result-object v15

    .line 7132
    .local v15, "accounts":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/provider/EmailContent$Account;>;"
    monitor-enter v15

    .line 7133
    const/16 v31, 0x1

    .line 7134
    .local v31, "requireManualSync":Z
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v28

    .line 7135
    .local v28, "mContext":Landroid/content/Context;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 7137
    .local v13, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    invoke-virtual {v13}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v32

    .line 7139
    .local v32, "syncScheduleData":Lcom/android/emailcommon/utility/SyncScheduleData;
    invoke-static/range {v32 .. v32}, Lcom/android/emailcommon/utility/SyncScheduler;->getIsPeakAndNextAlarm(Lcom/android/emailcommon/utility/SyncScheduleData;)Landroid/util/Pair;

    move-result-object v2

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    .line 7141
    .local v24, "isInPeakPeriod":Z
    const/16 v29, -0x2

    .line 7144
    .local v29, "newSyncInterval":I
    const/16 v31, 0x1

    .line 7145
    if-eqz v25, :cond_1

    if-eqz v28, :cond_1

    .line 7147
    iget-wide v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, v28

    invoke-static {v0, v2, v3}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v14

    .line 7150
    .local v14, "accountPolicies":Lcom/android/emailcommon/service/PolicySet;
    if-eqz v14, :cond_1

    .line 7151
    iget-boolean v0, v14, Lcom/android/emailcommon/service/PolicySet;->mRequireManualSyncWhenRoaming:Z

    move/from16 v31, v0

    .line 7156
    .end local v14    # "accountPolicies":Lcom/android/emailcommon/service/PolicySet;
    :cond_1
    if-eqz v25, :cond_a

    invoke-virtual/range {v32 .. v32}, Lcom/android/emailcommon/utility/SyncScheduleData;->getRoamingSchedule()I

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v31, :cond_a

    .line 7163
    :cond_2
    const/16 v29, -0x1

    .line 7167
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": in roaming manual=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " db interval=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7196
    :goto_1
    iget v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move/from16 v0, v29

    if-eq v2, v0, :cond_3

    iget-object v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 7202
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 7204
    .local v19, "cv":Landroid/content/ContentValues;
    const-string v2, "syncInterval"

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 7206
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v13, v2, v0}, Lcom/android/emailcommon/provider/EmailContent$Account;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 7208
    new-instance v27, Ljava/util/Date;

    invoke-direct/range {v27 .. v27}, Ljava/util/Date;-><init>()V

    .line 7210
    .local v27, "lv_localDate":Ljava/util/Date;
    new-instance v26, Ljava/text/SimpleDateFormat;

    const-string v2, "HH:mm"

    move-object/from16 v0, v26

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 7214
    .local v26, "lv_formatter":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "peak-offpeak/roaming switching - update db at ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v26 .. v27}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] old=["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7225
    .end local v19    # "cv":Landroid/content/ContentValues;
    .end local v26    # "lv_formatter":Ljava/text/SimpleDateFormat;
    .end local v27    # "lv_localDate":Ljava/util/Date;
    :cond_3
    const/4 v8, 0x0

    .line 7226
    .local v8, "INDEX_ID":I
    const/4 v11, 0x1

    .line 7227
    .local v11, "INDEX_REF_INTERVAL":I
    const/4 v10, 0x2

    .line 7228
    .local v10, "INDEX_PEAK":I
    const/4 v9, 0x3

    .line 7230
    .local v9, "INDEX_OFFPEAK":I
    const/4 v2, 0x4

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "SyncIntervalReference"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "peakSyncSchedule"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "offpeakSyncSchedule"

    aput-object v3, v4, v2

    .line 7234
    .local v4, "PROJ":[Ljava/lang/String;
    const-string v12, "accountKey=? and (type=6 or type=12 or type=5 or type=3)"

    .line 7240
    .local v12, "SELE":Ljava/lang/String;
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7241
    .local v30, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/16 v18, 0x0

    .line 7243
    .local v18, "boxcursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "accountKey=? and (type=6 or type=12 or type=5 or type=3)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v0, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v34, v0

    invoke-static/range {v34 .. v35}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 7248
    if-eqz v18, :cond_8

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 7250
    :cond_4
    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 7251
    .local v16, "box_id":J
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 7252
    .local v23, "interval_ref_org":I
    const/16 v22, -0x1

    .line 7253
    .local v22, "interval_new":I
    if-eqz v25, :cond_c

    invoke-virtual/range {v32 .. v32}, Lcom/android/emailcommon/utility/SyncScheduleData;->getRoamingSchedule()I

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v31, :cond_c

    .line 7255
    :cond_5
    const/16 v22, -0x1

    .line 7267
    :cond_6
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    .line 7268
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mailbox id "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", interval "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "from "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 7270
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "SyncIntervalReference"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "syncInterval"

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, v30

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7285
    :cond_7
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_4

    .line 7290
    .end local v16    # "box_id":J
    .end local v22    # "interval_new":I
    .end local v23    # "interval_ref_org":I
    :cond_8
    if-eqz v18, :cond_9

    .line 7292
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 7301
    :cond_9
    :goto_3
    :try_start_3
    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->size()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-lez v2, :cond_0

    .line 7303
    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.email.provider"

    move-object/from16 v0, v30

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 7304
    :catch_0
    move-exception v20

    .line 7306
    .local v20, "e":Landroid/os/RemoteException;
    :try_start_5
    invoke-virtual/range {v20 .. v20}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 7313
    .end local v4    # "PROJ":[Ljava/lang/String;
    .end local v8    # "INDEX_ID":I
    .end local v9    # "INDEX_OFFPEAK":I
    .end local v10    # "INDEX_PEAK":I
    .end local v11    # "INDEX_REF_INTERVAL":I
    .end local v12    # "SELE":Ljava/lang/String;
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v18    # "boxcursor":Landroid/database/Cursor;
    .end local v20    # "e":Landroid/os/RemoteException;
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v24    # "isInPeakPeriod":Z
    .end local v28    # "mContext":Landroid/content/Context;
    .end local v29    # "newSyncInterval":I
    .end local v30    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v32    # "syncScheduleData":Lcom/android/emailcommon/utility/SyncScheduleData;
    :catchall_0
    move-exception v2

    monitor-exit v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 7171
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v24    # "isInPeakPeriod":Z
    .restart local v28    # "mContext":Landroid/content/Context;
    .restart local v29    # "newSyncInterval":I
    .restart local v32    # "syncScheduleData":Lcom/android/emailcommon/utility/SyncScheduleData;
    :cond_a
    if-eqz v24, :cond_b

    .line 7175
    :try_start_6
    invoke-virtual/range {v32 .. v32}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v29

    .line 7177
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ": in peak=["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " db interval=["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7185
    :cond_b
    invoke-virtual/range {v32 .. v32}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v29

    .line 7187
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ": in offpeak=["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " db interval=["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 7256
    .restart local v4    # "PROJ":[Ljava/lang/String;
    .restart local v8    # "INDEX_ID":I
    .restart local v9    # "INDEX_OFFPEAK":I
    .restart local v10    # "INDEX_PEAK":I
    .restart local v11    # "INDEX_REF_INTERVAL":I
    .restart local v12    # "SELE":Ljava/lang/String;
    .restart local v16    # "box_id":J
    .restart local v18    # "boxcursor":Landroid/database/Cursor;
    .restart local v22    # "interval_new":I
    .restart local v23    # "interval_ref_org":I
    .restart local v30    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_c
    if-eqz v24, :cond_d

    .line 7257
    :try_start_7
    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 7258
    if-nez v22, :cond_6

    .line 7259
    move/from16 v22, v29

    goto/16 :goto_2

    .line 7262
    :cond_d
    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v22

    .line 7263
    if-nez v22, :cond_6

    .line 7264
    move/from16 v22, v29

    goto/16 :goto_2

    .line 7293
    .end local v16    # "box_id":J
    .end local v22    # "interval_new":I
    .end local v23    # "interval_ref_org":I
    :catch_1
    move-exception v20

    .line 7294
    .local v20, "e":Ljava/lang/IllegalStateException;
    :try_start_8
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateAccountDB: exception when closing cursor. Just ignore it."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_3

    .line 7287
    .end local v20    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v20

    .line 7288
    .local v20, "e":Ljava/lang/Exception;
    :try_start_9
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 7290
    if-eqz v18, :cond_9

    .line 7292
    :try_start_a
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_3

    .line 7293
    :catch_3
    move-exception v20

    .line 7294
    .local v20, "e":Ljava/lang/IllegalStateException;
    :try_start_b
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateAccountDB: exception when closing cursor. Just ignore it."

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 7290
    .end local v20    # "e":Ljava/lang/IllegalStateException;
    :catchall_1
    move-exception v2

    if-eqz v18, :cond_e

    .line 7292
    :try_start_c
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 7297
    :cond_e
    :goto_4
    :try_start_d
    throw v2

    .line 7293
    :catch_4
    move-exception v20

    .line 7294
    .restart local v20    # "e":Ljava/lang/IllegalStateException;
    const-string v3, "ExchangeService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateAccountDB: exception when closing cursor. Just ignore it."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalStateException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 7307
    .end local v20    # "e":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v20

    .line 7309
    .local v20, "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 7313
    .end local v4    # "PROJ":[Ljava/lang/String;
    .end local v8    # "INDEX_ID":I
    .end local v9    # "INDEX_OFFPEAK":I
    .end local v10    # "INDEX_PEAK":I
    .end local v11    # "INDEX_REF_INTERVAL":I
    .end local v12    # "SELE":Ljava/lang/String;
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v18    # "boxcursor":Landroid/database/Cursor;
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v24    # "isInPeakPeriod":Z
    .end local v29    # "newSyncInterval":I
    .end local v30    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v32    # "syncScheduleData":Lcom/android/emailcommon/utility/SyncScheduleData;
    :cond_f
    monitor-exit v15
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 7316
    const-string v2, "ExchangeService"

    const-string v3, "updateAccountDb end"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 7317
    return-void
.end method

.method private updatePIMSyncSettings(Lcom/android/emailcommon/provider/EmailContent$Account;ILjava/lang/String;)V
    .locals 24
    .param p1, "providerAccount"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "mailboxType"    # I
    .param p3, "authority"    # Ljava/lang/String;

    .prologue
    .line 4484
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 4485
    .local v14, "cv":Landroid/content/ContentValues;
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v4, v5, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v18

    .line 4487
    .local v18, "mailboxId":J
    const-wide/16 v4, -0x1

    cmp-long v4, v18, v4

    if-eqz v4, :cond_0

    .line 4490
    new-instance v10, Landroid/accounts/Account;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v5, "com.android.exchange"

    invoke-direct {v10, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4494
    .local v10, "acct":Landroid/accounts/Account;
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v16

    .line 4495
    .local v16, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v16, :cond_1

    .line 4591
    .end local v10    # "acct":Landroid/accounts/Account;
    .end local v16    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_0
    :goto_0
    return-void

    .line 4497
    .restart local v10    # "acct":Landroid/accounts/Account;
    .restart local v16    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    move/from16 v17, v0

    .line 4499
    .local v17, "syncInterval":I
    move-object/from16 v0, p3

    invoke-static {v10, v0}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v4

    if-lez v4, :cond_8

    .line 4502
    move-object/from16 v0, p3

    invoke-static {v10, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4513
    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v4

    const/4 v5, -0x2

    if-ne v4, v5, :cond_2

    const/4 v4, -0x3

    move/from16 v0, v17

    if-ne v0, v4, :cond_2

    .line 4515
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Account syncInterval is PUSH, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " syncInterval is PING. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "This is normal for Push mode. Don\'t update mailbox syncInterval"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 4520
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v4

    move/from16 v0, v17

    if-eq v4, v0, :cond_3

    .line 4521
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": syncInterval ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4523
    const-string v4, "syncInterval"

    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4524
    const-string v4, "Saritha"

    const-string v5, "-exchange service update PIM sync settings - putting sync interval as push//schedule "

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4543
    :cond_3
    :goto_1
    const-string v4, "syncInterval"

    invoke-virtual {v14, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4545
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v18

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v14, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 4554
    :goto_2
    const/16 v4, 0x42

    move/from16 v0, p2

    if-ne v0, v4, :cond_6

    .line 4555
    const/4 v11, 0x0

    .line 4557
    .local v11, "c":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x6

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v20

    .line 4559
    .local v20, "trashServerId":Ljava/lang/String;
    if-eqz v20, :cond_5

    .line 4560
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const-string v7, "accountKey=? and type = 83 and parentServerId!=? AND displayName != \'Suggested Contacts\'"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v8, v9

    const/4 v9, 0x1

    aput-object v20, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 4565
    if-eqz v11, :cond_5

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4566
    const-wide/16 v12, -0x1

    .line 4568
    .local v12, "contactSubFolderId":J
    :cond_4
    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 4569
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v14, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4572
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_4

    .line 4580
    .end local v12    # "contactSubFolderId":J
    :cond_5
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 4581
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 4587
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v20    # "trashServerId":Ljava/lang/String;
    :cond_6
    :goto_3
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/ExchangeService;->stopPing(J)V

    .line 4588
    const-string v4, "sync settings change"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4527
    :cond_7
    const/4 v4, -0x1

    move/from16 v0, v17

    if-eq v0, v4, :cond_3

    .line 4528
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": manual"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4529
    const-string v4, "syncInterval"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 4532
    :cond_8
    move-object/from16 v0, v16

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-eqz v4, :cond_9

    move-object/from16 v0, v16

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x61

    if-ne v4, v5, :cond_a

    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v4

    move/from16 v0, v17

    if-eq v4, v0, :cond_a

    .line 4534
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4536
    const-string v4, "syncInterval"

    invoke-virtual/range {p1 .. p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 4537
    :cond_a
    const/4 v4, -0x1

    move/from16 v0, v17

    if-eq v0, v4, :cond_3

    .line 4538
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": manual"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4539
    const-string v4, "syncInterval"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 4547
    :catch_0
    move-exception v15

    .line 4548
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->errorlog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 4549
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v15

    .line 4550
    .local v15, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v15}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->errorlog(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 4575
    .end local v15    # "e":Ljava/lang/IllegalStateException;
    .restart local v11    # "c":Landroid/database/Cursor;
    :catch_2
    move-exception v15

    .line 4576
    .local v15, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    invoke-virtual {v15}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->errorlog(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4580
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 4581
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 4577
    .end local v15    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_3
    move-exception v15

    .line 4578
    .local v15, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    invoke-virtual {v15}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->errorlog(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4580
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 4581
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 4580
    .end local v15    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v4

    if-eqz v11, :cond_b

    invoke-interface {v11}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_b

    .line 4581
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v4
.end method

.method private waitForConnectivity()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 4873
    const/4 v1, 0x0

    .line 4874
    .local v1, "waiting":Z
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 4876
    .local v0, "cm":Landroid/net/ConnectivityManager;
    :goto_0
    sget-boolean v2, Lcom/android/exchange/ExchangeService;->sStop:Z

    if-nez v2, :cond_1

    .line 4877
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    .line 4879
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    if-eqz v2, :cond_0

    .line 4880
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ActiveNetworkInfo:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", State:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo$State;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", DetailedState:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo$DetailedState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4884
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    sget-object v3, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v2

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v2, v3, :cond_2

    .line 4888
    if-eqz v1, :cond_1

    .line 4890
    const/4 v2, 0x0

    invoke-virtual {p0, p0, v8, v2}, Lcom/android/exchange/ExchangeService;->releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z

    .line 4892
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->logSyncHolds()V

    .line 4922
    :cond_1
    return-void

    .line 4898
    :cond_2
    if-nez v1, :cond_3

    .line 4899
    const/4 v1, 0x1

    .line 4900
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->stopServiceThreads()V

    .line 4906
    :cond_3
    sget-object v3, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    monitor-enter v3

    .line 4907
    const-wide/16 v4, -0x1

    const-wide/32 v6, 0x93b48

    :try_start_0
    invoke-static {v4, v5, v6, v7}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4909
    :try_start_1
    const-string v2, "Connectivity lock..."

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4910
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    .line 4911
    sget-object v2, Lcom/android/exchange/ExchangeService;->sConnectivityLock:Ljava/lang/Object;

    const-wide/32 v4, 0x927c0

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 4912
    const-string v2, "Connectivity lock released..."

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4916
    const/4 v2, 0x0

    :try_start_2
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    .line 4918
    :goto_1
    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 4919
    monitor-exit v3

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 4913
    :catch_0
    move-exception v2

    .line 4916
    const/4 v2, 0x0

    :try_start_3
    sput-boolean v2, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    goto :goto_1

    :catchall_1
    move-exception v2

    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 9
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 7373
    const-string v6, "DUMP ALL THE EXCHANGE ACCOUNTS:"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7374
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Model      :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ro.product.model"

    const-string v8, "Unknown"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7375
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Build      :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ro.build.PDA"

    const-string v8, "Unknown"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7376
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ChangeList :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ro.build.changelist"

    const-string v8, "Unknown"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7378
    :try_start_0
    sget-object v6, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    if-nez v6, :cond_0

    .line 7379
    const-string v6, "There are no exchange accounts configured"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7442
    :goto_0
    return-void

    .line 7383
    :cond_0
    sget-object v6, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v6}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 7384
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_4

    .line 7385
    invoke-virtual {v0}, Lcom/android/emailcommon/provider/EmailContent$Account;->dumpAccountInfo()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7386
    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {p0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxesWithAccoutId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v4

    .line 7387
    .local v4, "mailboxes":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v4, :cond_3

    .line 7388
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Mailbox information for the account: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7390
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Total Mailboxes: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7393
    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v8, 0x44

    invoke-static {p0, v6, v7, v8}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    .line 7395
    .local v1, "accountMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v1, :cond_1

    .line 7396
    invoke-virtual {v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->dump()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7401
    :goto_2
    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v8, 0x4

    invoke-static {p0, v6, v7, v8}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v5

    .line 7403
    .local v5, "outbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v5, :cond_2

    .line 7404
    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->dump()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7408
    :goto_3
    invoke-static {p0, v5, v0}, Lcom/android/exchange/EasOutboxService;->dumpOutboxMessages(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 7438
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v1    # "accountMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "mailboxes":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v5    # "outbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_0
    move-exception v2

    .line 7439
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 7440
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 7398
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v1    # "accountMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "mailboxes":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    :try_start_1
    const-string v6, "null account mailbox"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 7406
    .restart local v5    # "outbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    const-string v6, "null account outbox"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 7410
    .end local v1    # "accountMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v5    # "outbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "there are no mailboxes for account "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7414
    .end local v4    # "mailboxes":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_4
    const-string v6, "null account"

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 7417
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Next Wait Reason="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7419
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    if-eqz v6, :cond_6

    .line 7420
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mServiceMap: \n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7423
    :cond_6
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v6, :cond_7

    .line 7424
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mSyncErrorMap: \n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7427
    :cond_7
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v6, :cond_8

    .line 7428
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mSyncStatusErrorMap: \n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7431
    :cond_8
    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    if-eqz v6, :cond_9

    .line 7432
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " mProvisionErrorExist: \n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/ExchangeService;->mProvisionErrorExist:Ljava/util/HashMap;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7434
    :cond_9
    sget-object v6, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-static {p0, p2, v6}, Lcom/android/emailcommon/provider/EmailContent$Policies;->dump(Landroid/content/Context;Ljava/io/PrintWriter;Ljava/util/List;)V

    .line 7436
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " \nRestrictBackgroundData= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->getAppRestrictBackground()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 7437
    sget-object v6, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    invoke-virtual {v6, p2}, Lcom/android/exchange/ServiceLogger;->dumpLogStats(Ljava/io/PrintWriter;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public getStatusErrorCount(JI)I
    .locals 3
    .param p1, "mailboxId"    # J
    .param p3, "reason"    # I

    .prologue
    .line 3155
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncStatusError;

    .line 3156
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncStatusError;
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/android/exchange/ExchangeService$SyncStatusError;->reason:I

    if-ne v1, p3, :cond_0

    .line 3157
    iget v1, v0, Lcom/android/exchange/ExchangeService$SyncStatusError;->count:I

    .line 3159
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMailboxSyncable(Lcom/android/emailcommon/provider/EmailContent$Account;I)Z
    .locals 8
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "type"    # I

    .prologue
    const/16 v7, 0x52

    const/16 v4, 0x42

    const/16 v6, 0x41

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5682
    if-eq p2, v4, :cond_0

    if-eq p2, v6, :cond_0

    const/16 v3, 0x53

    if-eq p2, v3, :cond_0

    if-ne p2, v7, :cond_8

    .line 5685
    :cond_0
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_2

    .line 5740
    :cond_1
    :goto_0
    return v1

    .line 5690
    :cond_2
    if-eq p2, v4, :cond_3

    const/16 v3, 0x53

    if-ne p2, v3, :cond_7

    .line 5694
    :cond_3
    const-string v0, "com.android.contacts"

    .line 5704
    .local v0, "authority":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mAmAccount:Landroid/accounts/Account;

    invoke-static {v3, v0}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5708
    if-eq p2, v6, :cond_5

    if-ne p2, v7, :cond_6

    :cond_5
    iget-wide v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/ExchangeService;->isCalendarEnabled(J)Z

    move-result v3

    if-eqz v3, :cond_1

    .end local v0    # "authority":Ljava/lang/String;
    :cond_6
    move v1, v2

    .line 5740
    goto :goto_0

    .line 5696
    :cond_7
    const-string v0, "com.android.calendar"

    .line 5697
    .restart local v0    # "authority":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/exchange/ExchangeService;->mCalendarObservers:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 5700
    invoke-direct {p0, p1}, Lcom/android/exchange/ExchangeService;->registerCalendarObserver(Lcom/android/emailcommon/provider/EmailContent$Account;)V

    goto :goto_1

    .line 5713
    .end local v0    # "authority":Ljava/lang/String;
    :cond_8
    const/4 v3, 0x6

    if-eq p2, v3, :cond_1

    .line 5726
    const/16 v3, 0x43

    if-ne p2, v3, :cond_9

    .line 5731
    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mAmAccount:Landroid/accounts/Account;

    const-string v4, "tasks"

    invoke-static {v3, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 5734
    goto :goto_0

    .line 5735
    :cond_9
    const/4 v3, 0x4

    if-eq p2, v3, :cond_6

    const/16 v3, 0x44

    if-eq p2, v3, :cond_6

    invoke-static {p0, p1}, Lcom/android/exchange/ExchangeService;->canAutoSync(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mAmAccount:Landroid/accounts/Account;

    invoke-direct {p0, v3}, Lcom/android/exchange/ExchangeService;->canSyncEmail(Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/android/exchange/ExchangeService;->mBackgroundData:Z

    if-nez v3, :cond_6

    goto :goto_0
.end method

.method maybeStartExchangeServiceThread()V
    .locals 4

    .prologue
    .line 5231
    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 5232
    :cond_0
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "protocol=\"eas\""

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    .line 5233
    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    if-nez v0, :cond_2

    const-string v0, "Starting thread..."

    :goto_0
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5234
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "ExchangeService"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    sput-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    .line 5235
    sput-object p0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 5240
    invoke-direct {p0, p0}, Lcom/android/exchange/ExchangeService;->runAccountReconcilerSync(Landroid/content/Context;)V

    .line 5242
    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 5243
    sget-object v0, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ServiceThread=started sServiceThread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Service="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/ServiceLogger;->logEASServiceStats(Ljava/lang/String;)V

    .line 5247
    :cond_1
    return-void

    .line 5233
    :cond_2
    const-string v0, "Restarting thread..."

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 3484
    iget-object v0, p0, Lcom/android/exchange/ExchangeService;->mBinder:Lcom/android/emailcommon/service/IEmailService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 4956
    sget-object v1, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Service=created sStop="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/android/exchange/ExchangeService;->sStop:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/ServiceLogger;->logEASServiceStats(Ljava/lang/String;)V

    .line 4958
    invoke-static {p0}, Lcom/android/exchange/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/exchange/Preferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;

    .line 4959
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;

    invoke-virtual {v1}, Lcom/android/exchange/Preferences;->getDebugBits()I

    move-result v1

    invoke-static {v1}, Lcom/android/emailcommon/EasRefs;->setUserDebug(I)V

    .line 4963
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 4964
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z

    .line 4979
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/exchange/ExchangeService;->isWiFiConnected:Z

    .line 4981
    sget-boolean v1, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z

    if-eqz v1, :cond_0

    .line 4982
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Exchange] adShutdownCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4983
    const/4 v1, 0x0

    sput v1, Lcom/android/exchange/ExchangeService;->adShutdownCount:I

    .line 4986
    :cond_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v1}, Lcom/android/exchange/ExchangeService$AccountList;->clear()V

    .line 4987
    sget-object v2, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v2

    .line 4997
    :try_start_0
    new-instance v1, Lcom/android/exchange/ExchangeService$8;

    invoke-direct {v1, p0}, Lcom/android/exchange/ExchangeService$8;-><init>(Lcom/android/exchange/ExchangeService;)V

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 5032
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5034
    new-instance v1, Lcom/android/exchange/ExchangeService$9;

    invoke-direct {v1, p0}, Lcom/android/exchange/ExchangeService$9;-><init>(Lcom/android/exchange/ExchangeService;)V

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 5060
    return-void

    .line 5032
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    .line 5195
    const-string v0, "!!! EAS ExchangeService, onDestroy"

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5196
    sget-object v1, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service=destroyed sStop="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/exchange/ExchangeService;->sStop:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " sServiceThread="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/exchange/ServiceLogger;->logEASServiceStats(Ljava/lang/String;)V

    .line 5199
    new-instance v0, Lcom/android/exchange/ExchangeService$11;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$11;-><init>(Lcom/android/exchange/ExchangeService;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 5226
    return-void

    .line 5196
    :cond_0
    const-string v0, "Died"

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v1, 0x1

    .line 5064
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "!!! EAS ExchangeService, onStartCommand, startingUp = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v2, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", running = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 5066
    sget-boolean v0, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v0, :cond_0

    .line 5067
    sput-boolean v1, Lcom/android/exchange/ExchangeService;->sStartingUp:Z

    .line 5068
    new-instance v0, Lcom/android/exchange/ExchangeService$10;

    invoke-direct {v0, p0}, Lcom/android/exchange/ExchangeService$10;-><init>(Lcom/android/exchange/ExchangeService;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 5190
    :cond_0
    return v1

    .line 5064
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # I
    .param p3, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 3243
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/ExchangeService;->releaseSyncHoldsImpl(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v0

    .line 3244
    .local v0, "holdWasReleased":Z
    const-string v1, "security release"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 3245
    return v0
.end method

.method releaseSyncHoldsForEasMailbox(Landroid/content/Context;IJ)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reason"    # I
    .param p3, "accountId"    # J

    .prologue
    .line 3141
    const-string v1, "releaseSyncHoldsForEasMailbox(): Entry"

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 3142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/exchange/ExchangeService;->releaseSyncHoldsImplForEasMailbox(Landroid/content/Context;IJ)Z

    move-result v0

    .line 3143
    .local v0, "holdWasReleased":Z
    return v0
.end method

.method public releaseWakeLock(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 4286
    iget-object v2, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    monitor-enter v2

    .line 4287
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 4288
    .local v0, "lock":Ljava/lang/Boolean;
    if-eqz v0, :cond_1

    .line 4289
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4290
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLocks:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4291
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    .line 4292
    iget-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 4294
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/exchange/ExchangeService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 4299
    :cond_1
    monitor-exit v2

    .line 4300
    return-void

    .line 4299
    .end local v0    # "lock":Ljava/lang/Boolean;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 5266
    sput-boolean v4, Lcom/android/exchange/ExchangeService;->sStop:Z

    .line 5267
    const-string v4, "ExchangeService thread running"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 5269
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 5270
    sput-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    .line 5271
    sput-boolean v5, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    .line 5272
    sput-boolean v5, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    .line 5275
    :cond_0
    invoke-static {p0}, Lcom/android/emailcommon/TempDirectory;->setTempDirectory(Landroid/content/Context;)V

    .line 5277
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->WAIT_DEBUG:Z

    if-eqz v4, :cond_1

    .line 5278
    invoke-static {}, Landroid/os/Debug;->waitForDebugger()V

    .line 5284
    :cond_1
    sget-object v5, Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 5285
    :try_start_0
    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v4, :cond_3

    .line 5286
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    .line 5292
    new-instance v4, Lcom/android/exchange/ExchangeService$AccountObserver;

    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mHandler:Landroid/os/Handler;

    invoke-direct {v4, p0, v6}, Lcom/android/exchange/ExchangeService$AccountObserver;-><init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    .line 5293
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Account;->NOTIFIER_URI:Landroid/net/Uri;

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/exchange/ExchangeService;->mAccountObserver:Lcom/android/exchange/ExchangeService$AccountObserver;

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 5294
    new-instance v4, Lcom/android/exchange/ExchangeService$MailboxObserver;

    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mHandler:Landroid/os/Handler;

    invoke-direct {v4, p0, v6}, Lcom/android/exchange/ExchangeService$MailboxObserver;-><init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

    .line 5295
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/android/exchange/ExchangeService;->mMailboxObserver:Lcom/android/exchange/ExchangeService$MailboxObserver;

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 5296
    new-instance v4, Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mHandler:Landroid/os/Handler;

    invoke-direct {v4, p0, v6}, Lcom/android/exchange/ExchangeService$SyncedMessageObserver;-><init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    .line 5297
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->SYNCED_CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/exchange/ExchangeService;->mSyncedMessageObserver:Lcom/android/exchange/ExchangeService$SyncedMessageObserver;

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 5299
    new-instance v4, Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;

    invoke-direct {v4, p0}, Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;-><init>(Lcom/android/exchange/ExchangeService;)V

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusObserver:Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;

    .line 5300
    const/4 v4, 0x1

    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mSyncStatusObserver:Lcom/android/exchange/ExchangeService$EasSyncStatusObserver;

    invoke-static {v4, v6}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mStatusChangeListener:Ljava/lang/Object;

    .line 5303
    new-instance v4, Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    invoke-direct {v4, p0}, Lcom/android/exchange/ExchangeService$ConnectivityReceiver;-><init>(Lcom/android/exchange/ExchangeService;)V

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    .line 5304
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mConnectivityReceiver:Lcom/android/exchange/ExchangeService$ConnectivityReceiver;

    new-instance v6, Landroid/content/IntentFilter;

    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4, v6}, Lcom/android/exchange/ExchangeService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 5328
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "phone"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 5330
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    if-eqz v4, :cond_2

    .line 5331
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v6, p0, Lcom/android/exchange/ExchangeService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v7, 0x1

    invoke-virtual {v4, v6, v7}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 5347
    :cond_2
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Lcom/android/exchange/ExchangeService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 5348
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mBackgroundData:Z

    .line 5365
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    :cond_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 5368
    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v4, :cond_4

    .line 5370
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->checkPIMSyncSettings()V

    .line 5373
    :cond_4
    sget-object v4, Lcom/android/exchange/ExchangeService;->mServiceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ServiceThread=running sServiceThread="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sStop="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/android/exchange/ExchangeService;->sStop:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Service="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/exchange/ServiceLogger;->logEASServiceStats(Ljava/lang/String;)V

    .line 5377
    :goto_0
    :try_start_1
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->sStop:Z

    if-nez v4, :cond_c

    .line 5378
    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->runAwake(J)V

    .line 5379
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->waitForConnectivity()V

    .line 5380
    const-string v4, "Heartbeat"

    iput-object v4, p0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    .line 5383
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->updateAccountDb()V

    .line 5385
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->checkMailboxes()J
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v2

    .line 5387
    .local v2, "nextWait":J
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 5388
    :try_start_3
    iget-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    if-nez v4, :cond_8

    .line 5389
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_5

    .line 5390
    const-string v4, "Negative wait? Setting to 1s"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5391
    const-wide/16 v2, 0x3e8

    .line 5393
    :cond_5
    const-wide/16 v4, 0x2710

    cmp-long v4, v2, v4

    if-lez v4, :cond_7

    .line 5394
    iget-object v4, p0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    if-eqz v4, :cond_6

    .line 5395
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Next awake "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/ExchangeService;->mNextWaitReason:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 5397
    :cond_6
    const-wide/16 v4, -0x1

    const-wide/16 v6, 0xbb8

    add-long/2addr v6, v2

    invoke-static {v4, v5, v6, v7}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5399
    :cond_7
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 5401
    :cond_8
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 5406
    :try_start_4
    monitor-enter p0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 5407
    :try_start_5
    iget-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    if-eqz v4, :cond_9

    .line 5409
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    .line 5411
    :cond_9
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5415
    .end local v2    # "nextWait":J
    :catch_0
    move-exception v1

    .line 5416
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_7
    const-string v4, "ExchangeService"

    const-string v5, "RuntimeException in ExchangeService"

    invoke-static {v4, v5, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 5417
    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 5419
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v4

    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->shutdown()V

    throw v4

    .line 5365
    :catchall_2
    move-exception v4

    :try_start_8
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v4

    .line 5401
    .restart local v2    # "nextWait":J
    :catchall_3
    move-exception v4

    :try_start_9
    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :try_start_a
    throw v4
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    .line 5402
    :catch_1
    move-exception v1

    .line 5404
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_b
    const-string v4, "ExchangeService interrupted"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    .line 5406
    :try_start_c
    monitor-enter p0
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 5407
    :try_start_d
    iget-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    if-eqz v4, :cond_a

    .line 5409
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    .line 5411
    :cond_a
    monitor-exit p0

    goto/16 :goto_0

    :catchall_4
    move-exception v4

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :try_start_e
    throw v4

    .line 5406
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_5
    move-exception v4

    monitor-enter p0
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 5407
    :try_start_f
    iget-boolean v5, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    if-eqz v5, :cond_b

    .line 5409
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/exchange/ExchangeService;->mKicked:Z

    .line 5411
    :cond_b
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    :try_start_10
    throw v4
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :catchall_6
    move-exception v4

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    :try_start_12
    throw v4

    .line 5414
    .end local v2    # "nextWait":J
    :cond_c
    const-string v4, "Shutdown requested"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 5419
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService;->shutdown()V

    .line 5421
    return-void
.end method
