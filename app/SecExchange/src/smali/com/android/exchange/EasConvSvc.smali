.class public Lcom/android/exchange/EasConvSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasConvSvc.java"


# instance fields
.field private mConversationId:[B

.field mIgnore:I

.field mToMailboxId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;JJ[BI)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailboxId"    # J
    .param p4, "_toMailboxId"    # J
    .param p6, "_conversationId"    # [B
    .param p7, "_ignore"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;J)V

    .line 40
    iput-object p6, p0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    .line 41
    iput-wide p4, p0, Lcom/android/exchange/EasConvSvc;->mToMailboxId:J

    .line 42
    iput p7, p0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    .line 43
    iget-object v0, p0, Lcom/android/exchange/EasConvSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasConvSvc;->mProtocolVersion:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/android/exchange/EasConvSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasConvSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 45
    return-void
.end method

.method private moveConvAlwaysCb([BIII)V
    .locals 1
    .param p1, "convId"    # [B
    .param p2, "status"    # I
    .param p3, "progress"    # I
    .param p4, "ignore"    # I

    .prologue
    .line 49
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->moveConvAlwaysStatus([BIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private moveConversationAlways()I
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V

    .line 58
    const/16 v16, 0x0

    .line 60
    .local v16, "result":I
    new-instance v18, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 61
    .local v18, "s":Lcom/android/exchange/adapter/Serializer;
    const/4 v7, 0x0

    .line 63
    .local v7, "commandType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    const-wide/high16 v22, 0x402c000000000000L    # 14.0

    cmpl-double v20, v20, v22

    if-ltz v20, :cond_1

    .line 64
    const-string v7, "ItemOperations"

    .line 65
    const/16 v20, 0x505

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 72
    sget-object v20, Lcom/android/exchange/EasConvSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/EasConvSvc;->mToMailboxId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v8

    .line 73
    .local v8, "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v8, :cond_0

    .line 74
    const/16 v16, 0x0

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, -0x3

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V

    move/from16 v17, v16

    .line 158
    .end local v8    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v16    # "result":I
    .local v17, "result":I
    :goto_0
    return v17

    .line 80
    .end local v17    # "result":I
    .restart local v8    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v16    # "result":I
    :cond_0
    const/16 v20, 0x516

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    const/16 v21, 0x518

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/android/exchange/adapter/Serializer;->dataOpaque(I[B)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    const/16 v21, 0x517

    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    const/16 v21, 0x508

    invoke-virtual/range {v20 .. v21}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    const/16 v21, 0x519

    invoke-virtual/range {v20 .. v21}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 85
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 86
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 89
    .end local v8    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    if-nez v7, :cond_2

    .line 90
    const/16 v16, -0x2

    move/from16 v17, v16

    .line 91
    .end local v16    # "result":I
    .restart local v17    # "result":I
    goto :goto_0

    .line 94
    .end local v17    # "result":I
    .restart local v16    # "result":I
    :cond_2
    const/4 v15, 0x0

    .line 97
    .local v15, "res":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v7, v1}, Lcom/android/exchange/EasConvSvc;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v15

    .line 100
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v6

    .local v6, "code":I
    const/16 v20, 0xc8

    move/from16 v0, v20

    if-ne v6, v0, :cond_9

    .line 104
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v13

    .local v13, "len":I
    if-lez v13, :cond_8

    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .local v12, "is":Ljava/io/InputStream;
    if-eqz v12, :cond_8

    .line 105
    const/16 v19, 0x1

    .line 107
    .local v19, "successp":Z
    :try_start_1
    new-instance v11, Lcom/android/exchange/adapter/ItemOperationsParser;

    new-instance v20, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v20

    invoke-direct {v11, v12, v0}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 109
    .local v11, "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    invoke-virtual {v11}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v19

    .line 122
    .end local v11    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    :goto_1
    if-eqz v19, :cond_7

    .line 123
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 134
    :cond_3
    :goto_2
    const/16 v16, 0x0

    .line 154
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v19    # "successp":Z
    :goto_3
    if-eqz v15, :cond_4

    .line 155
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    :cond_4
    move/from16 v17, v16

    .line 158
    .end local v16    # "result":I
    .restart local v17    # "result":I
    goto/16 :goto_0

    .line 110
    .end local v17    # "result":I
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v16    # "result":I
    .restart local v19    # "successp":Z
    :catch_0
    move-exception v10

    .line 111
    .local v10, "ioe":Ljava/io/IOException;
    const/16 v19, 0x0

    .line 112
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/high16 v21, 0x80000

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 149
    .end local v6    # "code":I
    .end local v10    # "ioe":Ljava/io/IOException;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v19    # "successp":Z
    :catch_1
    move-exception v9

    .line 150
    .local v9, "e":Ljava/lang/Exception;
    const/16 v16, 0x0

    .line 151
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, -0x3

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 154
    if-eqz v15, :cond_5

    .line 155
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    :cond_5
    move/from16 v17, v16

    .end local v16    # "result":I
    .restart local v17    # "result":I
    goto/16 :goto_0

    .line 115
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v17    # "result":I
    .restart local v6    # "code":I
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v16    # "result":I
    .restart local v19    # "successp":Z
    :catch_2
    move-exception v14

    .line 116
    .local v14, "ome":Ljava/lang/OutOfMemoryError;
    const/16 v19, 0x0

    .line 119
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/high16 v21, 0x90000

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 154
    .end local v6    # "code":I
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v14    # "ome":Ljava/lang/OutOfMemoryError;
    .end local v19    # "successp":Z
    :catchall_0
    move-exception v20

    if-eqz v15, :cond_6

    .line 155
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    :cond_6
    throw v20

    .line 128
    .restart local v6    # "code":I
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v19    # "successp":Z
    :cond_7
    :try_start_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/exchange/EasConvSvc;->isProvisionError(I)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, 0x17

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V

    goto/16 :goto_2

    .line 136
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v19    # "successp":Z
    :cond_8
    const/16 v16, 0x0

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, 0x15

    const/16 v22, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V

    goto/16 :goto_3

    .line 141
    .end local v13    # "len":I
    :cond_9
    const/16 v16, 0x15

    .line 143
    invoke-static {v6}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 144
    const/16 v16, 0x16

    .line 146
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    move-object/from16 v20, v0

    const/16 v21, 0x64

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v16

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 147
    const/16 v16, 0x0

    goto/16 :goto_3
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/android/exchange/EasConvSvc;->setupService()Z

    .line 165
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasConvSvc;->mDeviceId:Ljava/lang/String;

    .line 167
    invoke-direct {p0}, Lcom/android/exchange/EasConvSvc;->moveConversationAlways()I

    move-result v1

    .line 168
    .local v1, "result":I
    packed-switch v1, :pswitch_data_0

    .line 176
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    :goto_0
    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/EasConvSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasConvSvc;->userLog([Ljava/lang/String;)V

    .line 192
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 194
    .end local v1    # "result":I
    :goto_1
    return-void

    .line 170
    .restart local v1    # "result":I
    :pswitch_0
    const/4 v2, 0x2

    :try_start_1
    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_2
    const-string v2, "DeviceAccessPermission"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Caught Exceptoin, Device is blocked or quarantined "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/DeviceAccessException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v2, 0x6

    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I

    .line 183
    iget-object v2, p0, Lcom/android/exchange/EasConvSvc;->mConversationId:[B

    const/4 v3, 0x3

    const/16 v4, 0x64

    iget v5, p0, Lcom/android/exchange/EasConvSvc;->mIgnore:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/android/exchange/EasConvSvc;->moveConvAlwaysCb([BIII)V

    .line 184
    iget-object v2, p0, Lcom/android/exchange/EasConvSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v4, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v2, v3, v4}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/EasConvSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasConvSvc;->userLog([Ljava/lang/String;)V

    .line 192
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_1

    .line 173
    .end local v0    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v1    # "result":I
    :pswitch_1
    const/4 v2, 0x3

    :try_start_3
    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 185
    .end local v1    # "result":I
    :catch_1
    move-exception v0

    .line 186
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x1

    :try_start_4
    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 191
    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/EasConvSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasConvSvc;->userLog([Ljava/lang/String;)V

    .line 192
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_1

    .line 187
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v2, "Exception caught in EasConvSvc"

    invoke-virtual {p0, v2, v0}, Lcom/android/exchange/EasConvSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 189
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/exchange/EasConvSvc;->mExitStatus:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 191
    new-array v2, v8, [Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/EasConvSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v3, v2, v6

    const-string v3, ": sync finished"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasConvSvc;->userLog([Ljava/lang/String;)V

    .line 192
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    goto/16 :goto_1

    .line 191
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    new-array v3, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/EasConvSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v6

    const-string v4, ": sync finished"

    aput-object v4, v3, v7

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasConvSvc;->userLog([Ljava/lang/String;)V

    .line 192
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    throw v2

    .line 168
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
