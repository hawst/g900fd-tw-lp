.class public Lcom/android/exchange/DeviceInformationTask;
.super Landroid/os/AsyncTask;
.source "DeviceInformationTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDeviceInfo:Lcom/android/exchange/DeviceInformation;

.field private mSvc:Lcom/android/exchange/EasSyncService;


# direct methods
.method public constructor <init>(Lcom/android/exchange/DeviceInformation;)V
    .locals 2
    .param p1, "dInfo"    # Lcom/android/exchange/DeviceInformation;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 17
    const-string v0, "DeviceInformationTask"

    iput-object v0, p0, Lcom/android/exchange/DeviceInformationTask;->TAG:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/android/exchange/DeviceInformationTask;->mSvc:Lcom/android/exchange/EasSyncService;

    .line 34
    iput-object v1, p0, Lcom/android/exchange/DeviceInformationTask;->mDeviceInfo:Lcom/android/exchange/DeviceInformation;

    .line 37
    iput-object p1, p0, Lcom/android/exchange/DeviceInformationTask;->mDeviceInfo:Lcom/android/exchange/DeviceInformation;

    .line 38
    return-void
.end method

.method private deviceInfoCb(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 55
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->deviceInfoStatus(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/exchange/DeviceInformationTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 70
    const/16 v3, 0x1388

    invoke-direct {p0, v3}, Lcom/android/exchange/DeviceInformationTask;->deviceInfoCb(I)V

    .line 71
    const/4 v2, 0x0

    .line 73
    .local v2, "result":I
    iget-object v3, p0, Lcom/android/exchange/DeviceInformationTask;->mSvc:Lcom/android/exchange/EasSyncService;

    if-eqz v3, :cond_0

    .line 75
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/DeviceInformationTask;->mDeviceInfo:Lcom/android/exchange/DeviceInformation;

    invoke-virtual {v3}, Lcom/android/exchange/DeviceInformation;->buildCommand()Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 76
    .local v0, "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    if-eqz v0, :cond_1

    .line 77
    iget-object v3, p0, Lcom/android/exchange/DeviceInformationTask;->mSvc:Lcom/android/exchange/EasSyncService;

    iget-object v4, p0, Lcom/android/exchange/DeviceInformationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, v0}, Lcom/android/exchange/EasSyncService;->sendDeviceInformation(Landroid/content/Context;Lcom/android/exchange/adapter/Serializer;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 89
    :goto_0
    invoke-direct {p0, v2}, Lcom/android/exchange/DeviceInformationTask;->deviceInfoCb(I)V

    .line 92
    .end local v0    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :cond_0
    :goto_1
    const/4 v3, 0x0

    return-object v3

    .line 79
    .restart local v0    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :cond_1
    :try_start_1
    const-string v3, "DeviceInformationTask"

    const-string v4, "deviceSerializer is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    .end local v0    # "deviceSerializer":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v1

    .line 82
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/16 v2, 0x138a

    .line 83
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89
    invoke-direct {p0, v2}, Lcom/android/exchange/DeviceInformationTask;->deviceInfoCb(I)V

    goto :goto_1

    .line 84
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 85
    .local v1, "e":Ljava/io/IOException;
    const/16 v2, 0x1389

    .line 86
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 89
    invoke-direct {p0, v2}, Lcom/android/exchange/DeviceInformationTask;->deviceInfoCb(I)V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    invoke-direct {p0, v2}, Lcom/android/exchange/DeviceInformationTask;->deviceInfoCb(I)V

    throw v3
.end method

.method public setUpService(Landroid/content/Context;Lcom/android/exchange/EasSyncService;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "svc"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 49
    iput-object p2, p0, Lcom/android/exchange/DeviceInformationTask;->mSvc:Lcom/android/exchange/EasSyncService;

    .line 50
    iput-object p1, p0, Lcom/android/exchange/DeviceInformationTask;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method
