.class public Lcom/android/exchange/adapter/FolderSyncParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "FolderSyncParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/FolderSyncParser$1;,
        Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;
    }
.end annotation


# static fields
.field private static final MAILBOX_ID_COLUMNS_PROJECTION:[Ljava/lang/String;

.field private static final MAILBOX_STATE_PROJECTION:[Ljava/lang/String;

.field private static final UNINITIALIZED_PARENT_KEY:Landroid/content/ContentValues;

.field public static final VALID_EAS_FOLDER_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAccountId:J

.field private final mAccountIdAsString:Ljava/lang/String;

.field private final mBindArguments:[Ljava/lang/String;

.field private final mCalendarSubFolderDeleteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mCalendarSubFolderMovedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContactSubFolderDeleteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContactSubFolderMovedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFixupUninitializedNeeded:Z

.field private mInitialSync:Z

.field private final mOperations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;"
        }
    .end annotation
.end field

.field final mSyncOptionsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 100
    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/FolderSyncParser;->VALID_EAS_FOLDER_TYPES:Ljava/util/List;

    .line 120
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v7

    const-string v1, "serverId"

    aput-object v1, v0, v3

    const-string v1, "parentServerId"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/exchange/adapter/FolderSyncParser;->MAILBOX_ID_COLUMNS_PROJECTION:[Ljava/lang/String;

    .line 150
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sput-object v0, Lcom/android/exchange/adapter/FolderSyncParser;->UNINITIALIZED_PARENT_KEY:Landroid/content/ContentValues;

    .line 356
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "serverId"

    aput-object v1, v0, v7

    const-string v1, "syncInterval"

    aput-object v1, v0, v3

    const-string v1, "syncLookback"

    aput-object v1, v0, v4

    const-string v1, "offpeakSyncSchedule"

    aput-object v1, v0, v5

    const-string v1, "peakSyncSchedule"

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/exchange/adapter/FolderSyncParser;->MAILBOX_STATE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 4
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderDeleteList:Ljava/util/ArrayList;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderMovedList:Ljava/util/ArrayList;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderDeleteList:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderMovedList:Ljava/util/ArrayList;

    .line 144
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mOperations:Ljava/util/ArrayList;

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mFixupUninitializedNeeded:Z

    .line 153
    sget-object v0, Lcom/android/exchange/adapter/FolderSyncParser;->UNINITIALIZED_PARENT_KEY:Landroid/content/ContentValues;

    const-string v1, "parentKey"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 367
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mSyncOptionsMap:Ljava/util/HashMap;

    .line 158
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    .line 159
    iget-wide v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountIdAsString:Ljava/lang/String;

    .line 160
    return-void
.end method

.method private commitMailboxes(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Mailbox;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Mailbox;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/emailcommon/provider/EmailContent$Mailbox;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "validMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .local p2, "userMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .local p3, "mailboxMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .local p4, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 807
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 808
    .local v2, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    invoke-virtual {p0, v2, p3}, Lcom/android/exchange/adapter/FolderSyncParser;->isValidMailFolder(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/util/HashMap;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 809
    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 810
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 812
    :cond_0
    new-array v5, v3, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Rejecting unknown type mailbox: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 817
    .end local v2    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 818
    .restart local v2    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "Adding mailbox: "

    aput-object v6, v5, v4

    iget-object v6, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v6, v5, v3

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 819
    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->toContentValues()Landroid/content/ContentValues;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 824
    .end local v2    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    const-string v5, "Applying "

    iget-object v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const-string v7, " mailbox operations."

    invoke-virtual {p0, v5, v6, v7}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 830
    :try_start_0
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v6, "com.android.email.provider"

    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mOperations:Ljava/util/ArrayList;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 837
    :goto_2
    return v3

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/os/RemoteException;
    new-array v3, v3, [Ljava/lang/String;

    const-string v5, "RemoteException in commitMailboxes"

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    move v3, v4

    .line 834
    goto :goto_2

    .line 835
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 836
    .local v0, "e":Landroid/content/OperationApplicationException;
    new-array v3, v3, [Ljava/lang/String;

    const-string v5, "OperationApplicationException in commitMailboxes"

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    move v3, v4

    .line 837
    goto :goto_2
.end method

.method private getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 269
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountIdAsString:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 270
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/FolderSyncParser;->MAILBOX_ID_COLUMNS_PROJECTION:[Ljava/lang/String;

    const-string v3, "serverId=? and accountKey=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .locals 5
    .param p1, "parentServerId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 843
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v1, v2, v3, p1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithServerId(Landroid/content/Context;JLjava/lang/String;)[Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 845
    .local v0, "result":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v0, :cond_0

    .line 846
    const/4 v1, 0x0

    .line 851
    :goto_0
    return-object v1

    .line 848
    :cond_0
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    if-eqz v1, :cond_1

    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 849
    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    aput-object v1, v0, v4

    .line 851
    :cond_1
    aget-object v1, v0, v4

    goto :goto_0
.end method


# virtual methods
.method public addParser()Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 427
    const/4 v6, 0x0

    .line 428
    .local v6, "name":Ljava/lang/String;
    const/4 v11, 0x0

    .line 429
    .local v11, "serverId":Ljava/lang/String;
    const/4 v9, 0x0

    .line 430
    .local v9, "parentId":Ljava/lang/String;
    const/4 v13, 0x0

    .line 432
    .local v13, "type":I
    :goto_0
    const/16 v14, 0x1cf

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v14

    const/4 v15, 0x3

    if-eq v14, v15, :cond_0

    .line 433
    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    packed-switch v14, :pswitch_data_0

    .line 451
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->skipTag()V

    goto :goto_0

    .line 435
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 436
    goto :goto_0

    .line 439
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValueInt()I

    move-result v13

    .line 440
    goto :goto_0

    .line 443
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 444
    goto :goto_0

    .line 447
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 448
    goto :goto_0

    .line 455
    :cond_0
    sget-object v14, Lcom/android/exchange/adapter/FolderSyncParser;->VALID_EAS_FOLDER_TYPES:Ljava/util/List;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 456
    new-instance v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 457
    .local v5, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iput-object v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 458
    iput-object v11, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 459
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    iput-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 460
    const/4 v14, 0x1

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 465
    const/4 v14, -0x1

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 467
    const/4 v14, -0x1

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 468
    const/4 v14, -0x1

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    .line 470
    packed-switch v13, :pswitch_data_1

    .line 626
    :pswitch_4
    const/16 v14, 0x60

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 632
    :cond_1
    :goto_1
    iget v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v15, 0x40

    if-ge v14, v15, :cond_9

    const/4 v14, 0x1

    :goto_2
    iput-boolean v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlagVisible:Z

    .line 634
    if-eqz v9, :cond_2

    const-string v14, "0"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 637
    iput-object v9, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    .line 646
    .end local v5    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    :goto_3
    return-object v5

    .line 472
    .restart local v5    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :pswitch_5
    const/16 v14, 0xc

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto :goto_1

    .line 475
    :pswitch_6
    const/4 v14, 0x0

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 477
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 480
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    const-string v15, "phone"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/telephony/TelephonyManager;

    .line 481
    .local v12, "tm":Landroid/telephony/TelephonyManager;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v4

    .line 482
    .local v4, "isRoaming":Z
    if-eqz v4, :cond_6

    .line 483
    const/4 v10, 0x1

    .line 484
    .local v10, "requireManualSync":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 485
    .local v2, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v3

    .line 487
    .local v3, "accountPolicies":Lcom/android/emailcommon/service/PolicySet;
    if-eqz v3, :cond_3

    .line 488
    iget-boolean v10, v3, Lcom/android/emailcommon/service/PolicySet;->mRequireManualSyncWhenRoaming:Z

    .line 489
    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "FolderSyncParser"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "requireManualSync is "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 491
    :cond_3
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getRoamingSchedule()I

    move-result v14

    if-eqz v14, :cond_5

    :cond_4
    if-eqz v10, :cond_6

    .line 493
    :cond_5
    const/4 v14, -0x1

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 494
    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "FolderSyncParser"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "set inbox default syncInterval : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget v0, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 500
    .end local v2    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v3    # "accountPolicies":Lcom/android/emailcommon/service/PolicySet;
    .end local v10    # "requireManualSync":Z
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 501
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 506
    .end local v4    # "isRoaming":Z
    .end local v12    # "tm":Landroid/telephony/TelephonyManager;
    :pswitch_7
    const/16 v14, 0x42

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 507
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 509
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 510
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 520
    :pswitch_8
    const/4 v14, 0x4

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 523
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    const-wide v16, 0x4028333333333333L    # 12.1

    cmpl-double v14, v14, v16

    if-lez v14, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v14, v14, 0x800

    if-eqz v14, :cond_1

    .line 526
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 529
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 531
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 535
    :catch_0
    move-exception v7

    .line 536
    .local v7, "nfe":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto/16 :goto_1

    .line 537
    .end local v7    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v8

    .line 538
    .local v8, "npe":Ljava/lang/NullPointerException;
    invoke-virtual {v8}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_1

    .line 543
    .end local v8    # "npe":Ljava/lang/NullPointerException;
    :pswitch_9
    const/4 v14, 0x5

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 546
    :pswitch_a
    const/4 v14, 0x3

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 549
    :pswitch_b
    const/4 v14, 0x6

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 552
    :pswitch_c
    const/16 v14, 0x41

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 553
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 555
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 556
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 562
    :pswitch_d
    if-eqz v9, :cond_7

    const-string v14, "0"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    iget-object v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    const-string v15, "Unwanted"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 564
    const/4 v14, 0x7

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 566
    :cond_7
    const/16 v14, 0xc

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 569
    :pswitch_e
    const/16 v14, 0x52

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 571
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 572
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 573
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 578
    :pswitch_f
    const/16 v14, 0x53

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 580
    const-string v14, "Suggested Contacts"

    iget-object v15, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 581
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 582
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 583
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 589
    :pswitch_10
    const/16 v14, 0x51

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 593
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 594
    .restart local v2    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    iget-object v14, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v14

    const-wide/high16 v16, 0x402c000000000000L    # 14.0

    cmpg-double v14, v14, v16

    if-gez v14, :cond_8

    .line 598
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 600
    :cond_8
    const/16 v14, 0x45

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 601
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 603
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 604
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v14}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v14

    invoke-virtual {v14}, Lcom/android/emailcommon/utility/SyncScheduleData;->getOffPeakSchedule()I

    move-result v14

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    goto/16 :goto_1

    .line 611
    .end local v2    # "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    :pswitch_12
    const/16 v14, 0x43

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 612
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    goto/16 :goto_1

    .line 616
    :pswitch_13
    const/16 v14, 0x54

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 619
    :pswitch_14
    const/16 v14, 0x60

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 622
    :pswitch_15
    const/16 v14, 0x61

    iput v14, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    goto/16 :goto_1

    .line 632
    :cond_9
    const/4 v14, 0x0

    goto/16 :goto_2

    .line 646
    .end local v5    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x1c7
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 470
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_b
        :pswitch_9
        :pswitch_8
        :pswitch_12
        :pswitch_c
        :pswitch_7
        :pswitch_11
        :pswitch_4
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_4
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch
.end method

.method public callback(I)V
    .locals 4
    .param p1, "status"    # I

    .prologue
    .line 164
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    return-void

    .line 166
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public changesParser(Ljava/util/ArrayList;Z)V
    .locals 26
    .param p2, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 858
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 864
    .local v4, "addMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    const/4 v7, 0x0

    .line 865
    .local v7, "checkCount":Z
    :goto_0
    const/16 v19, 0x1ce

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_6

    .line 866
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    move/from16 v19, v0

    const/16 v20, 0x1cf

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 867
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->addParser()Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v11

    .line 868
    .local v11, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v11, :cond_0

    .line 869
    invoke-virtual {v4, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 871
    :cond_0
    const/4 v7, 0x1

    .line 872
    goto :goto_0

    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    move/from16 v19, v0

    const/16 v20, 0x1d0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    .line 873
    invoke-virtual/range {p0 .. p1}, Lcom/android/exchange/adapter/FolderSyncParser;->deleteParser(Ljava/util/ArrayList;)V

    .line 874
    const/4 v7, 0x1

    goto :goto_0

    .line 875
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    move/from16 v19, v0

    const/16 v20, 0x1d1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 876
    invoke-virtual/range {p0 .. p1}, Lcom/android/exchange/adapter/FolderSyncParser;->updateParser(Ljava/util/ArrayList;)V

    .line 877
    const/4 v7, 0x1

    goto :goto_0

    .line 878
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    move/from16 v19, v0

    const/16 v20, 0x1d7

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 879
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValueInt()I

    move-result v19

    if-eqz v19, :cond_4

    const/4 v7, 0x1

    :goto_1
    goto :goto_0

    :cond_4
    const/4 v7, 0x0

    goto :goto_1

    .line 881
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->skipTag()V

    goto :goto_0

    .line 891
    :cond_6
    monitor-enter p0

    .line 893
    :try_start_0
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 895
    .local v18, "validMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 897
    .local v17, "userMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 898
    .local v14, "mailboxMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 899
    .restart local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v14, v0, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1075
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v14    # "mailboxMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .end local v17    # "userMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .end local v18    # "validMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    :catchall_0
    move-exception v19

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v19

    .line 902
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v14    # "mailboxMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .restart local v17    # "userMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    .restart local v18    # "validMailboxes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    :cond_7
    :try_start_1
    const-string v19, "KTT"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v20

    const-string v21, "CscFeature_Email_AddIspAccount"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 904
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 905
    .restart local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_8

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 906
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 907
    .local v15, "parent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :goto_4
    if-eqz v15, :cond_9

    .line 908
    iget v0, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v19, v0

    if-nez v19, :cond_b

    .line 914
    :cond_9
    if-nez v15, :cond_a

    .line 915
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v15

    .line 917
    :cond_a
    if-eqz v15, :cond_8

    iget v0, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v19, v0

    if-nez v19, :cond_8

    .line 918
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 919
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    .line 920
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    goto :goto_3

    .line 910
    :cond_b
    iget-object v0, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    iget-object v0, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_9

    .line 912
    iget-object v0, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "parent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    check-cast v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .restart local v15    # "parent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    goto :goto_4

    .line 927
    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v15    # "parent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_c
    const/4 v12, 0x0

    .line 928
    .local v12, "mailboxCommitCount":I
    const/4 v5, 0x1

    .line 929
    .local v5, "bNoInbox":Z
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_d
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 931
    .restart local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v19, v0

    const/16 v20, 0x60

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 932
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    :goto_6
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    move/from16 v19, v0

    if-nez v19, :cond_e

    .line 938
    const/4 v5, 0x0

    .line 942
    :cond_e
    if-eqz p2, :cond_d

    add-int/lit8 v12, v12, 0x1

    const/16 v19, 0x14

    move/from16 v0, v19

    if-ne v12, v0, :cond_d

    .line 943
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->commitMailboxes(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)Z

    move-result v19

    if-nez v19, :cond_10

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/EasSyncService;->stop()V

    .line 945
    monitor-exit p0

    .line 1076
    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :goto_7
    return-void

    .line 934
    .restart local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_f
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 948
    :cond_10
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 949
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 950
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 951
    const/4 v12, 0x0

    goto :goto_5

    .line 954
    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_11
    if-eqz p2, :cond_12

    if-nez v5, :cond_12

    .line 956
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    move/from16 v20, v0

    const/high16 v21, 0x20000

    or-int v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    .line 960
    :cond_12
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 961
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v19, "syncKey"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    const-string v19, "flags"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 963
    sget-object v19, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v20, v0

    invoke-static/range {v19 .. v21}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 967
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v14, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->commitMailboxes(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;Ljava/util/ArrayList;)Z

    move-result v19

    if-nez v19, :cond_13

    .line 968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/EasSyncService;->stop()V

    .line 969
    monitor-exit p0

    goto/16 :goto_7

    .line 973
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_17

    .line 976
    :cond_14
    new-instance v6, Lcom/android/exchange/adapter/ContactsSyncAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v6, v0, v1}, Lcom/android/exchange/adapter/ContactsSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 977
    .local v6, "cSyncAdpater":Lcom/android/exchange/adapter/ContactsSyncAdapter;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 978
    .local v9, "cv1":Landroid/content/ContentValues;
    const-string v19, "syncKey"

    const-string v20, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    const-string v19, "syncInterval"

    const/16 v20, -0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_15

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 983
    .local v16, "serverId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v19, v0

    sget-object v20, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v21, "accountKey=? and serverId=?"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    aput-object v16, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 988
    .local v13, "mailboxCount":I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Updating Contact SubFolder mailbox with sync key 0 and updaing interval as manual. Count = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 990
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "Deleting SubFolder contacts from Contact DB for serverId "

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 991
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/android/exchange/adapter/ContactsSyncAdapter;->deleteContactsOfSubFolder(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 993
    .end local v13    # "mailboxCount":I
    .end local v16    # "serverId":Ljava/lang/String;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 994
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_16

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 995
    .restart local v16    # "serverId":Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "Deleting SubFolder contacts from Contact DB for serverId "

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 996
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/android/exchange/adapter/ContactsSyncAdapter;->deleteContactsOfSubFolder(Ljava/lang/String;)V

    goto :goto_9

    .line 998
    .end local v16    # "serverId":Ljava/lang/String;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 1005
    .end local v6    # "cSyncAdpater":Lcom/android/exchange/adapter/ContactsSyncAdapter;
    .end local v9    # "cv1":Landroid/content/ContentValues;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_1b

    .line 1008
    :cond_18
    new-instance v6, Lcom/android/exchange/adapter/CalendarSyncAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v6, v0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 1009
    .local v6, "cSyncAdpater":Lcom/android/exchange/adapter/CalendarSyncAdapter;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1010
    .restart local v9    # "cv1":Landroid/content/ContentValues;
    const-string v19, "syncKey"

    const-string v20, "0"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    const-string v19, "syncInterval"

    const/16 v20, -0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1012
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_19

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1015
    .restart local v16    # "serverId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v19, v0

    sget-object v20, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v21, "accountKey=? and serverId=?"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    aput-object v16, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 1020
    .restart local v13    # "mailboxCount":I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Updating Calendar SubFolder mailbox with sync key 0 and updaing interval as manual. Count = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 1022
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "Deleting SubFolder calendars from Calendar DB for serverId "

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 1023
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteCalendarEventsOfSubFolder(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1025
    .end local v13    # "mailboxCount":I
    .end local v16    # "serverId":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderMovedList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_1a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1027
    .restart local v16    # "serverId":Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "Deleting SubFolder calendar events from Calendar DB for serverId "

    aput-object v21, v19, v20

    const/16 v20, 0x1

    aput-object v16, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 1028
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteCalendarEventsOfSubFolder(Ljava/lang/String;)V

    goto :goto_b

    .line 1030
    .end local v16    # "serverId":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 1065
    .end local v6    # "cSyncAdpater":Lcom/android/exchange/adapter/CalendarSyncAdapter;
    .end local v9    # "cv1":Landroid/content/ContentValues;
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mFixupUninitializedNeeded:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1c

    .line 1066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "accountKey="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/android/exchange/provider/MailboxUtilities;->removeInvalidFolder(Landroid/content/Context;Ljava/lang/String;)V

    .line 1075
    :cond_1c
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_7
.end method

.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1084
    return-void
.end method

.method public commit()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 1093
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const-string v1, "Sync Issues"

    aput-object v1, v0, v10

    .line 1094
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountIdAsString:Ljava/lang/String;

    aput-object v1, v0, v2

    .line 1095
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/FolderSyncParser;->MAILBOX_ID_COLUMNS_PROJECTION:[Ljava/lang/String;

    const-string v3, "displayName=? and accountKey=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1097
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1098
    .local v7, "parentServerId":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 1099
    .local v8, "id":J
    if-eqz v6, :cond_1

    .line 1101
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1102
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1103
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 1106
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1109
    :cond_1
    if-eqz v7, :cond_2

    .line 1110
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1112
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    aput-object v7, v0, v10

    .line 1113
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "parentServerId=? and accountKey=?"

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1119
    :cond_2
    iget-boolean v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mInitialSync:Z

    if-eqz v0, :cond_3

    .line 1120
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->restoreMailboxSyncOptions()V

    .line 1124
    :cond_3
    invoke-virtual {p0, v10}, Lcom/android/exchange/adapter/FolderSyncParser;->callback(I)V

    .line 1126
    return-void

    .line 1106
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public deleteParser(Ljava/util/ArrayList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :goto_0
    const/16 v3, 0x1d0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_5

    .line 276
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    packed-switch v3, :pswitch_data_0

    .line 326
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->skipTag()V

    goto :goto_0

    .line 278
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v17

    .line 280
    .local v17, "serverId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/FolderSyncParser;->getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 281
    .local v2, "c":Landroid/database/Cursor;
    if-eqz v2, :cond_4

    .line 283
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 284
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Deleting "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v17, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 285
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 286
    .local v6, "mailboxLocalId":J
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v3, v4, v5, v6, v7}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllMailboxAttachmentFiles(Landroid/content/Context;JJ)V

    .line 291
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMailboxBodyFiles(Landroid/content/Context;JJZ)V

    .line 293
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v3, v4, v5, v0, v1}, Lcom/android/exchange/provider/MailboxUtilities;->deleteAllChildren(Landroid/content/Context;JLjava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v15

    .line 295
    .local v15, "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 296
    .local v12, "did":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v3, v4, v5, v12, v13}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllMailboxAttachmentFiles(Landroid/content/Context;JJ)V

    .line 298
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v14, 0x1

    invoke-static/range {v9 .. v14}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMailboxBodyFiles(Landroid/content/Context;JJZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 320
    .end local v6    # "mailboxLocalId":J
    .end local v12    # "did":J
    .end local v15    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v16    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v3

    .line 301
    .restart local v6    # "mailboxLocalId":J
    .restart local v15    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v16    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mInitialSync:Z

    if-nez v3, :cond_1

    .line 302
    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 309
    :cond_1
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v8, 0x53

    cmp-long v3, v4, v8

    if-nez v3, :cond_2

    .line 310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    :cond_2
    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v8, 0x52

    cmp-long v3, v4, v8

    if-nez v3, :cond_3

    .line 315
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderDeleteList:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 320
    .end local v6    # "mailboxLocalId":J
    .end local v15    # "deleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v16    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/exchange/adapter/FolderSyncParser;->mFixupUninitializedNeeded:Z

    goto/16 :goto_0

    .line 329
    .end local v2    # "c":Landroid/database/Cursor;
    .end local v17    # "serverId":Ljava/lang/String;
    :cond_5
    return-void

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x1c8
        :pswitch_0
    .end packed-switch
.end method

.method isValidMailFolder(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/util/HashMap;)Z
    .locals 12
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/provider/EmailContent$Mailbox;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/emailcommon/provider/EmailContent$Mailbox;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "mailboxMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Mailbox;>;"
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 665
    iget v8, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 667
    .local v8, "folderType":I
    const/16 v1, 0x40

    if-ge v8, v1, :cond_1

    move v6, v0

    .line 693
    :cond_0
    :goto_0
    return v6

    .line 670
    :cond_1
    const/16 v1, 0x60

    if-ne v8, v1, :cond_0

    .line 673
    iget-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 677
    .local v9, "parent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v9, :cond_2

    .line 678
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 679
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    aput-object v2, v1, v0

    .line 680
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "accountKey=? AND serverId=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/android/emailcommon/utility/Utility;->getFirstRowInt(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v10, v0

    .line 684
    .local v10, "parentId":J
    const-wide/16 v0, -0x1

    cmp-long v0, v10, v0

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v9

    .line 687
    if-eqz v9, :cond_0

    .line 693
    .end local v10    # "parentId":J
    :cond_2
    invoke-virtual {p0, v9, p2}, Lcom/android/exchange/adapter/FolderSyncParser;->isValidMailFolder(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/util/HashMap;)Z

    move-result v6

    goto :goto_0
.end method

.method public parse()Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 174
    const/4 v2, 0x0

    .line 175
    .local v2, "res":Z
    const/4 v3, 0x0

    .line 179
    .local v3, "resetFolders":Z
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v1, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 180
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v5, "0"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    const/4 v5, 0x1

    :goto_0
    iput-boolean v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mInitialSync:Z

    .line 181
    iget-boolean v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mInitialSync:Z

    if-eqz v5, :cond_1

    .line 182
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "accountKey=? and type!=68"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 186
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v5

    const/16 v6, 0x1d6

    if-eq v5, v6, :cond_4

    .line 187
    new-instance v5, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v5, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v5

    .line 180
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 208
    .local v4, "status":I
    :cond_3
    const/4 v5, 0x6

    if-ne v4, v5, :cond_7

    .line 209
    const/16 v5, 0x22

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->callback(I)V

    .line 188
    .end local v4    # "status":I
    :cond_4
    :goto_1
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_d

    .line 189
    iget v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    const/16 v6, 0x1cc

    if-ne v5, v6, :cond_a

    .line 190
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValueInt()I

    move-result v4

    .line 192
    .restart local v4    # "status":I
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/FolderSyncParser;->isProvisioningStatus(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 193
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 194
    const/4 v2, 0x0

    .line 195
    new-instance v5, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v5, v4}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v5

    .line 199
    :cond_5
    const/4 v5, 0x1

    if-eq v4, v5, :cond_4

    .line 200
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "FolderSync failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 203
    invoke-static {v4}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isDeniedAccess(I)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_3

    .line 206
    :cond_6
    new-instance v5, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v5, v4}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v5

    .line 212
    :cond_7
    const/16 v5, 0x9

    if-ne v4, v5, :cond_8

    .line 213
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v6, "Bad sync key; RESET and delete all folders"

    invoke-virtual {v5, v6}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 215
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    const-string v6, "0"

    iput-object v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 216
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 217
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v5, "syncKey"

    iget-object v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v0, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 222
    iget-wide v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->deleteAccountPIMData(J)V

    .line 224
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v7, "accountKey=? and type!=68"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 229
    iget-wide v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->stopNonAccountMailboxSyncsForAccount(J)V

    .line 230
    const/4 v2, 0x1

    .line 231
    const/4 v3, 0x1

    .line 232
    goto/16 :goto_1

    .line 234
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_8
    const/16 v5, 0x81

    if-ne v4, v5, :cond_9

    .line 235
    new-instance v5, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v6, 0x40001

    const v7, 0x7f060015

    invoke-direct {v5, v6, v7}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v5

    .line 244
    :cond_9
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v6, "Throwing IOException; will retry later"

    invoke-virtual {v5, v6}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 245
    new-instance v5, Lcom/android/exchange/adapter/Parser$EasParserException;

    const-string v6, "Folder status error"

    invoke-direct {v5, p0, v6}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;Ljava/lang/String;)V

    throw v5

    .line 250
    .end local v4    # "status":I
    :cond_a
    iget v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    const/16 v6, 0x1d2

    if-ne v5, v6, :cond_b

    .line 251
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 252
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "New Account SyncKey: "

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 253
    :cond_b
    iget v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    const/16 v6, 0x1ce

    if-ne v5, v6, :cond_c

    .line 254
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mOperations:Ljava/util/ArrayList;

    iget-boolean v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mInitialSync:Z

    invoke-virtual {p0, v5, v6}, Lcom/android/exchange/adapter/FolderSyncParser;->changesParser(Ljava/util/ArrayList;Z)V

    goto/16 :goto_1

    .line 256
    :cond_c
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->skipTag()V

    goto/16 :goto_1

    .line 258
    :cond_d
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v5}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 259
    :try_start_0
    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v5}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v5

    if-eqz v5, :cond_e

    if-eqz v3, :cond_f

    .line 260
    :cond_e
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->commit()V

    .line 261
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "Leaving FolderSyncParser with Account syncKey="

    aput-object v8, v5, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    aput-object v8, v5, v7

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 263
    :cond_f
    monitor-exit v6

    .line 264
    return v2

    .line 263
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1130
    return-void
.end method

.method restoreMailboxSyncOptions()V
    .locals 8

    .prologue
    .line 405
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 406
    .local v0, "cv":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountIdAsString:Ljava/lang/String;

    aput-object v6, v4, v5

    .line 407
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mSyncOptionsMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 408
    .local v3, "serverId":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mSyncOptionsMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;

    .line 409
    .local v2, "options":Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;
    const-string v4, "syncInterval"

    # getter for: Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mInterval:I
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->access$100(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    const-string v4, "syncLookback"

    # getter for: Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mLookback:I
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->access$200(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string v4, "offpeakSyncSchedule"

    # getter for: Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mOffpeakSchedule:I
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->access$300(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 412
    const-string v4, "peakSyncSchedule"

    # getter for: Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->mPeakSchedule:I
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;->access$400(Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 413
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 415
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "serverId=? and accountKey=?"

    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mBindArguments:[Ljava/lang/String;

    invoke-virtual {v4, v5, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 419
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "options":Lcom/android/exchange/adapter/FolderSyncParser$SyncOptions;
    .end local v3    # "serverId":Ljava/lang/String;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mSyncOptionsMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    throw v4

    .restart local v0    # "cv":Landroid/content/ContentValues;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    iget-object v4, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mSyncOptionsMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 421
    return-void
.end method

.method public updateParser(Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v8, 0x3

    .line 697
    const/4 v5, 0x0

    .line 698
    .local v5, "serverId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 699
    .local v2, "displayName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 701
    .local v4, "parentId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 702
    .local v3, "isTrashFolderRestored":Z
    const/4 v6, 0x0

    .line 704
    .local v6, "trashServerId":Ljava/lang/String;
    :goto_0
    const/16 v7, 0x1d1

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/FolderSyncParser;->nextTag(I)I

    move-result v7

    if-eq v7, v8, :cond_0

    .line 705
    iget v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->tag:I

    packed-switch v7, :pswitch_data_0

    .line 716
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->skipTag()V

    goto :goto_0

    .line 707
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 708
    goto :goto_0

    .line 710
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 711
    goto :goto_0

    .line 713
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 714
    goto :goto_0

    .line 722
    :cond_0
    if-eqz v5, :cond_9

    if-nez v2, :cond_1

    if-eqz v4, :cond_9

    .line 723
    :cond_1
    invoke-direct {p0, v5}, Lcom/android/exchange/adapter/FolderSyncParser;->getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 724
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_9

    .line 727
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 728
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Updating "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v5, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 738
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 739
    .local v1, "cv":Landroid/content/ContentValues;
    if-eqz v2, :cond_2

    .line 740
    const-string v7, "displayName"

    invoke-virtual {v1, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    :cond_2
    if-eqz v4, :cond_4

    .line 745
    const-string v7, "0"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 746
    const/4 v4, 0x0

    .line 747
    :cond_3
    const-string v7, "parentServerId"

    invoke-virtual {v1, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :cond_4
    const-string v7, "parentKey"

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 753
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mFixupUninitializedNeeded:Z

    .line 762
    const/4 v7, 0x3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide/16 v10, 0x53

    cmp-long v7, v8, v10

    if-nez v7, :cond_6

    .line 764
    if-nez v3, :cond_5

    .line 765
    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    const/4 v10, 0x6

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    .line 766
    const/4 v3, 0x1

    .line 768
    :cond_5
    if-eqz v6, :cond_6

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 771
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Contact SubFolder, Adding mailboxId "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " movedlist"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 772
    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContactSubFolderMovedList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 779
    :cond_6
    const/4 v7, 0x3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const-wide/16 v10, 0x52

    cmp-long v7, v8, v10

    if-nez v7, :cond_8

    .line 781
    if-nez v3, :cond_7

    .line 782
    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mContext:Landroid/content/Context;

    iget-wide v8, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mAccountId:J

    const/4 v10, 0x6

    invoke-static {v7, v8, v9, v10}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v6

    .line 783
    const/4 v3, 0x1

    .line 785
    :cond_7
    if-eqz v6, :cond_8

    invoke-virtual {v6, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 788
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Calendar SubFolder, Adding mailboxId "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " movedlist"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/FolderSyncParser;->userLog([Ljava/lang/String;)V

    .line 789
    iget-object v7, p0, Lcom/android/exchange/adapter/FolderSyncParser;->mCalendarSubFolderMovedList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 795
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_8
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 799
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_9
    return-void

    .line 795
    .restart local v0    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v7

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v7

    .line 705
    nop

    :pswitch_data_0
    .packed-switch 0x1c7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
