.class Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
.super Ljava/lang/Object;
.source "NotesSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/NotesSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoteContents"
.end annotation


# instance fields
.field private final mAccountKey:J

.field public final mAccountType:Ljava/lang/String;

.field private mBodyData:Ljava/lang/String;

.field private mBodySize:J

.field private mBodyTruncated:Z

.field private mBodyType:I

.field private mCategories:Ljava/lang/String;

.field private mClientID:Ljava/lang/String;

.field private mDeletedMsgRecieved:Z

.field public final mDisplayName:Ljava/lang/String;

.field public final mEmailAddress:Ljava/lang/String;

.field private mFlagNoteLoaded:Z

.field private mFlagNoteMoved:Z

.field private mFlagNoteRead:Z

.field private mHtmlText:Ljava/lang/String;

.field private mLastModifyDate:Ljava/lang/String;

.field private mMessageForeignKey:J

.field private mModifiedMsgRecieved:Z

.field private mNewMsgRecieved:Z

.field private mNewNoteCount:J

.field private mPlanText:Ljava/lang/String;

.field private mServerIDNote:Ljava/lang/String;

.field private mServerTimeStamp:Ljava/lang/String;

.field private mSubject:Ljava/lang/String;

.field private mSyncNeededFlag:I

.field final synthetic this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "account_key"    # J
    .param p4, "accountType"    # Ljava/lang/String;
    .param p5, "displayName"    # Ljava/lang/String;
    .param p6, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 674
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 676
    iput-wide p2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mAccountKey:J

    .line 677
    iput-object p4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mAccountType:Ljava/lang/String;

    .line 678
    iput-object p5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mDisplayName:Ljava/lang/String;

    .line 679
    iput-object p6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mEmailAddress:Ljava/lang/String;

    .line 681
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->init()V

    .line 682
    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 685
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mServerIDNote:Ljava/lang/String;

    .line 686
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSubject:Ljava/lang/String;

    .line 687
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mLastModifyDate:Ljava/lang/String;

    .line 688
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mHtmlText:Ljava/lang/String;

    .line 689
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mPlanText:Ljava/lang/String;

    .line 690
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    .line 691
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mFlagNoteRead:Z

    .line 692
    iput-wide v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mNewNoteCount:J

    .line 693
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mServerTimeStamp:Ljava/lang/String;

    .line 694
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mFlagNoteLoaded:Z

    .line 695
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mClientID:Ljava/lang/String;

    .line 696
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mFlagNoteMoved:Z

    .line 697
    iput v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSyncNeededFlag:I

    .line 698
    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyData:Ljava/lang/String;

    .line 699
    iput-wide v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodySize:J

    .line 700
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyType:I

    .line 701
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyTruncated:Z

    .line 702
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mMessageForeignKey:J

    .line 703
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mNewMsgRecieved:Z

    .line 704
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mModifiedMsgRecieved:Z

    .line 705
    iput-boolean v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mDeletedMsgRecieved:Z

    .line 707
    return-void
.end method


# virtual methods
.method public checkDeletedMsgRecieved()Z
    .locals 1

    .prologue
    .line 726
    iget-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mDeletedMsgRecieved:Z

    return v0
.end method

.method public checkModifiedMsgRecieved()Z
    .locals 1

    .prologue
    .line 718
    iget-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mModifiedMsgRecieved:Z

    return v0
.end method

.method public checkNewMsgRecieved()Z
    .locals 1

    .prologue
    .line 710
    iget-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mNewMsgRecieved:Z

    return v0
.end method

.method public getBodyData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 762
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyData:Ljava/lang/String;

    return-object v0
.end method

.method public getBodySize()J
    .locals 2

    .prologue
    .line 746
    iget-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodySize:J

    return-wide v0
.end method

.method public getBodyTruncated()Z
    .locals 1

    .prologue
    .line 770
    iget-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyTruncated:Z

    return v0
.end method

.method public getBodyType()I
    .locals 1

    .prologue
    .line 766
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyType:I

    return v0
.end method

.method public getMessageForeignKey()J
    .locals 2

    .prologue
    .line 734
    iget-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mMessageForeignKey:J

    return-wide v0
.end method

.method public getSyncNeededFlag()I
    .locals 1

    .prologue
    .line 838
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSyncNeededFlag:I

    return v0
.end method

.method public getmAccountKey()J
    .locals 2

    .prologue
    .line 782
    iget-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mAccountKey:J

    return-wide v0
.end method

.method public getmCategories()Ljava/lang/String;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    return-object v0
.end method

.method public getmClientID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mClientID:Ljava/lang/String;

    return-object v0
.end method

.method public getmDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getmLastModifyDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mLastModifyDate:Ljava/lang/String;

    return-object v0
.end method

.method public getmServerIDNote()Ljava/lang/String;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mServerIDNote:Ljava/lang/String;

    return-object v0
.end method

.method public getmSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSubject:Ljava/lang/String;

    return-object v0
.end method

.method public setBodyData(Ljava/lang/String;)V
    .locals 0
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 750
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyData:Ljava/lang/String;

    .line 751
    return-void
.end method

.method public setBodySize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 742
    iput-wide p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodySize:J

    .line 743
    return-void
.end method

.method public setBodyTruncated(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 758
    iput-boolean p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyTruncated:Z

    .line 759
    return-void
.end method

.method public setBodyType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 754
    iput p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mBodyType:I

    .line 755
    return-void
.end method

.method public setDeletedMsgRecieved(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 730
    iput-boolean p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mDeletedMsgRecieved:Z

    .line 731
    return-void
.end method

.method public setModifiedMsgRecieved(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 722
    iput-boolean p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mModifiedMsgRecieved:Z

    .line 723
    return-void
.end method

.method public setNewMsgRecieved(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 714
    iput-boolean p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mNewMsgRecieved:Z

    .line 715
    return-void
.end method

.method public setSyncNeededFlag(I)V
    .locals 0
    .param p1, "flag"    # I

    .prologue
    .line 834
    iput p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSyncNeededFlag:I

    .line 835
    return-void
.end method

.method public setmCategories(Ljava/lang/String;)V
    .locals 2
    .param p1, "categories"    # Ljava/lang/String;

    .prologue
    .line 842
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 843
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    .line 848
    :goto_0
    return-void

    .line 845
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mCategories:Ljava/lang/String;

    goto :goto_0
.end method

.method public setmClientID(Ljava/lang/String;)V
    .locals 0
    .param p1, "mClientID"    # Ljava/lang/String;

    .prologue
    .line 887
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mClientID:Ljava/lang/String;

    .line 888
    return-void
.end method

.method public setmLastModifyDate(Ljava/lang/String;)V
    .locals 0
    .param p1, "mLastModifyDate"    # Ljava/lang/String;

    .prologue
    .line 810
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mLastModifyDate:Ljava/lang/String;

    .line 811
    return-void
.end method

.method public setmServerIDNote(Ljava/lang/String;)V
    .locals 0
    .param p1, "mServerIDNote"    # Ljava/lang/String;

    .prologue
    .line 794
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mServerIDNote:Ljava/lang/String;

    .line 795
    return-void
.end method

.method public setmSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSubject"    # Ljava/lang/String;

    .prologue
    .line 802
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mSubject:Ljava/lang/String;

    .line 803
    return-void
.end method

.method public updateMessageForeignKey(J)V
    .locals 1
    .param p1, "key"    # J

    .prologue
    .line 738
    iput-wide p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->mMessageForeignKey:J

    .line 739
    return-void
.end method
