.class public Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "FolderCreateAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/FolderCreateAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FolderCreateParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/FolderCreateAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V
    .locals 2
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractCommandAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iput-object p1, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    .line 159
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 160
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.FolderCreateParser"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method private getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .locals 5
    .param p1, "parentServerId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 181
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v1, v2, v3, p1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithServerId(Landroid/content/Context;JLjava/lang/String;)[Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 183
    .local v0, "result":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v0, :cond_0

    .line 184
    const/4 v1, 0x0

    .line 189
    :goto_0
    return-object v1

    .line 186
    :cond_0
    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    if-eqz v1, :cond_1

    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 187
    aget-object v1, v0, v4

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    aput-object v1, v0, v4

    .line 189
    :cond_1
    aget-object v1, v0, v4

    goto :goto_0
.end method


# virtual methods
.method public commandsParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 165
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.commandsParser"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public commit()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.commit"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->mParentFolderId:J
    invoke-static {v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$000(Lcom/android/exchange/adapter/FolderCreateAdapter;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v8

    .line 197
    .local v8, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v8, :cond_0

    .line 198
    const-string v0, "FolderCreateAdapter"

    const-string v1, "fail to restore mailbox"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :goto_0
    return-void

    .line 201
    :cond_0
    new-instance v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 202
    .local v7, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->mNewFolderName:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$100(Lcom/android/exchange/adapter/FolderCreateAdapter;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mServerId:Ljava/lang/String;

    iput-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 204
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iput-wide v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 205
    const/16 v0, 0xc

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 207
    const/4 v0, -0x1

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 209
    const/4 v0, -0x1

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 211
    const/4 v0, -0x1

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    .line 213
    const-string v0, "KTT"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Email_AddIspAccount"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    iget v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v0, :cond_6

    .line 217
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 218
    const/4 v0, 0x0

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    .line 219
    const/4 v0, 0x0

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    .line 234
    :cond_1
    :goto_1
    iget v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v1, 0x44

    if-eq v0, v1, :cond_7

    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    :goto_2
    iput-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    .line 240
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 241
    const-wide/16 v0, -0x1

    iput-wide v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentKey:J

    .line 242
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    const-string v1, "Unwanted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    const/4 v0, 0x7

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 244
    :cond_2
    const-string v0, "FolderCreateAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create mailbox:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " below Root"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :goto_3
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Create mailbox: "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->userLog([Ljava/lang/String;)V

    .line 253
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v9, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/AbstractCommandAdapter;->MAILBOX_ID_COLUMNS_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "serverId=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "accountKey"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 260
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 262
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 263
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->toContentValues()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 271
    :cond_4
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 279
    :try_start_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 280
    const-string v0, "Applying "

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    const-string v3, " mailbox operations."

    invoke-virtual {p0, v0, v2, v3}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v2, v2, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget-object v2, v2, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mSyncKey:Ljava/lang/String;

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 282
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Account;->toContentValues()Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 287
    :try_start_2
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "com.android.email.provider"

    invoke-virtual {v0, v2, v9}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 289
    const-string v0, "New Account SyncKey: "

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 298
    :cond_5
    :goto_4
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 299
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.commit before callback"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->mNewFolderName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$102(Lcom/android/exchange/adapter/FolderCreateAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 304
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const-wide/16 v2, -0x1

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->mParentFolderId:J
    invoke-static {v0, v2, v3}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$002(Lcom/android/exchange/adapter/FolderCreateAdapter;J)J

    .line 306
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    .line 307
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.commit after callback"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 220
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v9    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_6
    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->getTopParentMailbox(Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v10

    .line 222
    .local v10, "topParent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v10, :cond_1

    iget v0, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 224
    const/4 v0, 0x0

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mOffpeakSyncSchedule:I

    .line 225
    const/4 v0, 0x0

    iput v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mPeakSyncSchedule:I

    goto/16 :goto_1

    .line 234
    .end local v10    # "topParent":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 246
    :cond_8
    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentKey:J

    .line 247
    const-string v0, "FolderCreateAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Create mailbox:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " below mMailbox.mId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 267
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 298
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 293
    :catch_0
    move-exception v0

    goto/16 :goto_4

    .line 290
    :catch_1
    move-exception v0

    goto/16 :goto_4
.end method

.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 312
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.parse"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    new-instance v1, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    invoke-direct {v1, v2}, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;-><init>(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    iput-object v1, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    .line 315
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->nextTag(I)I

    move-result v0

    const/16 v1, 0x1d3

    if-eq v0, v1, :cond_0

    .line 316
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 317
    :cond_0
    :goto_0
    :sswitch_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->nextTag(I)I

    move-result v0

    if-eq v0, v4, :cond_3

    .line 319
    iget v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->tag:I

    const/16 v1, 0x1d2

    if-ne v0, v1, :cond_1

    .line 320
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mSyncKey:Ljava/lang/String;

    goto :goto_0

    .line 321
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->tag:I

    const/16 v1, 0x1c8

    if-ne v0, v1, :cond_2

    .line 322
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mServerId:Ljava/lang/String;

    goto :goto_0

    .line 323
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->tag:I

    const/16 v1, 0x1cc

    if-ne v0, v1, :cond_0

    .line 324
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->getValueInt()I

    move-result v1

    iput v1, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    .line 325
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    sparse-switch v0, :sswitch_data_0

    .line 360
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unknown error."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :sswitch_1
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/16 v1, 0x25

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    goto :goto_0

    .line 334
    :sswitch_2
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/16 v1, 0x23

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    goto :goto_0

    .line 338
    :sswitch_3
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/16 v1, 0x26

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    goto :goto_0

    .line 348
    :sswitch_4
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    const/16 v1, 0x22

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    goto :goto_0

    .line 353
    :sswitch_5
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    # setter for: Lcom/android/exchange/adapter/FolderCreateAdapter;->isStatus:I
    invoke-static {v0, v4}, Lcom/android/exchange/adapter/FolderCreateAdapter;->access$202(Lcom/android/exchange/adapter/FolderCreateAdapter;I)I

    .line 354
    new-instance v0, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v1, 0x40001

    const v2, 0x7f060015

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v0

    .line 364
    :cond_3
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->this$0:Lcom/android/exchange/adapter/FolderCreateAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderCreateAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 365
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.parse  CODE_FOLDER_CREATE_SUCCESS"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderCreateAdapter$FolderCreateParser;->commit()V

    .line 369
    :cond_4
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.parse exit"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    return v3

    .line 325
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x5 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0x81 -> :sswitch_5
    .end sparse-switch
.end method

.method public responsesParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    const-string v0, "FolderCreateAdapter"

    const-string v1, "FolderCreateAdapter.FolderCreateParser.responsesParser"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method
