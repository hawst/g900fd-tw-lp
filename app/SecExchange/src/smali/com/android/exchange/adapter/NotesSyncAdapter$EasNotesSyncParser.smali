.class public Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "NotesSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/NotesSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EasNotesSyncParser"
.end annotation


# instance fields
.field mNewMsgCount:J

.field mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

.field final synthetic this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/NotesSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/NotesSyncAdapter;Z)V
    .locals 2
    .param p2, "parse"    # Lcom/android/exchange/adapter/Parser;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/NotesSyncAdapter;
    .param p4, "resumeParser"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    .line 919
    invoke-direct {p0, p2, p3, p4}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V

    .line 904
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 906
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNewMsgCount:J

    .line 920
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    .line 921
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/adapter/NotesSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/NotesSyncAdapter;)V
    .locals 2
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/NotesSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 908
    iput-object p1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    .line 909
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 904
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 906
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNewMsgCount:J

    .line 911
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    .line 914
    return-void
.end method

.method private addData()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    .line 1047
    :cond_0
    :goto_0
    const/16 v1, 0x1d

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v1

    if-eq v1, v5, :cond_2

    .line 1048
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotesSync Tag:+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->userLog([Ljava/lang/String;)V

    .line 1049
    iget v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    sparse-switch v1, :sswitch_data_0

    .line 1077
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->skipTag()V

    goto :goto_0

    .line 1052
    :cond_1
    :goto_1
    :sswitch_0
    const/16 v1, 0x5c8

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 1053
    iget v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    const/16 v2, 0x5c9

    if-ne v1, v2, :cond_1

    .line 1054
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmCategories(Ljava/lang/String;)V

    goto :goto_1

    .line 1062
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->parseNotesDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 1063
    .local v0, "timeInMs":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v1, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmLastModifyDate(Ljava/lang/String;)V

    goto :goto_0

    .line 1068
    .end local v0    # "timeInMs":Ljava/lang/String;
    :sswitch_2
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmSubject(Ljava/lang/String;)V

    goto :goto_0

    .line 1072
    :sswitch_3
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->bodyParser()V

    goto :goto_0

    .line 1083
    :cond_2
    return-void

    .line 1049
    nop

    :sswitch_data_0
    .sparse-switch
        0x44a -> :sswitch_3
        0x5c5 -> :sswitch_2
        0x5c7 -> :sswitch_1
        0x5c8 -> :sswitch_0
    .end sparse-switch
.end method

.method private addParser()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1020
    :goto_0
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1021
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NotesSync Tag:+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->userLog([Ljava/lang/String;)V

    .line 1022
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1024
    :sswitch_0
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmServerIDNote(Ljava/lang/String;)V

    goto :goto_0

    .line 1028
    :sswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->addData()V

    goto :goto_0

    .line 1034
    :cond_0
    return-void

    .line 1022
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method private bodyParser()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1086
    const/4 v0, 0x0

    .line 1087
    .local v0, "value":Ljava/lang/String;
    :goto_0
    const/16 v1, 0x44a

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 1088
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NotesSync Tag:+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->userLog([Ljava/lang/String;)V

    .line 1089
    iget v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 1122
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->skipTag()V

    goto :goto_0

    .line 1096
    :pswitch_1
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodyType(I)V

    goto :goto_0

    .line 1103
    :pswitch_2
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodySize(J)V

    goto :goto_0

    .line 1110
    :pswitch_3
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodyTruncated(Z)V

    goto :goto_0

    .line 1118
    :pswitch_4
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodyData(Ljava/lang/String;)V

    goto :goto_0

    .line 1126
    :cond_0
    return-void

    .line 1089
    :pswitch_data_0
    .packed-switch 0x446
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private changeParser()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1004
    :goto_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1005
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NotesSync Tag:+"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->userLog([Ljava/lang/String;)V

    .line 1006
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1008
    :sswitch_0
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmServerIDNote(Ljava/lang/String;)V

    goto :goto_0

    .line 1012
    :sswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->addData()V

    goto :goto_0

    .line 1017
    :cond_0
    return-void

    .line 1006
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method private deleteParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 995
    :cond_0
    :goto_0
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 996
    const/16 v0, 0xd

    iget v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    if-ne v0, v1, :cond_0

    .line 997
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmServerIDNote(Ljava/lang/String;)V

    goto :goto_0

    .line 1000
    :cond_1
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 933
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNewMsgCount:J

    .line 935
    :cond_0
    :goto_0
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    .line 937
    new-array v0, v8, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotesSync Tag:+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->userLog([Ljava/lang/String;)V

    .line 938
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 939
    new-instance v0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const-string v4, "Eas"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 942
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->addParser()V

    .line 943
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->incrementChangeCount()V

    .line 945
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v7}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setSyncNeededFlag(I)V

    .line 946
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v8}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setNewMsgRecieved(Z)V

    .line 947
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 948
    iget-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNewMsgCount:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNewMsgCount:J

    .line 950
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 951
    const-string v0, "Notes"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recieved NEW Note --  Account ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Server ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 955
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 956
    new-instance v0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const-string v4, "Eas"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 959
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->changeParser()V

    .line 960
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->incrementChangeCount()V

    .line 962
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v7}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setSyncNeededFlag(I)V

    .line 963
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v8}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setModifiedMsgRecieved(Z)V

    .line 964
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 966
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 967
    const-string v0, "Notes"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recieved CHANGED Note --  Account ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Server ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 971
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    .line 972
    new-instance v0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const-string v4, "Eas"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 975
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->deleteParser()V

    .line 976
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->incrementChangeCount()V

    .line 978
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v7}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setSyncNeededFlag(I)V

    .line 979
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v8}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setDeletedMsgRecieved(Z)V

    .line 980
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 982
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 983
    const-string v0, "Notes"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recieved DELETED Note --  Account ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Server ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mNote:Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    invoke-virtual {v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 991
    :cond_3
    return-void
.end method

.method public commit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1135
    const/4 v0, 0x0

    .line 1137
    .local v0, "noteIndex":I
    :goto_0
    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1138
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->committMessage(I)V

    .line 1139
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->committBody(I)V

    .line 1140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1142
    :cond_0
    return-void
.end method

.method public committBody(I)V
    .locals 7
    .param p1, "noteIndex"    # I

    .prologue
    const/4 v6, 0x0

    .line 1223
    iget-object v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v3, v3, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 1225
    .local v0, "Note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkNewMsgRecieved()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkModifiedMsgRecieved()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1227
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1228
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "_id"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getMessageForeignKey()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1229
    const-string v3, "noteKey"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getMessageForeignKey()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1230
    const-string v3, "size"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodySize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1231
    const-string v3, "type"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1232
    const-string v3, "truncated"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyTruncated()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1235
    const-string v3, "textContent"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    const-string v3, "accountKey"

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmAccountKey()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1238
    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkNewMsgRecieved()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1239
    iget-object v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 1240
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1254
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-void

    .line 1243
    .restart local v1    # "cv":Landroid/content/ContentValues;
    :cond_2
    sget-object v3, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getMessageForeignKey()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1245
    .restart local v2    # "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1248
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkDeletedMsgRecieved()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1250
    sget-object v3, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getMessageForeignKey()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1252
    .restart local v2    # "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public committMessage(I)V
    .locals 13
    .param p1, "noteIndex"    # I

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->this$0:Lcom/android/exchange/adapter/NotesSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 1148
    .local v6, "Note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkNewMsgRecieved()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkModifiedMsgRecieved()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1150
    :cond_0
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1151
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v0, "accountKey"

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1152
    const-string v0, "server_id"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    const-string v0, "subject"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmSubject()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    const-string v0, "displayName"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    const-string v0, "last_modified_date"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmLastModifyDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    const-string v0, "categories"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmCategories()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const-string v0, "flag_sync_needed"

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getSyncNeededFlag()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1159
    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkNewMsgRecieved()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1160
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v11

    .line 1161
    .local v11, "uri":Landroid/net/Uri;
    invoke-virtual {v11}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1162
    .local v10, "id":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v6, v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->updateMessageForeignKey(J)V

    .line 1219
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-void

    .line 1165
    .restart local v8    # "cv":Landroid/content/ContentValues;
    :cond_2
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1169
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1171
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "server_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1175
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1176
    sget-object v0, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 1179
    .restart local v11    # "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v11, v8, v1, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1181
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v6, v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->updateMessageForeignKey(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1187
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_3
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1188
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1183
    :catch_0
    move-exception v9

    .line 1184
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1187
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1188
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1187
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1188
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1194
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v7    # "c":Landroid/database/Cursor;
    .end local v8    # "cv":Landroid/content/ContentValues;
    :cond_5
    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->checkDeletedMsgRecieved()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1199
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 1202
    .restart local v2    # "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "server_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1206
    .restart local v7    # "c":Landroid/database/Cursor;
    if-eqz v7, :cond_7

    .line 1207
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1209
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {v6, v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->updateMessageForeignKey(J)V

    .line 1211
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1213
    :cond_7
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "server_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public parseAddResponse()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1257
    const-wide/16 v14, -0x1

    .line 1258
    .local v14, "status":J
    const/4 v9, 0x0

    .line 1259
    .local v9, "clientId":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1261
    .local v12, "serverId":Ljava/lang/String;
    :goto_0
    const/4 v2, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 1262
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1267
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 1268
    goto :goto_0

    .line 1264
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValueInt()I

    move-result v2

    int-to-long v14, v2

    .line 1265
    goto :goto_0

    .line 1270
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v12

    goto :goto_0

    .line 1275
    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v2, v14, v2

    if-nez v2, :cond_4

    .line 1277
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 1281
    .local v4, "projection":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 1283
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "clientId=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v9, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1288
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1290
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 1292
    .local v17, "uri_update":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1294
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1295
    .local v10, "cv":Landroid/content/ContentValues;
    const-string v2, "server_id"

    invoke-virtual {v10, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1296
    const-string v2, "flag_sync_needed"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1297
    const-string v2, "clientId"

    const-string v3, "0"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 1301
    .local v13, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v13, v10, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1303
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_1

    .line 1304
    const-string v2, "Notes"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Done! Local Note ADDDITION --  Account ID:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Client ID: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Server ID:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1311
    .end local v10    # "cv":Landroid/content/ContentValues;
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v17    # "uri_update":Landroid/net/Uri;
    :cond_1
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1336
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_1
    return-void

    .line 1311
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v8    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 1316
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_4
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1317
    .restart local v10    # "cv":Landroid/content/ContentValues;
    const-string v2, "flag_sync_needed"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "clientId=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v9, v6, v7

    invoke-virtual {v2, v3, v10, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1323
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 1324
    .local v11, "cv_acc":Landroid/content/ContentValues;
    const-string v2, "syncNeeded"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1325
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1327
    .local v16, "uri_acc":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v11, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1329
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_2

    .line 1330
    const-string v2, "Notes"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed while ADDING local Note ( Status: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") -- "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Account ID:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " Client ID: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1262
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public parseChangeResponse()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 1375
    const-wide/16 v4, -0x1

    .line 1376
    .local v4, "status":J
    const/4 v2, 0x0

    .line 1378
    .local v2, "serverId":Ljava/lang/String;
    :goto_0
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_0

    .line 1379
    iget v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 1384
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1381
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValueInt()I

    move-result v6

    int-to-long v4, v6

    .line 1382
    goto :goto_0

    .line 1389
    :cond_0
    const-wide/16 v6, 0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1

    .line 1391
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1392
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v6, "flag_sync_needed"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1393
    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v8, "server_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    invoke-virtual {v6, v7, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1398
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1399
    .local v1, "cv_acc":Landroid/content/ContentValues;
    const-string v6, "syncNeeded"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1400
    sget-object v6, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v7, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1402
    .local v3, "uri_acc":Landroid/net/Uri;
    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v3, v1, v11, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1404
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_1

    .line 1405
    const-string v6, "Notes"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed while MODIFYING local Note ( Status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") -- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Account ID:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Server ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "cv_acc":Landroid/content/ContentValues;
    .end local v3    # "uri_acc":Landroid/net/Uri;
    :cond_1
    return-void

    .line 1379
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public parseDeleteResponse()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x3

    .line 1339
    const-wide/16 v4, -0x1

    .line 1340
    .local v4, "status":J
    const/4 v2, 0x0

    .line 1342
    .local v2, "serverId":Ljava/lang/String;
    :goto_0
    const/16 v6, 0x9

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v6

    if-eq v6, v8, :cond_0

    .line 1343
    iget v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 1348
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1345
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->getValueInt()I

    move-result v6

    int-to-long v4, v6

    .line 1346
    goto :goto_0

    .line 1353
    :cond_0
    const-wide/16 v6, 0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_1

    .line 1354
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1355
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v6, "flag_sync_needed"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1356
    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const-string v8, "server_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v2, v9, v10

    invoke-virtual {v6, v7, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1361
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1362
    .local v1, "cv_acc":Landroid/content/ContentValues;
    const-string v6, "syncNeeded"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1363
    sget-object v6, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v7, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1365
    .local v3, "uri_acc":Landroid/net/Uri;
    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v3, v1, v11, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1367
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_1

    .line 1368
    const-string v6, "Notes"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed while DELETING local Note ( Status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") -- "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Account ID:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Server ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "cv_acc":Landroid/content/ContentValues;
    .end local v3    # "uri_acc":Landroid/net/Uri;
    :cond_1
    return-void

    .line 1343
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public parseNotesDateTimeToMillis(Ljava/lang/String;)J
    .locals 8
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xd

    const/16 v5, 0xb

    const/4 v4, 0x6

    const/4 v2, 0x4

    .line 1037
    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x8

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x9

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xf

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    .line 1041
    .local v0, "cal":Ljava/util/GregorianCalendar;
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1042
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public responsesParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1416
    :goto_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1418
    iget v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->tag:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1420
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->parseAddResponse()V

    goto :goto_0

    .line 1425
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->parseChangeResponse()V

    goto :goto_0

    .line 1430
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->parseDeleteResponse()V

    goto :goto_0

    .line 1436
    :cond_0
    return-void

    .line 1418
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
