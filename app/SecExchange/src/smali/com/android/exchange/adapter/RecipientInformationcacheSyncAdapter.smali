.class public Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "RecipientInformationcacheSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;
    }
.end annotation


# instance fields
.field mIsLooping:Z


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 44
    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mIsLooping:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mIsLooping:Z

    .line 41
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;
    .locals 2
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    new-instance v0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;-><init>(Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V

    return-object v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;

    invoke-direct {v1, p0, p1, p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;-><init>(Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 66
    .local v1, "p":Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;
    const/4 v2, 0x0

    .line 68
    .local v2, "res":Z
    :try_start_0
    invoke-virtual {v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->parse()Z
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 85
    :goto_0
    invoke-virtual {v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->isLooping()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mIsLooping:Z

    move v3, v2

    .line 86
    :goto_1
    return v3

    .line 69
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    invoke-virtual {v0}, Lcom/android/emailcommon/utility/DeviceAccessException;->printStackTrace()V

    goto :goto_0

    .line 72
    .end local v0    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v3, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpl-double v3, v4, v6

    if-nez v3, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->setIntervalPing()V

    .line 80
    const/4 v3, 0x0

    goto :goto_1

    .line 82
    :cond_0
    throw v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 1
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 2
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    if-nez p3, :cond_0

    .line 349
    const/16 v0, 0x1e

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 350
    const/16 v0, 0x13

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 351
    const/16 v0, 0x15

    const-string v1, "10"

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 352
    const/16 v0, 0x17

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 356
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 358
    :cond_0
    return-void
.end method

.method public wipe()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 363
    const/4 v1, 0x0

    .line 364
    .local v1, "cv":Landroid/content/ContentValues;
    const/4 v0, 0x0

    .line 365
    .local v0, "Nu_of":I
    new-instance v1, Landroid/content/ContentValues;

    .end local v1    # "cv":Landroid/content/ContentValues;
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 366
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v2, "syncKey"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v2, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "_id=?"

    new-array v5, v9, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 371
    iget-object v2, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "accountkey=?"

    new-array v5, v9, [Ljava/lang/String;

    iget-object v6, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 375
    const-string v2, "RecipientInformation Cache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RIC adapter-afterdel wipe"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    return-void
.end method
