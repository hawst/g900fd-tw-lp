.class public Lcom/android/exchange/adapter/ValidateCertParser;
.super Lcom/android/exchange/adapter/Parser;
.source "ValidateCertParser.java"


# instance fields
.field private mResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mStatus:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mResult:Ljava/util/ArrayList;

    .line 27
    return-void
.end method


# virtual methods
.method public getResult()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mResult:Ljava/util/ArrayList;

    return-object v0
.end method

.method public parse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/ValidateCertParser;->nextTag(I)I

    move-result v2

    const/16 v3, 0x2c5

    if-eq v2, v3, :cond_0

    .line 41
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 43
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/ValidateCertParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 44
    iget v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->tag:I

    const/16 v3, 0x2c7

    if-ne v2, v3, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ValidateCertParser;->parseCertificate()V

    goto :goto_0

    .line 46
    :cond_1
    iget v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->tag:I

    const/16 v3, 0x2ca

    if-ne v2, v3, :cond_2

    .line 47
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ValidateCertParser;->getValueInt()I

    move-result v2

    iput v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    .line 49
    iget v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    if-eq v2, v0, :cond_0

    .line 50
    iget v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    invoke-static {v2}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    new-instance v0, Lcom/android/exchange/CommandStatusException;

    iget v1, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    invoke-direct {v0, v1}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v0

    .line 55
    :cond_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ValidateCertParser;->skipTag()V

    goto :goto_0

    .line 58
    :cond_3
    iget v2, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mStatus:I

    if-ne v2, v0, :cond_4

    :goto_1
    return v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public parseCertificate()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    :goto_0
    const/16 v0, 0x2c7

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ValidateCertParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 63
    iget v0, p0, Lcom/android/exchange/adapter/ValidateCertParser;->tag:I

    const/16 v1, 0x2ca

    if-ne v0, v1, :cond_0

    .line 64
    iget-object v0, p0, Lcom/android/exchange/adapter/ValidateCertParser;->mResult:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/ValidateCertParser;->getValueInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ValidateCertParser;->skipTag()V

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method
