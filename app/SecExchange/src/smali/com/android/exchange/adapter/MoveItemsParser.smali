.class public Lcom/android/exchange/adapter/MoveItemsParser;
.super Lcom/android/exchange/adapter/Parser;
.source "MoveItemsParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    }
.end annotation


# instance fields
.field private mNewServerId:Ljava/lang/String;

.field private mResponsesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mService:Lcom/android/exchange/EasSyncService;

.field private mStatusCode:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mResponsesList:Ljava/util/ArrayList;

    .line 60
    iput-object p2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    .line 61
    return-void
.end method


# virtual methods
.method public getMoveItemsResponseList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mResponsesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNewServerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    return v0
.end method

.method public parse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "res":Z
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/MoveItemsParser;->nextTag(I)I

    move-result v1

    const/16 v2, 0x145

    if-eq v1, v2, :cond_0

    .line 127
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 129
    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/MoveItemsParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 130
    iget v1, p0, Lcom/android/exchange/adapter/MoveItemsParser;->tag:I

    const/16 v2, 0x14a

    if-ne v1, v2, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->parseResponse()V

    goto :goto_0

    .line 133
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->skipTag()V

    goto :goto_0

    .line 136
    :cond_2
    return v0
.end method

.method public parseResponse()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 76
    new-instance v0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;

    invoke-direct {v0}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;-><init>()V

    .line 77
    .local v0, "response":Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    :cond_0
    :goto_0
    const/16 v2, 0x14a

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/MoveItemsParser;->nextTag(I)I

    move-result v2

    if-eq v2, v7, :cond_4

    .line 78
    iget v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->tag:I

    const/16 v3, 0x14b

    if-ne v2, v3, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->getValueInt()I

    move-result v1

    .line 81
    .local v1, "status":I
    packed-switch v1, :pswitch_data_0

    .line 102
    :pswitch_0
    const/4 v2, 0x2

    iput v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    .line 105
    :goto_1
    iget v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->setStatusCode(I)V

    .line 106
    if-eq v1, v7, :cond_0

    .line 108
    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v3, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in MoveItems: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :pswitch_1
    iput v6, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    goto :goto_1

    .line 93
    :pswitch_2
    iput v7, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mStatusCode:I

    goto :goto_1

    .line 110
    .end local v1    # "status":I
    :cond_1
    iget v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->tag:I

    const/16 v3, 0x14c

    if-ne v2, v3, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    .line 112
    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v3, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Moved message id is now: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 113
    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mNewServerId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->setNewServerId(Ljava/lang/String;)V

    goto :goto_0

    .line 114
    :cond_2
    iget v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->tag:I

    const/16 v3, 0x147

    if-ne v2, v3, :cond_3

    .line 115
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->setOriginalServerId(Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemsParser;->skipTag()V

    goto/16 :goto_0

    .line 120
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemsParser;->mResponsesList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    return-void

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
