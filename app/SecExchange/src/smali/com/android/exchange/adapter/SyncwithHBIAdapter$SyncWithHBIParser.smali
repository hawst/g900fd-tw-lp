.class public Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "SyncwithHBIAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/SyncwithHBIAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SyncWithHBIParser"
.end annotation


# instance fields
.field private mBindArguments:[Ljava/lang/String;

.field final synthetic this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/SyncwithHBIAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/SyncwithHBIAdapter;)V
    .locals 4
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/SyncwithHBIAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    iput-object p1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    .line 90
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->mBindArguments:[Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 92
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 96
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->findAdapterObject()Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-result-object v0

    .line 97
    .local v0, "adapter":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    if-eqz v0, :cond_0

    .line 98
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v0, p0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;

    move-result-object v2

    iput-object v2, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    .line 99
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->commandsParser()V

    .line 102
    :cond_0
    return-void
.end method

.method public commit()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->commit()V

    .line 118
    :cond_0
    return-void
.end method

.method protected resetParser()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    .line 123
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->findAdapterObject()Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-result-object v0

    .line 124
    .local v0, "adapter":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    if-eqz v0, :cond_0

    .line 125
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v0, p0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;

    move-result-object v2

    iput-object v2, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    .line 127
    :cond_0
    return-void
.end method

.method public responsesParser()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->findAdapterObject()Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-result-object v0

    .line 107
    .local v0, "adapter":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    if-eqz v0, :cond_0

    .line 108
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    invoke-virtual {v0, p0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;

    move-result-object v2

    iput-object v2, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    .line 109
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->this$0:Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->responsesParser()V

    .line 112
    :cond_0
    return-void
.end method
