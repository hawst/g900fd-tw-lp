.class Lcom/android/exchange/adapter/AbstractSyncAdapter$SyncStatusException;
.super Ljava/io/IOException;
.source "AbstractSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/AbstractSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SyncStatusException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final mServerId:Ljava/lang/String;

.field public final mStatus:I
