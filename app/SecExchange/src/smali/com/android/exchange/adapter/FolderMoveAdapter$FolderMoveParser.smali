.class public Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "FolderMoveAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/FolderMoveAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FolderMoveParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/FolderMoveAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V
    .locals 2
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractCommandAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    iput-object p1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    .line 188
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 189
    # getter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$000(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FolderMoveParser"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 194
    return-void
.end method

.method public commit()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$000(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "FolderMoveAdapter.FolderMoveParser.commit"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v1, :cond_1

    .line 207
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$100(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentKey:J
    invoke-static {v2}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$200(Lcom/android/exchange/adapter/FolderMoveAdapter;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentKey:J

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 212
    .local v0, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->toContentValues()Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 227
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 228
    const-string v1, "Applying "

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const-string v4, " mailbox operations."

    invoke-virtual {p0, v1, v3, v4}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 229
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v3, v3, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget-object v3, v3, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mSyncKey:Ljava/lang/String;

    iput-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    .line 230
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->toContentValues()Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :try_start_1
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "com.android.email.provider"

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 238
    const-string v1, "New Account SyncKey: "

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 247
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 250
    .end local v0    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/4 v2, 0x0

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v1, v2}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    .line 251
    const-string v1, "Mahskyript"

    const-string v2, "FolderUpdateAdapter.FolderUpdateParser.commit after callback"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    return-void

    .line 247
    .restart local v0    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 242
    :catch_0
    move-exception v1

    goto :goto_0

    .line 239
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 255
    const-string v0, "Mahskyript"

    const-string v1, "FolderUpdateAdapter.FolderUpdateParser.parse "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    new-instance v1, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    invoke-direct {v1, v2}, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;-><init>(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    iput-object v1, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    .line 258
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->nextTag(I)I

    move-result v0

    const/16 v1, 0x1d5

    if-eq v0, v1, :cond_0

    .line 259
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 260
    :cond_0
    :goto_0
    :sswitch_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->nextTag(I)I

    move-result v0

    if-eq v0, v4, :cond_2

    .line 262
    iget v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->tag:I

    const/16 v1, 0x1d2

    if-ne v0, v1, :cond_1

    .line 263
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mSyncKey:Ljava/lang/String;

    goto :goto_0

    .line 264
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->tag:I

    const/16 v1, 0x1cc

    if-ne v0, v1, :cond_0

    .line 265
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->getValueInt()I

    move-result v1

    iput v1, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    .line 266
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    sparse-switch v0, :sswitch_data_0

    .line 307
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unknown error."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :sswitch_1
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/16 v1, 0x25

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    goto :goto_0

    .line 274
    :sswitch_2
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/16 v1, 0x23

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    goto :goto_0

    .line 278
    :sswitch_3
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/16 v1, 0x24

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    goto :goto_0

    .line 282
    :sswitch_4
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/16 v1, 0x26

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    goto :goto_0

    .line 293
    :sswitch_5
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    const/16 v1, 0x22

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    goto :goto_0

    .line 299
    :sswitch_6
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    # setter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    invoke-static {v0, v4}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I

    .line 300
    const-string v0, "DeviceAccessException"

    const-string v1, "Received in FolderUpdateAdapter"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    new-instance v0, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v1, 0x40001

    const v2, 0x7f060015

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v0

    .line 312
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 313
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->commit()V

    .line 318
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    # getter for: Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/android/exchange/adapter/FolderMoveAdapter;->access$000(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FolderMoveAdapter.FolderRenameParser.parse exit"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return v3

    .line 314
    :cond_4
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->this$0:Lcom/android/exchange/adapter/FolderMoveAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    iget v0, v0, Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;->mStatus:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    .line 317
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->wipe()V

    goto :goto_1

    .line 266
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x9 -> :sswitch_5
        0xa -> :sswitch_5
        0x81 -> :sswitch_6
    .end sparse-switch
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    return-void
.end method
