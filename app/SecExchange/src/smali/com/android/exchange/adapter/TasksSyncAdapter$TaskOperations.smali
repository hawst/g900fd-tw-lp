.class Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
.super Ljava/util/ArrayList;
.source "TasksSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/TasksSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskOperations"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Landroid/content/ContentProviderOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mCount:I

.field private mResults:[Landroid/content/ContentProviderResult;

.field private mTaskIndexCount:I

.field final synthetic this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;


# direct methods
.method private constructor <init>(Lcom/android/exchange/adapter/TasksSyncAdapter;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1781
    iput-object p1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 1784
    iput v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mCount:I

    .line 1787
    iput v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mTaskIndexCount:I

    .line 1789
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mResults:[Landroid/content/ContentProviderResult;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/TasksSyncAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/exchange/adapter/TasksSyncAdapter;
    .param p2, "x1"    # Lcom/android/exchange/adapter/TasksSyncAdapter$1;

    .prologue
    .line 1781
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;)V

    return-void
.end method


# virtual methods
.method public add(Landroid/content/ContentProviderOperation;)Z
    .locals 1
    .param p1, "op"    # Landroid/content/ContentProviderOperation;

    .prologue
    .line 1793
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1794
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mCount:I

    .line 1795
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1781
    check-cast p1, Landroid/content/ContentProviderOperation;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    move-result v0

    return v0
.end method

.method public addReminder(JILjava/lang/String;JJJI)V
    .locals 5
    .param p1, "mins"    # J
    .param p3, "state"    # I
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "start_date"    # J
    .param p7, "due_date"    # J
    .param p9, "accountkey"    # J
    .param p11, "reminder_type"    # I

    .prologue
    .line 1811
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Inside addreminder and going to insert a reminder "

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 1813
    sget-object v0, Lcom/android/emailcommon/provider/Tasks$TaskReminderAlerts;->SPLANNER_REMINDER_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "reminder_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "state"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "subject"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "start_date"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "due_date"

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "accountkey"

    invoke-static {p9, p10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "reminder_type"

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "task_id"

    iget v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mTaskIndexCount:I

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    .line 1823
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "Have to check whether task_id is inserted or taken properly from the tasks table"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 1824
    return-void
.end method

.method public delete(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 1842
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    .line 1843
    return-void
.end method

.method public execute()V
    .locals 6

    .prologue
    .line 1847
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1848
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->isStopped()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 1850
    :try_start_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1851
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    const-string v3, "Executing "

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->size()I

    move-result v4

    const-string v5, " CPO\'s"

    invoke-virtual {v1, v3, v4, v5}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 1854
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "com.android.calendar"

    invoke-virtual {v1, v3, p0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mResults:[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1870
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1871
    return-void

    .line 1858
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Nothing to execute. isEmpty = true"

    aput-object v5, v3, v4

    invoke-virtual {v1, v3}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1859
    :catch_0
    move-exception v0

    .line 1861
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_4
    const-string v1, "ExchTasksSyncAdapter"

    const-string v3, "problem inserting task during server update"

    invoke-static {v1, v3, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1870
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    .line 1862
    :catch_1
    move-exception v0

    .line 1864
    .local v0, "e":Landroid/content/OperationApplicationException;
    :try_start_5
    const-string v1, "ExchTasksSyncAdapter"

    const-string v3, "problem inserting task during server update"

    invoke-static {v1, v3, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1865
    .end local v0    # "e":Landroid/content/OperationApplicationException;
    :catch_2
    move-exception v0

    .line 1867
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "ExchTasksSyncAdapter"

    const-string v3, "problem inserting task during server update"

    invoke-static {v1, v3, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0
.end method

.method public newTask(Landroid/content/ContentProviderOperation;)I
    .locals 1
    .param p1, "op"    # Landroid/content/ContentProviderOperation;

    .prologue
    .line 1799
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mCount:I

    iput v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mTaskIndexCount:I

    .line 1800
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    .line 1801
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->mTaskIndexCount:I

    return v0
.end method
