.class public Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "ComposeMailAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/ComposeMailAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SmartReplyForwardParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/ComposeMailAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V
    .locals 0
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractCommandAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    iput-object p1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    .line 204
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 205
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    return-void
.end method

.method public commit()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    return-void
.end method

.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 227
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    # getter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->isSmartSend:Z
    invoke-static {v2}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$000(Lcom/android/exchange/adapter/ComposeMailAdapter;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    # getter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->isReply:Z
    invoke-static {v2}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$100(Lcom/android/exchange/adapter/ComposeMailAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x547

    .line 229
    .local v0, "startTag":I
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->nextTag(I)I

    move-result v2

    if-eq v2, v0, :cond_2

    .line 230
    new-instance v2, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v2, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v2

    .line 227
    .end local v0    # "startTag":I
    :cond_0
    const/16 v0, 0x546

    goto :goto_0

    :cond_1
    const/16 v0, 0x545

    goto :goto_0

    .line 232
    .restart local v0    # "startTag":I
    :cond_2
    const/4 v1, 0x0

    .line 233
    .local v1, "status":I
    :cond_3
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_4

    .line 235
    const/4 v1, 0x1

    .line 236
    iget v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->tag:I

    const/16 v3, 0x552

    if-ne v2, v3, :cond_3

    .line 237
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->getValueInt()I

    move-result v1

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->log(Ljava/lang/String;)V

    .line 239
    sparse-switch v1, :sswitch_data_0

    .line 287
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0x190

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    .line 293
    :cond_4
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "httpCode mapped for 14.0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    # getter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$200(Lcom/android/exchange/adapter/ComposeMailAdapter;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->log(Ljava/lang/String;)V

    .line 294
    const/4 v2, 0x1

    return v2

    .line 241
    :sswitch_0
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0xc8

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 246
    :sswitch_1
    const-string v2, "SmartReplyForwardParser"

    const-string v3, "Status code 129 received, to block the device"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    new-instance v2, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v3, 0x40001

    const v4, 0x7f060015

    invoke-direct {v2, v3, v4}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v2

    .line 257
    :sswitch_2
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0x1c1

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 261
    :sswitch_3
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0x1f7

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 267
    :sswitch_4
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0x1fb

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 272
    :sswitch_5
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    const/16 v3, 0x1f4

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 275
    :sswitch_6
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 283
    :sswitch_7
    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->this$0:Lcom/android/exchange/adapter/ComposeMailAdapter;

    # setter for: Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I
    invoke-static {v2, v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I

    goto :goto_1

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6e -> :sswitch_5
        0x6f -> :sswitch_3
        0x71 -> :sswitch_4
        0x73 -> :sswitch_4
        0x78 -> :sswitch_5
        0x7a -> :sswitch_4
        0x81 -> :sswitch_1
        0x8c -> :sswitch_2
        0x8e -> :sswitch_2
        0x8f -> :sswitch_2
        0x90 -> :sswitch_2
        0x96 -> :sswitch_6
        0xa8 -> :sswitch_7
        0xa9 -> :sswitch_7
        0xaa -> :sswitch_7
        0xab -> :sswitch_7
        0xac -> :sswitch_7
    .end sparse-switch
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    return-void
.end method
