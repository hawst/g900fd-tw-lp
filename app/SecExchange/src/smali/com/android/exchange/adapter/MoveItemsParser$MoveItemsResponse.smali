.class public Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
.super Ljava/lang/Object;
.source "MoveItemsParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/MoveItemsParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MoveItemsResponse"
.end annotation


# instance fields
.field newServerId:Ljava/lang/String;

.field originalServerId:Ljava/lang/String;

.field statusCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNewServerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->newServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginalServerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->originalServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->statusCode:I

    return v0
.end method

.method public setNewServerId(Ljava/lang/String;)V
    .locals 0
    .param p1, "newServerId"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->newServerId:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setOriginalServerId(Ljava/lang/String;)V
    .locals 0
    .param p1, "originalServerId"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->originalServerId:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "statusCode"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->statusCode:I

    .line 146
    return-void
.end method
