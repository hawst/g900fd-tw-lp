.class public Lcom/android/exchange/adapter/ProvisionParser;
.super Lcom/android/exchange/adapter/Parser;
.source "ProvisionParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    }
.end annotation


# instance fields
.field mIsSupportable:Z

.field mPolicyKey:Ljava/lang/String;

.field mPolicySet:Lcom/android/emailcommon/service/PolicySet;

.field mPolicyStatus:I

.field mRemoteWipe:Z

.field private mService:Lcom/android/exchange/EasSyncService;

.field mUnsupportedPolicies:[Ljava/lang/String;

.field public sbAllow:Ljava/lang/StringBuilder;

.field public sbBlock:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 50
    const-string v0, "0"

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyKey:Ljava/lang/String;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mRemoteWipe:Z

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->sbAllow:Ljava/lang/StringBuilder;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->sbBlock:Ljava/lang/StringBuilder;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyStatus:I

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mIsSupportable:Z

    .line 70
    iput-object p2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    .line 71
    return-void
.end method

.method private deviceSupportsEncryption()Z
    .locals 4

    .prologue
    .line 835
    iget-object v2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const-string v3, "device_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 837
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getStorageEncryptionStatus()I

    move-result v1

    .line 838
    .local v1, "status":I
    iget-object v2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v3, "dpm.getStorageEncryptionStatus returns : "

    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 839
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private deviceSupportsSdCardEncryption()Z
    .locals 4

    .prologue
    .line 843
    iget-object v2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const-string v3, "device_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 845
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getExternalSdCardEncryptionStatus()I

    move-result v1

    .line 846
    .local v1, "status":I
    iget-object v2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v3, "dpm.getExternalSdCardEncryptionStatus returns : "

    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 847
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private parseCharacteristic(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V
    .locals 7
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "sps"    # Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 680
    const/4 v0, 0x1

    .line 682
    .local v0, "enforceInactivityTimer":Z
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v2

    .line 683
    .local v2, "type":I
    const/4 v4, 0x3

    if-ne v2, v4, :cond_1

    const-string v4, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 716
    return-void

    .line 685
    :cond_1
    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    .line 686
    const-string v4, "parm"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 687
    const-string v4, "name"

    invoke-interface {p1, v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 688
    .local v1, "name":Ljava/lang/String;
    const-string v4, "value"

    invoke-interface {p1, v6, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 689
    .local v3, "value":Ljava/lang/String;
    const-string v4, "AEFrequencyValue"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 690
    if-eqz v0, :cond_0

    .line 691
    const-string v4, "0"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 692
    const/4 v4, 0x0

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxScreenLockTime:I

    goto :goto_0

    .line 694
    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxScreenLockTime:I

    goto :goto_0

    .line 697
    :cond_3
    const-string v4, "AEFrequencyType"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 698
    const-string v4, "0"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 699
    const/4 v0, 0x0

    goto :goto_0

    .line 701
    :cond_4
    const-string v4, "DeviceWipeThreshold"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 702
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxPasswordFails:I

    goto :goto_0

    .line 703
    :cond_5
    const-string v4, "CodewordFrequency"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 704
    const-string v4, "MinimumPasswordLength"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 705
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMinPasswordLength:I

    goto/16 :goto_0

    .line 706
    :cond_6
    const-string v4, "PasswordComplexity"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 707
    const-string v4, "0"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 708
    const/16 v4, 0x40

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordMode:I

    goto/16 :goto_0

    .line 710
    :cond_7
    const/16 v4, 0x20

    iput v4, p2, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordMode:I

    goto/16 :goto_0
.end method

.method private parsePolicies()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 797
    :goto_0
    const/16 v0, 0x386

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 798
    iget v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    const/16 v1, 0x387

    if-ne v0, v1, :cond_0

    .line 799
    invoke-direct {p0}, Lcom/android/exchange/adapter/ProvisionParser;->parsePolicy()V

    goto :goto_0

    .line 801
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_0

    .line 804
    :cond_1
    return-void
.end method

.method private parsePolicy()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 767
    const/4 v0, 0x0

    .line 768
    .local v0, "policyType":Ljava/lang/String;
    :goto_0
    const/16 v1, 0x387

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 769
    iget v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 791
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_0

    .line 771
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 772
    iget-object v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Policy type: "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 775
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyKey:Ljava/lang/String;

    goto :goto_0

    .line 778
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v1

    iput v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyStatus:I

    .line 779
    iget-object v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v2, "Policy status: "

    iget v3, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyStatus:I

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    goto :goto_0

    .line 782
    :pswitch_3
    const-string v1, "MS-WAP-Provisioning-XML"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 784
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/ProvisionParser;->parseProvisionDocXml(Ljava/lang/String;)V

    goto :goto_0

    .line 787
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/ProvisionParser;->parseProvisionData()V

    goto :goto_0

    .line 794
    :cond_1
    return-void

    .line 769
    :pswitch_data_0
    .packed-switch 0x388
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private parseProvisionData()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 757
    :goto_0
    const/16 v0, 0x38a

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 758
    iget v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    const/16 v1, 0x38d

    if-ne v0, v1, :cond_0

    .line 759
    invoke-direct {p0}, Lcom/android/exchange/adapter/ProvisionParser;->parseProvisionDocWbxml()V

    goto :goto_0

    .line 761
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_0

    .line 764
    :cond_1
    return-void
.end method

.method private parseProvisionDocWbxml()V
    .locals 59
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    new-instance v54, Ljava/util/ArrayList;

    invoke-direct/range {v54 .. v54}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v54, "unsupportedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 118
    .local v3, "minPasswordLength":I
    const/4 v4, 0x0

    .line 119
    .local v4, "passwordMode":I
    const/4 v5, 0x0

    .line 120
    .local v5, "maxPasswordFails":I
    const/4 v9, 0x0

    .line 121
    .local v9, "passwordExpirationDays":I
    const/4 v10, 0x0

    .line 122
    .local v10, "passwordHistory":I
    const/16 v23, 0x0

    .line 123
    .local v23, "passwordComplexChars":I
    const/16 v49, 0x0

    .line 128
    .local v49, "passwordEnabled":Z
    const/4 v6, 0x0

    .line 129
    .local v6, "maxScreenLockTime":I
    const/4 v8, 0x0

    .line 132
    .local v8, "passwordRecoverable":Z
    const/4 v11, 0x1

    .line 133
    .local v11, "attachmentsEnabled":Z
    const/16 v38, 0x1

    .line 134
    .local v38, "simplePasswordEnabled":Z
    const/4 v12, 0x0

    .line 135
    .local v12, "maxAttachmentSize":I
    const/16 v18, 0x1

    .line 136
    .local v18, "allowHTMLEmail":Z
    const/16 v21, 0x0

    .line 140
    .local v21, "requireManualSyncWhenRoaming":Z
    const/16 v24, 0x0

    .line 141
    .local v24, "maxCalendarAgeFilter":I
    const/16 v25, 0x0

    .line 143
    .local v25, "maxEmailAgeFilter":I
    const/16 v26, 0x0

    .line 144
    .local v26, "maxEmailBodyTruncationSize":I
    const/16 v27, 0x0

    .line 145
    .local v27, "maxEmailHtmlBodyTruncationSize":I
    const/16 v28, 0x0

    .line 146
    .local v28, "requireSignedSMIMEMessages":Z
    const/16 v29, 0x0

    .line 147
    .local v29, "requireEncryptedSMIMEMessages":Z
    const/16 v30, -0x1

    .line 148
    .local v30, "requireSignedSMIMEAlgorithm":I
    const/16 v31, -0x1

    .line 149
    .local v31, "requireEncryptionSMIMEAlgorithm":I
    const/16 v32, -0x1

    .line 150
    .local v32, "allowSMIMEEncryptionAlgorithmNegotiation":I
    const/16 v33, 0x1

    .line 153
    .local v33, "allowSMIMESoftCerts":Z
    const/4 v13, 0x1

    .line 154
    .local v13, "allowStorageCard":Z
    const/4 v14, 0x1

    .line 155
    .local v14, "allowCamera":Z
    const/4 v15, 0x1

    .line 156
    .local v15, "allowWifi":Z
    const/16 v16, 0x1

    .line 157
    .local v16, "allowTextMessaging":Z
    const/16 v17, 0x1

    .line 158
    .local v17, "allowPOPIMAPEmail":Z
    const/16 v19, 0x1

    .line 159
    .local v19, "allowBrowser":Z
    const/16 v20, 0x1

    .line 161
    .local v20, "allowInternetSharing":Z
    const/16 v22, 0x2

    .line 162
    .local v22, "allowBluetoothMode":I
    const/16 v34, 0x1

    .line 163
    .local v34, "allowDesktopSync":Z
    const/16 v35, 0x1

    .line 164
    .local v35, "allowIrDA":Z
    const/16 v43, 0x1

    .line 167
    .local v43, "allowUnsignedPackage":Z
    const/16 v36, 0x0

    .line 168
    .local v36, "encryptionRequired":Z
    const/16 v37, 0x0

    .line 171
    .local v37, "deviceEncryptionEnabled":Z
    const-string v40, ""

    .line 173
    .local v40, "mAppBlockListInRom":Ljava/lang/String;
    const-string v39, ""

    .line 174
    .local v39, "mAppAllowListThirdParty":Ljava/lang/String;
    const/16 v41, 0x1

    .line 175
    .local v41, "allowUnsignedApp":Z
    const/16 v42, 0x1

    .line 178
    .local v42, "allowUnsignedInstallationPkg":Z
    const/16 v44, 0x0

    .line 179
    .local v44, "context":Landroid/content/Context;
    const/16 v48, 0x0

    .line 181
    .local v48, "isInContainer":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    if-eqz v2, :cond_0

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v44, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 183
    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v48

    .line 186
    :cond_0
    :goto_0
    const/16 v2, 0x38d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v2

    const/4 v7, 0x3

    if-eq v2, v7, :cond_11

    .line 187
    const/16 v53, 0x1

    .line 188
    .local v53, "tagIsSupported":Z
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 462
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    .line 465
    :cond_1
    :goto_1
    if-nez v53, :cond_0

    .line 466
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Policy not supported: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/ProvisionParser;->log(Ljava/lang/String;)V

    .line 467
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mIsSupportable:Z

    goto :goto_0

    .line 190
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    .line 191
    const/16 v49, 0x1

    .line 192
    if-nez v4, :cond_1

    .line 193
    const/16 v4, 0x20

    goto :goto_1

    .line 198
    :pswitch_2
    if-eqz v49, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 199
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v55

    .line 200
    .local v55, "val":I
    move/from16 v3, v55

    goto :goto_1

    .line 202
    .end local v55    # "val":I
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_1

    .line 205
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    if-eqz v49, :cond_1

    .line 206
    const/16 v4, 0x40

    goto :goto_1

    .line 211
    :pswitch_4
    if-eqz v49, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 212
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v55

    .line 213
    .restart local v55    # "val":I
    move/from16 v6, v55

    goto :goto_1

    .line 215
    .end local v55    # "val":I
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_1

    .line 218
    :pswitch_5
    if-eqz v49, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v5

    goto :goto_1

    .line 221
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_1

    .line 224
    :pswitch_6
    if-eqz v49, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v9

    goto/16 :goto_1

    .line 227
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 230
    :pswitch_7
    if-eqz v49, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 231
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v10

    goto/16 :goto_1

    .line 233
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 240
    :pswitch_8
    if-eqz v49, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 241
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 242
    const/16 v38, 0x0

    goto/16 :goto_1

    .line 244
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 248
    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 249
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 253
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    if-eqz v49, :cond_1

    .line 254
    if-nez v48, :cond_1

    .line 255
    const/4 v8, 0x1

    goto/16 :goto_1

    .line 259
    :pswitch_b
    if-eqz v11, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 260
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v12

    goto/16 :goto_1

    .line 262
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 266
    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_a

    if-eqz v49, :cond_a

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->deviceSupportsEncryption()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 268
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eqz v2, :cond_9

    .line 269
    const/16 v53, 0x0

    .line 270
    const v2, 0x7f060021

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 273
    :cond_9
    const/16 v36, 0x1

    goto/16 :goto_1

    .line 276
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 285
    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    if-eqz v49, :cond_1

    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 286
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->deviceSupportsSdCardEncryption()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 287
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eqz v2, :cond_b

    .line 288
    const/16 v53, 0x0

    .line 289
    const v2, 0x7f060020

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 292
    :cond_b
    const/16 v37, 0x1

    goto/16 :goto_1

    .line 303
    :pswitch_e
    if-eqz v49, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 304
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v23

    goto/16 :goto_1

    .line 306
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto/16 :goto_1

    .line 309
    :pswitch_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 310
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    if-eqz v2, :cond_d

    .line 311
    const/16 v53, 0x0

    .line 312
    const v2, 0x7f060022

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 315
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 319
    :pswitch_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 320
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 324
    :pswitch_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 325
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 329
    :pswitch_12
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 331
    if-nez v44, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    if-eqz v2, :cond_e

    .line 332
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v44, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 333
    :cond_e
    if-eqz v44, :cond_1

    .line 334
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 335
    .local v50, "productName":Ljava/lang/String;
    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->isNonPhone(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->notSupportSMS(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 337
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 344
    .end local v50    # "productName":Ljava/lang/String;
    :pswitch_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 345
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 349
    :pswitch_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 350
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 354
    :pswitch_15
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 355
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 359
    :pswitch_16
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 360
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 364
    :pswitch_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    .line 367
    if-nez v44, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    if-eqz v2, :cond_f

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v44, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 369
    :cond_f
    if-eqz v44, :cond_1

    .line 370
    invoke-static/range {v44 .. v44}, Lcom/android/emailcommon/utility/Utility;->isNonPhone(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 371
    const/16 v21, 0x1

    goto/16 :goto_1

    .line 376
    :pswitch_18
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v22

    .line 377
    goto/16 :goto_1

    .line 379
    :pswitch_19
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v24

    .line 380
    goto/16 :goto_1

    .line 382
    :pswitch_1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v25

    .line 383
    goto/16 :goto_1

    .line 385
    :pswitch_1b
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v26

    if-gez v26, :cond_1

    .line 386
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 389
    :pswitch_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v27

    if-gez v27, :cond_1

    .line 390
    const/16 v27, 0x0

    goto/16 :goto_1

    .line 393
    :pswitch_1d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    .line 394
    const/16 v28, 0x1

    goto/16 :goto_1

    .line 398
    :pswitch_1e
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_1

    .line 399
    const/16 v29, 0x1

    goto/16 :goto_1

    .line 404
    :pswitch_1f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 405
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v30

    goto/16 :goto_1

    .line 409
    :pswitch_20
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 410
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v31

    goto/16 :goto_1

    .line 414
    :pswitch_21
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->hasContent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 415
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v32

    goto/16 :goto_1

    .line 420
    :pswitch_22
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 421
    const/16 v33, 0x0

    goto/16 :goto_1

    .line 425
    :pswitch_23
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 426
    const/16 v34, 0x0

    goto/16 :goto_1

    .line 430
    :pswitch_24
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    .line 431
    const/16 v35, 0x0

    goto/16 :goto_1

    .line 436
    :pswitch_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    if-nez v48, :cond_1

    .line 437
    const/16 v41, 0x0

    goto/16 :goto_1

    .line 442
    :pswitch_26
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v2

    if-nez v2, :cond_1

    if-nez v48, :cond_1

    .line 444
    const/16 v42, 0x0

    goto/16 :goto_1

    .line 453
    :pswitch_27
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbBlock:Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbAllow:Ljava/lang/StringBuilder;

    move-object/from16 v56, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v2, v7, v1}, Lcom/android/exchange/adapter/ProvisionParser;->specifiesApplications(ILjava/lang/StringBuilder;Ljava/lang/StringBuilder;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbAllow:Ljava/lang/StringBuilder;

    if-eqz v2, :cond_10

    if-nez v48, :cond_10

    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbAllow:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v39

    .line 456
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbBlock:Ljava/lang/StringBuilder;

    if-eqz v2, :cond_1

    if-nez v48, :cond_1

    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->sbBlock:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v40

    goto/16 :goto_1

    .line 473
    .end local v53    # "tagIsSupported":Z
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    if-eqz v2, :cond_12

    .line 474
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/16 v56, 0x0

    const-string v57, "ProvisionParser"

    aput-object v57, v7, v56

    const/16 v56, 0x1

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    const-string v58, "PasswordEnabled = "

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move/from16 v1, v49

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v7, v56

    invoke-virtual {v2, v7}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 479
    :cond_12
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 480
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mUnsupportedPolicies:[Ljava/lang/String;

    .line 481
    const/16 v45, 0x0

    .line 483
    .local v45, "i":I
    if-nez v44, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    if-eqz v2, :cond_13

    .line 484
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v44, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 486
    :cond_13
    if-eqz v44, :cond_14

    .line 487
    invoke-virtual/range {v44 .. v44}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v52

    .line 488
    .local v52, "resources":Landroid/content/res/Resources;
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v47

    .local v47, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .line 489
    .local v51, "res":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mUnsupportedPolicies:[Ljava/lang/String;

    add-int/lit8 v46, v45, 0x1

    .end local v45    # "i":I
    .local v46, "i":I
    move-object/from16 v0, v52

    move/from16 v1, v51

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v45

    move/from16 v45, v46

    .line 490
    .end local v46    # "i":I
    .restart local v45    # "i":I
    goto :goto_2

    .line 494
    .end local v45    # "i":I
    .end local v47    # "i$":Ljava/util/Iterator;
    .end local v51    # "res":I
    .end local v52    # "resources":Landroid/content/res/Resources;
    :cond_14
    new-instance v2, Lcom/android/emailcommon/service/PolicySet;

    const/4 v7, 0x1

    invoke-direct/range {v2 .. v42}, Lcom/android/emailcommon/service/PolicySet;-><init>(IIIIZZIIZIZZZZZZZZZIIIIIIZZIIIZZZZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v2, :cond_15

    .line 510
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyKey:Ljava/lang/String;

    iput-object v7, v2, Lcom/android/emailcommon/service/PolicySet;->mSecuritySyncKey:Ljava/lang/String;

    .line 511
    :cond_15
    return-void

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x38e
        :pswitch_1
        :pswitch_3
        :pswitch_d
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_b
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_f
        :pswitch_10
        :pswitch_c
        :pswitch_25
        :pswitch_26
        :pswitch_e
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_18
        :pswitch_24
        :pswitch_17
        :pswitch_23
        :pswitch_19
        :pswitch_14
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_15
        :pswitch_13
        :pswitch_0
        :pswitch_16
        :pswitch_27
        :pswitch_0
        :pswitch_27
    .end packed-switch
.end method

.method private parseRegistry(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V
    .locals 4
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "sps"    # Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 721
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v1

    .line 722
    .local v1, "type":I
    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    const-string v2, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 731
    return-void

    .line 724
    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 725
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 726
    .local v0, "name":Ljava/lang/String;
    const-string v2, "characteristic"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 727
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/ProvisionParser;->parseCharacteristic(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V

    goto :goto_0
.end method

.method private parseSecurityPolicy(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)Z
    .locals 8
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "sps"    # Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 657
    const/4 v1, 0x1

    .line 659
    .local v1, "passwordRequired":Z
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v3

    .line 660
    .local v3, "type":I
    const/4 v5, 0x3

    if-ne v3, v5, :cond_1

    const-string v5, "characteristic"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 675
    return v1

    .line 662
    :cond_1
    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    .line 663
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 664
    .local v2, "tagName":Ljava/lang/String;
    const-string v5, "parm"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 665
    const-string v5, "name"

    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 666
    .local v0, "name":Ljava/lang/String;
    const-string v5, "4131"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 667
    const-string v5, "value"

    invoke-interface {p1, v7, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 668
    .local v4, "value":Ljava/lang/String;
    const-string v5, "1"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 669
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private parseWapProvisioningDoc(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V
    .locals 5
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "sps"    # Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 736
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v2

    .line 737
    .local v2, "type":I
    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    const-string v3, "wap-provisioningdoc"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 754
    :goto_0
    return-void

    .line 739
    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 740
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 741
    .local v1, "name":Ljava/lang/String;
    const-string v3, "characteristic"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 742
    const/4 v3, 0x0

    const-string v4, "type"

    invoke-interface {p1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 743
    .local v0, "atype":Ljava/lang/String;
    const-string v3, "SecurityPolicy"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 744
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/ProvisionParser;->parseSecurityPolicy(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    .line 747
    :cond_2
    const-string v3, "Registry"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 748
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/ProvisionParser;->parseRegistry(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V

    goto :goto_0
.end method

.method private specifiesApplications(ILjava/lang/StringBuilder;Ljava/lang/StringBuilder;)Z
    .locals 5
    .param p1, "endTag"    # I
    .param p2, "sbBlock"    # Ljava/lang/StringBuilder;
    .param p3, "sbAllow"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 523
    const/4 v1, 0x0

    .line 524
    .local v1, "specifiesApplications":Z
    const/4 v2, 0x0

    .line 526
    .local v2, "str":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 527
    iget v3, p0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    packed-switch v3, :pswitch_data_0

    .line 543
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 548
    throw v0

    .line 529
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 530
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 531
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    const/4 v1, 0x1

    goto :goto_0

    .line 536
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 537
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 538
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 540
    :cond_1
    const/4 v1, 0x1

    .line 541
    goto :goto_0

    .line 549
    :catch_1
    move-exception v0

    .line 550
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 552
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    return v1

    .line 527
    :pswitch_data_0
    .packed-switch 0x3b8
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public clearUnsupportedPolicies()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    invoke-static {v0, v1}, Lcom/android/exchange/SecurityPolicyDelegate;->clearUnsupportedPolicies(Landroid/content/Context;Lcom/android/emailcommon/service/PolicySet;)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mIsSupportable:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mUnsupportedPolicies:[Ljava/lang/String;

    .line 97
    return-void
.end method

.method public getPolicyKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyKey:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicySet()Lcom/android/emailcommon/service/PolicySet;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    return-object v0
.end method

.method public getRemoteWipe()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mRemoteWipe:Z

    return v0
.end method

.method public getUnsupportedPolicies()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mUnsupportedPolicies:[Ljava/lang/String;

    return-object v0
.end method

.method public hasSupportablePolicySet()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mIsSupportable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parse()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 808
    const/4 v0, 0x0

    .line 809
    .local v0, "res":Z
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v4

    const/16 v5, 0x385

    if-eq v4, v5, :cond_0

    .line 810
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2

    .line 815
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->getValueInt()I

    move-result v1

    .line 816
    .local v1, "status":I
    iget-object v4, p0, Lcom/android/exchange/adapter/ProvisionParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v5, "Provision status: "

    invoke-virtual {v4, v5, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 817
    if-ne v1, v2, :cond_1

    move v0, v2

    .line 812
    .end local v1    # "status":I
    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ProvisionParser;->nextTag(I)I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    .line 813
    iget v4, p0, Lcom/android/exchange/adapter/ProvisionParser;->tag:I

    sparse-switch v4, :sswitch_data_0

    .line 828
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ProvisionParser;->skipTag()V

    goto :goto_0

    .restart local v1    # "status":I
    :cond_1
    move v0, v3

    .line 817
    goto :goto_0

    .line 820
    .end local v1    # "status":I
    :sswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/ProvisionParser;->parsePolicies()V

    .line 821
    iget-object v4, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicyKey:Ljava/lang/String;

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move v0, v2

    .line 822
    :goto_1
    goto :goto_0

    :cond_3
    move v0, v3

    .line 821
    goto :goto_1

    .line 824
    :sswitch_2
    iput-boolean v2, p0, Lcom/android/exchange/adapter/ProvisionParser;->mRemoteWipe:Z

    .line 825
    const/4 v0, 0x1

    .line 826
    goto :goto_0

    .line 831
    :cond_4
    return v0

    .line 813
    nop

    :sswitch_data_0
    .sparse-switch
        0x386 -> :sswitch_1
        0x38b -> :sswitch_0
        0x38c -> :sswitch_2
    .end sparse-switch
.end method

.method parseProvisionDocXml(Ljava/lang/String;)V
    .locals 50
    .param p1, "doc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 612
    new-instance v47, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;

    move-object/from16 v0, v47

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;-><init>(Lcom/android/exchange/adapter/ProvisionParser;)V

    .line 614
    .local v47, "sps":Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v45

    .line 615
    .local v45, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual/range {v45 .. v45}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v46

    .line 616
    .local v46, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const-string v4, "UTF-8"

    move-object/from16 v0, v46

    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 617
    invoke-interface/range {v46 .. v46}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v49

    .line 618
    .local v49, "type":I
    if-nez v49, :cond_0

    .line 619
    invoke-interface/range {v46 .. v46}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v49

    .line 620
    const/4 v3, 0x2

    move/from16 v0, v49

    if-ne v0, v3, :cond_0

    .line 621
    invoke-interface/range {v46 .. v46}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v48

    .line 622
    .local v48, "tagName":Ljava/lang/String;
    const-string v3, "wap-provisioningdoc"

    move-object/from16 v0, v48

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 624
    move-object/from16 v0, p0

    move-object/from16 v1, v46

    move-object/from16 v2, v47

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/ProvisionParser;->parseWapProvisioningDoc(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    .end local v48    # "tagName":Ljava/lang/String;
    :cond_0
    new-instance v3, Lcom/android/emailcommon/service/PolicySet;

    move-object/from16 v0, v47

    iget v4, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMinPasswordLength:I

    move-object/from16 v0, v47

    iget v5, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordMode:I

    move-object/from16 v0, v47

    iget v6, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxPasswordFails:I

    move-object/from16 v0, v47

    iget v7, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxScreenLockTime:I

    const/4 v8, 0x0

    move-object/from16 v0, v47

    iget-boolean v9, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordRecoverable:Z

    move-object/from16 v0, v47

    iget v10, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordExpiration:I

    move-object/from16 v0, v47

    iget v11, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordHistory:I

    move-object/from16 v0, v47

    iget-boolean v12, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAttachmentsEnabled:Z

    move-object/from16 v0, v47

    iget v13, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxAttachmentSize:I

    move-object/from16 v0, v47

    iget-boolean v14, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowStorageCard:Z

    move-object/from16 v0, v47

    iget-boolean v15, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowCamera:Z

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowWifi:Z

    move/from16 v16, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowTextMessaging:Z

    move/from16 v17, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowPOPIMAPEmail:Z

    move/from16 v18, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowHTMLEmail:Z

    move/from16 v19, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowBrowser:Z

    move/from16 v20, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowInternetSharing:Z

    move/from16 v21, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireManualSyncWhenRoaming:Z

    move/from16 v22, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowBluetoothMode:I

    move/from16 v23, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mPasswordComplexChars:I

    move/from16 v24, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxCalendarAgeFilter:I

    move/from16 v25, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxEmailAgeFilter:I

    move/from16 v26, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxEmailBodyTruncationSize:I

    move/from16 v27, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mMaxEmailHtmlBodyTruncationSize:I

    move/from16 v28, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireSignedSMIMEMessages:Z

    move/from16 v29, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireEncryptedSMIMEMessages:Z

    move/from16 v30, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireSignedSMIMEAlgorithm:I

    move/from16 v31, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireEncryptionSMIMEAlgorithm:I

    move/from16 v32, v0

    move-object/from16 v0, v47

    iget v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowSMIMEEncryptionAlgorithmNegotiation:I

    move/from16 v33, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowSMIMESoftCerts:Z

    move/from16 v34, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowDesktopSync:Z

    move/from16 v35, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowIrDA:Z

    move/from16 v36, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mRequireEncryption:Z

    move/from16 v37, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mDeviceEncryptionEnabled:Z

    move/from16 v38, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mSimplePasswordEnabled:Z

    move/from16 v39, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowList:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mBlockList:Ljava/lang/String;

    move-object/from16 v41, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowUnsignedApp:Z

    move/from16 v42, v0

    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ProvisionParser$ShadowPolicySet;->mAllowUnsignedInstallationPkg:Z

    move/from16 v43, v0

    invoke-direct/range {v3 .. v43}, Lcom/android/emailcommon/service/PolicySet;-><init>(IIIIZZIIZIZZZZZZZZZIIIIIIZZIIIZZZZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 649
    return-void

    .line 628
    .end local v45    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v46    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v49    # "type":I
    :catch_0
    move-exception v44

    .line 629
    .local v44, "e":Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3
.end method

.method public updateSecuritySyncKey(Ljava/lang/String;)V
    .locals 1
    .param p1, "newSecuritySyncKey"    # Ljava/lang/String;

    .prologue
    .line 851
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/android/exchange/adapter/ProvisionParser;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iput-object p1, v0, Lcom/android/emailcommon/service/PolicySet;->mSecuritySyncKey:Ljava/lang/String;

    .line 853
    :cond_0
    return-void
.end method
