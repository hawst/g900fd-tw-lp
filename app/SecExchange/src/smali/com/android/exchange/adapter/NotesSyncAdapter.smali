.class public Lcom/android/exchange/adapter/NotesSyncAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "NotesSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;,
        Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    }
.end annotation


# instance fields
.field mIsLooping:Z

.field mNoteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mIsLooping:Z

    .line 50
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->committAccount()V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mNoteList:Ljava/util/ArrayList;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mIsLooping:Z

    .line 42
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->committAccount()V

    .line 43
    return-void
.end method

.method private queryDBForDeletedNotes(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    .local p1, "noteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;>;"
    const/4 v13, 0x0

    .line 575
    .local v13, "c_deleted":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 576
    .local v17, "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "accountKey"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "server_id"

    aput-object v3, v4, v2

    .line 582
    .local v4, "projectionDeletedNotes":[Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "accountKey=?"

    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 587
    if-eqz v13, :cond_0

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v16, 0x1

    .line 589
    .local v16, "nextRow":Z
    :goto_0
    if-eqz v16, :cond_1

    .line 590
    new-instance v6, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v12}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    .end local v17    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .local v6, "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    const/4 v2, 0x2

    :try_start_1
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmServerIDNote(Ljava/lang/String;)V

    .line 594
    const/4 v2, 0x3

    invoke-virtual {v6, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setSyncNeededFlag(I)V

    .line 595
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 598
    .local v14, "cv":Landroid/content/ContentValues;
    const-string v2, "flag_sync_needed"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 599
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 601
    .local v18, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v14, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 603
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 604
    .local v15, "cv_acc":Landroid/content/ContentValues;
    const-string v2, "syncNeeded"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 605
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 607
    .local v19, "uri_acc":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v15, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 609
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v16

    move-object/from16 v17, v6

    .line 611
    .end local v6    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v17    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto/16 :goto_0

    .line 587
    .end local v14    # "cv":Landroid/content/ContentValues;
    .end local v15    # "cv_acc":Landroid/content/ContentValues;
    .end local v16    # "nextRow":Z
    .end local v18    # "uri":Landroid/net/Uri;
    .end local v19    # "uri_acc":Landroid/net/Uri;
    :cond_0
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 613
    .restart local v16    # "nextRow":Z
    :cond_1
    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 614
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 618
    :cond_2
    return-object p1

    .line 613
    .end local v16    # "nextRow":Z
    :catchall_0
    move-exception v2

    move-object/from16 v6, v17

    .end local v17    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v6    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :goto_1
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 614
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 613
    .restart local v16    # "nextRow":Z
    :catchall_1
    move-exception v2

    goto :goto_1
.end method

.method private queryDBForLocalChanges()Ljava/util/ArrayList;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;",
            ">;"
        }
    .end annotation

    .prologue
    .line 462
    const/16 v18, 0x0

    .line 463
    .local v18, "c":Landroid/database/Cursor;
    const/16 v20, 0x0

    .line 464
    .local v20, "c_msg":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 466
    .local v19, "c_body":Landroid/database/Cursor;
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v26, "noteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;>;"
    const/4 v10, 0x0

    .line 469
    .local v10, "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    const/16 v2, 0x8

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v17, v2

    const/4 v2, 0x1

    const-string v3, "accountKey"

    aput-object v3, v17, v2

    const/4 v2, 0x2

    const-string v3, "clientId"

    aput-object v3, v17, v2

    const/4 v2, 0x3

    const-string v3, "subject"

    aput-object v3, v17, v2

    const/4 v2, 0x4

    const-string v3, "last_modified_date"

    aput-object v3, v17, v2

    const/4 v2, 0x5

    const-string v3, "categories"

    aput-object v3, v17, v2

    const/4 v2, 0x6

    const-string v3, "flag_sync_needed"

    aput-object v3, v17, v2

    const/4 v2, 0x7

    const-string v3, "server_id"

    aput-object v3, v17, v2

    .line 482
    .local v17, "NEW_CLIENT_MESSAGE_PROJECTION":[Ljava/lang/String;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "accountKey"

    aput-object v3, v4, v2

    .line 486
    .local v4, "projection":[Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "accountKey=?"

    const/4 v7, 0x1

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 491
    if-eqz v18, :cond_3

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    const/16 v24, 0x1

    .local v24, "nextRow":Z
    :goto_0
    move-object/from16 v25, v10

    .line 493
    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .local v25, "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :goto_1
    if-eqz v24, :cond_4

    .line 495
    :try_start_1
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 497
    .local v6, "uri_msg":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v7, v17

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 500
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 502
    .local v8, "uri_body":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v9, Lcom/android/emailcommon/provider/Notes$Body;->BODY_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 505
    if-eqz v20, :cond_e

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 507
    new-instance v10, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v11, p0

    invoke-direct/range {v10 .. v16}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 511
    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    const/4 v2, 0x2

    :try_start_2
    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmClientID(Ljava/lang/String;)V

    .line 512
    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmSubject(Ljava/lang/String;)V

    .line 513
    const/4 v2, 0x4

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmLastModifyDate(Ljava/lang/String;)V

    .line 514
    const/4 v2, 0x5

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmCategories(Ljava/lang/String;)V

    .line 515
    const/4 v2, 0x6

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setSyncNeededFlag(I)V

    .line 516
    const/4 v2, 0x7

    move-object/from16 v0, v20

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setmServerIDNote(Ljava/lang/String;)V

    .line 518
    if-eqz v19, :cond_0

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 520
    const/4 v2, 0x2

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodyData(Ljava/lang/String;)V

    .line 521
    const/4 v2, 0x4

    move-object/from16 v0, v19

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->setBodyType(I)V

    .line 526
    :cond_0
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    new-instance v21, Landroid/content/ContentValues;

    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    .line 529
    .local v21, "cv":Landroid/content/ContentValues;
    const-string v2, "flag_sync_needed"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 530
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v27

    .line 532
    .local v27, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v2, v0, v1, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 534
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 535
    .local v22, "cv_acc":Landroid/content/ContentValues;
    const-string v2, "syncNeeded"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 536
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v28

    .line 538
    .local v28, "uri_acc":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v22

    invoke-virtual {v2, v0, v1, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 541
    .end local v21    # "cv":Landroid/content/ContentValues;
    .end local v22    # "cv_acc":Landroid/content/ContentValues;
    .end local v27    # "uri":Landroid/net/Uri;
    .end local v28    # "uri_acc":Landroid/net/Uri;
    :goto_2
    if-eqz v19, :cond_1

    .line 542
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 544
    :cond_1
    if-eqz v20, :cond_2

    .line 545
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 547
    :cond_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v24

    move-object/from16 v25, v10

    .line 548
    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto/16 :goto_1

    .line 491
    .end local v6    # "uri_msg":Landroid/net/Uri;
    .end local v8    # "uri_body":Landroid/net/Uri;
    .end local v24    # "nextRow":Z
    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_3
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 553
    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v24    # "nextRow":Z
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_4
    if-eqz v18, :cond_5

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_5

    .line 554
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 556
    :cond_5
    if-eqz v19, :cond_6

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_6

    .line 557
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 559
    :cond_6
    if-eqz v20, :cond_d

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_d

    .line 560
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    move-object/from16 v10, v25

    .line 564
    .end local v24    # "nextRow":Z
    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter;->queryDBForDeletedNotes(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v26

    .line 566
    return-object v26

    .line 549
    :catch_0
    move-exception v23

    .line 550
    .local v23, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_3
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 553
    if-eqz v18, :cond_8

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_8

    .line 554
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 556
    :cond_8
    if-eqz v19, :cond_9

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_9

    .line 557
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 559
    :cond_9
    if-eqz v20, :cond_7

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_7

    .line 560
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 553
    .end local v23    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v18, :cond_a

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_a

    .line 554
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 556
    :cond_a
    if-eqz v19, :cond_b

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_b

    .line 557
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 559
    :cond_b
    if-eqz v20, :cond_c

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_c

    .line 560
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 553
    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v24    # "nextRow":Z
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :catchall_1
    move-exception v2

    move-object/from16 v10, v25

    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto :goto_5

    .line 549
    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :catch_1
    move-exception v23

    move-object/from16 v10, v25

    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto :goto_4

    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_d
    move-object/from16 v10, v25

    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto :goto_3

    .end local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v6    # "uri_msg":Landroid/net/Uri;
    .restart local v8    # "uri_body":Landroid/net/Uri;
    .restart local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_e
    move-object/from16 v10, v25

    .end local v25    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    .restart local v10    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    goto/16 :goto_2
.end method


# virtual methods
.method public cleanup()V
    .locals 18

    .prologue
    .line 58
    const/4 v10, 0x0

    .line 59
    .local v10, "count":I
    const/4 v8, 0x0

    .line 60
    .local v8, "c_deleted":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 61
    .local v9, "c_updated":Landroid/database/Cursor;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 66
    .local v6, "argument":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    .line 71
    .local v4, "projection":[Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "flag_sync_needed=?"

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 74
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v11, 0x1

    .line 75
    .local v11, "nextRow":Z
    :goto_0
    if-eqz v11, :cond_1

    .line 76
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 78
    .local v12, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v12, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 80
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 82
    .local v13, "uri2":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v13, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 84
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    .line 85
    goto :goto_0

    .line 74
    .end local v11    # "nextRow":Z
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v13    # "uri2":Landroid/net/Uri;
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 88
    .restart local v11    # "nextRow":Z
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "flag_sync_needed=?"

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 91
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "flag_sync_needed=?"

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 95
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v11, 0x1

    .line 96
    :goto_1
    if-eqz v11, :cond_3

    .line 97
    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 99
    .restart local v12    # "uri":Landroid/net/Uri;
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 100
    .local v14, "values":Landroid/content/ContentValues;
    const-string v2, "flag_sync_needed"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v12, v14, v3, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 102
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    .line 103
    goto :goto_1

    .line 95
    .end local v12    # "uri":Landroid/net/Uri;
    .end local v14    # "values":Landroid/content/ContentValues;
    :cond_2
    const/4 v11, 0x0

    goto :goto_1

    .line 106
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v5, "flag_sync_needed=?"

    invoke-virtual {v2, v3, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 110
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 111
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 113
    :cond_4
    if-eqz v8, :cond_5

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_5

    .line 114
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 118
    :cond_5
    return-void

    .line 110
    .end local v11    # "nextRow":Z
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_6

    .line 111
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 113
    :cond_6
    if-eqz v8, :cond_7

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_7

    .line 114
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v2
.end method

.method public committAccount()V
    .locals 8

    .prologue
    .line 439
    const-wide/16 v2, -0x1

    .line 441
    .local v2, "rowId":J
    iget-object v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {p0, v4, v5}, Lcom/android/exchange/adapter/NotesSyncAdapter;->isAccountPresent(J)Z

    move-result v4

    if-nez v4, :cond_0

    .line 442
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 443
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v4, "_id"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 444
    const-string v4, "accountKey"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 445
    const-string v4, "mailBoxId"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 447
    const-string v4, "accountType"

    const-string v5, "com.android.exchange"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v4, "emailAddress"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v4, "displayName"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    const-string v4, "newMessageCount"

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mNewMessageCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 451
    iget-object v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 452
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 453
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v4, :cond_0

    .line 454
    const-string v4, "Notes"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Account Added -- Account ID:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Email: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Notes Mailbox ID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    const-string v0, "IPM.StickyNote"

    return-object v0
.end method

.method public getNoteFilter()Ljava/lang/String;
    .locals 3

    .prologue
    .line 172
    sget-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    .line 173
    .local v0, "filter":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v1, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    .line 175
    .local v1, "syncLookback":I
    packed-switch v1, :pswitch_data_0

    .line 198
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    :goto_0
    return-object v2

    .line 177
    :pswitch_0
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_1_DAY:Ljava/lang/String;

    goto :goto_0

    .line 179
    :pswitch_1
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_3_DAYS:Ljava/lang/String;

    goto :goto_0

    .line 181
    :pswitch_2
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    goto :goto_0

    .line 183
    :pswitch_3
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    goto :goto_0

    .line 185
    :pswitch_4
    sget-object v2, Lcom/android/emailcommon/EasRefs;->FILTER_1_MONTH:Ljava/lang/String;

    goto :goto_0

    .line 187
    :pswitch_5
    const-string v2, "0"

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;
    .locals 2
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1440
    new-instance v0, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/NotesSyncAdapter;Z)V

    return-object v0
.end method

.method public isAccountPresent(J)Z
    .locals 9
    .param p1, "id"    # J

    .prologue
    const/4 v8, 0x1

    .line 415
    new-array v2, v8, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    .line 418
    .local v2, "projection":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 419
    .local v7, "flag":Z
    const/4 v6, 0x0

    .line 422
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Account;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 425
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, v8, :cond_0

    .line 426
    const/4 v7, 0x1

    .line 429
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_1
    return v7

    .line 429
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 430
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public isLooping()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mIsLooping:Z

    return v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 244
    const/4 v1, 0x0

    .line 246
    .local v1, "p":Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
    :try_start_0
    new-instance v2, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;

    invoke-direct {v2, p0, p1, p0}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;-><init>(Lcom/android/exchange/adapter/NotesSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/NotesSyncAdapter;)V
    :try_end_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    .end local v1    # "p":Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
    .local v2, "p":Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
    invoke-virtual {v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->parse()Z

    move-result v3

    .line 259
    .local v3, "res":Z
    invoke-virtual {v2}, Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;->isLooping()Z

    move-result v4

    iput-boolean v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mIsLooping:Z

    move-object v1, v2

    .line 260
    .end local v2    # "p":Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
    .end local v3    # "res":Z
    .restart local v1    # "p":Lcom/android/exchange/adapter/NotesSyncAdapter$EasNotesSyncParser;
    :goto_0
    return v3

    .line 247
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v4, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpl-double v4, v4, v6

    if-nez v4, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->setIntervalPing()V

    .line 251
    const/4 v3, 0x0

    goto :goto_0

    .line 253
    :cond_0
    throw v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 10
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v6

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 267
    const/4 v6, 0x0

    .line 410
    :goto_0
    return v6

    .line 269
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->queryDBForLocalChanges()Ljava/util/ArrayList;

    move-result-object v5

    .line 271
    .local v5, "noteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;>;"
    const/4 v4, 0x0

    .line 272
    .local v4, "noteIndex":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 274
    .local v1, "listSize":I
    if-lez v1, :cond_1

    .line 275
    const/16 v6, 0x16

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 278
    :cond_1
    :goto_1
    if-ge v4, v1, :cond_e

    .line 280
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;

    .line 282
    .local v3, "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    const/4 v2, 0x0

    .line 284
    .local v2, "multiCategories":[Ljava/lang/String;
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmCategories()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 285
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmCategories()Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 288
    :cond_2
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getSyncNeededFlag()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 403
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 404
    goto :goto_1

    .line 292
    :pswitch_0
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_3

    .line 293
    const-string v6, "Notes"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Send local NEW Note --  Account ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Client ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmClientID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_3
    const/4 v6, 0x7

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0xc

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmClientID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 298
    const/16 v6, 0x1d

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 302
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyType()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    .line 304
    const/16 v6, 0x44a

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0x446

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0x44b

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 311
    :cond_4
    if-eqz v2, :cond_6

    array-length v6, v2

    if-lez v6, :cond_6

    .line 312
    const/16 v6, 0x5c8

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 313
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    array-length v6, v2

    if-ge v0, v6, :cond_5

    .line 314
    const/16 v6, 0x5c9

    aget-object v7, v2, v0

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 313
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 316
    :cond_5
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 326
    .end local v0    # "i":I
    :cond_6
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmSubject()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 327
    const/16 v6, 0x5c5

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmSubject()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 331
    :cond_7
    const/16 v6, 0x5c6

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 333
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 335
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_2

    .line 341
    :pswitch_1
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_8

    .line 342
    const-string v6, "Notes"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "send local MODIFIED Note --  Account ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Server ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :cond_8
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0xd

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 347
    const/16 v6, 0x1d

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 350
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyType()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_9

    .line 351
    const/16 v6, 0x44a

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0x446

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0x44b

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getBodyData()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 363
    :cond_9
    if-eqz v2, :cond_b

    array-length v6, v2

    if-lez v6, :cond_b

    .line 364
    const/16 v6, 0x5c8

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 365
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    array-length v6, v2

    if-ge v0, v6, :cond_a

    .line 366
    const/16 v6, 0x5c9

    aget-object v7, v2, v0

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 368
    :cond_a
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 378
    .end local v0    # "i":I
    :cond_b
    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmSubject()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 379
    const/16 v6, 0x5c5

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmSubject()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 383
    :cond_c
    const/16 v6, 0x5c6

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 385
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 387
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_2

    .line 393
    :pswitch_2
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_d

    .line 394
    const-string v6, "Notes"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "send local DELETED Note --  Account ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Server ID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    :cond_d
    const/16 v6, 0x9

    invoke-virtual {p1, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    const/16 v7, 0xd

    invoke-virtual {v3}, Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;->getmServerIDNote()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_2

    .line 406
    .end local v2    # "multiCategories":[Ljava/lang/String;
    .end local v3    # "note":Lcom/android/exchange/adapter/NotesSyncAdapter$NoteContents;
    :cond_e
    if-lez v1, :cond_f

    .line 407
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 410
    :cond_f
    if-lez v1, :cond_10

    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 4
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    if-nez p3, :cond_0

    .line 210
    const/16 v0, 0x1e

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 211
    const/16 v0, 0x13

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 212
    const/16 v0, 0x15

    const-string v1, "5"

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 213
    const/16 v0, 0x17

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 214
    const/16 v0, 0x18

    invoke-virtual {p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;->getNoteFilter()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 216
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    .line 217
    const/16 v0, 0x445

    invoke-virtual {p2, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 219
    const/16 v0, 0x446

    const-string v1, "1"

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 220
    const/16 v0, 0x447

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailSize()B

    move-result v1

    invoke-static {v1}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Text()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 221
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 225
    :goto_0
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 227
    :cond_0
    return-void

    .line 223
    :cond_1
    const/16 v0, 0x19

    const-string v1, "8"

    invoke-virtual {p2, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_0
.end method

.method public wipe()V
    .locals 14

    .prologue
    .line 125
    const/4 v8, 0x0

    .line 126
    .local v8, "count":I
    const/4 v7, 0x0

    .line 127
    .local v7, "c_msg":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 128
    .local v9, "cv":Landroid/content/ContentValues;
    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 133
    .local v6, "argument":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/Notes$Message;->MESSAGE_PROJECTION:[Ljava/lang/String;

    const-string v3, "accountKey=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v12, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 138
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v10, 0x1

    .line 139
    .local v10, "nextMsg":Z
    :goto_0
    if-eqz v10, :cond_1

    .line 140
    sget-object v0, Lcom/android/emailcommon/provider/Notes$Body;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 141
    .local v11, "uri_body":Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v11, v1, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 143
    goto :goto_0

    .line 138
    .end local v10    # "nextMsg":Z
    .end local v11    # "uri_body":Landroid/net/Uri;
    :cond_0
    const/4 v10, 0x0

    goto :goto_0

    .line 145
    .restart local v10    # "nextMsg":Z
    :cond_1
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 146
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "accountKey=?"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 152
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "accountKey=?"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 154
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Notes$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const-string v2, "accountKey=?"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 157
    new-instance v9, Landroid/content/ContentValues;

    .end local v9    # "cv":Landroid/content/ContentValues;
    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 158
    .restart local v9    # "cv":Landroid/content/ContentValues;
    const-string v0, "syncKey"

    const-string v1, "0"

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 164
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_3

    .line 165
    const-string v0, "Notes"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Note\'s Wipe is recieved  for  Account ID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Email: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/NotesSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_3
    return-void

    .line 145
    .end local v10    # "nextMsg":Z
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 146
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method
