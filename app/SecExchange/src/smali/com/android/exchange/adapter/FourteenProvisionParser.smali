.class public Lcom/android/exchange/adapter/FourteenProvisionParser;
.super Lcom/android/exchange/adapter/AbstractUtiltyParser;
.source "FourteenProvisionParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractUtiltyParser;-><init>(Ljava/io/InputStream;)V

    .line 17
    return-void
.end method


# virtual methods
.method public checkForProvisioning()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FourteenProvisionParser;->parse()Z

    move-result v0

    return v0
.end method

.method public parse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 25
    const/4 v0, 0x0

    .line 26
    .local v0, "status":I
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/FourteenProvisionParser;->nextTag(I)I

    move-result v2

    const/16 v3, 0x1d6

    if-eq v2, v3, :cond_0

    .line 27
    new-instance v1, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v1

    .line 28
    :cond_0
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/FourteenProvisionParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 29
    iget v2, p0, Lcom/android/exchange/adapter/FourteenProvisionParser;->tag:I

    const/16 v3, 0x1cc

    if-ne v2, v3, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/android/exchange/adapter/FourteenProvisionParser;->getValueInt()I

    move-result v0

    .line 31
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/FourteenProvisionParser;->isProvisioningStatus(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 32
    const/4 v1, 0x1

    .line 48
    :cond_1
    return v1

    .line 35
    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/FourteenProvisionParser;->isDeviceAccessDenied(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 36
    const-string v1, "FourteenProvisionParser"

    const-string v2, "FourteenProvisionParser::parse() - Received status 129, to Block device "

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    new-instance v1, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v2, 0x40001

    const v3, 0x7f060015

    invoke-direct {v1, v2, v3}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v1

    .line 41
    :cond_3
    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/FourteenProvisionParser;->istomanypartnerships(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 42
    const-string v1, "FourteenProvisionParser"

    const-string v2, "FourteenProvisionParser::parse() - Received status 177, too may partnerships "

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    new-instance v1, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v1, v0}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v1
.end method
