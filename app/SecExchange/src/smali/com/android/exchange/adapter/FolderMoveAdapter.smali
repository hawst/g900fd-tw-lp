.class public Lcom/android/exchange/adapter/FolderMoveAdapter;
.super Lcom/android/exchange/adapter/AbstractCommandAdapter;
.source "FolderMoveAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;
    }
.end annotation


# instance fields
.field public final CODE_FOLDER_CREATE_ACCESS_DENIED:I

.field public final CODE_FOLDER_UPDATE_FOLDER_NOT_EXISTS:I

.field public final CODE_FOLDER_UPDATE_INVALID_SYNC_KEY:I

.field public final CODE_FOLDER_UPDATE_MALFORMED_REQUEST:I

.field public final CODE_FOLDER_UPDATE_NAME_EXISTS:I

.field public final CODE_FOLDER_UPDATE_PARENT_FOLDER_NOT_FOUND:I

.field public final CODE_FOLDER_UPDATE_SERVER_ERROR:I

.field public final CODE_FOLDER_UPDATE_SUCCESS:I

.field public final CODE_FOLDER_UPDATE_SYSTEM_FOLDER:I

.field public final CODE_FOLDER_UPDATE_TIME_OUT:I

.field public final CODE_FOLDER_UPDATE_UNKNOWN_ERROR:I

.field private TAG:Ljava/lang/String;

.field private isStatus:I

.field private mDeleteFlag:Z

.field private mDoNotRetry:Z

.field mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

.field private mToMailboxParentId:Ljava/lang/String;

.field private mToMailboxParentKey:J


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Z)V
    .locals 7
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;
    .param p2, "toMailboxParentId"    # Ljava/lang/String;
    .param p3, "deleteFlag"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 61
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 28
    const-string v0, "FolderMoveAdapter"

    iput-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    .line 31
    iput v5, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_SUCCESS:I

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_NAME_EXISTS:I

    .line 35
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_SYSTEM_FOLDER:I

    .line 37
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_FOLDER_NOT_EXISTS:I

    .line 39
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_PARENT_FOLDER_NOT_FOUND:I

    .line 41
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_SERVER_ERROR:I

    .line 43
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_CREATE_ACCESS_DENIED:I

    .line 45
    const/16 v0, 0x8

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_TIME_OUT:I

    .line 47
    const/16 v0, 0x9

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_INVALID_SYNC_KEY:I

    .line 49
    const/16 v0, 0xa

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_MALFORMED_REQUEST:I

    .line 51
    const/16 v0, 0xb

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->CODE_FOLDER_UPDATE_UNKNOWN_ERROR:I

    .line 53
    iput-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentKey:J

    .line 56
    iput-boolean v4, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDoNotRetry:Z

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    .line 58
    iput-boolean v4, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDeleteFlag:Z

    .line 62
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    const-string v1, "FolderUpdateAdapter.FolderUpdateAdapter"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iput-object p2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    .line 64
    iput-boolean p3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDeleteFlag:Z

    .line 65
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 66
    new-array v2, v5, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    .line 67
    .local v2, "proj":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "serverId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "accountKey"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 69
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 71
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 72
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentKey:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 83
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v3    # "where":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    :goto_0
    return-void

    .line 75
    .restart local v2    # "proj":[Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v0

    .line 78
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 79
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 78
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 79
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method static synthetic access$000(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderMoveAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/exchange/adapter/FolderMoveAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderMoveAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/FolderMoveAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderMoveAdapter;

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentKey:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/android/exchange/adapter/FolderMoveAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderMoveAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    return p1
.end method


# virtual methods
.method public callback(I)V
    .locals 4
    .param p1, "status"    # I

    .prologue
    .line 121
    :try_start_0
    iget-boolean v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDeleteFlag:Z

    if-eqz v0, :cond_0

    .line 122
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V

    .line 124
    iput p1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    .line 130
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDoNotRetry:Z

    .line 133
    :goto_1
    return-void

    .line 126
    :cond_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V

    .line 128
    iput p1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    const-string v1, "FolderMoveAdapter.getCommandName: FolderUpdate"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "FolderUpdate"

    return-object v0
.end method

.method public getSatus()I
    .locals 1

    .prologue
    .line 323
    iget v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    return v0
.end method

.method public hasChangedItems()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 95
    iget-boolean v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mDoNotRetry:Z

    if-ne v2, v1, :cond_0

    .line 116
    :goto_0
    return v0

    .line 97
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 98
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 101
    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    goto :goto_0

    .line 104
    :cond_1
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    goto :goto_0

    .line 109
    :cond_2
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 113
    iput v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->isStatus:I

    goto :goto_0

    :cond_3
    move v0, v1

    .line 116
    goto :goto_0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 156
    new-instance v0, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;-><init>(Lcom/android/exchange/adapter/FolderMoveAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 157
    .local v0, "parser":Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/FolderMoveAdapter$FolderMoveParser;->parse()Z

    move-result v1

    return v1
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 4
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mToMailboxParentId:Ljava/lang/String;

    .line 142
    .local v0, "parentId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 143
    const-string v0, "0"

    .line 144
    :cond_0
    const/16 v1, 0x1d5

    invoke-virtual {p1, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x1d2

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x1c8

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x1c9

    invoke-virtual {v1, v2, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x1c7

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 149
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    const-string v2, "FolderMoveAdapter.FolderMoveAdapter.sendLocalChanges. "

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const/4 v1, 0x1

    return v1
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 173
    return-void
.end method

.method public wipe()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 176
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDstServerId:Ljava/lang/String;

    .line 178
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mNewDisplayName:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->toContentValues()Landroid/content/ContentValues;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderMoveAdapter;->TAG:Ljava/lang/String;

    const-string v1, "FolderMoveAdapter.FolderMoveParser.wipe finished"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method
