.class public Lcom/android/exchange/adapter/OoOCommandParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "OoOCommandParser.java"


# instance fields
.field private mEndDate:Ljava/util/Date;

.field private mExtKnownMsgEnable:I

.field private mExtMsgKnown:Ljava/lang/String;

.field private mExtMsgUnKnown:Ljava/lang/String;

.field private mExtUnKnownMsgEnable:I

.field private mInternalMsg:Ljava/lang/String;

.field private mInternalMsgEnable:I

.field private mOofState:I

.field private mResult:Z

.field private mStartDate:Ljava/util/Date;

.field private mStatus:I

.field oodl:Lcom/android/emailcommon/service/OoODataList;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    .line 46
    return-void
.end method

.method private checkForBadCharacters(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 52
    .local v0, "arr":[C
    const/4 v1, 0x0

    .line 54
    .local v1, "badCharsFound":Z
    array-length v4, v0

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 55
    aget-char v4, v0, v2

    const/16 v5, 0xd

    if-eq v4, v5, :cond_0

    aget-char v4, v0, v2

    const/16 v5, 0xa

    if-ne v4, v5, :cond_1

    .line 56
    :cond_0
    const/16 v4, 0x20

    aput-char v4, v0, v2

    .line 57
    const/4 v1, 0x1

    .line 54
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 61
    :cond_1
    if-eqz v1, :cond_2

    .line 62
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    .line 63
    .local v3, "result":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 66
    .end local v3    # "result":Ljava/lang/String;
    :goto_1
    return-object v3

    :cond_2
    move-object v3, p1

    .line 65
    goto :goto_1
.end method

.method private convertUTCToLocal(Ljava/lang/String;)Ljava/util/Date;
    .locals 8
    .param p1, "utc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 80
    const-wide/16 v4, 0x0

    .line 81
    .local v4, "offsetCal":J
    const-string v3, "T"

    const-string v6, " "

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 82
    const-string v3, ".000Z"

    const-string v6, ""

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 84
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd hh:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 85
    .local v2, "df":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    .line 91
    invoke-virtual {v2}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 92
    .local v0, "cal":Ljava/util/Calendar;
    const/16 v3, 0xf

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v6, 0x10

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/2addr v3, v6

    int-to-long v4, v3

    .line 93
    const/16 v3, 0x9

    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/OoOCommandParser;->getUTCAMPM(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v3, v6}, Ljava/util/Calendar;->set(II)V

    .line 95
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 96
    .local v1, "convertedDate":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    add-long/2addr v6, v4

    invoke-virtual {v1, v6, v7}, Ljava/util/Date;->setTime(J)V

    .line 97
    return-object v1
.end method

.method private getUTCAMPM(Ljava/lang/String;)I
    .locals 8
    .param p1, "utc"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 70
    const-string v6, " "

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 71
    .local v0, "dateTime":[Ljava/lang/String;
    aget-object v6, v0, v5

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    .line 72
    .local v3, "utcTime":[B
    aget-byte v6, v3, v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v2, v6, -0x30

    .line 73
    .local v2, "hHH":I
    aget-byte v6, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v1, v6, -0x30

    .line 74
    .local v1, "hH":I
    mul-int/lit8 v6, v2, 0xa

    add-int/2addr v6, v1

    const/16 v7, 0xc

    if-ge v6, v7, :cond_0

    .line 76
    :goto_0
    return v4

    :cond_0
    move v4, v5

    goto :goto_0
.end method

.method private parseGetTag()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 155
    :cond_0
    :goto_0
    const/16 v0, 0x487

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_6

    .line 156
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x48a

    if-ne v0, v1, :cond_1

    .line 157
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValueInt()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    .line 158
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    if-nez v0, :cond_1

    .line 159
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    .line 162
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x48b

    if-ne v0, v1, :cond_2

    .line 163
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    if-ne v0, v2, :cond_4

    .line 165
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->convertUTCToLocal(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    .line 169
    :cond_2
    :goto_1
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x48c

    if-ne v0, v1, :cond_3

    .line 170
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    if-ne v0, v2, :cond_5

    .line 172
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->convertUTCToLocal(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    .line 176
    :cond_3
    :goto_2
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x48d

    if-ne v0, v1, :cond_0

    .line 177
    invoke-direct {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->parseOoOMessageTag()V

    goto :goto_0

    .line 167
    :cond_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    goto :goto_1

    .line 174
    :cond_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    goto :goto_2

    .line 180
    :cond_6
    return-void
.end method

.method private parseOoOMessageTag()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 101
    const/4 v3, 0x0

    .line 102
    .local v3, "internal":Z
    const/4 v1, 0x0

    .line 103
    .local v1, "extKnown":Z
    const/4 v2, 0x0

    .line 104
    .local v2, "extUnKnown":Z
    const/4 v0, 0x0

    .line 106
    :cond_0
    :goto_0
    const/16 v5, 0x48d

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/OoOCommandParser;->nextTag(I)I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_b

    .line 108
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x48e

    if-ne v5, v6, :cond_1

    .line 109
    const/4 v3, 0x1

    goto :goto_0

    .line 110
    :cond_1
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x48f

    if-ne v5, v6, :cond_2

    .line 111
    const/4 v1, 0x1

    goto :goto_0

    .line 112
    :cond_2
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x490

    if-ne v5, v6, :cond_3

    .line 113
    const/4 v2, 0x1

    goto :goto_0

    .line 114
    :cond_3
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x491

    if-ne v5, v6, :cond_4

    .line 115
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValueInt()I

    move-result v4

    .line 116
    .local v4, "value":I
    if-ne v4, v7, :cond_0

    .line 117
    const/4 v0, 0x1

    .local v0, "enabled":Z
    goto :goto_0

    .line 118
    .end local v0    # "enabled":Z
    .end local v4    # "value":I
    :cond_4
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x492

    if-ne v5, v6, :cond_a

    .line 119
    if-eqz v3, :cond_6

    .line 120
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    .line 121
    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/exchange/adapter/OoOCommandParser;->checkForBadCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    .line 122
    if-eqz v0, :cond_5

    .line 123
    iput v7, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    .line 126
    :goto_1
    const/4 v3, 0x0

    move v0, v3

    .local v0, "enabled":I
    goto :goto_0

    .line 125
    .end local v0    # "enabled":I
    :cond_5
    iput v8, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    goto :goto_1

    .line 127
    :cond_6
    if-eqz v1, :cond_8

    .line 128
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    .line 129
    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/exchange/adapter/OoOCommandParser;->checkForBadCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    .line 130
    if-eqz v0, :cond_7

    .line 131
    iput v7, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    .line 134
    :goto_2
    const/4 v1, 0x0

    move v0, v1

    .restart local v0    # "enabled":I
    goto :goto_0

    .line 133
    .end local v0    # "enabled":I
    :cond_7
    iput v8, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    goto :goto_2

    .line 136
    :cond_8
    if-eqz v2, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    .line 138
    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/android/exchange/adapter/OoOCommandParser;->checkForBadCharacters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    .line 139
    if-eqz v0, :cond_9

    .line 140
    iput v7, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    .line 143
    :goto_3
    const/4 v2, 0x0

    move v0, v2

    .restart local v0    # "enabled":I
    goto :goto_0

    .line 142
    .end local v0    # "enabled":I
    :cond_9
    iput v8, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    goto :goto_3

    .line 147
    :cond_a
    iget v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v6, 0x493

    if-ne v5, v6, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValue()Ljava/lang/String;

    goto/16 :goto_0

    .line 151
    :cond_b
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    return-void
.end method

.method public commit()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    return-void
.end method

.method public getParsedData()Lcom/android/emailcommon/service/OoODataList;
    .locals 1

    .prologue
    .line 316
    iget-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    .line 318
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStatus:I

    return v0
.end method

.method public parse()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    const/4 v9, 0x0

    .line 185
    .local v9, "settingsStatus":Z
    const/4 v8, 0x0

    .line 186
    .local v8, "oofStatus":Z
    new-instance v0, Lcom/android/emailcommon/service/OoODataList;

    invoke-direct {v0}, Lcom/android/emailcommon/service/OoODataList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    .line 188
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->nextTag(I)I

    move-result v0

    const/16 v1, 0x485

    if-eq v0, v1, :cond_0

    .line 189
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 190
    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_10

    .line 192
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x486

    if-ne v0, v1, :cond_1

    .line 193
    invoke-virtual {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->getValueInt()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStatus:I

    .line 194
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStatus:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 195
    if-nez v9, :cond_2

    if-nez v8, :cond_2

    .line 196
    const/4 v9, 0x1

    .line 210
    :cond_1
    :goto_1
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x496

    if-ne v0, v1, :cond_4

    .line 212
    const-string v0, "OoOCommandParser"

    const-string v1, "Hurray! DeviceInformation was set successfully"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_2
    if-eqz v9, :cond_1

    if-nez v8, :cond_1

    .line 198
    const/4 v8, 0x1

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    goto :goto_1

    .line 203
    :cond_3
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStatus:I

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/OoOCommandParser;->isProvisioningStatus(I)Z

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    goto :goto_1

    .line 216
    :cond_4
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->tag:I

    const/16 v1, 0x487

    if-ne v0, v1, :cond_0

    .line 218
    :try_start_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/OoOCommandParser;->parseGetTag()V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    if-nez v0, :cond_8

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    .line 225
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 226
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    .line 231
    :goto_2
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 232
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    .line 237
    :goto_3
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 238
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto :goto_0

    .line 219
    :catch_0
    move-exception v7

    .line 220
    .local v7, "e":Ljava/text/ParseException;
    new-instance v0, Ljava/io/IOException;

    const-string v1, "date_parse"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    .end local v7    # "e":Ljava/text/ParseException;
    :cond_5
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto :goto_2

    .line 235
    :cond_6
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto :goto_3

    .line 241
    :cond_7
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto/16 :goto_0

    .line 244
    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    .line 245
    iget v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mOofState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_c

    .line 247
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 248
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    .line 255
    :goto_4
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 256
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    .line 263
    :goto_5
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 264
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    goto/16 :goto_0

    .line 252
    :cond_9
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    goto :goto_4

    .line 260
    :cond_a
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    goto :goto_5

    .line 268
    :cond_b
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x2

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mStartDate:Ljava/util/Date;

    iget-object v6, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mEndDate:Ljava/util/Date;

    invoke-virtual/range {v0 .. v6}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;Ljava/util/Date;Ljava/util/Date;)I

    goto/16 :goto_0

    .line 272
    :cond_c
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 273
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsg:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    .line 278
    :goto_6
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 279
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgKnown:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    .line 284
    :goto_7
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 285
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    iget-object v4, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtMsgUnKnown:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto/16 :goto_0

    .line 276
    :cond_d
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x4

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mInternalMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto :goto_6

    .line 282
    :cond_e
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x5

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtKnownMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto :goto_7

    .line 288
    :cond_f
    iget-object v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->oodl:Lcom/android/emailcommon/service/OoODataList;

    const/4 v1, 0x6

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mExtUnKnownMsgEnable:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/service/OoODataList;->AddOoOData(IIILjava/lang/String;)I

    goto/16 :goto_0

    .line 294
    :cond_10
    if-eqz v9, :cond_11

    if-eqz v8, :cond_11

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Lcom/android/exchange/adapter/OoOCommandParser;->mResult:Z

    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 313
    return-void
.end method
