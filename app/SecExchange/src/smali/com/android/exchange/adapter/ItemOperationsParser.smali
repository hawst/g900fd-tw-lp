.class public Lcom/android/exchange/adapter/ItemOperationsParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "ItemOperationsParser.java"


# instance fields
.field private isDoc:Z

.field private isSigned:Z

.field private mBindArguments:[Ljava/lang/String;

.field private mIrmLicensePresent:Z

.field private mMailboxIdAsString:Ljava/lang/String;

.field private mObserver:Ljava/util/Observer;

.field private mOs:Ljava/io/OutputStream;

.field private mResult:Z

.field private mServerId:Ljava/lang/String;

.field private mStatus:I

.field public outStream:Ljava/io/InputStream;

.field private tempMimeFile:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 48
    iput-boolean v1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    .line 50
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mBindArguments:[Ljava/lang/String;

    .line 54
    iput-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    .line 56
    iput v1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    .line 58
    iput-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mOs:Ljava/io/OutputStream;

    .line 60
    iput-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    .line 66
    iput-boolean v1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    .line 69
    iput-boolean v1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mIrmLicensePresent:Z

    .line 78
    iput-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mMailboxIdAsString:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->outStream:Ljava/io/InputStream;

    .line 85
    return-void
.end method

.method private deleteTempFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 737
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 738
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 743
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 739
    :catch_0
    move-exception v0

    .line 741
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ItemOperationsParser"

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private fetchDocPropertiesParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
    .locals 9
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 821
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_0

    .line 822
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "ItemOperationsPaser::fetchDocPropertiesParser() start"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    :cond_0
    const-wide/16 v4, 0x0

    .line 827
    .local v4, "total":J
    const/4 v2, 0x0

    .line 828
    .local v2, "range":Ljava/lang/String;
    const/4 v3, 0x0

    .line 829
    .local v3, "version":Ljava/lang/String;
    const/4 v1, 0x0

    .line 830
    .local v1, "docDataBase64":Ljava/lang/String;
    const/4 v0, 0x0

    .line 832
    .local v0, "docData":[B
    :cond_1
    :goto_0
    const/16 v6, 0x50b

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_3

    .line 833
    iget v6, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    packed-switch v6, :pswitch_data_0

    .line 862
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 835
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueInt()I

    move-result v6

    int-to-long v4, v6

    .line 836
    const-string v6, "ItemOperationsParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ITEMOPERATIONS_TOTAL : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 839
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 840
    const-string v6, "ItemOperationsParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ITEMOPERATIONS_RANGE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 843
    :pswitch_3
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_2

    .line 844
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "ItemOperationsPaser::fetchDocPropertiesParser() - Base64.decode() start"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 848
    :cond_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 849
    invoke-static {v1}, Lcom/android/sec/org/bouncycastle/util/encoders/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 850
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v6, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->outStream:Ljava/io/InputStream;

    .line 852
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_1

    .line 853
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "ItemOperationsPaser::fetchDocPropertiesParser() - Base64.decode() end"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 858
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 859
    const-string v6, "ItemOperationsParser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ITEMOPERATIONS_VERSION : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 867
    :cond_3
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_4

    .line 868
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "ItemOperationsPaser::fetchDocPropertiesParser() end"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    :cond_4
    return-void

    .line 833
    :pswitch_data_0
    .packed-switch 0x509
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private fetchPropertiesBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
    .locals 27
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 509
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_0

    .line 510
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_0
    const/16 v18, 0x0

    .line 515
    .local v18, "msgData":Ljava/lang/String;
    const/4 v7, 0x0

    .line 517
    .local v7, "bodyType":Ljava/lang/String;
    :cond_1
    :goto_0
    const/16 v24, 0x44a

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v24

    const/16 v25, 0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_23

    .line 518
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    move/from16 v24, v0

    sparse-switch v24, :sswitch_data_0

    .line 716
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 520
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 521
    goto :goto_0

    .line 526
    :sswitch_1
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v24, v0

    check-cast v24, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    move-object/from16 v0, v24

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsAdapter;->isMIMEDataRequested:Z

    move/from16 v24, v0

    if-eqz v24, :cond_2

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    move/from16 v24, v0

    if-nez v24, :cond_3

    move-object/from16 v0, p1

    iget-boolean v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    move/from16 v24, v0

    if-nez v24, :cond_3

    .line 529
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v18

    .line 532
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/android/exchange/utility/MessageBodyRefresher;

    move/from16 v24, v0

    if-eqz v24, :cond_1

    .line 533
    const-string v24, "ItemOperationsParser"

    const-string v25, "svc(Adapter.mService) is instanceof MessageBodyRefresher"

    invoke-static/range {v24 .. v25}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    check-cast v20, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 535
    .local v20, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/utility/MessageBodyRefresher;->getIsSaveMode()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 536
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/utility/MessageBodyRefresher;->saveAsText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 542
    .end local v20    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_3
    const-string v24, "ItemOperationsParser"

    const-string v25, "inside Tags.BASE_DATA"

    invoke-static/range {v24 .. v25}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getUniqueTempFileName()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    .line 545
    const/16 v24, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z

    .line 547
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_4

    .line 548
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - new MimeMessage() start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    :cond_4
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getTempMIMEDataPath()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 553
    .local v11, "is":Ljava/io/InputStream;
    const/16 v16, 0x0

    .line 556
    .local v16, "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    :try_start_0
    new-instance v17, Lcom/android/emailcommon/internet/MimeMessage;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v16    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .local v17, "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    move-object/from16 v16, v17

    .line 561
    .end local v17    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v16    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    :goto_1
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 563
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_5

    .line 564
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - new MimeMessage() end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    :cond_5
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v21

    .line 570
    .local v21, "tempBody2":Lcom/android/emailcommon/mail/Body;
    new-instance v14, Lcom/android/emailcommon/internet/MimeBodyPart;

    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-direct {v14, v0, v1}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>(Lcom/android/emailcommon/mail/Body;Ljava/lang/String;)V

    .line 573
    .local v14, "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    const/4 v15, 0x0

    .line 574
    .local v15, "mimeBodyStringText":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 576
    .local v8, "contentType":Ljava/lang/String;
    const-string v24, "ItemOperationsParser"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "ContentType = "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v23, "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 580
    .local v5, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    move-object/from16 v0, v23

    invoke-static {v14, v0, v5}, Lcom/android/emailcommon/internet/MimeUtility;->collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 582
    const-string v24, "text/html"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 583
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_6

    .line 584
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [text/html] start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_6
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/emailcommon/mail/Part;

    .line 589
    .local v22, "viewable":Lcom/android/emailcommon/mail/Part;
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v15

    .line 591
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 592
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_8

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 593
    :cond_8
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 706
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v8    # "contentType":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v15    # "mimeBodyStringText":Ljava/lang/String;
    .end local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    .end local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catch_0
    move-exception v19

    .line 707
    .local v19, "npe":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 711
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    move/from16 v24, v0

    if-nez v24, :cond_1

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->deleteTempFile(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 557
    .end local v19    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v9

    .line 558
    .local v9, "e":Lcom/android/emailcommon/mail/MessagingException;
    invoke-virtual {v9}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V

    goto/16 :goto_1

    .line 596
    .end local v9    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .restart local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .restart local v8    # "contentType":Ljava/lang/String;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v15    # "mimeBodyStringText":Ljava/lang/String;
    .restart local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .restart local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    .restart local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_9
    :try_start_3
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_a

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 597
    :cond_a
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 708
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v8    # "contentType":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v15    # "mimeBodyStringText":Ljava/lang/String;
    .end local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    .end local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catch_2
    move-exception v9

    .line 709
    .restart local v9    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_4
    invoke-virtual {v9}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 711
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    move/from16 v24, v0

    if-nez v24, :cond_1

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->deleteTempFile(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 602
    .end local v9    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .restart local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .restart local v8    # "contentType":Ljava/lang/String;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v15    # "mimeBodyStringText":Ljava/lang/String;
    .restart local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .restart local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_b
    :try_start_5
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_c

    .line 603
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [text/html] end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_c
    :goto_3
    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getMessageDispostion()Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_d

    .line 699
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v24, v0

    move/from16 v0, v24

    or-int/lit16 v0, v0, 0x800

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 701
    :cond_d
    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/internet/MimeMessage;->getReturnPath()Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_e

    .line 702
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v24, v0

    move/from16 v0, v24

    or-int/lit16 v0, v0, 0x1000

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 711
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    move/from16 v24, v0

    if-nez v24, :cond_1

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->deleteTempFile(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 606
    :cond_f
    :try_start_6
    const-string v24, "multipart/"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1f

    .line 607
    const-string v24, "multipart/alternative"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_10

    const-string v24, "multipart/mixed"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_10

    const-string v24, "multipart/report"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_10

    const-string v24, "multipart/related"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_18

    .line 611
    :cond_10
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_11

    .line 612
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [multipart/alternative...] start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_11
    const/4 v12, 0x0

    .line 617
    .local v12, "isReadHtml":Z
    const/4 v13, 0x0

    .line 619
    .local v13, "isReadText":Z
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 620
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 622
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_12
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_16

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/emailcommon/mail/Part;

    .line 623
    .restart local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v15

    .line 625
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 626
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    :cond_13
    if-nez v12, :cond_12

    .line 628
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 629
    const/4 v12, 0x1

    goto :goto_4

    .line 631
    :cond_14
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_15

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_12

    :cond_15
    if-nez v13, :cond_12

    .line 633
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 634
    const/4 v13, 0x1

    goto :goto_4

    .line 637
    .end local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    :cond_16
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_c

    .line 638
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [multipart/alternative...] end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_3

    .line 711
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v8    # "contentType":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v12    # "isReadHtml":Z
    .end local v13    # "isReadText":Z
    .end local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v15    # "mimeBodyStringText":Ljava/lang/String;
    .end local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catchall_0
    move-exception v24

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    move/from16 v25, v0

    if-nez v25, :cond_17

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->deleteTempFile(Ljava/lang/String;)V

    :cond_17
    throw v24

    .line 641
    .restart local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .restart local v8    # "contentType":Ljava/lang/String;
    .restart local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v15    # "mimeBodyStringText":Ljava/lang/String;
    .restart local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .restart local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_18
    :try_start_7
    const-string v24, "multipart/signed"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 642
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_19

    .line 643
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [multipart/signed] start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    :cond_19
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_1a
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_1e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/android/emailcommon/mail/Part;

    .line 648
    .restart local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v15

    .line 650
    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v24

    if-eqz v24, :cond_1c

    .line 651
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1b

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1a

    .line 652
    :cond_1b
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    goto :goto_5

    .line 653
    :cond_1c
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1d

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1a

    .line 654
    :cond_1d
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    goto :goto_5

    .line 659
    .end local v22    # "viewable":Lcom/android/emailcommon/mail/Part;
    :cond_1e
    new-instance v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v4}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 661
    .local v4, "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const-string v24, "SMIME Signed Message.p7s"

    move-object/from16 v0, v24

    iput-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 662
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 663
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    .line 665
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 667
    .local v6, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    move-object/from16 v0, p1

    iput-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 669
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 671
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    .line 673
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_c

    .line 674
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [multipart/signed] end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 679
    .end local v4    # "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v6    # "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_1f
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_20

    .line 680
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [] start"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    :cond_20
    invoke-static {v14}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v15

    .line 686
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_21

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v24, v0

    const-string v25, ""

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_22

    .line 687
    :cond_21
    move-object/from16 v0, p2

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 688
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 691
    :cond_22
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_c

    .line 692
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() - [] end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 721
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v8    # "contentType":Ljava/lang/String;
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v14    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v15    # "mimeBodyStringText":Ljava/lang/String;
    .end local v16    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .end local v21    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v23    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_23
    if-eqz v7, :cond_24

    .line 722
    const-string v24, "1"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_26

    .line 723
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 729
    :cond_24
    :goto_6
    sget-boolean v24, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v24, :cond_25

    .line 730
    const-string v24, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v25, "ItemOperationsPaser::fetchPropertiesBodyParser() end"

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_25
    return-void

    .line 724
    :cond_26
    const-string v24, "2"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_24

    .line 725
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    goto :goto_6

    .line 518
    :sswitch_data_0
    .sparse-switch
        0x446 -> :sswitch_0
        0x44b -> :sswitch_1
    .end sparse-switch
.end method

.method private fetchPropertiesParser()V
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    sget-boolean v20, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v20, :cond_0

    .line 289
    const-string v20, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v21, "ItemOperationsPaser::fetchPropertiesParser() start"

    invoke-static/range {v20 .. v21}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    :cond_0
    const/4 v11, 0x0

    .line 294
    .local v11, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v7, 0x0

    .line 295
    .local v7, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    const/4 v15, 0x0

    .line 296
    .local v15, "range":Ljava/lang/String;
    const/16 v18, 0x0

    .line 298
    .local v18, "total":Ljava/lang/String;
    :cond_1
    :goto_0
    const/16 v20, 0x50b

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v20

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_a

    .line 299
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    move/from16 v20, v0

    sparse-switch v20, :sswitch_data_0

    .line 423
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 301
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v15

    .line 302
    const-string v20, "ItemOperationsParser"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "ITEMOPERATIONS_RANGE : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :sswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v18

    .line 307
    const-string v20, "ItemOperationsParser"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "ITEMOPERATIONS_TOTAL : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :sswitch_2
    const/16 v16, 0x0

    .line 313
    .local v16, "sizeConflict":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/android/exchange/EasDownLoadAttachmentSvc;

    move/from16 v20, v0

    if-eqz v20, :cond_4

    .line 314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    move-object/from16 v17, v0

    check-cast v17, Lcom/android/exchange/EasDownLoadAttachmentSvc;

    .line 316
    .local v17, "svc":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    if-nez v15, :cond_2

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mOs:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->resetPartFileOutputStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->mOs:Ljava/io/OutputStream;

    .line 322
    :cond_2
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getAttachment()Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v6

    .line 323
    .local v6, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v18, :cond_4

    iget-wide v0, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    cmp-long v20, v20, v22

    if-gez v20, :cond_4

    .line 324
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->updateAttachmentActualSize(Ljava/lang/Object;)Z

    move-result v9

    .line 325
    .local v9, "isUpdated":Z
    if-eqz v9, :cond_4

    .line 326
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->updateAttachmentInstance()V

    .line 327
    const/16 v20, 0x14

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    .line 328
    const/16 v16, 0x1

    .line 488
    .end local v6    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v9    # "isUpdated":Z
    .end local v16    # "sizeConflict":Z
    .end local v17    # "svc":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    :cond_3
    :goto_1
    return-void

    .line 335
    .restart local v16    # "sizeConflict":Z
    :cond_4
    if-nez v16, :cond_1

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mOs:Ljava/io/OutputStream;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueAsStream(Ljava/io/OutputStream;Ljava/util/Observer;)I

    move-result v10

    .line 337
    .local v10, "length":I
    const-string v20, "ItemOperationsParser"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "ITEMOPERATIONS_DATA : len = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 342
    .end local v10    # "length":I
    .end local v16    # "sizeConflict":Z
    :sswitch_3
    const/4 v14, 0x0

    .line 344
    .local v14, "parsed":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v17, v0

    .line 349
    .local v17, "svc":Lcom/android/exchange/EasSyncService;
    move-object/from16 v0, v17

    instance-of v0, v0, Lcom/android/exchange/EasLoadMoreSvc;

    move/from16 v20, v0

    if-eqz v20, :cond_11

    .line 350
    const-string v20, "ItemOperationsParser"

    const-string v21, "svc(Adapter.mService) is instanceof EasLoadMoreSvc"

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v20, "ItemOperationsParser"

    const-string v21, "get message from service"

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    check-cast v17, Lcom/android/exchange/EasLoadMoreSvc;

    .end local v17    # "svc":Lcom/android/exchange/EasSyncService;
    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    move-object v12, v11

    .line 355
    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .local v12, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_2
    if-nez v12, :cond_10

    .line 356
    const-string v20, "ItemOperationsParser"

    const-string v21, "get message from restore cursor func()"

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    move-object/from16 v20, v0

    sget-object v21, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/ItemOperationsParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 359
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_10

    .line 361
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v20

    if-eqz v20, :cond_f

    .line 362
    new-instance v11, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v11}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    .end local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_start_1
    invoke-virtual {v11, v8}, Lcom/android/emailcommon/provider/EmailContent$Message;->restore(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 366
    :goto_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 372
    .end local v8    # "c":Landroid/database/Cursor;
    :goto_4
    if-eqz v11, :cond_5

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    iget-wide v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v7

    .line 374
    if-eqz v7, :cond_5

    .line 375
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v7}, Lcom/android/exchange/adapter/ItemOperationsParser;->fetchPropertiesBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    .line 376
    const/4 v14, 0x1

    .line 379
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    .line 380
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/emailcommon/mail/Snippet;->fromHtmlText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    .line 388
    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/android/exchange/utility/MessageBodyRefresher;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 389
    if-eqz v14, :cond_1

    if-eqz v7, :cond_1

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v17, v0

    check-cast v17, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 391
    .local v17, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/android/exchange/utility/MessageBodyRefresher;->updateMessageBody(Lcom/android/emailcommon/provider/EmailContent$Body;)V

    goto/16 :goto_0

    .line 366
    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v17    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catchall_0
    move-exception v20

    move-object v11, v12

    .end local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v20

    .line 381
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_6
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 382
    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/android/emailcommon/mail/Snippet;->fromPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    goto :goto_5

    .line 393
    :cond_7
    if-eqz v14, :cond_8

    .line 395
    if-eqz v11, :cond_1

    if-eqz v7, :cond_1

    .line 396
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v11, v7}, Lcom/android/exchange/adapter/LoadMoreUtility;->updateEmail(Lcom/android/exchange/AbstractSyncService;Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    .line 397
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->isSigned:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v11, v1}, Lcom/android/exchange/adapter/LoadMoreUtility;->updateSignedMIMEDataAttachment(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 402
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto/16 :goto_0

    .line 408
    .end local v14    # "parsed":Z
    :sswitch_4
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->isDoc:Z

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    goto/16 :goto_0

    .line 414
    :sswitch_5
    if-eqz v11, :cond_9

    .line 415
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/exchange/adapter/ItemOperationsParser;->mIrmLicensePresent:Z

    .line 416
    move-object/from16 v0, p0

    invoke-static {v11, v0}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->parseLicense(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/exchange/adapter/AbstractSyncParser;)V

    goto/16 :goto_0

    .line 418
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto/16 :goto_0

    .line 428
    :cond_a
    sget-boolean v20, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v20, :cond_b

    .line 429
    const-string v20, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v21, "ItemOperationsPaser::fetchPropertiesParser() - nextTag(Tags.ITEMOPERATIONS_PROPERTIES) end"

    invoke-static/range {v20 .. v21}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :cond_b
    if-eqz v11, :cond_c

    .line 435
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mIrmLicensePresent:Z

    move/from16 v20, v0

    if-eqz v20, :cond_d

    .line 436
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMRemovalFlag:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 437
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 438
    .local v19, "values":Landroid/content/ContentValues;
    const-string v20, "IRMContentExpiryDate"

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v20, "IRMContentOwner"

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentOwner:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v20, "IRMLicenseFlag"

    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 441
    const-string v20, "IRMOwner"

    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMOwner:I

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 442
    const-string v20, "IRMTemplateId"

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateId:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v20, "IRMTemplateName"

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v20, "IRMTemplateDescription"

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateDescription:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v20, "IRMRemovalFlag"

    const/16 v21, 0x2

    invoke-static/range {v21 .. v21}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    sget-object v21, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "_id = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-wide v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 484
    .end local v19    # "values":Landroid/content/ContentValues;
    :cond_c
    :goto_7
    sget-boolean v20, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v20, :cond_3

    .line 485
    const-string v20, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v21, "ItemOperationsPaser::fetchPropertiesParser() end"

    invoke-static/range {v20 .. v21}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 451
    :cond_d
    const/4 v13, 0x0

    .line 453
    .local v13, "nullStr":Ljava/lang/String;
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 454
    .restart local v19    # "values":Landroid/content/ContentValues;
    const-string v20, "IRMContentExpiryDate"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v20, "IRMContentOwner"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v20, "IRMLicenseFlag"

    const/16 v21, -0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 461
    const-string v20, "IRMOwner"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 462
    const-string v20, "IRMTemplateId"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v20, "IRMTemplateName"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string v20, "IRMTemplateDescription"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_e

    .line 470
    const-string v20, "IRMRemovalFlag"

    const/16 v21, 0x1

    invoke-static/range {v21 .. v21}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    .line 478
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    sget-object v21, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "_id = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    iget-wide v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v19

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_7

    .line 475
    :cond_e
    const-string v20, "IRMRemovalFlag"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    goto :goto_8

    .line 366
    .end local v13    # "nullStr":Ljava/lang/String;
    .end local v19    # "values":Landroid/content/ContentValues;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v14    # "parsed":Z
    :catchall_1
    move-exception v20

    goto/16 :goto_6

    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_f
    move-object v11, v12

    .end local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_3

    .end local v8    # "c":Landroid/database/Cursor;
    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_10
    move-object v11, v12

    .end local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_4

    .local v17, "svc":Lcom/android/exchange/EasSyncService;
    :cond_11
    move-object v12, v11

    .end local v11    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v12    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_2

    .line 299
    nop

    :sswitch_data_0
    .sparse-switch
        0x44a -> :sswitch_3
        0x4c5 -> :sswitch_4
        0x509 -> :sswitch_0
        0x50a -> :sswitch_1
        0x50c -> :sswitch_2
        0x608 -> :sswitch_5
    .end sparse-switch
.end method

.method private getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "serverId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 89
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mMailboxIdAsString:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "syncServerId=? and mailboxKey=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getTempMIMEDataPath()Ljava/lang/String;
    .locals 6

    .prologue
    .line 491
    const/4 v1, 0x0

    .line 492
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 493
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tempMimeFile:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 496
    :cond_0
    iget-object v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v3, v3, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    instance-of v3, v3, Lcom/android/exchange/utility/MessageBodyRefresher;

    if-eqz v3, :cond_2

    .line 497
    const-string v3, "ItemOperationsParser"

    const-string v4, "svc(Adapter.mService) is instanceof MessageBodyRefresher"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    iget-object v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v2, v3, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    check-cast v2, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 499
    .local v2, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    invoke-virtual {v2}, Lcom/android/exchange/utility/MessageBodyRefresher;->getIsSaveMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 500
    invoke-virtual {v2}, Lcom/android/exchange/utility/MessageBodyRefresher;->getSaveFilePath()Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "_path":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 502
    :cond_1
    :goto_0
    const-string v3, "ItemOperationsParser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "svc(Adapter.mService) will save data at : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    .end local v0    # "_path":Ljava/lang/String;
    .end local v2    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_2
    return-object v1

    .restart local v0    # "_path":Ljava/lang/String;
    .restart local v2    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_3
    move-object v1, v0

    .line 501
    goto :goto_0
.end method

.method private itemOperationsEmptyFolderContentsParser()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 755
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v3, :cond_0

    .line 756
    const-string v3, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v4, "ItemOperationsPaser::itemOperationsEmptyFolderContentsParser() start"

    invoke-static {v3, v4}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    :cond_0
    const/4 v0, 0x0

    .line 761
    .local v0, "collectionId":Ljava/lang/String;
    const/4 v1, -0x1

    .line 762
    .local v1, "status":I
    :cond_1
    :goto_0
    const/16 v3, 0x512

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 763
    iget v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    sparse-switch v3, :sswitch_data_0

    .line 773
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 765
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueInt()I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 766
    iget-object v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ItemOperations failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    goto :goto_0

    .line 770
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 771
    goto :goto_0

    .line 779
    :cond_2
    if-ne v1, v2, :cond_4

    if-eqz v0, :cond_4

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    .line 781
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_3

    .line 782
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v3, "ItemOperationsPaser::itemOperationsEmptyFolderContentsParser() end"

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    :cond_3
    return-void

    .line 779
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 763
    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_1
        0x50d -> :sswitch_0
    .end sparse-switch
.end method

.method private itemOperationsFetchParser()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 202
    sget-boolean v7, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v7, :cond_0

    .line 203
    const-string v7, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v8, "ItemOperationsPaser::itemOperationsFetchParser() start"

    invoke-static {v7, v8}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    const/4 v3, 0x0

    .line 209
    .local v3, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v0, 0x0

    .line 211
    .local v0, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    const/4 v2, 0x0

    .line 213
    .local v2, "fileReference":Ljava/lang/String;
    iput-boolean v6, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->isDoc:Z

    .line 214
    :cond_1
    :goto_0
    const/16 v7, 0x506

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v7

    const/4 v8, 0x3

    if-eq v7, v8, :cond_5

    .line 215
    iget v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    sparse-switch v7, :sswitch_data_0

    .line 273
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 217
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueInt()I

    move-result v7

    iput v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    if-eq v7, v5, :cond_1

    .line 218
    iget-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ItemOperations failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    goto :goto_0

    .line 229
    :sswitch_2
    iput-boolean v5, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->isDoc:Z

    .line 230
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    goto :goto_0

    .line 235
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 236
    const-string v7, "ItemOperationsParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BASE_FILE_REFERENCE : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :sswitch_4
    iget-boolean v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->isDoc:Z

    if-eqz v7, :cond_4

    .line 244
    iget-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 246
    iget-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mServerId:Ljava/lang/String;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v7, v8}, Lcom/android/exchange/adapter/ItemOperationsParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 247
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_3

    .line 249
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 250
    new-instance v4, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v4}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 251
    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .local v4, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_start_1
    invoke-virtual {v4, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restore(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v4

    .line 254
    .end local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 259
    :cond_3
    if-eqz v3, :cond_1

    .line 260
    iget-object v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mContext:Landroid/content/Context;

    iget-wide v8, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_1

    .line 262
    invoke-direct {p0, v3, v0}, Lcom/android/exchange/adapter/ItemOperationsParser;->fetchDocPropertiesParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    goto/16 :goto_0

    .line 254
    :catchall_0
    move-exception v5

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v5

    .line 268
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_4
    invoke-direct {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->fetchPropertiesParser()V

    goto/16 :goto_0

    .line 279
    :cond_5
    iget v7, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    if-ne v7, v5, :cond_7

    :goto_2
    iput-boolean v5, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    .line 281
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v5, :cond_6

    .line 282
    const-string v5, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v6, "ItemOperationsPaser::itemOperationsFetchParser() end"

    invoke-static {v5, v6}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_6
    return-void

    :cond_7
    move v5, v6

    .line 279
    goto :goto_2

    .line 254
    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v1    # "c":Landroid/database/Cursor;
    .restart local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto :goto_1

    .line 215
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_1
        0x3d8 -> :sswitch_1
        0x451 -> :sswitch_3
        0x4c5 -> :sswitch_2
        0x50b -> :sswitch_4
        0x50d -> :sswitch_0
    .end sparse-switch
.end method

.method private itemOperationsMoveParser()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 790
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v3, :cond_0

    .line 791
    const-string v3, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v4, "ItemOperationsPaser::itemOperationsMoveParser() start"

    invoke-static {v3, v4}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    :cond_0
    const/4 v1, -0x1

    .line 796
    .local v1, "status":I
    const/4 v0, 0x0

    .line 797
    .local v0, "convId":[B
    :cond_1
    :goto_0
    const/16 v3, 0x516

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    .line 798
    iget v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    const/16 v4, 0x50d

    if-ne v3, v4, :cond_2

    .line 799
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueInt()I

    move-result v1

    .line 800
    if-eq v1, v2, :cond_1

    .line 801
    iget-object v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ItemOperation:Move  failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    goto :goto_0

    .line 803
    :cond_2
    iget v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    const/16 v4, 0x518

    if-ne v3, v4, :cond_1

    .line 804
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueOpaque()[B

    move-result-object v0

    goto :goto_0

    .line 809
    :cond_3
    if-ne v1, v2, :cond_5

    if-eqz v0, :cond_5

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    .line 811
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_4

    .line 812
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v3, "ItemOperationsPaser::itemOperationsMoveParser() end"

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :cond_4
    return-void

    .line 809
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private itemOperationsResponsesParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v1, "ItemOperationsPaser::itemOperationsResponsesParser() start"

    invoke-static {v0, v1}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    :goto_0
    const/16 v0, 0x50e

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 175
    iget v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    .line 190
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 177
    :sswitch_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->itemOperationsFetchParser()V

    goto :goto_0

    .line 180
    :sswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->itemOperationsEmptyFolderContentsParser()V

    goto :goto_0

    .line 185
    :sswitch_2
    invoke-direct {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->itemOperationsMoveParser()V

    goto :goto_0

    .line 195
    :cond_1
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v0, :cond_2

    .line 196
    const-string v0, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v1, "ItemOperationsPaser::itemOperationsResponsesParser() end"

    invoke-static {v0, v1}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_2
    return-void

    .line 175
    nop

    :sswitch_data_0
    .sparse-switch
        0x506 -> :sswitch_0
        0x512 -> :sswitch_1
        0x516 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    return-void
.end method

.method public commit()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    return-void
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    return v0
.end method

.method public final getUniqueTempFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 746
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "tempFile_ItemOperationsParser"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 747
    .local v0, "ret":Ljava/lang/StringBuffer;
    const-string v1, "_tid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 748
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 749
    const-string v1, "_created_at_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 750
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 751
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v1, "ItemOperationsPaser::parse() start"

    invoke-static {v0, v1}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v0

    const/16 v1, 0x505

    if-eq v0, v1, :cond_1

    .line 114
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 142
    :pswitch_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->itemOperationsResponsesParser()V

    .line 116
    :cond_1
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    .line 117
    iget v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->tag:I

    packed-switch v0, :pswitch_data_0

    .line 145
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->skipTag()V

    goto :goto_0

    .line 121
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/ItemOperationsParser;->getValueInt()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    if-eq v0, v4, :cond_1

    .line 122
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ItemOperations failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 125
    iget v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ItemOperationsParser;->isProvisioningStatus(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mService:Lcom/android/exchange/EasSyncService;

    iput-boolean v4, v0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 127
    iput-boolean v3, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    .line 133
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mStatus:I

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/ItemOperationsParser;->isDeviceAccessDenied(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    new-instance v0, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v1, 0x40001

    const v2, 0x7f060015

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v0

    .line 150
    :cond_3
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v0, :cond_4

    .line 151
    const-string v0, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v1, "ItemOperationsPaser::parse() end"

    invoke-static {v0, v1}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_4
    iget-boolean v0, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mResult:Z

    return v0

    .line 117
    :pswitch_data_0
    .packed-switch 0x50d
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    return-void
.end method

.method public setObserver(Ljava/util/Observer;)V
    .locals 0
    .param p1, "observer"    # Ljava/util/Observer;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mObserver:Ljava/util/Observer;

    .line 104
    return-void
.end method

.method public setOutputStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "os"    # Ljava/io/OutputStream;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/exchange/adapter/ItemOperationsParser;->mOs:Ljava/io/OutputStream;

    .line 100
    return-void
.end method
