.class public Lcom/android/exchange/adapter/ItemOperationsAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "ItemOperationsAdapter.java"


# instance fields
.field public isMIMEDataRequested:Z


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ItemOperationsAdapter;->isMIMEDataRequested:Z

    .line 20
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 1
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    return-void
.end method

.method public setMIMERequested(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/android/exchange/adapter/ItemOperationsAdapter;->isMIMEDataRequested:Z

    .line 67
    return-void
.end method

.method public wipe()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method
