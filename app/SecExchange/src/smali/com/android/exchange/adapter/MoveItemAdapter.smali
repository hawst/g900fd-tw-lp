.class public Lcom/android/exchange/adapter/MoveItemAdapter;
.super Lcom/android/exchange/adapter/AbstractCommandAdapter;
.source "MoveItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;
    }
.end annotation


# instance fields
.field private MOVE_ITEMS_PROJECTION:[Ljava/lang/String;

.field private mDstMailboxId:I

.field private parser:Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;


# direct methods
.method static synthetic access$000(Lcom/android/exchange/adapter/MoveItemAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/MoveItemAdapter;

    .prologue
    .line 25
    iget v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mDstMailboxId:I

    return v0
.end method


# virtual methods
.method public callback(I)V
    .locals 4
    .param p1, "status"    # I

    .prologue
    .line 112
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-interface {v0, v2, v3, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->moveItemStatus(JI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MoveItemAdapter;->wipe()V

    .line 57
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    const-string v0, "MoveItems"

    return-object v0
.end method

.method public hasChangedItems()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 95
    const/4 v6, 0x0

    .line 96
    .local v6, "count":I
    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 97
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->MOVE_ITEMS_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mailboxKey="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "flagMoved"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=1"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 100
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 102
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 104
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 107
    :cond_0
    if-lez v6, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    .line 104
    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    .line 107
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;-><init>(Lcom/android/exchange/adapter/MoveItemAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/MoveItemAdapter;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->parser:Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;

    .line 88
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->parser:Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;->parse()Z

    move-result v0

    return v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 14
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 123
    const/16 v0, 0x145

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 125
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mailbox is null"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 130
    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Mailbox can not be synchronized."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->MOVE_ITEMS_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mailboxKey="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "flagMoved"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=1"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 138
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_5

    .line 140
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 142
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 144
    .local v8, "serverId":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mDstMailboxId:I

    .line 145
    iget-object v0, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mDstMailboxId:I

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v7

    .line 147
    .local v7, "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v7, :cond_3

    if-eqz v8, :cond_3

    .line 149
    const/16 v0, 0x146

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x147

    invoke-virtual {v0, v1, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x148

    iget-object v2, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x149

    iget-object v2, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 166
    .end local v7    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v8    # "serverId":Ljava/lang/String;
    :goto_0
    return v0

    .line 155
    .restart local v7    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v8    # "serverId":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MoveItem: mailbox not found"

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/MoveItemAdapter;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v10

    goto :goto_0

    .end local v7    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v8    # "serverId":Ljava/lang/String;
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 164
    :cond_5
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move v0, v10

    .line 166
    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 290
    return-void
.end method

.method public wipe()V
    .locals 6

    .prologue
    .line 65
    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->parser:Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;

    iget-object v1, v1, Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;->mSrcMsgId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 66
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 67
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "flagMoved"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "dstMailboxKey"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 71
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v1

    if-nez v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncServerId=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/MoveItemAdapter;->parser:Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;

    iget-object v5, v5, Lcom/android/exchange/adapter/MoveItemAdapter$MoveItemParser;->mSrcMsgId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 75
    :cond_0
    monitor-exit v2

    .line 77
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_1
    return-void

    .line 75
    .restart local v0    # "cv":Landroid/content/ContentValues;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
