.class public Lcom/android/exchange/adapter/LogAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "LogAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/LogAdapter$LogParser;
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 0
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 13
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    new-instance v0, Lcom/android/exchange/adapter/LogAdapter$LogParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/adapter/LogAdapter$LogParser;-><init>(Lcom/android/exchange/adapter/LogAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 19
    .local v0, "parser":Lcom/android/exchange/adapter/LogAdapter$LogParser;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/LogAdapter$LogParser;->parse()Z

    move-result v1

    return v1
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 1
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    return-void
.end method

.method public wipe()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
