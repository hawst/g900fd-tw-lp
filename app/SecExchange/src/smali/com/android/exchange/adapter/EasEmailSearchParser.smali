.class public Lcom/android/exchange/adapter/EasEmailSearchParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "EasEmailSearchParser.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private msgs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/android/exchange/provider/EmailResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/android/exchange/adapter/EasEmailSearchParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/EasEmailSearchParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Lcom/android/exchange/provider/EmailResult;

    invoke-direct {v0}, Lcom/android/exchange/provider/EmailResult;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    .line 55
    return-void
.end method

.method private parsePropertiesTag(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 1
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/16 v0, 0x3cf

    invoke-static {p0, p1, v0}, Lcom/android/exchange/adapter/ParserUtility;->addMessageData(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Message;I)V

    .line 59
    iget-object v0, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method private parseResultTag()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    .line 63
    new-instance v1, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v1}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 64
    .local v1, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-object v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    .line 65
    iget-object v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 67
    const/4 v3, 0x1

    iput v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 68
    const-string v3, "eas"

    iput-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountSchema:Ljava/lang/String;

    .line 69
    iput v6, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxType:I

    .line 70
    :goto_0
    const/16 v3, 0x3ce

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/EasEmailSearchParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_5

    .line 71
    iget v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v4, 0x3d8

    if-ne v3, v4, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "longId":Ljava/lang/String;
    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    goto :goto_0

    .line 74
    .end local v0    # "longId":Ljava/lang/String;
    :cond_0
    iget v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v4, 0x3cf

    if-ne v3, v4, :cond_1

    .line 75
    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/EasEmailSearchParser;->parsePropertiesTag(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 76
    :cond_1
    iget v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 77
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "syncClass":Ljava/lang/String;
    const-string v3, "SMS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit16 v3, v3, 0x100

    iput v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_0

    .line 80
    :cond_2
    const-string v3, "Calendar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 81
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit16 v3, v3, 0x300

    iput v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_0

    .line 83
    :cond_3
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit8 v3, v3, 0x0

    iput v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_0

    .line 89
    .end local v2    # "syncClass":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->skipTag()V

    goto :goto_0

    .line 93
    :cond_5
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    return-void
.end method

.method public commit()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 171
    const/4 v5, 0x0

    .line 172
    .local v5, "notifyCount":I
    const/4 v0, 0x0

    .line 173
    .local v0, "count":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v6, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 176
    .local v4, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    add-int/lit8 v0, v0, 0x1

    .line 177
    iget v8, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    const/16 v9, 0x300

    if-eq v8, v9, :cond_0

    .line 180
    iget-boolean v8, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    if-nez v8, :cond_1

    .line 181
    add-int/lit8 v5, v5, 0x1

    .line 183
    :cond_1
    invoke-virtual {v4, v6}, Lcom/android/emailcommon/provider/EmailContent$Message;->addSaveOps(Ljava/util/ArrayList;)I

    .line 184
    rem-int/lit8 v8, v0, 0xa

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v0, v8, :cond_0

    .line 185
    :cond_2
    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 186
    :try_start_0
    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v8}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 187
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    .end local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_3
    :goto_1
    return-void

    .line 189
    .restart local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_4
    :try_start_1
    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v10, "com.android.email.provider"

    invoke-virtual {v8, v10, v6}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 190
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v11, v8, v10

    const/4 v10, 0x1

    const-string v11, " SyncKey saved as: "

    aput-object v11, v8, v10

    const/4 v10, 0x2

    iget-object v11, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    aput-object v11, v8, v10

    invoke-virtual {p0, v8}, Lcom/android/exchange/adapter/EasEmailSearchParser;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :goto_2
    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 197
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 191
    :catch_0
    move-exception v2

    .line 192
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_3
    sget-object v8, Lcom/android/exchange/adapter/EasEmailSearchParser;->TAG:Ljava/lang/String;

    const-string v10, "Failed at mContentResolver.applyBatch."

    invoke-static {v8, v10, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 196
    .end local v2    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v8

    .line 193
    :catch_1
    move-exception v2

    .line 194
    .local v2, "e":Landroid/content/OperationApplicationException;
    :try_start_4
    sget-object v8, Lcom/android/exchange/adapter/EasEmailSearchParser;->TAG:Ljava/lang/String;

    const-string v10, "Failed at mContentResolver.applyBatch."

    invoke-static {v8, v10, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 200
    .end local v2    # "e":Landroid/content/OperationApplicationException;
    .end local v4    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_5
    sget-object v8, Lcom/android/exchange/adapter/EasEmailSearchParser;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Search on server result set count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ;inserted set count : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    if-lez v5, :cond_3

    .line 203
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 204
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v8, "field"

    const-string v9, "newMessageCount"

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v8, "add"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 206
    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Account;->ADD_TO_FIELD_URI:Landroid/net/Uri;

    iget-object v9, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 207
    .local v7, "uri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v8, v7, v1, v12, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public parse()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/EasEmailSearchParser;->nextTag(I)I

    move-result v2

    const/16 v3, 0x3c5

    if-eq v2, v3, :cond_0

    .line 103
    new-instance v2, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v2, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v2

    .line 110
    .local v1, "status":I
    :sswitch_0
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v6, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 105
    .end local v1    # "status":I
    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/EasEmailSearchParser;->nextTag(I)I

    move-result v2

    if-eq v2, v7, :cond_4

    .line 106
    iget v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v3, 0x3cc

    if-ne v2, v3, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->getValueInt()I

    move-result v1

    .line 108
    .restart local v1    # "status":I
    sparse-switch v1, :sswitch_data_0

    .line 142
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v3, -0x1

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 113
    :sswitch_1
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v7, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 116
    :sswitch_2
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v3, 0x9

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 119
    :sswitch_3
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v3, 0xb

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 122
    :sswitch_4
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v3, 0x8

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 125
    :sswitch_5
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v3, 0xa

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 128
    :sswitch_6
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v3, 0xc

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 131
    :sswitch_7
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v5, v2, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 136
    :sswitch_8
    new-instance v2, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v3, 0x40001

    const v4, 0x7f060015

    invoke-direct {v2, v3, v4}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v2

    .line 145
    .end local v1    # "status":I
    :cond_1
    iget v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v3, 0x3ce

    if-ne v2, v3, :cond_2

    .line 146
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->parseResultTag()V

    goto :goto_0

    .line 147
    :cond_2
    iget v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v3, 0x3cb

    if-ne v2, v3, :cond_3

    .line 148
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "range":[Ljava/lang/String;
    array-length v2, v0

    if-ne v2, v6, :cond_0

    .line 150
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    aget-object v3, v0, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->startRange:I

    .line 151
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    aget-object v3, v0, v5

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->endRange:I

    goto/16 :goto_0

    .line 153
    .end local v0    # "range":[Ljava/lang/String;
    :cond_3
    iget v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->tag:I

    const/16 v3, 0x3d0

    if-ne v2, v3, :cond_0

    .line 154
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->getValueInt()I

    move-result v3

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->total:I

    goto/16 :goto_0

    .line 157
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iget v2, v2, Lcom/android/exchange/provider/EmailResult;->total:I

    iget-object v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_5

    .line 158
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iget-object v3, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, v2, Lcom/android/exchange/provider/EmailResult;->total:I

    .line 159
    :cond_5
    iget-object v2, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iget v2, v2, Lcom/android/exchange/provider/EmailResult;->total:I

    if-lez v2, :cond_6

    .line 160
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->commit()V

    .line 162
    :cond_6
    return v5

    .line 108
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_7
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_4
        0x9 -> :sswitch_2
        0xa -> :sswitch_5
        0xb -> :sswitch_3
        0xc -> :sswitch_6
        0x81 -> :sswitch_8
    .end sparse-switch
.end method

.method public parse_email_response()Lcom/android/exchange/provider/EmailResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasEmailSearchParser;->parse()Z

    .line 97
    iget-object v0, p0, Lcom/android/exchange/adapter/EasEmailSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    return-object v0
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    return-void
.end method
