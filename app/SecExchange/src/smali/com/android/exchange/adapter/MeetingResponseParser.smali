.class public Lcom/android/exchange/adapter/MeetingResponseParser;
.super Lcom/android/exchange/adapter/Parser;
.source "MeetingResponseParser.java"


# instance fields
.field private mService:Lcom/android/exchange/EasSyncService;

.field private mStatus:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;)V

    .line 33
    iput-object p2, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mService:Lcom/android/exchange/EasSyncService;

    .line 34
    return-void
.end method


# virtual methods
.method public getStatus()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mStatus:I

    return v0
.end method

.method public parse()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 53
    const/4 v0, 0x0

    .line 54
    .local v0, "res":Z
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/MeetingResponseParser;->nextTag(I)I

    move-result v1

    const/16 v2, 0x207

    if-eq v1, v2, :cond_0

    .line 55
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/MeetingResponseParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 58
    iget v1, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->tag:I

    const/16 v2, 0x20a

    if-ne v1, v2, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MeetingResponseParser;->parseResult()V

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MeetingResponseParser;->skipTag()V

    goto :goto_0

    .line 64
    :cond_2
    return v0
.end method

.method public parseResult()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 37
    :cond_0
    :goto_0
    const/16 v0, 0x20a

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/MeetingResponseParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    .line 38
    iget v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->tag:I

    const/16 v1, 0x20b

    if-ne v0, v1, :cond_1

    .line 39
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MeetingResponseParser;->getValueInt()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mStatus:I

    .line 40
    iget v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mStatus:I

    if-eq v0, v4, :cond_0

    .line 41
    iget-object v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v1, v4, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in meeting response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mStatus:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->tag:I

    const/16 v1, 0x205

    if-ne v0, v1, :cond_2

    .line 44
    iget-object v0, p0, Lcom/android/exchange/adapter/MeetingResponseParser;->mService:Lcom/android/exchange/EasSyncService;

    new-array v1, v4, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Meeting response calendar id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/exchange/adapter/MeetingResponseParser;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/MeetingResponseParser;->skipTag()V

    goto :goto_0

    .line 49
    :cond_3
    return-void
.end method
