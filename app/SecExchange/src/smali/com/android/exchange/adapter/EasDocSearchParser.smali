.class public Lcom/android/exchange/adapter/EasDocSearchParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "EasDocSearchParser.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private docs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Document;",
            ">;"
        }
    .end annotation
.end field

.field private mFoldId:J

.field private msgs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field private res:Lcom/android/exchange/provider/EmailResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/android/exchange/adapter/EasDocSearchParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->docs:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->msgs:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Lcom/android/exchange/provider/EmailResult;

    invoke-direct {v0}, Lcom/android/exchange/provider/EmailResult;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    .line 68
    return-void
.end method

.method private addDocData(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Document;Lcom/android/emailcommon/provider/EmailContent$Message;I)V
    .locals 20
    .param p1, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p2, "doc"    # Lcom/android/emailcommon/provider/EmailContent$Document;
    .param p3, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p4, "loopTag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    const/4 v12, 0x0

    .line 79
    .local v12, "fileName":Ljava/lang/String;
    const/4 v15, 0x0

    .line 80
    .local v15, "length":Ljava/lang/String;
    const/16 v17, 0x0

    .line 81
    .local v17, "location":Ljava/lang/String;
    const/4 v14, 0x0

    .line 82
    .local v14, "isFolder":Z
    const/16 v16, 0x0

    .line 83
    .local v16, "linkId":Ljava/lang/String;
    const/16 v18, 0x0

    .line 84
    .local v18, "parentLinkId":Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .local v10, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 86
    .local v2, "cal":Ljava/util/GregorianCalendar;
    const/4 v3, 0x1

    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagFavorite:Z

    .line 87
    :goto_0
    move-object/from16 v0, p1

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    .line 88
    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    packed-switch v3, :pswitch_data_0

    .line 141
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 90
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mLinkId:Ljava/lang/String;

    .line 92
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mLinkId:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    .line 93
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mLinkId:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 94
    goto :goto_0

    .line 96
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mDisplayName:Ljava/lang/String;

    .line 97
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mDisplayName:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 98
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mDisplayName:Ljava/lang/String;

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    .line 99
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mDisplayName:Ljava/lang/String;

    .line 100
    goto :goto_0

    .line 102
    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v14, 0x1

    .line 103
    :goto_1
    move-object/from16 v0, p2

    iput-boolean v14, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mFlagFolder:Z

    .line 104
    if-nez v14, :cond_1

    const/4 v3, 0x1

    :goto_2
    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagFavorite:Z

    goto :goto_0

    .line 102
    :cond_0
    const/4 v14, 0x0

    goto :goto_1

    .line 104
    :cond_1
    const/4 v3, 0x0

    goto :goto_2

    .line 107
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 108
    .local v11, "date":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v11, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x5

    const/4 v5, 0x7

    invoke-virtual {v11, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const/16 v5, 0x8

    const/16 v6, 0xa

    invoke-virtual {v11, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xb

    const/16 v7, 0xd

    invoke-virtual {v11, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0xe

    const/16 v8, 0x10

    invoke-virtual {v11, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x11

    const/16 v19, 0x13

    move/from16 v0, v19

    invoke-virtual {v11, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual/range {v2 .. v8}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 114
    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 115
    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mTimeStamp:J

    .line 116
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mTimeStamp:J

    move-object/from16 v0, p3

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    goto/16 :goto_0

    .line 119
    .end local v11    # "date":Ljava/lang/String;
    :pswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    :goto_3
    move-object/from16 v0, p2

    iput-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mFlagHidden:Z

    goto/16 :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    .line 123
    :pswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 124
    .restart local v11    # "date":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {v11, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x5

    const/4 v5, 0x7

    invoke-virtual {v11, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const/16 v5, 0x8

    const/16 v6, 0xa

    invoke-virtual {v11, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xb

    const/16 v7, 0xd

    invoke-virtual {v11, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0xe

    const/16 v8, 0x10

    invoke-virtual {v11, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x11

    const/16 v19, 0x13

    move/from16 v0, v19

    invoke-virtual {v11, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual/range {v2 .. v8}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 130
    const-string v3, "GMT"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 131
    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mLastModTime:J

    goto/16 :goto_0

    .line 134
    .end local v11    # "date":Ljava/lang/String;
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v15

    .line 135
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-object/from16 v0, p2

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mContentLength:J

    goto/16 :goto_0

    .line 138
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mContentType:Ljava/lang/String;

    goto/16 :goto_0

    .line 144
    :cond_3
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mFlagFolder:Z

    if-eqz v3, :cond_8

    .line 145
    const-string v3, "SharePoint-UNC : folder"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    .line 152
    :goto_4
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mLinkId:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 153
    const-string v3, "\\"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    .line 154
    .local v13, "index":I
    const/4 v3, -0x1

    if-ne v13, v3, :cond_4

    .line 155
    const-string v3, "/"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    .line 157
    :cond_4
    const/4 v3, -0x1

    if-eq v13, v3, :cond_5

    .line 158
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 159
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Document;->mParentFolderLinkId:Ljava/lang/String;

    .line 163
    :cond_5
    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    .line 165
    if-nez v14, :cond_6

    if-eqz v12, :cond_6

    if-eqz v15, :cond_6

    if-eqz v17, :cond_6

    .line 166
    new-instance v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v9}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 167
    .local v9, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const-string v3, "base64"

    iput-object v3, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mEncoding:Ljava/lang/String;

    .line 168
    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    iput-wide v4, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    .line 169
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 170
    iput-object v12, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 171
    move-object/from16 v0, v17

    iput-object v0, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    .line 172
    invoke-static {v12}, Lcom/android/exchange/adapter/ParserUtility;->getMimeTypeFromFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 173
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const/4 v3, 0x1

    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 176
    .end local v9    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_6
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 177
    move-object/from16 v0, p3

    iput-object v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 179
    :cond_7
    return-void

    .line 147
    .end local v13    # "index":I
    :cond_8
    const-string v3, "SharePoint-UNC"

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mContentType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mContentLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto/16 :goto_4

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x4c5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private parsePropertiesTag(Lcom/android/emailcommon/provider/EmailContent$Document;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 1
    .param p1, "doc"    # Lcom/android/emailcommon/provider/EmailContent$Document;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    const/16 v0, 0x3cf

    invoke-direct {p0, p0, p1, p2, v0}, Lcom/android/exchange/adapter/EasDocSearchParser;->addDocData(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Document;Lcom/android/emailcommon/provider/EmailContent$Message;I)V

    .line 183
    iget-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->docs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    return-void
.end method

.method private parseResultTag()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x62

    .line 188
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Document;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$Document;-><init>()V

    .line 189
    .local v0, "doc":Lcom/android/emailcommon/provider/EmailContent$Document;
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mAccountKey:J

    .line 190
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v4

    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Document;->mMailboxKey:J

    .line 192
    new-instance v1, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v1}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 193
    .local v1, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    .line 194
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v4

    iput-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 196
    const/4 v3, 0x1

    iput v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 198
    :goto_0
    const/16 v3, 0x3ce

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/EasDocSearchParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 199
    iget v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->tag:I

    const/16 v4, 0x3cf

    if-ne v3, v4, :cond_0

    .line 200
    invoke-direct {p0, v0, v1}, Lcom/android/exchange/adapter/EasDocSearchParser;->parsePropertiesTag(Lcom/android/emailcommon/provider/EmailContent$Document;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "otherValue":Ljava/lang/String;
    sget-object v3, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    .end local v2    # "otherValue":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 289
    return-void
.end method

.method public commit()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v4, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->docs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Document;

    .line 295
    .local v0, "doc":Lcom/android/emailcommon/provider/EmailContent$Document;
    invoke-virtual {v0, v4}, Lcom/android/emailcommon/provider/EmailContent$Document;->addSaveOps(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 297
    .end local v0    # "doc":Lcom/android/emailcommon/provider/EmailContent$Document;
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v5, "opsMsg":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->msgs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 299
    .local v3, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    invoke-virtual {v3, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->addSaveOps(Ljava/util/ArrayList;)I

    goto :goto_1

    .line 302
    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_1
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v6}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 304
    :try_start_0
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v6}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 305
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    :goto_2
    return-void

    .line 307
    :cond_2
    :try_start_1
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v8, "com.android.email.provider"

    invoke-virtual {v6, v8, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 308
    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "document search"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " total rows: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/EasDocSearchParser;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    :goto_3
    :try_start_2
    iget-object v6, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v8, "com.android.email.provider"

    invoke-virtual {v6, v8, v5}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 316
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, " SyncKey saved as: "

    aput-object v9, v6, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    aput-object v9, v6, v8

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/EasDocSearchParser;->userLog([Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 322
    :goto_4
    :try_start_3
    monitor-exit v7

    goto :goto_2

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 309
    :catch_0
    move-exception v1

    .line 310
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_4
    sget-object v6, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    const-string v8, "Failed at mContentResolver.applyBatch."

    invoke-static {v6, v8, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 311
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 312
    .local v1, "e":Landroid/content/OperationApplicationException;
    sget-object v6, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    const-string v8, "Failed at mContentResolver.applyBatch."

    invoke-static {v6, v8, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 317
    .end local v1    # "e":Landroid/content/OperationApplicationException;
    :catch_2
    move-exception v1

    .line 318
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    const-string v8, "Failed at mContentResolver.applyBatch."

    invoke-static {v6, v8, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 319
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v1

    .line 320
    .local v1, "e":Landroid/content/OperationApplicationException;
    sget-object v6, Lcom/android/exchange/adapter/EasDocSearchParser;->TAG:Ljava/lang/String;

    const-string v8, "Failed at mContentResolver.applyBatch."

    invoke-static {v6, v8, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4
.end method

.method public parse()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 215
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EasDocSearchParser;->nextTag(I)I

    move-result v4

    const/16 v5, 0x3c5

    if-eq v4, v5, :cond_0

    .line 216
    new-instance v2, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v2, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v2

    .line 223
    .local v1, "status":I
    :pswitch_0
    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v3, v4, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 218
    .end local v1    # "status":I
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EasDocSearchParser;->nextTag(I)I

    move-result v4

    if-eq v4, v7, :cond_4

    .line 219
    iget v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->tag:I

    const/16 v5, 0x3cc

    if-ne v4, v5, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->getValueInt()I

    move-result v1

    .line 221
    .restart local v1    # "status":I
    packed-switch v1, :pswitch_data_0

    .line 266
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v4, -0x1

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 284
    .end local v1    # "status":I
    :goto_1
    return v2

    .line 227
    .restart local v1    # "status":I
    :pswitch_1
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v6, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 230
    :pswitch_2
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iput v7, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 233
    :pswitch_3
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0x9

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 236
    :pswitch_4
    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v5, 0xb

    iput v5, v4, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_0

    .line 239
    :pswitch_5
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0x8

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 242
    :pswitch_6
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0xa

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 245
    :pswitch_7
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0xc

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 248
    :pswitch_8
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v4, 0x4

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 251
    :pswitch_9
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v4, 0x5

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 254
    :pswitch_a
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v4, 0x6

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 257
    :pswitch_b
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/4 v4, 0x7

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 260
    :pswitch_c
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0xd

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 263
    :pswitch_d
    iget-object v3, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    const/16 v4, 0xe

    iput v4, v3, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_1

    .line 269
    .end local v1    # "status":I
    :cond_1
    iget v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->tag:I

    const/16 v5, 0x3ce

    if-ne v4, v5, :cond_2

    .line 270
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->parseResultTag()V

    goto :goto_0

    .line 271
    :cond_2
    iget v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->tag:I

    const/16 v5, 0x3cb

    if-ne v4, v5, :cond_3

    .line 272
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 273
    .local v0, "range":[Ljava/lang/String;
    array-length v4, v0

    if-ne v4, v6, :cond_0

    .line 274
    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    aget-object v5, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lcom/android/exchange/provider/EmailResult;->startRange:I

    .line 275
    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    aget-object v5, v0, v3

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Lcom/android/exchange/provider/EmailResult;->endRange:I

    goto/16 :goto_0

    .line 277
    .end local v0    # "range":[Ljava/lang/String;
    :cond_3
    iget v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->tag:I

    const/16 v5, 0x3d0

    if-ne v4, v5, :cond_0

    .line 278
    iget-object v4, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->getValueInt()I

    move-result v5

    iput v5, v4, Lcom/android/exchange/provider/EmailResult;->total:I

    goto/16 :goto_0

    .line 281
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    iget v2, v2, Lcom/android/exchange/provider/EmailResult;->total:I

    if-lez v2, :cond_5

    .line 282
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->commit()V

    :cond_5
    move v2, v3

    .line 284
    goto/16 :goto_1

    .line 221
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public parse_doc_response()Lcom/android/exchange/provider/EmailResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EasDocSearchParser;->parse()Z

    .line 210
    iget-object v0, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->res:Lcom/android/exchange/provider/EmailResult;

    return-object v0
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 327
    return-void
.end method

.method public setFoldId(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 72
    iput-wide p1, p0, Lcom/android/exchange/adapter/EasDocSearchParser;->mFoldId:J

    .line 73
    return-void
.end method
