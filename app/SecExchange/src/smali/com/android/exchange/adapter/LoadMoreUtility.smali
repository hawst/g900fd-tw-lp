.class public Lcom/android/exchange/adapter/LoadMoreUtility;
.super Ljava/lang/Object;
.source "LoadMoreUtility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static updateEmail(Lcom/android/exchange/AbstractSyncService;Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
    .locals 20
    .param p0, "service"    # Lcom/android/exchange/AbstractSyncService;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    sget-boolean v15, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v15, :cond_0

    .line 50
    const-string v15, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v16, "LoadMoreUtility::updateEmail() start"

    invoke-static/range {v15 .. v16}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    const-string v15, "LoadMoreUtility"

    const-string v16, "updateEmail start"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 56
    .local v7, "cr":Landroid/content/ContentResolver;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v13, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 61
    .local v8, "cvBody":Landroid/content/ContentValues;
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v15, :cond_1

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 62
    :cond_1
    const-string v15, "LoadMoreUtility"

    const-string v16, "updateEmail empty HTML content"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v15, "htmlContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v15, :cond_3

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_3

    .line 67
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    const/16 v16, 0x0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 69
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    const-string v16, "\r\n"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x2

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v14

    .line 70
    .local v14, "value":I
    const/4 v15, -0x1

    if-eq v14, v15, :cond_2

    .line 71
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v15, v0, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 74
    :cond_2
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v15, :cond_7

    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    const v16, 0x32000

    move/from16 v0, v16

    if-le v15, v0, :cond_7

    .line 77
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 78
    const-string v15, "textContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v16, v0

    const v17, 0x7d000

    const v18, 0x7f06001a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v16 .. v19}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .end local v14    # "value":I
    :cond_3
    :goto_0
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-nez v15, :cond_4

    .line 95
    const-string v15, "LoadMoreUtility"

    const-string v16, "There is no MIME parsed Bodyparts."

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v15, ""

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 125
    :cond_4
    :goto_1
    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mId:J

    move-wide/from16 v16, v0

    invoke-static/range {v15 .. v17}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v15

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15, v8}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v4

    .line 131
    .local v4, "TmpMessage":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v4, :cond_c

    .line 132
    const-string v15, "LoadMoreUtility"

    const-string v16, "TmpMessage is null"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_5
    :goto_2
    return-void

    .line 82
    .end local v4    # "TmpMessage":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v14    # "value":I
    :cond_6
    move-object/from16 v0, p3

    iget v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v15, v15, 0x2

    move-object/from16 v0, p3

    iput v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 83
    const-string v15, "textContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v16, v0

    const v17, 0x32000

    const v18, 0x7f06001a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v16 .. v19}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v15, "fileSaveFlags"

    move-object/from16 v0, p3

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 89
    :cond_7
    const-string v15, "textContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 100
    .end local v14    # "value":I
    :cond_8
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->charAt(I)C

    move-result v12

    .line 103
    .local v12, "lastChar":C
    if-nez v12, :cond_9

    .line 104
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    const/16 v16, 0x0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 106
    :cond_9
    move-object/from16 v0, p3

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    const v16, 0x32000

    move/from16 v0, v16

    if-le v15, v0, :cond_b

    .line 108
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v15

    if-eqz v15, :cond_a

    .line 109
    const-string v15, "htmlContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v16, v0

    const v17, 0x7d000

    const v18, 0x7f06001a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v16 .. v19}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :goto_3
    const/4 v15, 0x0

    move-object/from16 v0, p3

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    goto/16 :goto_1

    .line 113
    :cond_a
    move-object/from16 v0, p3

    iget v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    or-int/lit8 v15, v15, 0x1

    move-object/from16 v0, p3

    iput v15, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 114
    const-string v15, "htmlContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v16, v0

    const v17, 0x32000

    const v18, 0x7f06001a

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    invoke-static/range {v16 .. v19}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v15, "fileSaveFlags"

    move-object/from16 v0, p3

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_3

    .line 120
    :cond_b
    const-string v15, "htmlContent"

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 137
    .end local v12    # "lastChar":C
    .restart local v4    # "TmpMessage":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_c
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 138
    .local v9, "cvMsg":Landroid/content/ContentValues;
    const-string v15, "flagLoaded"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 140
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    if-eqz v15, :cond_d

    .line 141
    const-string v15, "snippet"

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_d
    move-object/from16 v0, p2

    iget-boolean v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    iget-boolean v0, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    move/from16 v16, v0

    move/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 145
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-eqz v15, :cond_e

    .line 146
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 147
    .local v5, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->toContentValues()Landroid/content/ContentValues;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 153
    .end local v5    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v11    # "i$":Ljava/util/Iterator;
    :cond_e
    move-object/from16 v0, p2

    iget-boolean v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-nez v15, :cond_f

    .line 154
    const-string v15, "flagAttachment"

    move-object/from16 v0, p2

    iget-boolean v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 156
    :cond_f
    const-string v15, "istruncated"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    move-object/from16 v0, p2

    iget v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_10

    const/4 v15, 0x2

    :goto_5
    move-object/from16 v0, p2

    iput v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 161
    const-string v15, "flagLoaded"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 164
    const-string v15, "flags"

    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 166
    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "_id="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/AbstractSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v16

    monitor-enter v16

    .line 170
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/AbstractSyncService;->isStopped()Z

    move-result v15

    if-eqz v15, :cond_11

    .line 171
    new-instance v15, Ljava/io/IOException;

    invoke-direct {v15}, Ljava/io/IOException;-><init>()V

    throw v15

    .line 199
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v15

    .line 158
    :cond_10
    const/4 v15, 0x1

    goto :goto_5

    .line 175
    :cond_11
    :try_start_1
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Body;->saveBodyToFilesIfNecessary(Landroid/content/Context;)V

    .line 176
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v17, "com.android.email.provider"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v13}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 178
    const-string v15, "LoadMoreUtility"

    const-string v17, "Testing if the attmt entry is made in table"

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v6

    .line 181
    .local v6, "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v6, :cond_12

    array-length v15, v6

    if-nez v15, :cond_13

    .line 182
    :cond_12
    const-string v15, "LoadMoreUtility"

    const-string v17, "Inside attmt null"

    move-object/from16 v0, v17

    invoke-static {v15, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    move-object/from16 v0, p2

    iget-boolean v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    iget-boolean v0, v4, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v15, v0, :cond_13

    .line 184
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-eqz v15, :cond_13

    .line 185
    move-object/from16 v0, p2

    iget-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_13

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 186
    .restart local v5    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->toContentValues()Landroid/content/ContentValues;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v15, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 192
    .end local v5    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v6    # "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v11    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v10

    .line 193
    .local v10, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v15, "LoadMoreUtility"

    invoke-static {v15, v10}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 194
    new-instance v15, Ljava/io/IOException;

    invoke-direct {v15}, Ljava/io/IOException;-><init>()V

    throw v15

    .line 195
    .end local v10    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v10

    .line 196
    .local v10, "e":Landroid/content/OperationApplicationException;
    const-string v15, "LoadMoreUtility"

    invoke-static {v15, v10}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 197
    new-instance v15, Ljava/io/IOException;

    invoke-direct {v15}, Ljava/io/IOException;-><init>()V

    throw v15

    .line 199
    .end local v10    # "e":Landroid/content/OperationApplicationException;
    .restart local v6    # "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_13
    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 200
    const-string v15, "LoadMoreUtility"

    const-string v16, "updateEmail finish"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    sget-boolean v15, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v15, :cond_5

    .line 203
    const-string v15, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v16, "LoadMoreUtility::updateEmail() end"

    invoke-static/range {v15 .. v16}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static updateInlineAttachment(Lcom/android/exchange/AbstractSyncService;Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 16
    .param p0, "service"    # Lcom/android/exchange/AbstractSyncService;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 271
    .local v11, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    move-object/from16 v0, p2

    iget-wide v12, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v3

    .line 273
    .local v3, "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v3, :cond_0

    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v12, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v10

    .line 279
    .local v10, "msgAttmts":[Ljava/lang/Object;
    array-length v12, v3

    new-array v8, v12, [I

    .line 281
    .local v8, "matchedCidParts":[I
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    array-length v12, v3

    if-ge v7, v12, :cond_2

    .line 282
    const/4 v12, -0x1

    aput v12, v8, v7

    .line 281
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 285
    :cond_2
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    array-length v12, v10

    if-ge v6, v12, :cond_6

    .line 287
    aget-object v9, v10, v6

    check-cast v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 289
    .local v9, "msgAttmt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const/4 v7, 0x0

    :goto_3
    array-length v12, v3

    if-ge v7, v12, :cond_5

    .line 291
    aget-object v2, v3, v7

    .line 292
    .local v2, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v12, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v12, :cond_4

    .line 289
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 295
    :cond_4
    aget v12, v8, v7

    if-gez v12, :cond_3

    .line 298
    iget-object v12, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    iget-object v13, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 299
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 300
    .local v4, "cv":Landroid/content/ContentValues;
    const-string v12, "contentId"

    iget-object v13, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-virtual {v4, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v12, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v12}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "_id="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-wide v14, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    invoke-virtual {v12, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    iget-object v12, v9, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    iput-object v12, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 307
    aput v6, v8, v7

    .line 285
    .end local v2    # "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v4    # "cv":Landroid/content/ContentValues;
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 314
    .end local v9    # "msgAttmt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/AbstractSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v13

    monitor-enter v13

    .line 315
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/AbstractSyncService;->isStopped()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 316
    new-instance v12, Ljava/io/IOException;

    invoke-direct {v12}, Ljava/io/IOException;-><init>()V

    throw v12

    .line 325
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 319
    :cond_7
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const-string v14, "com.android.email.provider"

    invoke-virtual {v12, v14, v11}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    :try_start_2
    monitor-exit v13

    goto/16 :goto_0

    .line 320
    :catch_0
    move-exception v5

    .line 321
    .local v5, "e":Landroid/os/RemoteException;
    new-instance v12, Ljava/io/IOException;

    invoke-direct {v12}, Ljava/io/IOException;-><init>()V

    throw v12

    .line 322
    .end local v5    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v5

    .line 323
    .local v5, "e":Landroid/content/OperationApplicationException;
    new-instance v12, Ljava/io/IOException;

    invoke-direct {v12}, Ljava/io/IOException;-><init>()V

    throw v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public static updateSignedMIMEDataAttachment(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)V
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "tempMimeFile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 213
    .local v6, "SIGNED_TEMPMIME_PATH":Ljava/lang/String;
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 214
    .local v14, "in":Ljava/io/InputStream;
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v7

    .line 216
    .local v7, "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v7, :cond_0

    array-length v0, v7

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_1

    :cond_0
    if-nez v7, :cond_2

    .line 217
    :cond_1
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 264
    :goto_0
    return-void

    .line 221
    :cond_2
    const/16 v16, 0x0

    .line 223
    .local v16, "os":Ljava/io/OutputStream;
    const-wide/16 v10, 0x0

    .line 224
    .local v10, "copySize":J
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v20, v0

    const/16 v22, 0x0

    aget-object v22, v7, v22

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    invoke-static/range {v20 .. v23}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;

    move-result-object v17

    .line 227
    .local v17, "saveUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v16

    .line 228
    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v20

    move/from16 v0, v20

    int-to-long v10, v0

    .line 234
    if-eqz v14, :cond_3

    .line 235
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 236
    :cond_3
    if-eqz v16, :cond_4

    .line 237
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V

    .line 240
    :cond_4
    :goto_1
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 241
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v20, v0

    const/16 v22, 0x0

    aget-object v22, v7, v22

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    move-wide/from16 v3, v22

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentFilename(Landroid/content/Context;JJ)Ljava/io/File;

    move-result-object v18

    .line 242
    .local v18, "savedFile":Ljava/io/File;
    if-eqz v18, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 244
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 245
    .local v9, "cv":Landroid/content/ContentValues;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 250
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v18    # "savedFile":Ljava/io/File;
    :cond_5
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 252
    .local v13, "fileToDelete":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_6

    .line 253
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 255
    :cond_6
    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 258
    .local v8, "contentUriString":Ljava/lang/String;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 259
    .restart local v9    # "cv":Landroid/content/ContentValues;
    const-string v20, "size"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 260
    const-string v20, "contentUri"

    move-object/from16 v0, v20

    invoke-virtual {v9, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    sget-object v20, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    const/16 v21, 0x0

    aget-object v21, v7, v21

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v19

    .line 263
    .local v19, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .line 229
    .end local v8    # "contentUriString":Ljava/lang/String;
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v13    # "fileToDelete":Ljava/io/File;
    .end local v19    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v12

    .line 230
    .local v12, "fileEx":Ljava/io/FileNotFoundException;
    :try_start_1
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234
    if-eqz v14, :cond_7

    .line 235
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 236
    :cond_7
    if-eqz v16, :cond_4

    .line 237
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V

    goto/16 :goto_1

    .line 231
    .end local v12    # "fileEx":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v15

    .line 232
    .local v15, "ioe":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 234
    if-eqz v14, :cond_8

    .line 235
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 236
    :cond_8
    if-eqz v16, :cond_4

    .line 237
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V

    goto/16 :goto_1

    .line 234
    .end local v15    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    if-eqz v14, :cond_9

    .line 235
    invoke-virtual {v14}, Ljava/io/InputStream;->close()V

    .line 236
    :cond_9
    if-eqz v16, :cond_a

    .line 237
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V

    :cond_a
    throw v20
.end method
