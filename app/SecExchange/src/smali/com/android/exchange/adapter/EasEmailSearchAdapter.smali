.class public Lcom/android/exchange/adapter/EasEmailSearchAdapter;
.super Ljava/lang/Object;
.source "EasEmailSearchAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 1
    .param p0, "tag"    # I
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 203
    invoke-virtual {p2, p0, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    return-object v0
.end method

.method private static appendDate(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 2
    .param p0, "tag"    # I
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 218
    invoke-virtual {p2, p0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 219
    .local v0, "serializer":Lcom/android/exchange/adapter/Serializer;
    const/16 v1, 0x8f

    invoke-static {v1, v0}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendEmptyTag(ILcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 220
    const/16 v1, 0x3d2

    invoke-static {v1, p1, v0}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 221
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    return-object v1
.end method

.method private static appendEmptyTag(ILcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 1
    .param p0, "tag"    # I
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 214
    invoke-virtual {p1, p0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    return-object v0
.end method

.method private static appendOpaqueData(I[BLcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 1
    .param p0, "tag"    # I
    .param p1, "value"    # [B
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p2, p0, p1}, Lcom/android/exchange/adapter/Serializer;->dataOpaque(I[B)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    return-object v0
.end method

.method private static appendSearchOptions(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "searchRequest"    # Lcom/android/exchange/SearchRequest;
    .param p3, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getOptionsRebuildResults()Z

    move-result v5

    .line 138
    .local v5, "rebuildResults":Z
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getOptionsDeepTraversal()Z

    move-result v1

    .line 139
    .local v1, "deepTraversal":Z
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getOptionsRange()Ljava/lang/String;

    move-result-object v4

    .line 140
    .local v4, "range":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getOptionsOptionsMIMESupport()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "MIMESupport":Ljava/lang/String;
    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 144
    .local v2, "protocolVersion":D
    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    cmpl-double v7, v2, v8

    if-nez v7, :cond_0

    .line 145
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v6, p3

    .line 199
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .local v6, "s":Lcom/android/exchange/adapter/Serializer;
    :goto_0
    return-object v6

    .line 148
    .end local v6    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_0
    if-nez v5, :cond_1

    if-nez v1, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    move-object v6, p3

    .line 150
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v6    # "s":Lcom/android/exchange/adapter/Serializer;
    goto :goto_0

    .line 153
    .end local v6    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_1
    const/16 v7, 0x3ca

    invoke-virtual {p3, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 154
    if-eqz v5, :cond_2

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpl-double v7, v2, v8

    if-ltz v7, :cond_2

    .line 155
    const/16 v7, 0x3d9

    invoke-static {v7, p3}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendEmptyTag(ILcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 157
    :cond_2
    if-eqz v1, :cond_3

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpl-double v7, v2, v8

    if-ltz v7, :cond_3

    .line 158
    const/16 v7, 0x3d7

    invoke-static {v7, p3}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendEmptyTag(ILcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 160
    :cond_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 161
    const/16 v7, 0x3cb

    invoke-static {v7, v4, p3}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 164
    :cond_4
    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpl-double v7, v2, v8

    if-ltz v7, :cond_7

    .line 165
    const-wide/high16 v8, 0x402c000000000000L    # 14.0

    cmpl-double v7, v2, v8

    if-lez v7, :cond_a

    .line 166
    invoke-virtual {p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailMessageDiffEnabled()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_9

    .line 167
    const/16 v7, 0x459

    invoke-virtual {p3, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 175
    :goto_1
    const/16 v7, 0x446

    const-string v8, "2"

    invoke-virtual {p3, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 176
    invoke-virtual {p1, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v7

    invoke-static {v7}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v7, v8, :cond_5

    invoke-virtual {p1, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v7

    invoke-static {v7}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v7, v8, :cond_5

    .line 178
    const/16 v7, 0x447

    invoke-virtual {p1, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v8

    invoke-static {v8}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Text()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 182
    :cond_5
    const-wide/high16 v8, 0x402c000000000000L    # 14.0

    cmpl-double v7, v2, v8

    if-ltz v7, :cond_6

    invoke-virtual {p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getConversationMode()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    invoke-virtual {p1, p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v7

    invoke-static {v7}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v7, v8, :cond_6

    .line 185
    const/16 v7, 0x458

    invoke-virtual {p1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getTextPreviewSize()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 187
    :cond_6
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 189
    const/16 v7, 0x445

    invoke-virtual {p3, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    const/16 v8, 0x446

    const-string v9, "4"

    invoke-virtual {v7, v8, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 190
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 194
    :cond_7
    const-wide v8, 0x402c333333333333L    # 14.1

    cmpl-double v7, v2, v8

    if-ltz v7, :cond_8

    .line 195
    const/16 v7, 0x605

    const-string v8, "1"

    invoke-virtual {p3, v7, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 198
    :cond_8
    invoke-virtual {p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-object v6, p3

    .line 199
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v6    # "s":Lcom/android/exchange/adapter/Serializer;
    goto/16 :goto_0

    .line 169
    .end local v6    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_9
    const/16 v7, 0x445

    invoke-virtual {p3, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_1

    .line 172
    :cond_a
    const/16 v7, 0x445

    invoke-virtual {p3, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_1
.end method

.method private static appendSearchQuery(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "searchRequest"    # Lcom/android/exchange/SearchRequest;
    .param p3, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryClass()Ljava/lang/String;

    move-result-object v14

    .line 59
    .local v14, "queryClass":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryText()Ljava/lang/String;

    move-result-object v20

    .line 60
    .local v20, "queryText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryFreeText()Ljava/lang/String;

    move-result-object v17

    .line 61
    .local v17, "queryFreeText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryCollectionIds()[J

    move-result-object v15

    .line 62
    .local v15, "queryCollectionIds":[J
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryGreaterThan()Ljava/lang/String;

    move-result-object v18

    .line 63
    .local v18, "queryGreaterThan":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryLessThan()Ljava/lang/String;

    move-result-object v19

    .line 65
    .local v19, "queryLessThan":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/SearchRequest;->getQueryConvIdText()Ljava/lang/String;

    move-result-object v16

    .line 67
    .local v16, "queryConvId":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v12

    .line 69
    .local v12, "protocolVersion":D
    const-wide/high16 v24, 0x4004000000000000L    # 2.5

    cmpl-double v23, v12, v24

    if-nez v23, :cond_1

    .line 70
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_0

    move-object/from16 v21, p3

    .line 132
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .local v21, "s":Lcom/android/exchange/adapter/Serializer;
    :goto_0
    return-object v21

    .line 73
    .end local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_0
    const/16 v23, 0x3c9

    move/from16 v0, v23

    move-object/from16 v1, v20

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    move-object/from16 v21, p3

    .line 74
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    goto :goto_0

    .line 75
    .end local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_1
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    if-eqz v15, :cond_2

    array-length v0, v15

    move/from16 v23, v0

    if-gtz v23, :cond_3

    :cond_2
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_3

    move-object/from16 v21, p3

    .line 78
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    goto :goto_0

    .line 81
    .end local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_3
    const/16 v23, 0x3c9

    move-object/from16 v0, p3

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v23

    const/16 v24, 0x3d3

    invoke-virtual/range {v23 .. v24}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 85
    if-eqz v15, :cond_5

    .line 86
    move-object v4, v15

    .local v4, "arr$":[J
    array-length v10, v4

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v10, :cond_5

    aget-wide v6, v4, v9

    .line 87
    .local v6, "collectionId":J
    const-string v23, "EasSyncService"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "fzhang request QueryCollectionId"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v11

    .line 92
    .local v11, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v11, :cond_4

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_4

    if-nez v16, :cond_4

    .line 93
    const/16 v23, 0x12

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v24, v0

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 86
    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 98
    .end local v4    # "arr$":[J
    .end local v6    # "collectionId":J
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_5
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 99
    const/16 v23, 0x3d5

    move/from16 v0, v23

    move-object/from16 v1, v17

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendData(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 101
    :cond_6
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_7

    .line 102
    const/16 v23, 0x3db

    move/from16 v0, v23

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendDate(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 104
    :cond_7
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_8

    .line 105
    const/16 v23, 0x3da

    move/from16 v0, v23

    move-object/from16 v1, v19

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendDate(ILjava/lang/String;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 108
    :cond_8
    if-eqz v16, :cond_d

    .line 109
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v23

    if-nez v23, :cond_9

    .line 110
    new-instance v23, Ljava/lang/IllegalArgumentException;

    const-string v24, "Byte array to encript cannot be null or zero length"

    invoke-direct/range {v23 .. v24}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 113
    :cond_9
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    move/from16 v0, v23

    new-array v5, v0, [B

    .line 114
    .local v5, "convId":[B
    const/4 v8, 0x0

    .line 115
    .local v8, "i":I
    const/16 v22, 0x0

    .line 116
    .local v22, "temp":I
    :goto_2
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    if-ge v8, v0, :cond_c

    .line 117
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v23

    const/16 v24, 0x10

    invoke-static/range {v23 .. v24}, Ljava/lang/Character;->digit(CI)I

    move-result v23

    shl-int/lit8 v23, v23, 0x4

    add-int/lit8 v24, v8, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x10

    invoke-static/range {v24 .. v25}, Ljava/lang/Character;->digit(CI)I

    move-result v24

    or-int v23, v23, v24

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v22

    .line 119
    aget-byte v23, v5, v22

    const/16 v24, -0x80

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_b

    .line 125
    :cond_a
    :goto_3
    add-int/lit8 v8, v8, 0x2

    .line 126
    add-int/lit8 v22, v22, 0x1

    goto :goto_2

    .line 121
    :cond_b
    aget-byte v23, v5, v22

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0x80

    move/from16 v23, v0

    if-lez v23, :cond_a

    .line 122
    aget-byte v23, v5, v22

    and-int/lit8 v23, v23, 0x7f

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v22

    .line 123
    aget-byte v23, v5, v22

    mul-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    int-to-byte v0, v0

    move/from16 v23, v0

    aput-byte v23, v5, v22

    goto :goto_3

    .line 128
    :cond_c
    const/16 v23, 0x3e0

    move/from16 v0, v23

    move-object/from16 v1, p3

    invoke-static {v0, v5, v1}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendOpaqueData(I[BLcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object p3

    .line 130
    .end local v5    # "convId":[B
    .end local v8    # "i":I
    .end local v22    # "temp":I
    :cond_d
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-object/from16 v21, p3

    .line 132
    .end local p3    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v21    # "s":Lcom/android/exchange/adapter/Serializer;
    goto/16 :goto_0
.end method

.method public static buildEasDocSearchRequest(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;)Lcom/android/exchange/adapter/Serializer;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "searchRequest"    # Lcom/android/exchange/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 37
    .local v0, "protocolVersion":D
    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpg-double v5, v0, v6

    if-gez v5, :cond_0

    .line 38
    const-string v5, "EasSyncService"

    const-string v6, "document search can not be supported"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getQueryFreeText()Ljava/lang/String;

    move-result-object v2

    .line 41
    .local v2, "queryFreeText":Ljava/lang/String;
    new-instance v3, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v3}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 42
    .local v3, "s":Lcom/android/exchange/adapter/Serializer;
    const-string v4, "DocumentLibrary"

    .line 43
    .local v4, "storeName":Ljava/lang/String;
    const/16 v5, 0x3c5

    invoke-virtual {v3, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x3c7

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x3c8

    invoke-virtual {v5, v6, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x3c9

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 45
    const/16 v5, 0x3d1

    invoke-virtual {v3, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 46
    const/16 v5, 0x4c5

    invoke-virtual {v3, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x3d2

    invoke-virtual {v5, v6, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 47
    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 48
    const/16 v5, 0x3ca

    invoke-virtual {v3, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x3cb

    const-string v7, "0-999"

    invoke-virtual {v5, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 49
    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 50
    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 51
    return-object v3
.end method

.method public static buildEasEmailSearchRequest(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;)Lcom/android/exchange/adapter/Serializer;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "searchRequest"    # Lcom/android/exchange/SearchRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 20
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-virtual {p2}, Lcom/android/exchange/SearchRequest;->getStoreName()Ljava/lang/String;

    move-result-object v1

    .line 21
    .local v1, "storeName":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 22
    new-instance v2, Ljava/io/IOException;

    const v3, 0x7f060016

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 25
    :cond_0
    const/16 v2, 0x3c5

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x3c7

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x3c8

    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 26
    invoke-static {p0, p1, p2, v0}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendSearchQuery(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 27
    invoke-static {p0, p1, p2, v0}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->appendSearchOptions(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 29
    return-object v0
.end method
