.class public Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "EmailSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/EmailSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EasEmailSyncParser"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;
    }
.end annotation


# instance fields
.field changedEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;",
            ">;"
        }
    .end annotation
.end field

.field deletedEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field fetchedEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field private final mMailboxIdAsString:Ljava/lang/String;

.field newEmails:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field newResponseAdds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field subCommitIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/EmailSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/EmailSyncAdapter;Z)V
    .locals 2
    .param p2, "parse"    # Lcom/android/exchange/adapter/Parser;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p4, "resumeParser"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 875
    iput-object p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    .line 876
    invoke-direct {p0, p2, p3, p4}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V

    .line 856
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newResponseAdds:Ljava/util/ArrayList;

    .line 858
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    .line 860
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->fetchedEmails:Ljava/util/ArrayList;

    .line 862
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletedEmails:Ljava/util/ArrayList;

    .line 864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changedEmails:Ljava/util/ArrayList;

    .line 865
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    .line 877
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailboxIdAsString:Ljava/lang/String;

    .line 878
    const/4 v0, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z
    invoke-static {p1, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$102(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 879
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/EmailSyncAdapter;)V
    .locals 2
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 867
    iput-object p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    .line 868
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 856
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newResponseAdds:Ljava/util/ArrayList;

    .line 858
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    .line 860
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->fetchedEmails:Ljava/util/ArrayList;

    .line 862
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletedEmails:Ljava/util/ArrayList;

    .line 864
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changedEmails:Ljava/util/ArrayList;

    .line 865
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    .line 869
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailboxIdAsString:Ljava/lang/String;

    .line 870
    const/4 v0, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z
    invoke-static {p1, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$102(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 871
    return-void
.end method

.method private addDataClass(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 4
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 923
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 924
    .local v0, "messageClass":Ljava/lang/String;
    const-string v2, "IPM.Schedule.Meeting.Request"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 925
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 934
    :cond_0
    :goto_0
    invoke-static {v0}, Lcom/android/exchange/adapter/ParserUtility;->decodeMsgClass(Ljava/lang/String;)I

    move-result v1

    .line 935
    .local v1, "msgClass":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 936
    iput-boolean v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    .line 941
    :cond_1
    :goto_1
    return-void

    .line 926
    .end local v1    # "msgClass":I
    :cond_2
    const-string v2, "IPM.Schedule.Meeting.Canceled"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 927
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    goto :goto_0

    .line 928
    :cond_3
    const-string v2, "IPM.Note.Microsoft.Voicemail"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "IPM.Note.RPMSG.Microsoft.Voicemail"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "IPM.Note.Microsoft.Missed.Voice"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 931
    :cond_4
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_0

    .line 937
    .restart local v1    # "msgClass":I
    :cond_5
    if-ne v1, v3, :cond_1

    .line 938
    iput-boolean v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    goto :goto_1
.end method

.method private addDataConversationID(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 13
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 946
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueOpaque()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->crypt([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationId:Ljava/lang/String;

    .line 953
    iget-object v6, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 956
    .local v6, "acc":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailMessageDiffEnabled()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 957
    const/4 v11, 0x0

    .line 959
    .local v11, "messageDiffsFlagForNewEmail":Z
    const/4 v10, 0x0

    .line 961
    .local v10, "messageConversationIdCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "conversationId"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 965
    if-eqz v10, :cond_0

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 967
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 968
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 969
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 974
    .local v9, "mConvId":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 975
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 976
    const/4 v11, 0x1

    .line 985
    .end local v9    # "mConvId":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 986
    .local v12, "tmpMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-object v9, v12, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationId:Ljava/lang/String;

    .line 987
    .restart local v9    # "mConvId":Ljava/lang/String;
    if-eqz v9, :cond_1

    .line 988
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 989
    const/4 v11, 0x1

    .line 995
    .end local v9    # "mConvId":Ljava/lang/String;
    .end local v12    # "tmpMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_2
    if-nez v11, :cond_3

    .line 996
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEstimatedDataSize:I

    iget v1, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailSize:I

    invoke-static {v1}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(I)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Text()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-le v0, v1, :cond_6

    .line 999
    const/4 v0, 0x1

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1000
    const/4 v0, 0x2

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010
    :cond_3
    :goto_1
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1011
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1015
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "messageConversationIdCursor":Landroid/database/Cursor;
    .end local v11    # "messageDiffsFlagForNewEmail":Z
    :cond_4
    :goto_2
    return-void

    .line 981
    .restart local v9    # "mConvId":Ljava/lang/String;
    .restart local v10    # "messageConversationIdCursor":Landroid/database/Cursor;
    .restart local v11    # "messageDiffsFlagForNewEmail":Z
    :cond_5
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1007
    .end local v9    # "mConvId":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 1008
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1010
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1011
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1002
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_6
    const/4 v0, 0x0

    :try_start_3
    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1003
    const/4 v0, 0x1

    iput v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1010
    .end local v8    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v0

    if-eqz v10, :cond_7

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1011
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0
.end method

.method private addDataFlag(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 4
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 904
    const/4 v0, 0x0

    .line 906
    .local v0, "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    .end local v0    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;-><init>()V

    .line 909
    .restart local v0    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->flagParser(Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;)Ljava/lang/Boolean;

    .line 911
    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->MsgId:J

    .line 912
    iget-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->MsgSyncServerId:Ljava/lang/String;

    .line 913
    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFollowupFlag:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    .line 914
    iget-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_ACTIVE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v1, v2, :cond_0

    .line 915
    const/4 v1, 0x2

    iput v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    .line 920
    :goto_0
    return-void

    .line 916
    :cond_0
    iget-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_COMPLETE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v1, v2, :cond_1

    .line 917
    const/4 v1, 0x1

    iput v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    goto :goto_0

    .line 919
    :cond_1
    const/4 v1, 0x0

    iput v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    goto :goto_0
.end method

.method private addDataFrom(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 4
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 884
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 886
    .local v1, "tempSting":Ljava/lang/String;
    iget v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1

    .line 887
    iput-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    iput-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 897
    :goto_0
    iget-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 898
    invoke-static {v1}, Lcom/android/emailcommon/mail/Address;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    .line 901
    :cond_0
    return-void

    .line 890
    :cond_1
    invoke-static {v1}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    .line 891
    .local v0, "froms":[Lcom/android/emailcommon/mail/Address;
    if-eqz v0, :cond_2

    array-length v2, v0

    if-lez v2, :cond_2

    .line 892
    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-virtual {v2}, Lcom/android/emailcommon/mail/Address;->toFriendly()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 894
    :cond_2
    invoke-static {v0}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    goto :goto_0
.end method

.method private addParser()Lcom/android/emailcommon/provider/EmailContent$Message;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1429
    new-instance v2, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v2}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 1430
    .local v2, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    .line 1431
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$300(Lcom/android/exchange/adapter/EmailSyncAdapter;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 1432
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$300(Lcom/android/exchange/adapter/EmailSyncAdapter;)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 1436
    :goto_0
    iput v10, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 1440
    iput v11, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1442
    const-string v5, "eas"

    iput-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountSchema:Ljava/lang/String;

    .line 1443
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    iput v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxType:I

    .line 1447
    const/4 v3, 0x1

    .line 1449
    .local v3, "status":I
    :goto_1
    const/4 v5, 0x7

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    .line 1450
    iget v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v5, :sswitch_data_0

    .line 1477
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_1

    .line 1434
    .end local v3    # "status":I
    :cond_0
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    goto :goto_0

    .line 1452
    .restart local v3    # "status":I
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    goto :goto_1

    .line 1456
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v3

    .line 1457
    goto :goto_1

    .line 1460
    :sswitch_2
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addData(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_1

    .line 1464
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1465
    .local v4, "syncClass":Ljava/lang/String;
    const-string v5, "SMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1466
    iget v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit16 v5, v5, 0x100

    iput v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_1

    .line 1468
    :cond_1
    iget v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit8 v5, v5, 0x0

    iput v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    goto :goto_1

    .line 1473
    .end local v4    # "syncClass":Ljava/lang/String;
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mClientId:Ljava/lang/String;

    goto :goto_1

    .line 1482
    :cond_2
    if-eq v3, v10, :cond_3

    .line 1484
    new-instance v5, Lcom/android/exchange/CommandStatusException;

    iget-object v6, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-direct {v5, v3, v6}, Lcom/android/exchange/CommandStatusException;-><init>(ILjava/lang/String;)V

    throw v5

    .line 1491
    :cond_3
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_5

    iget v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    const/16 v6, 0x100

    if-eq v5, v6, :cond_5

    .line 1492
    const-string v5, "EmailSyncAdapter"

    const-string v6, "Synced message is not a SMS message in outbox, so we return before adding"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1494
    const/4 v2, 0x0

    .line 1519
    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_4
    return-object v2

    .line 1501
    .restart local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_5
    iget v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_6

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x402c333333333333L    # 14.1

    cmpl-double v5, v6, v8

    if-gez v5, :cond_7

    :cond_6
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    cmpl-double v5, v6, v8

    if-nez v5, :cond_4

    .line 1506
    :cond_7
    iput-boolean v11, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 1508
    iget-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 1509
    iget-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    iget-object v6, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-direct {p0, v5, v6}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->resetInlineTagByHTMLBody(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 1512
    :cond_8
    iget-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-eqz v5, :cond_4

    .line 1513
    iget-object v5, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_9
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1514
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    if-nez v5, :cond_9

    .line 1515
    iput-boolean v10, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    goto :goto_2

    .line 1450
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_4
        0xd -> :sswitch_0
        0xe -> :sswitch_1
        0x10 -> :sswitch_3
        0x1d -> :sswitch_2
    .end sparse-switch
.end method

.method private attachmentParser(Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 13
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    const/4 v12, 0x1

    .line 2144
    const/4 v2, 0x0

    .line 2145
    .local v2, "fileName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 2146
    .local v4, "length":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2148
    .local v5, "location":Ljava/lang/String;
    const/4 v1, 0x0

    .line 2149
    .local v1, "contentId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 2152
    .local v3, "isInline":I
    const/4 v8, 0x0

    .line 2153
    .local v8, "umAttOrder":I
    const/4 v7, 0x0

    .line 2155
    .local v7, "umAttDuration":I
    :cond_0
    :goto_0
    const/16 v9, 0x85

    invoke-virtual {p0, v9}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v9

    const/4 v10, 0x3

    if-eq v9, v10, :cond_2

    .line 2156
    iget v9, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v9, :sswitch_data_0

    .line 2191
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2159
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 2160
    goto :goto_0

    .line 2164
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 2165
    goto :goto_0

    .line 2169
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 2170
    goto :goto_0

    .line 2173
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 2174
    goto :goto_0

    .line 2177
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v8

    .line 2178
    goto :goto_0

    .line 2181
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v7

    .line 2182
    goto :goto_0

    .line 2185
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 2186
    .local v6, "tmp":Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "true"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2187
    :cond_1
    const/4 v3, 0x1

    goto :goto_0

    .line 2195
    .end local v6    # "tmp":Ljava/lang/String;
    :cond_2
    const-string v9, "Attmt"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IsInline Value:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    if-eqz v2, :cond_8

    if-eqz v4, :cond_8

    if-eqz v5, :cond_8

    .line 2200
    const-string v9, ".p7m"

    invoke-virtual {v2, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2201
    const-string v9, "/"

    const-string v10, ""

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2205
    :cond_3
    const-string v9, "S/MIME Encrypted Message"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, ".p7m"

    invoke-virtual {v5, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2206
    const-string v2, "smime.p7m"

    .line 2208
    :cond_4
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 2210
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 2211
    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    .line 2212
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 2213
    const-string v9, "base64"

    iput-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mEncoding:Ljava/lang/String;

    .line 2214
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getMimeTypeFromFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 2215
    iget-object v9, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v9, v9, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    .line 2219
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 2220
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2223
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3c

    if-ne v9, v10, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3e

    if-ne v9, v10, :cond_5

    .line 2224
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2226
    :cond_5
    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 2229
    :cond_6
    iput v7, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mVoiceMailAttDuration:I

    .line 2230
    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mVoiceMailAttOrder:I

    .line 2231
    iput v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 2233
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2237
    iget v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2238
    :cond_7
    iput-boolean v12, p2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 2241
    .end local v0    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_8
    return-void

    .line 2156
    :sswitch_data_0
    .sparse-switch
        0x87 -> :sswitch_1
        0x88 -> :sswitch_2
        0x90 -> :sswitch_0
        0x44c -> :sswitch_2
        0x450 -> :sswitch_0
        0x451 -> :sswitch_1
        0x453 -> :sswitch_3
        0x455 -> :sswitch_6
        0x587 -> :sswitch_5
        0x588 -> :sswitch_4
    .end sparse-switch
.end method

.method private attachmentsParser(Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 2
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2129
    .local p1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    :goto_0
    const/16 v0, 0x86

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 2130
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    .line 2137
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2134
    :sswitch_0
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->attachmentParser(Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 2140
    :cond_0
    return-void

    .line 2130
    nop

    :sswitch_data_0
    .sparse-switch
        0x85 -> :sswitch_0
        0x44f -> :sswitch_0
    .end sparse-switch
.end method

.method private bodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V
    .locals 13
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1926
    .local p2, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    const-string v1, "1"

    .line 1927
    .local v1, "bodyType":Ljava/lang/String;
    const-string v0, ""

    .line 1928
    .local v0, "body":Ljava/lang/String;
    const/4 v5, 0x0

    .line 1930
    .local v5, "mimeBodySavedToFile":Z
    const/4 v7, 0x0

    .line 1932
    .local v7, "preview":Ljava/lang/String;
    :goto_0
    const/16 v10, 0x8c

    invoke-virtual {p0, v10}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_5

    .line 1933
    iget v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v10, :sswitch_data_0

    .line 1993
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1937
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v10

    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEstimatedDataSize:I

    goto :goto_0

    .line 1941
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1942
    goto :goto_0

    .line 1945
    :sswitch_2
    const-string v10, "4"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1946
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v11}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getUniqueTempFileName()Ljava/lang/String;

    move-result-object v11

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$202(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 1947
    const/4 v10, 0x1

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    iget-object v12, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v12}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v10, v11, v12}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z

    .line 1948
    const/4 v5, 0x1

    goto :goto_0

    .line 1950
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1952
    goto :goto_0

    .line 1959
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 1960
    .local v8, "trunc":Ljava/lang/String;
    const-string v10, "EmailSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "BASE_TRUNCATED = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1962
    const-string v10, "true"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1963
    const/4 v10, 0x1

    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1981
    :goto_1
    iget v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    const/4 v10, 0x2

    :goto_2
    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 1984
    const-string v10, "EmailSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "msg.mFlagTruncated = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1964
    :cond_1
    const-string v10, "false"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1965
    const/4 v10, 0x0

    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    goto :goto_1

    .line 1970
    :cond_2
    :try_start_0
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1971
    .local v9, "truncated":I
    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    const/4 v10, 0x1

    :goto_3
    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1973
    .end local v9    # "truncated":I
    :catch_0
    move-exception v6

    .line 1976
    .local v6, "ne":Ljava/lang/NumberFormatException;
    const/4 v10, 0x0

    iput v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1977
    invoke-virtual {v6}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 1971
    .end local v6    # "ne":Ljava/lang/NumberFormatException;
    .restart local v9    # "truncated":I
    :cond_3
    const/4 v10, 0x0

    goto :goto_3

    .line 1981
    .end local v9    # "truncated":I
    :cond_4
    const/4 v10, 0x1

    goto :goto_2

    .line 1988
    .end local v8    # "trunc":Ljava/lang/String;
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 1989
    const-string v10, "PREVIEW"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Preview:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1997
    :cond_5
    if-eqz v0, :cond_6

    const-string v10, ""

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    :cond_6
    if-eqz v7, :cond_7

    .line 1998
    move-object v0, v7

    .line 2002
    :cond_7
    const-string v10, "EmailSyncAdapter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "bodyType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2004
    const-string v10, "2"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2005
    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 2027
    :goto_4
    :try_start_1
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-nez v10, :cond_8

    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    if-eqz v10, :cond_8

    .line 2028
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    invoke-static {v10}, Lcom/android/emailcommon/mail/Snippet;->fromHtmlText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2032
    :cond_8
    :goto_5
    return-void

    .line 2006
    :cond_9
    const-string v10, "4"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2007
    if-eqz v5, :cond_b

    .line 2008
    const-string v10, "EmailSyncAdapter"

    const-string v11, "Call MIME parser for body saved to file"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2009
    const/4 v3, 0x0

    .line 2011
    .local v3, "is":Ljava/io/InputStream;
    :try_start_2
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getTempMIMEDataPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v4, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .end local v3    # "is":Ljava/io/InputStream;
    .local v4, "is":Ljava/io/InputStream;
    move-object v3, v4

    .line 2015
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :goto_6
    if-eqz v3, :cond_a

    .line 2016
    invoke-direct {p0, p1, v3, p2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mimeBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/io/InputStream;Ljava/util/ArrayList;)V

    goto :goto_4

    .line 2012
    :catch_1
    move-exception v2

    .line 2013
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "EmailSyncAdapter"

    invoke-static {v10, v2}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6

    .line 2018
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_a
    const-string v10, "EmailSyncAdapter"

    const-string v11, "Temporary MIME file was missed!"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 2020
    .end local v3    # "is":Ljava/io/InputStream;
    :cond_b
    const-string v10, "EmailSyncAdapter"

    const-string v11, "MIME body is NULL!"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 2023
    :cond_c
    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto :goto_4

    .line 2030
    :catch_2
    move-exception v10

    goto :goto_5

    .line 1933
    :sswitch_data_0
    .sparse-switch
        0x446 -> :sswitch_1
        0x44b -> :sswitch_2
        0x44c -> :sswitch_0
        0x44d -> :sswitch_3
        0x458 -> :sswitch_4
    .end sparse-switch
.end method

.method private changeApplicationDataParser(Ljava/util/ArrayList;Ljava/lang/Boolean;Ljava/lang/Boolean;J)V
    .locals 28
    .param p2, "oldRead"    # Ljava/lang/Boolean;
    .param p3, "oldFlag"    # Ljava/lang/Boolean;
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "J)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2381
    .local p1, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;>;"
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    .line 2382
    .local v23, "truncated":Ljava/lang/Boolean;
    const/4 v6, 0x0

    .line 2383
    .local v6, "read":Ljava/lang/Boolean;
    const/4 v7, 0x0

    .line 2384
    .local v7, "flag":Ljava/lang/Boolean;
    const/4 v8, 0x0

    .line 2385
    .local v8, "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    const/16 v17, 0x0

    .line 2388
    .local v17, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/16 v19, 0x0

    .line 2389
    .local v19, "nonDraftChange":Z
    const/4 v15, 0x0

    .line 2392
    .local v15, "isIRMreceived":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p4

    invoke-static {v2, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v17

    .line 2396
    const/4 v9, -0x1

    .line 2397
    .local v9, "lastVerb":I
    const-wide/16 v10, -0x1

    .line 2399
    .local v10, "lastVerbTime":J
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2401
    .local v12, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    :cond_0
    :goto_0
    const/16 v2, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_15

    .line 2402
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v2, :sswitch_data_0

    .line 2572
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2404
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 2405
    goto :goto_0

    .line 2404
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 2407
    :sswitch_1
    new-instance v8, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    .end local v8    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    invoke-direct {v8}, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;-><init>()V

    .line 2408
    .restart local v8    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->flagParser(Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;)Ljava/lang/Boolean;

    move-result-object v7

    .line 2409
    move-wide/from16 v0, p4

    iput-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->MsgId:J

    goto :goto_0

    .line 2413
    :sswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v9

    .line 2414
    goto :goto_0

    .line 2416
    :sswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v10

    .line 2417
    goto :goto_0

    .line 2421
    :sswitch_4
    const/16 v19, 0x1

    .line 2423
    if-eqz v17, :cond_2

    .line 2424
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v12, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->attachmentsParser(Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 2426
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2431
    :sswitch_5
    const/16 v19, 0x1

    .line 2435
    if-eqz v17, :cond_3

    .line 2436
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto :goto_0

    .line 2438
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2442
    :sswitch_6
    const/16 v19, 0x1

    .line 2443
    if-eqz v17, :cond_5

    .line 2444
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v13

    .line 2445
    .local v13, "froms":[Lcom/android/emailcommon/mail/Address;
    if-eqz v13, :cond_4

    array-length v2, v13

    if-lez v2, :cond_4

    .line 2446
    const/4 v2, 0x0

    aget-object v2, v13, v2

    invoke-virtual {v2}, Lcom/android/emailcommon/mail/Address;->toFriendly()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 2448
    :cond_4
    invoke-static {v13}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    goto/16 :goto_0

    .line 2450
    .end local v13    # "froms":[Lcom/android/emailcommon/mail/Address;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2453
    :sswitch_7
    const/16 v19, 0x1

    .line 2454
    if-eqz v17, :cond_6

    .line 2455
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    invoke-static {v2}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    goto/16 :goto_0

    .line 2457
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2460
    :sswitch_8
    if-eqz v17, :cond_8

    .line 2461
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v21

    .line 2462
    .local v21, "tempSubject":Ljava/lang/String;
    if-eqz v21, :cond_7

    .line 2463
    const/16 v2, 0xa

    const/16 v3, 0x20

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v21

    .line 2465
    :cond_7
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_0

    .line 2468
    .end local v21    # "tempSubject":Ljava/lang/String;
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2471
    :sswitch_9
    const/16 v19, 0x1

    .line 2475
    if-eqz v17, :cond_9

    .line 2476
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    goto/16 :goto_0

    .line 2478
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2481
    :sswitch_a
    if-eqz v17, :cond_b

    .line 2482
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v23

    goto/16 :goto_0

    :cond_a
    const/4 v2, 0x0

    goto :goto_2

    .line 2484
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2491
    :sswitch_b
    if-eqz v17, :cond_f

    .line 2492
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2503
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    .line 2504
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Partially loaded: "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V

    .line 2505
    const/4 v2, 0x2

    move-object/from16 v0, v17

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 2506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v3, 0x1

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$602(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    goto/16 :goto_0

    .line 2508
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v2, :cond_e

    .line 2509
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4004000000000000L    # 2.5

    cmpl-double v2, v2, v4

    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v2

    if-eqz v2, :cond_0

    .line 2511
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getUniqueTempFileName()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$202(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 2512
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z

    .line 2513
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getTempMIMEDataPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v14, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 2514
    .local v14, "is":Ljava/io/InputStream;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v14, v12}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mimeBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/io/InputStream;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 2517
    .end local v14    # "is":Ljava/io/InputStream;
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2521
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2525
    :sswitch_c
    if-eqz v17, :cond_10

    .line 2526
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v22

    .line 2527
    .local v22, "text":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto/16 :goto_0

    .line 2529
    .end local v22    # "text":Ljava/lang/String;
    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2532
    :sswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    goto/16 :goto_0

    .line 2535
    :sswitch_e
    const/16 v19, 0x1

    .line 2536
    if-eqz v17, :cond_11

    .line 2537
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v12}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->bodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 2539
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2543
    :sswitch_f
    if-eqz v17, :cond_14

    .line 2544
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v16

    .line 2545
    .local v16, "messageClass":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/android/exchange/adapter/ParserUtility;->decodeMsgClass(Ljava/lang/String;)I

    move-result v18

    .line 2546
    .local v18, "msgClass":I
    const/4 v2, 0x2

    move/from16 v0, v18

    if-ne v0, v2, :cond_12

    .line 2547
    const/4 v2, 0x1

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    goto/16 :goto_0

    .line 2548
    :cond_12
    const/4 v2, 0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_13

    .line 2549
    const/4 v2, 0x1

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    goto/16 :goto_0

    .line 2551
    :cond_13
    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    .line 2552
    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    goto/16 :goto_0

    .line 2555
    .end local v16    # "messageClass":Ljava/lang/String;
    .end local v18    # "msgClass":I
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2561
    :sswitch_10
    const/4 v15, 0x1

    .line 2562
    if-eqz v17, :cond_0

    .line 2564
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->parseLicense(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/exchange/adapter/AbstractSyncParser;)V

    .line 2565
    sget-boolean v2, Lcom/android/exchange/irm/IRMLicenseParserUtility;->mRenewLicense:Z

    if-eqz v2, :cond_0

    .line 2566
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v2, v3, v4}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->renewLicense(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2578
    :cond_15
    if-nez v15, :cond_16

    if-eqz v19, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p4

    invoke-static {v2, v0, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->getIRMLicenseFlag(Landroid/content/Context;J)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_16

    .line 2581
    const/16 v20, 0x0

    .line 2582
    .local v20, "nullStr":Ljava/lang/String;
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 2583
    .local v24, "values":Landroid/content/ContentValues;
    const-string v2, "IRMContentExpiryDate"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2584
    const-string v2, "IRMContentOwner"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentOwner:Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2585
    const-string v2, "IRMLicenseFlag"

    const/4 v3, -0x1

    move-object/from16 v0, v17

    iput v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2586
    const-string v2, "IRMOwner"

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iput v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMOwner:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2587
    const-string v2, "IRMTemplateId"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateId:Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2588
    const-string v2, "IRMTemplateName"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateName:Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2589
    const-string v2, "IRMTemplateDescription"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateDescription:Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590
    const-string v2, "IRMRemovalFlag"

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iput v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMRemovalFlag:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2592
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2599
    .end local v20    # "nullStr":Ljava/lang/String;
    .end local v24    # "values":Landroid/content/ContentValues;
    :cond_16
    if-eqz v17, :cond_19

    const/4 v2, 0x1

    move/from16 v0, v19

    if-ne v0, v2, :cond_19

    .line 2600
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    move-object/from16 v0, v17

    iget-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_17

    .line 2601
    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 2605
    :cond_17
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    if-eqz v2, :cond_1d

    .line 2606
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/emailcommon/mail/Snippet;->fromHtmlText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    .line 2611
    :cond_18
    :goto_3
    move-object/from16 v0, v17

    iput-object v12, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v2, v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->draftChanges:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2616
    :cond_19
    const-wide/16 v2, -0x1

    cmp-long v2, p4, v2

    if-eqz v2, :cond_1c

    .line 2617
    if-eqz v6, :cond_1a

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    :cond_1a
    if-nez v8, :cond_1b

    if-lez v9, :cond_1c

    const-wide/16 v2, -0x1

    cmp-long v2, v10, v2

    if-eqz v2, :cond_1c

    .line 2620
    :cond_1b
    new-instance v2, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;

    move-object/from16 v3, p0

    move-wide/from16 v4, p4

    invoke-direct/range {v2 .. v11}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;-><init>(Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;JLjava/lang/Boolean;Ljava/lang/Boolean;Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;IJ)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2625
    :cond_1c
    return-void

    .line 2607
    :cond_1d
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v2, :cond_18

    .line 2608
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/emailcommon/mail/Snippet;->fromPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    goto :goto_3

    .line 2402
    nop

    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_4
        0x8c -> :sswitch_c
        0x92 -> :sswitch_9
        0x93 -> :sswitch_f
        0x94 -> :sswitch_8
        0x95 -> :sswitch_0
        0x96 -> :sswitch_5
        0x97 -> :sswitch_7
        0x98 -> :sswitch_6
        0xb6 -> :sswitch_b
        0xb7 -> :sswitch_a
        0xba -> :sswitch_1
        0x446 -> :sswitch_d
        0x44a -> :sswitch_e
        0x44e -> :sswitch_4
        0x58b -> :sswitch_2
        0x58c -> :sswitch_3
        0x608 -> :sswitch_10
    .end sparse-switch
.end method

.method private fetchDataParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
    .locals 29
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2990
    :cond_0
    :goto_0
    const/16 v26, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v26

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_15

    .line 2991
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    move/from16 v26, v0

    sparse-switch v26, :sswitch_data_0

    .line 3190
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2995
    :sswitch_0
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 2996
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 2999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    instance-of v0, v0, Lcom/android/exchange/utility/MessageBodyRefresher;

    move/from16 v26, v0

    if-eqz v26, :cond_0

    .line 3001
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1

    .line 3002
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 3005
    :cond_1
    const-string v26, "EmailSyncAdapter"

    const-string v27, "svc(Adapter.mService) is instanceof MessageBodyRefresher"

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v22, v0

    check-cast v22, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 3007
    .local v22, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/utility/MessageBodyRefresher;->getIsSaveMode()Z

    move-result v26

    if-eqz v26, :cond_0

    .line 3008
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/exchange/utility/MessageBodyRefresher;->saveAsText(Ljava/lang/String;)V

    goto :goto_0

    .line 3020
    .end local v22    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :sswitch_1
    const-string v26, "TAG"

    const-string v27, "inside Tags.EMAIL_MIME_DATA"

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getUniqueTempFileName()Ljava/lang/String;

    move-result-object v27

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static/range {v26 .. v27}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$202(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 3023
    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v28, v0

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    move-object/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z

    .line 3025
    new-instance v16, Ljava/io/FileInputStream;

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getTempMIMEDataPath()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 3026
    .local v16, "is":Ljava/io/InputStream;
    const/16 v19, 0x0

    .line 3029
    .local v19, "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    :try_start_0
    new-instance v20, Lcom/android/emailcommon/internet/MimeMessage;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v19    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .local v20, "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    move-object/from16 v19, v20

    .line 3034
    .end local v20    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v19    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/InputStream;->close()V

    .line 3038
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 3041
    :try_start_1
    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/internet/MimeMessage;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v23

    .line 3042
    .local v23, "tempBody2":Lcom/android/emailcommon/mail/Body;
    new-instance v17, Lcom/android/emailcommon/internet/MimeBodyPart;

    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>(Lcom/android/emailcommon/mail/Body;Ljava/lang/String;)V

    .line 3045
    .local v17, "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    const/16 v18, 0x0

    .line 3046
    .local v18, "mimeBodyStringText":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    .line 3049
    .local v10, "contentType":Ljava/lang/String;
    const-string v26, "EmailSyncAdapter"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ContentType = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/internet/MimeMessage;->getContentType()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3051
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 3052
    .local v25, "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3054
    .local v5, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-static {v0, v1, v5}, Lcom/android/emailcommon/internet/MimeUtility;->collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3056
    const-string v26, "text/html"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 3058
    const-string v26, "Content-Transfer-Encoding"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/internet/MimeMessage;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 3061
    .local v14, "headerValues":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 3063
    .local v9, "contentEncoding":Ljava/lang/String;
    if-eqz v14, :cond_2

    .line 3064
    const/16 v26, 0x0

    aget-object v9, v14, v26

    .line 3067
    :cond_2
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/emailcommon/mail/Part;

    .line 3068
    .local v24, "viewable":Lcom/android/emailcommon/mail/Part;
    move-object/from16 v0, v24

    invoke-static {v0, v9}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart2(Lcom/android/emailcommon/mail/Part;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 3071
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 3072
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 3182
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v9    # "contentEncoding":Ljava/lang/String;
    .end local v10    # "contentType":Ljava/lang/String;
    .end local v14    # "headerValues":[Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v18    # "mimeBodyStringText":Ljava/lang/String;
    .end local v23    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    .end local v25    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catch_0
    move-exception v21

    .line 3183
    .local v21, "npe":Ljava/lang/NullPointerException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .line 3030
    .end local v21    # "npe":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v13

    .line 3031
    .local v13, "e":Lcom/android/emailcommon/mail/MessagingException;
    invoke-virtual {v13}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V

    goto/16 :goto_1

    .line 3074
    .end local v13    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .restart local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .restart local v9    # "contentEncoding":Ljava/lang/String;
    .restart local v10    # "contentType":Ljava/lang/String;
    .restart local v14    # "headerValues":[Ljava/lang/String;
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v17    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v18    # "mimeBodyStringText":Ljava/lang/String;
    .restart local v23    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .restart local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    .restart local v25    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_3
    :try_start_2
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 3184
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v9    # "contentEncoding":Ljava/lang/String;
    .end local v10    # "contentType":Ljava/lang/String;
    .end local v14    # "headerValues":[Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v17    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v18    # "mimeBodyStringText":Ljava/lang/String;
    .end local v23    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    .end local v25    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catch_2
    move-exception v13

    .line 3185
    .restart local v13    # "e":Lcom/android/emailcommon/mail/MessagingException;
    invoke-virtual {v13}, Lcom/android/emailcommon/mail/MessagingException;->printStackTrace()V

    goto/16 :goto_0

    .line 3077
    .end local v13    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .restart local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .restart local v10    # "contentType":Ljava/lang/String;
    .restart local v17    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v18    # "mimeBodyStringText":Ljava/lang/String;
    .restart local v23    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .restart local v25    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_4
    :try_start_3
    const-string v26, "multipart/"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 3079
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 3081
    .local v8, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    const-string v26, "multipart/alternative"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_5

    const-string v26, "multipart/mixed"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_5

    const-string v26, "multipart/report"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_5

    const-string v26, "multipart/related"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 3086
    :cond_5
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 3087
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 3089
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .restart local v15    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/emailcommon/mail/Part;

    .line 3092
    .restart local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    move-object/from16 v0, v24

    instance-of v0, v0, Lcom/android/emailcommon/internet/MimeMessage;

    move/from16 v26, v0

    if-nez v26, :cond_6

    .line 3096
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v18

    .line 3098
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 3099
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v26, v0

    if-nez v26, :cond_7

    move-object/from16 v26, v18

    :goto_4
    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    goto :goto_3

    :cond_7
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    goto :goto_4

    .line 3101
    :cond_8
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->isPlainTextViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 3102
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v26, v0

    if-nez v26, :cond_9

    move-object/from16 v26, v18

    :goto_5
    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    goto :goto_3

    :cond_9
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    goto :goto_5

    .line 3109
    .end local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    :cond_a
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/emailcommon/mail/Part;

    .line 3110
    .local v7, "attmtPart":Lcom/android/emailcommon/mail/Part;
    new-instance v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v4}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 3112
    .local v4, "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const/16 v26, 0x0

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 3113
    const/16 v26, 0x0

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 3115
    invoke-interface {v7}, Lcom/android/emailcommon/mail/Part;->getDisposition()Ljava/lang/String;

    move-result-object v11

    .line 3116
    .local v11, "disposition":Ljava/lang/String;
    const/4 v12, 0x0

    .line 3121
    .local v12, "dispositionFilename":Ljava/lang/String;
    if-eqz v11, :cond_b

    .line 3122
    const-string v26, "filename"

    move-object/from16 v0, v26

    invoke-static {v11, v0}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 3129
    :cond_b
    if-nez v12, :cond_c

    .line 3130
    const-string v26, "EmailSyncAdapter"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "ATTACHMENT = "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-interface {v7}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3131
    invoke-interface {v7}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v6

    .line 3132
    .local v6, "attmtContentType":Ljava/lang/String;
    const-string v26, "name"

    move-object/from16 v0, v26

    invoke-static {v6, v0}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 3136
    .end local v6    # "attmtContentType":Ljava/lang/String;
    :cond_c
    const-string v26, "EmailSyncAdapter"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "FileName "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3137
    const-string v26, "EmailSyncAdapter"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Content-ID "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    iget-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 3139
    iput-object v12, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 3140
    invoke-interface {v7}, Lcom/android/emailcommon/mail/Part;->getContentId()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 3142
    iget-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_d

    .line 3143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z
    invoke-static/range {v26 .. v27}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1502(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 3145
    :cond_d
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3146
    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    goto/16 :goto_6

    .line 3148
    .end local v4    # "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v7    # "attmtPart":Lcom/android/emailcommon/mail/Part;
    .end local v11    # "disposition":Ljava/lang/String;
    .end local v12    # "dispositionFilename":Ljava/lang/String;
    .end local v15    # "i$":Ljava/util/Iterator;
    :cond_e
    const-string v26, "multipart/signed"

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 3151
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .restart local v15    # "i$":Ljava/util/Iterator;
    :cond_f
    :goto_7
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_13

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/emailcommon/mail/Part;

    .line 3152
    .restart local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v18

    .line 3154
    invoke-static/range {v24 .. v24}, Lcom/android/emailcommon/internet/MimeUtility;->isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 3155
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 3156
    :cond_10
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    goto :goto_7

    .line 3157
    :cond_11
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v26, v0

    if-eqz v26, :cond_12

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 3158
    :cond_12
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    goto :goto_7

    .line 3163
    .end local v24    # "viewable":Lcom/android/emailcommon/mail/Part;
    :cond_13
    new-instance v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v4}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 3165
    .restart local v4    # "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const-string v26, "SMIME Signed Message.p7s"

    move-object/from16 v0, v26

    iput-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 3166
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    iput-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 3167
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    iput-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    .line 3169
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3170
    move-object/from16 v0, p1

    iput-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 3171
    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p1

    iput-boolean v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 3173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z
    invoke-static/range {v26 .. v27}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1402(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    goto/16 :goto_0

    .line 3177
    .end local v4    # "attach":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v8    # "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    .end local v15    # "i$":Ljava/util/Iterator;
    :cond_14
    invoke-static/range {v17 .. v17}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v18

    .line 3178
    move-object/from16 v0, v18

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 3179
    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 3194
    .end local v5    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v10    # "contentType":Ljava/lang/String;
    .end local v16    # "is":Ljava/io/InputStream;
    .end local v17    # "mimeBody":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v18    # "mimeBodyStringText":Ljava/lang/String;
    .end local v19    # "mimeMsg":Lcom/android/emailcommon/internet/MimeMessage;
    .end local v23    # "tempBody2":Lcom/android/emailcommon/mail/Body;
    .end local v25    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :cond_15
    return-void

    .line 2991
    :sswitch_data_0
    .sparse-switch
        0x8c -> :sswitch_0
        0xb6 -> :sswitch_1
    .end sparse-switch
.end method

.method private findContentIdInHTMLBody(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "bodyHtml"    # Ljava/lang/String;
    .param p2, "cid"    # Ljava/lang/String;

    .prologue
    .line 1238
    const-string v3, "<\\s?(?i)img\\s.[^>]+>"

    .line 1241
    .local v3, "imgTagRe":Ljava/lang/String;
    const-string v2, "\\s+(?i)src=\"cid:.[^\"]+\""

    .line 1243
    .local v2, "contentIdRe":Ljava/lang/String;
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 1245
    const-string v8, "<\\s?(?i)img\\s.[^>]+>"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 1246
    .local v7, "pIMG":Ljava/util/regex/Pattern;
    invoke-virtual {v7, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 1250
    .local v5, "mIMG":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1252
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 1253
    .local v1, "_imgTag":Ljava/lang/String;
    const-string v8, "\\s+(?i)src=\"cid:.[^\"]+\""

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 1254
    .local v6, "pCID":Ljava/util/regex/Pattern;
    invoke-virtual {v6, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 1256
    .local v4, "mCID":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1257
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 1259
    .local v0, "_cid":Ljava/lang/String;
    invoke-virtual {v0, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1260
    const/4 v8, 0x1

    .line 1264
    .end local v0    # "_cid":Ljava/lang/String;
    .end local v1    # "_imgTag":Ljava/lang/String;
    .end local v4    # "mCID":Ljava/util/regex/Matcher;
    .end local v5    # "mIMG":Ljava/util/regex/Matcher;
    .end local v6    # "pCID":Ljava/util/regex/Pattern;
    .end local v7    # "pIMG":Ljava/util/regex/Pattern;
    :goto_0
    return v8

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private flagParser(Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;)Ljava/lang/Boolean;
    .locals 8
    .param p1, "flags"    # Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1858
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1860
    .local v0, "state":Ljava/lang/Boolean;
    :goto_0
    const/16 v2, 0xba

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v2

    const/4 v5, 0x3

    if-eq v2, v5, :cond_3

    .line 1861
    iget v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v2, :sswitch_data_0

    .line 1919
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1864
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v1

    .line 1865
    .local v1, "status":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1866
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1867
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_ACTIVE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 1868
    :cond_0
    if-ne v1, v3, :cond_1

    .line 1869
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1870
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_COMPLETE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 1872
    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1873
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_CLEARED:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 1877
    .end local v1    # "status":I
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->FlagType:Ljava/lang/String;

    goto :goto_0

    .line 1880
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1881
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CompleteTime:J

    goto :goto_0

    .line 1884
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1885
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DateCompleted:J

    goto :goto_0

    .line 1888
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1889
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->StartDate:J

    goto :goto_0

    .line 1892
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1893
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DueDate:J

    goto :goto_0

    .line 1896
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1897
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UTCDueDate:J

    goto/16 :goto_0

    .line 1900
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1901
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UTCStartDate:J

    goto/16 :goto_0

    .line 1904
    :sswitch_8
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v2

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->ReminderSet:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    .line 1907
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1908
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->ReminderTime:J

    goto/16 :goto_0

    .line 1911
    :sswitch_a
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    .line 1912
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->OrdinalDate:J

    goto/16 :goto_0

    .line 1915
    :sswitch_b
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->SubOrdinalDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 1922
    :cond_3
    return-object v0

    .line 1861
    nop

    :sswitch_data_0
    .sparse-switch
        0xbb -> :sswitch_0
        0xbd -> :sswitch_1
        0xbe -> :sswitch_2
        0x24b -> :sswitch_3
        0x24c -> :sswitch_5
        0x24d -> :sswitch_6
        0x25b -> :sswitch_8
        0x25c -> :sswitch_9
        0x25e -> :sswitch_4
        0x25f -> :sswitch_7
        0x262 -> :sswitch_a
        0x263 -> :sswitch_b
    .end sparse-switch
.end method

.method private getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "serverId"    # Ljava/lang/String;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    .line 2277
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 2278
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailboxIdAsString:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 2279
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "syncServerId=? and mailboxKey=?"

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v4, v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getTempMIMEDataPath()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2968
    const/4 v1, 0x0

    .line 2969
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 2970
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2973
    :cond_0
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v3, v3, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    instance-of v3, v3, Lcom/android/exchange/utility/MessageBodyRefresher;

    if-eqz v3, :cond_2

    .line 2974
    const-string v3, "EmailSyncAdapter"

    const-string v4, "svc(Adapter.mService) is instanceof MessageBodyRefresher"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2975
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v2, v3, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    check-cast v2, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 2976
    .local v2, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    invoke-virtual {v2}, Lcom/android/exchange/utility/MessageBodyRefresher;->getIsSaveMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2977
    invoke-virtual {v2}, Lcom/android/exchange/utility/MessageBodyRefresher;->getSaveFilePath()Ljava/lang/String;

    move-result-object v0

    .line 2978
    .local v0, "_path":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 2979
    :cond_1
    :goto_0
    const-string v3, "EmailSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "svc(Adapter.mService) will save data at : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    .end local v0    # "_path":Ljava/lang/String;
    .end local v2    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_2
    return-object v1

    .restart local v0    # "_path":Ljava/lang/String;
    .restart local v2    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_3
    move-object v1, v0

    .line 2978
    goto :goto_0
.end method

.method private meetingRequestParser(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 8
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1277
    new-instance v2, Lcom/android/emailcommon/mail/PackedString$Builder;

    invoke-direct {v2}, Lcom/android/emailcommon/mail/PackedString$Builder;-><init>()V

    .line 1278
    .local v2, "packedString":Lcom/android/emailcommon/mail/PackedString$Builder;
    :cond_0
    :goto_0
    const/16 v6, 0xa2

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v6

    const/4 v7, 0x3

    if-eq v6, v7, :cond_1

    .line 1279
    iget v6, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v6, :sswitch_data_0

    .line 1345
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1281
    :sswitch_0
    const-string v6, "DTSTAMP"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1284
    :sswitch_1
    const-string v6, "DTSTART"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1287
    :sswitch_2
    const-string v6, "DTEND"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1290
    :sswitch_3
    const-string v6, "ORGMAIL"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1293
    :sswitch_4
    const-string v6, "LOC"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1296
    :sswitch_5
    const-string v6, "UID"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/exchange/utility/CalendarUtilities;->getUidFromGlobalObjId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1300
    :sswitch_6
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nullParser()V

    goto :goto_0

    .line 1304
    :sswitch_7
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->recurrencesParser()Ljava/lang/String;

    move-result-object v5

    .line 1305
    .local v5, "rrule":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 1306
    const-string v6, "RRULE"

    invoke-virtual {v2, v6, v5}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1312
    .end local v5    # "rrule":Ljava/lang/String;
    :sswitch_8
    const-string v6, "RESPONSE"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1317
    :sswitch_9
    const-string v6, "PROPOSE_NEW_TIME"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1322
    :sswitch_a
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1323
    .local v4, "recurrenceID":Ljava/lang/String;
    const-string v6, "RECURRENCE_ID"

    invoke-virtual {v2, v6, v4}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1329
    .end local v4    # "recurrenceID":Ljava/lang/String;
    :sswitch_b
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1330
    .local v1, "instanceType":Ljava/lang/String;
    const-string v6, "INSTANCE_TYPE"

    invoke-virtual {v2, v6, v1}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1335
    .end local v1    # "instanceType":Ljava/lang/String;
    :sswitch_c
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1336
    .local v0, "allday":Ljava/lang/String;
    const-string v6, "IS_ALLDAY"

    invoke-virtual {v2, v6, v0}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1339
    .end local v0    # "allday":Ljava/lang/String;
    :sswitch_d
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 1340
    .local v3, "privacy":Ljava/lang/String;
    const-string v6, "SENSITIVITY"

    invoke-virtual {v2, v6, v3}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1348
    .end local v3    # "privacy":Ljava/lang/String;
    :cond_1
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1349
    const-string v6, "TITLE"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v2, v6, v7}, Lcom/android/emailcommon/mail/PackedString$Builder;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    :cond_2
    invoke-virtual {v2}, Lcom/android/emailcommon/mail/PackedString$Builder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    .line 1352
    return-void

    .line 1279
    :sswitch_data_0
    .sparse-switch
        0x9a -> :sswitch_c
        0x9b -> :sswitch_6
        0x9d -> :sswitch_0
        0x9e -> :sswitch_2
        0x9f -> :sswitch_b
        0xa1 -> :sswitch_4
        0xa3 -> :sswitch_3
        0xa4 -> :sswitch_a
        0xa6 -> :sswitch_8
        0xa7 -> :sswitch_7
        0xb1 -> :sswitch_1
        0xb2 -> :sswitch_d
        0xb4 -> :sswitch_5
        0xbf -> :sswitch_9
    .end sparse-switch
.end method

.method private mimeBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/io/InputStream;Ljava/util/ArrayList;)V
    .locals 18
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            "Ljava/io/InputStream;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2045
    .local p3, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    :try_start_0
    new-instance v12, Lcom/android/emailcommon/internet/MimeMessage;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V

    .line 2046
    .local v12, "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    if-eqz p2, :cond_0

    .line 2047
    invoke-virtual/range {p2 .. p2}, Ljava/io/InputStream;->close()V

    .line 2053
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 2054
    .local v14, "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2056
    .local v3, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    invoke-static {v12, v14, v3}, Lcom/android/emailcommon/internet/MimeUtility;->collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 2057
    new-instance v13, Lcom/android/emailcommon/provider/EmailContent$Body;

    invoke-direct {v13}, Lcom/android/emailcommon/provider/EmailContent$Body;-><init>()V

    .line 2060
    .local v13, "tempBody":Lcom/android/emailcommon/provider/EmailContent$Body;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const v16, 0x7f06001a

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v15, v13, v0, v14}, Lcom/android/emailcommon/utility/ConversionUtilities;->updateBodyFields(Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Body;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)Z

    .line 2067
    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 2068
    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2069
    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtmlReply:Ljava/lang/String;

    .line 2070
    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTextReply:Ljava/lang/String;

    .line 2071
    iget-object v15, v13, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    move-object/from16 v0, p1

    iput-object v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIntroText:Ljava/lang/String;

    .line 2075
    if-eqz p3, :cond_6

    .line 2076
    const/4 v6, 0x0

    .line 2077
    .local v6, "disposition":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2079
    .local v7, "dispositionFilename":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v4

    .line 2080
    .local v4, "attmtParts":[Ljava/lang/Object;
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v5

    .line 2082
    .local v5, "attmts":[Ljava/lang/Object;
    array-length v15, v5

    new-array v11, v15, [I

    .line 2084
    .local v11, "matchedCidParts":[I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    array-length v15, v5

    if-ge v10, v15, :cond_1

    .line 2085
    const/4 v15, -0x1

    aput v15, v11, v10

    .line 2084
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 2088
    :cond_1
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    array-length v15, v4

    if-ge v9, v15, :cond_6

    .line 2089
    aget-object v2, v4, v9

    check-cast v2, Lcom/android/emailcommon/mail/Part;

    .line 2093
    .local v2, "attachment":Lcom/android/emailcommon/mail/Part;
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 2094
    const/4 v15, 0x0

    move-object/from16 v0, p1

    iput v15, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 2097
    invoke-interface {v2}, Lcom/android/emailcommon/mail/Part;->getDisposition()Ljava/lang/String;

    move-result-object v6

    .line 2098
    if-eqz v6, :cond_2

    .line 2099
    const-string v15, "filename"

    invoke-static {v6, v15}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2106
    :cond_2
    const/4 v10, 0x0

    :goto_2
    array-length v15, v5

    if-ge v10, v15, :cond_5

    .line 2107
    aget v15, v11, v10

    if-ltz v15, :cond_4

    .line 2106
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 2110
    :cond_4
    aget-object v1, v5, v10

    check-cast v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 2111
    .local v1, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v15, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2112
    invoke-interface {v2}, Lcom/android/emailcommon/mail/Part;->getContentId()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 2113
    aput v9, v11, v10
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2088
    .end local v1    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 2124
    .end local v2    # "attachment":Lcom/android/emailcommon/mail/Part;
    .end local v4    # "attmtParts":[Ljava/lang/Object;
    .end local v5    # "attmts":[Ljava/lang/Object;
    .end local v6    # "disposition":Ljava/lang/String;
    .end local v7    # "dispositionFilename":Ljava/lang/String;
    .end local v9    # "i":I
    .end local v10    # "j":I
    .end local v11    # "matchedCidParts":[I
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v16, v0

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v16

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteTempFile(Ljava/lang/String;)V
    invoke-static/range {v15 .. v16}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$400(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)V

    .line 2126
    return-void

    .line 2120
    .end local v3    # "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .end local v12    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .end local v13    # "tempBody":Lcom/android/emailcommon/provider/EmailContent$Body;
    .end local v14    # "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    :catch_0
    move-exception v8

    .line 2122
    .local v8, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_1
    new-instance v15, Ljava/io/IOException;

    invoke-direct {v15, v8}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2124
    .end local v8    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :catchall_0
    move-exception v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v17, v0

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v17

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteTempFile(Ljava/lang/String;)V
    invoke-static/range {v16 .. v17}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$400(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)V

    throw v15
.end method

.method private nullParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1355
    :goto_0
    const/16 v0, 0x9b

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1356
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1358
    :cond_0
    return-void
.end method

.method private recurrencesParser()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1407
    const/4 v0, 0x0

    .line 1408
    .local v0, "rule":Ljava/lang/String;
    :goto_0
    const/16 v1, 0xa7

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 1409
    iget v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 1414
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1411
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->recurrenceParser()Ljava/lang/String;

    move-result-object v0

    .line 1412
    goto :goto_0

    .line 1417
    :cond_0
    return-object v0

    .line 1409
    nop

    :pswitch_data_0
    .packed-switch 0xa8
        :pswitch_0
    .end packed-switch
.end method

.method private resetInlineTagByHTMLBody(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 16
    .param p1, "bodyHtml"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1528
    .local p2, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    if-nez p1, :cond_1

    .line 1529
    const/4 v13, 0x0

    .line 1577
    :cond_0
    :goto_0
    return-object v13

    .line 1531
    :cond_1
    if-nez p2, :cond_2

    .line 1532
    const/4 v13, 0x0

    goto :goto_0

    .line 1535
    :cond_2
    const-string v8, "<\\s?(?i)img\\s.[^>]+>"

    .line 1538
    .local v8, "imgTagRe":Ljava/lang/String;
    const-string v4, "\\s+(?i)src=\"cid:.[^\"]+\""

    .line 1540
    .local v4, "contentIdRe":Ljava/lang/String;
    const-string v15, "<\\s?(?i)img\\s.[^>]+>"

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v12

    .line 1541
    .local v12, "pIMG":Ljava/util/regex/Pattern;
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    .line 1543
    .local v10, "mIMG":Ljava/util/regex/Matcher;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1545
    .local v5, "contentIdSrcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    :goto_1
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 1547
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 1548
    .local v2, "_imgTag":Ljava/lang/String;
    const-string v15, "\\s+(?i)src=\"cid:.[^\"]+\""

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v11

    .line 1549
    .local v11, "pCID":Ljava/util/regex/Pattern;
    invoke-virtual {v11, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v9

    .line 1551
    .local v9, "mCID":Ljava/util/regex/Matcher;
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->find()Z

    move-result v15

    if-eqz v15, :cond_3

    .line 1553
    invoke-virtual {v9}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1557
    .end local v2    # "_imgTag":Ljava/lang/String;
    .end local v9    # "mCID":Ljava/util/regex/Matcher;
    .end local v11    # "pCID":Ljava/util/regex/Pattern;
    :cond_4
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1559
    .local v13, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 1560
    .local v14, "size":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    if-ge v6, v14, :cond_0

    .line 1561
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1564
    .local v3, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    const/4 v15, 0x0

    iput v15, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 1567
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1568
    .local v1, "_cid":Ljava/lang/String;
    if-eqz v1, :cond_5

    iget-object v15, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v15, :cond_5

    .line 1571
    iget-object v15, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-virtual {v1, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 1572
    const/4 v15, 0x1

    iput v15, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    goto :goto_3

    .line 1575
    .end local v1    # "_cid":Ljava/lang/String;
    :cond_6
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1560
    add-int/lit8 v6, v6, 0x1

    goto :goto_2
.end method

.method private subCommit()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2722
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2725
    .local v10, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2727
    .local v2, "alNewEmailsIdIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 2728
    .local v8, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-boolean v15, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    if-nez v15, :cond_0

    .line 2729
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v15}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$808(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 2730
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v15}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$908(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 2734
    :cond_0
    const/4 v15, 0x1

    iput v15, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mDirtyCommit:I

    .line 2738
    invoke-virtual {v8, v10}, Lcom/android/emailcommon/provider/EmailContent$Message;->addSaveOps(Ljava/util/ArrayList;)I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2745
    .end local v8    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v15}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v16

    monitor-enter v16

    .line 2746
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v15}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2747
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_2

    .line 2748
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 2749
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 2750
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 2752
    :cond_2
    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2815
    :goto_1
    return-void

    .line 2758
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v17, "com.android.email.provider"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v10}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v11

    .line 2761
    .local v11, "pstCPResult":[Landroid/content/ContentProviderResult;
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-ge v6, v15, :cond_6

    .line 2762
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 2763
    .restart local v8    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    aget-object v15, v11, v15

    iget-object v15, v15, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v15}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v15

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-interface {v15, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    move-wide/from16 v0, v18

    iput-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    .line 2765
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2769
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-boolean v15, v15, Lcom/android/exchange/adapter/EmailSyncAdapter;->isBlackList:Z

    if-eqz v15, :cond_5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->isMessageBlockedByUser(Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    invoke-static {v15, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1000(Lcom/android/exchange/adapter/EmailSyncAdapter;Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2770
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v15}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1108(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 2761
    :cond_4
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 2773
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J
    invoke-static {v15}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1200(Lcom/android/exchange/adapter/EmailSyncAdapter;)J

    move-result-wide v18

    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    move-wide/from16 v20, v0

    cmp-long v15, v18, v20

    if-gez v15, :cond_4

    .line 2774
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J
    invoke-static {v15, v0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1202(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J

    .line 2775
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-wide v0, v8, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J
    invoke-static {v15, v0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1302(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 2797
    .end local v6    # "index":I
    .end local v8    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v11    # "pstCPResult":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v13

    .line 2798
    .local v13, "ree":Ljava/util/concurrent/RejectedExecutionException;
    :try_start_2
    const-string v15, "EmailSyncAdapter"

    invoke-static {v15, v13}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2813
    .end local v13    # "ree":Ljava/util/concurrent/RejectedExecutionException;
    :goto_4
    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2814
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    .line 2781
    .restart local v6    # "index":I
    .restart local v11    # "pstCPResult":[Landroid/content/ContentProviderResult;
    :cond_6
    :try_start_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->updateHistoryForEmail(Ljava/util/ArrayList;)V

    .line 2782
    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2783
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/ArrayList;

    .line 2784
    .local v14, "saveBodyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    .line 2785
    .local v3, "context":Landroid/content/Context;
    new-instance v15, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v14, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$1;-><init>(Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;Ljava/util/ArrayList;Landroid/content/Context;)V

    invoke-static {v15}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 2796
    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/String;

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v15, v17

    const/16 v17, 0x1

    const-string v18, " SyncKey saved as: "

    aput-object v18, v15, v17

    const/16 v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v15, v17

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 2799
    .end local v3    # "context":Landroid/content/Context;
    .end local v6    # "index":I
    .end local v11    # "pstCPResult":[Landroid/content/ContentProviderResult;
    .end local v14    # "saveBodyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    :catch_1
    move-exception v12

    .line 2800
    .local v12, "re":Landroid/os/RemoteException;
    :try_start_4
    const-string v15, "EmailSyncAdapter"

    invoke-static {v15, v12}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4

    .line 2813
    .end local v12    # "re":Landroid/os/RemoteException;
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v15

    .line 2802
    :catch_2
    move-exception v9

    .line 2803
    .local v9, "oae":Landroid/content/OperationApplicationException;
    :try_start_5
    const-string v15, "EmailSyncAdapter"

    invoke-static {v15, v9}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4

    .line 2806
    .end local v9    # "oae":Landroid/content/OperationApplicationException;
    :catch_3
    move-exception v7

    .line 2807
    .local v7, "ioobe":Ljava/lang/IndexOutOfBoundsException;
    const-string v15, "EmailSyncAdapter"

    invoke-static {v15, v7}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4

    .line 2809
    .end local v7    # "ioobe":Ljava/lang/IndexOutOfBoundsException;
    :catch_4
    move-exception v4

    .line 2810
    .local v4, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/16 v17, 0x1

    move/from16 v0, v17

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z
    invoke-static {v15, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$102(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 2811
    const-string v15, "EmailSyncAdapter"

    invoke-static {v15, v4}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4
.end method

.method private syncFetchRepliesParser()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2873
    const/4 v4, 0x0

    .line 2874
    .local v4, "serverId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 2875
    .local v2, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v0, 0x0

    .line 2877
    .local v0, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    :cond_0
    :goto_0
    const/16 v7, 0xa

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v7

    const/4 v8, 0x3

    if-eq v7, v8, :cond_2

    .line 2878
    iget v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v7, :sswitch_data_0

    .line 2941
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2883
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v5

    .local v5, "status":I
    const/4 v7, 0x1

    if-eq v5, v7, :cond_0

    .line 2884
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sync-Fetch failed: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    goto :goto_0

    .line 2890
    .end local v5    # "status":I
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 2891
    goto :goto_0

    .line 2895
    :sswitch_2
    if-eqz v4, :cond_0

    .line 2898
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v6, v7, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    .line 2900
    .local v6, "svc":Lcom/android/exchange/EasSyncService;
    instance-of v7, v6, Lcom/android/exchange/EasLoadMoreSvc;

    if-eqz v7, :cond_8

    .line 2901
    const-string v7, "ItemOperationsParser"

    const-string v8, "svc(Adapter.mService) is instanceof EasLoadMoreSvc"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2903
    const-string v7, "ItemOperationsParser"

    const-string v8, "get message from service"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2904
    check-cast v6, Lcom/android/exchange/EasLoadMoreSvc;

    .end local v6    # "svc":Lcom/android/exchange/EasSyncService;
    iget-object v2, v6, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    move-object v3, v2

    .line 2907
    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .local v3, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_1
    if-nez v3, :cond_7

    .line 2908
    const-string v7, "ItemOperationsParser"

    const-string v8, "get message from restore cursor func()"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2911
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v4, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2912
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_7

    .line 2914
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2915
    new-instance v2, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct {v2}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2916
    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_start_1
    invoke-virtual {v2, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restore(Landroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2919
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2925
    .end local v1    # "c":Landroid/database/Cursor;
    :goto_3
    if-eqz v2, :cond_0

    .line 2926
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    iget-wide v8, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v7, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v0

    .line 2927
    if-eqz v0, :cond_0

    .line 2928
    invoke-direct {p0, v2, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->fetchDataParser(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    .line 2931
    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 2932
    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/emailcommon/mail/Snippet;->fromHtmlText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    goto/16 :goto_0

    .line 2919
    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v1    # "c":Landroid/database/Cursor;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catchall_0
    move-exception v7

    move-object v2, v3

    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v7

    .line 2933
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_1
    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 2934
    iget-object v7, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/emailcommon/mail/Snippet;->fromPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    goto/16 :goto_0

    .line 2949
    :cond_2
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v7, v7, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    instance-of v7, v7, Lcom/android/exchange/utility/MessageBodyRefresher;

    if-eqz v7, :cond_4

    .line 2950
    if-eqz v0, :cond_3

    .line 2951
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v6, v7, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    check-cast v6, Lcom/android/exchange/utility/MessageBodyRefresher;

    .line 2952
    .local v6, "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    invoke-virtual {v6, v0}, Lcom/android/exchange/utility/MessageBodyRefresher;->updateMessageBody(Lcom/android/emailcommon/provider/EmailContent$Body;)V

    .line 2965
    .end local v6    # "svc":Lcom/android/exchange/utility/MessageBodyRefresher;
    :cond_3
    :goto_5
    return-void

    .line 2955
    :cond_4
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 2956
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v7, v8, v2, v0}, Lcom/android/exchange/adapter/LoadMoreUtility;->updateEmail(Lcom/android/exchange/AbstractSyncService;Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V

    .line 2957
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z
    invoke-static {v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1400(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2958
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v2, v8}, Lcom/android/exchange/adapter/LoadMoreUtility;->updateSignedMIMEDataAttachment(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/lang/String;)V

    .line 2960
    :cond_5
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z
    invoke-static {v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1500(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2961
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v7, v8, v2}, Lcom/android/exchange/adapter/LoadMoreUtility;->updateInlineAttachment(Lcom/android/exchange/AbstractSyncService;Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_5

    .line 2919
    .restart local v1    # "c":Landroid/database/Cursor;
    :catchall_1
    move-exception v7

    goto :goto_4

    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_6
    move-object v2, v3

    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto :goto_2

    .end local v1    # "c":Landroid/database/Cursor;
    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_7
    move-object v2, v3

    .end local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto :goto_3

    .local v6, "svc":Lcom/android/exchange/EasSyncService;
    :cond_8
    move-object v3, v2

    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v3    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_1

    .line 2878
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_1
        0xe -> :sswitch_0
        0x1d -> :sswitch_2
    .end sparse-switch
.end method

.method private updateHistoryForEmail(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "emails":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    const/16 v3, 0x190

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 1583
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-nez v0, :cond_0

    .line 1584
    const-string v0, "EmailSyncAdapter"

    const-string v1, "updateHistoryForEmail() : mMailbox is null"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1641
    :goto_0
    return-void

    .line 1588
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1592
    .local v11, "newMailIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1595
    .local v12, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 1598
    .local v10, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 1599
    invoke-direct {p0, v10}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->updateHistoryForOutgoingEmail(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_1

    .line 1600
    :cond_2
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-eq v0, v5, :cond_3

    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 1603
    :cond_3
    invoke-direct {p0, v10, v12}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->updateHistoryForIncomingEmail(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V

    .line 1607
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v0, :cond_1

    .line 1608
    iget-wide v0, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1619
    .end local v10    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_4
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    invoke-virtual {v0, v12}, Lcom/android/exchange/ExchangeService$UpdateHistory;->execBatchOpsInQueue(Ljava/util/ArrayList;)V

    .line 1622
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1625
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v9, v0, [Ljava/lang/String;

    .line 1627
    .local v9, "ids":[Ljava/lang/String;
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const-string v2, "com.sec.android.socialhub.action.EXTERNAL_DB_UPDATE"

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, p0

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->requestSendBroadcastIntent(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;IZZ)V

    .line 1632
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const-string v2, "com.android.email.intent.action.ACTION_NOTIFY_NEW_EMAIL"

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, p0

    move v6, v7

    invoke-virtual/range {v0 .. v7}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->requestSendBroadcastIntent(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;IZZ)V

    .line 1639
    .end local v9    # "ids":[Ljava/lang/String;
    :cond_5
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 1640
    const/4 v11, 0x0

    .line 1641
    goto/16 :goto_0
.end method

.method private updateHistoryForIncomingEmail(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1734
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1737
    .local v4, "value":Landroid/content/ContentValues;
    :try_start_0
    const-string v7, "date"

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1739
    const-string v7, "type"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1740
    const-string v7, "contactid"

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1741
    const-string v7, "messageid"

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1742
    const-string v7, "m_subject"

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1743
    const-string v7, "new"

    iget-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    if-eqz v8, :cond_0

    move v5, v6

    :cond_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1744
    const-string v5, "account_name"

    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1745
    const-string v5, "account_id"

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1747
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 1748
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v7, 0x32

    if-le v5, v7, :cond_3

    .line 1749
    const-string v5, "m_content"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    const/4 v8, 0x0

    const/16 v9, 0x31

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    :cond_1
    :goto_0
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1757
    .local v2, "fromAddress":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v0, v2, v5

    .line 1758
    .local v0, "address":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1759
    .local v3, "fromSplitAddress":[Ljava/lang/String;
    array-length v5, v3

    if-lez v5, :cond_2

    .line 1760
    const/4 v5, 0x0

    aget-object v5, v3, v5

    const-string v7, "@"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1761
    const-string v5, "number"

    const/4 v7, 0x0

    aget-object v7, v3, v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1764
    const-string v5, "content://logs/email"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1780
    .end local v0    # "address":Ljava/lang/String;
    .end local v2    # "fromAddress":[Ljava/lang/String;
    .end local v3    # "fromSplitAddress":[Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 1782
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v5, p1, v6}, Lcom/android/emailcommon/utility/Utility;->updateLog4LifeTimesApp(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)V

    .line 1783
    return-void

    .line 1751
    :cond_3
    :try_start_1
    const-string v5, "m_content"

    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1774
    :catch_0
    move-exception v1

    .line 1775
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_4

    .line 1776
    const-string v5, "EmailSyncAdapter"

    const-string v7, "update history caught exception"

    invoke-static {v5, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1777
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private updateHistoryForOutgoingEmail(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 14
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 1789
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 1792
    .local v9, "value":Landroid/content/ContentValues;
    :try_start_0
    const-string v10, "date"

    iget-wide v12, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1793
    const-string v10, "type"

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1794
    const-string v10, "contactid"

    iget-wide v12, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1795
    const-string v10, "messageid"

    iget-wide v12, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1796
    const-string v10, "m_subject"

    iget-object v11, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1797
    const-string v10, "account_name"

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    const-string v10, "account_id"

    iget-wide v12, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1800
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v10, :cond_0

    .line 1801
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x32

    if-le v10, v11, :cond_2

    .line 1802
    const-string v10, "m_content"

    iget-object v11, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    const/4 v12, 0x0

    const/16 v13, 0x31

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808
    :cond_0
    :goto_0
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    if-eqz v10, :cond_5

    .line 1809
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1811
    .local v7, "toAddress":[Ljava/lang/String;
    move-object v1, v7

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_5

    aget-object v0, v1, v5

    .line 1812
    .local v0, "address":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1814
    .local v8, "toSplitAddress":[Ljava/lang/String;
    array-length v10, v8

    if-lez v10, :cond_1

    .line 1815
    const/4 v10, 0x0

    aget-object v10, v8, v10

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1816
    const-string v10, "number"

    const/4 v11, 0x0

    aget-object v11, v8, v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1817
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "content://logs/email"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1811
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1804
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "toAddress":[Ljava/lang/String;
    .end local v8    # "toSplitAddress":[Ljava/lang/String;
    :cond_2
    const-string v10, "m_content"

    iget-object v11, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1840
    :catch_0
    move-exception v4

    .line 1841
    .local v4, "e":Ljava/lang/Exception;
    sget-boolean v10, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v10, :cond_3

    .line 1842
    const-string v10, "EmailSyncAdapter"

    const-string v11, "update history caught exception"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1843
    :cond_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1846
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 1849
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const/4 v11, 0x1

    invoke-static {v10, p1, v11}, Lcom/android/emailcommon/utility/Utility;->updateLog4LifeTimesApp(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)V

    .line 1850
    return-void

    .line 1825
    :cond_5
    :try_start_1
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    if-eqz v10, :cond_4

    .line 1826
    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1827
    .local v2, "ccAddress":[Ljava/lang/String;
    move-object v1, v2

    .restart local v1    # "arr$":[Ljava/lang/String;
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v5, 0x0

    .restart local v5    # "i$":I
    :goto_2
    if-ge v5, v6, :cond_4

    aget-object v0, v1, v5

    .line 1828
    .restart local v0    # "address":Ljava/lang/String;
    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1830
    .local v3, "ccSplitAddress":[Ljava/lang/String;
    array-length v10, v3

    if-lez v10, :cond_6

    .line 1831
    const/4 v10, 0x0

    aget-object v10, v3, v10

    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1832
    const-string v10, "number"

    const/4 v11, 0x0

    aget-object v11, v3, v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "content://logs/email"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1827
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method


# virtual methods
.method public addData(Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 13
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1018
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1020
    .local v1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    :cond_0
    :goto_0
    const/16 v5, 0x1d

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v5

    if-eq v5, v12, :cond_8

    .line 1021
    iget v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v5, :sswitch_data_0

    .line 1178
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    .line 1181
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v8, 0x5

    if-eq v5, v8, :cond_2

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v8, 0x4

    if-eq v5, v8, :cond_2

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-ne v5, v12, :cond_0

    .line 1184
    :cond_2
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    iget-object v9, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    iget-object v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    invoke-static {v5, v8, v9, v10}, Lcom/android/exchange/adapter/EmailSyncAdapter;->makeDisplayName(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    .line 1025
    :sswitch_0
    invoke-direct {p0, v1, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->attachmentsParser(Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_1

    .line 1029
    :sswitch_1
    iget v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    and-int/lit16 v5, v5, 0x100

    const/16 v8, 0x100

    if-ne v5, v8, :cond_3

    .line 1030
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto :goto_1

    .line 1032
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto :goto_1

    .line 1037
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addDataFrom(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_1

    .line 1041
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    goto :goto_1

    .line 1045
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    goto :goto_1

    .line 1049
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    goto :goto_1

    .line 1053
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1054
    .local v4, "tempSubject":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 1055
    const/16 v5, 0xa

    const/16 v8, 0x20

    invoke-virtual {v4, v5, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v4

    .line 1057
    :cond_4
    iput-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_1

    .line 1061
    .end local v4    # "tempSubject":Ljava/lang/String;
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v5

    if-ne v5, v6, :cond_5

    move v5, v6

    :goto_2
    iput-boolean v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    goto/16 :goto_1

    :cond_5
    move v5, v7

    goto :goto_2

    .line 1065
    :sswitch_8
    invoke-direct {p0, p1, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->bodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V

    goto/16 :goto_1

    .line 1069
    :sswitch_9
    invoke-direct {p0, p1, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->bodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)V

    goto/16 :goto_1

    .line 1073
    :sswitch_a
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addDataFlag(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_1

    .line 1080
    :sswitch_b
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v8}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getUniqueTempFileName()Ljava/lang/String;

    move-result-object v8

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v5, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$202(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 1081
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v5, :cond_1

    .line 1082
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/high16 v10, 0x4004000000000000L    # 2.5

    cmpl-double v5, v8, v10

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v8}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v5

    if-eqz v5, :cond_1

    .line 1084
    :cond_6
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;
    invoke-static {v8}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6, v5, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z

    .line 1085
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getTempMIMEDataPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 1086
    .local v3, "is":Ljava/io/InputStream;
    invoke-direct {p0, p1, v3, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mimeBodyParser(Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/io/InputStream;Ljava/util/ArrayList;)V

    goto/16 :goto_1

    .line 1092
    .end local v3    # "is":Ljava/io/InputStream;
    :sswitch_c
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto/16 :goto_1

    .line 1096
    :sswitch_d
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mUmCallerId:Ljava/lang/String;

    goto/16 :goto_1

    .line 1100
    :sswitch_e
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mUmUserNotes:Ljava/lang/String;

    goto/16 :goto_1

    .line 1104
    :sswitch_f
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addDataClass(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_1

    .line 1108
    :sswitch_10
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->meetingRequestParser(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_1

    .line 1114
    :sswitch_11
    const-string v5, "EmailSyncAdapter"

    const-string v8, "in EMAIL_MIME_TRUNCATED tag @ addData()"

    invoke-static {v5, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1117
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v5

    if-ne v5, v6, :cond_7

    .line 1118
    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 1119
    const/4 v5, 0x2

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 1123
    :cond_7
    const-string v5, "EmailSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "msg.mFlagTruncated = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1127
    :sswitch_12
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    goto/16 :goto_1

    .line 1131
    :sswitch_13
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addDataConversationID(Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_1

    .line 1135
    :sswitch_14
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueOpaque()[B

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationIndex:[B

    goto/16 :goto_1

    .line 1142
    :sswitch_15
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    .line 1143
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 1144
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    iput-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    goto/16 :goto_1

    .line 1148
    :sswitch_16
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_1

    .line 1152
    :sswitch_17
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto/16 :goto_1

    .line 1156
    :sswitch_18
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    goto/16 :goto_1

    .line 1161
    :sswitch_19
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v5

    iput v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mLastVerb:I

    goto/16 :goto_1

    .line 1165
    :sswitch_1a
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mLastVerbTime:J

    goto/16 :goto_1

    .line 1170
    :sswitch_1b
    invoke-static {p1, p0}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->parseLicense(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/exchange/adapter/AbstractSyncParser;)V

    .line 1171
    sget-boolean v5, Lcom/android/exchange/irm/IRMLicenseParserUtility;->mRenewLicense:Z

    if-eqz v5, :cond_1

    if-eqz p1, :cond_1

    .line 1172
    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    iget-object v9, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-static {v5, v8, v9}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->renewLicense(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1188
    :cond_8
    iget-boolean v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-eqz v5, :cond_9

    .line 1189
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const v8, 0x7f06001d

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 1190
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<html><body>"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "</body></html>"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 1193
    :cond_9
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_f

    .line 1197
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 1198
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-nez v0, :cond_b

    .line 1199
    const-string v5, "EmailSyncAdapter"

    const-string v8, "Attachment instance null. this should not be occur!"

    invoke-static {v5, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1206
    :cond_b
    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-nez v5, :cond_c

    .line 1207
    iput v7, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 1208
    iput-boolean v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    goto :goto_3

    .line 1215
    :cond_c
    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    if-ne v5, v6, :cond_a

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v8, "image"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 1222
    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v8, "application/octet-stream"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-direct {p0, v5, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->findContentIdInHTMLBody(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 1223
    :cond_d
    iput v7, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 1224
    iput-boolean v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    goto :goto_3

    .line 1231
    .end local v0    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_e
    iput-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 1233
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_f
    return-void

    .line 1021
    nop

    :sswitch_data_0
    .sparse-switch
        0x5b -> :sswitch_15
        0x5e -> :sswitch_16
        0x7d -> :sswitch_17
        0x7e -> :sswitch_18
        0x86 -> :sswitch_0
        0x8c -> :sswitch_c
        0x8e -> :sswitch_11
        0x8f -> :sswitch_5
        0x92 -> :sswitch_12
        0x93 -> :sswitch_f
        0x94 -> :sswitch_6
        0x95 -> :sswitch_7
        0x96 -> :sswitch_1
        0x97 -> :sswitch_3
        0x98 -> :sswitch_2
        0x99 -> :sswitch_4
        0xa2 -> :sswitch_10
        0xb6 -> :sswitch_b
        0xb7 -> :sswitch_11
        0xba -> :sswitch_a
        0x44a -> :sswitch_8
        0x44e -> :sswitch_0
        0x45a -> :sswitch_9
        0x585 -> :sswitch_d
        0x586 -> :sswitch_e
        0x589 -> :sswitch_13
        0x58a -> :sswitch_14
        0x58b -> :sswitch_19
        0x58c -> :sswitch_1a
        0x608 -> :sswitch_1b
    .end sparse-switch
.end method

.method changeParser(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;>;"
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2343
    const/4 v7, 0x0

    .line 2344
    .local v7, "serverId":Ljava/lang/String;
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2345
    .local v2, "oldRead":Ljava/lang/Boolean;
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2346
    .local v3, "oldFlag":Ljava/lang/Boolean;
    const-wide/16 v4, 0x0

    .line 2347
    .local v4, "id":J
    :cond_0
    :goto_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 2348
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    .line 2373
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2350
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 2351
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent$Message;->LIST_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v7, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2352
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2354
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2355
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v10, "Changing "

    aput-object v10, v0, v1

    const/4 v1, 0x1

    aput-object v7, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V

    .line 2356
    const/4 v0, 0x4

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v8, :cond_2

    move v0, v8

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2361
    const/16 v0, 0x12

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v8, :cond_3

    move v0, v8

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2362
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 2365
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    move v0, v9

    .line 2356
    goto :goto_1

    :cond_3
    move v0, v9

    .line 2361
    goto :goto_2

    .line 2365
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .end local v6    # "c":Landroid/database/Cursor;
    :sswitch_1
    move-object v0, p0

    move-object v1, p1

    .line 2370
    invoke-direct/range {v0 .. v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changeApplicationDataParser(Ljava/util/ArrayList;Ljava/lang/Boolean;Ljava/lang/Boolean;J)V

    goto :goto_0

    .line 2376
    :cond_4
    return-void

    .line 2348
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public commandsParser()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 2667
    const-wide/16 v0, 0x0

    .line 2670
    .local v0, "lAllEmailsSizePerReq":J
    const/4 v2, 0x0

    .line 2672
    .local v2, "tempMessage":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_0
    :goto_0
    const/16 v3, 0x16

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_9

    .line 2673
    iget v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_5

    .line 2674
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addParser()Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v2

    .line 2675
    if-eqz v2, :cond_0

    .line 2680
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->isDuplicateMail(Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2683
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->incrementChangeCount()V

    .line 2684
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_3

    .line 2685
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mEstimatedDataSize:I

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 2692
    :goto_1
    const-wide/32 v4, 0x3e800

    cmp-long v3, v0, v4

    if-gtz v3, :cond_1

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0xa

    if-lt v3, v4, :cond_2

    .line 2693
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_4

    .line 2694
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mEstimatedDataSize:I

    int-to-long v0, v3

    .line 2699
    :goto_2
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommit()V

    .line 2701
    :cond_2
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2702
    const/4 v2, 0x0

    goto :goto_0

    .line 2689
    :cond_3
    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->getComputedBodySize()I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    goto :goto_1

    .line 2696
    :cond_4
    invoke-virtual {v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->getComputedBodySize()I

    move-result v3

    int-to-long v0, v3

    goto :goto_2

    .line 2705
    :cond_5
    iget v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    const/16 v4, 0x9

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    const/16 v4, 0x21

    if-ne v3, v4, :cond_7

    .line 2706
    :cond_6
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletedEmails:Ljava/util/ArrayList;

    iget v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deleteParser(Ljava/util/ArrayList;I)V

    .line 2707
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 2708
    :cond_7
    iget v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_8

    .line 2709
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changedEmails:Ljava/util/ArrayList;

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changeParser(Ljava/util/ArrayList;)V

    .line 2710
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-virtual {v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->incrementChangeCount()V

    goto/16 :goto_0

    .line 2712
    :cond_8
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto/16 :goto_0

    .line 2715
    :cond_9
    return-void
.end method

.method public commit()V
    .locals 50

    .prologue
    .line 3198
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_0

    .line 3199
    const-string v4, "EmailSyncAdapter"

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v6, "Performance Check - commit() +++++"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3200
    :cond_0
    const/16 v22, 0x0

    .line 3202
    .local v22, "commitOk":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-nez v4, :cond_2

    .line 3203
    const-string v4, "EmailSyncAdapter"

    const-string v5, "Can not commit due to mMailbox is null"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3667
    :cond_1
    :goto_0
    return-void

    .line 3208
    :cond_2
    new-instance v18, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3212
    .local v18, "blacklistCommitIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$100(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3213
    const-string v4, "EmailSyncAdapter"

    const-string v5, "Memory full dectection"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3652
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_3

    .line 3653
    const-string v4, "EmailSyncAdapter"

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v6, "Performance Check - commit() finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 3658
    if-nez v22, :cond_1

    .line 3659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3660
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 3661
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 3216
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v5, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$102(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 3219
    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    .line 3222
    .local v43, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 3225
    .local v15, "alNewEmailsIdIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v47, Ljava/util/ArrayList;

    invoke-direct/range {v47 .. v47}, Ljava/util/ArrayList;-><init>()V

    .line 3227
    .local v47, "saveBodyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->fetchedEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    .local v33, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 3229
    .local v39, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v20

    .line 3230
    .local v20, "c":Landroid/database/Cursor;
    const/16 v35, 0x0

    .line 3231
    .local v35, "id":Ljava/lang/String;
    if-eqz v20, :cond_7

    .line 3233
    :try_start_2
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 3234
    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v35

    .line 3237
    :cond_6
    :try_start_3
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 3244
    :cond_7
    if-eqz v35, :cond_5

    .line 3245
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Fetched body successfully for "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v35, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V

    .line 3246
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v4, v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v35, v4, v5

    .line 3247
    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    if-eqz v4, :cond_8

    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const v5, 0x32000

    if-le v4, v5, :cond_8

    .line 3248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Lcom/android/emailcommon/provider/EmailContent$Message;->saveBodyToFilesIfNecessary(Landroid/content/Context;)V

    .line 3249
    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    const v5, 0x32000

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const v7, 0x7f06001a

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/android/emailcommon/utility/ConversionUtilities;->cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v39

    iput-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 3252
    :cond_8
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "messageKey=?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v6, v6, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "textContent"

    move-object/from16 v0, v39

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3255
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "_id=?"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v6, v6, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "flagLoaded"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 3652
    .end local v15    # "alNewEmailsIdIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v20    # "c":Landroid/database/Cursor;
    .end local v33    # "i$":Ljava/util/Iterator;
    .end local v35    # "id":Ljava/lang/String;
    .end local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v43    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v47    # "saveBodyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    :catchall_0
    move-exception v4

    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v5, :cond_9

    .line 3653
    const-string v5, "EmailSyncAdapter"

    const-string v6, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v7, "Performance Check - commit() finally"

    invoke-static {v5, v6, v7}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 3658
    if-nez v22, :cond_a

    .line 3659
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 3660
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 3661
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    :cond_a
    throw v4

    .line 3237
    .restart local v15    # "alNewEmailsIdIndex":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v20    # "c":Landroid/database/Cursor;
    .restart local v33    # "i$":Ljava/util/Iterator;
    .restart local v35    # "id":Ljava/lang/String;
    .restart local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v43    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v47    # "saveBodyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Message;>;"
    :catchall_1
    move-exception v4

    :try_start_4
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    throw v4

    .line 3261
    .end local v20    # "c":Landroid/database/Cursor;
    .end local v35    # "id":Ljava/lang/String;
    .end local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_c

    .line 3262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v5, 0x1

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1602(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z

    .line 3263
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    :goto_2
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 3264
    .restart local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    move-object/from16 v0, v39

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    if-nez v4, :cond_d

    .line 3265
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$908(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 3266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$808(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 3271
    :cond_d
    move-object/from16 v0, v39

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->addSaveOps(Ljava/util/ArrayList;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3275
    .end local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletedEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    :goto_3
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/Long;

    .line 3276
    .local v35, "id":Ljava/lang/Long;
    const-wide/16 v4, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-string v7, "flagRead"

    invoke-static {v6, v10, v11, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->getKeyColumnLong(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_f

    .line 3277
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$808(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 3279
    :cond_f
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3282
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v4, v6, v7, v10, v11}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllAttachmentFilesUri(Landroid/content/Context;JJ)V

    .line 3284
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v4, v6, v7, v10, v11}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMessageBodyFilesUri(Landroid/content/Context;JJ)V

    goto :goto_3

    .line 3287
    .end local v35    # "id":Ljava/lang/Long;
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changedEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_19

    .line 3289
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->changedEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    :cond_11
    :goto_4
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_19

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;

    .line 3290
    .local v21, "change":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;
    new-instance v24, Landroid/content/ContentValues;

    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    .line 3292
    .local v24, "cv":Landroid/content/ContentValues;
    if-eqz v21, :cond_12

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flags:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    if-eqz v4, :cond_12

    .line 3293
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flags:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_ACTIVE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v4, v5, :cond_17

    .line 3294
    const-string v4, "flagStatus"

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3301
    :cond_12
    :goto_5
    if-eqz v21, :cond_13

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->read:Ljava/lang/Boolean;

    if-eqz v4, :cond_13

    .line 3302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$808(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 3303
    const-string v4, "flagRead"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->read:Ljava/lang/Boolean;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3305
    :cond_13
    if-eqz v21, :cond_14

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flag:Ljava/lang/Boolean;

    if-eqz v4, :cond_14

    .line 3309
    const-string v4, "followupflag"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flag:Ljava/lang/Boolean;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3312
    :cond_14
    if-eqz v21, :cond_15

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->lastVerb:I

    if-lez v4, :cond_15

    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->lastVerbTime:J

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-lez v4, :cond_15

    .line 3313
    const-string v4, "lastVerb"

    move-object/from16 v0, v21

    iget v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->lastVerb:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3314
    const-string v4, "lastVerbTime"

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->lastVerbTime:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3316
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->id:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 3321
    .local v8, "tempString":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const-string v6, "com.sec.android.socialhub.action.EXTERNAL_DB_UPDATE"

    const/16 v7, 0x190

    const/16 v9, 0xb

    const/4 v10, 0x1

    move-object/from16 v0, v21

    iget v11, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->lastVerb:I

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v11}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->requestSendBroadcastIntent(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;IZI)V

    .line 3327
    .end local v8    # "tempString":[Ljava/lang/String;
    :cond_15
    if-eqz v21, :cond_16

    .line 3328
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->id:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3334
    :cond_16
    if-eqz v21, :cond_11

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flags:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    if-eqz v4, :cond_11

    .line 3335
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flags:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    invoke-virtual {v4}, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->toContentValues()Landroid/content/ContentValues;

    move-result-object v29

    .line 3336
    .local v29, "ffcv":Landroid/content/ContentValues;
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->id:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 3295
    .end local v29    # "ffcv":Landroid/content/ContentValues;
    :cond_17
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;->flags:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_COMPLETE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v4, v5, :cond_18

    .line 3296
    const-string v4, "flagStatus"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 3298
    :cond_18
    const-string v4, "flagStatus"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5

    .line 3346
    .end local v21    # "change":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$ServerChange;
    .end local v24    # "cv":Landroid/content/ContentValues;
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v4, v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->draftChanges:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    .end local v33    # "i$":Ljava/util/Iterator;
    :cond_1a
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_20

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 3347
    .local v27, "draftmsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 3349
    .local v25, "cv1":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1d

    .line 3350
    const/16 v19, 0x0

    .line 3351
    .local v19, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v19

    .line 3352
    if-eqz v19, :cond_1b

    .line 3353
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mId:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Body;->delete(Landroid/content/Context;Landroid/net/Uri;J)I

    .line 3356
    :cond_1b
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 3360
    .local v26, "cvBody":Landroid/content/ContentValues;
    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->addBodyPartsContentValues(Landroid/content/ContentValues;)I

    move-result v4

    if-lez v4, :cond_1c

    .line 3361
    move-object/from16 v0, v47

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3364
    :cond_1c
    const-string v4, "messageKey"

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3365
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3368
    const-string v4, "toList"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3369
    const-string v4, "ccList"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3370
    const-string v4, "bccList"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mBcc:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3371
    const-string v4, "subject"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3372
    const-string v4, "importance"

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3376
    const-string v4, "istruncated"

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3377
    const-string v4, "snippet"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3379
    move-object/from16 v0, v27

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-nez v4, :cond_1e

    move-object/from16 v0, v27

    iget-boolean v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-nez v4, :cond_1e

    .line 3380
    const-string v4, "smimeFlags"

    const-string v5, "0"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3386
    :goto_6
    const-string v4, "IRMTemplateId"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateId:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3387
    const-string v4, "IRMLicenseFlag"

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3388
    const-string v4, "IRMOwner"

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMOwner:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3389
    const-string v4, "IRMContentExpiryDate"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3390
    const-string v4, "IRMContentOwner"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentOwner:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3391
    const-string v4, "IRMRemovalFlag"

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMRemovalFlag:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3392
    const-string v4, "IRMTemplateDescription"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateDescription:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3393
    const-string v4, "IRMTemplateName"

    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateName:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3396
    .end local v19    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .end local v26    # "cvBody":Landroid/content/ContentValues;
    :cond_1d
    const-string v4, "flagAttachment"

    move-object/from16 v0, v27

    iget-boolean v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3398
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v42

    .line 3405
    .local v42, "oldAttach":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v42, :cond_1f

    .line 3406
    move-object/from16 v0, v42

    array-length v0, v0

    move/from16 v48, v0

    .line 3407
    .local v48, "size":I
    const/16 v32, 0x0

    .local v32, "i":I
    :goto_7
    move/from16 v0, v32

    move/from16 v1, v48

    if-ge v0, v1, :cond_1f

    .line 3408
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    aget-object v6, v42, v32

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v4, v5, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->delete(Landroid/content/Context;Landroid/net/Uri;J)I

    .line 3407
    add-int/lit8 v32, v32, 0x1

    goto :goto_7

    .line 3382
    .end local v32    # "i":I
    .end local v42    # "oldAttach":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v48    # "size":I
    .restart local v19    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .restart local v26    # "cvBody":Landroid/content/ContentValues;
    :cond_1e
    const-string v4, "smimeFlags"

    const-string v5, "2"

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 3412
    .end local v19    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .end local v26    # "cvBody":Landroid/content/ContentValues;
    .restart local v42    # "oldAttach":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_1f
    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v34

    .local v34, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1a

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 3415
    .local v16, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->toContentValues()Landroid/content/ContentValues;

    move-result-object v17

    .line 3416
    .local v17, "attContentValues":Landroid/content/ContentValues;
    const-string v4, "messageKey"

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3417
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 3426
    .end local v16    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v17    # "attContentValues":Landroid/content/ContentValues;
    .end local v25    # "cv1":Landroid/content/ContentValues;
    .end local v27    # "draftmsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v34    # "i$":Ljava/util/Iterator;
    .end local v42    # "oldAttach":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newResponseAdds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v33

    .restart local v33    # "i$":Ljava/util/Iterator;
    :cond_21
    :goto_9
    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-interface/range {v33 .. v33}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 3427
    .restart local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/16 v40, 0x0

    .line 3430
    .local v40, "msgCursor":Landroid/database/Cursor;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v10, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_PROJECTION:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "clientId=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mClientId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v40

    .line 3433
    if-eqz v40, :cond_23

    .line 3434
    invoke-interface/range {v40 .. v40}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_23

    .line 3436
    :cond_22
    const/4 v4, 0x0

    move-object/from16 v0, v40

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    .line 3437
    .local v35, "id":I
    new-instance v49, Landroid/content/ContentValues;

    invoke-direct/range {v49 .. v49}, Landroid/content/ContentValues;-><init>()V

    .line 3438
    .local v49, "values":Landroid/content/ContentValues;
    const-string v4, "syncServerId"

    move-object/from16 v0, v39

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v49

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3443
    const-string v4, "messageDirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v49

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3444
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move/from16 v0, v35

    int-to-long v6, v0

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v49

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3448
    invoke-interface/range {v40 .. v40}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v4

    if-nez v4, :cond_22

    .line 3452
    .end local v35    # "id":I
    .end local v49    # "values":Landroid/content/ContentValues;
    :cond_23
    if-eqz v40, :cond_21

    .line 3453
    :try_start_6
    invoke-interface/range {v40 .. v40}, Landroid/database/Cursor;->close()V

    goto/16 :goto_9

    .line 3452
    :catchall_2
    move-exception v4

    if-eqz v40, :cond_24

    .line 3453
    invoke-interface/range {v40 .. v40}, Landroid/database/Cursor;->close()V

    :cond_24
    throw v4

    .line 3464
    .end local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v40    # "msgCursor":Landroid/database/Cursor;
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    instance-of v4, v4, Lcom/android/exchange/EasLoadMoreSvc;

    if-nez v4, :cond_27

    .line 3465
    new-instance v38, Landroid/content/ContentValues;

    invoke-direct/range {v38 .. v38}, Landroid/content/ContentValues;-><init>()V

    .line 3466
    .local v38, "mailboxValues":Landroid/content/ContentValues;
    const-string v4, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3467
    const-string v4, "syncKey"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlags:I

    move/from16 v30, v0

    .line 3469
    .local v30, "flag":I
    const-string v4, "0"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mPrevSyncKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1700(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 3471
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->checkNumberOfMessageToSync()I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v4

    if-eqz v4, :cond_26

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->canSyncContinue()Z
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1900(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 3472
    or-int/lit8 v30, v30, 0x40

    .line 3473
    const-string v4, "progressing dummy sync"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->log(Ljava/lang/String;)V

    .line 3475
    :cond_26
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "first sync established"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V

    .line 3480
    :goto_a
    const-string v4, "flags"

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3482
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    move-object/from16 v0, v43

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->resetMessageDirtyCommit(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    invoke-static {v4, v5, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2000(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 3490
    .end local v30    # "flag":I
    .end local v38    # "mailboxValues":Landroid/content/ContentValues;
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, v43

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->addCleanupOps(Ljava/util/ArrayList;)V
    invoke-static {v4, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2100(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;)V

    .line 3493
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 3494
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 3495
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_28

    .line 3496
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 3497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 3498
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->clear()V

    .line 3500
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v6, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4, v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$902(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3501
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v6, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4, v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$802(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3502
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 3652
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_29

    .line 3653
    const-string v4, "EmailSyncAdapter"

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v6, "Performance Check - commit() finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 3658
    if-nez v22, :cond_1

    .line 3659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 3660
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 3661
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 3477
    .restart local v30    # "flag":I
    .restart local v38    # "mailboxValues":Landroid/content/ContentValues;
    :cond_2a
    and-int/lit8 v30, v30, -0x41

    .line 3478
    const/4 v4, 0x1

    :try_start_8
    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "real sync established"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_a

    .line 3508
    .end local v30    # "flag":I
    .end local v38    # "mailboxValues":Landroid/content/ContentValues;
    :cond_2b
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    const-string v6, "com.android.email.provider"

    move-object/from16 v0, v43

    invoke-virtual {v4, v6, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v44

    .line 3510
    .local v44, "pstCPResult":[Landroid/content/ContentProviderResult;
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_2c

    .line 3511
    const-string v4, "EmailSyncAdapter"

    const-string v6, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v7, "Performance Check - commit() right after"

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3512
    :cond_2c
    const/16 v36, 0x0

    .local v36, "index":I
    :goto_b
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v36

    if-ge v0, v4, :cond_36

    .line 3513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    move/from16 v0, v36

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 3514
    .restart local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    move/from16 v0, v36

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aget-object v4, v44, v4

    iget-object v4, v4, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    move-object/from16 v0, v39

    iput-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    .line 3519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-boolean v4, v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->isBlackList:Z

    if-eqz v4, :cond_2e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, v39

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->isMessageBlockedByUser(Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    invoke-static {v4, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1000(Lcom/android/exchange/adapter/EmailSyncAdapter;Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 3520
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # operator++ for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1108(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    .line 3530
    :cond_2d
    :goto_c
    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3512
    add-int/lit8 v36, v36, 0x1

    goto :goto_b

    .line 3523
    :cond_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1200(Lcom/android/exchange/adapter/EmailSyncAdapter;)J

    move-result-wide v6

    move-object/from16 v0, v39

    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    cmp-long v4, v6, v10

    if-gez v4, :cond_2d

    .line 3524
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J
    invoke-static {v4, v6, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1202(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J

    .line 3525
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J
    invoke-static {v4, v6, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1302(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J
    :try_end_9
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_c

    .line 3562
    .end local v36    # "index":I
    .end local v39    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v44    # "pstCPResult":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v46

    .line 3563
    .local v46, "ree":Ljava/util/concurrent/RejectedExecutionException;
    :try_start_a
    const-string v4, "EmailSyncAdapter"

    move-object/from16 v0, v46

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 3578
    .end local v46    # "ree":Ljava/util/concurrent/RejectedExecutionException;
    :goto_d
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 3582
    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1100(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v5

    # -= operator for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$920(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3584
    const-string v4, "EmailSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "commit() moreAvailable = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->moreAvailable:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mNotifyCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mSpamCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1100(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mChangedMessageCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3586
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v5, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1102(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3588
    const/16 v31, 0x0

    .line 3589
    .local v31, "forceAlarm":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v5

    # += operator for: Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2412(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3590
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2400(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v4

    if-lez v4, :cond_2f

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->moreAvailable:Z

    if-nez v4, :cond_2f

    .line 3591
    const/16 v31, 0x1

    .line 3594
    :cond_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v4

    if-gtz v4, :cond_30

    if-eqz v31, :cond_31

    .line 3609
    :cond_30
    :try_start_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_31

    .line 3610
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->moreAvailable:Z

    if-eqz v4, :cond_39

    .line 3611
    new-instance v4, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v5

    invoke-virtual {v4, v6, v7, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyNewMessagesNoAlarm(JI)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 3623
    :cond_31
    :goto_e
    :try_start_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result v4

    if-lez v4, :cond_32

    .line 3625
    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v5, 0x4

    if-eq v4, v5, :cond_32

    .line 3626
    new-instance v4, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v4, v6, v7}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyMessageUpdated(J)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 3632
    :cond_32
    :goto_f
    :try_start_f
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mAccount.mId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mMailbox.mDisplayName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " moreAvailable="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->moreAvailable:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mNotifyCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mSpamCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1100(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mChangedMessageCount="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " sumOfNewMsg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I
    invoke-static {v6}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2400(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/exchange/ServiceLogger;->logNotificationStats(Ljava/lang/String;)V

    .line 3640
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const-wide/16 v6, -0x1

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J
    invoke-static {v4, v6, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1302(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J

    .line 3641
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const-wide/16 v6, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J
    invoke-static {v4, v6, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1202(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J

    .line 3642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v5, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$802(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I

    .line 3643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    const/4 v5, 0x0

    # setter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$902(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 3644
    if-eqz v22, :cond_33

    .line 3646
    :try_start_10
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    const/4 v5, 0x0

    const/16 v9, 0x64

    invoke-interface {v4, v6, v7, v5, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 3652
    :cond_33
    :goto_10
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_34

    .line 3653
    const-string v4, "EmailSyncAdapter"

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v6, "Performance Check - commit() finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3656
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 3658
    if-nez v22, :cond_35

    .line 3659
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_35

    .line 3660
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->deletePartiallyCommitedMails()V

    .line 3661
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 3665
    :cond_35
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_1

    .line 3666
    const-string v4, "EmailSyncAdapter"

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    const-string v6, "Performance Check - commit() -----"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3535
    .end local v31    # "forceAlarm":Z
    .restart local v36    # "index":I
    .restart local v44    # "pstCPResult":[Landroid/content/ContentProviderResult;
    :cond_36
    :try_start_11
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_37

    .line 3536
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, v18

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->insertBlacklistMessage(Ljava/util/ArrayList;)V
    invoke-static {v4, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2200(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;)V

    .line 3537
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    const-string v7, "intent.blacklist.action.PROCESS_EMAIL"

    const/16 v9, 0x1f4

    # invokes: Lcom/android/exchange/adapter/EmailSyncAdapter;->requestSendBroadcastIntentBlacklistModule(Landroid/content/Context;Ljava/lang/String;I)V
    invoke-static {v4, v6, v7, v9}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$2300(Lcom/android/exchange/adapter/EmailSyncAdapter;Landroid/content/Context;Ljava/lang/String;I)V

    .line 3540
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 3541
    const/16 v18, 0x0

    .line 3545
    :cond_37
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->updateHistoryForEmail(Ljava/util/ArrayList;)V

    .line 3546
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 3547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newEmails:Ljava/util/ArrayList;

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 3548
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_38

    .line 3549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    .line 3550
    .local v23, "context":Landroid/content/Context;
    new-instance v4, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v47

    move-object/from16 v2, v23

    invoke-direct {v4, v0, v1, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser$2;-><init>(Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;Ljava/util/ArrayList;Landroid/content/Context;)V

    invoke-static {v4}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;

    .line 3559
    .end local v23    # "context":Landroid/content/Context;
    :cond_38
    const/16 v22, 0x1

    .line 3561
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, " SyncKey saved as: "

    aput-object v7, v4, v6

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    aput-object v7, v4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_11 .. :try_end_11} :catch_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_11 .. :try_end_11} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_11 .. :try_end_11} :catch_4
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto/16 :goto_d

    .line 3564
    .end local v36    # "index":I
    .end local v44    # "pstCPResult":[Landroid/content/ContentProviderResult;
    :catch_1
    move-exception v45

    .line 3565
    .local v45, "re":Landroid/os/RemoteException;
    :try_start_12
    const-string v4, "EmailSyncAdapter"

    move-object/from16 v0, v45

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_d

    .line 3578
    .end local v45    # "re":Landroid/os/RemoteException;
    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 3567
    :catch_2
    move-exception v41

    .line 3568
    .local v41, "oae":Landroid/content/OperationApplicationException;
    :try_start_14
    const-string v4, "EmailSyncAdapter"

    move-object/from16 v0, v41

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_d

    .line 3571
    .end local v41    # "oae":Landroid/content/OperationApplicationException;
    :catch_3
    move-exception v37

    .line 3572
    .local v37, "ioobe":Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "EmailSyncAdapter"

    move-object/from16 v0, v37

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_d

    .line 3574
    .end local v37    # "ioobe":Ljava/lang/IndexOutOfBoundsException;
    :catch_4
    move-exception v28

    .line 3575
    .local v28, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    const-string v4, "EmailSyncAdapter"

    const-string v6, "Memory full"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 3576
    const-string v4, "EmailSyncAdapter"

    move-object/from16 v0, v28

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    goto/16 :goto_d

    .line 3614
    .end local v28    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    .restart local v31    # "forceAlarm":Z
    :cond_39
    :try_start_15
    new-instance v9, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-direct {v9, v4}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$1300(Lcom/android/exchange/adapter/EmailSyncAdapter;)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I
    invoke-static {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I

    move-result v14

    invoke-virtual/range {v9 .. v14}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyNewMessages(JJI)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_5
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto/16 :goto_e

    .line 3619
    :catch_5
    move-exception v4

    goto/16 :goto_e

    .line 3647
    :catch_6
    move-exception v28

    .line 3648
    .local v28, "e":Landroid/os/RemoteException;
    :try_start_16
    invoke-virtual/range {v28 .. v28}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_10

    .line 3627
    .end local v28    # "e":Landroid/os/RemoteException;
    :catch_7
    move-exception v4

    goto/16 :goto_f
.end method

.method deleteParser(Ljava/util/ArrayList;I)V
    .locals 6
    .param p2, "entryTag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2284
    .local p1, "deletes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_0
    :goto_0
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 2285
    iget v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 2308
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2287
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 2290
    .local v1, "serverId":Ljava/lang/String;
    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->MESSAGE_ID_SUBJECT_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$500()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getServerIdCursor(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2291
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 2293
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2294
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2295
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_1

    .line 2296
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Deleting "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2303
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2311
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "serverId":Ljava/lang/String;
    :cond_2
    return-void

    .line 2285
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method deletePartiallyCommitedMails()V
    .locals 6

    .prologue
    .line 3672
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 3673
    .local v2, "tempOps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->subCommitIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 3674
    .local v1, "id":Ljava/lang/Long;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3679
    .end local v1    # "id":Ljava/lang/Long;
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.email.provider"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3686
    :goto_1
    return-void

    .line 3683
    :catch_0
    move-exception v3

    goto :goto_1

    .line 3681
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public getMimeTypeFromFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 2255
    const-string v2, "application/octet-stream"

    .line 2257
    .local v2, "mimeType":Ljava/lang/String;
    const/16 v3, 0x2e

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2259
    .local v0, "_lastDotIdx":I
    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 2260
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 2262
    .local v1, "extension":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2266
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2268
    if-nez v2, :cond_0

    .line 2269
    const-string v2, "application/octet-stream"

    .line 2273
    .end local v1    # "extension":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method public isDuplicateMail(Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    .locals 10
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    const/4 v4, 0x0

    .line 2628
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDuplicateMail(): serverId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AccountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MailboxId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->userLog([Ljava/lang/String;)V

    .line 2630
    const/4 v7, 0x0

    .line 2631
    .local v7, "isDuplicateMail":Z
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter;->UPDATES_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/adapter/EmailSyncAdapter;->access$700()[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncServerId=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\' AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "accountKey"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "mailboxKey"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "flags"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x200

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")=0"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2640
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 2641
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2642
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2643
    const-string v0, "CancelSync"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " duplicate mail found and will be reverted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ServerId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Subject : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2644
    const/4 v7, 0x1

    .line 2648
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2649
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2652
    :cond_1
    return v7

    .line 2648
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2649
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public recurrenceParser()Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1363
    const/4 v0, -0x1

    .line 1364
    .local v0, "type":I
    const/4 v1, -0x1

    .line 1365
    .local v1, "occurrences":I
    const/4 v2, -0x1

    .line 1366
    .local v2, "interval":I
    const/4 v3, -0x1

    .line 1367
    .local v3, "dow":I
    const/4 v4, -0x1

    .line 1368
    .local v4, "dom":I
    const/4 v5, -0x1

    .line 1369
    .local v5, "wom":I
    const/4 v6, -0x1

    .line 1370
    .local v6, "moy":I
    const/4 v7, 0x0

    .line 1372
    .local v7, "until":Ljava/lang/String;
    :goto_0
    const/16 v8, 0xa7

    invoke-virtual {p0, v8}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_0

    .line 1373
    iget v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    packed-switch v8, :pswitch_data_0

    .line 1399
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 1375
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v0

    .line 1376
    goto :goto_0

    .line 1378
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v2

    .line 1379
    goto :goto_0

    .line 1381
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v1

    .line 1382
    goto :goto_0

    .line 1384
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v3

    .line 1385
    goto :goto_0

    .line 1387
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v4

    .line 1388
    goto :goto_0

    .line 1390
    :pswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v5

    .line 1391
    goto :goto_0

    .line 1393
    :pswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValueInt()I

    move-result v6

    .line 1394
    goto :goto_0

    .line 1396
    :pswitch_7
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 1397
    goto :goto_0

    .line 1402
    :cond_0
    invoke-static/range {v0 .. v7}, Lcom/android/exchange/utility/CalendarUtilities;->rruleFromRecurrence(IIIIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    return-object v8

    .line 1373
    nop

    :pswitch_data_0
    .packed-switch 0xa9
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public requestSendBroadcastIntent(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;IZI)V
    .locals 10
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "szAction"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "ids"    # [Ljava/lang/String;
    .param p5, "action"    # I
    .param p6, "status"    # Z
    .param p7, "flag"    # I

    .prologue
    .line 1690
    const-string v7, "EmailSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestSendBroadcastIntent() action="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1694
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "intentType"

    invoke-virtual {v4, v7, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1696
    if-eqz p4, :cond_1

    .line 1699
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 1700
    .local v6, "sb":Ljava/lang/StringBuffer;
    move-object v1, p4

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v1, v2

    .line 1701
    .local v3, "id_item":Ljava/lang/String;
    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1702
    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1700
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1706
    .end local v3    # "id_item":Ljava/lang/String;
    :cond_0
    const-string v7, "EmailSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestSendBroadcastIntent() ids="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708
    const-string v7, "id_array"

    invoke-virtual {v4, v7, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1712
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v7, 0x2

    if-ne p3, v7, :cond_3

    .line 1713
    const-string v7, "is_read"

    move/from16 v0, p7

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1722
    :cond_2
    :goto_1
    const-string v7, "action"

    invoke-virtual {v4, v7, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1724
    const-string v7, "status"

    move/from16 v0, p6

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1726
    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1728
    const/4 v4, 0x0

    .line 1730
    return-void

    .line 1714
    :cond_3
    const/16 v7, 0x8

    if-ne p3, v7, :cond_4

    .line 1715
    const-string v7, "is_favorite"

    move/from16 v0, p7

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 1716
    :cond_4
    const/16 v7, 0xc

    if-ne p3, v7, :cond_2

    .line 1717
    const-string v7, "status_lastverb"

    move/from16 v0, p7

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public requestSendBroadcastIntent(Landroid/content/Context;Ljava/lang/String;I[Ljava/lang/String;IZZ)V
    .locals 10
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "szAction"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "ids"    # [Ljava/lang/String;
    .param p5, "action"    # I
    .param p6, "status"    # Z
    .param p7, "flag"    # Z

    .prologue
    .line 1648
    const-string v7, "EmailSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestSendBroadcastIntent() action="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1652
    .local v4, "intent":Landroid/content/Intent;
    const-string v7, "intentType"

    invoke-virtual {v4, v7, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1654
    if-eqz p4, :cond_1

    .line 1657
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 1658
    .local v6, "sb":Ljava/lang/StringBuffer;
    move-object v1, p4

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v1, v2

    .line 1659
    .local v3, "id_item":Ljava/lang/String;
    const/16 v7, 0x20

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1660
    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1658
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1664
    .end local v3    # "id_item":Ljava/lang/String;
    :cond_0
    const-string v7, "EmailSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "requestSendBroadcastIntent() ids="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666
    const-string v7, "id_array"

    invoke-virtual {v4, v7, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1669
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    const/4 v7, 0x2

    if-ne p3, v7, :cond_3

    .line 1670
    const-string v7, "is_read"

    move/from16 v0, p7

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1675
    :cond_2
    :goto_1
    const-string v7, "action"

    invoke-virtual {v4, v7, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1676
    const-string v7, "status"

    move/from16 v0, p6

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1678
    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1680
    const/4 v4, 0x0

    .line 1681
    return-void

    .line 1671
    :cond_3
    const/16 v7, 0x8

    if-ne p3, v7, :cond_2

    .line 1672
    const-string v7, "is_favorite"

    move/from16 v0, p7

    invoke-virtual {v4, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method public responsesParser()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2835
    :cond_0
    :goto_0
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 2836
    iget v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 2863
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->skipTag()V

    goto :goto_0

    .line 2838
    :pswitch_1
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->syncFetchRepliesParser()V

    goto :goto_0

    .line 2845
    :pswitch_2
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->newResponseAdds:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->addParser()Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2846
    :catch_0
    move-exception v0

    .line 2847
    .local v0, "sse":Lcom/android/exchange/CommandStatusException;
    iget v1, v0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 2853
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/android/exchange/CommandStatusException;->mItemId:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 2854
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v1, v1, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mMailboxIdAsString:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 2855
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "syncServerId=? and mailboxKey=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->this$0:Lcom/android/exchange/adapter/EmailSyncAdapter;

    iget-object v4, v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 2868
    .end local v0    # "sse":Lcom/android/exchange/CommandStatusException;
    :cond_1
    return-void

    .line 2836
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
