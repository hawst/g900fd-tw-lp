.class public Lcom/android/exchange/adapter/ParserUtility;
.super Ljava/lang/Object;
.source "ParserUtility.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 560
    return-void
.end method

.method public static addMessageData(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Message;I)V
    .locals 10
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "loopTag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    const/4 v2, 0x0

    .line 36
    .local v2, "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    :cond_0
    :goto_0
    invoke-virtual {p0, p2}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_9

    .line 37
    iget v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    sparse-switch v8, :sswitch_data_0

    .line 178
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 41
    :sswitch_0
    invoke-static {p0, v1, p1}, Lcom/android/exchange/adapter/ParserUtility;->attachmentsParser(Lcom/android/exchange/adapter/AbstractSyncParser;Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 44
    :sswitch_1
    iget v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    and-int/lit16 v8, v8, 0x100

    const/16 v9, 0x100

    if-ne v8, v9, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto :goto_0

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    goto :goto_0

    .line 52
    :sswitch_2
    iget v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    and-int/lit16 v8, v8, 0x100

    const/16 v9, 0x100

    if-ne v8, v9, :cond_2

    .line 53
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v3

    .line 56
    .local v3, "froms":[Lcom/android/emailcommon/mail/Address;
    if-eqz v3, :cond_3

    array-length v8, v3

    if-lez v8, :cond_3

    .line 57
    const/4 v8, 0x0

    aget-object v8, v3, v8

    invoke-virtual {v8}, Lcom/android/emailcommon/mail/Address;->toFriendly()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 59
    :cond_3
    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    goto :goto_0

    .line 64
    .end local v3    # "froms":[Lcom/android/emailcommon/mail/Address;
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    goto :goto_0

    .line 67
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    goto :goto_0

    .line 70
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    goto/16 :goto_0

    .line 73
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 74
    .local v6, "tempSubject":Ljava/lang/String;
    if-eqz v6, :cond_4

    .line 75
    const/16 v8, 0xa

    const/16 v9, 0x20

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    .line 77
    :cond_4
    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    goto/16 :goto_0

    .line 80
    .end local v6    # "tempSubject":Ljava/lang/String;
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    const/4 v8, 0x1

    :goto_1
    iput-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 83
    :sswitch_8
    invoke-static {p0, p1}, Lcom/android/exchange/adapter/ParserUtility;->bodyParser(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_0

    .line 87
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mUmCallerId:Ljava/lang/String;

    goto/16 :goto_0

    .line 90
    :sswitch_a
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mUmUserNotes:Ljava/lang/String;

    goto/16 :goto_0

    .line 104
    :sswitch_b
    new-instance v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    .end local v2    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    invoke-direct {v2}, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;-><init>()V

    .line 107
    .restart local v2    # "flags":Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    invoke-static {p0, v2}, Lcom/android/exchange/adapter/ParserUtility;->flagParser(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;)Ljava/lang/Boolean;

    .line 108
    iget-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    iput-wide v8, v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->MsgId:J

    .line 109
    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    iput-object v8, v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->MsgSyncServerId:Ljava/lang/String;

    .line 110
    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFollowupFlag:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;

    .line 111
    iget-object v8, v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_ACTIVE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v8, v9, :cond_6

    .line 112
    const/4 v8, 0x2

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    goto/16 :goto_0

    .line 113
    :cond_6
    iget-object v8, v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_COMPLETE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    if-ne v8, v9, :cond_7

    .line 114
    const/4 v8, 0x1

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    goto/16 :goto_0

    .line 116
    :cond_7
    const/4 v8, 0x0

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagStatus:I

    goto/16 :goto_0

    .line 122
    :sswitch_c
    const-string v8, "ParserUtility"

    const-string v9, "EMAIL_BODY_TRUNCATED1"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 126
    const/4 v8, 0x1

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 127
    const/4 v8, 0x2

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 128
    const-string v8, "ParserUtility"

    const-string v9, "EMAIL_BODY_TRUNCATED2"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 137
    :sswitch_d
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueOpaque()[B

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/android/exchange/adapter/AbstractSyncParser;->crypt([B)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationId:Ljava/lang/String;

    goto/16 :goto_0

    .line 142
    :sswitch_e
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueOpaque()[B

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mConversationIndex:[B

    goto/16 :goto_0

    .line 150
    :sswitch_f
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    goto/16 :goto_0

    .line 155
    :sswitch_10
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 156
    .local v7, "text":Ljava/lang/String;
    invoke-static {v7}, Lcom/android/exchange/adapter/ParserUtility;->decodeMsgClass(Ljava/lang/String;)I

    move-result v5

    .line 157
    .local v5, "msgClass":I
    const/4 v8, 0x2

    if-ne v5, v8, :cond_8

    .line 158
    const/4 v8, 0x1

    iput-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    goto/16 :goto_0

    .line 159
    :cond_8
    const/4 v8, 0x1

    if-ne v5, v8, :cond_0

    .line 160
    const/4 v8, 0x1

    iput-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    goto/16 :goto_0

    .line 165
    .end local v5    # "msgClass":I
    .end local v7    # "text":Ljava/lang/String;
    :sswitch_11
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v8

    iput v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mLastVerb:I

    goto/16 :goto_0

    .line 169
    :sswitch_12
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mLastVerbTime:J

    goto/16 :goto_0

    .line 174
    :sswitch_13
    invoke-static {p1, p0}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->parseLicense(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/exchange/adapter/AbstractSyncParser;)V

    goto/16 :goto_0

    .line 182
    :cond_9
    iget-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mEncrypted:Z

    if-eqz v8, :cond_a

    .line 183
    iget-object v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    const v9, 0x7f06001d

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 184
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "<html><body>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "</body></html>"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 187
    :cond_a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_10

    .line 191
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 192
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-nez v0, :cond_c

    .line 193
    const-string v8, "ParserUtility"

    const-string v9, "Attachment instance null. this should not be occur!"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 200
    :cond_c
    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    if-nez v8, :cond_d

    .line 201
    const/4 v8, 0x0

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 202
    const/4 v8, 0x1

    iput-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    goto :goto_2

    .line 209
    :cond_d
    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_b

    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v9, "image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 216
    iget-object v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    const-string v9, "application/octet-stream"

    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_e

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/android/exchange/adapter/ParserUtility;->findContentIdInHTMLBody(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 217
    :cond_e
    const/4 v8, 0x0

    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 218
    const/4 v8, 0x1

    iput-boolean v8, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    goto :goto_2

    .line 225
    .end local v0    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_f
    iput-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 227
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_10
    return-void

    .line 37
    :sswitch_data_0
    .sparse-switch
        0x86 -> :sswitch_0
        0x8e -> :sswitch_c
        0x8f -> :sswitch_5
        0x92 -> :sswitch_f
        0x93 -> :sswitch_10
        0x94 -> :sswitch_6
        0x95 -> :sswitch_7
        0x96 -> :sswitch_1
        0x97 -> :sswitch_3
        0x98 -> :sswitch_2
        0x99 -> :sswitch_4
        0xba -> :sswitch_b
        0x44a -> :sswitch_8
        0x44e -> :sswitch_0
        0x585 -> :sswitch_9
        0x586 -> :sswitch_a
        0x589 -> :sswitch_d
        0x58a -> :sswitch_e
        0x58b -> :sswitch_11
        0x58c -> :sswitch_12
        0x608 -> :sswitch_13
    .end sparse-switch
.end method

.method private static attachmentParser(Lcom/android/exchange/adapter/AbstractSyncParser;Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 13
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/adapter/AbstractSyncParser;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    const/4 v12, 0x1

    .line 427
    const/4 v2, 0x0

    .line 428
    .local v2, "fileName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 429
    .local v4, "length":Ljava/lang/String;
    const/4 v5, 0x0

    .line 431
    .local v5, "location":Ljava/lang/String;
    const/4 v1, 0x0

    .line 432
    .local v1, "contentId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 435
    .local v3, "isInline":I
    const/4 v8, 0x0

    .line 436
    .local v8, "umAttOrder":I
    const/4 v7, 0x0

    .line 438
    .local v7, "umAttDuration":I
    :cond_0
    :goto_0
    const/16 v9, 0x85

    invoke-virtual {p0, v9}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v9

    const/4 v10, 0x3

    if-eq v9, v10, :cond_2

    .line 439
    iget v9, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    sparse-switch v9, :sswitch_data_0

    .line 474
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 442
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 443
    goto :goto_0

    .line 447
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 448
    goto :goto_0

    .line 452
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 453
    goto :goto_0

    .line 456
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 457
    goto :goto_0

    .line 460
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v8

    .line 461
    goto :goto_0

    .line 464
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v7

    .line 465
    goto :goto_0

    .line 468
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 469
    .local v6, "tmp":Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "true"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 470
    :cond_1
    const/4 v3, 0x1

    goto :goto_0

    .line 478
    .end local v6    # "tmp":Ljava/lang/String;
    :cond_2
    const-string v9, "Attmt"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IsInline Value:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    if-eqz v2, :cond_8

    if-eqz v4, :cond_8

    if-eqz v5, :cond_8

    .line 483
    const-string v9, ".p7m"

    invoke-virtual {v2, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 484
    const-string v9, "/"

    const-string v10, ""

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 488
    :cond_3
    const-string v9, "S/MIME Encrypted Message"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, ".p7m"

    invoke-virtual {v5, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 489
    const-string v2, "smime.p7m"

    .line 491
    :cond_4
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;-><init>()V

    .line 493
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    .line 494
    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    .line 495
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    .line 496
    const-string v9, "base64"

    iput-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mEncoding:Ljava/lang/String;

    .line 497
    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getMimeTypeFromFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    .line 498
    iget-object v9, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    .line 503
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 504
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 507
    const/4 v9, 0x0

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3c

    if-ne v9, v10, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    const/16 v10, 0x3e

    if-ne v9, v10, :cond_5

    .line 508
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 510
    :cond_5
    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    .line 513
    :cond_6
    iput v7, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mVoiceMailAttDuration:I

    .line 514
    iput v8, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mVoiceMailAttOrder:I

    .line 515
    iput v3, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    .line 517
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    iget v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mIsInline:I

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 523
    :cond_7
    iput-boolean v12, p2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 526
    .end local v0    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_8
    return-void

    .line 439
    :sswitch_data_0
    .sparse-switch
        0x87 -> :sswitch_1
        0x88 -> :sswitch_2
        0x90 -> :sswitch_0
        0x44c -> :sswitch_2
        0x450 -> :sswitch_0
        0x451 -> :sswitch_1
        0x453 -> :sswitch_3
        0x455 -> :sswitch_6
        0x587 -> :sswitch_5
        0x588 -> :sswitch_4
    .end sparse-switch
.end method

.method private static attachmentsParser(Lcom/android/exchange/adapter/AbstractSyncParser;Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 2
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/adapter/AbstractSyncParser;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Attachment;",
            ">;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    .local p1, "atts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$Attachment;>;"
    :goto_0
    const/16 v0, 0x86

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 412
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    .line 419
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 416
    :sswitch_0
    invoke-static {p0, p1, p2}, Lcom/android/exchange/adapter/ParserUtility;->attachmentParser(Lcom/android/exchange/adapter/AbstractSyncParser;Ljava/util/ArrayList;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    goto :goto_0

    .line 422
    :cond_0
    return-void

    .line 412
    nop

    :sswitch_data_0
    .sparse-switch
        0x85 -> :sswitch_0
        0x44f -> :sswitch_0
    .end sparse-switch
.end method

.method private static bodyParser(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 5
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 278
    const-string v1, "1"

    .line 279
    .local v1, "bodyType":Ljava/lang/String;
    const-string v0, ""

    .line 280
    .local v0, "body":Ljava/lang/String;
    :cond_0
    :goto_0
    const/16 v2, 0x8c

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 281
    iget v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    sparse-switch v2, :sswitch_data_0

    .line 301
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 283
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 284
    goto :goto_0

    .line 286
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 287
    goto :goto_0

    .line 291
    :sswitch_2
    const-string v2, "ParserUtility"

    const-string v3, "BASE_TRUNCATED1"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 294
    iput v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 295
    const/4 v2, 0x2

    iput v2, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 296
    const-string v2, "ParserUtility"

    const-string v3, "BASE_TRUNCATED2"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 314
    :cond_1
    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 315
    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 321
    :goto_1
    return-void

    .line 317
    :cond_2
    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    goto :goto_1

    .line 281
    nop

    :sswitch_data_0
    .sparse-switch
        0x446 -> :sswitch_0
        0x44b -> :sswitch_1
        0x44d -> :sswitch_2
    .end sparse-switch
.end method

.method public static decodeMsgClass(Ljava/lang/String;)I
    .locals 2
    .param p0, "cl"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 263
    if-nez p0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return v0

    .line 265
    :cond_1
    const-string v1, "IPM.Note.SMIME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    const-string v1, "IPM.Note.SMIME"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 267
    const/4 v0, 0x2

    goto :goto_0

    .line 268
    :cond_2
    const-string v1, "IPM.Note.SMIME.MultipartSigned"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static findContentIdInHTMLBody(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p0, "bodyHtml"    # Ljava/lang/String;
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 232
    const-string v3, "<\\s?(?i)img\\s.[^>]+>"

    .line 235
    .local v3, "imgTagRe":Ljava/lang/String;
    const-string v2, "\\s+(?i)src=\"cid:.[^\"]+\""

    .line 237
    .local v2, "contentIdRe":Ljava/lang/String;
    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    .line 239
    const-string v8, "<\\s?(?i)img\\s.[^>]+>"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 240
    .local v7, "pIMG":Ljava/util/regex/Pattern;
    invoke-virtual {v7, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 244
    .local v5, "mIMG":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 246
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v1

    .line 247
    .local v1, "_imgTag":Ljava/lang/String;
    const-string v8, "\\s+(?i)src=\"cid:.[^\"]+\""

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 248
    .local v6, "pCID":Ljava/util/regex/Pattern;
    invoke-virtual {v6, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 250
    .local v4, "mCID":Ljava/util/regex/Matcher;
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 251
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "_cid":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 254
    const/4 v8, 0x1

    .line 258
    .end local v0    # "_cid":Ljava/lang/String;
    .end local v1    # "_imgTag":Ljava/lang/String;
    .end local v4    # "mCID":Ljava/util/regex/Matcher;
    .end local v5    # "mIMG":Ljava/util/regex/Matcher;
    .end local v6    # "pCID":Ljava/util/regex/Pattern;
    .end local v7    # "pIMG":Ljava/util/regex/Pattern;
    :goto_0
    return v8

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private static flagParser(Lcom/android/exchange/adapter/AbstractSyncParser;Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;)Ljava/lang/Boolean;
    .locals 8
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p1, "flags"    # Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 328
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 330
    .local v0, "state":Ljava/lang/Boolean;
    :goto_0
    const/16 v2, 0xba

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v2

    const/4 v5, 0x3

    if-eq v2, v5, :cond_3

    .line 331
    iget v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    sparse-switch v2, :sswitch_data_0

    .line 389
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 334
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v1

    .line 335
    .local v1, "status":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 336
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 337
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_ACTIVE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 338
    :cond_0
    if-ne v1, v3, :cond_1

    .line 339
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 340
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_COMPLETE:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 342
    :cond_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 343
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;->FOLLOWUP_STATUS_CLEARED:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->Status:Lcom/android/emailcommon/provider/EmailContent$FollowupFlag$FollowupFlagStatus;

    goto :goto_0

    .line 347
    .end local v1    # "status":I
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->FlagType:Ljava/lang/String;

    goto :goto_0

    .line 350
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 351
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CompleteTime:J

    goto :goto_0

    .line 354
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 355
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DateCompleted:J

    goto :goto_0

    .line 358
    :sswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 359
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->StartDate:J

    goto :goto_0

    .line 362
    :sswitch_5
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 363
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DueDate:J

    goto :goto_0

    .line 366
    :sswitch_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 367
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UTCDueDate:J

    goto/16 :goto_0

    .line 370
    :sswitch_7
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 371
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UTCStartDate:J

    goto/16 :goto_0

    .line 374
    :sswitch_8
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v2

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->ReminderSet:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    .line 377
    :sswitch_9
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 378
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->ReminderTime:J

    goto/16 :goto_0

    .line 381
    :sswitch_a
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    .line 382
    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->OrdinalDate:J

    goto/16 :goto_0

    .line 385
    :sswitch_b
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->SubOrdinalDate:Ljava/lang/String;

    goto/16 :goto_0

    .line 392
    :cond_3
    return-object v0

    .line 331
    nop

    :sswitch_data_0
    .sparse-switch
        0xbb -> :sswitch_0
        0xbd -> :sswitch_1
        0xbe -> :sswitch_2
        0x24b -> :sswitch_3
        0x24c -> :sswitch_5
        0x24d -> :sswitch_6
        0x25b -> :sswitch_8
        0x25c -> :sswitch_9
        0x25e -> :sswitch_4
        0x25f -> :sswitch_7
        0x262 -> :sswitch_a
        0x263 -> :sswitch_b
    .end sparse-switch
.end method

.method public static getMimeTypeFromFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 542
    const/16 v3, 0x2e

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 543
    .local v1, "lastDot":I
    const/4 v0, 0x0

    .line 544
    .local v0, "extension":Ljava/lang/String;
    if-lez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_0

    .line 545
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 547
    :cond_0
    if-nez v0, :cond_2

    .line 549
    const-string v2, "application/octet-stream"

    .line 556
    .local v2, "mimeType":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v2

    .line 551
    .end local v2    # "mimeType":Ljava/lang/String;
    :cond_2
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552
    .restart local v2    # "mimeType":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 553
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "application/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getTimeInMillis(Ljava/lang/String;)J
    .locals 2
    .param p0, "tstr"    # Ljava/lang/String;

    .prologue
    .line 397
    const-string v0, "GMT"

    invoke-static {p0, v0}, Lcom/android/exchange/adapter/ParserUtility;->getTimeInMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getTimeInMillis(Ljava/lang/String;Ljava/lang/String;)J
    .locals 8
    .param p0, "tstr"    # Ljava/lang/String;
    .param p1, "timezone"    # Ljava/lang/String;

    .prologue
    .line 401
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 402
    .local v0, "cal":Ljava/util/GregorianCalendar;
    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x8

    const/16 v4, 0xa

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xb

    const/16 v5, 0xd

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xe

    const/16 v6, 0x10

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0x11

    const/16 v7, 0x13

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 405
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 406
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method
