.class public Lcom/android/exchange/adapter/TasksSyncAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "TasksSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/TasksSyncAdapter$1;,
        Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;,
        Lcom/android/exchange/adapter/TasksSyncAdapter$RowBuilder;,
        Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;,
        Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;
    }
.end annotation


# static fields
.field private static final ID_PROJECTION:[Ljava/lang/String;

.field private static final PLACEHOLDER_OPERATION:Landroid/content/ContentProviderOperation;

.field private static final TASKS_SYNC_STATE_URI:Landroid/net/Uri;

.field static count:I


# instance fields
.field private final ExchangeType:Ljava/lang/String;

.field private final SIMPlannerTaskAuthrity:Ljava/lang/String;

.field public TAG:Ljava/lang/String;

.field private final Task_AccountName:Ljava/lang/String;

.field private final Task_AccountType:Ljava/lang/String;

.field mDeletedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mEmailAddress:Ljava/lang/String;

.field mInsertedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTasksAccountId:J

.field private mTasksAccountIdArgument:[Ljava/lang/String;

.field private mTasksAccountIdString:Ljava/lang/String;

.field mUpdatedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 88
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    .line 113
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->PLACEHOLDER_OPERATION:Landroid/content/ContentProviderOperation;

    .line 117
    sput v2, Lcom/android/exchange/adapter/TasksSyncAdapter;->count:I

    .line 904
    const-string v0, "content://syncstate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "state"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->TASKS_SYNC_STATE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 9
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 167
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 66
    const-string v0, "TasksSyncAdapter"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->TAG:Ljava/lang/String;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mInsertedIdList:Ljava/util/ArrayList;

    .line 119
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    .line 130
    const-string v0, "com.android.calendar"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->SIMPlannerTaskAuthrity:Ljava/lang/String;

    .line 131
    const-string v0, "account_name"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->Task_AccountName:Ljava/lang/String;

    .line 132
    const-string v0, "account_type"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->Task_AccountType:Ljava/lang/String;

    .line 133
    const-string v0, "com.android.exchange"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->ExchangeType:Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating Task SyncAdapter for MailBox Id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "_sync_account_key"

    aput-object v3, v2, v7

    const-string v3, "_sync_account=? AND _sync_account_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "com.android.exchange"

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 179
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 197
    :goto_0
    return-void

    .line 182
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    .line 188
    :goto_1
    iget-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdString:Ljava/lang/String;

    .line 189
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdString:Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdArgument:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 196
    const-string v0, "ExchTasksSyncAdapter"

    const-string v1, "Created "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 185
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->createTasksAccount(Lcom/android/exchange/EasSyncService;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 193
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 9
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 136
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 66
    const-string v0, "TasksSyncAdapter"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->TAG:Ljava/lang/String;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mInsertedIdList:Ljava/util/ArrayList;

    .line 119
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    .line 130
    const-string v0, "com.android.calendar"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->SIMPlannerTaskAuthrity:Ljava/lang/String;

    .line 131
    const-string v0, "account_name"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->Task_AccountName:Ljava/lang/String;

    .line 132
    const-string v0, "account_type"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->Task_AccountType:Ljava/lang/String;

    .line 133
    const-string v0, "com.android.exchange"

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->ExchangeType:Ljava/lang/String;

    .line 137
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating Task SyncAdapter for MailBox Id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    .line 141
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "_sync_account_key"

    aput-object v3, v2, v7

    const-string v3, "_sync_account=? AND _sync_account_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    aput-object v5, v4, v7

    const-string v5, "com.android.exchange"

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 146
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 163
    :goto_0
    return-void

    .line 149
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    .line 154
    :goto_1
    iget-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdString:Ljava/lang/String;

    .line 155
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdString:Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountIdArgument:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 162
    const-string v0, "ExchTasksSyncAdapter"

    const-string v1, "Created "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-static {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->createTasksAccount(Lcom/android/exchange/EasSyncService;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mTasksAccountId:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 159
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/TasksSyncAdapter;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Landroid/content/ContentProviderOperation;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->PLACEHOLDER_OPERATION:Landroid/content/ContentProviderOperation;

    return-object v0
.end method

.method public static asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 848
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static createTasksAccount(Lcom/android/exchange/EasSyncService;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)J
    .locals 13
    .param p0, "service"    # Lcom/android/exchange/EasSyncService;
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p2, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    .line 203
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v10, "cv":Landroid/content/ContentValues;
    const/4 v8, 0x0

    .line 207
    .local v8, "c":Landroid/database/Cursor;
    const-wide/16 v6, 0x0

    .line 209
    .local v6, "accountKey":J
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 210
    if-nez v8, :cond_1

    .line 211
    const-wide/16 v0, -0x1

    .line 218
    if-eqz v8, :cond_0

    .line 219
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 237
    :cond_0
    :goto_0
    return-wide v0

    .line 213
    :cond_1
    :try_start_1
    const-string v0, "_sync_account_key"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 214
    .local v9, "column_idx":I
    invoke-interface {v8}, Landroid/database/Cursor;->moveToLast()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v6

    .line 218
    :cond_2
    if-eqz v8, :cond_3

    .line 219
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 223
    :cond_3
    const-string v0, "displayName"

    iget-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "_sync_account"

    iget-object v1, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "_sync_account_key"

    const-wide/16 v2, 0x1

    add-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 227
    const-string v0, "_sync_account_type"

    const-string v1, "com.android.exchange"

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v0, "selected"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 230
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v12

    .line 232
    .local v12, "uri":Landroid/net/Uri;
    if-eqz v12, :cond_5

    .line 233
    invoke-virtual {v12}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 234
    .local v11, "stringId":Ljava/lang/String;
    iput-object v11, p2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncStatus:Ljava/lang/String;

    .line 235
    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 218
    .end local v9    # "column_idx":I
    .end local v11    # "stringId":Ljava/lang/String;
    .end local v12    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 219
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 237
    .restart local v9    # "column_idx":I
    .restart local v12    # "uri":Landroid/net/Uri;
    :cond_5
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private getClientId(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "column_name"    # Ljava/lang/String;

    .prologue
    .line 481
    invoke-interface {p1, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 482
    .local v0, "column_idx":I
    const/4 v1, 0x0

    .line 484
    .local v1, "tasks_client_id":Ljava/lang/String;
    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 486
    :cond_0
    return-object v1
.end method

.method private sendData(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    const-string v1, "The sending task data  "

    const-string v2, " tasks"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v1, v1, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 402
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_base_body(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V

    .line 414
    :cond_0
    :goto_0
    const-string v1, "The sending task categories  "

    const-string v2, " tasks"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_categories(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V

    .line 417
    const-string v1, "subject"

    const/16 v2, 0x260

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 418
    const-string v1, "importance"

    const/16 v2, 0x24e

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 419
    const-string v1, "utc_due_date"

    const/16 v2, 0x24d

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 420
    const-string v1, "utc_start_date"

    const/16 v2, 0x25f

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 421
    const-string v1, "start_date"

    const/16 v2, 0x25e

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 422
    const-string v1, "complete"

    const/16 v2, 0x24a

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 426
    const-string v1, "sensitivity"

    const/16 v2, 0x25d

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 427
    const-string v1, "reminder_set"

    const/16 v2, 0x25b

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 430
    const-string v1, "reminder_time"

    const/16 v2, 0x25c

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 431
    const-string v1, "due_date"

    const/16 v2, 0x24c

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 433
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_recurrence(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V

    .line 434
    return-void

    .line 405
    :cond_1
    const-string v1, "body_size"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 407
    .local v0, "column_idx":I
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v2, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 409
    const-string v1, "body_truncated"

    const/16 v2, 0x247

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 410
    const-string v1, "body"

    const/16 v2, 0x245

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private send_tasks_base_body(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 335
    const-string v1, "body_size"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 338
    .local v0, "column_idx":I
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v2, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 339
    const/16 v1, 0x44a

    invoke-virtual {p1, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 340
    const-string v1, "bodyType"

    const/16 v2, 0x446

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 341
    const-string v1, "body_size"

    const/16 v2, 0x44c

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 342
    const-string v1, "body_truncated"

    const/16 v2, 0x44d

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 347
    const-string v1, "body"

    const/16 v2, 0x44b

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 348
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 350
    :cond_0
    return-void
.end method

.method private send_tasks_categories(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x249

    .line 353
    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->tasks_has_category(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 354
    const/16 v0, 0x248

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 355
    const-string v0, "category1"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 356
    const-string v0, "category2"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 357
    const-string v0, "category3"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 358
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 360
    :cond_0
    return-void
.end method

.method private send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V
    .locals 4
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "column_name"    # Ljava/lang/String;
    .param p4, "tag_name"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 319
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 320
    .local v0, "column_idx":I
    const/4 v1, 0x0

    .line 322
    .local v1, "tasks_item_value":Ljava/lang/String;
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 323
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-virtual {p1, p4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 326
    const-string v2, "complete"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 327
    const-string v2, "date_completed"

    const/16 v3, 0x24b

    invoke-direct {p0, p1, p2, v2, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 331
    :cond_0
    return-void
.end method

.method private send_tasks_recurrence(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 363
    const-string v0, "recurrence_type"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    :goto_0
    return-void

    .line 366
    :cond_0
    const/16 v0, 0x24f

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 368
    const-string v0, "recurrence_regenerate"

    const/16 v1, 0x259

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 369
    const-string v0, "recurrence_dead_occur"

    const/16 v1, 0x25a

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 370
    const-string v0, "recurrence_type"

    const/16 v1, 0x250

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 371
    const-string v0, "recurrence_start"

    const/16 v1, 0x251

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 372
    const-string v0, "recurrence_until"

    const/16 v1, 0x252

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 376
    const-string v0, "recurrence_interval"

    const/16 v1, 0x254

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 377
    const-string v0, "recurrence_day_of_month"

    const/16 v1, 0x255

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 378
    const-string v0, "recurrence_day_of_week"

    const/16 v1, 0x256

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 379
    const-string v0, "recurrence_week_of_month"

    const/16 v1, 0x257

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 380
    const-string v0, "recurrence_month_of_year"

    const/16 v1, 0x258

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 381
    const-string v0, "recurrence_occurrences"

    const/16 v1, 0x253

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->send_tasks_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V

    .line 383
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_0
.end method

.method private send_tasks_time_item(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;Ljava/lang/String;I)V
    .locals 4
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "column_name"    # Ljava/lang/String;
    .param p4, "tag_name"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 388
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 389
    .local v0, "column_idx":I
    const-wide/16 v2, 0x0

    .line 391
    .local v2, "time_value":J
    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 392
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 393
    invoke-direct {p0, v2, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter;->setTimeInMillis(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, p4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 395
    :cond_0
    return-void
.end method

.method private setTimeInMillis(J)Ljava/lang/String;
    .locals 5
    .param p1, "millis"    # J

    .prologue
    .line 254
    new-instance v3, Landroid/text/format/Time;

    const-string v4, "UTC"

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 255
    .local v3, "utcTime":Landroid/text/format/Time;
    invoke-virtual {v3, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 256
    const-string v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    .line 257
    .local v2, "pattern":Ljava/lang/String;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 258
    .local v0, "date":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v4, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v1, v2, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 259
    .local v1, "df":Ljava/text/SimpleDateFormat;
    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 260
    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private syncTasks(Lcom/android/exchange/adapter/Serializer;Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;)V
    .locals 18
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "retVal"    # Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 701
    sget-object v2, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v16

    .line 702
    .local v16, "taskSyncUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v11, v2, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 705
    .local v11, "cr":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 706
    .local v9, "c1":Landroid/database/Cursor;
    const-wide/16 v14, -0x1

    .line 708
    .local v14, "sync_account_key":J
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_sync_account_key"

    aput-object v6, v4, v5

    const-string v5, "_sync_account=? AND _sync_account_type=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v6, v7

    const/4 v7, 0x1

    const-string v17, "com.android.exchange"

    aput-object v17, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 713
    if-nez v9, :cond_1

    .line 720
    if-eqz v9, :cond_0

    .line 721
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 782
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 717
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v14

    .line 720
    :cond_2
    if-eqz v9, :cond_3

    .line 721
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 724
    :cond_3
    const/4 v8, 0x0

    .line 726
    .local v8, "c":Landroid/database/Cursor;
    const/4 v4, 0x0

    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_sync_dirty= 1 AND accountKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v11

    move-object/from16 v3, v16

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v8

    .line 729
    if-nez v8, :cond_5

    .line 778
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 779
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 720
    .end local v8    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_4

    .line 721
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    .line 732
    .restart local v8    # "c":Landroid/database/Cursor;
    :cond_5
    :try_start_3
    const-string v2, "ExchTasksSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New tasks count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    if-nez v2, :cond_6

    .line 778
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 779
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 738
    :cond_6
    const/4 v2, 0x0

    :try_start_4
    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    .line 739
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 741
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;->getVal()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 742
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 743
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Sending Tasks changes to the server"

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 744
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;->setVal(Z)V

    .line 746
    :cond_8
    const-string v2, "clientId"

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->getClientId(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 747
    .local v10, "clientId":Ljava/lang/String;
    const-string v2, "sourceid"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 748
    .local v13, "sourceId":Ljava/lang/String;
    const-string v2, "deleted"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 749
    .local v12, "isDelete":I
    const/4 v2, 0x1

    if-ne v12, v2, :cond_a

    const/16 v2, 0xc7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    if-le v2, v3, :cond_a

    .line 750
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    .line 751
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v2, v3, v13}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 752
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 753
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v2

    if-nez v2, :cond_7

    .line 778
    .end local v10    # "clientId":Ljava/lang/String;
    .end local v12    # "isDelete":I
    .end local v13    # "sourceId":Ljava/lang/String;
    :cond_9
    :goto_2
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 779
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 756
    .restart local v10    # "clientId":Ljava/lang/String;
    .restart local v12    # "isDelete":I
    .restart local v13    # "sourceId":Ljava/lang/String;
    :cond_a
    if-nez v13, :cond_c

    if-eqz v10, :cond_c

    const/16 v2, 0xc7

    :try_start_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    if-le v2, v3, :cond_c

    .line 758
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    .line 759
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v10}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 760
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 761
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mInsertedIdList:Ljava/util/ArrayList;

    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8}, Lcom/android/exchange/adapter/TasksSyncAdapter;->sendData(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;)V

    .line 774
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 778
    .end local v10    # "clientId":Ljava/lang/String;
    .end local v12    # "isDelete":I
    .end local v13    # "sourceId":Ljava/lang/String;
    :catchall_1
    move-exception v2

    if-eqz v8, :cond_b

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_b

    .line 779
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v2

    .line 762
    .restart local v10    # "clientId":Ljava/lang/String;
    .restart local v12    # "isDelete":I
    .restart local v13    # "sourceId":Ljava/lang/String;
    :cond_c
    if-eqz v13, :cond_d

    const/16 v2, 0xc7

    :try_start_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    if-le v2, v3, :cond_d

    .line 763
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    .line 764
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xd

    invoke-virtual {v2, v3, v13}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 765
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 766
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 768
    :cond_d
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TasksSyncAdapter Max limit of local changes reached, stop sendLocalChanges; mSyncLocalChangesCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mSyncLocalChangesCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->userLog([Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_2
.end method

.method private tasks_has_category(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 311
    const-string v0, "category1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "category2"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "category3"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 936
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cleanup()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 812
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mInsertedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 813
    .local v2, "id":Ljava/lang/Long;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 814
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "_sync_dirty"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 815
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v6, "com.android.exchange"

    invoke-static {v4, v5, v6}, Lcom/android/exchange/adapter/TasksSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 821
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "id":Ljava/lang/Long;
    :cond_0
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 822
    .restart local v2    # "id":Ljava/lang/Long;
    const-string v3, "ExchTasksSyncAdapter:CleanUp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Going to  cleanup the mDeleted list "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/Tasks;->DELETED_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v6, "com.android.exchange"

    invoke-static {v4, v5, v6}, Lcom/android/exchange/adapter/TasksSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 830
    .end local v2    # "id":Ljava/lang/Long;
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 831
    .restart local v2    # "id":Ljava/lang/Long;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 832
    .restart local v0    # "cv":Landroid/content/ContentValues;
    const-string v3, "_sync_dirty"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 833
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v6, "com.android.exchange"

    invoke-static {v4, v5, v6}, Lcom/android/exchange/adapter/TasksSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 842
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "id":Ljava/lang/Long;
    :cond_2
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mInsertedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 843
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 844
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 845
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    const-string v0, "Tasks"

    return-object v0
.end method

.method protected getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;
    .locals 2
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2043
    new-instance v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/TasksSyncAdapter;Z)V

    return-object v0
.end method

.method public getSyncKey()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 913
    const-string v0, "0"

    .line 914
    .local v0, "syncKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 915
    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    .line 916
    :cond_0
    const-string v1, "ExchTasksSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSyncKey "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    return-object v0
.end method

.method public isSyncable()Z
    .locals 2

    .prologue
    .line 2038
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccountManagerAccount:Landroid/accounts/Account;

    const-string v1, "tasks"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 8
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 881
    const/4 v1, 0x0

    .line 883
    .local v1, "p":Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
    :try_start_0
    new-instance v2, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;

    invoke-direct {v2, p0, p1, p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/TasksSyncAdapter;)V
    :try_end_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_0

    .line 894
    .end local v1    # "p":Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
    .local v2, "p":Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
    invoke-virtual {v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->parse()Z

    move-result v3

    move-object v1, v2

    .end local v2    # "p":Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
    .restart local v1    # "p":Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
    :goto_0
    return v3

    .line 884
    :catch_0
    move-exception v0

    .line 886
    .local v0, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpl-double v3, v4, v6

    if-nez v3, :cond_0

    .line 887
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->setIntervalPing()V

    .line 888
    const/4 v3, 0x0

    goto :goto_0

    .line 890
    :cond_0
    throw v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 4
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 661
    const/4 v2, 0x0

    .line 690
    :goto_0
    return v2

    .line 664
    :cond_0
    new-instance v1, Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;)V

    .line 679
    .local v1, "retVal":Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter;->syncTasks(Lcom/android/exchange/adapter/Serializer;Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;)V

    .line 682
    invoke-virtual {v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$ReturnVal;->getVal()Z

    move-result v2

    if-nez v2, :cond_1

    .line 683
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :cond_1
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 687
    :catch_0
    move-exception v0

    .line 688
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 1
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2025
    if-nez p3, :cond_0

    .line 2026
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/exchange/adapter/TasksSyncAdapter;->setPimSyncOptions(Ljava/lang/Double;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 2028
    :cond_0
    return-void
.end method

.method public setSyncKey(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "syncKey"    # Ljava/lang/String;
    .param p2, "inCommands"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 926
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object p1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    .line 927
    const-string v0, "ExchTasksSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSyncKey "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    return-void
.end method

.method public wipe()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 857
    const-string v4, "ExchTasksSyncAdapter"

    const-string v5, "Inside wipe..."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "accountName like \'%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 859
    .local v3, "select":Ljava/lang/String;
    const-string v4, "ExchTasksSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Select value..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    iget-object v4, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 865
    const-string v4, "ExchTasksSyncAdapter"

    const-string v5, "TASKS BAD SYNC KEY"

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 868
    .local v0, "cvx":Landroid/content/ContentValues;
    const-string v4, "syncKey"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 870
    .local v1, "mMailboxUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/android/exchange/adapter/TasksSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v1, v0, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 871
    .local v2, "mailboxCount":I
    const-string v4, "ExchTasksSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating Tasks mailbox with sync key 0. Count = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    return-void
.end method
