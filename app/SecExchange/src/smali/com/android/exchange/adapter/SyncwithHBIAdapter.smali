.class public Lcom/android/exchange/adapter/SyncwithHBIAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "SyncwithHBIAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;
    }
.end annotation


# instance fields
.field public absSyncAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

.field public absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

.field syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 24
    iput-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncParser:Lcom/android/exchange/adapter/AbstractSyncParser;

    .line 26
    iput-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    .line 28
    iput-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;

    .line 32
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public findAdapterObject()Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .locals 3

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    .local v0, "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x42

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x53

    if-ne v1, v2, :cond_1

    .line 138
    :cond_0
    new-instance v0, Lcom/android/exchange/adapter/ContactsSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/ContactsSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 170
    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :goto_0
    return-object v0

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x41

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x52

    if-ne v1, v2, :cond_3

    .line 141
    :cond_2
    new-instance v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_0

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x45

    if-ne v1, v2, :cond_4

    .line 143
    new-instance v0, Lcom/android/exchange/adapter/NotesSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/NotesSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_0

    .line 147
    :cond_4
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x43

    if-ne v1, v2, :cond_5

    .line 149
    new-instance v0, Lcom/android/exchange/adapter/TasksSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_0

    .line 152
    :cond_5
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v2, 0x61

    if-ne v1, v2, :cond_6

    .line 153
    new-instance v0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_0

    .line 167
    :cond_6
    new-instance v0, Lcom/android/exchange/adapter/EmailSyncAdapter;

    .end local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    iget-object v1, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v0    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto :goto_0
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    .line 43
    :try_start_0
    new-instance v2, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;

    invoke-direct {v2, p0, p1, p0}, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;-><init>(Lcom/android/exchange/adapter/SyncwithHBIAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/SyncwithHBIAdapter;)V

    iput-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;
    :try_end_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;

    invoke-virtual {v2}, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->parse()Z

    move-result v1

    .line 56
    :goto_0
    return v1

    .line 44
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v2, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v4, 0x4028333333333333L    # 12.1

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->setIntervalPing()V

    .line 48
    const/4 v1, 0x0

    goto :goto_0

    .line 50
    :cond_0
    throw v0
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 1
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    return-void
.end method

.method public wipe()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->syncWithHBIParser:Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;

    iget-object v0, v0, Lcom/android/exchange/adapter/SyncwithHBIAdapter$SyncWithHBIParser;->serverId:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->findAdapterObject()Lcom/android/exchange/adapter/AbstractSyncAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    .line 177
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;->absSyncAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->wipe()V

    .line 181
    :cond_0
    return-void
.end method
