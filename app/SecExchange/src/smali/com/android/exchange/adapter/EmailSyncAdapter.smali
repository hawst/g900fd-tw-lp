.class public Lcom/android/exchange/adapter/EmailSyncAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "EmailSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;,
        Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;,
        Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;,
        Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;
    }
.end annotation


# static fields
.field private static final BODY_ID_MESSAGE_KEY_PROJECTION:[Ljava/lang/String;

.field private static FIRST_COMMAND:Ljava/lang/String;

.field private static final MESSAGE_ID_SUBJECT_PROJECTION:[Ljava/lang/String;

.field private static SYNC_CHANGED_FIRST:Ljava/lang/String;

.field private static final UPDATES_PROJECTION:[Ljava/lang/String;


# instance fields
.field draftChanges:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field fDeletedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field fUpdatedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private isInlineImage:Z

.field private isLookBackChanged:Z

.field private isNewMsg:Z

.field private isSigned:Z

.field private latestCommitMessageId:J

.field private latestCommitTimeStamp:J

.field mBindArgument:[Ljava/lang/String;

.field mBindArguments:[Ljava/lang/String;

.field private mChangedMessageCount:I

.field mDeletedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mFakeInboxId:J

.field private mFetchNeeded:Z

.field mFetchRequestList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;",
            ">;"
        }
    .end annotation
.end field

.field mIsLooping:Z

.field private mNotifyCount:I

.field private mPrevSyncKey:Ljava/lang/String;

.field private mSpamCount:I

.field private mUpdateDeleteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;",
            ">;"
        }
    .end annotation
.end field

.field protected mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

.field mUpdatedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private maybeMemoryFull:Z

.field private sumOfNewMsg:I

.field private tempMimeFile:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "flagRead"

    aput-object v1, v0, v3

    const-string v1, "mailboxKey"

    aput-object v1, v0, v4

    const-string v1, "syncServerId"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "followupflag"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "messageId"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->UPDATES_PROJECTION:[Ljava/lang/String;

    .line 131
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "subject"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->MESSAGE_ID_SUBJECT_PROJECTION:[Ljava/lang/String;

    .line 136
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "messageKey"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->BODY_ID_MESSAGE_KEY_PROJECTION:[Ljava/lang/String;

    .line 150
    const-string v0, "firstCommand"

    sput-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->FIRST_COMMAND:Ljava/lang/String;

    .line 151
    const-string v0, "syncChangeFirst"

    sput-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->SYNC_CHANGED_FIRST:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 6
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 229
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 168
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;

    .line 176
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    .line 177
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->draftChanges:Ljava/util/ArrayList;

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    .line 191
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z

    .line 194
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mIsLooping:Z

    .line 196
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z

    .line 199
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z

    .line 201
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    .line 204
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    .line 207
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    .line 209
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mPrevSyncKey:Ljava/lang/String;

    .line 212
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    .line 216
    iput-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    .line 218
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I

    .line 236
    iput-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J

    .line 238
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J

    .line 241
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z

    .line 244
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isLookBackChanged:Z

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    .line 248
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z

    .line 231
    invoke-static {}, Lcom/android/exchange/ExchangeService$UpdateHistory;->getInstance()Lcom/android/exchange/ExchangeService$UpdateHistory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    .line 233
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z

    .line 234
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 6
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 221
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 168
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;

    .line 176
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArguments:[Ljava/lang/String;

    .line 177
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    .line 184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->draftChanges:Ljava/util/ArrayList;

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    .line 191
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z

    .line 194
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mIsLooping:Z

    .line 196
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z

    .line 199
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z

    .line 201
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    .line 204
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    .line 207
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    .line 209
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mPrevSyncKey:Ljava/lang/String;

    .line 212
    iput-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    .line 216
    iput-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    .line 218
    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I

    .line 236
    iput-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J

    .line 238
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J

    .line 241
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z

    .line 244
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isLookBackChanged:Z

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    .line 248
    iput-boolean v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z

    .line 223
    invoke-static {}, Lcom/android/exchange/ExchangeService$UpdateHistory;->getInstance()Lcom/android/exchange/ExchangeService$UpdateHistory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    .line 225
    return-void
.end method

.method static synthetic access$100(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/android/exchange/adapter/EmailSyncAdapter;Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->isMessageBlockedByUser(Lcom/android/emailcommon/provider/EmailContent$Message;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->maybeMemoryFull:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    return v0
.end method

.method static synthetic access$1102(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    return p1
.end method

.method static synthetic access$1108(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSpamCount:I

    return v0
.end method

.method static synthetic access$1200(Lcom/android/exchange/adapter/EmailSyncAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitTimeStamp:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/android/exchange/adapter/EmailSyncAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/android/exchange/adapter/EmailSyncAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->latestCommitMessageId:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isSigned:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isInlineImage:Z

    return p1
.end method

.method static synthetic access$1602(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mPrevSyncKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->checkNumberOfMessageToSync()I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/android/exchange/adapter/EmailSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->canSyncContinue()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/EmailSyncAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->resetMessageDirtyCommit(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$202(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->tempMimeFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->addCleanupOps(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->insertBlacklistMessage(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/android/exchange/adapter/EmailSyncAdapter;Landroid/content/Context;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->requestSendBroadcastIntentBlacklistModule(Landroid/content/Context;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I

    return v0
.end method

.method static synthetic access$2412(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->sumOfNewMsg:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/exchange/adapter/EmailSyncAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteTempFile(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->MESSAGE_ID_SUBJECT_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/android/exchange/adapter/EmailSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z

    return p1
.end method

.method static synthetic access$700()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->UPDATES_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    return p1
.end method

.method static synthetic access$808(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mChangedMessageCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    return v0
.end method

.method static synthetic access$902(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    return p1
.end method

.method static synthetic access$908(Lcom/android/exchange/adapter/EmailSyncAdapter;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    return v0
.end method

.method static synthetic access$920(Lcom/android/exchange/adapter/EmailSyncAdapter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/EmailSyncAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iget v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mNotifyCount:I

    return v0
.end method

.method private addCleanupOps(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3802
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 3803
    .local v1, "id":Ljava/lang/Long;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3807
    .end local v1    # "id":Ljava/lang/Long;
    :cond_0
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 3810
    .restart local v1    # "id":Ljava/lang/Long;
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v2

    .line 3811
    .local v2, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v2, :cond_2

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagMoved:I

    if-nez v3, :cond_1

    .line 3812
    :cond_2
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3819
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_3
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 3820
    .restart local v1    # "id":Ljava/lang/Long;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 3823
    .end local v1    # "id":Ljava/lang/Long;
    :cond_4
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 3824
    .restart local v1    # "id":Ljava/lang/Long;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DELETED_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 3833
    .end local v1    # "id":Ljava/lang/Long;
    :cond_5
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 3834
    iget-boolean v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isNewMsg:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 3835
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "mailboxKey=? AND (flags&512)!=0"

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3838
    :cond_6
    return-void
.end method

.method private canSyncContinue()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3740
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 3741
    const-string v3, "canSyncContinue : sync schedule is manual : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    .line 3777
    :goto_0
    return v2

    .line 3745
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v4, "com.android.exchange"

    invoke-direct {v0, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3747
    .local v0, "acct":Landroid/accounts/Account;
    const-string v3, "com.android.email.provider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v3

    if-gtz v3, :cond_1

    .line 3748
    const-string v3, "canSyncContinue : getIsSyncable : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3751
    :cond_1
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3752
    const-string v3, "canSyncContinue : getMasterSyncAutomatically : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3755
    :cond_2
    const-string v3, "com.android.email.provider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3756
    const-string v3, "canSyncContinue : getSyncAutomatically : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3760
    :cond_3
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v1

    .line 3761
    .local v1, "isRoaming":Z
    if-eqz v1, :cond_5

    .line 3762
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-nez v3, :cond_4

    .line 3763
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v4

    iput-object v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    .line 3765
    :cond_4
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mPolicySet:Lcom/android/emailcommon/service/PolicySet;

    iget-boolean v3, v3, Lcom/android/emailcommon/service/PolicySet;->mRequireManualSyncWhenRoaming:Z

    if-eqz v3, :cond_5

    .line 3766
    const-string v3, "canSyncContinue : mRequireManualSyncWhenRoaming is set true : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3771
    :cond_5
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/emailcommon/utility/SyncScheduleData;->getRoamingSchedule()I

    move-result v3

    if-nez v3, :cond_6

    if-eqz v1, :cond_6

    .line 3773
    const-string v3, "canSyncContinue : romaing schedule is manual : false"

    invoke-direct {p0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 3776
    :cond_6
    const-string v2, "canSyncContinue : true"

    invoke-direct {p0, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->log(Ljava/lang/String;)V

    .line 3777
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkFakeInbox()V
    .locals 6

    .prologue
    .line 552
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/16 v1, 0x200

    invoke-static {v0, v2, v3, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    .line 554
    iget-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 555
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fake inbox found, Id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 557
    :cond_0
    return-void
.end method

.method private checkNumberOfMessageToSync()I
    .locals 4

    .prologue
    .line 3690
    const/4 v1, 0x0

    .line 3691
    .local v1, "filter":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    packed-switch v3, :pswitch_data_0

    .line 3711
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    .line 3714
    :goto_0
    const/4 v2, 0x0

    .line 3716
    .local v2, "item":I
    :try_start_0
    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getEstimate(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 3722
    :goto_1
    return v2

    .line 3693
    .end local v2    # "item":I
    :pswitch_0
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_DAY:Ljava/lang/String;

    .line 3694
    goto :goto_0

    .line 3696
    :pswitch_1
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_3_DAYS:Ljava/lang/String;

    .line 3697
    goto :goto_0

    .line 3699
    :pswitch_2
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    .line 3700
    goto :goto_0

    .line 3702
    :pswitch_3
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    .line 3703
    goto :goto_0

    .line 3705
    :pswitch_4
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_MONTH:Ljava/lang/String;

    .line 3706
    goto :goto_0

    .line 3708
    :pswitch_5
    const-string v1, "0"

    .line 3709
    goto :goto_0

    .line 3717
    .restart local v2    # "item":I
    :catch_0
    move-exception v0

    .line 3719
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 3691
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private clearDirtyCommitMsgs(Ljava/lang/String;)V
    .locals 12
    .param p1, "mailboxId"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 4456
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 4457
    .local v6, "tempOps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz p1, :cond_1

    .line 4458
    const-string v3, "dirtyCommit = ? and mailboxKey= ?"

    .line 4459
    .local v3, "dirtyMessagesSelection":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v2, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v10

    .line 4460
    .local v2, "dirtyMessagesArguments":[Ljava/lang/String;
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7, v3, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->getMessagesIdsWhere(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)[J

    move-result-object v1

    .line 4461
    .local v1, "dirtyMessages":[J
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    .line 4462
    .local v0, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    array-length v7, v1

    if-lez v7, :cond_0

    .line 4463
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v7, v1

    if-ge v5, v7, :cond_0

    .line 4464
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    aget-wide v10, v1, v5

    invoke-static {v0, v8, v9, v10, v11}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMessageBodyFilesUri(Landroid/content/Context;JJ)V

    .line 4463
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 4467
    .end local v5    # "i":I
    :cond_0
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7, v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4470
    :try_start_0
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "com.android.email.provider"

    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4482
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "dirtyMessages":[J
    .end local v2    # "dirtyMessagesArguments":[Ljava/lang/String;
    .end local v3    # "dirtyMessagesSelection":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 4476
    .restart local v0    # "context":Landroid/content/Context;
    .restart local v1    # "dirtyMessages":[J
    .restart local v2    # "dirtyMessagesArguments":[Ljava/lang/String;
    .restart local v3    # "dirtyMessagesSelection":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 4477
    .local v4, "e":Ljava/lang/Exception;
    sget-boolean v7, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v7, :cond_1

    .line 4478
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EmailSyncAdapter<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ">"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Uncaught exception in ExchangeSyncService"

    invoke-static {v7, v8, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 4474
    .end local v4    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v7

    goto :goto_1

    .line 4472
    :catch_2
    move-exception v7

    goto :goto_1
.end method

.method private completeSpecialInboxWipeHandling()V
    .locals 6

    .prologue
    .line 373
    const-string v1, "EmailSyncAdapter"

    const-string v2, "completeSpecialInboxWipeHandling start"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteAllMailboxMessages(Landroid/content/Context;JJ)V

    .line 376
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->moveMessagesFromFakeToRealInbox()V

    .line 377
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteFakeInbox()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    :goto_0
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EmailSyncAdapter"

    invoke-static {v1, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 380
    const-string v1, "EmailSyncAdapter"

    const-string v2, "completeSpecialInboxWipeHandling: exception, make common wipe to avoid lost messages"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->wipe()V

    goto :goto_0
.end method

.method private static deleteAllMailboxMessages(Landroid/content/Context;JJ)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxId"    # J

    .prologue
    .line 289
    const-string v1, "EmailSyncAdapter"

    const-string v2, "deleteAllMailboxMessages() start"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    if-eqz p0, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v1, p3, v2

    if-eqz v1, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 291
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 292
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 293
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mailboxKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 295
    .local v7, "messageCount":I
    const-string v1, "EmailSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EmailProvider Message table row deleted. Count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mailboxKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 301
    .local v8, "msgDeletedCount":I
    const-string v1, "EmailSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EmailProvider Message_Deletes table row deleted. Count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mailboxKey="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v9

    .line 306
    .local v9, "msgUpdatedCount":I
    const-string v1, "EmailSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EmailProvider Message_Updates row deleted. Count = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllMailboxAttachmentFiles(Landroid/content/Context;JJ)V

    .line 312
    const/4 v6, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMailboxBodyFiles(Landroid/content/Context;JJZ)V

    .line 315
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v7    # "messageCount":I
    .end local v8    # "msgDeletedCount":I
    .end local v9    # "msgUpdatedCount":I
    :cond_0
    return-void
.end method

.method private deleteFakeInbox()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const-wide/16 v8, -0x1

    .line 337
    const-string v1, "EmailSyncAdapter"

    const-string v2, "deleteFakeInbox() start"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v1, :cond_1

    .line 339
    iget-wide v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    cmp-long v1, v2, v8

    if-nez v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/16 v4, 0x200

    invoke-static {v1, v2, v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    .line 342
    :cond_0
    iget-wide v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 343
    const-string v1, "EmailSyncAdapter"

    const-string v2, "deleteFakeInbox() fake Inbox found, delete it"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllMailboxAttachmentFiles(Landroid/content/Context;JJ)V

    .line 346
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMailboxBodyFiles(Landroid/content/Context;JJZ)V

    .line 347
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    .line 349
    .local v7, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v7, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 350
    iput-wide v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    .line 352
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->setBadSyncAccount(JZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_0
    return-void

    .line 353
    .restart local v7    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EmailSyncAdapter"

    invoke-static {v1, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private deleteTempFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 4645
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 4646
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4651
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 4647
    :catch_0
    move-exception v0

    .line 4649
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EmailSyncAdapter"

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private getDate(J)Ljava/lang/String;
    .locals 5
    .param p1, "timeval"    # J

    .prologue
    .line 4402
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/GregorianCalendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 4403
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 4404
    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 4405
    :cond_0
    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->formatDateTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getEstimate(Ljava/lang/String;)I
    .locals 14
    .param p1, "filter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 772
    new-instance v8, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v8}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 773
    .local v8, "s":Lcom/android/exchange/adapter/Serializer;
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v10, v10, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    const-wide/high16 v12, 0x402c000000000000L    # 14.0

    cmpl-double v10, v10, v12

    if-ltz v10, :cond_1

    const/4 v4, 0x1

    .line 774
    .local v4, "ex10":Z
    :goto_0
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v10, v10, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    const-wide/high16 v12, 0x4028000000000000L    # 12.0

    cmpg-double v10, v10, v12

    if-gez v10, :cond_2

    const/4 v2, 0x1

    .line 775
    .local v2, "ex03":Z
    :goto_1
    if-nez v4, :cond_3

    if-nez v2, :cond_3

    const/4 v3, 0x1

    .line 777
    .local v3, "ex07":Z
    :goto_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v0

    .line 778
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v9

    .line 779
    .local v9, "syncKey":Ljava/lang/String;
    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "gie, sending "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v0, v10, v11

    const/4 v11, 0x2

    const-string v12, " syncKey: "

    aput-object v12, v10, v11

    const/4 v11, 0x3

    aput-object v9, v10, v11

    invoke-virtual {p0, v10}, Lcom/android/exchange/adapter/EmailSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 781
    const/16 v10, 0x185

    invoke-virtual {v8, v10}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    const/16 v11, 0x187

    invoke-virtual {v10, v11}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 782
    const/16 v10, 0x188

    invoke-virtual {v8, v10}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 783
    if-eqz v3, :cond_4

    .line 785
    const/16 v10, 0x18a

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 786
    const/16 v10, 0x18

    invoke-virtual {v8, v10, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 787
    const/16 v10, 0xb

    invoke-virtual {v8, v10, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 801
    :goto_3
    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 807
    iget-object v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    const-string v11, "GetItemEstimate"

    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v7

    .line 810
    .local v7, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v1

    .line 811
    .local v1, "code":I
    const/16 v10, 0xc8

    if-ne v1, v10, :cond_6

    .line 812
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 813
    .local v6, "is":Ljava/io/InputStream;
    new-instance v5, Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;

    invoke-direct {v5, v6}, Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;-><init>(Ljava/io/InputStream;)V

    .line 814
    .local v5, "gieParser":Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;
    invoke-virtual {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;->parse()Z

    .line 816
    # getter for: Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;->mEstimate:I
    invoke-static {v5}, Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;->access$000(Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 819
    if-eqz v7, :cond_0

    .line 820
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    .line 823
    .end local v5    # "gieParser":Lcom/android/exchange/adapter/EmailSyncAdapter$GetItemEstimateParser;
    .end local v6    # "is":Ljava/io/InputStream;
    :cond_0
    :goto_4
    return v10

    .line 773
    .end local v0    # "className":Ljava/lang/String;
    .end local v1    # "code":I
    .end local v2    # "ex03":Z
    .end local v3    # "ex07":Z
    .end local v4    # "ex10":Z
    .end local v7    # "resp":Lcom/android/exchange/EasResponse;
    .end local v9    # "syncKey":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 774
    .restart local v4    # "ex10":Z
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 775
    .restart local v2    # "ex03":Z
    :cond_3
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 788
    .restart local v0    # "className":Ljava/lang/String;
    .restart local v3    # "ex07":Z
    .restart local v9    # "syncKey":Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    .line 790
    const/16 v10, 0x189

    invoke-virtual {v8, v10, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 791
    const/16 v10, 0xb

    invoke-virtual {v8, v10, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 792
    const/16 v10, 0x18a

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 793
    const/16 v10, 0x18

    invoke-virtual {v8, v10, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_3

    .line 797
    :cond_5
    const/16 v10, 0xb

    invoke-virtual {v8, v10, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 798
    const/16 v10, 0x18a

    iget-object v11, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v11, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v8, v10, v11}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 799
    const/16 v10, 0x17

    invoke-virtual {v8, v10}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    const/16 v11, 0x18

    invoke-virtual {v10, v11, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_3

    .line 819
    .restart local v1    # "code":I
    .restart local v7    # "resp":Lcom/android/exchange/EasResponse;
    :cond_6
    if-eqz v7, :cond_7

    .line 820
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    .line 823
    :cond_7
    const/4 v10, -0x1

    goto :goto_4

    .line 819
    .end local v1    # "code":I
    :catchall_0
    move-exception v10

    if-eqz v7, :cond_8

    .line 820
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    :cond_8
    throw v10
.end method

.method private static getSenderList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "senderAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4598
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4599
    .local v3, "senderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 4601
    .local v2, "domain":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 4602
    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4605
    const/16 v5, 0x40

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 4606
    .local v0, "amp":I
    add-int/lit8 v5, v0, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 4607
    if-eqz v2, :cond_0

    .line 4608
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4612
    .end local v0    # "amp":I
    :cond_0
    const/4 v1, 0x0

    .line 4613
    .local v1, "arg":[Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 4614
    const-string v5, "\\."

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 4616
    :cond_1
    if-eqz v1, :cond_2

    array-length v5, v1

    const/4 v6, 0x2

    if-le v5, v6, :cond_2

    .line 4617
    invoke-static {v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getSubDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4618
    .local v4, "subDomain":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 4619
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4622
    .end local v4    # "subDomain":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method private static getSubDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "domain"    # Ljava/lang/String;

    .prologue
    .line 4633
    const/4 v2, 0x0

    .line 4634
    .local v2, "subDomain":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 4635
    .local v1, "pos":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 4636
    const/16 v3, 0x2e

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v1

    .line 4635
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4638
    :cond_0
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 4639
    return-object v2
.end method

.method public static getTimeInMillis(Ljava/lang/String;)J
    .locals 2
    .param p0, "tstr"    # Ljava/lang/String;

    .prologue
    .line 4389
    const-string v0, "GMT"

    invoke-static {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getTimeInMillis(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getTimeInMillis(Ljava/lang/String;Ljava/lang/String;)J
    .locals 8
    .param p0, "tstr"    # Ljava/lang/String;
    .param p1, "timezone"    # Ljava/lang/String;

    .prologue
    .line 4393
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 4394
    .local v0, "cal":Ljava/util/GregorianCalendar;
    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x8

    const/16 v4, 0xa

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xb

    const/16 v5, 0xd

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xe

    const/16 v6, 0x10

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0x11

    const/16 v7, 0x13

    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Ljava/util/GregorianCalendar;->set(IIIIII)V

    .line 4397
    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 4398
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method private insertBlacklistMessage(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4492
    .local p1, "subCommitIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    if-nez p1, :cond_0

    .line 4514
    :goto_0
    return-void

    .line 4495
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4497
    .local v4, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 4498
    .local v3, "msgId":Ljava/lang/Long;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4499
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v5, "messagekey"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4500
    const-string v5, "processdirty"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4502
    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$BlackListMessage;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 4508
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v3    # "msgId":Ljava/lang/Long;
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const-string v6, "com.android.email.provider"

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 4509
    :catch_0
    move-exception v1

    .line 4510
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 4511
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 4512
    .local v1, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v1}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_0
.end method

.method private isBlocked(Ljava/lang/String;)Z
    .locals 10
    .param p1, "mEmailAddress"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 4561
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4562
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 4563
    .local v8, "isPresent":Ljava/lang/Boolean;
    const-string v6, "emailAddress=? COLLATE NOCASE"

    .line 4566
    .local v6, "BLACKLIST_ID":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 4567
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 4588
    :goto_0
    return v1

    .line 4570
    :cond_0
    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v2

    .line 4571
    .local v4, "args":[Ljava/lang/String;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$BlackList;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$BlackList;->BLACKLIST_EMAIL_PROJECTION:[Ljava/lang/String;

    const-string v3, "emailAddress=? COLLATE NOCASE"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 4574
    .local v7, "getItemCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 4576
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4577
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4579
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 4583
    :cond_1
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eq v1, v9, :cond_2

    .line 4584
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 4588
    :cond_2
    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    .line 4583
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eq v2, v9, :cond_3

    .line 4584
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private isMessageBlockedByUser(Lcom/android/emailcommon/provider/EmailContent$Message;)Z
    .locals 5
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 4534
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/mail/Address;->unpackFirst(Ljava/lang/String;)Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    .line 4535
    .local v2, "senderEmail":Lcom/android/emailcommon/mail/Address;
    if-eqz v2, :cond_1

    .line 4536
    invoke-virtual {v2}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 4537
    .local v1, "senderAddress":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 4538
    const/4 v0, 0x0

    .line 4539
    .local v0, "count":I
    invoke-static {v1}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getSenderList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 4541
    .local v3, "senderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 4542
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->isBlocked(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 4543
    const/4 v4, 0x1

    .line 4550
    .end local v0    # "count":I
    .end local v1    # "senderAddress":Ljava/lang/String;
    .end local v3    # "senderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    return v4

    .line 4545
    .restart local v0    # "count":I
    .restart local v1    # "senderAddress":Ljava/lang/String;
    .restart local v3    # "senderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4550
    .end local v0    # "count":I
    .end local v1    # "senderAddress":Ljava/lang/String;
    .end local v3    # "senderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private log(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 3735
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3736
    const-string v0, "EmailSyncAdapter"

    invoke-static {v0, p1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3737
    return-void
.end method

.method public static makeDisplayName(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packedTo"    # Ljava/lang/String;
    .param p2, "packedCc"    # Ljava/lang/String;
    .param p3, "packedBcc"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 4416
    const/4 v2, 0x0

    .line 4417
    .local v2, "first":Lcom/android/emailcommon/mail/Address;
    const/4 v6, 0x0

    .line 4418
    .local v6, "nRecipients":I
    const/4 v8, 0x3

    new-array v1, v8, [Ljava/lang/String;

    aput-object p1, v1, v10

    aput-object p2, v1, v11

    aput-object p3, v1, v9

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v7, v1, v4

    .line 4421
    .local v7, "packed":Ljava/lang/String;
    invoke-static {v7}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v0

    .line 4422
    .local v0, "addresses":[Lcom/android/emailcommon/mail/Address;
    array-length v8, v0

    add-int/2addr v6, v8

    .line 4423
    if-nez v2, :cond_0

    array-length v8, v0

    if-lez v8, :cond_0

    .line 4424
    aget-object v2, v0, v10

    .line 4418
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 4427
    .end local v0    # "addresses":[Lcom/android/emailcommon/mail/Address;
    .end local v7    # "packed":Ljava/lang/String;
    :cond_1
    if-nez v6, :cond_3

    .line 4428
    const-string v3, ""

    .line 4439
    :cond_2
    :goto_1
    return-object v3

    .line 4431
    :cond_3
    const/4 v3, 0x0

    .line 4432
    .local v3, "friendly":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 4433
    invoke-virtual {v2}, Lcom/android/emailcommon/mail/Address;->toFriendly()Ljava/lang/String;

    move-result-object v3

    .line 4434
    if-eq v6, v11, :cond_2

    .line 4439
    :cond_4
    const v8, 0x7f060019

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v3, v9, v10

    add-int/lit8 v10, v6, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {p0, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method private makeFakeInbox()J
    .locals 6

    .prologue
    .line 318
    const-string v2, "EmailSyncAdapter"

    const-string v3, "makeFakeInbox() start"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    new-instance v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 320
    .local v1, "fakeInbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const-string v2, "tmpInbox"

    iput-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tmpInbox"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 322
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iput-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 323
    const/16 v2, 0x200

    iput v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 324
    const/4 v2, -0x1

    iput v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 325
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlagVisible:Z

    .line 326
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->save(Landroid/content/Context;)Landroid/net/Uri;

    .line 328
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/4 v3, 0x1

    invoke-interface {v2, v4, v5, v3}, Lcom/android/emailcommon/service/IEmailServiceCallback;->setBadSyncAccount(JZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    :goto_0
    const-string v2, "EmailSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fake Inbox created, Id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    return-wide v2

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "EmailSyncAdapter"

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private messageReferenced(Landroid/content/ContentResolver;J)Z
    .locals 12
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "id"    # J

    .prologue
    .line 3863
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 3869
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->BODY_ID_MESSAGE_KEY_PROJECTION:[Ljava/lang/String;

    const-string v3, "sourceMessageKey=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mBindArgument:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3873
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 3874
    .local v7, "isReferenced":Z
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3875
    const/4 v7, 0x1

    .line 3876
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 3877
    .local v10, "messageKey":J
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v9

    .line 3879
    .local v9, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v9, :cond_0

    .line 3880
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-wide v2, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v8

    .line 3884
    .local v8, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v8, :cond_0

    iget v0, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 3886
    const/4 v7, 0x0

    .line 3888
    const-string v0, "EmailSyncAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " messageReferenced, message ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is referenced and present in draft folder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3895
    .end local v8    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v9    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v10    # "messageKey":J
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3896
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return v7

    .line 3895
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3896
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private moveMessagesFromFakeToRealInbox()V
    .locals 8

    .prologue
    .line 361
    const-string v2, "EmailSyncAdapter"

    const-string v3, "moveMessagesFromFakeToRealInbox: start"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-wide v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v2, :cond_0

    .line 363
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 364
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "mailboxKey"

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 366
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mailboxKey="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 368
    .local v1, "messageCount":I
    const-string v2, "EmailSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "moveMessagesFromFakeToRealInbox: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " messages moved"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v1    # "messageCount":I
    :cond_0
    return-void
.end method

.method private requestSendBroadcastIntentBlacklistModule(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "szAction"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    .line 4518
    const-string v1, "EmailSyncAdapter"

    const-string v2, "requestSendBroadcastIntentBlacklistModule()"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4519
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4520
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "intentType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4521
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 4522
    const/4 v0, 0x0

    .line 4523
    return-void
.end method

.method private resetMessageDirtyCommit(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3786
    .local p1, "subCommitIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .local p2, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 3787
    .local v2, "msgId":Ljava/lang/Long;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 3788
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "dirtyCommit"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3789
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3793
    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v2    # "msgId":Ljava/lang/Long;
    :cond_0
    return-void
.end method

.method private sendFFStateItem(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;ZZLandroid/content/ContentResolver;)Landroid/os/Bundle;
    .locals 15
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "firstCommand"    # Z
    .param p4, "syncChangeFirst"    # Z
    .param p5, "cr"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4047
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 4048
    .local v12, "id":J
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_b

    .line 4049
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CONTENT_PROJECTION:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "messageKey=\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 4052
    .local v10, "currentFFCursor":Landroid/database/Cursor;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CONTENT_PROJECTION:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "messageKey=\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 4055
    .local v14, "updatedFFCursor":Landroid/database/Cursor;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->DELETED_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$FollowupFlag;->CONTENT_PROJECTION:[Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "messageKey=\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 4058
    .local v11, "deletedFFCursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 4063
    .local v9, "baseFFCursor":Landroid/database/Cursor;
    if-eqz v14, :cond_0

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    if-eqz v11, :cond_7

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_7

    .line 4065
    :cond_1
    if-eqz p3, :cond_2

    .line 4066
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4067
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4068
    const/16 p3, 0x0

    .line 4070
    :cond_2
    if-eqz p4, :cond_3

    .line 4072
    iget v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4073
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xd

    const/16 v4, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4077
    const/16 p4, 0x0

    .line 4079
    :cond_3
    const/16 v2, 0xba

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4080
    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 4081
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4086
    :cond_4
    if-eqz v10, :cond_c

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 4087
    move-object v9, v10

    .line 4091
    :cond_5
    :goto_0
    if-eqz v14, :cond_6

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v9, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4094
    iget-object v2, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4096
    const/4 v2, 0x6

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 4158
    :cond_6
    :goto_1
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4161
    :cond_7
    if-eqz v10, :cond_8

    .line 4162
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4164
    :cond_8
    if-eqz v11, :cond_9

    .line 4165
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 4167
    :cond_9
    if-eqz v14, :cond_a

    .line 4168
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 4170
    :cond_a
    if-eqz v9, :cond_b

    .line 4171
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 4175
    .end local v9    # "baseFFCursor":Landroid/database/Cursor;
    .end local v10    # "currentFFCursor":Landroid/database/Cursor;
    .end local v11    # "deletedFFCursor":Landroid/database/Cursor;
    .end local v14    # "updatedFFCursor":Landroid/database/Cursor;
    :cond_b
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 4176
    .local v8, "b":Landroid/os/Bundle;
    sget-object v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->FIRST_COMMAND:Ljava/lang/String;

    move/from16 v0, p3

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4177
    sget-object v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->SYNC_CHANGED_FIRST:Ljava/lang/String;

    move/from16 v0, p4

    invoke-virtual {v8, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4178
    return-object v8

    .line 4088
    .end local v8    # "b":Landroid/os/Bundle;
    .restart local v9    # "baseFFCursor":Landroid/database/Cursor;
    .restart local v10    # "currentFFCursor":Landroid/database/Cursor;
    .restart local v11    # "deletedFFCursor":Landroid/database/Cursor;
    .restart local v14    # "updatedFFCursor":Landroid/database/Cursor;
    :cond_c
    if-eqz v11, :cond_5

    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 4089
    move-object v9, v11

    goto :goto_0

    .line 4104
    :pswitch_1
    const/16 v2, 0xbb

    const-string v3, "1"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4106
    const/16 v2, 0x24b

    const/16 v3, 0x9

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4109
    const/16 v2, 0xbe

    const/16 v3, 0x8

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4161
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_d

    .line 4162
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4164
    :cond_d
    if-eqz v11, :cond_e

    .line 4165
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 4167
    :cond_e
    if-eqz v14, :cond_f

    .line 4168
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 4170
    :cond_f
    if-eqz v9, :cond_10

    .line 4171
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_10
    throw v2

    .line 4114
    :pswitch_2
    const/16 v2, 0xbb

    :try_start_2
    const-string v3, "2"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4116
    const/4 v2, 0x7

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 4118
    const/16 v2, 0xbd

    const/4 v3, 0x7

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4123
    :goto_2
    const/16 v2, 0x25e

    const/16 v3, 0xa

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4126
    const/16 v2, 0x25f

    const/16 v3, 0xc

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4129
    const/16 v2, 0x24c

    const/16 v3, 0xb

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4131
    const/16 v2, 0x24d

    const/16 v3, 0xd

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4134
    const/16 v3, 0x25b

    const/16 v2, 0xe

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_12

    const-string v2, "1"

    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4138
    const/16 v2, 0x25c

    const/16 v3, 0xf

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getDate(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_1

    .line 4122
    :cond_11
    const/16 v2, 0xbd

    const-string v3, "FollowUp"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_2

    .line 4134
    :cond_12
    const-string v2, "0"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 4096
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendSmSChanges(Lcom/android/exchange/adapter/Serializer;Landroid/content/ContentResolver;Z)Z
    .locals 20
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "firstCommand"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3975
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/emailcommon/CursorManager;->inst(Landroid/content/Context;)Lcom/android/emailcommon/CursorManager;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v5, "accountKey=? AND messageType=256 AND messageDirty=1"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/android/emailcommon/CursorManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 3980
    .local v13, "sms":Landroid/database/Cursor;
    if-eqz v13, :cond_3

    .line 3982
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3983
    if-eqz p3, :cond_0

    .line 3984
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 3985
    const/16 p3, 0x0

    .line 3986
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3990
    :cond_0
    const/16 v2, 0xa

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 3991
    .local v10, "clientId":Ljava/lang/String;
    const/16 v2, 0xe

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 3992
    .local v12, "from":Ljava/lang/String;
    const/16 v2, 0xf

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 3993
    .local v15, "to":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 3994
    .local v16, "timeStamp":J
    new-instance v9, Ljava/util/GregorianCalendar;

    const-string v2, "GMT"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 3995
    .local v9, "cal":Ljava/util/GregorianCalendar;
    move-wide/from16 v0, v16

    invoke-virtual {v9, v0, v1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 3996
    invoke-static {v9}, Lcom/android/exchange/utility/CalendarUtilities;->formatDateTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v11

    .line 3998
    .local v11, "dateRecvd":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v4, v3

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v8

    .line 4000
    .local v8, "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    const/4 v14, 0x0

    .line 4001
    .local v14, "smsBody":Ljava/lang/String;
    if-eqz v8, :cond_1

    .line 4002
    iget-object v14, v8, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 4005
    :cond_1
    if-eqz v14, :cond_4

    .line 4006
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4007
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x10

    const-string v4, "SMS"

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3, v10}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x96

    invoke-virtual {v2, v3, v15}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x98

    invoke-virtual {v2, v3, v12}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x8f

    invoke-virtual {v2, v3, v11}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x92

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x95

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x44a

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x446

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x44c

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x44b

    invoke-virtual {v2, v3, v14}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4016
    const/16 v2, 0xc7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    if-ne v2, v3, :cond_4

    .line 4018
    const-string v2, "EmailSyncAdapter"

    const-string v3, "MAX_LOCAL_CHANGES ----break the sendSmSChanges list"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4025
    .end local v8    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .end local v9    # "cal":Ljava/util/GregorianCalendar;
    .end local v10    # "clientId":Ljava/lang/String;
    .end local v11    # "dateRecvd":Ljava/lang/String;
    .end local v12    # "from":Ljava/lang/String;
    .end local v14    # "smsBody":Ljava/lang/String;
    .end local v15    # "to":Ljava/lang/String;
    .end local v16    # "timeStamp":J
    :cond_2
    :goto_0
    if-eqz v13, :cond_3

    .line 4026
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 4031
    :cond_3
    return p3

    .line 4022
    .restart local v8    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .restart local v9    # "cal":Ljava/util/GregorianCalendar;
    .restart local v10    # "clientId":Ljava/lang/String;
    .restart local v11    # "dateRecvd":Ljava/lang/String;
    .restart local v12    # "from":Ljava/lang/String;
    .restart local v14    # "smsBody":Ljava/lang/String;
    .restart local v15    # "to":Ljava/lang/String;
    .restart local v16    # "timeStamp":J
    :cond_4
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 4025
    .end local v8    # "body":Lcom/android/emailcommon/provider/EmailContent$Body;
    .end local v9    # "cal":Ljava/util/GregorianCalendar;
    .end local v10    # "clientId":Ljava/lang/String;
    .end local v11    # "dateRecvd":Ljava/lang/String;
    .end local v12    # "from":Ljava/lang/String;
    .end local v14    # "smsBody":Ljava/lang/String;
    .end local v15    # "to":Ljava/lang/String;
    .end local v16    # "timeStamp":J
    :catchall_0
    move-exception v2

    if-eqz v13, :cond_5

    .line 4026
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method private sendUpdateItems(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;ZLandroid/content/ContentResolver;)Z
    .locals 18
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "firstCommand"    # Z
    .param p4, "cr"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4183
    :cond_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 4185
    const/16 v16, 0x1

    .line 4187
    .local v16, "syncChangeFirst":Z
    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 4188
    .local v12, "id":J
    const/16 v2, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 4190
    .local v14, "mailboxKey":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4194
    const/4 v9, 0x0

    .line 4195
    .local v9, "bNotExsist":Z
    const/4 v10, 0x0

    .line 4196
    .local v10, "currentCursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 4197
    .local v11, "serverId":Ljava/lang/String;
    const/16 v17, 0x0

    .line 4199
    .local v17, "targetread":I
    :try_start_0
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->UPDATES_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p4

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 4204
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_9

    .line 4205
    :cond_1
    const/4 v9, 0x1

    .line 4212
    :goto_0
    if-eqz v10, :cond_2

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4213
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4216
    :cond_2
    if-eqz v9, :cond_4

    .line 4218
    :try_start_1
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/adapter/EmailSyncAdapter;->UPDATES_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p4

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 4223
    if-eqz v10, :cond_3

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_b

    .line 4224
    :cond_3
    const/4 v9, 0x1

    .line 4232
    :goto_1
    if-eqz v10, :cond_4

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_4

    .line 4233
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 4237
    :cond_4
    if-eqz v11, :cond_0

    .line 4241
    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move/from16 v0, v17

    if-eq v0, v2, :cond_e

    .line 4242
    if-eqz p3, :cond_5

    .line 4243
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4244
    const/16 v2, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4245
    const/16 p3, 0x0

    .line 4248
    :cond_5
    if-eqz v16, :cond_d

    .line 4251
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4252
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0xd

    const/16 v4, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x1d

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4256
    const/4 v6, 0x0

    .line 4259
    .end local v16    # "syncChangeFirst":Z
    .local v6, "syncChangeFirst":Z
    :goto_2
    const/16 v2, 0x95

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    :goto_3
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v7, p4

    .line 4261
    invoke-direct/range {v2 .. v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->sendFFStateItem(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;ZZLandroid/content/ContentResolver;)Landroid/os/Bundle;

    move-result-object v8

    .line 4262
    .local v8, "b":Landroid/os/Bundle;
    sget-object v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->SYNC_CHANGED_FIRST:Ljava/lang/String;

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 4263
    sget-object v2, Lcom/android/exchange/adapter/EmailSyncAdapter;->FIRST_COMMAND:Ljava/lang/String;

    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p3

    .line 4264
    if-nez v6, :cond_6

    .line 4265
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4266
    :cond_6
    const/16 v2, 0xc7

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    if-ne v2, v3, :cond_0

    .line 4268
    const-string v2, "EmailSyncAdapter"

    const-string v3, "MAX_LOCAL_CHANGES ----break the sendUpdateItems list"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4273
    .end local v6    # "syncChangeFirst":Z
    .end local v8    # "b":Landroid/os/Bundle;
    .end local v9    # "bNotExsist":Z
    .end local v10    # "currentCursor":Landroid/database/Cursor;
    .end local v11    # "serverId":Ljava/lang/String;
    .end local v12    # "id":J
    .end local v14    # "mailboxKey":J
    .end local v17    # "targetread":I
    :cond_7
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->isLast()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 4274
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4276
    :cond_8
    return p3

    .line 4208
    .restart local v9    # "bNotExsist":Z
    .restart local v10    # "currentCursor":Landroid/database/Cursor;
    .restart local v11    # "serverId":Ljava/lang/String;
    .restart local v12    # "id":J
    .restart local v14    # "mailboxKey":J
    .restart local v16    # "syncChangeFirst":Z
    .restart local v17    # "targetread":I
    :cond_9
    const/4 v2, 0x2

    :try_start_2
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 4209
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v17

    goto/16 :goto_0

    .line 4212
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_a

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_a

    .line 4213
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v2

    .line 4227
    :cond_b
    const/4 v2, 0x2

    :try_start_3
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 4228
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 4229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    new-instance v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;

    invoke-direct {v3, v12, v13, v14, v15}, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;-><init>(JJ)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/16 :goto_1

    .line 4232
    :catchall_1
    move-exception v2

    if-eqz v10, :cond_c

    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_c

    .line 4233
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    :cond_d
    move/from16 v6, v16

    .end local v16    # "syncChangeFirst":Z
    .restart local v6    # "syncChangeFirst":Z
    goto/16 :goto_2

    .end local v6    # "syncChangeFirst":Z
    .restart local v16    # "syncChangeFirst":Z
    :cond_e
    move/from16 v6, v16

    .end local v16    # "syncChangeFirst":Z
    .restart local v6    # "syncChangeFirst":Z
    goto/16 :goto_3
.end method


# virtual methods
.method public cleanup()V
    .locals 3

    .prologue
    .line 3843
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3845
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3846
    .local v0, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->addCleanupOps(Ljava/util/ArrayList;)V

    .line 3848
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "com.android.email.provider"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3855
    .end local v0    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_1
    :goto_0
    return-void

    .line 3851
    .restart local v0    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catch_0
    move-exception v1

    goto :goto_0

    .line 3849
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public forceStopLooping()V
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 387
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->completeSpecialInboxWipeHandling()V

    .line 389
    :cond_0
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3797
    const-string v0, "Email"

    return-object v0
.end method

.method public getEmailFilter()Ljava/lang/String;
    .locals 2

    .prologue
    .line 392
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncLookback:I

    .line 393
    .local v0, "syncLookback":I
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v1, :cond_1

    .line 394
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v0, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    .line 396
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 410
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    :goto_0
    return-object v1

    .line 398
    :pswitch_0
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_DAY:Ljava/lang/String;

    goto :goto_0

    .line 400
    :pswitch_1
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_3_DAYS:Ljava/lang/String;

    goto :goto_0

    .line 402
    :pswitch_2
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    goto :goto_0

    .line 404
    :pswitch_3
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    goto :goto_0

    .line 406
    :pswitch_4
    sget-object v1, Lcom/android/emailcommon/EasRefs;->FILTER_1_MONTH:Ljava/lang/String;

    goto :goto_0

    .line 408
    :pswitch_5
    const-string v1, "0"

    goto :goto_0

    .line 396
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;
    .locals 2
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4446
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->checkFakeInbox()V

    .line 4447
    new-instance v0, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;-><init>(Lcom/android/exchange/adapter/EmailSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/EmailSyncAdapter;Z)V

    return-object v0
.end method

.method public final getUniqueTempFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 3726
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "tempFile_EmailSyncAdapter"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3727
    .local v0, "ret":Ljava/lang/StringBuffer;
    const-string v1, "_tid_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3728
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 3729
    const-string v1, "_created_at_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3730
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 3731
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isLooping()Z
    .locals 1

    .prologue
    .line 833
    iget-boolean v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mIsLooping:Z

    return v0
.end method

.method public isSyncable()Z
    .locals 2

    .prologue
    .line 845
    iget-object v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccountManagerAccount:Landroid/accounts/Account;

    const-string v1, "com.android.email.provider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isWipeRequested()Z
    .locals 1

    .prologue
    .line 838
    iget-boolean v0, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mWipeRequested:Z

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 13
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v7, 0x0

    .line 568
    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    if-eqz v8, :cond_0

    .line 569
    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    const v9, 0x7f06001a

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/emailcommon/utility/BodyUtilites;->setCutText(Ljava/lang/String;)V

    .line 571
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->checkFakeInbox()V

    .line 574
    :cond_0
    const/4 v3, 0x0

    .line 576
    .local v3, "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    :try_start_0
    new-instance v4, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;

    invoke-direct {v4, p0, p1, p0}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;-><init>(Lcom/android/exchange/adapter/EmailSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/EmailSyncAdapter;)V
    :try_end_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    .end local v3    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    .local v4, "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    iput-boolean v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z

    .line 589
    const/4 v5, 0x0

    .line 591
    .local v5, "res":Z
    :try_start_1
    invoke-virtual {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->parse()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 602
    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_1

    iget-wide v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    if-nez v5, :cond_1

    .line 603
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->completeSpecialInboxWipeHandling()V

    .line 607
    :cond_1
    invoke-virtual {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;->isLooping()Z

    move-result v8

    iput-boolean v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mIsLooping:Z

    .line 610
    iget-boolean v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchNeeded:Z

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 611
    :cond_2
    const/4 v5, 0x1

    move-object v3, v4

    .line 635
    .end local v4    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    .end local v5    # "res":Z
    .restart local v3    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    :goto_0
    return v5

    .line 577
    :catch_0
    move-exception v1

    .line 579
    .local v1, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v8, v8, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide v10, 0x4028333333333333L    # 12.1

    cmpl-double v8, v8, v10

    if-nez v8, :cond_3

    .line 580
    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->setIntervalPing()V

    move v5, v7

    .line 581
    goto :goto_0

    .line 583
    :cond_3
    throw v1

    .line 592
    .end local v1    # "e":Lcom/android/exchange/adapter/Parser$EofException;
    .end local v3    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    .restart local v4    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    .restart local v5    # "res":Z
    :catch_1
    move-exception v2

    .line 594
    .local v2, "ioException":Ljava/io/IOException;
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v7}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 595
    :try_start_2
    iget-object v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->clearDirtyCommitMsgs(Ljava/lang/String;)V

    .line 596
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 597
    throw v2

    .line 596
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7

    .line 615
    .end local v2    # "ioException":Ljava/io/IOException;
    :cond_4
    iget-boolean v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isLookBackChanged:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_5

    .line 617
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 618
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v8, "syncLookback"

    iget-object v9, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 619
    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    iget-object v9, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 620
    .local v6, "uri":Landroid/net/Uri;
    iget-object v8, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v8, v6, v0, v12, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 622
    iput-boolean v7, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->isLookBackChanged:Z

    .end local v0    # "cv":Landroid/content/ContentValues;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_5
    move-object v3, v4

    .line 635
    .end local v4    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    .restart local v3    # "p":Lcom/android/exchange/adapter/EmailSyncAdapter$EasEmailSyncParser;
    goto :goto_0
.end method

.method sendDeletedItems(Lcom/android/exchange/adapter/Serializer;Ljava/util/ArrayList;Z)Z
    .locals 18
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "first"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/exchange/adapter/Serializer;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;Z)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3913
    .local p2, "deletedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 3916
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mailboxKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 3917
    .local v5, "where":Ljava/lang/String;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->LIST_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 3922
    .local v12, "c":Landroid/database/Cursor;
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->clear()V

    .line 3923
    if-eqz v12, :cond_7

    .line 3925
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3926
    const/4 v13, 0x0

    .line 3927
    .local v13, "cur1":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 3928
    .local v17, "thereIsUpdatedMessage":Z
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v14

    .line 3930
    .local v14, "messageId":J
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/emailcommon/CursorManager;->inst(Landroid/content/Context;)Lcom/android/emailcommon/CursorManager;

    move-result-object v6

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    const/4 v3, 0x1

    new-array v8, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v8, v3

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Lcom/android/emailcommon/CursorManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 3933
    if-eqz v13, :cond_1

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-lez v3, :cond_1

    .line 3934
    const/16 v17, 0x1

    .line 3936
    :cond_1
    if-eqz v13, :cond_2

    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_2

    .line 3937
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 3939
    :cond_2
    if-nez v17, :cond_0

    .line 3941
    const/16 v3, 0xb

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 3943
    .local v16, "serverId":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 3946
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6, v7}, Lcom/android/exchange/adapter/EmailSyncAdapter;->messageReferenced(Landroid/content/ContentResolver;J)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 3947
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "Postponing deletion of referenced message: "

    aput-object v6, v3, v4

    const/4 v4, 0x1

    aput-object v16, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/EmailSyncAdapter;->userLog([Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3965
    .end local v13    # "cur1":Landroid/database/Cursor;
    .end local v14    # "messageId":J
    .end local v16    # "serverId":Ljava/lang/String;
    .end local v17    # "thereIsUpdatedMessage":Z
    :catchall_0
    move-exception v3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v3

    .line 3936
    .restart local v13    # "cur1":Landroid/database/Cursor;
    .restart local v14    # "messageId":J
    .restart local v17    # "thereIsUpdatedMessage":Z
    :catchall_1
    move-exception v3

    if-eqz v13, :cond_3

    :try_start_3
    invoke-interface {v13}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_3

    .line 3937
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v3

    .line 3949
    .restart local v16    # "serverId":Ljava/lang/String;
    :cond_4
    if-eqz p3, :cond_5

    .line 3950
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 3951
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3952
    const/16 p3, 0x0

    .line 3955
    :cond_5
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0xd

    move-object/from16 v0, v16

    invoke-virtual {v3, v4, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 3956
    const/4 v3, 0x0

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3957
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 3958
    const/16 v3, 0xc7

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    if-ne v3, v4, :cond_0

    .line 3960
    const-string v3, "EmailSyncAdapter"

    const-string v4, "MAX_LOCAL_CHANGES ----break the deleted list"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3965
    .end local v13    # "cur1":Landroid/database/Cursor;
    .end local v14    # "messageId":J
    .end local v16    # "serverId":Ljava/lang/String;
    .end local v17    # "thereIsUpdatedMessage":Z
    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 3968
    :cond_7
    return p3
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 18
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4282
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 4283
    .local v2, "cr":Landroid/content/ContentResolver;
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mPrevSyncKey:Ljava/lang/String;

    .line 4284
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4285
    const/4 v3, 0x0

    .line 4374
    :goto_0
    return v3

    .line 4289
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 4290
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 4294
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->sendDeletedItems(Lcom/android/exchange/adapter/Serializer;Ljava/util/ArrayList;Z)Z

    move-result v12

    .line 4296
    .local v12, "firstCommand":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 4302
    if-eqz v12, :cond_3

    .line 4303
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    .line 4304
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4305
    const/4 v12, 0x0

    .line 4307
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;

    .line 4308
    .local v16, "req":Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0xd

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;->serverId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_1

    .line 4313
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v16    # "req":Lcom/android/exchange/adapter/EmailSyncAdapter$FetchRequest;
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v3, v3, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    const/16 v4, 0xc7

    if-ge v3, v4, :cond_5

    .line 4314
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2, v12}, Lcom/android/exchange/adapter/EmailSyncAdapter;->sendSmSChanges(Lcom/android/exchange/adapter/Serializer;Landroid/content/ContentResolver;Z)Z

    move-result v12

    .line 4321
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 4322
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->fDeletedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 4326
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdatedIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 4327
    const/4 v9, 0x0

    .line 4328
    .local v9, "c":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mSyncLocalChangesCount:I

    const/16 v4, 0xc7

    if-ge v3, v4, :cond_7

    .line 4330
    :try_start_0
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->LIST_PROJECTION:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mailboxKey="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 4332
    if-eqz v9, :cond_6

    .line 4333
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9, v12, v2}, Lcom/android/exchange/adapter/EmailSyncAdapter;->sendUpdateItems(Lcom/android/exchange/adapter/Serializer;Landroid/database/Cursor;ZLandroid/content/ContentResolver;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v12

    .line 4336
    :cond_6
    if-eqz v9, :cond_7

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_7

    .line 4337
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 4345
    :cond_7
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 4346
    .local v15, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 4347
    .local v10, "cv":Landroid/content/ContentValues;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v13, v3, :cond_9

    .line 4348
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->DELETED_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;

    iget-wide v6, v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;->mId:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v17

    .line 4350
    .local v17, "uri":Landroid/net/Uri;
    const-string v4, "mailboxKey"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;

    iget-wide v6, v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;->mMailboxKey:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4351
    invoke-static/range {v17 .. v17}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    .line 4352
    .local v8, "b":Landroid/content/ContentProviderOperation$Builder;
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4354
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;

    iget-wide v6, v3, Lcom/android/exchange/adapter/EmailSyncAdapter$UpdateDeleteItems;->mId:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v17

    .line 4355
    invoke-static/range {v17 .. v17}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v8

    .line 4356
    invoke-virtual {v8}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4357
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    .line 4347
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 4336
    .end local v8    # "b":Landroid/content/ContentProviderOperation$Builder;
    .end local v10    # "cv":Landroid/content/ContentValues;
    .end local v13    # "i":I
    .end local v15    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v17    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_8

    invoke-interface {v9}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_8

    .line 4337
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v3

    .line 4360
    .restart local v10    # "cv":Landroid/content/ContentValues;
    .restart local v13    # "i":I
    .restart local v15    # "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :cond_9
    :try_start_1
    const-string v3, "com.android.email.provider"

    invoke-virtual {v2, v3, v15}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 4366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 4367
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 4370
    :goto_3
    if-nez v12, :cond_a

    .line 4371
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4372
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 4361
    :catch_0
    move-exception v11

    .line 4362
    .local v11, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 4367
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    .line 4363
    .end local v11    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v11

    .line 4364
    .local v11, "e":Landroid/content/OperationApplicationException;
    :try_start_3
    invoke-virtual {v11}, Landroid/content/OperationApplicationException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4366
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 4367
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    .line 4366
    .end local v11    # "e":Landroid/content/OperationApplicationException;
    :catchall_1
    move-exception v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mUpdateDeleteList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 4367
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    throw v3

    .line 4374
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 8
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 431
    const/4 v0, 0x1

    .line 432
    .local v0, "deleteAsMoveFlag":Z
    if-eqz p3, :cond_1

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    const-string v3, "EmailSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current email filter is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getEmailFilter()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_12

    .line 445
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_9

    const/4 v2, 0x1

    .line 446
    .local v2, "isTrashMailbox":Z
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpg-double v3, v4, v6

    if-gez v3, :cond_a

    .line 447
    if-nez v2, :cond_2

    if-eqz v0, :cond_2

    .line 448
    const/16 v3, 0x1e

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 453
    :cond_2
    :goto_2
    const/16 v3, 0x13

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 455
    const-string v1, "50"

    .line 456
    .local v1, "emailWindowSize":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpg-double v3, v4, v6

    if-gez v3, :cond_d

    .line 457
    const-string v1, "5"

    .line 462
    :goto_3
    const/16 v3, 0x15

    invoke-virtual {p2, v3, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 465
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_3

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getConversationMode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 467
    const/16 v3, 0x27

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 470
    :cond_3
    const/16 v3, 0x17

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 473
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mConflictFlags:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    .line 474
    const/16 v3, 0x1b

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mConflictFlags:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 477
    :cond_4
    const/16 v3, 0x18

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getEmailFilter()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 479
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x402c333333333333L    # 14.1

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_5

    .line 480
    const/16 v3, 0x605

    const-string v4, "1"

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 482
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_10

    .line 483
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_f

    .line 484
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailMessageDiffEnabled()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_e

    .line 485
    const/16 v3, 0x459

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 492
    :goto_4
    const/16 v3, 0x446

    const-string v4, "2"

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 494
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v3

    invoke-static {v3}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v3, v4, :cond_6

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v3

    invoke-static {v3}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v3, v4, :cond_6

    .line 497
    const/16 v3, 0x447

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v4

    invoke-static {v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas12Text()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 501
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_7

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->getConversationMode()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v3

    invoke-static {v3}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v3, v4, :cond_7

    .line 504
    const/16 v3, 0x458

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getTextPreviewSize()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 508
    :cond_7
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 511
    const/16 v3, 0x445

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x446

    const-string v5, "4"

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 512
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 527
    :goto_5
    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 530
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 532
    :cond_8
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v3, v3, 0x800

    if-eqz v3, :cond_0

    .line 533
    const/16 v3, 0x17

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x10

    const-string v5, "SMS"

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x18

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getEmailFilter()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x445

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x446

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_0

    .line 445
    .end local v1    # "emailWindowSize":Ljava/lang/String;
    .end local v2    # "isTrashMailbox":Z
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 451
    .restart local v2    # "isTrashMailbox":Z
    :cond_a
    const/16 v4, 0x1e

    if-nez v2, :cond_b

    if-nez v0, :cond_c

    :cond_b
    const-string v3, "0"

    :goto_6
    invoke-virtual {p2, v4, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_2

    :cond_c
    const-string v3, "1"

    goto :goto_6

    .line 459
    .restart local v1    # "emailWindowSize":Ljava/lang/String;
    :cond_d
    const-string v1, "50"

    goto/16 :goto_3

    .line 487
    :cond_e
    const/16 v3, 0x445

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_4

    .line 490
    :cond_f
    const/16 v3, 0x445

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_4

    .line 517
    :cond_10
    const/16 v3, 0x22

    const-string v4, "2"

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 518
    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v3

    invoke-static {v3}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v3, v4, :cond_11

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v3

    invoke-static {v3}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v3

    sget-object v4, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    if-eq v3, v4, :cond_11

    .line 520
    const/16 v3, 0x23

    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->getRealEmailSize(Landroid/content/Context;)B

    move-result v4

    invoke-static {v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->toEas2_5Text()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_5

    .line 523
    :cond_11
    const/16 v3, 0x23

    const-string v4, "8"

    invoke-virtual {p2, v3, v4}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_5

    .line 542
    .end local v1    # "emailWindowSize":Ljava/lang/String;
    .end local v2    # "isTrashMailbox":Z
    :cond_12
    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v3, v3, 0x800

    if-eqz v3, :cond_0

    .line 544
    const/16 v3, 0x17

    invoke-virtual {p2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x10

    const-string v5, "SMS"

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x18

    invoke-virtual {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->getEmailFilter()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x445

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x446

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_0
.end method

.method public wipe()V
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v8, 0x0

    .line 259
    const-string v4, "EmailSyncAdapter"

    const-string v5, "EMAIL BAD SYNC KEY"

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mWipeRequested:Z

    .line 262
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 263
    .local v0, "cvx":Landroid/content/ContentValues;
    const-string v4, "syncKey"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlags:I

    and-int/lit8 v1, v4, -0x41

    .line 265
    .local v1, "flag":I
    const-string v4, "flags"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 266
    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 267
    .local v2, "mMailboxUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v2, v0, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 268
    .local v3, "mailboxCount":I
    const-string v4, "EmailSyncAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Updating Email mailbox with sync key 0. Count = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_1

    .line 271
    iget-wide v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    cmp-long v4, v4, v10

    if-nez v4, :cond_0

    .line 272
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->makeFakeInbox()J

    .line 284
    :goto_0
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->clearRequests()V

    .line 285
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFetchRequestList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 286
    return-void

    .line 276
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteFakeInbox()V

    .line 277
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v6, v7, v8, v9}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteAllMailboxMessages(Landroid/content/Context;JJ)V

    .line 278
    iput-wide v10, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mFakeInboxId:J

    goto :goto_0

    .line 281
    :cond_1
    iget-object v4, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    iget-object v5, p0, Lcom/android/exchange/adapter/EmailSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v6, v7, v8, v9}, Lcom/android/exchange/adapter/EmailSyncAdapter;->deleteAllMailboxMessages(Landroid/content/Context;JJ)V

    goto :goto_0
.end method
