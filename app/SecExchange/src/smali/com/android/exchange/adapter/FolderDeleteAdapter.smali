.class public Lcom/android/exchange/adapter/FolderDeleteAdapter;
.super Lcom/android/exchange/adapter/AbstractCommandAdapter;
.source "FolderDeleteAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/FolderDeleteAdapter$FolderDeleteParser;
    }
.end annotation


# instance fields
.field public final CODE_FOLDER_CREATE_ACCESS_DENIED:I

.field public final CODE_FOLDER_DELETE_FOLDER_NOT_EXISTS:I

.field public final CODE_FOLDER_DELETE_INVALID_SYNC_KEY:I

.field public final CODE_FOLDER_DELETE_MALFORMED_REQUEST:I

.field public final CODE_FOLDER_DELETE_SERVER_ERROR:I

.field public final CODE_FOLDER_DELETE_SUCCESS:I

.field public final CODE_FOLDER_DELETE_SYSTEM_FOLDER:I

.field public final CODE_FOLDER_DELETE_TIME_OUT:I

.field public final CODE_FOLDER_DELETE_UNKNOWN_ERROR:I

.field private isStatus:I

.field private mDoNotRetry:Z

.field mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

.field private mServerId:Ljava/lang/String;

.field private thisMailboxChanged:I

.field private thisMailboxId:J


# direct methods
.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 7
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v4, -0x1

    .line 70
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 34
    const/4 v1, 0x1

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_SUCCESS:I

    .line 36
    const/4 v1, 0x3

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_SYSTEM_FOLDER:I

    .line 38
    const/4 v1, 0x4

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_FOLDER_NOT_EXISTS:I

    .line 40
    const/4 v1, 0x6

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_SERVER_ERROR:I

    .line 45
    const/4 v1, 0x7

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_CREATE_ACCESS_DENIED:I

    .line 48
    const/16 v1, 0x8

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_TIME_OUT:I

    .line 50
    const/16 v1, 0x9

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_INVALID_SYNC_KEY:I

    .line 52
    const/16 v1, 0xa

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_MALFORMED_REQUEST:I

    .line 54
    const/16 v1, 0xb

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->CODE_FOLDER_DELETE_UNKNOWN_ERROR:I

    .line 56
    iput-object v6, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mResponse:Lcom/android/exchange/adapter/AbstractCommandAdapter$FolderCommandResponse;

    .line 58
    iput-object v6, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mServerId:Ljava/lang/String;

    .line 61
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxId:J

    .line 63
    iput v5, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxChanged:I

    .line 65
    iput-boolean v5, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mDoNotRetry:Z

    .line 67
    iput v4, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->isStatus:I

    .line 71
    const-string v1, "Mahskyript"

    const-string v2, "FolderDeleteAdapter.FolderDeleteAdapter"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 74
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "syncInterval"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 75
    const-string v1, "SyncIntervalReference"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 76
    const-string v1, "offpeakSyncSchedule"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    const-string v1, "peakSyncSchedule"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 78
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/adapter/FolderDeleteAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderDeleteAdapter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mServerId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/exchange/adapter/FolderDeleteAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderDeleteAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mServerId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/android/exchange/adapter/FolderDeleteAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderDeleteAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->isStatus:I

    return p1
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/FolderDeleteAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/FolderDeleteAdapter;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/android/exchange/adapter/FolderDeleteAdapter;->deleteLocal()V

    return-void
.end method

.method private deleteLocal()V
    .locals 6

    .prologue
    .line 142
    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 144
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    :goto_0
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public callback(I)V
    .locals 4
    .param p1, "status"    # I

    .prologue
    .line 156
    const-string v0, "Mahskyript"

    const-string v1, "FolderDeleteAdapter.callback "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-interface {v0, v1, v2, v3, p1}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mDoNotRetry:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-void

    .line 161
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 166
    const-string v0, "Mahskyript"

    const-string v1, "FolderDeleteAdapter.getCommandName: FolderDelete"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "FolderDelete"

    return-object v0
.end method

.method public getSatus()I
    .locals 1

    .prologue
    .line 367
    iget v0, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->isStatus:I

    return v0
.end method

.method public hasChangedItems()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 150
    iget-boolean v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mDoNotRetry:Z

    if-ne v2, v0, :cond_0

    .line 152
    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    if-eqz v2, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public isSyncable()Z
    .locals 2

    .prologue
    .line 84
    const-string v0, "Mahskyript"

    const-string v1, "FolderDeleteAdapter.isSyncable"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Lcom/android/exchange/adapter/FolderDeleteAdapter$FolderDeleteParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/adapter/FolderDeleteAdapter$FolderDeleteParser;-><init>(Lcom/android/exchange/adapter/FolderDeleteAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 106
    .local v0, "parser":Lcom/android/exchange/adapter/FolderDeleteAdapter$FolderDeleteParser;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/FolderDeleteAdapter$FolderDeleteParser;->parse()Z

    move-result v1

    return v1
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 3
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    const/16 v0, 0x1d4

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x1d2

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x1c8

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 94
    const-string v0, "Mahskyript"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FolderDeleteAdapter.sendLocalChanges. Deleting item serverId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "Mahskyript"

    const-string v1, "FolderDeleteAdapter.sendLocalChanges Nothing to delete"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 364
    return-void
.end method

.method public wipe()V
    .locals 6

    .prologue
    .line 126
    iget-wide v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v1, :cond_0

    .line 127
    const-string v1, "FolderDeleteAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wipe(). Marking this mailbox:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as not deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxChanged:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxChanged:I

    .line 130
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 131
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "flagChanged"

    iget v2, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxChanged:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 132
    iget-object v1, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/exchange/adapter/FolderDeleteAdapter;->thisMailboxId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 135
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method
