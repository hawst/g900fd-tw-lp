.class public abstract Lcom/android/exchange/adapter/Parser;
.super Ljava/util/Observable;
.source "Parser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/Parser$EasParserException;,
        Lcom/android/exchange/adapter/Parser$EodException;,
        Lcom/android/exchange/adapter/Parser$EofException;
    }
.end annotation


# static fields
.field private static tagTables:[[Ljava/lang/String;


# instance fields
.field private capture:Z

.field private captureArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field captureFile:Ljava/io/BufferedOutputStream;

.field private capturebytes:Z

.field private depth:I

.field public endTag:I

.field private in:Ljava/io/InputStream;

.field private logTag:Ljava/lang/String;

.field private logging:Z

.field private mForceStopParsing:Z

.field public name:Ljava/lang/String;

.field private nameArray:[Ljava/lang/String;

.field private nextId:I

.field private noContent:Z

.field public num:I

.field public opaqueData:[B

.field public page:I

.field public startTag:I

.field private startTagArray:[I

.field public tag:I

.field private tagTable:[Ljava/lang/String;

.field public text:Ljava/lang/String;

.field tid:J

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/16 v0, 0x96

    new-array v0, v0, [[Ljava/lang/String;

    sput-object v0, Lcom/android/exchange/adapter/Parser;->tagTables:[[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/adapter/Parser;Z)V
    .locals 1
    .param p1, "parse"    # Lcom/android/exchange/adapter/Parser;
    .param p2, "resumeStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Parser;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;Z)V

    .line 224
    iget v0, p1, Lcom/android/exchange/adapter/Parser;->num:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->num:I

    .line 225
    iget v0, p1, Lcom/android/exchange/adapter/Parser;->page:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->page:I

    .line 226
    iget v0, p1, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    .line 227
    iget v0, p1, Lcom/android/exchange/adapter/Parser;->tag:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    .line 228
    iget-object v0, p1, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    .line 229
    iget v0, p1, Lcom/android/exchange/adapter/Parser;->endTag:I

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 230
    iget-object v0, p1, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    .line 232
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    .line 233
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/exchange/adapter/Parser;-><init>(Ljava/io/InputStream;Z)V

    .line 204
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    .line 206
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "resumeStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v5, -0x80000000

    const/4 v4, 0x0

    .line 208
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 72
    iput-boolean v4, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    .line 74
    iput-boolean v4, p0, Lcom/android/exchange/adapter/Parser;->capture:Z

    .line 77
    iput-boolean v4, p0, Lcom/android/exchange/adapter/Parser;->mForceStopParsing:Z

    .line 79
    const-string v3, "EAS Parser"

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->logTag:Ljava/lang/String;

    .line 82
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    .line 84
    iput-boolean v4, p0, Lcom/android/exchange/adapter/Parser;->capturebytes:Z

    .line 107
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    .line 116
    const/16 v3, 0x20

    new-array v3, v3, [Ljava/lang/String;

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->nameArray:[Ljava/lang/String;

    .line 120
    const/16 v3, 0x80

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    .line 126
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 188
    sget-object v2, Lcom/android/exchange/adapter/Tags;->pages:[[Ljava/lang/String;

    .line 189
    .local v2, "pages":[[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 190
    aget-object v1, v2, v0

    .line 191
    .local v1, "page":[Ljava/lang/String;
    array-length v3, v1

    if-lez v3, :cond_0

    .line 192
    sget-object v3, Lcom/android/exchange/adapter/Parser;->tagTables:[[Ljava/lang/String;

    aput-object v1, v3, v0

    .line 189
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    .end local v1    # "page":[Ljava/lang/String;
    :cond_1
    if-nez p2, :cond_2

    .line 211
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/Parser;->setInput(Ljava/io/InputStream;)V

    .line 214
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/exchange/adapter/Parser;->tid:J

    .line 216
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    iput-boolean v3, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    .line 217
    return-void

    .line 213
    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/Parser;->resetInput(Ljava/io/InputStream;)V

    goto :goto_1
.end method

.method private final getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I
    .locals 10
    .param p1, "asInt"    # Z
    .param p2, "os"    # Ljava/io/OutputStream;
    .param p3, "observer"    # Ljava/util/Observer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 640
    iget v5, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 641
    .local v5, "savedEndTag":I
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_1

    .line 642
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 646
    :goto_0
    iget-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->noContent:Z

    if-eqz v7, :cond_3

    .line 647
    const/4 v7, 0x3

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 648
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->noContent:Z

    .line 649
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 651
    iget-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v7, :cond_0

    .line 653
    :try_start_0
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    array-length v7, v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    if-le v7, v8, :cond_2

    .line 654
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    aget-object v6, v7, v8

    .line 655
    .local v6, "tempname":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " />"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 663
    .end local v6    # "tempname":Ljava/lang/String;
    :cond_0
    :goto_1
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 769
    :goto_2
    return v7

    .line 644
    :cond_1
    const/high16 v7, -0x80000000

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_0

    .line 657
    :cond_2
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "noContent : new Tag - check Tags.java!!! Added tag = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 658
    :catch_0
    move-exception v2

    .line 659
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "exception occurred while getting next TAG"

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 665
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 666
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 667
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->nextId()I

    move-result v3

    .line 668
    .local v3, "id":I
    :goto_3
    if-nez v3, :cond_4

    .line 669
    const/high16 v7, -0x80000000

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    .line 671
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readByte()I

    move-result v4

    .line 673
    .local v4, "pg":I
    shl-int/lit8 v7, v4, 0x6

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->page:I

    .line 675
    sget-object v7, Lcom/android/exchange/adapter/Parser;->tagTables:[[Ljava/lang/String;

    aget-object v7, v7, v4

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    .line 676
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->nextId()I

    move-result v3

    .line 677
    goto :goto_3

    .line 678
    .end local v4    # "pg":I
    :cond_4
    const/high16 v7, -0x80000000

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    .line 679
    sparse-switch v3, :sswitch_data_0

    .line 747
    const/4 v7, 0x2

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 749
    and-int/lit8 v7, v3, 0x3f

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    .line 751
    and-int/lit8 v7, v3, 0x40

    if-nez v7, :cond_e

    const/4 v7, 0x1

    :goto_4
    iput-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->noContent:Z

    .line 752
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 753
    iget-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v7, :cond_5

    .line 755
    :try_start_2
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    array-length v7, v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    if-le v7, v8, :cond_f

    .line 756
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    aget-object v7, v7, v8

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 758
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->nameArray:[Ljava/lang/String;

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    iget-object v9, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    aput-object v9, v7, v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 766
    :cond_5
    :goto_5
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    iget v9, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    aput v9, v7, v8

    .line 769
    :cond_6
    :goto_6
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    goto/16 :goto_2

    .line 682
    :sswitch_0
    const/4 v7, 0x1

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    goto :goto_6

    .line 686
    :sswitch_1
    const/4 v7, 0x3

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 687
    iget-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v7, :cond_7

    .line 688
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->nameArray:[Ljava/lang/String;

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget-object v7, v7, v8

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 692
    :cond_7
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget v7, v7, v8

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    goto :goto_6

    .line 696
    :sswitch_2
    if-nez p2, :cond_a

    .line 698
    const/4 v7, 0x4

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 699
    if-eqz p1, :cond_9

    .line 700
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInlineInt()I

    move-result v7

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->num:I

    .line 710
    :goto_7
    iget-boolean v7, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v7, :cond_6

    .line 712
    :try_start_3
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    array-length v7, v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    if-le v7, v8, :cond_c

    .line 713
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v8, v8, -0x5

    aget-object v7, v7, v8

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 714
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-nez p2, :cond_8

    if-eqz p1, :cond_b

    :cond_8
    iget v7, p0, Lcom/android/exchange/adapter/Parser;->num:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    :goto_8
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    .line 717
    :catch_1
    move-exception v2

    .line 718
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "exception occurred while getting next TAG"

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_6

    .line 702
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInlineString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    goto :goto_7

    .line 705
    :cond_a
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 706
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/Parser;->readInlineStream(Ljava/io/OutputStream;Ljava/util/Observer;)I

    move-result v7

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->num:I

    goto :goto_7

    .line 714
    :cond_b
    :try_start_4
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    goto :goto_8

    .line 716
    :cond_c
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "STR_I : new Tag - check Tags.java!!! Added tag = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_6

    .line 726
    :sswitch_3
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    move-result v1

    .line 727
    .local v1, "count":I
    new-array v0, v1, [B

    .line 729
    .local v0, "buf":[B
    :goto_9
    if-lez v1, :cond_d

    .line 730
    iget-object v7, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    array-length v8, v0

    sub-int/2addr v8, v1

    invoke-virtual {v7, v0, v8, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    sub-int/2addr v1, v7

    goto :goto_9

    .line 733
    :cond_d
    const/4 v7, 0x4

    iput v7, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 736
    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->opaqueData:[B

    goto/16 :goto_6

    .line 751
    .end local v0    # "buf":[B
    .end local v1    # "count":I
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 760
    :cond_f
    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "default : new Tag - check Tags.java!!! Added tag = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_5

    .line 761
    :catch_2
    move-exception v2

    .line 762
    .restart local v2    # "e":Ljava/lang/Exception;
    const-string v7, "exception occurred while getting next TAG"

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 679
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0xc3 -> :sswitch_3
    .end sparse-switch
.end method

.method private final getNextStream()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/high16 v4, -0x80000000

    .line 775
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v3, v5, :cond_0

    .line 776
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 781
    :goto_0
    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 782
    iput-object v7, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 784
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->nextId()I

    move-result v2

    .line 785
    .local v2, "id":I
    iput v4, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    .line 787
    sparse-switch v2, :sswitch_data_0

    .line 828
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream : default -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 829
    const/4 v3, 0x5

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 834
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream -> return type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->type:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 835
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    return v3

    .line 778
    .end local v2    # "id":I
    :cond_0
    iput v4, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_0

    .line 789
    .restart local v2    # "id":I
    :sswitch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream : EOF_BYTE -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 791
    const/4 v3, 0x1

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    goto :goto_1

    .line 795
    :sswitch_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream : Wbxml.END -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 797
    iput v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 798
    iget-boolean v3, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v3, :cond_1

    .line 799
    iget-object v3, p0, Lcom/android/exchange/adapter/Parser;->nameArray:[Ljava/lang/String;

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->name:Ljava/lang/String;

    .line 802
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/adapter/Parser;->startTagArray:[I

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    aget v3, v3, v4

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    iput v3, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    goto :goto_1

    .line 806
    :sswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream : Wbxml.STR_I -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 808
    iput v6, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 809
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInlineBytes()V

    goto/16 :goto_1

    .line 815
    :sswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNextStream : Wbxml.OPAQUE -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    .line 816
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    move-result v1

    .line 817
    .local v1, "count":I
    new-array v0, v1, [B

    .line 819
    .local v0, "buf":[B
    :goto_2
    if-lez v1, :cond_2

    .line 820
    iget-object v3, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    array-length v4, v0

    sub-int/2addr v4, v1

    invoke-virtual {v3, v0, v4, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    sub-int/2addr v1, v3

    goto :goto_2

    .line 823
    :cond_2
    iput v6, p0, Lcom/android/exchange/adapter/Parser;->type:I

    .line 824
    new-instance v3, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v3, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    goto/16 :goto_1

    .line 787
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0xc3 -> :sswitch_3
    .end sparse-switch
.end method

.method private nextId()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 889
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 890
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v0

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    .line 892
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->nextId:I

    return v0
.end method

.method private read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 877
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    if-eqz v1, :cond_1

    .line 879
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 880
    .local v0, "i":I
    iget-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->capture:Z

    if-eqz v1, :cond_0

    .line 881
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureArray:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 885
    .end local v0    # "i":I
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private readByte()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 896
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v0

    .line 897
    .local v0, "i":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 898
    new-instance v1, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v1

    .line 900
    :cond_0
    return v0
.end method

.method private readInlineBytes()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 857
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readStreamIntoFile()I

    move-result v0

    .line 858
    .local v0, "i":I
    if-nez v0, :cond_1

    .line 864
    return-void

    .line 860
    :cond_1
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 861
    new-instance v1, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v1
.end method

.method private readInlineInt()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 912
    const/4 v3, 0x0

    .line 914
    .local v3, "result":I
    const/4 v2, 0x1

    .line 915
    .local v2, "multiplier":I
    const/4 v0, 0x1

    .line 918
    .local v0, "beginning":Z
    :goto_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readByte()I

    move-result v1

    .line 921
    .local v1, "i":I
    if-nez v1, :cond_0

    .line 922
    mul-int v4, v2, v3

    return v4

    .line 924
    :cond_0
    if-eqz v0, :cond_1

    const/16 v4, 0x2d

    if-ne v1, v4, :cond_1

    .line 925
    const/4 v2, -0x1

    .line 926
    const/4 v0, 0x0

    .line 927
    goto :goto_0

    .line 929
    :cond_1
    const/4 v0, 0x0

    .line 932
    const/16 v4, 0x30

    if-lt v1, v4, :cond_2

    const/16 v4, 0x39

    if-gt v1, v4, :cond_2

    .line 933
    mul-int/lit8 v4, v3, 0xa

    add-int/lit8 v5, v1, -0x30

    add-int v3, v4, v5

    goto :goto_0

    .line 935
    :cond_2
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Non integer"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private readInlineStream(Ljava/io/OutputStream;Ljava/util/Observer;)I
    .locals 16
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "observer"    # Ljava/util/Observer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 976
    const-wide/16 v10, 0x0

    .line 977
    .local v10, "totalRead":J
    const-wide/16 v12, 0x0

    .line 979
    .local v12, "totalWrite":J
    const/16 v2, 0x100

    .line 981
    .local v2, "BUF_SIZE":I
    if-eqz p1, :cond_b

    .line 983
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v9, 0x100

    invoke-direct {v5, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 985
    .local v5, "bufStream":Ljava/io/ByteArrayOutputStream;
    if-eqz p2, :cond_0

    .line 986
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Parser;->addObserver(Ljava/util/Observer;)V

    .line 992
    :cond_0
    const/4 v8, 0x0

    .line 993
    .local v8, "currentByte":I
    const/4 v4, 0x0

    .line 994
    .local v4, "bufRead":I
    const/4 v6, 0x0

    .line 995
    .local v6, "bufWrite":I
    const/4 v7, 0x0

    .line 999
    .local v7, "chunkRead":I
    :cond_1
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/android/exchange/adapter/Parser;->mForceStopParsing:Z

    if-eqz v9, :cond_4

    .line 1000
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iput-boolean v9, v0, Lcom/android/exchange/adapter/Parser;->mForceStopParsing:Z

    .line 1002
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1003
    new-instance v9, Ljava/lang/String;

    const-string v14, "ATTACHMENT_DOWNLOAD_CANCEL"

    invoke-direct {v9, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1005
    if-eqz p2, :cond_2

    .line 1006
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    :cond_2
    const/4 v9, -0x1

    .line 1048
    int-to-long v14, v4

    add-long/2addr v10, v14

    .line 1049
    int-to-long v14, v6

    add-long/2addr v12, v14

    .line 1051
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1052
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1054
    if-eqz p2, :cond_3

    .line 1055
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    .line 1064
    .end local v4    # "bufRead":I
    .end local v5    # "bufStream":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "bufWrite":I
    .end local v7    # "chunkRead":I
    .end local v8    # "currentByte":I
    :cond_3
    :goto_1
    return v9

    .line 1011
    .restart local v4    # "bufRead":I
    .restart local v5    # "bufStream":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "bufWrite":I
    .restart local v7    # "chunkRead":I
    .restart local v8    # "currentByte":I
    :cond_4
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v8

    .line 1012
    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    if-nez v8, :cond_8

    .line 1014
    :cond_5
    if-lez v4, :cond_6

    .line 1015
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    const/4 v14, 0x0

    invoke-static {v9, v14}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 1016
    .local v3, "b":[B
    array-length v6, v3

    .line 1017
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1048
    .end local v3    # "b":[B
    :cond_6
    int-to-long v14, v4

    add-long/2addr v10, v14

    .line 1049
    int-to-long v14, v6

    add-long/2addr v12, v14

    .line 1051
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1052
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1054
    if-eqz p2, :cond_7

    .line 1055
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    .line 1064
    .end local v4    # "bufRead":I
    .end local v5    # "bufStream":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "bufWrite":I
    .end local v7    # "chunkRead":I
    .end local v8    # "currentByte":I
    :cond_7
    :goto_2
    long-to-int v9, v10

    goto :goto_1

    .line 1023
    .restart local v4    # "bufRead":I
    .restart local v5    # "bufStream":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "bufWrite":I
    .restart local v7    # "chunkRead":I
    .restart local v8    # "currentByte":I
    :cond_8
    :try_start_2
    invoke-virtual {v5, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 1024
    add-int/lit8 v4, v4, 0x1

    .line 1026
    const/16 v9, 0x100

    if-ne v4, v9, :cond_1

    .line 1028
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    const/4 v14, 0x0

    invoke-static {v9, v14}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 1029
    .restart local v3    # "b":[B
    array-length v6, v3

    .line 1031
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write([B)V

    .line 1032
    add-int/2addr v7, v4

    .line 1033
    int-to-long v14, v4

    add-long/2addr v10, v14

    .line 1034
    int-to-long v14, v6

    add-long/2addr v12, v14

    .line 1035
    const/4 v4, 0x0

    .line 1037
    const/16 v9, 0x4000

    if-lt v7, v9, :cond_9

    .line 1038
    add-int/lit16 v7, v7, -0x4000

    .line 1040
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1041
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1044
    :cond_9
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1048
    .end local v3    # "b":[B
    :catchall_0
    move-exception v9

    int-to-long v14, v4

    add-long/2addr v10, v14

    .line 1049
    int-to-long v14, v6

    add-long/2addr v12, v14

    .line 1051
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/Parser;->setChanged()V

    .line 1052
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/android/exchange/adapter/Parser;->notifyObservers(Ljava/lang/Object;)V

    .line 1054
    if-eqz p2, :cond_a

    .line 1055
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Parser;->deleteObserver(Ljava/util/Observer;)V

    :cond_a
    throw v9

    .line 1060
    .end local v4    # "bufRead":I
    .end local v5    # "bufStream":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "bufWrite":I
    .end local v7    # "chunkRead":I
    .end local v8    # "currentByte":I
    :cond_b
    const-wide/16 v10, 0x0

    goto :goto_2
.end method

.method private readInlineString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 957
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x100

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 961
    .local v1, "outputStream":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->read()I

    move-result v0

    .line 962
    .local v0, "i":I
    if-nez v0, :cond_0

    .line 969
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 970
    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 971
    .local v2, "res":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 972
    return-object v2

    .line 964
    .end local v2    # "res":Ljava/lang/String;
    :cond_0
    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    .line 965
    new-instance v3, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v3, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v3

    .line 967
    :cond_1
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method

.method private readInt()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 941
    const/4 v1, 0x0

    .line 944
    .local v1, "result":I
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readByte()I

    move-result v0

    .line 945
    .local v0, "i":I
    shl-int/lit8 v2, v1, 0x7

    and-int/lit8 v3, v0, 0x7f

    or-int v1, v2, v3

    .line 946
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_0

    .line 947
    return v1
.end method

.method private readStreamIntoFile()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 844
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 845
    .local v0, "i":I
    iget-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->capturebytes:Z

    if-eqz v1, :cond_0

    .line 846
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/BufferedOutputStream;->write(I)V

    .line 848
    :cond_0
    return v0
.end method


# virtual methods
.method public captureBytesOff()V
    .locals 3

    .prologue
    .line 305
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V

    .line 306
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->capturebytes:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    :try_start_1
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 316
    :goto_0
    return-void

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "ioe":Ljava/io/IOException;
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->capturebytes:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    :try_start_3
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 312
    :catch_1
    move-exception v1

    goto :goto_0

    .line 310
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    .line 311
    :try_start_4
    iget-object v2, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 314
    :goto_1
    throw v1

    .line 312
    :catch_2
    move-exception v2

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public captureBytesOn(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "file"    # Ljava/lang/String;

    .prologue
    .line 291
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/exchange/adapter/Parser;->capturebytes:Z

    .line 294
    :try_start_0
    new-instance v1, Ljava/io/BufferedOutputStream;

    const/4 v2, 0x2

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, p0, Lcom/android/exchange/adapter/Parser;->captureFile:Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public crypt([B)Ljava/lang/String;
    .locals 4
    .param p1, "dataOP"    # [B

    .prologue
    .line 1081
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_1

    .line 1082
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Byte array to encript cannot be null or zero length"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1094
    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1096
    .local v1, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_4

    .line 1097
    aget-byte v2, p1, v0

    if-ltz v2, :cond_3

    .line 1098
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    const/16 v3, 0x10

    if-ge v2, v3, :cond_2

    .line 1101
    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1102
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1096
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1107
    :cond_2
    aget-byte v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1113
    :cond_3
    aget-byte v2, p1, v0

    mul-int/lit8 v2, v2, -0x1

    or-int/lit16 v2, v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 1120
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 328
    invoke-direct {p0, v4, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 331
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v2, v5, :cond_2

    .line 332
    iget-boolean v2, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v2, :cond_0

    .line 334
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    array-length v2, v2

    iget v3, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v3, v3, -0x5

    if-le v2, v3, :cond_1

    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No value for tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v4, v4, -0x5

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :cond_0
    :goto_0
    const-string v1, ""

    .line 353
    :goto_1
    return-object v1

    .line 337
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getValue() : new Tag - check Tags.java!!! Added tag = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "exception occurred while getting next TAG"

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 345
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->text:Ljava/lang/String;

    .line 347
    .local v1, "val":Ljava/lang/String;
    invoke-direct {p0, v4, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 349
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v2, v5, :cond_3

    .line 350
    new-instance v2, Ljava/io/IOException;

    const-string v3, "No END found!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 352
    :cond_3
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v2, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_1
.end method

.method public getValueAsStream(Ljava/io/OutputStream;Ljava/util/Observer;)I
    .locals 5
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "observer"    # Ljava/util/Observer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 436
    const/4 v0, -0x1

    .line 440
    .local v0, "ret_value":I
    invoke-direct {p0, v2, p1, p2}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 441
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v1, v3, :cond_0

    .line 442
    const/4 v0, 0x0

    .line 447
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->num:I

    .line 450
    invoke-direct {p0, v2, v4, v4}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 452
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v1, v3, :cond_1

    .line 453
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No END found!"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 455
    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v1, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 457
    return v0
.end method

.method public getValueInt()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 399
    const/4 v2, 0x1

    invoke-direct {p0, v2, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 400
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v2, v4, :cond_0

    move v0, v1

    .line 412
    :goto_0
    return v0

    .line 404
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->num:I

    .line 406
    .local v0, "val":I
    invoke-direct {p0, v1, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 408
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v1, v4, :cond_1

    .line 409
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No END found!"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 411
    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v1, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_0
.end method

.method public getValueIntForReminder()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 418
    const/4 v1, 0x1

    invoke-direct {p0, v1, v2, v2}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 419
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v1, v3, :cond_0

    .line 420
    const/4 v0, -0x1

    .line 431
    :goto_0
    return v0

    .line 423
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->num:I

    .line 425
    .local v0, "val":I
    const/4 v1, 0x0

    invoke-direct {p0, v1, v2, v2}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 427
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v1, v3, :cond_1

    .line 428
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No END found!"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 430
    :cond_1
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v1, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_0
.end method

.method public getValueOpaque()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 359
    invoke-direct {p0, v4, v2, v2}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 363
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v3, v5, :cond_2

    .line 364
    iget-boolean v3, p0, Lcom/android/exchange/adapter/Parser;->logging:Z

    if-eqz v3, :cond_0

    .line 366
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    array-length v3, v3

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v4, v4, -0x5

    if-le v3, v4, :cond_1

    .line 367
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No value for tag: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    add-int/lit8 v5, v5, -0x5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    move-object v1, v2

    .line 386
    :goto_1
    return-object v1

    .line 369
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getValueOpaque() : new Tag - check Tags.java!!! Added tag = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "exception occurred while getting next TAG"

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/Parser;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 378
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/adapter/Parser;->opaqueData:[B

    .line 380
    .local v1, "val":[B
    invoke-direct {p0, v4, v2, v2}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    .line 382
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-eq v2, v5, :cond_3

    .line 383
    new-instance v2, Ljava/io/IOException;

    const-string v3, "No END found!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 385
    :cond_3
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iput v2, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    goto :goto_1
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 1073
    iget-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->noContent:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method log(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 623
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 624
    .local v0, "cr":I
    if-lez v0, :cond_0

    .line 625
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 627
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/exchange/adapter/Parser;->logTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/exchange/adapter/Parser;->tid:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    return-void
.end method

.method public nextTag(I)I
    .locals 4
    .param p1, "endingTag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x3

    .line 477
    and-int/lit8 p1, p1, 0x3f

    iput p1, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    .line 478
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_3

    .line 480
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 481
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->page:I

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    .line 482
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->tag:I

    .line 492
    :cond_1
    :goto_0
    return v0

    .line 485
    :cond_2
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    iget v2, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 491
    :cond_3
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->endTag:I

    if-eqz v1, :cond_1

    .line 498
    new-instance v0, Lcom/android/exchange/adapter/Parser$EodException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EodException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0
.end method

.method public parse()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/EasException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method resetInput(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 613
    iput-object p1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    .line 614
    return-void
.end method

.method public setInput(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 604
    iput-object p1, p0, Lcom/android/exchange/adapter/Parser;->in:Ljava/io/InputStream;

    .line 605
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readByte()I

    .line 606
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 607
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 608
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->readInt()I

    .line 609
    sget-object v0, Lcom/android/exchange/adapter/Parser;->tagTables:[[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/Parser;->tagTable:[Ljava/lang/String;

    .line 610
    return-void
.end method

.method public setLoggingTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/android/exchange/adapter/Parser;->logTag:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public skipTag()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 509
    iget v0, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    .line 511
    .local v0, "thisTag":I
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3, v3}, Lcom/android/exchange/adapter/Parser;->getNext(ZLjava/io/OutputStream;Ljava/util/Observer;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 512
    iget v1, p0, Lcom/android/exchange/adapter/Parser;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    if-ne v1, v0, :cond_0

    .line 513
    return-void

    .line 518
    :cond_1
    new-instance v1, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v1
.end method

.method public skipTag(ZLandroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p1, "needskipped"    # Z
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "file"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x3

    .line 538
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v5, :cond_0

    .line 539
    const-string v5, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v6, "Paser::skipTag(3) start"

    invoke-static {v5, v6}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    :cond_0
    iget v3, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    .line 543
    .local v3, "thisTag":I
    iget v2, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    .line 544
    .local v2, "tagDepth":I
    const/4 v1, 0x0

    .line 546
    .local v1, "resultGetNextString":I
    if-eqz p1, :cond_1

    .line 547
    invoke-virtual {p0, p2, p3}, Lcom/android/exchange/adapter/Parser;->captureBytesOn(Landroid/content/Context;Ljava/lang/String;)V

    .line 550
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/Parser;->getNextStream()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 554
    iget v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v5, v7, :cond_2

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    if-ne v5, v3, :cond_2

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    if-ne v5, v2, :cond_2

    .line 555
    if-eqz p1, :cond_2

    .line 556
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->captureBytesOff()V

    .line 560
    :cond_2
    :goto_0
    iget v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v5, v7, :cond_7

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    if-ne v5, v3, :cond_7

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    if-ne v5, v2, :cond_7

    .line 561
    if-eqz p1, :cond_5

    .line 562
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v5, :cond_3

    .line 563
    const-string v5, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v6, "Paser::skipTag(3) end"

    invoke-static {v5, v6}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_3
    :goto_1
    return v4

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "ioe":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    iget v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v5, v7, :cond_2

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    if-ne v5, v3, :cond_2

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    if-ne v5, v2, :cond_2

    .line 555
    if-eqz p1, :cond_2

    .line 556
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->captureBytesOff()V

    goto :goto_0

    .line 554
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->type:I

    if-ne v5, v7, :cond_4

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->startTag:I

    if-ne v5, v3, :cond_4

    iget v5, p0, Lcom/android/exchange/adapter/Parser;->depth:I

    if-ne v5, v2, :cond_4

    .line 555
    if-eqz p1, :cond_4

    .line 556
    invoke-virtual {p0}, Lcom/android/exchange/adapter/Parser;->captureBytesOff()V

    :cond_4
    throw v4

    .line 568
    :cond_5
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v4, :cond_6

    .line 569
    const-string v4, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v5, "Paser::skipTag(3) end"

    invoke-static {v4, v5}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 576
    :cond_7
    if-ne v1, v4, :cond_1

    .line 579
    new-instance v4, Lcom/android/exchange/adapter/Parser$EofException;

    invoke-direct {v4, p0}, Lcom/android/exchange/adapter/Parser$EofException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v4
.end method

.method public stopParser()V
    .locals 1

    .prologue
    .line 1125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/Parser;->mForceStopParsing:Z

    .line 1126
    return-void
.end method
