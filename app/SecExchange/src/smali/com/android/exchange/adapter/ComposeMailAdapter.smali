.class public Lcom/android/exchange/adapter/ComposeMailAdapter;
.super Lcom/android/exchange/adapter/AbstractCommandAdapter;
.source "ComposeMailAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;
    }
.end annotation


# instance fields
.field private final STATUS_SUCCESS:I

.field private calendarRecEventFwd:Z

.field private calendarRecEventInstanceId:Ljava/lang/String;

.field private collectionId:Ljava/lang/String;

.field private httpCode:I

.field private isReply:Z

.field private isSaveInSentItems:Z

.field private isSmartSend:Z

.field private itemId:Ljava/lang/String;

.field private mIRMTemplateId:Ljava/lang/String;

.field private msgId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V
    .locals 9
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;
    .param p3, "msgID"    # Ljava/lang/String;
    .param p4, "itemId"    # Ljava/lang/String;
    .param p5, "collectionId"    # Ljava/lang/String;
    .param p6, "isSaveInSentItems"    # Z
    .param p7, "isSmartSend"    # Z
    .param p8, "isReply"    # Z
    .param p9, "calendarRecEventFwd"    # Z
    .param p10, "calendarRecEventInstanceId"    # Ljava/lang/String;

    .prologue
    .line 70
    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/AbstractCommandAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 33
    const/4 v3, 0x1

    iput v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->STATUS_SUCCESS:I

    .line 56
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventFwd:Z

    .line 58
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventInstanceId:Ljava/lang/String;

    .line 61
    const/16 v3, 0xc8

    iput v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I

    .line 63
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->msgId:Ljava/lang/String;

    .line 72
    iput-object p4, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->itemId:Ljava/lang/String;

    .line 73
    iput-object p5, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->collectionId:Ljava/lang/String;

    .line 74
    iput-boolean p6, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSaveInSentItems:Z

    .line 75
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSmartSend:Z

    .line 76
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isReply:Z

    .line 78
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventFwd:Z

    .line 79
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventInstanceId:Ljava/lang/String;

    .line 82
    iget-object v3, p2, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x402c333333333333L    # 14.1

    cmpl-double v3, v4, v6

    if-ltz v3, :cond_1

    .line 83
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 84
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "IRMTemplateId"

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->msgId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 87
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 88
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 89
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 91
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    .line 92
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v3, :cond_0

    .line 93
    const-string v3, "IRM"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "composemailadapter:mIRMTemplateId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 100
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/adapter/ComposeMailAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/ComposeMailAdapter;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSmartSend:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/exchange/adapter/ComposeMailAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/ComposeMailAdapter;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isReply:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/ComposeMailAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/ComposeMailAdapter;

    .prologue
    .line 29
    iget v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/exchange/adapter/ComposeMailAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/ComposeMailAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I

    return p1
.end method

.method public static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 299
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "SmartRF"

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_0
    return-void
.end method


# virtual methods
.method public callback(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 194
    return-void
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    const-string v0, "SmartReply/SmartForward"

    return-object v0
.end method

.method public getHttpCode()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I

    return v0
.end method

.method public hasChangedItems()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 158
    const-string v1, "Entering parse"

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;-><init>(Lcom/android/exchange/adapter/ComposeMailAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 160
    .local v0, "parser":Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/ComposeMailAdapter$SmartReplyForwardParser;->parse()Z

    move-result v1

    return v1
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 6
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    const-string v1, "Enter sendLocalChanges"

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "msgId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->msgId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "itemId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "collectionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->collectionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 114
    iget-object v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->msgId:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 116
    iget-boolean v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSmartSend:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isReply:Z

    if-eqz v1, :cond_4

    const/16 v0, 0x547

    .line 125
    .local v0, "startTag":I
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x551

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SendMail-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 127
    iget-boolean v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSaveInSentItems:Z

    if-eqz v1, :cond_0

    .line 128
    const/16 v1, 0x548

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 130
    :cond_0
    iget-boolean v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->isSmartSend:Z

    if-eqz v1, :cond_2

    .line 131
    const/16 v1, 0x54b

    invoke-virtual {p1, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x54c

    iget-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->collectionId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x54d

    iget-object v3, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->itemId:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 134
    iget-boolean v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventFwd:Z

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventInstanceId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 136
    const/16 v1, 0x54f

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->calendarRecEventInstanceId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 139
    :cond_1
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 148
    :cond_3
    :goto_1
    const/16 v1, 0x550

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 149
    const-string v1, "Exit 0 sendLocalChanges"

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 150
    const/4 v1, 0x1

    .line 153
    .end local v0    # "startTag":I
    :goto_2
    return v1

    .line 116
    :cond_4
    const/16 v0, 0x546

    goto :goto_0

    :cond_5
    const/16 v0, 0x545

    goto :goto_0

    .line 145
    .restart local v0    # "startTag":I
    :cond_6
    const/16 v1, 0x614

    iget-object v2, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->mIRMTemplateId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_1

    .line 152
    .end local v0    # "startTag":I
    :cond_7
    const-string v1, "Exit 1 sendLocalChanges"

    invoke-static {v1}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 153
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    return-void
.end method

.method public setHttpCode(I)V
    .locals 0
    .param p1, "httpCode"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/android/exchange/adapter/ComposeMailAdapter;->httpCode:I

    .line 169
    return-void
.end method

.method public wipe()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method
