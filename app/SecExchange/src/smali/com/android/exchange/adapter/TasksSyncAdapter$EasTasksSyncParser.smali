.class Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "TasksSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/TasksSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EasTasksSyncParser"
.end annotation


# instance fields
.field mAccountUri:Landroid/net/Uri;

.field mBindArgument:[Ljava/lang/String;

.field ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

.field final synthetic this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/TasksSyncAdapter;Z)V
    .locals 3
    .param p2, "parse"    # Lcom/android/exchange/adapter/Parser;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/TasksSyncAdapter;
    .param p4, "resumeParser"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 963
    iput-object p1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    .line 964
    invoke-direct {p0, p2, p3, p4}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V

    .line 947
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mBindArgument:[Ljava/lang/String;

    .line 953
    new-instance v0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/TasksSyncAdapter$1;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    .line 965
    sget-object v0, Lcom/android/emailcommon/provider/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {p1, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccountUri:Landroid/net/Uri;

    .line 966
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/TasksSyncAdapter;)V
    .locals 3
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/TasksSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 955
    iput-object p1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    .line 956
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 947
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mBindArgument:[Ljava/lang/String;

    .line 953
    new-instance v0, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/TasksSyncAdapter$1;)V

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    .line 957
    sget-object v0, Lcom/android/emailcommon/provider/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {p1, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccountUri:Landroid/net/Uri;

    .line 959
    return-void
.end method

.method private bodyParser(Landroid/content/ContentValues;Landroid/content/Entity;)V
    .locals 3
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "entity"    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1181
    const/4 v0, 0x0

    .line 1182
    .local v0, "value":Ljava/lang/String;
    :goto_0
    const/16 v1, 0x44a

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 1183
    iget v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 1214
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1190
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1192
    const-string v1, "bodyType"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1196
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1198
    const-string v1, "body_size"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1202
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1204
    const-string v1, "body_truncated"

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1208
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1210
    const-string v1, "body"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1217
    :cond_0
    return-void

    .line 1183
    :pswitch_data_0
    .packed-switch 0x446
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getClientIdCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "clientId"    # Ljava/lang/String;

    .prologue
    .line 1102
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mBindArgument:[Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1103
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    # getter for: Lcom/android/exchange/adapter/TasksSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$200()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "clientId=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mBindArgument:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1077
    const/4 v6, 0x0

    .line 1078
    .local v6, "c":Landroid/database/Cursor;
    const-wide/16 v8, -0x1

    .line 1080
    .local v8, "sync_account_key":J
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_sync_account_key"

    aput-object v5, v2, v3

    const-string v3, "_sync_account=? AND _sync_account_type=?"

    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    const-string v7, "com.android.exchange"

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1084
    if-eqz v6, :cond_0

    .line 1085
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1086
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 1090
    :cond_0
    if-eqz v6, :cond_1

    .line 1091
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1093
    :cond_1
    new-array v4, v12, [Ljava/lang/String;

    .line 1094
    .local v4, "mArguments":[Ljava/lang/String;
    aput-object p1, v4, v10

    .line 1095
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    .line 1096
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccountUri:Landroid/net/Uri;

    # getter for: Lcom/android/exchange/adapter/TasksSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$200()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "sourceid=? AND accountKey=?"

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 1090
    .end local v4    # "mArguments":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1091
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getTimeInMillis(Ljava/lang/String;)J
    .locals 7
    .param p1, "dateTime"    # Ljava/lang/String;

    .prologue
    .line 1165
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1166
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    const-wide/16 v2, 0x0

    .line 1168
    .local v2, "time_in_ms":J
    :try_start_0
    const-string v4, "GMT"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1169
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    .line 1170
    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->getCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1172
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "time_is_ms "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v4, v2

    .line 1177
    :goto_0
    return-wide v4

    .line 1174
    :catch_0
    move-exception v0

    .line 1175
    .local v0, "pe":Ljava/text/ParseException;
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cant parse dateTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    const-wide/16 v4, 0x0

    goto :goto_0
.end method

.method private moveParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V
    .locals 8
    .param p1, "ops"    # Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1019
    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "inside move parser because of soft delete "

    aput-object v4, v3, v5

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1020
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1021
    .local v1, "cv":Landroid/content/ContentValues;
    :cond_0
    :goto_0
    const/16 v3, 0x21

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 1022
    iget v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1024
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 1026
    .local v2, "serverId":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1027
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1029
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1030
    const-string v3, "complete"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1042
    iget-object v3, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    sget-object v4, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v3, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    .line 1046
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "taskComplete "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " completed value is : 1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1049
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3

    .line 1054
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v2    # "serverId":Ljava/lang/String;
    :cond_2
    return-void

    .line 1022
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method private recurrenceParser(Landroid/content/ContentValues;Landroid/content/Entity;)V
    .locals 4
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "entity"    # Landroid/content/Entity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1220
    :goto_0
    const/16 v0, 0x24f

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 1221
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v0, :pswitch_data_0

    .line 1266
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1223
    :pswitch_0
    const-string v0, "recurrence_type"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1227
    :pswitch_1
    const-string v0, "recurrence_start"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 1231
    :pswitch_2
    const-string v0, "recurrence_until"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 1235
    :pswitch_3
    const-string v0, "recurrence_occurrences"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1239
    :pswitch_4
    const-string v0, "recurrence_interval"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1243
    :pswitch_5
    const-string v0, "recurrence_day_of_month"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 1247
    :pswitch_6
    const-string v0, "recurrence_day_of_week"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1251
    :pswitch_7
    const-string v0, "recurrence_week_of_month"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1255
    :pswitch_8
    const-string v0, "recurrence_month_of_year"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1259
    :pswitch_9
    const-string v0, "recurrence_regenerate"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1263
    :pswitch_a
    const-string v0, "recurrence_dead_occur"

    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 1269
    :cond_0
    return-void

    .line 1221
    :pswitch_data_0
    .packed-switch 0x250
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method public addData(Ljava/lang/String;Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;Landroid/content/Entity;Z)V
    .locals 30
    .param p1, "serverId"    # Ljava/lang/String;
    .param p2, "ops"    # Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    .param p3, "entity"    # Landroid/content/Entity;
    .param p4, "is_update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1325
    const-string v4, "EasTasksSyncParser"

    const-string v5, "addData.."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    const/16 v21, 0x0

    .line 1328
    .local v21, "eventOffset":I
    const-wide/16 v14, 0x0

    .line 1330
    .local v14, "sync_account_key":J
    const/16 v25, 0x0

    .line 1331
    .local v25, "reminder_subject":Ljava/lang/String;
    const-wide/16 v10, 0x0

    .line 1332
    .local v10, "reminder_start_date":J
    const-wide/16 v12, 0x0

    .line 1333
    .local v12, "reminder_due_date":J
    const/16 v16, 0x0

    .line 1336
    .local v16, "reminder_type":I
    const/16 v19, 0x0

    .line 1338
    .local v19, "c1":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/Tasks$TasksAccounts;->CONTENT_URI:Landroid/net/Uri;

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v6, v0, [Ljava/lang/String;

    const/16 v27, 0x0

    const-string v28, "_sync_account_key"

    aput-object v28, v6, v27

    const-string v7, "_sync_account=? AND _sync_account_type=?"

    const/16 v27, 0x2

    move/from16 v0, v27

    new-array v8, v0, [Ljava/lang/String;

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v28, v0

    aput-object v28, v8, v27

    const/16 v27, 0x1

    const-string v28, "com.android.exchange"

    aput-object v28, v8, v27

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v19

    .line 1343
    if-nez v19, :cond_1

    .line 1350
    if-eqz v19, :cond_0

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1351
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object/from16 v9, v25

    .line 1638
    .end local v25    # "reminder_subject":Ljava/lang/String;
    .local v9, "reminder_subject":Ljava/lang/String;
    :goto_0
    return-void

    .line 1346
    .end local v9    # "reminder_subject":Ljava/lang/String;
    .restart local v25    # "reminder_subject":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1347
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v14

    .line 1350
    :cond_2
    if-eqz v19, :cond_3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1351
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1357
    :cond_3
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 1358
    .local v20, "cv":Landroid/content/ContentValues;
    const-string v4, "sourceid"

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1360
    const-string v4, "accountKey"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1361
    const-string v4, "mailboxKey"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1362
    const-string v4, "accountName"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    if-nez p4, :cond_6

    .line 1364
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    sget-object v5, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v17

    .line 1366
    .local v17, "builder":Landroid/content/ContentProviderOperation$Builder;
    # getter for: Lcom/android/exchange/adapter/TasksSyncAdapter;->PLACEHOLDER_OPERATION:Landroid/content/ContentProviderOperation;
    invoke-static {}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$300()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->newTask(Landroid/content/ContentProviderOperation;)I

    move-result v21

    move-object/from16 v9, v25

    .line 1386
    .end local v25    # "reminder_subject":Ljava/lang/String;
    .restart local v9    # "reminder_subject":Ljava/lang/String;
    :cond_4
    :goto_1
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_9

    .line 1390
    const/16 v26, 0x0

    .line 1392
    .local v26, "task_value":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    sparse-switch v4, :sswitch_data_0

    .line 1508
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_1

    .line 1350
    .end local v9    # "reminder_subject":Ljava/lang/String;
    .end local v17    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .end local v20    # "cv":Landroid/content/ContentValues;
    .end local v26    # "task_value":Ljava/lang/String;
    .restart local v25    # "reminder_subject":Ljava/lang/String;
    :catchall_0
    move-exception v4

    if-eqz v19, :cond_5

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1351
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4

    .line 1368
    .restart local v20    # "cv":Landroid/content/ContentValues;
    :cond_6
    const-wide/16 v22, 0x0

    .line 1369
    .local v22, "id":J
    invoke-direct/range {p0 .. p1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1370
    .local v18, "c":Landroid/database/Cursor;
    if-eqz v18, :cond_8

    .line 1372
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1373
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v27, "Updating "

    aput-object v27, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1375
    const-string v4, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-wide v22

    .line 1378
    :cond_7
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1381
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    iget-object v4, v4, Lcom/android/exchange/adapter/TasksSyncAdapter;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "inside tasksyncapdater updating id "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    sget-object v5, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v4, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v17

    .restart local v17    # "builder":Landroid/content/ContentProviderOperation$Builder;
    move-object/from16 v9, v25

    .end local v25    # "reminder_subject":Ljava/lang/String;
    .restart local v9    # "reminder_subject":Ljava/lang/String;
    goto/16 :goto_1

    .line 1378
    .end local v9    # "reminder_subject":Ljava/lang/String;
    .end local v17    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .restart local v25    # "reminder_subject":Ljava/lang/String;
    :catchall_1
    move-exception v4

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v4

    .line 1402
    .end local v18    # "c":Landroid/database/Cursor;
    .end local v22    # "id":J
    .end local v25    # "reminder_subject":Ljava/lang/String;
    .restart local v9    # "reminder_subject":Ljava/lang/String;
    .restart local v17    # "builder":Landroid/content/ContentProviderOperation$Builder;
    .restart local v26    # "task_value":Ljava/lang/String;
    :sswitch_0
    const-string v4, "body"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1406
    :sswitch_1
    const-string v4, "body_size"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 1410
    :sswitch_2
    const-string v4, "body_truncated"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 1415
    :sswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->bodyParser(Landroid/content/ContentValues;Landroid/content/Entity;)V

    goto/16 :goto_1

    .line 1422
    :pswitch_0
    const-string v4, "category1"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    :goto_2
    :sswitch_4
    const/16 v4, 0x248

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    .line 1420
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v4, :pswitch_data_0

    .line 1425
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_2

    .line 1430
    :sswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 1431
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Complete "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v4, "complete"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1437
    :sswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 1438
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Importance "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    const-string v4, "importance"

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    .line 1447
    :sswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 1448
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Reminder Set "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1449
    const-string v4, "reminder_set"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1454
    :sswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 1455
    const-string v4, "EasTasksSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Sensitivity "

    move-object/from16 v0, v27

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    const-string v4, "sensitivity"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1461
    :sswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v26

    .line 1462
    move-object/from16 v9, v26

    .line 1464
    const-string v4, "subject"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1472
    :sswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v6

    .line 1474
    .local v6, "reminder_time":J
    const-string v4, "reminder_time"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1478
    .end local v6    # "reminder_time":J
    :sswitch_b
    const-string v4, "date_completed"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1484
    :sswitch_c
    const-string v4, "due_date"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1485
    const-string v4, "due_date"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 1486
    goto/16 :goto_1

    .line 1489
    :sswitch_d
    const-string v4, "utc_due_date"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1495
    :sswitch_e
    const-string v4, "start_date"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1496
    const-string v4, "start_date"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 1497
    goto/16 :goto_1

    .line 1500
    :sswitch_f
    const-string v4, "utc_start_date"

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getTimeInMillis(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_1

    .line 1504
    :sswitch_10
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->recurrenceParser(Landroid/content/ContentValues;Landroid/content/Entity;)V

    goto/16 :goto_1

    .line 1511
    .end local v26    # "task_value":Ljava/lang/String;
    :cond_9
    const-string v4, "reminder_set"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1514
    const-string v4, "reminder_set"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 1515
    .local v24, "reminder_set":I
    if-lez v24, :cond_c

    .line 1517
    const-string v4, "reminder_type"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1518
    const-string v4, "reminder_type"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 1528
    .end local v24    # "reminder_set":I
    :cond_a
    :goto_3
    if-nez p4, :cond_b

    const-string v4, "reminder_set"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1530
    const-string v4, "reminder_set"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 1532
    .restart local v24    # "reminder_set":I
    const/4 v4, 0x1

    move/from16 v0, v24

    if-ne v0, v4, :cond_d

    .line 1535
    const-string v4, "reminder_time"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 1536
    .restart local v6    # "reminder_time":J
    const/4 v8, 0x0

    .local v8, "reminder_state":I
    move-object/from16 v5, p2

    .line 1538
    invoke-virtual/range {v5 .. v16}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->addReminder(JILjava/lang/String;JJJI)V

    .line 1543
    .end local v6    # "reminder_time":J
    .end local v8    # "reminder_state":I
    .end local v24    # "reminder_set":I
    :cond_b
    :goto_4
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 1544
    if-nez p4, :cond_e

    if-ltz v21, :cond_e

    .line 1547
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p2

    move/from16 v1, v21

    invoke-virtual {v0, v1, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1522
    .restart local v24    # "reminder_set":I
    :cond_c
    const-string v4, "reminder_time"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_3

    .line 1541
    :cond_d
    const-string v4, "reminder_time"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    goto :goto_4

    .line 1549
    .end local v24    # "reminder_set":I
    :cond_e
    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    goto/16 :goto_0

    .line 1392
    nop

    :sswitch_data_0
    .sparse-switch
        0x245 -> :sswitch_0
        0x246 -> :sswitch_1
        0x247 -> :sswitch_2
        0x248 -> :sswitch_4
        0x24a -> :sswitch_5
        0x24b -> :sswitch_b
        0x24c -> :sswitch_c
        0x24d -> :sswitch_d
        0x24e -> :sswitch_6
        0x24f -> :sswitch_10
        0x25b -> :sswitch_7
        0x25c -> :sswitch_a
        0x25d -> :sswitch_8
        0x25e -> :sswitch_e
        0x25f -> :sswitch_f
        0x260 -> :sswitch_9
        0x44a -> :sswitch_3
    .end sparse-switch

    .line 1420
    :pswitch_data_0
    .packed-switch 0x249
        :pswitch_0
    .end packed-switch
.end method

.method public addParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V
    .locals 3
    .param p1, "ops"    # Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1057
    const/4 v0, 0x0

    .line 1058
    .local v0, "serverId":Ljava/lang/String;
    const-string v1, "EasTasksSyncParser"

    const-string v2, "addParser.."

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1059
    :goto_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 1060
    iget v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    sparse-switch v1, :sswitch_data_0

    .line 1070
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1062
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1065
    goto :goto_0

    .line 1067
    :sswitch_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->addData(Ljava/lang/String;Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;Landroid/content/Entity;Z)V

    goto :goto_0

    .line 1073
    :cond_0
    return-void

    .line 1060
    nop

    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public addResponsesParser()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1641
    const/4 v4, 0x0

    .line 1642
    .local v4, "serverId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1643
    .local v1, "clientId":Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1644
    .local v2, "cv":Landroid/content/ContentValues;
    new-instance v3, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    const/4 v6, 0x0

    invoke-direct {v3, v5, v6}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;-><init>(Lcom/android/exchange/adapter/TasksSyncAdapter;Lcom/android/exchange/adapter/TasksSyncAdapter$1;)V

    .line 1645
    .local v3, "ops":Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    :goto_0
    const/4 v5, 0x7

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_0

    .line 1646
    iget v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v5, :pswitch_data_0

    .line 1657
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1648
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1649
    goto :goto_0

    .line 1651
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1652
    goto :goto_0

    .line 1654
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    goto :goto_0

    .line 1660
    :cond_0
    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "server id and client ids are "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1662
    if-eqz v1, :cond_1

    if-nez v4, :cond_2

    .line 1691
    :cond_1
    :goto_1
    return-void

    .line 1664
    :cond_2
    new-array v5, v9, [Ljava/lang/String;

    const-string v6, " not returned from task till now, going to write to db response vals "

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1665
    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getClientIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1666
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 1667
    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " The cursor size for updation is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1669
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1670
    const-string v5, "sourceid"

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    const-string v5, "_sync_dirty"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1678
    iget-object v5, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    sget-object v6, Lcom/android/emailcommon/provider/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    # invokes: Lcom/android/exchange/adapter/TasksSyncAdapter;->uriWithAccountAndIsSyncAdapter(Landroid/net/Uri;)Landroid/net/Uri;
    invoke-static {v5, v6}, Lcom/android/exchange/adapter/TasksSyncAdapter;->access$100(Lcom/android/exchange/adapter/TasksSyncAdapter;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->add(Landroid/content/ContentProviderOperation;)Z

    .line 1683
    invoke-virtual {v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->execute()V

    .line 1684
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "New task "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was given serverId: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1687
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :catchall_0
    move-exception v5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v5

    .line 1646
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public changeParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V
    .locals 6
    .param p1, "ops"    # Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1132
    const/4 v1, 0x0

    .line 1133
    .local v1, "serverId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1134
    .local v0, "entity":Landroid/content/Entity;
    :goto_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 1135
    iget v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    sparse-switch v2, :sswitch_data_0

    .line 1159
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1137
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1139
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Changing task "

    aput-object v4, v2, v3

    aput-object v1, v2, v5

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 1156
    :sswitch_1
    invoke-virtual {p0, v1, p1, v0, v5}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->addData(Ljava/lang/String;Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;Landroid/content/Entity;Z)V

    goto :goto_0

    .line 1162
    :cond_0
    return-void

    .line 1135
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public changeResponsesParser()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1694
    const/4 v0, 0x0

    .line 1695
    .local v0, "serverId":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1696
    .local v1, "status":Ljava/lang/String;
    :goto_0
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 1697
    iget v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 1705
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1699
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 1700
    goto :goto_0

    .line 1702
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1703
    goto :goto_0

    .line 1708
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1709
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Changed task "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed with status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1711
    :cond_1
    return-void

    .line 1697
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public commandsParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 972
    const-string v0, "EasTasksSyncParser"

    const-string v1, "commandsParser.."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    :goto_0
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 974
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 975
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->addParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V

    .line 976
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 977
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 978
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->deleteParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V

    .line 979
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 980
    :cond_1
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 981
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->changeParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V

    .line 982
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 984
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/16 v1, 0x21

    if-ne v0, v1, :cond_3

    .line 985
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->moveParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V

    .line 986
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->this$0:Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/TasksSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 988
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 990
    :cond_4
    return-void
.end method

.method public commit()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 994
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v1, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 998
    iget-object v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->ops:Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->execute()V

    .line 1000
    return-void
.end method

.method public deleteParser(Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;)V
    .locals 5
    .param p1, "ops"    # Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1108
    :cond_0
    :goto_0
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    .line 1109
    iget v2, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    packed-switch v2, :pswitch_data_0

    .line 1126
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1111
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 1113
    .local v1, "serverId":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->getServerIdCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1114
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1116
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1117
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Deleting "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->userLog([Ljava/lang/String;)V

    .line 1118
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/android/exchange/adapter/TasksSyncAdapter$TaskOperations;->delete(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1121
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1129
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "serverId":Ljava/lang/String;
    :cond_2
    return-void

    .line 1109
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public responsesParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1005
    const-string v0, "EasTasksSyncParser"

    const-string v1, "responsesParser.."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    :goto_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 1007
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 1008
    const-string v0, "EasTasksSyncParser"

    const-string v1, " inside add responsesParser.."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->addResponsesParser()V

    goto :goto_0

    .line 1010
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->tag:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1011
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->changeResponsesParser()V

    goto :goto_0

    .line 1013
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/TasksSyncAdapter$EasTasksSyncParser;->skipTag()V

    goto :goto_0

    .line 1015
    :cond_2
    return-void
.end method
