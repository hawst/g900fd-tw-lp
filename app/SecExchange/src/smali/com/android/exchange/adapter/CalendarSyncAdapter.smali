.class public Lcom/android/exchange/adapter/CalendarSyncAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "CalendarSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/CalendarSyncAdapter$CalendarOperations;,
        Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    }
.end annotation


# static fields
.field private static final ATTNDEES_PROJECTION:[Ljava/lang/String;

.field private static final EVENTS_URI:Landroid/net/Uri;

.field private static final EXTENDED_PROPERTY_PROJECTION:[Ljava/lang/String;

.field private static final ID_PROJECTION:[Ljava/lang/String;

.field private static final LOCK:Ljava/lang/Object;

.field private static final ORIGINAL_EVENT_PROJECTION:[Ljava/lang/String;

.field private static final PLACEHOLDER_OPERATION:Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

.field private static final REMINDERS_PROJECTION:[Ljava/lang/String;

.field private static final UTC_TIMEZONE:Ljava/util/TimeZone;

.field private static final sSyncKeyLock:Ljava/lang/Object;


# instance fields
.field private final mAsSyncAdapterAttendees:Landroid/net/Uri;

.field private final mAsSyncAdapterEvents:Landroid/net/Uri;

.field private final mAsSyncAdapterExtendedProperties:Landroid/net/Uri;

.field private final mAsSyncAdapterReminders:Landroid/net/Uri;

.field private mCalendarId:J

.field private mCalendarIdArgument:[Ljava/lang/String;

.field private mCalendarIdString:Ljava/lang/String;

.field private mDeletedEventsIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDeletedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mEmailAddress:Ljava/lang/String;

.field private final mLocalTimeZone:Ljava/util/TimeZone;

.field private mNewEventServerIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOutgoingMailList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            ">;"
        }
    .end annotation
.end field

.field private mSendCancelIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mUploadedIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 177
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_sync_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    .line 197
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "original_id"

    aput-object v1, v0, v2

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ORIGINAL_EVENT_PROJECTION:[Ljava/lang/String;

    .line 225
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->EXTENDED_PROPERTY_PROJECTION:[Ljava/lang/String;

    .line 230
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->REMINDERS_PROJECTION:[Ljava/lang/String;

    .line 234
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ATTNDEES_PROJECTION:[Ljava/lang/String;

    .line 269
    new-instance v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    sget-object v1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;-><init>(Landroid/content/ContentProviderOperation$Builder;)V

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->PLACEHOLDER_OPERATION:Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    .line 280
    sget-object v0, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapterForBusystatus(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->EVENTS_URI:Landroid/net/Uri;

    .line 282
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sSyncKeyLock:Ljava/lang/Object;

    .line 284
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->UTC_TIMEZONE:Ljava/util/TimeZone;

    .line 318
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 4
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 367
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 286
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    .line 320
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    .line 328
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 331
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedEventsIdList:Ljava/util/ArrayList;

    .line 333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSendCancelIdList:Ljava/util/ArrayList;

    .line 337
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    .line 339
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mNewEventServerIdList:Ljava/util/ArrayList;

    .line 368
    iget-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    .line 371
    const-string v0, "com.android.exchange"

    .line 372
    .local v0, "amType":Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterAttendees:Landroid/net/Uri;

    .line 374
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterEvents:Landroid/net/Uri;

    .line 376
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterReminders:Landroid/net/Uri;

    .line 378
    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterExtendedProperties:Landroid/net/Uri;

    .line 381
    invoke-virtual {p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->init()V

    .line 382
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/EasSyncService;)V
    .locals 4
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 348
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 286
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    .line 320
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    .line 328
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    .line 331
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedEventsIdList:Ljava/util/ArrayList;

    .line 333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSendCancelIdList:Ljava/util/ArrayList;

    .line 337
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    .line 339
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mNewEventServerIdList:Ljava/util/ArrayList;

    .line 349
    iget-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    .line 352
    const-string v0, "com.android.exchange"

    .line 353
    .local v0, "amType":Ljava/lang/String;
    sget-object v1, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterAttendees:Landroid/net/Uri;

    .line 355
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterEvents:Landroid/net/Uri;

    .line 357
    sget-object v1, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterReminders:Landroid/net/Uri;

    .line 359
    sget-object v1, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterExtendedProperties:Landroid/net/Uri;

    .line 362
    invoke-virtual {p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->init()V

    .line 363
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/TimeZone;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->UTC_TIMEZONE:Ljava/util/TimeZone;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedEventsIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSendCancelIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/exchange/adapter/CalendarSyncAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteCommittedEvents()V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterExtendedProperties:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1800()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->EXTENDED_PROPERTY_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterReminders:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/exchange/adapter/CalendarSyncAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    return-wide v0
.end method

.method static synthetic access$2000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->REMINDERS_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterAttendees:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$400()Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->PLACEHOLDER_OPERATION:Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/exchange/adapter/CalendarSyncAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->truncateEventDescription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterEvents:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mNewEventServerIdList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/exchange/adapter/CalendarSyncAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/adapter/CalendarSyncAdapter;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    return-object v0
.end method

.method public static asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "accountType"    # Ljava/lang/String;

    .prologue
    .line 611
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static asSyncAdapterForBusystatus(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 620
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private decodeVisibility(I)Ljava/lang/String;
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 2719
    const/4 v0, 0x0

    .line 2720
    .local v0, "easVisibility":I
    packed-switch p1, :pswitch_data_0

    .line 2738
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2723
    :pswitch_0
    const/4 v0, 0x0

    .line 2724
    goto :goto_0

    .line 2727
    :pswitch_1
    const/4 v0, 0x1

    .line 2728
    goto :goto_0

    .line 2731
    :pswitch_2
    const/4 v0, 0x2

    .line 2732
    goto :goto_0

    .line 2735
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 2720
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private deleteCommittedEvents()V
    .locals 6

    .prologue
    .line 4474
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Inside deleteCommittedEvents()"

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4476
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mNewEventServerIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 4477
    .local v2, "serverId":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4479
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "serverId":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4480
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    const-string v4, "Exception in deleteCommittedEvents"

    invoke-virtual {v3, v4, v0}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 4482
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method private getCalendarFilter()Ljava/lang/String;
    .locals 2

    .prologue
    .line 530
    sget-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    .line 531
    .local v0, "filter":Ljava/lang/String;
    iget-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mCalendarSyncLookback:I

    packed-switch v1, :pswitch_data_0

    .line 546
    :pswitch_0
    sget-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    .line 550
    :goto_0
    return-object v0

    .line 533
    :pswitch_1
    const-string v0, "0"

    .line 534
    goto :goto_0

    .line 536
    :pswitch_2
    sget-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_1_MONTH:Ljava/lang/String;

    .line 537
    goto :goto_0

    .line 539
    :pswitch_3
    const-string v0, "6"

    .line 540
    goto :goto_0

    .line 542
    :pswitch_4
    const-string v0, "7"

    .line 543
    goto :goto_0

    .line 531
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private getCalendarUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 522
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.android.exchange"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getInt(Landroid/content/ContentValues;Ljava/lang/String;)I
    .locals 2
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "column"    # Ljava/lang/String;

    .prologue
    .line 2742
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 2743
    .local v0, "i":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 2744
    const/4 v1, 0x0

    .line 2745
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method private getServerIdDeleteCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 741
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "(_sync_id =? OR original_sync_id=?) AND calendar_id=?"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private getSubFolderCalendarEventsCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "mailboxServerId"    # Ljava/lang/String;

    .prologue
    .line 4529
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "sync_data5=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private sendEvent(Landroid/content/Entity;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    .locals 60
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2754
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v18

    .line 2755
    .local v18, "entityValues":Landroid/content/ContentValues;
    if-nez p2, :cond_12

    const/16 v30, 0x1

    .line 2756
    .local v30, "isException":Z
    :goto_0
    const/16 v24, 0x0

    .line 2757
    .local v24, "hasAttendees":Z
    const-string v55, "_sync_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v27

    .line 2758
    .local v27, "isChange":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v53, v0

    .line 2759
    .local v53, "version":Ljava/lang/Double;
    const-string v55, "allDay"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getIntegerValueAsBoolean(Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v4

    .line 2764
    .local v4, "allDay":Z
    const-string v55, "deleted"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    .line 2765
    .local v10, "deleted":Ljava/lang/Integer;
    if-eqz v10, :cond_13

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v55

    const/16 v56, 0x1

    move/from16 v0, v55

    move/from16 v1, v56

    if-ne v0, v1, :cond_13

    const/16 v28, 0x1

    .line 2766
    .local v28, "isDeleted":Z
    :goto_1
    const-string v55, "eventStatus"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v19

    .line 2767
    .local v19, "eventStatus":Ljava/lang/Integer;
    if-eqz v19, :cond_14

    const/16 v55, 0x2

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v55

    move-object/from16 v0, v19

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_14

    const/16 v26, 0x1

    .line 2768
    .local v26, "isCanceled":Z
    :goto_2
    const-string v55, "dirty"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 2774
    .local v29, "isDirty":Ljava/lang/String;
    if-eqz v30, :cond_3

    .line 2782
    if-nez v28, :cond_0

    if-eqz v26, :cond_15

    .line 2783
    :cond_0
    const/16 v55, 0x115

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2787
    if-eqz v28, :cond_1

    if-nez v26, :cond_1

    .line 2788
    const-string v55, "_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 2789
    .local v20, "eventId":J
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2790
    .local v9, "cv":Landroid/content/ContentValues;
    const-string v55, "eventStatus"

    const/16 v56, 0x2

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v55, v0

    sget-object v56, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v56

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v56

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v57, v0

    const-string v58, "com.android.exchange"

    invoke-static/range {v56 .. v58}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v56

    const/16 v57, 0x0

    const/16 v58, 0x0

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    move-object/from16 v2, v57

    move-object/from16 v3, v58

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2803
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v20    # "eventId":J
    :cond_1
    :goto_3
    const-string v55, "originalInstanceTime"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v39

    .line 2804
    .local v39, "originalTime":Ljava/lang/Long;
    if-eqz v39, :cond_3

    .line 2805
    const-string v55, "originalAllDay"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getIntegerValueAsBoolean(Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v38

    .line 2807
    .local v38, "originalAllDay":Z
    if-eqz v38, :cond_2

    .line 2809
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v56

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    move-object/from16 v55, v0

    move-wide/from16 v0, v56

    move-object/from16 v2, v55

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v56

    invoke-static/range {v56 .. v57}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    .line 2812
    :cond_2
    const/16 v55, 0x116

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v56

    invoke-static/range {v56 .. v57}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2823
    .end local v38    # "originalAllDay":Z
    .end local v39    # "originalTime":Ljava/lang/Long;
    :cond_3
    if-eqz v4, :cond_16

    const-string v55, "sync_data1"

    :goto_4
    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 2826
    .local v51, "timeZoneName":Ljava/lang/String;
    if-nez v51, :cond_4

    .line 2827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v51

    .line 2829
    :cond_4
    invoke-static/range {v51 .. v51}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v22

    .line 2831
    .local v22, "eventTimeZone":Ljava/util/TimeZone;
    if-nez v30, :cond_5

    .line 2835
    invoke-static/range {v22 .. v22}, Lcom/android/exchange/utility/CalendarUtilities;->timeZoneToTziString(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v50

    .line 2836
    .local v50, "timeZone":Ljava/lang/String;
    const/16 v55, 0x105

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2841
    .end local v50    # "timeZone":Ljava/lang/String;
    :cond_5
    if-eqz v30, :cond_6

    if-eqz v30, :cond_7

    if-nez v28, :cond_7

    if-nez v26, :cond_7

    .line 2842
    :cond_6
    const/16 v56, 0x106

    if-eqz v4, :cond_17

    const-string v55, "1"

    :goto_5
    move-object/from16 v0, p3

    move/from16 v1, v56

    move-object/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2843
    const/16 v55, 0x1

    move/from16 v0, v55

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v56, v0

    const/16 v57, 0x0

    new-instance v55, Ljava/lang/StringBuilder;

    invoke-direct/range {v55 .. v55}, Ljava/lang/StringBuilder;-><init>()V

    const-string v58, "Allday: "

    move-object/from16 v0, v55

    move-object/from16 v1, v58

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v58

    if-eqz v4, :cond_18

    const-string v55, "1"

    :goto_6
    move-object/from16 v0, v58

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v55

    aput-object v55, v56, v57

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 2848
    :cond_7
    const-string v55, "dtstart"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Long;->longValue()J

    move-result-wide v46

    .line 2853
    .local v46, "startTime":J
    const-string v55, "dtend"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_19

    .line 2854
    const-string v55, "dtend"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 2868
    .local v16, "endTime":J
    :goto_7
    if-eqz v4, :cond_8

    .line 2869
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    move-object/from16 v52, v0

    .line 2870
    .local v52, "tz":Ljava/util/TimeZone;
    move-wide/from16 v0, v46

    move-object/from16 v2, v52

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v46

    .line 2871
    move-wide/from16 v0, v16

    move-object/from16 v2, v52

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v16

    .line 2877
    .end local v52    # "tz":Ljava/util/TimeZone;
    :cond_8
    if-eqz v30, :cond_9

    if-eqz v30, :cond_d

    if-nez v28, :cond_d

    if-nez v26, :cond_d

    .line 2878
    :cond_9
    const/16 v55, 0x127

    invoke-static/range {v46 .. v47}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2879
    const/16 v55, 0x112

    invoke-static/range {v16 .. v17}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2880
    const/16 v55, 0x111

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v56

    invoke-static/range {v56 .. v57}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2882
    const-string v55, "eventLocation"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 2883
    .local v31, "loc":Ljava/lang/String;
    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v55

    if-nez v55, :cond_b

    .line 2884
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpg-double v55, v56, v58

    if-gez v55, :cond_a

    .line 2886
    invoke-static/range {v31 .. v31}, Lcom/android/emailcommon/utility/Utility;->replaceBareLfWithCrlf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 2888
    :cond_a
    const/16 v55, 0x117

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2891
    :cond_b
    const-string v55, "title"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    .line 2893
    .local v49, "subj":Ljava/lang/String;
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_1c

    .line 2894
    if-eqz v49, :cond_1b

    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->length()I

    move-result v55

    if-lez v55, :cond_1b

    .line 2895
    const/16 v55, 0x126

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v49

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2914
    :goto_8
    if-eqz v30, :cond_c

    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpg-double v55, v56, v58

    if-ltz v55, :cond_d

    .line 2915
    :cond_c
    const-string v55, "accessLevel"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v54

    .line 2917
    .local v54, "visibility":Ljava/lang/Integer;
    if-eqz v54, :cond_1e

    .line 2918
    const/16 v55, 0x125

    invoke-virtual/range {v54 .. v54}, Ljava/lang/Integer;->intValue()I

    move-result v56

    move-object/from16 v0, p0

    move/from16 v1, v56

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->decodeVisibility(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2928
    .end local v31    # "loc":Ljava/lang/String;
    .end local v49    # "subj":Ljava/lang/String;
    .end local v54    # "visibility":Ljava/lang/Integer;
    :cond_d
    :goto_9
    const-string v55, "description"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2929
    .local v11, "desc":Ljava/lang/String;
    if-eqz v11, :cond_20

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v55

    if-lez v55, :cond_20

    .line 2930
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_1f

    .line 2931
    const/16 v55, 0x44a

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2932
    const/16 v55, 0x446

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2933
    const/16 v55, 0x44b

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v11}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2934
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 2951
    :goto_a
    if-nez v30, :cond_38

    .line 2953
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-gez v55, :cond_e

    if-nez v27, :cond_f

    .line 2954
    :cond_e
    const-string v55, "organizer"

    const/16 v56, 0x119

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    move-object/from16 v2, v55

    move/from16 v3, v56

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->writeStringValue(Landroid/content/ContentValues;Ljava/lang/String;I)V

    .line 2957
    :cond_f
    const-string v55, "rrule"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v43

    .line 2958
    .local v43, "rrule":Ljava/lang/String;
    if-eqz v43, :cond_10

    .line 2959
    move-object/from16 v0, v43

    move-wide/from16 v1, v46

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/utility/CalendarUtilities;->recurrenceFromRrule(Ljava/lang/String;JLcom/android/exchange/adapter/Serializer;)V

    .line 2964
    :cond_10
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v48

    .line 2967
    .local v48, "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    const/4 v13, -0x1

    .line 2968
    .local v13, "earliestReminder":I
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .local v25, "i$":Ljava/util/Iterator;
    :cond_11
    :goto_b
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_25

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Entity$NamedContentValues;

    .line 2969
    .local v33, "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v34, v0

    .line 2970
    .local v34, "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v35, v0

    .line 2971
    .local v35, "ncvValues":Landroid/content/ContentValues;
    sget-object v55, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_23

    .line 2972
    const-string v55, "name"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 2973
    .local v40, "propertyName":Ljava/lang/String;
    const-string v55, "value"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    .line 2974
    .local v41, "propertyValue":Ljava/lang/String;
    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v55

    if-nez v55, :cond_11

    .line 2977
    const-string v55, "categories"

    move-object/from16 v0, v40

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_11

    .line 2980
    new-instance v45, Ljava/util/StringTokenizer;

    const-string v55, "\\"

    move-object/from16 v0, v45

    move-object/from16 v1, v41

    move-object/from16 v2, v55

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2982
    .local v45, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v45 .. v45}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v55

    if-lez v55, :cond_11

    .line 2983
    const/16 v55, 0x10e

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2984
    :goto_c
    invoke-virtual/range {v45 .. v45}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v55

    if-eqz v55, :cond_22

    .line 2985
    invoke-virtual/range {v45 .. v45}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 2986
    .local v8, "category":Ljava/lang/String;
    const/16 v55, 0x10f

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v8}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_c

    .line 2755
    .end local v4    # "allDay":Z
    .end local v8    # "category":Ljava/lang/String;
    .end local v10    # "deleted":Ljava/lang/Integer;
    .end local v11    # "desc":Ljava/lang/String;
    .end local v13    # "earliestReminder":I
    .end local v16    # "endTime":J
    .end local v19    # "eventStatus":Ljava/lang/Integer;
    .end local v22    # "eventTimeZone":Ljava/util/TimeZone;
    .end local v24    # "hasAttendees":Z
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v26    # "isCanceled":Z
    .end local v27    # "isChange":Z
    .end local v28    # "isDeleted":Z
    .end local v29    # "isDirty":Ljava/lang/String;
    .end local v30    # "isException":Z
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    .end local v40    # "propertyName":Ljava/lang/String;
    .end local v41    # "propertyValue":Ljava/lang/String;
    .end local v43    # "rrule":Ljava/lang/String;
    .end local v45    # "st":Ljava/util/StringTokenizer;
    .end local v46    # "startTime":J
    .end local v48    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    .end local v51    # "timeZoneName":Ljava/lang/String;
    .end local v53    # "version":Ljava/lang/Double;
    :cond_12
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 2765
    .restart local v4    # "allDay":Z
    .restart local v10    # "deleted":Ljava/lang/Integer;
    .restart local v24    # "hasAttendees":Z
    .restart local v27    # "isChange":Z
    .restart local v30    # "isException":Z
    .restart local v53    # "version":Ljava/lang/Double;
    :cond_13
    const/16 v28, 0x0

    goto/16 :goto_1

    .line 2767
    .restart local v19    # "eventStatus":Ljava/lang/Integer;
    .restart local v28    # "isDeleted":Z
    :cond_14
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 2799
    .restart local v26    # "isCanceled":Z
    .restart local v29    # "isDirty":Ljava/lang/String;
    :cond_15
    const/16 v55, 0x115

    const-string v56, "0"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_3

    .line 2823
    :cond_16
    const-string v55, "eventTimezone"

    goto/16 :goto_4

    .line 2842
    .restart local v22    # "eventTimeZone":Ljava/util/TimeZone;
    .restart local v51    # "timeZoneName":Ljava/lang/String;
    :cond_17
    const-string v55, "0"

    goto/16 :goto_5

    .line 2843
    :cond_18
    const-string v55, "0"

    goto/16 :goto_6

    .line 2856
    .restart local v46    # "startTime":J
    :cond_19
    const-wide/32 v14, 0x36ee80

    .line 2857
    .local v14, "durationMillis":J
    const-string v55, "duration"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_1a

    .line 2858
    new-instance v12, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V

    .line 2860
    .local v12, "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_0
    const-string v55, "duration"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    move-object/from16 v0, v55

    invoke-virtual {v12, v0}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 2861
    invoke-virtual {v12}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    .line 2866
    .end local v12    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    :cond_1a
    :goto_d
    add-long v16, v46, v14

    .restart local v16    # "endTime":J
    goto/16 :goto_7

    .line 2897
    .end local v14    # "durationMillis":J
    .restart local v31    # "loc":Ljava/lang/String;
    .restart local v49    # "subj":Ljava/lang/String;
    :cond_1b
    const/16 v55, 0x126

    const-string v56, ""

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_8

    .line 2900
    :cond_1c
    if-eqz v49, :cond_1d

    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->length()I

    move-result v55

    if-lez v55, :cond_1d

    .line 2901
    const-string v55, "title"

    const/16 v56, 0x126

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    move-object/from16 v2, v55

    move/from16 v3, v56

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->writeStringValue(Landroid/content/ContentValues;Ljava/lang/String;I)V

    goto/16 :goto_8

    .line 2904
    :cond_1d
    const/16 v55, 0x126

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_8

    .line 2922
    .restart local v54    # "visibility":Ljava/lang/Integer;
    :cond_1e
    const/16 v55, 0x125

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_9

    .line 2937
    .end local v31    # "loc":Ljava/lang/String;
    .end local v49    # "subj":Ljava/lang/String;
    .end local v54    # "visibility":Ljava/lang/Integer;
    .restart local v11    # "desc":Ljava/lang/String;
    :cond_1f
    const/16 v55, 0x10b

    invoke-static {v11}, Lcom/android/emailcommon/utility/Utility;->replaceBareLfWithCrlf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_a

    .line 2940
    :cond_20
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_21

    .line 2941
    const/16 v55, 0x44a

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2942
    const/16 v55, 0x446

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2943
    const/16 v55, 0x44b

    const-string v56, ""

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2944
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_a

    .line 2947
    :cond_21
    const/16 v55, 0x10b

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_a

    .line 2988
    .restart local v13    # "earliestReminder":I
    .restart local v25    # "i$":Ljava/util/Iterator;
    .restart local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .restart local v34    # "ncvUri":Landroid/net/Uri;
    .restart local v35    # "ncvValues":Landroid/content/ContentValues;
    .restart local v40    # "propertyName":Ljava/lang/String;
    .restart local v41    # "propertyValue":Ljava/lang/String;
    .restart local v43    # "rrule":Ljava/lang/String;
    .restart local v45    # "st":Ljava/util/StringTokenizer;
    .restart local v48    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    :cond_22
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_b

    .line 2991
    .end local v40    # "propertyName":Ljava/lang/String;
    .end local v41    # "propertyValue":Ljava/lang/String;
    .end local v45    # "st":Ljava/util/StringTokenizer;
    :cond_23
    sget-object v55, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_11

    .line 2992
    const-string v55, "minutes"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v32

    .line 2993
    .local v32, "mins":Ljava/lang/Integer;
    if-eqz v32, :cond_11

    .line 2995
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v55

    if-gez v55, :cond_24

    .line 2996
    const/16 v55, 0x1e

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    .line 3000
    :cond_24
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v55

    move/from16 v0, v55

    if-le v0, v13, :cond_11

    .line 3001
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v13

    goto/16 :goto_b

    .line 3008
    .end local v32    # "mins":Ljava/lang/Integer;
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    :cond_25
    if-ltz v13, :cond_26

    .line 3009
    const/16 v55, 0x124

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3016
    :cond_26
    if-eqz p2, :cond_27

    .line 3017
    const/16 v55, 0x128

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3022
    :cond_27
    const/16 v37, 0x0

    .line 3023
    .local v37, "organizerName":Ljava/lang/String;
    const/16 v36, 0x0

    .line 3026
    .local v36, "organizerEmail":Ljava/lang/String;
    const-string v55, "organizer"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_2a

    .line 3027
    const-string v55, "organizer"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3044
    :cond_28
    :goto_e
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_29
    :goto_f
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_32

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Entity$NamedContentValues;

    .line 3045
    .restart local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v34, v0

    .line 3046
    .restart local v34    # "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v35, v0

    .line 3047
    .restart local v35    # "ncvValues":Landroid/content/ContentValues;
    sget-object v55, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_29

    .line 3048
    const-string v55, "attendeeRelationship"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    .line 3051
    .local v42, "relationship":Ljava/lang/Integer;
    if-eqz v42, :cond_29

    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_29

    .line 3053
    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v55

    const/16 v56, 0x2

    move/from16 v0, v55

    move/from16 v1, v56

    if-ne v0, v1, :cond_2c

    .line 3054
    const-string v55, "attendeeName"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 3055
    goto :goto_f

    .line 3029
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    .end local v42    # "relationship":Ljava/lang/Integer;
    :cond_2a
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_2b
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_28

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Entity$NamedContentValues;

    .line 3030
    .restart local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v35, v0

    .line 3031
    .restart local v35    # "ncvValues":Landroid/content/ContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v55, v0

    sget-object v56, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v55 .. v56}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_2b

    .line 3032
    const-string v55, "attendeeRelationship"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    .line 3035
    .restart local v42    # "relationship":Ljava/lang/Integer;
    if-eqz v42, :cond_2b

    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_2b

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v55

    const/16 v56, 0x2

    move/from16 v0, v55

    move/from16 v1, v56

    if-ne v0, v1, :cond_2b

    .line 3037
    const-string v55, "attendeeName"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 3038
    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3039
    goto/16 :goto_e

    .line 3058
    .restart local v34    # "ncvUri":Landroid/net/Uri;
    :cond_2c
    if-nez v24, :cond_2d

    .line 3059
    const/16 v55, 0x107

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3060
    const/16 v24, 0x1

    .line 3062
    :cond_2d
    const/16 v55, 0x108

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3063
    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3064
    .local v5, "attendeeEmail":Ljava/lang/String;
    const-string v55, "attendeeName"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3065
    .local v6, "attendeeName":Ljava/lang/String;
    const-string v55, "_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3066
    .local v20, "eventId":Ljava/lang/String;
    if-nez v6, :cond_31

    .line 3067
    move-object v6, v5

    .line 3074
    :cond_2e
    :goto_10
    const/16 v55, 0x10a

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3075
    const/16 v55, 0x109

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3076
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_2f

    .line 3077
    const/16 v55, 0x12a

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3079
    :cond_2f
    const-string v55, "1"

    move-object/from16 v0, v55

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_30

    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_30

    .line 3080
    const/16 v55, 0x129

    const/16 v56, 0x0

    invoke-static/range {v56 .. v56}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3081
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 3082
    .restart local v9    # "cv":Landroid/content/ContentValues;
    const-string v55, "attendeeStatus"

    const/16 v56, 0x0

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v55

    sget-object v56, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    const-string v57, "event_id=? AND attendeeEmail=?"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v20, v58, v59

    const/16 v59, 0x1

    aput-object v5, v58, v59

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    move-object/from16 v2, v57

    move-object/from16 v3, v58

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3087
    .end local v9    # "cv":Landroid/content/ContentValues;
    :cond_30
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_f

    .line 3071
    :cond_31
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v55

    if-nez v55, :cond_2e

    .line 3072
    move-object v6, v5

    goto/16 :goto_10

    .line 3091
    .end local v5    # "attendeeEmail":Ljava/lang/String;
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v20    # "eventId":Ljava/lang/String;
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    .end local v42    # "relationship":Ljava/lang/Integer;
    :cond_32
    if-eqz v24, :cond_33

    .line 3092
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 3096
    :cond_33
    const-string v55, "_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 3108
    .local v20, "eventId":J
    const/4 v7, 0x2

    .line 3110
    .local v7, "busyStatus":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getAvailabilityStatus(Landroid/content/Context;J)I

    move-result v7

    .line 3114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_37

    .line 3115
    const/16 v55, 0x10d

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3116
    const/16 v56, 0x118

    if-eqz v24, :cond_36

    const-string v55, "1"

    :goto_11
    move-object/from16 v0, p3

    move/from16 v1, v56

    move-object/from16 v2, v55

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3127
    :goto_12
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-gez v55, :cond_34

    if-nez v27, :cond_35

    :cond_34
    if-eqz v37, :cond_35

    .line 3129
    const/16 v55, 0x11a

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3258
    .end local v13    # "earliestReminder":I
    .end local v37    # "organizerName":Ljava/lang/String;
    .end local v43    # "rrule":Ljava/lang/String;
    :cond_35
    :goto_13
    return-void

    .line 3116
    .restart local v13    # "earliestReminder":I
    .restart local v37    # "organizerName":Ljava/lang/String;
    .restart local v43    # "rrule":Ljava/lang/String;
    :cond_36
    const-string v55, "0"

    goto :goto_11

    .line 3119
    :cond_37
    const-string v55, "selfAttendeeStatus"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Integer;->intValue()I

    move-result v44

    .line 3120
    .local v44, "selfAttendeeStatus":I
    invoke-static/range {v44 .. v44}, Lcom/android/exchange/utility/CalendarUtilities;->busyStatusFromSelfAttendeeStatus(I)I

    move-result v7

    .line 3121
    const/16 v55, 0x10d

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3123
    const/16 v55, 0x118

    const-string v56, "3"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_12

    .line 3136
    .end local v7    # "busyStatus":I
    .end local v13    # "earliestReminder":I
    .end local v20    # "eventId":J
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v36    # "organizerEmail":Ljava/lang/String;
    .end local v37    # "organizerName":Ljava/lang/String;
    .end local v43    # "rrule":Ljava/lang/String;
    .end local v44    # "selfAttendeeStatus":I
    .end local v48    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    :cond_38
    invoke-virtual/range {p1 .. p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v48

    .line 3137
    .restart local v48    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    const/16 v36, 0x0

    .line 3139
    .restart local v36    # "organizerEmail":Ljava/lang/String;
    const/16 v23, -0x1

    .line 3141
    .local v23, "exEarliestReminder":I
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    .restart local v25    # "i$":Ljava/util/Iterator;
    :cond_39
    :goto_14
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v55

    if-eqz v55, :cond_42

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Landroid/content/Entity$NamedContentValues;

    .line 3142
    .restart local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    move-object/from16 v34, v0

    .line 3143
    .restart local v34    # "ncvUri":Landroid/net/Uri;
    move-object/from16 v0, v33

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v35, v0

    .line 3144
    .restart local v35    # "ncvValues":Landroid/content/ContentValues;
    sget-object v55, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_40

    .line 3145
    const-string v55, "attendeeRelationship"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v42

    .line 3149
    .restart local v42    # "relationship":Ljava/lang/Integer;
    if-eqz v42, :cond_3a

    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_3a

    .line 3152
    invoke-virtual/range {v42 .. v42}, Ljava/lang/Integer;->intValue()I

    move-result v55

    const/16 v56, 0x2

    move/from16 v0, v55

    move/from16 v1, v56

    if-ne v0, v1, :cond_3a

    .line 3153
    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3155
    goto :goto_14

    .line 3159
    :cond_3a
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x402c000000000000L    # 14.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_39

    .line 3160
    if-nez v24, :cond_3b

    .line 3161
    const/16 v55, 0x107

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3162
    const/16 v24, 0x1

    .line 3164
    :cond_3b
    const/16 v55, 0x108

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3165
    const-string v55, "attendeeEmail"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3166
    .restart local v5    # "attendeeEmail":Ljava/lang/String;
    const-string v55, "attendeeName"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3167
    .restart local v6    # "attendeeName":Ljava/lang/String;
    const-string v55, "_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 3168
    .local v20, "eventId":Ljava/lang/String;
    if-nez v6, :cond_3f

    .line 3169
    move-object v6, v5

    .line 3176
    :cond_3c
    :goto_15
    const/16 v55, 0x10a

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3177
    const/16 v55, 0x109

    move-object/from16 v0, p3

    move/from16 v1, v55

    invoke-virtual {v0, v1, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3178
    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_3d

    .line 3179
    const/16 v55, 0x12a

    const-string v56, "1"

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3181
    :cond_3d
    const-string v55, "1"

    move-object/from16 v0, v55

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_3e

    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x4028000000000000L    # 12.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_3e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_3e

    .line 3184
    const/16 v55, 0x129

    const/16 v56, 0x0

    invoke-static/range {v56 .. v56}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3186
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 3187
    .restart local v9    # "cv":Landroid/content/ContentValues;
    const-string v55, "attendeeStatus"

    const/16 v56, 0x0

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v55, v0

    invoke-virtual/range {v55 .. v55}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v55

    sget-object v56, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    const-string v57, "event_id=? AND attendeeEmail=?"

    const/16 v58, 0x2

    move/from16 v0, v58

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v58, v0

    const/16 v59, 0x0

    aput-object v20, v58, v59

    const/16 v59, 0x1

    aput-object v5, v58, v59

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    move-object/from16 v2, v57

    move-object/from16 v3, v58

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3196
    .end local v9    # "cv":Landroid/content/ContentValues;
    :cond_3e
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_14

    .line 3173
    :cond_3f
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v55

    if-nez v55, :cond_3c

    .line 3174
    move-object v6, v5

    goto/16 :goto_15

    .line 3200
    .end local v5    # "attendeeEmail":Ljava/lang/String;
    .end local v6    # "attendeeName":Ljava/lang/String;
    .end local v20    # "eventId":Ljava/lang/String;
    .end local v42    # "relationship":Ljava/lang/Integer;
    :cond_40
    sget-object v55, Landroid/provider/CalendarContract$Reminders;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v34

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v55

    if-eqz v55, :cond_39

    .line 3201
    const-string v55, "minutes"

    move-object/from16 v0, v35

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v32

    .line 3202
    .restart local v32    # "mins":Ljava/lang/Integer;
    if-eqz v32, :cond_39

    .line 3204
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v55

    if-gez v55, :cond_41

    .line 3205
    const/16 v55, 0x1e

    invoke-static/range {v55 .. v55}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    .line 3209
    :cond_41
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v55

    move/from16 v0, v55

    move/from16 v1, v23

    if-le v0, v1, :cond_39

    .line 3210
    invoke-virtual/range {v32 .. v32}, Ljava/lang/Integer;->intValue()I

    move-result v23

    goto/16 :goto_14

    .line 3216
    .end local v32    # "mins":Ljava/lang/Integer;
    .end local v33    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v34    # "ncvUri":Landroid/net/Uri;
    .end local v35    # "ncvValues":Landroid/content/ContentValues;
    :cond_42
    if-eqz v24, :cond_43

    invoke-virtual/range {v53 .. v53}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v56

    const-wide/high16 v58, 0x402c000000000000L    # 14.0

    cmpl-double v55, v56, v58

    if-ltz v55, :cond_43

    .line 3217
    invoke-virtual/range {p3 .. p3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 3221
    :cond_43
    if-ltz v23, :cond_44

    .line 3222
    const/16 v55, 0x124

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3225
    :cond_44
    if-nez v36, :cond_45

    const-string v55, "organizer"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_45

    .line 3227
    const-string v55, "organizer"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 3230
    :cond_45
    const-string v55, "_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 3232
    .local v20, "eventId":J
    const/4 v7, 0x2

    .line 3233
    .restart local v7    # "busyStatus":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getAvailabilityStatus(Landroid/content/Context;J)I

    move-result v7

    .line 3235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v55, v0

    move-object/from16 v0, v55

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v55

    if-eqz v55, :cond_46

    .line 3236
    const/16 v55, 0x10d

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_13

    .line 3243
    :cond_46
    const-string v55, "selfAttendeeStatus"

    move-object/from16 v0, v18

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v55

    invoke-virtual/range {v55 .. v55}, Ljava/lang/Integer;->intValue()I

    move-result v44

    .line 3245
    .restart local v44    # "selfAttendeeStatus":I
    invoke-static/range {v44 .. v44}, Lcom/android/exchange/utility/CalendarUtilities;->busyStatusFromSelfAttendeeStatus(I)I

    move-result v7

    .line 3247
    const/16 v55, 0x10d

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p3

    move/from16 v1, v55

    move-object/from16 v2, v56

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_13

    .line 2862
    .end local v7    # "busyStatus":I
    .end local v11    # "desc":Ljava/lang/String;
    .end local v16    # "endTime":J
    .end local v20    # "eventId":J
    .end local v23    # "exEarliestReminder":I
    .end local v25    # "i$":Ljava/util/Iterator;
    .end local v36    # "organizerEmail":Ljava/lang/String;
    .end local v44    # "selfAttendeeStatus":I
    .end local v48    # "subValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Entity$NamedContentValues;>;"
    .restart local v12    # "duration":Lcom/android/emailcommon/utility/calendar/Duration;
    .restart local v14    # "durationMillis":J
    :catch_0
    move-exception v55

    goto/16 :goto_d
.end method

.method private sendMeetingResponse(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/ContentValues;)V
    .locals 24
    .param p1, "entityValues"    # Landroid/content/ContentValues;
    .param p2, "userResponse"    # Ljava/lang/String;
    .param p3, "collectionId"    # Ljava/lang/String;
    .param p4, "isException"    # Z
    .param p5, "exEntityValues"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3273
    const-string v20, "_sync_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 3275
    .local v14, "serverId":Ljava/lang/String;
    new-instance v13, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v13}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 3277
    .local v13, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v20, 0x207

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    const/16 v21, 0x209

    invoke-virtual/range {v20 .. v21}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3281
    const/16 v20, 0x20c

    move/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v13, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3285
    const/16 v20, 0x206

    move/from16 v0, v20

    move-object/from16 v1, p3

    invoke-virtual {v13, v0, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3287
    const/16 v20, 0x208

    move/from16 v0, v20

    invoke-virtual {v13, v0, v14}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3289
    if-eqz p4, :cond_1

    .line 3291
    const-string v20, "allDay"

    move-object/from16 v0, p5

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/android/exchange/utility/CalendarUtilities;->getIntegerValueAsBoolean(Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v4

    .line 3294
    .local v4, "allDay":Z
    const-string v20, "dtstart"

    move-object/from16 v0, p5

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 3296
    .local v16, "startTime":J
    if-eqz v4, :cond_0

    .line 3298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mLocalTimeZone:Ljava/util/TimeZone;

    move-object/from16 v19, v0

    .line 3300
    .local v19, "tz":Ljava/util/TimeZone;
    move-wide/from16 v0, v16

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v16

    .line 3302
    .end local v19    # "tz":Ljava/util/TimeZone;
    :cond_0
    invoke-static/range {v16 .. v17}, Lcom/android/exchange/utility/CalendarUtilities;->millisToInstanceId(J)Ljava/lang/String;

    move-result-object v15

    .line 3304
    .local v15, "startDate":Ljava/lang/String;
    const/16 v20, 0x20e

    move/from16 v0, v20

    invoke-virtual {v13, v0, v15}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3308
    .end local v4    # "allDay":Z
    .end local v15    # "startDate":Ljava/lang/String;
    .end local v16    # "startTime":J
    :cond_1
    const-string v20, "MeetingResponse"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Sending Meeting Response: serverId:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ","

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "userResponse:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",CollectionId:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3311
    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 3313
    const/4 v12, 0x0

    .line 3315
    .local v12, "res":Lcom/android/exchange/EasResponse;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    const-string v21, "MeetingResponse"

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v22

    invoke-virtual/range {v20 .. v22}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v12

    .line 3317
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v18

    .line 3319
    .local v18, "status":I
    sget-boolean v20, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v20, :cond_2

    .line 3320
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "sendMeetingResponse(): MeetingResponse http response code:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3323
    :cond_2
    const/16 v20, 0xc8

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 3324
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v10

    .line 3325
    .local v10, "len":I
    if-eqz v10, :cond_3

    .line 3326
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 3327
    .local v7, "is":Ljava/io/InputStream;
    new-instance v11, Lcom/android/exchange/adapter/MeetingResponseParser;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v11, v7, v0}, Lcom/android/exchange/adapter/MeetingResponseParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 3328
    .local v11, "parser":Lcom/android/exchange/adapter/MeetingResponseParser;
    invoke-virtual {v11}, Lcom/android/exchange/adapter/MeetingResponseParser;->parse()Z

    .line 3329
    invoke-virtual {v11}, Lcom/android/exchange/adapter/MeetingResponseParser;->getStatus()I

    move-result v18

    .line 3330
    const/16 v20, 0x1

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 3331
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 3332
    .local v5, "cv":Landroid/content/ContentValues;
    const-string v20, "dirty"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3333
    const-string v20, "sync_data8"

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3335
    if-eqz p4, :cond_5

    .line 3336
    const-string v20, "_id"

    move-object/from16 v0, p5

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 3340
    .local v8, "eventId":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v20, v0

    sget-object v21, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v21

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "com.android.exchange"

    invoke-static/range {v21 .. v23}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v5, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3356
    .end local v5    # "cv":Landroid/content/ContentValues;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v8    # "eventId":J
    .end local v11    # "parser":Lcom/android/exchange/adapter/MeetingResponseParser;
    :cond_3
    if-eqz v12, :cond_4

    .line 3357
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    .line 3359
    :cond_4
    return-void

    .line 3338
    .restart local v5    # "cv":Landroid/content/ContentValues;
    .restart local v7    # "is":Ljava/io/InputStream;
    .restart local v11    # "parser":Lcom/android/exchange/adapter/MeetingResponseParser;
    :cond_5
    :try_start_1
    const-string v20, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .restart local v8    # "eventId":J
    goto :goto_0

    .line 3347
    .end local v5    # "cv":Landroid/content/ContentValues;
    .end local v7    # "is":Ljava/io/InputStream;
    .end local v8    # "eventId":J
    .end local v10    # "len":I
    .end local v11    # "parser":Lcom/android/exchange/adapter/MeetingResponseParser;
    :cond_6
    invoke-static/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 3348
    new-instance v20, Lcom/android/exchange/EasAuthenticationException;

    invoke-direct/range {v20 .. v20}, Lcom/android/exchange/EasAuthenticationException;-><init>()V

    throw v20
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3353
    .end local v18    # "status":I
    :catch_0
    move-exception v6

    .line 3354
    .local v6, "e":Ljava/io/IOException;
    :try_start_2
    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3356
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v20

    if-eqz v12, :cond_7

    .line 3357
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    :cond_7
    throw v20

    .line 3350
    .restart local v18    # "status":I
    :cond_8
    const/16 v20, 0x1

    :try_start_3
    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Meeting response request failed, code: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3351
    new-instance v20, Ljava/io/IOException;

    invoke-direct/range {v20 .. v20}, Ljava/io/IOException;-><init>()V

    throw v20
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private truncateEventDescription(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "desc"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 4282
    move-object v1, p1

    .line 4284
    .local v1, "truncatedDesc":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const v3, 0x8400

    if-le v2, v3, :cond_0

    .line 4289
    const/4 v2, 0x0

    const v3, 0x83ff

    :try_start_0
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 4290
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "truncateEventDescription: desc.length > 50KB msg clipped"

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4296
    :cond_0
    :goto_0
    return-object v1

    .line 4291
    :catch_0
    move-exception v0

    .line 4292
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "EasCalendarSyncAdapter"

    aput-object v3, v2, v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception during event description truncation: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected applyAndCopyResults1(Ljava/lang/String;Ljava/util/ArrayList;[Landroid/content/ContentProviderResult;I)V
    .locals 4
    .param p1, "authority"    # Ljava/lang/String;
    .param p3, "result"    # [Landroid/content/ContentProviderResult;
    .param p4, "offset"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;",
            ">;[",
            "Landroid/content/ContentProviderResult;",
            "I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 4459
    .local p2, "mini":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4467
    :goto_0
    return-void

    .line 4461
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, p2, p4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;I)[Landroid/content/ContentProviderResult;

    move-result-object v0

    .line 4463
    .local v0, "miniResult":[Landroid/content/ContentProviderResult;
    const/4 v1, 0x0

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v1, p3, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4464
    .end local v0    # "miniResult":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 424
    return-void
.end method

.method public deleteCalendarEventsOfSubFolder(Ljava/lang/String;)V
    .locals 6
    .param p1, "mailboxServerId"    # Ljava/lang/String;

    .prologue
    .line 4509
    const/4 v0, 0x0

    .line 4510
    .local v0, "c":Landroid/database/Cursor;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4512
    .local v1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getSubFolderCalendarEventsCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4513
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4515
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Deleting SubContact SOURCE_ID "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4516
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAsSyncAdapterEvents:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4518
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 4521
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4522
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4525
    :cond_2
    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->executeSubFolderDelete(Ljava/util/ArrayList;)V

    .line 4526
    return-void

    .line 4521
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 4522
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method public deleteEvent(Ljava/lang/String;)V
    .locals 6
    .param p1, "serverId"    # Ljava/lang/String;

    .prologue
    .line 4485
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getServerIdDeleteCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 4487
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4488
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete ServerID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4490
    :cond_0
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Deleting Event with ServerID: "

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4491
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v5, "com.android.exchange"

    invoke-static {v3, v4, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4496
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 4501
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4502
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 4505
    :cond_2
    :goto_0
    return-void

    .line 4498
    :catch_0
    move-exception v1

    .line 4499
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4501
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4502
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4501
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 4502
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method public executeSubFolderDelete(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4433
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v2}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 4435
    const/4 v1, 0x0

    .line 4436
    .local v1, "result":[Landroid/content/ContentProviderResult;
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 4437
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    const-string v4, "Executing "

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    const-string v6, " CPO\'s"

    invoke-virtual {v2, v4, v5, v6}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 4438
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const-string v4, "com.android.calendar"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4449
    :cond_0
    :goto_0
    :try_start_1
    monitor-exit v3

    .line 4450
    return-void

    .line 4440
    :catch_0
    move-exception v0

    .line 4442
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "EasCalendarSyncAdapter"

    const-string v4, "problem while deleting contact subfolder data"

    invoke-static {v2, v4, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 4449
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 4444
    :catch_1
    move-exception v0

    .line 4446
    .local v0, "e":Landroid/content/OperationApplicationException;
    :try_start_2
    const-string v2, "EasCalendarSyncAdapter"

    const-string v4, "problem while deleting contact subfolder data"

    invoke-static {v2, v4, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 419
    const-string v0, "Calendar"

    return-object v0
.end method

.method public getMessageId(Landroid/content/Context;Ljava/lang/String;)J
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "conversationId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 3381
    const-wide/16 v8, -0x1

    .line 3382
    .local v8, "messageId":J
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 3383
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 3384
    .local v6, "c":Landroid/database/Cursor;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v4

    .line 3388
    .local v2, "PROJECTION":[Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "meetingInfo LIKE \'%"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "%\' AND "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "mailboxType"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3389
    .local v3, "where":Ljava/lang/String;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3391
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3392
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 3400
    .end local v8    # "messageId":J
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3401
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v3    # "where":Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v8

    .line 3400
    .restart local v3    # "where":Ljava/lang/String;
    .restart local v8    # "messageId":J
    :cond_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3401
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 3396
    .end local v3    # "where":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 3397
    .local v7, "ie":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3400
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3401
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 3400
    .end local v7    # "ie":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_2

    .line 3401
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method protected getParser(Lcom/android/exchange/adapter/Parser;)Lcom/android/exchange/adapter/AbstractSyncParser;
    .locals 2
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4277
    new-instance v0, Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;-><init>(Lcom/android/exchange/adapter/CalendarSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/CalendarSyncAdapter;Z)V

    return-object v0
.end method

.method public getSyncKey()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 643
    sget-object v5, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sSyncKeyLock:Ljava/lang/Object;

    monitor-enter v5

    .line 646
    :try_start_0
    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v6, 0x52

    if-ne v4, v6, :cond_1

    .line 647
    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    :goto_0
    monitor-exit v5

    .line 694
    :goto_1
    return-object v3

    .line 647
    :cond_0
    const-string v3, "0"

    goto :goto_0

    .line 655
    :cond_1
    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v4, v4, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v6}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 658
    .local v0, "client":Landroid/content/ContentProviderClient;
    if-nez v0, :cond_2

    .line 660
    const-string v3, "0"

    monitor-exit v5

    goto :goto_1

    .line 697
    .end local v0    # "client":Landroid/content/ContentProviderClient;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 669
    .restart local v0    # "client":Landroid/content/ContentProviderClient;
    :cond_2
    :try_start_1
    sget-object v4, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v7, "com.android.exchange"

    invoke-static {v4, v6, v7}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccountManagerAccount:Landroid/accounts/Account;

    invoke-static {v0, v4, v6}, Landroid/provider/SyncStateContract$Helpers;->get(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;)[B

    move-result-object v1

    .line 674
    .local v1, "data":[B
    if-eqz v1, :cond_3

    array-length v4, v1

    if-nez v4, :cond_7

    .line 675
    :cond_3
    const-string v3, "0"

    .line 676
    .local v3, "syncKey":Ljava/lang/String;
    const-string v4, "EasCalendarSyncAdapter"

    const-string v6, "getSyncKey() is null"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    if-eqz v4, :cond_5

    .line 678
    const-string v4, "EasCalendarSyncAdapter"

    const-string v6, "Replace the SyncKey with Mailbox\'s sync key"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 693
    if-eqz v0, :cond_4

    .line 694
    :try_start_2
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_4
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 683
    :cond_5
    :try_start_3
    const-string v4, "0"

    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 684
    const-string v3, "0"
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 693
    .end local v3    # "syncKey":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 694
    :try_start_4
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_6
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 686
    :cond_7
    :try_start_5
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    .line 687
    .restart local v3    # "syncKey":Ljava/lang/String;
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "SyncKey retrieved as "

    aput-object v7, v4, v6

    const/4 v6, 0x1

    aput-object v3, v4, v6

    const/4 v6, 0x2

    const-string v7, " from CalendarProvider"

    aput-object v7, v4, v6

    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 693
    if-eqz v0, :cond_8

    .line 694
    :try_start_6
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_8
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 690
    .end local v1    # "data":[B
    .end local v3    # "syncKey":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 691
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_7
    new-instance v4, Ljava/io/IOException;

    const-string v6, "Can\'t get SyncKey from CalendarProvider"

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 693
    .end local v2    # "e":Landroid/os/RemoteException;
    :catchall_1
    move-exception v4

    if-eqz v0, :cond_9

    .line 694
    :try_start_8
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    :cond_9
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0
.end method

.method init()V
    .locals 9

    .prologue
    .line 386
    sget-object v7, Lcom/android/exchange/adapter/CalendarSyncAdapter;->LOCK:Ljava/lang/Object;

    monitor-enter v7

    .line 387
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "account_name=? AND account_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "com.android.exchange"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 392
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 393
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :goto_0
    return-void

    .line 395
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    .line 407
    :goto_1
    iget-wide v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    .line 408
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdArgument:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 412
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 414
    monitor-exit v7

    goto :goto_0

    .end local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 398
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v1, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->createCalendar(Lcom/android/exchange/EasSyncService;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarId:J

    .line 404
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    const-string v1, "0"

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 412
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public isSyncable()Z
    .locals 2

    .prologue
    .line 566
    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccountManagerAccount:Landroid/accounts/Account;

    const-string v1, "com.android.calendar"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 10
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 576
    const/4 v1, 0x0

    .line 579
    .local v1, "p":Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v7, 0x52

    if-ne v6, v7, :cond_0

    .line 580
    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 581
    .local v3, "tempMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/4 v7, 0x6

    invoke-static {v6, v8, v9, v7}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v4

    .line 583
    .local v4, "trashServerId":Ljava/lang/String;
    if-eqz v4, :cond_0

    iget-object v6, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentServerId:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 584
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Calendar SubFolder was delete before getting sync response, Mailbox ID "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-virtual {p0, v6}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 602
    .end local v3    # "tempMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v4    # "trashServerId":Ljava/lang/String;
    :goto_0
    return v5

    .line 590
    :cond_0
    :try_start_0
    new-instance v2, Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;

    invoke-direct {v2, p0, p1, p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;-><init>(Lcom/android/exchange/adapter/CalendarSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/CalendarSyncAdapter;)V
    :try_end_0
    .catch Lcom/android/exchange/adapter/Parser$EofException; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    .end local v1    # "p":Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    .local v2, "p":Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    iget-object v5, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mNewEventServerIdList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 602
    invoke-virtual {v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;->parse()Z

    move-result v5

    move-object v1, v2

    .end local v2    # "p":Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    .restart local v1    # "p":Lcom/android/exchange/adapter/CalendarSyncAdapter$EasCalendarSyncParser;
    goto :goto_0

    .line 591
    :catch_0
    move-exception v0

    .line 593
    .local v0, "e":Lcom/android/exchange/adapter/Parser$EofException;
    iget-object v6, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v6, v6, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide v8, 0x4028333333333333L    # 12.1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_1

    .line 594
    invoke-virtual {p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->setIntervalPing()V

    goto :goto_0

    .line 597
    :cond_1
    throw v0
.end method

.method protected safeExecute(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 33
    .param p1, "authority"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 4314
    .local p2, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const-string v29, "CalendarSyncAdapter >> Try to execute "

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v30

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, " CPO\'s for "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v28 .. v31}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 4315
    const/16 v26, 0x0

    .line 4318
    .local v26, "result":[Landroid/content/ContentProviderResult;
    const/16 v28, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, v28

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;I)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v28

    .line 4426
    :goto_0
    return-object v28

    .line 4319
    :catch_0
    move-exception v7

    .line 4321
    .local v7, "e":Landroid/os/TransactionTooLargeException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const-string v31, "CalendarSyncAdapter >> Transaction too large; spliting!"

    aput-object v31, v29, v30

    invoke-virtual/range {v28 .. v29}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4322
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 4324
    .local v17, "mini":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    new-array v0, v0, [Landroid/content/ContentProviderResult;

    move-object/from16 v26, v0

    .line 4325
    const/4 v5, 0x0

    .line 4326
    .local v5, "count":I
    const/16 v22, 0x0

    .line 4327
    .local v22, "offset":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_8

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    .line 4328
    .local v23, "op":Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;
    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;->mSeparator:Z

    move/from16 v28, v0

    if-eqz v28, :cond_7

    .line 4330
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const-string v29, "CalendarSyncAdapter >> Try mini-batch of "

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v30

    const-string v31, " CPO\'s"

    invoke-virtual/range {v28 .. v31}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 4331
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v26

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyAndCopyResults(Ljava/lang/String;Ljava/util/ArrayList;[Landroid/content/ContentProviderResult;I)V

    .line 4332
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Landroid/os/TransactionTooLargeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3

    .line 4335
    add-int/lit8 v22, v5, 0x1

    .line 4414
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 4415
    goto :goto_1

    .line 4336
    :catch_1
    move-exception v8

    .line 4338
    .local v8, "e1":Landroid/os/TransactionTooLargeException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v29

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    const-string v31, "CalendarSyncAdapter >> Still Transaction too large; spliting using exception seperator!"

    aput-object v31, v29, v30

    invoke-virtual/range {v28 .. v29}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4339
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 4340
    .local v10, "event":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 4341
    .local v11, "exception":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    const/4 v12, 0x0

    .line 4343
    .local v12, "flagExceptionEnd":Z
    const/16 v16, 0x0

    .line 4344
    .local v16, "isnOPSBEStartSet":Z
    const/16 v21, 0x0

    .line 4346
    .local v21, "nOPSBeforeExceptionStart":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    if-ge v13, v0, :cond_4

    .line 4347
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;->mExceptionStartSeparator:Z

    move/from16 v28, v0

    if-eqz v28, :cond_3

    .line 4349
    if-nez v16, :cond_0

    .line 4350
    move/from16 v21, v13

    .line 4351
    const/16 v16, 0x1

    .line 4353
    :cond_0
    :goto_4
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    if-ge v13, v0, :cond_1

    .line 4354
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4355
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;->mExceptionEndSeparator:Z

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 4356
    const/4 v12, 0x1

    .line 4360
    :cond_1
    if-eqz v12, :cond_3

    .line 4361
    const/4 v12, 0x0

    .line 4346
    :goto_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 4353
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 4365
    :cond_3
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 4367
    :cond_4
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    .line 4370
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    new-array v0, v0, [Landroid/content/ContentProviderResult;

    move-object/from16 v27, v0

    .line 4372
    .local v27, "result1":[Landroid/content/ContentProviderResult;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const-string v29, "CalendarSyncAdapter >> Try mini-batch [event] of "

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v30

    const-string v31, " CPO\'s"

    invoke-virtual/range {v28 .. v31}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 4373
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    move/from16 v3, v22

    invoke-virtual {v0, v1, v10, v2, v3}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyAndCopyResults1(Ljava/lang/String;Ljava/util/ArrayList;[Landroid/content/ContentProviderResult;I)V

    .line 4374
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 4378
    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-object/from16 v2, v26

    move/from16 v3, v22

    move/from16 v4, v21

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4379
    const/4 v6, 0x0

    .line 4380
    .local v6, "count1":I
    add-int v19, v22, v21

    .line 4381
    .local v19, "miniOffset":I
    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v28, v0

    sub-int v25, v28, v21

    .line 4382
    .local v25, "remainingEventOffset":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 4384
    .local v18, "miniException":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_6

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    .line 4385
    .local v24, "op1":Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;
    move-object/from16 v0, v24

    iget-boolean v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;->mExceptionEndSeparator:Z

    move/from16 v28, v0

    if-eqz v28, :cond_5

    .line 4386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v28, v0

    const-string v29, "CalendarSyncAdapter >> Try mini-batch [miniException] of "

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v30

    const-string v31, " CPO\'s"

    invoke-virtual/range {v28 .. v31}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 4387
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v26

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyAndCopyResults(Ljava/lang/String;Ljava/util/ArrayList;[Landroid/content/ContentProviderResult;I)V

    .line 4388
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 4391
    add-int v28, v22, v21

    add-int v28, v28, v6

    add-int/lit8 v19, v28, 0x1

    .line 4395
    :goto_7
    add-int/lit8 v6, v6, 0x1

    .line 4396
    goto :goto_6

    .line 4393
    :cond_5
    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Landroid/os/TransactionTooLargeException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_7

    .line 4402
    .end local v6    # "count1":I
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v18    # "miniException":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .end local v19    # "miniOffset":I
    .end local v24    # "op1":Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;
    .end local v25    # "remainingEventOffset":I
    :catch_2
    move-exception v9

    .line 4405
    .local v9, "e2":Landroid/os/TransactionTooLargeException;
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteCommittedEvents()V

    .line 4406
    new-instance v28, Ljava/lang/RuntimeException;

    const-string v29, "Can\'t send transaction; sync stopped."

    invoke-direct/range {v28 .. v29}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 4397
    .end local v9    # "e2":Landroid/os/TransactionTooLargeException;
    .restart local v6    # "count1":I
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v18    # "miniException":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .restart local v19    # "miniOffset":I
    .restart local v25    # "remainingEventOffset":I
    :cond_6
    :try_start_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 4399
    move-object/from16 v0, v27

    move/from16 v1, v21

    move-object/from16 v2, v26

    move/from16 v3, v19

    move/from16 v4, v25

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3
    .catch Landroid/os/TransactionTooLargeException; {:try_start_3 .. :try_end_3} :catch_2

    .line 4401
    add-int v28, v19, v25

    add-int/lit8 v22, v28, 0x1

    goto/16 :goto_2

    .line 4408
    .end local v6    # "count1":I
    .end local v8    # "e1":Landroid/os/TransactionTooLargeException;
    .end local v10    # "event":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .end local v11    # "exception":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .end local v12    # "flagExceptionEnd":Z
    .end local v13    # "i":I
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "isnOPSBEStartSet":Z
    .end local v18    # "miniException":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .end local v19    # "miniOffset":I
    .end local v21    # "nOPSBeforeExceptionStart":I
    .end local v25    # "remainingEventOffset":I
    .end local v27    # "result1":[Landroid/content/ContentProviderResult;
    :catch_3
    move-exception v8

    .line 4409
    .local v8, "e1":Landroid/os/RemoteException;
    throw v8

    .line 4412
    .end local v8    # "e1":Landroid/os/RemoteException;
    :cond_7
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 4417
    .end local v23    # "op":Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;
    :cond_8
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 4418
    .local v20, "miniSize":I
    if-lez v20, :cond_a

    const/16 v28, 0x1

    move/from16 v0, v20

    move/from16 v1, v28

    if-ne v0, v1, :cond_9

    const/16 v28, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;->mSeparator:Z

    move/from16 v28, v0

    if-nez v28, :cond_a

    .line 4419
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    move-object/from16 v3, v26

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->applyAndCopyResults(Ljava/lang/String;Ljava/util/ArrayList;[Landroid/content/ContentProviderResult;I)V

    .end local v5    # "count":I
    .end local v7    # "e":Landroid/os/TransactionTooLargeException;
    .end local v17    # "mini":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/adapter/AbstractSyncAdapter$Operation;>;"
    .end local v20    # "miniSize":I
    .end local v22    # "offset":I
    :cond_a
    :goto_8
    move-object/from16 v28, v26

    .line 4426
    goto/16 :goto_0

    .line 4421
    :catch_4
    move-exception v7

    .line 4422
    .local v7, "e":Landroid/os/RemoteException;
    throw v7

    .line 4423
    .end local v7    # "e":Landroid/os/RemoteException;
    :catch_5
    move-exception v28

    goto :goto_8
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 90
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3409
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v4, v5, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 3411
    .local v4, "cr":Landroid/content/ContentResolver;
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v5

    const-string v6, "0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3412
    const/4 v5, 0x0

    .line 4272
    :goto_0
    return v5

    .line 3419
    :cond_0
    :try_start_0
    new-instance v73, Ljava/util/ArrayList;

    invoke-direct/range {v73 .. v73}, Ljava/util/ArrayList;-><init>()V

    .line 3420
    .local v73, "orphanedExceptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/16 v48, 0x0

    .line 3421
    .local v48, "c":Landroid/database/Cursor;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    .line 3422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v6, 0x41

    if-ne v5, v6, :cond_2

    .line 3425
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ORIGINAL_EVENT_PROJECTION:[Ljava/lang/String;

    const-string v7, "dirty=1 AND original_id NOTNULL AND calendar_id=? AND (sync_data5 ISNULL OR sync_data5 =?)"

    const/4 v11, 0x2

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v48

    .line 3437
    :goto_1
    if-eqz v48, :cond_4

    .line 3439
    :try_start_1
    new-instance v52, Landroid/content/ContentValues;

    invoke-direct/range {v52 .. v52}, Landroid/content/ContentValues;-><init>()V

    .line 3444
    .local v52, "cv":Landroid/content/ContentValues;
    const-string v5, "sync_data8"

    const-string v6, "1"

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3445
    :cond_1
    :goto_2
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3448
    const/4 v5, 0x0

    move-object/from16 v0, v48

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v76

    .line 3454
    .local v76, "parentId":J
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const-string v6, "_id=? AND original_sync_id ISNULL AND calendar_id=?"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {v76 .. v77}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v11, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v15, v11, v14

    move-object/from16 v0, v52

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v50

    .line 3462
    .local v50, "cnt":I
    if-nez v50, :cond_1

    .line 3463
    const/4 v5, 0x1

    move-object/from16 v0, v48

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v73

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 3467
    .end local v50    # "cnt":I
    .end local v52    # "cv":Landroid/content/ContentValues;
    .end local v76    # "parentId":J
    :catchall_0
    move-exception v5

    :try_start_2
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    throw v5
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 4268
    .end local v48    # "c":Landroid/database/Cursor;
    .end local v73    # "orphanedExceptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :catch_0
    move-exception v53

    .line 4269
    .local v53, "e":Landroid/os/RemoteException;
    const-string v5, "EasCalendarSyncAdapter"

    const-string v6, "Could not read dirty events."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4272
    .end local v53    # "e":Landroid/os/RemoteException;
    :goto_3
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 3430
    .restart local v48    # "c":Landroid/database/Cursor;
    .restart local v73    # "orphanedExceptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_2
    :try_start_3
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/exchange/adapter/CalendarSyncAdapter;->ORIGINAL_EVENT_PROJECTION:[Ljava/lang/String;

    const-string v7, "dirty=1 AND original_id NOTNULL AND calendar_id=? AND sync_data5 =?"

    const/4 v11, 0x2

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v48

    goto/16 :goto_1

    .line 3467
    .restart local v52    # "cv":Landroid/content/ContentValues;
    :cond_3
    invoke-interface/range {v48 .. v48}, Landroid/database/Cursor;->close()V

    .line 3471
    .end local v52    # "cv":Landroid/content/ContentValues;
    :cond_4
    invoke-virtual/range {v73 .. v73}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    .local v65, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v74

    .line 3472
    .local v74, "orphan":J
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "EasCalendarSyncAdapter"

    aput-object v11, v5, v6

    const/4 v6, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Deleted orphaned exception: "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, v74

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3475
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v74

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v5, v6, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_4

    .line 3479
    .end local v74    # "orphan":J
    :cond_5
    invoke-virtual/range {v73 .. v73}, Ljava/util/ArrayList;->clear()V

    .line 3489
    const/16 v56, 0x0

    .line 3490
    .local v56, "eventIterator":Landroid/content/EntityIterator;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v6, 0x41

    if-ne v5, v6, :cond_8

    .line 3493
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "(dirty=1 OR sync_data8= 1) AND original_id ISNULL AND calendar_id=? AND (sync_data5 ISNULL OR sync_data5 =?)"

    const/4 v11, 0x2

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    invoke-static {v5, v4}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v56

    .line 3504
    :goto_5
    new-instance v49, Landroid/content/ContentValues;

    invoke-direct/range {v49 .. v49}, Landroid/content/ContentValues;-><init>()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 3507
    .local v49, "cidValues":Landroid/content/ContentValues;
    const/16 v63, 0x1

    .line 3508
    .local v63, "first":Z
    :cond_6
    :goto_6
    :try_start_4
    invoke-interface/range {v56 .. v56}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 3509
    invoke-interface/range {v56 .. v56}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Landroid/content/Entity;

    .line 3511
    .local v54, "entity":Landroid/content/Entity;
    invoke-virtual/range {v49 .. v49}, Landroid/content/ContentValues;->clear()V

    .line 3513
    invoke-virtual/range {v54 .. v54}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v55

    .line 3514
    .local v55, "entityValues":Landroid/content/ContentValues;
    const-string v5, "_sync_id"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v82

    .line 3521
    .local v82, "serverId":Ljava/lang/String;
    invoke-virtual/range {v54 .. v54}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_7
    :goto_7
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 3522
    .local v68, "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3523
    move-object/from16 v0, v68

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v69, v0

    .line 3524
    .local v69, "ncvValues":Landroid/content/ContentValues;
    const-string v5, "name"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "upsyncProhibited"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3526
    const-string v5, "1"

    const-string v6, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3529
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    const-string v6, "_id"

    move-object/from16 v0, v55

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_7

    .line 4266
    .end local v54    # "entity":Landroid/content/Entity;
    .end local v55    # "entityValues":Landroid/content/ContentValues;
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    .end local v82    # "serverId":Ljava/lang/String;
    :catchall_1
    move-exception v5

    :try_start_5
    invoke-interface/range {v56 .. v56}, Landroid/content/EntityIterator;->close()V

    throw v5

    .line 3498
    .end local v49    # "cidValues":Landroid/content/ContentValues;
    .end local v63    # "first":Z
    :cond_8
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "(dirty=1 OR sync_data8= 1) AND original_id ISNULL AND calendar_id=? AND sync_data5 =?"

    const/4 v11, 0x2

    new-array v8, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    aput-object v14, v8, v11

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    invoke-static {v5, v4}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-object v56

    goto/16 :goto_5

    .line 3539
    .restart local v49    # "cidValues":Landroid/content/ContentValues;
    .restart local v54    # "entity":Landroid/content/Entity;
    .restart local v55    # "entityValues":Landroid/content/ContentValues;
    .restart local v63    # "first":Z
    .restart local v82    # "serverId":Ljava/lang/String;
    :cond_9
    :try_start_6
    const-string v5, "sync_data2"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 3540
    .local v10, "clientId":Ljava/lang/String;
    if-nez v10, :cond_a

    .line 3541
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 3547
    :cond_a
    const-string v5, "organizer"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v71

    .line 3548
    .local v71, "organizerEmail":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v0, v71

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v81

    .line 3550
    .local v81, "selfOrganizer":Z
    const-string v5, "dtstart"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "duration"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "dtend"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_b
    if-eqz v71, :cond_6

    .line 3556
    if-eqz v63, :cond_c

    .line 3557
    const/16 v5, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3558
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "Sending Calendar changes to the server"

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3559
    const/16 v63, 0x0

    .line 3561
    :cond_c
    const-string v5, "_id"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 3562
    .local v7, "eventId":J
    if-nez v82, :cond_10

    const/16 v5, 0xc7

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    if-le v5, v6, :cond_10

    .line 3564
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "Creating new event with clientId: "

    aput-object v11, v5, v6

    const/4 v6, 0x1

    aput-object v10, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3565
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    .line 3566
    const/4 v5, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0xc

    invoke-virtual {v5, v6, v10}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3570
    const-string v5, "sync_data2"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3573
    const-string v5, "sync_data4"

    const-string v6, "0"

    move-object/from16 v0, v49

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3578
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3673
    :goto_8
    const/16 v5, 0x1d

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3675
    move-object/from16 v0, p0

    move-object/from16 v1, v54

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v10, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sendEvent(Landroid/content/Entity;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 3678
    if-eqz v82, :cond_37

    .line 3685
    const/16 v60, 0x0

    .line 3687
    .local v60, "exIterator":Landroid/content/EntityIterator;
    :try_start_7
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    const/16 v16, 0x0

    const-string v17, "original_sync_id=? AND calendar_id=?"

    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v5, 0x0

    aput-object v82, v18, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mCalendarIdString:Ljava/lang/String;

    aput-object v6, v18, v5

    const/16 v19, 0x0

    move-object v14, v4

    invoke-virtual/range {v14 .. v19}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    invoke-static {v5, v4}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v60

    .line 3694
    const/16 v57, 0x1

    .line 3696
    .local v57, "exFirst":Z
    invoke-interface/range {v60 .. v60}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 3697
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "We have exceptions for this event"

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3699
    :cond_d
    :goto_9
    invoke-interface/range {v60 .. v60}, Landroid/content/EntityIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_35

    .line 3700
    const/16 v62, 0x0

    .line 3701
    .local v62, "exceptionDeleted":Z
    invoke-interface/range {v60 .. v60}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/Entity;

    .line 3702
    .local v16, "exEntity":Landroid/content/Entity;
    if-eqz v57, :cond_e

    .line 3703
    const/16 v5, 0x114

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3704
    const/16 v57, 0x0

    .line 3706
    :cond_e
    const/16 v5, 0x113

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 3707
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v5, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sendEvent(Landroid/content/Entity;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 3708
    invoke-virtual/range {v16 .. v16}, Landroid/content/Entity;->getEntityValues()Landroid/content/ContentValues;

    move-result-object v61

    .line 3711
    .local v61, "exValues":Landroid/content/ContentValues;
    const-string v5, "dirty"

    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getInt(Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_34

    .line 3715
    const-string v5, "_id"

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v58

    .line 3722
    .local v58, "exEventId":J
    const/16 v45, 0x0

    .line 3723
    .local v45, "attendeeString":Ljava/lang/String;
    const-wide/16 v46, -0x1

    .line 3724
    .local v46, "attendeeStringId":J
    const/16 v85, 0x0

    .line 3725
    .local v85, "userAttendeeStatus":Ljava/lang/String;
    const-wide/16 v86, -0x1

    .line 3726
    .local v86, "userAttendeeStatusId":J
    const/16 v64, 0x0

    .line 3727
    .local v64, "hasAttendeeProperty":Z
    const/16 v69, 0x0

    .line 3729
    .restart local v69    # "ncvValues":Landroid/content/ContentValues;
    const-string v5, "sync_data8"

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_19

    const-string v5, "_sync_id"

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_19

    .line 3730
    invoke-virtual/range {v54 .. v54}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_f
    :goto_a
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 3731
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 3732
    move-object/from16 v0, v68

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v69, v0

    .line 3733
    const-string v5, "name"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    .line 3734
    .local v78, "propertyName":Ljava/lang/String;
    const-string v5, "attendees"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 3735
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    if-eqz v5, :cond_f

    .line 3736
    const/16 v64, 0x0

    .line 3737
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 3738
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-wide v46

    goto :goto_a

    .line 3584
    .end local v16    # "exEntity":Landroid/content/Entity;
    .end local v45    # "attendeeString":Ljava/lang/String;
    .end local v46    # "attendeeStringId":J
    .end local v57    # "exFirst":Z
    .end local v58    # "exEventId":J
    .end local v60    # "exIterator":Landroid/content/EntityIterator;
    .end local v61    # "exValues":Landroid/content/ContentValues;
    .end local v62    # "exceptionDeleted":Z
    .end local v64    # "hasAttendeeProperty":Z
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    .end local v78    # "propertyName":Ljava/lang/String;
    .end local v85    # "userAttendeeStatus":Ljava/lang/String;
    .end local v86    # "userAttendeeStatusId":J
    :cond_10
    const/16 v5, 0xc7

    :try_start_8
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    if-le v5, v6, :cond_15

    .line 3588
    const-string v5, "deleted"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_13

    .line 3589
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "Deleting event with serverId: "

    aput-object v11, v5, v6

    const/4 v6, 0x1

    aput-object v82, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3590
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    .line 3591
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0xd

    move-object/from16 v0, v82

    invoke-virtual {v5, v6, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 3592
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mDeletedIdList:Ljava/util/ArrayList;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3594
    const/4 v9, -0x1

    .line 3595
    .local v9, "msgFlag":I
    if-eqz v81, :cond_12

    .line 3596
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Event Deletion by organizer : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "title"

    move-object/from16 v0, v55

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3597
    const/16 v9, 0x20

    .line 3603
    :goto_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getMessageId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v12

    .line 3604
    .local v12, "meessageId":J
    const-wide/16 v14, -0x1

    cmp-long v5, v12, v14

    if-eqz v5, :cond_6

    .line 3605
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Sending decilne/cancel mail: "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "title"

    move-object/from16 v0, v55

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3606
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-static/range {v6 .. v13}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 3609
    .local v23, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v23, :cond_11

    .line 3610
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing cancel or decline meeting invitation  to "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3611
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3616
    :cond_11
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "meetingInfo LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mailboxType"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v89

    .line 3617
    .local v89, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    move-object/from16 v0, v89

    invoke-virtual {v5, v6, v0, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_6

    .line 3600
    .end local v12    # "meessageId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v89    # "where":Ljava/lang/String;
    :cond_12
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Event Deletion by Attendee : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, "title"

    move-object/from16 v0, v55

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3601
    const/16 v9, 0x80

    goto/16 :goto_b

    .line 3623
    .end local v9    # "msgFlag":I
    :cond_13
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Upsync change to event with serverId: "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v82

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3629
    const-string v5, "sync_data4"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v88

    .line 3634
    .local v88, "version":Ljava/lang/String;
    if-nez v88, :cond_14

    .line 3635
    const-string v88, "0"

    .line 3650
    :goto_c
    const-string v5, "sync_data4"

    move-object/from16 v0, v49

    move-object/from16 v1, v88

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3655
    const-string v5, "sync_data4"

    move-object/from16 v0, v55

    move-object/from16 v1, v88

    invoke-virtual {v0, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3660
    sget-object v5, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3666
    const/16 v5, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0xd

    move-object/from16 v0, v82

    invoke-virtual {v5, v6, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 3667
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_8

    .line 3639
    :cond_14
    :try_start_9
    invoke-static/range {v88 .. v88}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v88

    goto :goto_c

    .line 3640
    :catch_1
    move-exception v53

    .line 3645
    .local v53, "e":Ljava/lang/Exception;
    :try_start_a
    const-string v88, "0"

    goto :goto_c

    .line 3669
    .end local v53    # "e":Ljava/lang/Exception;
    .end local v88    # "version":Ljava/lang/String;
    :cond_15
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "CalendarSyncAdapter Max limit of local changes reached, stop sendLocalChanges; mSyncLocalChangesCount="

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget v14, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mSyncLocalChangesCount:I

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4262
    .end local v7    # "eventId":J
    .end local v10    # "clientId":Ljava/lang/String;
    .end local v54    # "entity":Landroid/content/Entity;
    .end local v55    # "entityValues":Landroid/content/ContentValues;
    .end local v71    # "organizerEmail":Ljava/lang/String;
    .end local v81    # "selfOrganizer":Z
    .end local v82    # "serverId":Ljava/lang/String;
    :cond_16
    if-nez v63, :cond_17

    .line 4263
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 4266
    :cond_17
    :try_start_b
    invoke-interface/range {v56 .. v56}, Landroid/content/EntityIterator;->close()V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_0

    goto/16 :goto_3

    .line 3740
    .restart local v7    # "eventId":J
    .restart local v10    # "clientId":Ljava/lang/String;
    .restart local v16    # "exEntity":Landroid/content/Entity;
    .restart local v45    # "attendeeString":Ljava/lang/String;
    .restart local v46    # "attendeeStringId":J
    .restart local v54    # "entity":Landroid/content/Entity;
    .restart local v55    # "entityValues":Landroid/content/ContentValues;
    .restart local v57    # "exFirst":Z
    .restart local v58    # "exEventId":J
    .restart local v60    # "exIterator":Landroid/content/EntityIterator;
    .restart local v61    # "exValues":Landroid/content/ContentValues;
    .restart local v62    # "exceptionDeleted":Z
    .restart local v64    # "hasAttendeeProperty":Z
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .restart local v69    # "ncvValues":Landroid/content/ContentValues;
    .restart local v71    # "organizerEmail":Ljava/lang/String;
    .restart local v78    # "propertyName":Ljava/lang/String;
    .restart local v81    # "selfOrganizer":Z
    .restart local v82    # "serverId":Ljava/lang/String;
    .restart local v85    # "userAttendeeStatus":Ljava/lang/String;
    .restart local v86    # "userAttendeeStatusId":J
    :cond_18
    :try_start_c
    const-string v5, "userAttendeeStatus"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 3742
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v85

    .line 3744
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v86

    goto/16 :goto_a

    .line 3750
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v78    # "propertyName":Ljava/lang/String;
    :cond_19
    invoke-virtual/range {v16 .. v16}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_1a
    :goto_d
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 3751
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 3752
    move-object/from16 v0, v68

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v69, v0

    .line 3753
    const-string v5, "name"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    .line 3755
    .restart local v78    # "propertyName":Ljava/lang/String;
    const-string v5, "attendees"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 3757
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    if-eqz v5, :cond_1a

    .line 3758
    const/16 v64, 0x1

    .line 3759
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 3761
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v46

    goto :goto_d

    .line 3765
    :cond_1b
    const-string v5, "userAttendeeStatus"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 3767
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v85

    .line 3769
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v86

    goto :goto_d

    .line 3776
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v78    # "propertyName":Ljava/lang/String;
    :cond_1c
    const-string v5, "selfAttendeeStatus"

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    move-result v51

    .line 3777
    .local v51, "currentStatus":I
    const/16 v84, 0x0

    .line 3778
    .local v84, "syncStatus":I
    if-eqz v85, :cond_1d

    .line 3780
    :try_start_d
    invoke-static/range {v85 .. v85}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    move-result v84

    .line 3789
    :cond_1d
    :goto_e
    :try_start_e
    const-string v5, "deleted"

    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getInt(Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1e

    const-string v5, "eventStatus"

    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getInt(Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_25

    .line 3792
    :cond_1e
    if-nez v81, :cond_24

    .line 3800
    const-string v5, "organizer"

    const-string v6, "organizer"

    move-object/from16 v0, v55

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3803
    const/16 v17, 0x80

    .line 3808
    .local v17, "flag":I
    :goto_f
    const/16 v62, 0x1

    .line 3809
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getMessageId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v20

    .line 3810
    .local v20, "tmpMsgId":J
    const-wide/16 v14, -0x1

    cmp-long v5, v20, v14

    if-eqz v5, :cond_20

    .line 3811
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v19, v0

    move-object/from16 v18, v10

    invoke-static/range {v15 .. v21}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 3813
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v23, :cond_1f

    .line 3814
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing decline/cancel invitation  to "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3815
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3820
    :cond_1f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "meetingInfo LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mailboxType"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v89

    .line 3822
    .restart local v89    # "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    move-object/from16 v0, v89

    invoke-virtual {v5, v6, v0, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3914
    .end local v20    # "tmpMsgId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v89    # "where":Ljava/lang/String;
    :cond_20
    :goto_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    invoke-static/range {v58 .. v59}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3922
    const-string v5, "sync_data4"

    const-string v6, "sync_data4"

    move-object/from16 v0, v55

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3927
    const-string v5, "eventLocation"

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_21

    .line 3928
    const-string v5, "eventLocation"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 3929
    const-string v5, "eventLocation"

    const-string v6, "eventLocation"

    move-object/from16 v0, v55

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v61

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3934
    :cond_21
    if-eqz v81, :cond_22

    if-nez v62, :cond_22

    .line 3935
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v5, v0, v1, v10, v6}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 3937
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v23, :cond_22

    .line 3938
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing exception update to "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3939
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3948
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_22
    new-instance v72, Ljava/util/ArrayList;

    invoke-direct/range {v72 .. v72}, Ljava/util/ArrayList;-><init>()V

    .line 3949
    .local v72, "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v45, :cond_2f

    .line 3950
    new-instance v83, Ljava/util/StringTokenizer;

    const-string v5, "\\"

    move-object/from16 v0, v83

    move-object/from16 v1, v45

    invoke-direct {v0, v1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3953
    .local v83, "st":Ljava/util/StringTokenizer;
    :goto_11
    invoke-virtual/range {v83 .. v83}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_2f

    .line 3954
    invoke-virtual/range {v83 .. v83}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v72

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_11

    .line 4044
    .end local v16    # "exEntity":Landroid/content/Entity;
    .end local v17    # "flag":I
    .end local v45    # "attendeeString":Ljava/lang/String;
    .end local v46    # "attendeeStringId":J
    .end local v51    # "currentStatus":I
    .end local v57    # "exFirst":Z
    .end local v58    # "exEventId":J
    .end local v61    # "exValues":Landroid/content/ContentValues;
    .end local v62    # "exceptionDeleted":Z
    .end local v64    # "hasAttendeeProperty":Z
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    .end local v72    # "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v83    # "st":Ljava/util/StringTokenizer;
    .end local v84    # "syncStatus":I
    .end local v85    # "userAttendeeStatus":Ljava/lang/String;
    .end local v86    # "userAttendeeStatusId":J
    :catchall_2
    move-exception v5

    if-eqz v60, :cond_23

    .line 4046
    :try_start_f
    invoke-interface/range {v60 .. v60}, Landroid/content/EntityIterator;->close()V

    :cond_23
    throw v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 3806
    .restart local v16    # "exEntity":Landroid/content/Entity;
    .restart local v45    # "attendeeString":Ljava/lang/String;
    .restart local v46    # "attendeeStringId":J
    .restart local v51    # "currentStatus":I
    .restart local v57    # "exFirst":Z
    .restart local v58    # "exEventId":J
    .restart local v61    # "exValues":Landroid/content/ContentValues;
    .restart local v62    # "exceptionDeleted":Z
    .restart local v64    # "hasAttendeeProperty":Z
    .restart local v69    # "ncvValues":Landroid/content/ContentValues;
    .restart local v84    # "syncStatus":I
    .restart local v85    # "userAttendeeStatus":Ljava/lang/String;
    .restart local v86    # "userAttendeeStatusId":J
    :cond_24
    const/16 v17, 0x20

    .restart local v17    # "flag":I
    goto/16 :goto_f

    .line 3826
    .end local v17    # "flag":I
    :cond_25
    move/from16 v0, v51

    move/from16 v1, v84

    if-eq v0, v1, :cond_2e

    if-eqz v51, :cond_2e

    if-nez v81, :cond_2e

    .line 3830
    const/16 v66, 0x0

    .line 3831
    .local v66, "isProposeNewTime":Z
    packed-switch v51, :pswitch_data_0

    .line 3842
    :pswitch_0
    const/16 v17, 0x0

    .line 3849
    .restart local v17    # "flag":I
    :goto_12
    if-eqz v17, :cond_28

    const-wide/16 v14, 0x0

    cmp-long v5, v86, v14

    if-ltz v5, :cond_28

    .line 3851
    :try_start_10
    invoke-virtual/range {v49 .. v49}, Landroid/content/ContentValues;->clear()V

    .line 3852
    const-string v5, "value"

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v49

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3854
    if-eqz v64, :cond_26

    .line 3855
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v86

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3860
    :cond_26
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getMessageId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v12

    .line 3861
    .restart local v12    # "meessageId":J
    const-wide/16 v14, -0x1

    cmp-long v5, v12, v14

    if-eqz v5, :cond_28

    .line 3864
    const/16 v5, 0x100

    move/from16 v0, v17

    if-ne v0, v5, :cond_2d

    .line 3865
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 3867
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    new-instance v67, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-direct {v0, v5}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 3868
    .local v67, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    if-nez v67, :cond_29

    const/16 v80, 0x0

    .line 3872
    .local v80, "resString":Ljava/lang/String;
    :goto_13
    if-nez v80, :cond_2b

    const/16 v79, -0x1

    .line 3874
    .local v79, "res":I
    :goto_14
    const/4 v5, -0x1

    move/from16 v0, v79

    if-eq v0, v5, :cond_2c

    or-int/lit8 v5, v79, 0x8

    if-eqz v5, :cond_2c

    .line 3875
    const/16 v66, 0x1

    .line 3876
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v0, v23

    move/from16 v1, v79

    invoke-virtual {v5, v0, v1}, Lcom/android/exchange/EasSyncService;->createResponseEntity(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;

    move-result-object v24

    .line 3877
    .local v24, "localEntity":Landroid/content/Entity;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v5, "UID"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v22 .. v27}, Lcom/android/exchange/utility/CalendarUtilities;->updateProposeNewTimeMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 3879
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v5, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 3880
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v5, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 3892
    .end local v24    # "localEntity":Landroid/content/Entity;
    .end local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    .end local v79    # "res":I
    .end local v80    # "resString":Ljava/lang/String;
    :goto_15
    if-eqz v23, :cond_27

    .line 3893
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing invitation reply to "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 3894
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3896
    :cond_27
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "meetingInfo LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mailboxType"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v89

    .line 3898
    .restart local v89    # "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    move-object/from16 v0, v89

    invoke-virtual {v5, v6, v0, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3902
    .end local v12    # "meessageId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v89    # "where":Ljava/lang/String;
    :cond_28
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    const-wide/high16 v18, 0x402c000000000000L    # 14.0

    cmpl-double v5, v14, v18

    if-lez v5, :cond_20

    if-nez v66, :cond_20

    .line 3903
    invoke-static/range {v51 .. v51}, Lcom/android/exchange/utility/CalendarUtilities;->userResponsefromSelfAttendeeStatus(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    .line 3904
    .local v27, "userResponse":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v5, Lcom/android/exchange/EasSyncService;->CollectionId:Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x1

    move-object/from16 v25, p0

    move-object/from16 v26, v55

    move-object/from16 v30, v61

    invoke-direct/range {v25 .. v30}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sendMeetingResponse(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/ContentValues;)V

    goto/16 :goto_10

    .line 3833
    .end local v17    # "flag":I
    .end local v27    # "userResponse":Ljava/lang/String;
    :pswitch_1
    const/16 v17, 0x40

    .line 3834
    .restart local v17    # "flag":I
    goto/16 :goto_12

    .line 3836
    .end local v17    # "flag":I
    :pswitch_2
    const/16 v17, 0x80

    .line 3837
    .restart local v17    # "flag":I
    goto/16 :goto_12

    .line 3839
    .end local v17    # "flag":I
    :pswitch_3
    const/16 v17, 0x100

    .line 3840
    .restart local v17    # "flag":I
    goto/16 :goto_12

    .line 3868
    .restart local v12    # "meessageId":J
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    :cond_29
    const-string v5, "MEETING_RESPONSE"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2a

    const/16 v80, 0x0

    goto/16 :goto_13

    :cond_2a
    const-string v5, "MEETING_RESPONSE"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v80

    goto/16 :goto_13

    .line 3872
    .restart local v80    # "resString":Ljava/lang/String;
    :cond_2b
    invoke-static/range {v80 .. v80}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v79

    goto/16 :goto_14

    .line 3882
    .restart local v79    # "res":I
    :cond_2c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v31, v0

    move-wide/from16 v27, v58

    move/from16 v29, v17

    move-object/from16 v30, v10

    move-wide/from16 v32, v12

    invoke-static/range {v26 .. v33}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    goto/16 :goto_15

    .line 3888
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    .end local v79    # "res":I
    .end local v80    # "resString":Ljava/lang/String;
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v31, v0

    move-wide/from16 v27, v58

    move/from16 v29, v17

    move-object/from16 v30, v10

    move-wide/from16 v32, v12

    invoke-static/range {v26 .. v33}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_15

    .line 3909
    .end local v12    # "meessageId":J
    .end local v17    # "flag":I
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v66    # "isProposeNewTime":Z
    :cond_2e
    const/16 v17, 0x10

    .restart local v17    # "flag":I
    goto/16 :goto_10

    .line 3957
    .restart local v72    # "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2f
    new-instance v70, Ljava/lang/StringBuilder;

    invoke-direct/range {v70 .. v70}, Ljava/lang/StringBuilder;-><init>()V

    .line 3963
    .local v70, "newTokenizedAttendees":Ljava/lang/StringBuilder;
    invoke-virtual/range {v16 .. v16}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_30
    :goto_16
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_31

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 3965
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_30

    .line 3966
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    const-string v6, "attendeeEmail"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 3969
    .local v44, "attendeeEmail":Ljava/lang/String;
    move-object/from16 v0, v72

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3970
    move-object/from16 v0, v70

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3971
    const-string v5, "\\"

    move-object/from16 v0, v70

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_16

    .line 3984
    .end local v44    # "attendeeEmail":Ljava/lang/String;
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    :cond_31
    new-instance v52, Landroid/content/ContentValues;

    invoke-direct/range {v52 .. v52}, Landroid/content/ContentValues;-><init>()V

    .line 3985
    .restart local v52    # "cv":Landroid/content/ContentValues;
    const-string v5, "value"

    invoke-virtual/range {v70 .. v70}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3987
    if-eqz v45, :cond_33

    if-eqz v64, :cond_33

    .line 3992
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v46

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4018
    :goto_17
    invoke-virtual/range {v72 .. v72}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_32
    :goto_18
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_34

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    .line 4021
    .local v35, "removedAttendee":Ljava/lang/String;
    if-eqz v81, :cond_32

    if-nez v62, :cond_32

    .line 4022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    const/16 v32, 0x20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v34, v0

    move-wide/from16 v30, v58

    move-object/from16 v33, v10

    invoke-static/range {v29 .. v35}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 4029
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v23, :cond_32

    .line 4031
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing cancellation to removed attendee "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4033
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .line 4003
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v35    # "removedAttendee":Ljava/lang/String;
    :cond_33
    const-string v5, "name"

    const-string v6, "attendees"

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4005
    const-string v5, "event_id"

    invoke-static/range {v58 .. v59}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4009
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v52

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_17

    .line 4038
    .end local v17    # "flag":I
    .end local v45    # "attendeeString":Ljava/lang/String;
    .end local v46    # "attendeeStringId":J
    .end local v51    # "currentStatus":I
    .end local v52    # "cv":Landroid/content/ContentValues;
    .end local v58    # "exEventId":J
    .end local v64    # "hasAttendeeProperty":Z
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    .end local v70    # "newTokenizedAttendees":Ljava/lang/StringBuilder;
    .end local v72    # "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v84    # "syncStatus":I
    .end local v85    # "userAttendeeStatus":Ljava/lang/String;
    .end local v86    # "userAttendeeStatusId":J
    :cond_34
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_9

    .line 4040
    .end local v16    # "exEntity":Landroid/content/Entity;
    .end local v61    # "exValues":Landroid/content/ContentValues;
    .end local v62    # "exceptionDeleted":Z
    :cond_35
    if-nez v57, :cond_36

    .line 4041
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 4044
    :cond_36
    if-eqz v60, :cond_37

    .line 4046
    :try_start_11
    invoke-interface/range {v60 .. v60}, Landroid/content/EntityIterator;->close()V

    .line 4051
    .end local v57    # "exFirst":Z
    .end local v60    # "exIterator":Landroid/content/EntityIterator;
    :cond_37
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4052
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mUploadedIdList:Ljava/util/ArrayList;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4058
    const/16 v45, 0x0

    .line 4059
    .restart local v45    # "attendeeString":Ljava/lang/String;
    const-wide/16 v46, -0x1

    .line 4060
    .restart local v46    # "attendeeStringId":J
    const/16 v85, 0x0

    .line 4061
    .restart local v85    # "userAttendeeStatus":Ljava/lang/String;
    const-wide/16 v86, -0x1

    .line 4062
    .restart local v86    # "userAttendeeStatusId":J
    invoke-virtual/range {v54 .. v54}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_38
    :goto_19
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3a

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 4063
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_38

    .line 4064
    move-object/from16 v0, v68

    iget-object v0, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    move-object/from16 v69, v0

    .line 4065
    .restart local v69    # "ncvValues":Landroid/content/ContentValues;
    const-string v5, "name"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    .line 4066
    .restart local v78    # "propertyName":Ljava/lang/String;
    const-string v5, "attendees"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_39

    .line 4067
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 4068
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v46

    goto :goto_19

    .line 4069
    :cond_39
    const-string v5, "userAttendeeStatus"

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_38

    .line 4070
    const-string v5, "value"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v85

    .line 4072
    const-string v5, "_id"

    move-object/from16 v0, v69

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v86

    goto :goto_19

    .line 4085
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    .end local v78    # "propertyName":Ljava/lang/String;
    :cond_3a
    if-eqz v81, :cond_41

    const-string v5, "dirty"

    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getInt(Landroid/content/ContentValues;Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_41

    .line 4086
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    const/16 v32, 0x10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v34, v0

    move-wide/from16 v30, v7

    move-object/from16 v33, v10

    invoke-static/range {v29 .. v34}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 4090
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v23, :cond_3b

    .line 4091
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "Queueing invitation to "

    aput-object v11, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4092
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4096
    :cond_3b
    new-instance v72, Ljava/util/ArrayList;

    invoke-direct/range {v72 .. v72}, Ljava/util/ArrayList;-><init>()V

    .line 4097
    .restart local v72    # "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v45, :cond_3c

    .line 4098
    new-instance v83, Ljava/util/StringTokenizer;

    const-string v5, "\\"

    move-object/from16 v0, v83

    move-object/from16 v1, v45

    invoke-direct {v0, v1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4100
    .restart local v83    # "st":Ljava/util/StringTokenizer;
    :goto_1a
    invoke-virtual/range {v83 .. v83}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 4101
    invoke-virtual/range {v83 .. v83}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v72

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 4104
    .end local v83    # "st":Ljava/util/StringTokenizer;
    :cond_3c
    new-instance v70, Ljava/lang/StringBuilder;

    invoke-direct/range {v70 .. v70}, Ljava/lang/StringBuilder;-><init>()V

    .line 4108
    .restart local v70    # "newTokenizedAttendees":Ljava/lang/StringBuilder;
    invoke-virtual/range {v54 .. v54}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_3d
    :goto_1b
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3e

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v68

    check-cast v68, Landroid/content/Entity$NamedContentValues;

    .line 4109
    .restart local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v6, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    .line 4110
    move-object/from16 v0, v68

    iget-object v5, v0, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    const-string v6, "attendeeEmail"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v44

    .line 4113
    .restart local v44    # "attendeeEmail":Ljava/lang/String;
    move-object/from16 v0, v72

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4114
    move-object/from16 v0, v70

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4115
    const-string v5, "\\"

    move-object/from16 v0, v70

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1b

    .line 4124
    .end local v44    # "attendeeEmail":Ljava/lang/String;
    .end local v68    # "ncv":Landroid/content/Entity$NamedContentValues;
    :cond_3e
    new-instance v52, Landroid/content/ContentValues;

    invoke-direct/range {v52 .. v52}, Landroid/content/ContentValues;-><init>()V

    .line 4125
    .restart local v52    # "cv":Landroid/content/ContentValues;
    const-string v5, "value"

    invoke-virtual/range {v70 .. v70}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4126
    if-eqz v45, :cond_40

    .line 4129
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v46

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4148
    :goto_1c
    invoke-virtual/range {v72 .. v72}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v65

    :cond_3f
    :goto_1d
    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface/range {v65 .. v65}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/lang/String;

    .line 4150
    .restart local v35    # "removedAttendee":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    const/16 v32, 0x20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v34, v0

    move-wide/from16 v30, v7

    move-object/from16 v33, v10

    invoke-static/range {v29 .. v35}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 4153
    if-eqz v23, :cond_3f

    .line 4155
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing cancellation to removed attendee "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4156
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1d

    .line 4137
    .end local v35    # "removedAttendee":Ljava/lang/String;
    :cond_40
    const-string v5, "name"

    const-string v6, "attendees"

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4138
    const-string v5, "event_id"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, v52

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 4141
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v52

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_1c

    .line 4159
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v52    # "cv":Landroid/content/ContentValues;
    .end local v70    # "newTokenizedAttendees":Ljava/lang/StringBuilder;
    .end local v72    # "originalAttendeeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_41
    if-nez v81, :cond_6

    .line 4165
    const-string v5, "selfAttendeeStatus"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-result v51

    .line 4166
    .restart local v51    # "currentStatus":I
    const/16 v84, 0x0

    .line 4167
    .restart local v84    # "syncStatus":I
    if-eqz v85, :cond_42

    .line 4169
    :try_start_12
    invoke-static/range {v85 .. v85}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_12
    .catch Ljava/lang/NumberFormatException; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result v84

    .line 4175
    :cond_42
    :goto_1e
    move/from16 v0, v51

    move/from16 v1, v84

    if-eq v0, v1, :cond_6

    if-eqz v51, :cond_6

    .line 4178
    const/16 v39, 0x0

    .line 4179
    .local v39, "messageFlag":I
    const/16 v66, 0x0

    .line 4180
    .restart local v66    # "isProposeNewTime":Z
    packed-switch v51, :pswitch_data_1

    .line 4193
    :goto_1f
    :pswitch_4
    if-eqz v39, :cond_44

    const-wide/16 v14, 0x0

    cmp-long v5, v86, v14

    if-ltz v5, :cond_44

    .line 4195
    :try_start_13
    invoke-virtual/range {v49 .. v49}, Landroid/content/ContentValues;->clear()V

    .line 4196
    const-string v5, "value"

    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v49

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4205
    sget-object v5, Landroid/provider/CalendarContract$ExtendedProperties;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v86

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v11, "com.android.exchange"

    invoke-static {v5, v6, v11}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v49

    invoke-virtual {v4, v5, v0, v6, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4212
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v10}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getMessageId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v12

    .line 4213
    .restart local v12    # "meessageId":J
    const-wide/16 v14, -0x1

    cmp-long v5, v12, v14

    if-eqz v5, :cond_44

    .line 4216
    const/16 v5, 0x100

    move/from16 v0, v39

    if-ne v0, v5, :cond_49

    .line 4217
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 4218
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    new-instance v67, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-direct {v0, v5}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 4219
    .restart local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    if-nez v67, :cond_45

    const/16 v80, 0x0

    .line 4223
    .restart local v80    # "resString":Ljava/lang/String;
    :goto_20
    if-eqz v80, :cond_47

    invoke-static/range {v80 .. v80}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v79

    .line 4224
    .restart local v79    # "res":I
    :goto_21
    const/4 v5, -0x1

    move/from16 v0, v79

    if-eq v0, v5, :cond_48

    or-int/lit8 v5, v79, 0x8

    if-eqz v5, :cond_48

    .line 4225
    const/16 v66, 0x1

    .line 4226
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    move-object/from16 v0, v23

    move/from16 v1, v79

    invoke-virtual {v5, v0, v1}, Lcom/android/exchange/EasSyncService;->createResponseEntity(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;

    move-result-object v24

    .line 4227
    .restart local v24    # "localEntity":Landroid/content/Entity;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    const-string v5, "UID"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    move-object/from16 v29, v23

    move-object/from16 v30, v24

    invoke-static/range {v28 .. v33}, Lcom/android/exchange/utility/CalendarUtilities;->updateProposeNewTimeMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    .line 4229
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v5, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 4230
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v5, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    iput-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 4242
    .end local v24    # "localEntity":Landroid/content/Entity;
    .end local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    .end local v79    # "res":I
    .end local v80    # "resString":Ljava/lang/String;
    :goto_22
    if-eqz v23, :cond_43

    .line 4243
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Queueing invitation reply to "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v23

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V

    .line 4244
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mOutgoingMailList:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4248
    :cond_43
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "meetingInfo LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mailboxType"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v89

    .line 4249
    .restart local v89    # "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v11, 0x0

    move-object/from16 v0, v89

    invoke-virtual {v5, v6, v0, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4254
    .end local v12    # "meessageId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v89    # "where":Ljava/lang/String;
    :cond_44
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v5, v5, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    const-wide/high16 v18, 0x402c000000000000L    # 14.0

    cmpl-double v5, v14, v18

    if-ltz v5, :cond_6

    if-nez v66, :cond_6

    .line 4255
    invoke-static/range {v51 .. v51}, Lcom/android/exchange/utility/CalendarUtilities;->userResponsefromSelfAttendeeStatus(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    .line 4256
    .restart local v27    # "userResponse":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v5, Lcom/android/exchange/EasSyncService;->CollectionId:Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v25, p0

    move-object/from16 v26, v55

    invoke-direct/range {v25 .. v30}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sendMeetingResponse(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/ContentValues;)V

    goto/16 :goto_6

    .line 4182
    .end local v27    # "userResponse":Ljava/lang/String;
    :pswitch_5
    const/16 v39, 0x40

    .line 4183
    goto/16 :goto_1f

    .line 4185
    :pswitch_6
    const/16 v39, 0x80

    .line 4186
    goto/16 :goto_1f

    .line 4188
    :pswitch_7
    const/16 v39, 0x100

    goto/16 :goto_1f

    .line 4219
    .restart local v12    # "meessageId":J
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    :cond_45
    const-string v5, "MEETING_RESPONSE"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_46

    const/16 v80, 0x0

    goto/16 :goto_20

    :cond_46
    const-string v5, "MEETING_RESPONSE"

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v80

    goto/16 :goto_20

    .line 4223
    .restart local v80    # "resString":Ljava/lang/String;
    :cond_47
    const/16 v79, -0x1

    goto/16 :goto_21

    .line 4232
    .restart local v79    # "res":I
    :cond_48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v41, v0

    move-wide/from16 v37, v7

    move-object/from16 v40, v10

    move-wide/from16 v42, v12

    invoke-static/range {v36 .. v43}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v23

    goto/16 :goto_22

    .line 4238
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v67    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    .end local v79    # "res":I
    .end local v80    # "resString":Ljava/lang/String;
    :cond_49
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v41, v0

    move-wide/from16 v37, v7

    move-object/from16 v40, v10

    move-wide/from16 v42, v12

    invoke-static/range {v36 .. v43}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEventId(Landroid/content/Context;JILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;J)Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    move-result-object v23

    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    goto/16 :goto_22

    .line 3781
    .end local v12    # "meessageId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v39    # "messageFlag":I
    .end local v66    # "isProposeNewTime":Z
    .restart local v16    # "exEntity":Landroid/content/Entity;
    .restart local v57    # "exFirst":Z
    .restart local v58    # "exEventId":J
    .restart local v60    # "exIterator":Landroid/content/EntityIterator;
    .restart local v61    # "exValues":Landroid/content/ContentValues;
    .restart local v62    # "exceptionDeleted":Z
    .restart local v64    # "hasAttendeeProperty":Z
    .restart local v69    # "ncvValues":Landroid/content/ContentValues;
    :catch_2
    move-exception v5

    goto/16 :goto_e

    .line 4170
    .end local v16    # "exEntity":Landroid/content/Entity;
    .end local v57    # "exFirst":Z
    .end local v58    # "exEventId":J
    .end local v60    # "exIterator":Landroid/content/EntityIterator;
    .end local v61    # "exValues":Landroid/content/ContentValues;
    .end local v62    # "exceptionDeleted":Z
    .end local v64    # "hasAttendeeProperty":Z
    .end local v69    # "ncvValues":Landroid/content/ContentValues;
    :catch_3
    move-exception v5

    goto/16 :goto_1e

    .line 3831
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 4180
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 1
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 555
    if-nez p3, :cond_0

    .line 556
    invoke-direct {p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getCalendarFilter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->setPimSyncOptions(Ljava/lang/Double;Ljava/lang/String;Lcom/android/exchange/adapter/Serializer;)V

    .line 558
    :cond_0
    return-void
.end method

.method public setSyncKey(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "syncKey"    # Ljava/lang/String;
    .param p2, "inCommands"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 706
    sget-object v3, Lcom/android/exchange/adapter/CalendarSyncAdapter;->sSyncKeyLock:Ljava/lang/Object;

    monitor-enter v3

    .line 709
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v4, 0x52

    if-eq v2, v4, :cond_1

    const-string v2, "0"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez p2, :cond_1

    .line 715
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v2, v2, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 723
    .local v0, "client":Landroid/content/ContentProviderClient;
    :try_start_1
    sget-object v2, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mEmailAddress:Ljava/lang/String;

    const-string v5, "com.android.exchange"

    invoke-static {v2, v4, v5}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->asSyncAdapter(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mAccountManagerAccount:Landroid/accounts/Account;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v0, v2, v4, v5}, Landroid/provider/SyncStateContract$Helpers;->set(Landroid/content/ContentProviderClient;Landroid/net/Uri;Landroid/accounts/Account;[B)V

    .line 729
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "SyncKey set to "

    aput-object v5, v2, v4

    const/4 v4, 0x1

    aput-object p1, v2, v4

    const/4 v4, 0x2

    const-string v5, " in CalendarProvider"

    aput-object v5, v2, v4

    invoke-virtual {p0, v2}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 734
    .end local v0    # "client":Landroid/content/ContentProviderClient;
    :cond_1
    :try_start_2
    iget-object v2, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object p1, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    .line 735
    monitor-exit v3

    .line 736
    return-void

    .line 730
    .restart local v0    # "client":Landroid/content/ContentProviderClient;
    :catch_0
    move-exception v1

    .line 731
    .local v1, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/io/IOException;

    const-string v4, "Can\'t set SyncKey in CalendarProvider"

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 735
    .end local v0    # "client":Landroid/content/ContentProviderClient;
    .end local v1    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public wipe()V
    .locals 14

    .prologue
    .line 440
    const-string v7, "EasCalendarSyncAdapter"

    const-string v8, "CALENDAR BAD SYNC KEY"

    invoke-static {v7, v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 442
    .local v1, "cvx":Landroid/content/ContentValues;
    const-string v7, "syncKey"

    const-string v8, "0"

    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v8, 0x52

    if-ne v7, v8, :cond_0

    .line 446
    const-string v7, "EasCalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CALENDAR SUBFOLDER "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " HAS BAD SYNC KEY :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v9, "accountKey=? and serverId=?"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 452
    .local v5, "mailboxCount":I
    const-string v7, "EasCalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Updating Calendar SubFolder mailbox with sync key 0. Count = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->deleteCalendarEventsOfSubFolder(Ljava/lang/String;)V

    .line 510
    :goto_0
    return-void

    .line 462
    .end local v5    # "mailboxCount":I
    :cond_0
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v9, "accountKey=? and type in (65,82)"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v12, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v7, v8, v1, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 469
    .restart local v5    # "mailboxCount":I
    const-string v7, "EasCalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Updating Calendar mailbox with sync key 0. Count = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "account_type="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "com.android.exchange"

    invoke-static {v8}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 482
    .local v6, "where":Ljava/lang/String;
    sget-object v7, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v7}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getCalendarUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    .line 485
    .local v4, "mCalendarUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v6, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 489
    .local v0, "calCount":I
    const-string v7, "EasCalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Calendars table row deleted. Count = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    sget-object v7, Landroid/provider/CalendarContract$SyncState;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v7}, Lcom/android/exchange/adapter/CalendarSyncAdapter;->getCalendarUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 498
    .local v3, "mCalendarSyncStateUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/android/exchange/adapter/CalendarSyncAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v3, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 505
    .local v2, "mCalendarSyncStateCount":I
    const-string v7, "EasCalendarSyncAdapter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Calendar _sync_state table row deleted. rowCount = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    invoke-static {}, Lcom/android/exchange/ExchangeService;->unregisterCalendarObservers()V

    goto/16 :goto_0
.end method
