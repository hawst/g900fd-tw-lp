.class public Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "RecipientInformationcacheSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RecipientInformationcacheSyncParser"
.end annotation


# instance fields
.field RIcacheids:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;"
        }
    .end annotation
.end field

.field RIcacheids_Del:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;"
        }
    .end annotation
.end field

.field RIcacheids_Modi:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V
    .locals 1
    .param p2, "parse"    # Lcom/android/exchange/adapter/Parser;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .param p4, "resumeParser"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    .line 121
    invoke-direct {p0, p2, p3, p4}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Modi:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Del:Ljava/util/ArrayList;

    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 1
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    .line 115
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids:Ljava/util/ArrayList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Modi:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Del:Ljava/util/ArrayList;

    .line 117
    return-void
.end method

.method private addData(Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;)V
    .locals 2
    .param p1, "ric"    # Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    :goto_0
    const/16 v0, 0x1d

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 209
    iget v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 211
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riEmailAddress:Ljava/lang/String;

    goto :goto_0

    .line 217
    :sswitch_1
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riFileAs:Ljava/lang/String;

    goto :goto_0

    .line 221
    :sswitch_2
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAlias:Ljava/lang/String;

    goto :goto_0

    .line 225
    :sswitch_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riWeightedRank:Ljava/lang/String;

    goto :goto_0

    .line 231
    :cond_0
    return-void

    .line 209
    :sswitch_data_0
    .sparse-switch
        0x5b -> :sswitch_0
        0x5e -> :sswitch_1
        0x7d -> :sswitch_2
        0x7e -> :sswitch_3
    .end sparse-switch
.end method

.method private addParser(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "rIcacheids2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;>;"
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;-><init>()V

    .line 148
    .local v0, "ric":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    .line 149
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riMailboxKey:J

    .line 150
    :goto_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 151
    iget v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 153
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    goto :goto_0

    .line 157
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->addData(Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;)V

    goto :goto_0

    .line 161
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    return-void

    .line 151
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method private changeParser(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "rIcacheids2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;>;"
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;-><init>()V

    .line 170
    .local v0, "ric":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    .line 171
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riMailboxKey:J

    .line 172
    :goto_0
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 173
    iget v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 175
    :sswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    goto :goto_0

    .line 179
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->addData(Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;)V

    goto :goto_0

    .line 183
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    return-void

    .line 173
    :sswitch_data_0
    .sparse-switch
        0xd -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method private deleteParser(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "rIcacheids2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;>;"
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;-><init>()V

    .line 191
    .local v0, "ric":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    .line 192
    iget-object v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riMailboxKey:J

    .line 193
    :goto_0
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->nextTag(I)I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 194
    iget v1, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 196
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    return-void

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public commandsParser()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    :goto_0
    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 129
    iget v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 130
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->addParser(Ljava/util/ArrayList;)V

    .line 131
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 132
    :cond_0
    iget v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    const/16 v1, 0x21

    if-ne v0, v1, :cond_2

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Del:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->deleteParser(Ljava/util/ArrayList;)V

    .line 134
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 135
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->tag:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 136
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Modi:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->changeParser(Ljava/util/ArrayList;)V

    .line 137
    iget-object v0, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->this$0:Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;->incrementChangeCount()V

    goto :goto_0

    .line 139
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->skipTag()V

    goto :goto_0

    .line 142
    :cond_4
    return-void
.end method

.method public commit()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 243
    const/4 v0, 0x0

    .line 244
    .local v0, "Nu_of":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v4, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    .line 250
    .local v1, "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 251
    .local v2, "cValues":Landroid/content/ContentValues;
    const-string v7, "server_id"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v7, "accountkey"

    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 253
    const-string v7, "email_address"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riEmailAddress:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    const-string v7, "fileas"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riFileAs:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v7, "alias"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAlias:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v7, "weightedrank"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riWeightedRank:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto :goto_0

    .line 273
    .end local v1    # "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    .end local v2    # "cValues":Landroid/content/ContentValues;
    :cond_0
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Modi:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 274
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Modi:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    .line 279
    .restart local v1    # "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    const-string v5, "server_id=? AND accountkey=?"

    .line 281
    .local v5, "selection":Ljava/lang/String;
    new-array v6, v12, [Ljava/lang/String;

    iget-object v7, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    aput-object v7, v6, v10

    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    .line 284
    .local v6, "selectionArgs":[Ljava/lang/String;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 285
    .restart local v2    # "cValues":Landroid/content/ContentValues;
    const-string v7, "weightedrank"

    iget-object v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riWeightedRank:Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    const-string v7, "RecipientInformation Cache"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RIC adapter-commit"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 310
    const-string v7, "RecipientInformation Cache"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RIC adapter-after modify commit"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 313
    .end local v1    # "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    .end local v2    # "cValues":Landroid/content/ContentValues;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Del:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 314
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->RIcacheids_Del:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;

    .line 316
    .restart local v1    # "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    const-string v5, "server_id=? AND accountkey=?"

    .line 318
    .restart local v5    # "selection":Ljava/lang/String;
    new-array v6, v12, [Ljava/lang/String;

    iget-object v7, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riServerId:Ljava/lang/String;

    aput-object v7, v6, v10

    iget-wide v8, v1, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->riAccountKey:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v11

    .line 321
    .restart local v6    # "selectionArgs":[Ljava/lang/String;
    iget-object v7, p0, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter$RecipientInformationcacheSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 323
    const-string v7, "RecipientInformation Cache"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RIC adapter-afterdel commit"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 327
    .end local v1    # "Ri":Lcom/android/emailcommon/provider/EmailContent$RecipientInformationCache;
    .end local v5    # "selection":Ljava/lang/String;
    .end local v6    # "selectionArgs":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 237
    return-void
.end method
