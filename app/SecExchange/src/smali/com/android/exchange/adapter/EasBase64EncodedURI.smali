.class public Lcom/android/exchange/adapter/EasBase64EncodedURI;
.super Ljava/lang/Object;
.source "EasBase64EncodedURI.java"


# instance fields
.field mCmd:[B

.field mDeviceId:Ljava/lang/String;

.field mDeviceType:Ljava/lang/String;

.field mExtra:Ljava/lang/String;

.field mExtraCmd:Ljava/lang/String;

.field mHostAddress:Ljava/lang/String;

.field mLocale:[B

.field mPolicyKey:Ljava/lang/String;

.field mProtocol:[B

.field mSsl:Z

.field mTrustSsl:Z

.field mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "ssl"    # Z
    .param p2, "trustSsl"    # Z
    .param p3, "hostAddress"    # Ljava/lang/String;
    .param p4, "userName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-array v0, v4, [B

    const/16 v1, 0x79

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mProtocol:[B

    .line 30
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    .line 32
    const-string v0, "Android"

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    .line 34
    new-array v0, v4, [B

    const/16 v1, 0x17

    aput-byte v1, v0, v3

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mCmd:[B

    .line 40
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtraCmd:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtra:Ljava/lang/String;

    .line 44
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mPolicyKey:Ljava/lang/String;

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mLocale:[B

    .line 292
    iput-boolean p1, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mSsl:Z

    .line 294
    iput-boolean p2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mTrustSsl:Z

    .line 296
    iput-object p3, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mHostAddress:Ljava/lang/String;

    .line 298
    iput-object p4, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mUserName:Ljava/lang/String;

    .line 300
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtraCmd:Ljava/lang/String;

    .line 302
    iput-object v2, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtra:Ljava/lang/String;

    .line 304
    return-void

    .line 48
    nop

    :array_0
    .array-data 1
        0x9t
        0x4t
    .end array-data
.end method

.method private encodeUriString()Ljava/lang/String;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 414
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 416
    .local v2, "buf":Ljava/io/ByteArrayOutputStream;
    const/16 v17, 0x5

    move/from16 v0, v17

    new-array v10, v0, [B

    .line 418
    .local v10, "policyByte":[B
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v9, v0, [B

    const/16 v17, 0x0

    const/16 v18, 0x0

    aput-byte v18, v9, v17

    .line 424
    .local v9, "optionsByte":[B
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v7, v0, [B

    const/16 v17, 0x0

    const/16 v18, 0x1

    aput-byte v18, v7, v17

    .line 432
    .local v7, "length":[B
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mSsl:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mTrustSsl:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    const-string v17, "httpts"

    :goto_0
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "://"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mHostAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mSsl:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    const-string v17, ":443"

    :goto_1
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/Microsoft-Server-ActiveSync?"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 440
    .local v15, "us":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mProtocol:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mCmd:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mLocale:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_4

    .line 456
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 482
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mPolicyKey:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_6

    .line 486
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 546
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_b

    .line 550
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 576
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtraCmd:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v9, v1}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->writeCmdParams(Ljava/io/OutputStream;[BLjava/lang/String;)V

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtra:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v9, v1}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->writeCmdParams(Ljava/io/OutputStream;[BLjava/lang/String;)V

    .line 584
    const/16 v17, 0x0

    aget-byte v17, v9, v17

    if-eqz v17, :cond_0

    .line 588
    const/16 v17, 0x7

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 592
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 596
    const/16 v17, 0x0

    aget-byte v17, v9, v17

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 608
    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 610
    .local v3, "byteData":[B
    const-string v4, "Fix It"

    .line 612
    .local v4, "encoded":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    .end local v4    # "encoded":Ljava/lang/String;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v3, v0}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V

    .line 613
    .restart local v4    # "encoded":Ljava/lang/String;
    const-string v17, "EasBase64EncodedURI"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "encoded: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 617
    return-object v15

    .line 432
    .end local v3    # "byteData":[B
    .end local v4    # "encoded":Ljava/lang/String;
    .end local v15    # "us":Ljava/lang/String;
    :cond_1
    const-string v17, "https"

    goto/16 :goto_0

    :cond_2
    const-string v17, "http"

    goto/16 :goto_0

    :cond_3
    const-string v17, ""

    goto/16 :goto_1

    .line 460
    .restart local v15    # "us":Ljava/lang/String;
    :cond_4
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v7, v17

    .line 464
    const/16 v17, 0x0

    aget-byte v17, v7, v17

    const/16 v18, 0x10

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_5

    .line 466
    const/16 v17, 0x0

    aget-byte v17, v7, v17

    add-int/lit8 v8, v17, -0x10

    .line 468
    .local v8, "lengthExtra":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    .line 470
    const/16 v17, 0x0

    const/16 v18, 0x10

    aput-byte v18, v7, v17

    .line 474
    .end local v8    # "lengthExtra":I
    :cond_5
    invoke-virtual {v2, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 490
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mPolicyKey:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    .line 492
    .local v16, "zeroPolicy":Z
    if-eqz v16, :cond_8

    .line 494
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_5
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ge v6, v0, :cond_7

    .line 496
    const/16 v17, 0x0

    aput-byte v17, v10, v6

    .line 494
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 500
    :cond_7
    const/16 v17, 0x0

    const/16 v18, 0x4

    aput-byte v18, v10, v17

    .line 502
    invoke-virtual {v2, v10}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_3

    .line 506
    .end local v6    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mPolicyKey:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 508
    .local v12, "policyKeyValue":J
    invoke-static {v12, v13}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v11

    .line 510
    .local v11, "policyKeyString":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v14

    .line 514
    .local v14, "size":I
    const/16 v17, 0x8

    move/from16 v0, v17

    if-eq v14, v0, :cond_9

    .line 518
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto/16 :goto_3

    .line 522
    :cond_9
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_6
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ge v6, v0, :cond_a

    .line 524
    rsub-int/lit8 v17, v6, 0x4

    mul-int/lit8 v18, v6, 0x2

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->charAt(I)C

    move-result v18

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->hex2int(C)C

    move-result v18

    shl-int/lit8 v18, v18, 0x4

    mul-int/lit8 v19, v6, 0x2

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->hex2int(C)C

    move-result v19

    or-int v18, v18, v19

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v10, v17

    .line 522
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 530
    :cond_a
    const/16 v17, 0x0

    const/16 v18, 0x4

    aput-byte v18, v10, v17

    .line 532
    const-string v17, "EasBase64EncodedURI"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "policyByte: length["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x0

    aget-byte v19, v10, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "] "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x1

    aget-byte v19, v10, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x2

    aget-byte v19, v10, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x3

    aget-byte v19, v10, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x4

    aget-byte v19, v10, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    invoke-virtual {v2, v10}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto/16 :goto_3

    .line 554
    .end local v6    # "i":I
    .end local v11    # "policyKeyString":Ljava/lang/String;
    .end local v12    # "policyKeyValue":J
    .end local v14    # "size":I
    .end local v16    # "zeroPolicy":Z
    :cond_b
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v7, v17

    .line 558
    const/16 v17, 0x0

    aget-byte v17, v7, v17

    const/16 v18, 0x10

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_c

    .line 560
    const/16 v17, 0x0

    aget-byte v17, v7, v17

    add-int/lit8 v5, v17, -0x10

    .line 562
    .local v5, "extra":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    .line 564
    const/16 v17, 0x0

    const/16 v18, 0x10

    aput-byte v18, v7, v17

    .line 568
    .end local v5    # "extra":I
    :cond_c
    invoke-virtual {v2, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v2, v1}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method private getCommandCode(Ljava/lang/String;)B
    .locals 2
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "cmd":Ljava/lang/String;
    const-string v1, "sync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const/4 v1, 0x0

    .line 204
    :goto_0
    return v1

    .line 112
    :cond_0
    const-string v1, "sendmail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 114
    const/4 v1, 0x1

    goto :goto_0

    .line 116
    :cond_1
    const-string v1, "smartforward"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 118
    const/4 v1, 0x2

    goto :goto_0

    .line 120
    :cond_2
    const-string v1, "smartreply"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 122
    const/4 v1, 0x3

    goto :goto_0

    .line 124
    :cond_3
    const-string v1, "getattachment"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 126
    const/4 v1, 0x4

    goto :goto_0

    .line 128
    :cond_4
    const-string v1, "gethierarchy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 130
    const/4 v1, 0x5

    goto :goto_0

    .line 132
    :cond_5
    const-string v1, "createcollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 134
    const/4 v1, 0x6

    goto :goto_0

    .line 136
    :cond_6
    const-string v1, "deletecollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 138
    const/4 v1, 0x7

    goto :goto_0

    .line 140
    :cond_7
    const-string v1, "movecollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 142
    const/16 v1, 0x8

    goto :goto_0

    .line 144
    :cond_8
    const-string v1, "foldersync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 146
    const/16 v1, 0x9

    goto :goto_0

    .line 148
    :cond_9
    const-string v1, "foldercreate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 150
    const/16 v1, 0xa

    goto :goto_0

    .line 152
    :cond_a
    const-string v1, "folderdelete"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 154
    const/16 v1, 0xb

    goto :goto_0

    .line 156
    :cond_b
    const-string v1, "folderupdate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 158
    const/16 v1, 0xc

    goto :goto_0

    .line 160
    :cond_c
    const-string v1, "moveitems"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 162
    const/16 v1, 0xd

    goto/16 :goto_0

    .line 164
    :cond_d
    const-string v1, "getitemestimate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 166
    const/16 v1, 0xe

    goto/16 :goto_0

    .line 168
    :cond_e
    const-string v1, "meetingresponse"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 170
    const/16 v1, 0xf

    goto/16 :goto_0

    .line 172
    :cond_f
    const-string v1, "search"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 174
    const/16 v1, 0x10

    goto/16 :goto_0

    .line 176
    :cond_10
    const-string v1, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 178
    const/16 v1, 0x11

    goto/16 :goto_0

    .line 180
    :cond_11
    const-string v1, "ping"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 182
    const/16 v1, 0x12

    goto/16 :goto_0

    .line 184
    :cond_12
    const-string v1, "itemoperations"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 186
    const/16 v1, 0x13

    goto/16 :goto_0

    .line 188
    :cond_13
    const-string v1, "provision"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 190
    const/16 v1, 0x14

    goto/16 :goto_0

    .line 192
    :cond_14
    const-string v1, "resolverecipients"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 194
    const/16 v1, 0x15

    goto/16 :goto_0

    .line 196
    :cond_15
    const-string v1, "validatecert"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 198
    const/16 v1, 0x16

    goto/16 :goto_0

    .line 204
    :cond_16
    const/16 v1, 0x17

    goto/16 :goto_0
.end method

.method private getParameterTag(Ljava/lang/String;)B
    .locals 2
    .param p1, "parameters"    # Ljava/lang/String;

    .prologue
    .line 234
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 236
    .local v0, "paramName":Ljava/lang/String;
    const-string v1, "attachmentname"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    const/4 v1, 0x0

    .line 284
    :goto_0
    return v1

    .line 240
    :cond_0
    const-string v1, "collectionid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    const/4 v1, 0x1

    goto :goto_0

    .line 244
    :cond_1
    const-string v1, "collectionname"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 246
    const/4 v1, 0x2

    goto :goto_0

    .line 248
    :cond_2
    const-string v1, "itemid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 250
    const/4 v1, 0x3

    goto :goto_0

    .line 252
    :cond_3
    const-string v1, "longid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 254
    const/4 v1, 0x4

    goto :goto_0

    .line 256
    :cond_4
    const-string v1, "parentid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 258
    const/4 v1, 0x5

    goto :goto_0

    .line 260
    :cond_5
    const-string v1, "occurrence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 262
    const/4 v1, 0x6

    goto :goto_0

    .line 264
    :cond_6
    const-string v1, "options"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 266
    const/4 v1, 0x7

    goto :goto_0

    .line 268
    :cond_7
    const-string v1, "roundtripid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 270
    const/16 v1, 0x8

    goto :goto_0

    .line 272
    :cond_8
    const-string v1, "saveinsent"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 274
    const/16 v1, 0x17

    goto :goto_0

    .line 276
    :cond_9
    const-string v1, "acceptmultipart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 278
    const/16 v1, 0x27

    goto :goto_0

    .line 284
    :cond_a
    const/16 v1, 0x3f

    goto :goto_0
.end method

.method public static hex2int(C)C
    .locals 2
    .param p0, "b"    # C

    .prologue
    .line 384
    const/16 v1, 0x61

    if-lt p0, v1, :cond_0

    .line 386
    add-int/lit8 v1, p0, -0x57

    int-to-char v0, v1

    .line 394
    .local v0, "num":C
    :goto_0
    and-int/lit8 v1, v0, 0xf

    int-to-char v0, v1

    .line 396
    return v0

    .line 390
    .end local v0    # "num":C
    :cond_0
    add-int/lit8 v1, p0, -0x30

    int-to-char v0, v1

    .restart local v0    # "num":C
    goto :goto_0
.end method

.method private writeCmdParams(Ljava/io/OutputStream;[BLjava/lang/String;)V
    .locals 12
    .param p1, "buf"    # Ljava/io/OutputStream;
    .param p2, "optionsByte"    # [B
    .param p3, "extra"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 625
    if-nez p3, :cond_1

    .line 749
    :cond_0
    return-void

    .line 633
    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    .line 635
    .local v0, "extraLen":I
    const/4 v10, 0x1

    new-array v1, v10, [B

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput-byte v11, v1, v10

    .line 643
    .local v1, "length":[B
    const/4 v10, 0x1

    invoke-virtual {p3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 645
    :goto_0
    if-lez v0, :cond_0

    .line 651
    const/16 v10, 0x26

    invoke-virtual {p3, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 653
    .local v3, "myLength":I
    const/4 v10, -0x1

    if-eq v3, v10, :cond_4

    .line 655
    const/4 v10, 0x0

    invoke-virtual {p3, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 659
    .local v2, "myExtra":Ljava/lang/String;
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {p3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 671
    :goto_1
    const/16 v10, 0x3d

    invoke-virtual {v2, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 673
    .local v9, "tagLength":I
    const/4 v10, -0x1

    if-ne v9, v10, :cond_2

    .line 679
    :cond_2
    const/4 v10, 0x0

    invoke-virtual {v2, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 681
    .local v5, "paramsTag":Ljava/lang/String;
    add-int/lit8 v10, v9, 0x1

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 687
    .local v7, "paramsValue":Ljava/lang/String;
    const/4 v10, 0x1

    new-array v8, v10, [B

    const/4 v10, 0x0

    const/16 v11, 0x3f

    aput-byte v11, v8, v10

    .line 693
    .local v8, "tag":[B
    invoke-direct {p0, v5}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->getParameterTag(Ljava/lang/String;)B

    move-result v6

    .line 695
    .local v6, "paramsTagByte":I
    const/4 v10, 0x0

    aget-byte v10, v8, v10

    const/16 v11, 0x3f

    if-ne v10, v11, :cond_3

    .line 701
    :cond_3
    const/4 v10, 0x0

    and-int/lit8 v11, v6, 0xf

    int-to-byte v11, v11

    aput-byte v11, v8, v10

    .line 703
    const/4 v10, 0x0

    aget-byte v10, v8, v10

    const/4 v11, 0x7

    if-ne v10, v11, :cond_5

    .line 707
    and-int/lit8 v10, v6, 0x30

    shr-int/lit8 v10, v10, 0x4

    int-to-byte v4, v10

    .line 709
    .local v4, "optionValueByte":B
    const/4 v10, 0x0

    aget-byte v11, p2, v10

    or-int/2addr v11, v4

    int-to-byte v11, v11

    aput-byte v11, p2, v10

    .line 739
    .end local v4    # "optionValueByte":B
    :goto_2
    if-eqz p3, :cond_0

    .line 745
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    .line 747
    goto :goto_0

    .line 663
    .end local v2    # "myExtra":Ljava/lang/String;
    .end local v5    # "paramsTag":Ljava/lang/String;
    .end local v6    # "paramsTagByte":I
    .end local v7    # "paramsValue":Ljava/lang/String;
    .end local v8    # "tag":[B
    .end local v9    # "tagLength":I
    :cond_4
    const/4 v10, 0x0

    invoke-virtual {p3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 665
    .restart local v2    # "myExtra":Ljava/lang/String;
    const/4 p3, 0x0

    goto :goto_1

    .line 715
    .restart local v5    # "paramsTag":Ljava/lang/String;
    .restart local v6    # "paramsTagByte":I
    .restart local v7    # "paramsValue":Ljava/lang/String;
    .restart local v8    # "tag":[B
    .restart local v9    # "tagLength":I
    :cond_5
    invoke-virtual {p1, v8}, Ljava/io/OutputStream;->write([B)V

    .line 719
    if-nez v7, :cond_6

    .line 723
    const/4 v10, 0x0

    invoke-virtual {p1, v10}, Ljava/io/OutputStream;->write(I)V

    goto :goto_2

    .line 727
    :cond_6
    const/4 v10, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    int-to-byte v11, v11

    aput-byte v11, v1, v10

    .line 729
    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 731
    invoke-direct {p0, p1, v7}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 402
    if-eqz p2, :cond_0

    .line 404
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 406
    .local v0, "data":[B
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 410
    .end local v0    # "data":[B
    :cond_0
    return-void
.end method


# virtual methods
.method public getUriString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "protocolVersion"    # Ljava/lang/String;
    .param p2, "cmd"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "deviceId"    # Ljava/lang/String;
    .param p5, "policyKey"    # Ljava/lang/String;
    .param p6, "deviceType"    # Ljava/lang/String;

    .prologue
    .line 312
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 314
    .local v8, "version":D
    const-wide/high16 v10, 0x402c000000000000L    # 14.0

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_2

    .line 318
    iget-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mProtocol:[B

    const/4 v10, 0x0

    const/16 v11, -0x73

    aput-byte v11, v7, v10

    .line 328
    :goto_0
    const/16 v7, 0x26

    invoke-virtual {p2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 330
    .local v2, "cmdLength":I
    const/4 v7, -0x1

    if-eq v2, v7, :cond_0

    .line 332
    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtraCmd:Ljava/lang/String;

    .line 334
    const/4 v7, 0x0

    invoke-virtual {p2, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 338
    :cond_0
    iget-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mCmd:[B

    const/4 v10, 0x0

    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->getCommandCode(Ljava/lang/String;)B

    move-result v11

    aput-byte v11, v7, v10

    .line 340
    iput-object p3, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mExtra:Ljava/lang/String;

    .line 342
    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    .line 344
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mPolicyKey:Ljava/lang/String;

    .line 346
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceType:Ljava/lang/String;

    .line 350
    iget-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v5

    .line 352
    .local v5, "idLength":I
    const/16 v7, 0x10

    if-le v5, v7, :cond_1

    .line 354
    add-int/lit8 v6, v5, -0x10

    .line 356
    .local v6, "index":I
    iget-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mDeviceId:Ljava/lang/String;

    .line 362
    .end local v6    # "index":I
    :cond_1
    const/4 v4, 0x0

    .line 366
    .local v4, "encodedURI":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->encodeUriString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 376
    :goto_1
    return-object v4

    .line 322
    .end local v2    # "cmdLength":I
    .end local v4    # "encodedURI":Ljava/lang/String;
    .end local v5    # "idLength":I
    :cond_2
    iget-object v7, p0, Lcom/android/exchange/adapter/EasBase64EncodedURI;->mProtocol:[B

    const/4 v10, 0x0

    const/16 v11, 0x79

    aput-byte v11, v7, v10

    goto :goto_0

    .line 368
    .restart local v2    # "cmdLength":I
    .restart local v4    # "encodedURI":Ljava/lang/String;
    .restart local v5    # "idLength":I
    :catch_0
    move-exception v3

    .line 372
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
