.class public abstract Lcom/android/exchange/adapter/AbstractSyncParser;
.super Lcom/android/exchange/adapter/AbstractUtiltyParser;
.source "AbstractSyncParser.java"


# instance fields
.field private final DELETE_MAILBOX_OF_TYPE:Ljava/lang/String;

.field cv:Landroid/content/ContentValues;

.field interval:I

.field protected mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

.field protected mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

.field private mBindArguments:[Ljava/lang/String;

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContext:Landroid/content/Context;

.field private mLooping:Z

.field protected mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

.field protected mService:Lcom/android/exchange/EasSyncService;

.field mailboxUpdated:Z

.field protected moreAvailable:Z

.field protected multiFolder:Z

.field newKey:Ljava/lang/String;

.field protected serverId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/exchange/adapter/Parser;Lcom/android/exchange/adapter/AbstractSyncAdapter;Z)V
    .locals 5
    .param p1, "parser"    # Lcom/android/exchange/adapter/Parser;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .param p3, "resumeStream"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 105
    invoke-direct {p0, p1, p3}, Lcom/android/exchange/adapter/AbstractUtiltyParser;-><init>(Lcom/android/exchange/adapter/Parser;Z)V

    .line 56
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    .line 61
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    .line 62
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    .line 65
    iput v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->interval:I

    .line 66
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    .line 67
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 71
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    .line 75
    const-string v0, "accountKey=? and type=?"

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->DELETE_MAILBOX_OF_TYPE:Ljava/lang/String;

    .line 170
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 106
    iput-object p2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    .line 107
    iget-object v0, p2, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    .line 108
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    .line 109
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    .line 111
    :cond_0
    iget-object v0, p2, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 112
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 115
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    instance-of v0, v0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    if-eqz v0, :cond_1

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    .line 117
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 121
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 85
    invoke-direct {p0, p1}, Lcom/android/exchange/adapter/AbstractUtiltyParser;-><init>(Ljava/io/InputStream;)V

    .line 56
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    .line 61
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    .line 62
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    .line 65
    iput v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->interval:I

    .line 66
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    .line 67
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 71
    iput-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    .line 75
    const-string v0, "accountKey=? and type=?"

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->DELETE_MAILBOX_OF_TYPE:Ljava/lang/String;

    .line 170
    iput-boolean v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 86
    iput-object p2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    .line 87
    iget-object v0, p2, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mService:Lcom/android/exchange/EasSyncService;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    .line 88
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    sget-object v0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 92
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 95
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    instance-of v0, v0, Lcom/android/exchange/adapter/SyncwithHBIAdapter;

    if-eqz v0, :cond_1

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    .line 97
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 101
    :cond_1
    return-void
.end method

.method private handleStatus()Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/16 v10, 0xd

    const/4 v9, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 393
    const/4 v1, 0x0

    .line 395
    .local v1, "moreAvailable":Z
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 398
    .local v0, "errorTemp":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v3

    .line 399
    .local v3, "status":I
    new-array v4, v8, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AbstractSyncParser.handleStatus(): status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {p0, v4}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 401
    if-eq v3, v8, :cond_1

    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v4}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v4

    if-nez v4, :cond_1

    .line 402
    const-string v4, "Sync failed: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 405
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 407
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    invoke-virtual {v0, v7, v4}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 410
    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    invoke-static {v3}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isBadSyncKey(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 413
    :cond_0
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    const-string v5, "0"

    invoke-virtual {v4, v5, v7}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 416
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    const/4 v5, -0x2

    iput v5, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 417
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    const-string v5, "Bad sync key; RESET and delete data"

    invoke-virtual {v4, v5}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 422
    const-string v4, "AbstractSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mailbox Id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v4, "AbstractSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mailbox Type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    const-string v4, "AbstractSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Account Id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v4}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->wipe()V

    .line 439
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad Sync Key for mailbox type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 515
    :cond_1
    :goto_0
    return v1

    .line 441
    :cond_2
    if-ne v3, v9, :cond_4

    .line 443
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5, v10}, Lcom/android/exchange/ExchangeService;->getRetryCount(JI)I

    move-result v2

    .line 444
    .local v2, "retrycount":I
    const-string v4, "AbstractSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got Status 5, retry Sync count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    if-le v2, v9, :cond_3

    .line 446
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    const-string v5, "0"

    invoke-virtual {v4, v5, v7}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 447
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got status 5(Server error); RESET syncKey to 0, mailbox: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 448
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v4}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->wipe()V

    .line 449
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->removeSyncError(J)V

    goto :goto_0

    .line 451
    :cond_3
    new-instance v4, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v4, v3}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v4

    .line 455
    .end local v2    # "retrycount":I
    :cond_4
    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    .line 464
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x61

    if-eq v4, v5, :cond_1

    .line 465
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto :goto_0

    .line 472
    :cond_5
    const/16 v4, 0xc

    if-ne v3, v4, :cond_6

    .line 473
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_12.1:status[12] - folder hierarchy has changed"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_0

    .line 476
    :cond_6
    if-ne v3, v10, :cond_7

    .line 477
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_12.1:status[13] - server can\'t process empty sync request"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 479
    :cond_7
    const/16 v4, 0xe

    if-ne v3, v4, :cond_8

    .line 480
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_12.1:status[14] - wait-interval is out of range"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 483
    :cond_8
    const/16 v4, 0xf

    if-ne v3, v4, :cond_9

    .line 484
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_12.1:status[15] - too many folders for server to moniter their changes"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 487
    :cond_9
    const/16 v4, 0x10

    if-ne v3, v4, :cond_a

    .line 488
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_12.1:status[16] - indeterminate state"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 492
    :cond_a
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/AbstractSyncParser;->isProvisioningStatus(I)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 493
    const-string v4, "AbstractSyncParser"

    const-string v5, "protocol_14.0:status[142/143/144] - Provisioning needed"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iput-boolean v8, v4, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 495
    new-instance v4, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v4, v3}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v4

    .line 499
    :cond_b
    invoke-virtual {p0, v3}, Lcom/android/exchange/adapter/AbstractSyncParser;->isDeviceAccessDenied(I)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 500
    const-string v4, "AbstractSyncParser"

    const-string v5, "AbstractSyncParser::parse() - Received status 129, to Block device "

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    new-instance v4, Lcom/android/emailcommon/utility/DeviceAccessException;

    const v5, 0x40001

    const v6, 0x7f060015

    invoke-direct {v4, v5, v6}, Lcom/android/emailcommon/utility/DeviceAccessException;-><init>(II)V

    throw v4

    .line 508
    :cond_c
    const-string v4, "AbstractSyncParser"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exchange response status"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    new-instance v4, Lcom/android/exchange/CommandStatusException;

    invoke-direct {v4, v3}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v4
.end method


# virtual methods
.method public abstract commandsParser()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation
.end method

.method public abstract commit()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public isLooping()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mLooping:Z

    return v0
.end method

.method protected mailBoxUpdate()V
    .locals 1

    .prologue
    .line 385
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->mailBoxUpdate(Z)V

    .line 386
    return-void
.end method

.method protected mailBoxUpdate(Z)V
    .locals 7
    .param p1, "forceUpdateMailbox"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 345
    const/4 v0, 0x0

    .line 349
    .local v0, "abortSyncs":Z
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    iget v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->interval:I

    if-eq v1, v2, :cond_6

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    const/4 v2, -0x4

    if-eq v1, v2, :cond_6

    .line 350
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    const-string v2, "syncInterval"

    iget-object v3, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 351
    iput-boolean v5, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 365
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->isWipeRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "Remove sync key from mailbox content values"

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 367
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    const-string v2, "syncKey"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 370
    :cond_1
    iget-boolean v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    invoke-virtual {v1}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 371
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 372
    if-nez p1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v1}, Lcom/android/exchange/EasSyncService;->isStopped()Z

    move-result v1

    if-nez v1, :cond_3

    .line 373
    :cond_2
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Apply mailbox updates"

    aput-object v4, v1, v3

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    invoke-virtual {v1, v3, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 376
    :cond_3
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    :cond_4
    if-eqz v0, :cond_5

    .line 380
    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "Aborting account syncs due to mailbox change to ping..."

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 381
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->stopAccountSyncs(J)V

    .line 383
    :cond_5
    return-void

    .line 354
    :cond_6
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    iget v1, v1, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v2, -0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    if-lez v1, :cond_0

    .line 357
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "Changes found to ping loop mailbox "

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, ": will ping."

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 358
    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    const-string v2, "syncInterval"

    const/4 v3, -0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 359
    iput-boolean v5, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 360
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 376
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public parse()Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v12, -0x2

    const/4 v11, -0x3

    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 173
    iput-boolean v9, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 174
    const/4 v7, 0x0

    .line 175
    .local v7, "newSyncKey":Z
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    iput v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->interval:I

    .line 176
    iput-boolean v9, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mLooping:Z

    .line 178
    invoke-virtual {p0, v9}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 179
    new-instance v0, Lcom/android/exchange/adapter/Parser$EasParserException;

    invoke-direct {v0, p0}, Lcom/android/exchange/adapter/Parser$EasParserException;-><init>(Lcom/android/exchange/adapter/Parser;)V

    throw v0

    .line 182
    :cond_0
    iput-boolean v9, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 183
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    .line 186
    :cond_1
    :goto_0
    invoke-virtual {p0, v9}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    if-eq v0, v10, :cond_15

    .line 194
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_2

    .line 196
    invoke-direct {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->handleStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    goto :goto_0

    .line 199
    :cond_2
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_1

    .line 200
    :cond_3
    :goto_1
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    if-eq v0, v10, :cond_1

    .line 201
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_14

    .line 202
    :cond_4
    :goto_2
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    if-eq v0, v10, :cond_3

    .line 203
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_5

    .line 205
    invoke-direct {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->handleStatus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 206
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    goto :goto_2

    .line 210
    :cond_5
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_d

    .line 212
    const-string v0, "saritha"

    const-string v1, "sync collection id"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 214
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->commit()V

    .line 215
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->mailBoxUpdate()V

    .line 216
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    if-eqz v0, :cond_6

    .line 217
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMoreAvailableSyncHBI:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    :cond_6
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    .line 223
    const-string v0, "saritha"

    const-string v1, "sync collection id"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "saritha"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sync collection id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->serverId:Ljava/lang/String;

    aput-object v1, v0, v8

    .line 226
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    if-eqz v0, :cond_4

    .line 228
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_PROJECTION:[Ljava/lang/String;

    const-string v3, "accountKey=? and serverId=?"

    iget-object v4, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mBindArguments:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 233
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_7

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 234
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 236
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput-object v1, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 237
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    iput v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->interval:I

    .line 238
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    const/4 v1, 0x0

    iput v1, v0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 242
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    .line 243
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Parsed key for "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ": "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 250
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->resetParser()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    :cond_7
    if-eqz v6, :cond_8

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_8

    .line 255
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 258
    :cond_8
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 259
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 263
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 265
    :cond_9
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 266
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 267
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    const-string v1, "syncKey"

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 269
    const/4 v7, 0x1

    .line 273
    :cond_a
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    if-ne v0, v12, :cond_b

    .line 274
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput v11, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 277
    :cond_b
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    goto/16 :goto_2

    .line 254
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_c

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_c

    .line 255
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v0

    .line 280
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_d
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_e

    .line 281
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->commandsParser()V

    goto/16 :goto_2

    .line 282
    :cond_e
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_f

    .line 283
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->responsesParser()V

    goto/16 :goto_2

    .line 284
    :cond_f
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_10

    .line 285
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    goto/16 :goto_2

    .line 286
    :cond_10
    iget v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_13

    .line 287
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    .line 290
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->multiFolder:Z

    if-nez v0, :cond_4

    .line 291
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    invoke-virtual {v0}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 292
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    .line 297
    :cond_11
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Parsed key for "

    aput-object v1, v0, v9

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v1, v0, v8

    const/4 v1, 0x2

    const-string v2, ": "

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    aput-object v1, v0, v10

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 299
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v8}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setSyncKey(Ljava/lang/String;Z)V

    .line 300
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->cv:Landroid/content/ContentValues;

    const-string v1, "syncKey"

    iget-object v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->newKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mailboxUpdated:Z

    .line 302
    const/4 v7, 0x1

    .line 307
    :cond_12
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    if-ne v0, v12, :cond_4

    .line 308
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iput v11, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    goto/16 :goto_2

    .line 312
    :cond_13
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto/16 :goto_2

    .line 316
    :cond_14
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto/16 :goto_1

    .line 323
    :cond_15
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    if-eqz v0, :cond_16

    if-nez v7, :cond_16

    .line 324
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "!! SyncKey hasn\'t changed, setting moreAvailable = false"

    aput-object v1, v0, v9

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 325
    iput-boolean v8, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mLooping:Z

    .line 329
    :cond_16
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->commit()V

    .line 331
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->mailBoxUpdate()V

    .line 332
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    if-eqz v0, :cond_17

    .line 333
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mAdapter:Lcom/android/exchange/adapter/AbstractSyncAdapter;

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMoreAvailableSyncHBI:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_17
    new-array v0, v8, [Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Returning moreAvailable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-virtual {p0, v0}, Lcom/android/exchange/adapter/AbstractSyncParser;->userLog([Ljava/lang/String;)V

    .line 340
    iget-boolean v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->moreAvailable:Z

    return v0
.end method

.method protected resetParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 532
    return-void
.end method

.method public abstract responsesParser()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public skipParser(I)V
    .locals 2
    .param p1, "endTag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    goto :goto_0

    .line 160
    :cond_0
    return-void
.end method

.method userLog(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "num"    # I
    .param p3, "string2"    # Ljava/lang/String;

    .prologue
    .line 524
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;ILjava/lang/String;)V

    .line 525
    return-void
.end method

.method varargs userLog([Ljava/lang/String;)V
    .locals 1
    .param p1, "strings"    # [Ljava/lang/String;

    .prologue
    .line 520
    iget-object v0, p0, Lcom/android/exchange/adapter/AbstractSyncParser;->mService:Lcom/android/exchange/EasSyncService;

    invoke-virtual {v0, p1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 521
    return-void
.end method
