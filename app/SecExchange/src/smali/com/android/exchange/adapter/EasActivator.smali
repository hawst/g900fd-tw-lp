.class public Lcom/android/exchange/adapter/EasActivator;
.super Ljava/lang/Object;
.source "EasActivator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/adapter/EasActivator$ASAPullParser;,
        Lcom/android/exchange/adapter/EasActivator$ASABaseParser;,
        Lcom/android/exchange/adapter/EasActivator$IASAParser;
    }
.end annotation


# static fields
.field static final Xmlreq_elem:[Ljava/lang/String;

.field private static mChecksum:Ljava/lang/String;

.field private static mCountryCode:Ljava/lang/String;

.field private static mDeviceModelName:Ljava/lang/String;

.field private static mImei:Ljava/lang/String;

.field private static mMsisdnNumber:Ljava/lang/String;

.field private static mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private static mUTCTimestamp:Ljava/lang/String;

.field private static xmlReqBuffer:Ljava/lang/StringBuffer;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 403
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "<?xml version=\"1.0\" ?>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "<methodCall>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "<methodName>activesync.activate</methodName>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "<params>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "<param>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "<value>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<struct>\n"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "<name>imei</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "<name>msisdn</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "<name>mcc</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "<name>model_name</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "<name>timestamp</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "<name>email</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "<name>live</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "<name>checksum</name>\n"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "</struct>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "</value>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "</param>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "</params>\n"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "</methodCall>"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    .line 77
    iput-object p1, p0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    .line 78
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    sput-object v0, Lcom/android/exchange/adapter/EasActivator;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 79
    return-void
.end method

.method static buffer_append_elem(ILjava/lang/String;)V
    .locals 2
    .param p0, "i"    # I
    .param p1, "a"    # Ljava/lang/String;

    .prologue
    .line 413
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    const-string v1, "<member>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 414
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    aget-object v1, v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 415
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    const-string v1, "<value>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 416
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 417
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    const-string v1, "</value>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 418
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    const-string v1, "</member>\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 419
    return-void
.end method

.method static byteToHex(B)Ljava/lang/String;
    .locals 4
    .param p0, "b"    # B

    .prologue
    .line 351
    const/16 v2, 0x10

    new-array v1, v2, [C

    fill-array-data v1, :array_0

    .line 354
    .local v1, "hexDigit":[C
    const/4 v2, 0x2

    new-array v0, v2, [C

    const/4 v2, 0x0

    shr-int/lit8 v3, p0, 0x4

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v1, v3

    aput-char v3, v0, v2

    const/4 v2, 0x1

    and-int/lit8 v3, p0, 0xf

    aget-char v3, v1, v3

    aput-char v3, v0, v2

    .line 357
    .local v0, "array":[C
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2

    .line 351
    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method static bytesToHex([B)Ljava/lang/String;
    .locals 3
    .param p0, "b"    # [B

    .prologue
    .line 343
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 344
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 345
    aget-byte v2, p0, v1

    invoke-static {v2}, Lcom/android/exchange/adapter/EasActivator;->byteToHex(B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 346
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method static computeHmacSha1(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/GeneralSecurityException;
        }
    .end annotation

    .prologue
    .line 370
    const/4 v1, 0x0

    .line 371
    .local v1, "digest":Ljava/lang/String;
    const-string v0, "HmacSha1"

    .line 372
    .local v0, "algorithm":Ljava/lang/String;
    const/4 v3, 0x0

    .line 375
    .local v3, "digestByteArray":[B
    :try_start_0
    const-string v9, "UTF8"

    invoke-virtual {p0, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 383
    .local v5, "keyByteArray":[B
    :try_start_1
    const-string v9, "UTF8"

    invoke-virtual {p1, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 391
    .local v7, "messageByteArray":[B
    new-instance v8, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v8, v5, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 394
    .local v8, "secretkey":Ljavax/crypto/spec/SecretKeySpec;
    invoke-static {v0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v6

    .line 395
    .local v6, "mac":Ljavax/crypto/Mac;
    invoke-virtual {v6, v8}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 397
    invoke-virtual {v6, v7}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v3

    .line 398
    invoke-static {v3}, Lcom/android/exchange/adapter/EasActivator;->bytesToHex([B)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 400
    .end local v1    # "digest":Ljava/lang/String;
    .end local v5    # "keyByteArray":[B
    .end local v6    # "mac":Ljavax/crypto/Mac;
    .end local v7    # "messageByteArray":[B
    .end local v8    # "secretkey":Ljavax/crypto/spec/SecretKeySpec;
    .local v2, "digest":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 376
    .end local v2    # "digest":Ljava/lang/String;
    .restart local v1    # "digest":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 377
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 378
    const-string v9, "ExchangeActivation"

    const-string v10, "computeHmacSha1 cannot get key byte"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 379
    .end local v1    # "digest":Ljava/lang/String;
    .restart local v2    # "digest":Ljava/lang/String;
    goto :goto_0

    .line 384
    .end local v2    # "digest":Ljava/lang/String;
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v1    # "digest":Ljava/lang/String;
    .restart local v5    # "keyByteArray":[B
    :catch_1
    move-exception v4

    .line 385
    .restart local v4    # "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 386
    const-string v9, "ExchangeActivation"

    const-string v10, "computeHmacSha1 cannot get message byte"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 387
    .end local v1    # "digest":Ljava/lang/String;
    .restart local v2    # "digest":Ljava/lang/String;
    goto :goto_0
.end method

.method private getChecksum()Z
    .locals 6

    .prologue
    .line 314
    const/4 v3, 0x0

    .line 315
    .local v3, "noError":Z
    const/4 v4, 0x0

    sput-object v4, Lcom/android/exchange/adapter/EasActivator;->mChecksum:Ljava/lang/String;

    .line 322
    new-instance v1, Ljava/lang/StringBuffer;

    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 323
    .local v1, "messageBuffer":Ljava/lang/StringBuffer;
    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 324
    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 325
    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mDeviceModelName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 326
    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mUTCTimestamp:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 327
    const-string v4, "user@companyx.com"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    const-string v4, "Y"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 329
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 332
    .local v2, "messageValue":Ljava/lang/String;
    :try_start_0
    const-string v4, "procuring nay end happiness allowance assurance frankness"

    invoke-static {v4, v2}, Lcom/android/exchange/adapter/EasActivator;->computeHmacSha1(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/exchange/adapter/EasActivator;->mChecksum:Ljava/lang/String;

    .line 333
    const-string v4, "ExchangeActivation"

    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mChecksum:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    sget-object v4, Lcom/android/exchange/adapter/EasActivator;->mChecksum:Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_0

    .line 335
    const/4 v3, 0x1

    .line 339
    :cond_0
    :goto_0
    return v3

    .line 336
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Ljava/security/GeneralSecurityException;
    const-string v4, "ExchangeActivation"

    const-string v5, "getChecksum cannot complete"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDate()V
    .locals 3

    .prologue
    .line 175
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 176
    .local v1, "lv_localDate":Ljava/util/Date;
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyMMddHHmmss"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "lv_formatter_UTC":Ljava/text/SimpleDateFormat;
    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 178
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/exchange/adapter/EasActivator;->mUTCTimestamp:Ljava/lang/String;

    .line 179
    return-void
.end method

.method private getDeviceInfo()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    .line 114
    const/4 v1, 0x0

    .line 116
    .local v1, "noError":Z
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mDeviceModelName:Ljava/lang/String;

    .line 117
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasActivator;->getDate()V

    .line 122
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    .line 124
    .local v3, "phoneType":I
    iget-object v5, p0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/emailcommon/utility/Utility;->isNonPhone(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 132
    const-string v5, "ril.serialnumber"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    .line 134
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_0

    .line 135
    const-string v5, "ExchangeActivation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wifi only device, hardware_id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_0
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    .line 149
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 150
    const-string v5, "000000000000"

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    .line 151
    :cond_1
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 152
    :cond_2
    const-string v4, "ExchangeActivation"

    const-string v5, "getDeviceInfo - Can not get device info imei or isdn"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .end local v1    # "noError":Z
    :goto_1
    return v1

    .line 139
    .restart local v1    # "noError":Z
    :cond_3
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    .line 140
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_0

    .line 141
    const-string v5, "ExchangeActivation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Phone device, mImei : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 156
    :cond_4
    :try_start_0
    sget-object v5, Lcom/android/exchange/adapter/EasActivator;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "operatorInfo":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 161
    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mCountryCode:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    move v1, v4

    .line 170
    goto :goto_1

    .line 163
    .end local v2    # "operatorInfo":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "ExchangeActivation"

    const-string v6, "getDeviceInfo - Can not get country code!"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v5, "000"

    sput-object v5, Lcom/android/exchange/adapter/EasActivator;->mCountryCode:Ljava/lang/String;

    move v1, v4

    .line 167
    goto :goto_1
.end method

.method private getLicenseKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 21
    .param p1, "urlServerToConnect"    # Ljava/lang/String;
    .param p2, "xmlContentToSend"    # Ljava/lang/String;

    .prologue
    .line 190
    const/4 v11, 0x0

    .line 191
    .local v11, "noError":Z
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 192
    .local v7, "httpclient":Lorg/apache/http/client/HttpClient;
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 194
    .local v8, "httppost":Lorg/apache/http/client/methods/HttpPost;
    const/4 v15, 0x0

    .line 195
    .local v15, "response":Lorg/apache/http/HttpResponse;
    const/4 v9, 0x0

    .line 196
    .local v9, "instream":Ljava/io/InputStream;
    const-string v10, "unknown"

    .line 198
    .local v10, "license_key":Ljava/lang/String;
    const-string v18, "content-type"

    const-string v19, "application/x-www-form-urlencoded"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v18, "user-agent"

    const-string v19, "text/xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v18, "accept"

    const-string v19, "text/xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :try_start_0
    new-instance v6, Lorg/apache/http/entity/StringEntity;

    const-string v18, "UTF-8"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-direct {v6, v0, v1}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .local v6, "entity":Lorg/apache/http/entity/StringEntity;
    const-string v18, "application/xml"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 205
    invoke-virtual {v8, v6}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    .line 210
    .local v13, "proxyHost":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v14

    .line 211
    .local v14, "proxyPort":I
    const-string v18, "PROXY1"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Proxy Host: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v18, "PROXY1"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Proxy port: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_0

    if-ltz v14, :cond_0

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/adapter/EasActivator;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-static {v0, v7, v8, v13, v14}, Lcom/android/exchange/EasSyncService;->addProxyParamsIfProxySet(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V

    .line 219
    :cond_0
    invoke-interface {v7, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 228
    .end local v6    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v13    # "proxyHost":Ljava/lang/String;
    .end local v14    # "proxyPort":I
    :goto_0
    if-eqz v15, :cond_3

    .line 229
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    .line 230
    .local v17, "statusInfo":Ljava/lang/String;
    const-string v18, "ExchangeActivation"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 232
    .local v3, "code":I
    const-string v18, "ExchangeActivation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Device activation response: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/16 v18, 0xc8

    move/from16 v0, v18

    if-ne v3, v0, :cond_5

    .line 235
    const-string v18, "Content-Length"

    move-object/from16 v0, v18

    invoke-interface {v15, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    .line 236
    .local v4, "content_len":Lorg/apache/http/Header;
    if-eqz v4, :cond_1

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v18

    const-string v19, "0"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 237
    :cond_1
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - http response has no content"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const/16 v18, 0x0

    .line 273
    .end local v3    # "code":I
    .end local v4    # "content_len":Lorg/apache/http/Header;
    .end local v17    # "statusInfo":Ljava/lang/String;
    :goto_1
    return-object v18

    .line 220
    :catch_0
    move-exception v5

    .line 221
    .local v5, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 222
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - http post ClientProtocolException"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 223
    .end local v5    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v5

    .line 224
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 225
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - http post IOException"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "code":I
    .restart local v4    # "content_len":Lorg/apache/http/Header;
    .restart local v17    # "statusInfo":Ljava/lang/String;
    :cond_2
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - http response status = 200"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :try_start_1
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    .line 246
    .local v6, "entity":Lorg/apache/http/HttpEntity;
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v9

    .line 247
    new-instance v12, Lcom/android/exchange/adapter/EasActivator$ASAPullParser;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/android/exchange/adapter/EasActivator$ASAPullParser;-><init>(Lcom/android/exchange/adapter/EasActivator;)V

    .line 248
    .local v12, "parser":Lcom/android/exchange/adapter/EasActivator$ASAPullParser;
    invoke-virtual {v12, v9}, Lcom/android/exchange/adapter/EasActivator$ASAPullParser;->parse(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v16

    .line 249
    .local v16, "responseString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Ljava/lang/String;

    move-object v10, v0

    .line 250
    const/16 v18, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 251
    .local v2, "checksum":Ljava/lang/String;
    const-string v18, "ExchangeActivation"

    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    const-string v18, "ExchangeActivation"

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/android/exchange/adapter/EasActivator;->verifyLicenseLey(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 254
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - Licensekey verification success."

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 255
    const/4 v11, 0x1

    .line 269
    .end local v2    # "checksum":Ljava/lang/String;
    .end local v3    # "code":I
    .end local v4    # "content_len":Lorg/apache/http/Header;
    .end local v6    # "entity":Lorg/apache/http/HttpEntity;
    .end local v12    # "parser":Lcom/android/exchange/adapter/EasActivator$ASAPullParser;
    .end local v16    # "responseString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v17    # "statusInfo":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 270
    if-eqz v11, :cond_6

    move-object/from16 v18, v10

    .line 271
    goto :goto_1

    .line 257
    .restart local v2    # "checksum":Ljava/lang/String;
    .restart local v3    # "code":I
    .restart local v4    # "content_len":Lorg/apache/http/Header;
    .restart local v6    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v12    # "parser":Lcom/android/exchange/adapter/EasActivator$ASAPullParser;
    .restart local v16    # "responseString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v17    # "statusInfo":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - Licensekey verification failed."

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 259
    .end local v2    # "checksum":Ljava/lang/String;
    .end local v6    # "entity":Lorg/apache/http/HttpEntity;
    .end local v12    # "parser":Lcom/android/exchange/adapter/EasActivator$ASAPullParser;
    .end local v16    # "responseString":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_2
    move-exception v5

    .line 260
    .local v5, "e":Ljava/lang/Exception;
    const-string v18, "ExchangeActivation"

    const-string v19, "getLicenseKey - http response has no license"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 264
    .end local v4    # "content_len":Lorg/apache/http/Header;
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v18, "ExchangeActivation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getLicenseKey - http response status "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 273
    .end local v3    # "code":I
    .end local v17    # "statusInfo":Ljava/lang/String;
    :cond_6
    const/16 v18, 0x0

    goto/16 :goto_1
.end method

.method private verifyLicenseLey(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "licensekeyStr"    # Ljava/lang/String;
    .param p2, "checksumStr"    # Ljava/lang/String;

    .prologue
    .line 277
    const/4 v3, 0x0

    .line 279
    .local v3, "noError":Z
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v4, v3

    .line 303
    .end local v3    # "noError":Z
    .local v4, "noError":I
    :goto_0
    return v4

    .line 282
    .end local v4    # "noError":I
    .restart local v3    # "noError":Z
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 285
    .local v1, "licensekey":[B
    if-eqz v1, :cond_2

    .line 288
    :try_start_0
    const-string v5, "procuring nay end happiness allowance assurance frankness"

    invoke-static {v5, p1}, Lcom/android/exchange/adapter/EasActivator;->computeHmacSha1(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 289
    .local v2, "myChecksum":Ljava/lang/String;
    const-string v5, "ExchangeActivation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "server "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    const-string v5, "ExchangeActivation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "device "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 297
    const-string v5, "ExchangeActivation"

    const-string v6, "verify_licensekey - verify license key success"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const/4 v3, 0x1

    .end local v2    # "myChecksum":Ljava/lang/String;
    :cond_2
    :goto_1
    move v4, v3

    .line 303
    .restart local v4    # "noError":I
    goto :goto_0

    .line 291
    .end local v4    # "noError":I
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Ljava/security/GeneralSecurityException;
    const-string v5, "ExchangeActivation"

    const-string v6, "verify_licensekey - can not complete computeHmacSha1"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 293
    .restart local v4    # "noError":I
    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/security/GeneralSecurityException;
    .end local v4    # "noError":I
    .restart local v2    # "myChecksum":Ljava/lang/String;
    :cond_3
    const-string v5, "ExchangeActivation"

    const-string v6, "verify_licensekey - verify license key fail"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static xmlReqBuffer_setup()Ljava/lang/String;
    .locals 3

    .prologue
    .line 428
    new-instance v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    .line 429
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 432
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 433
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 434
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 435
    const/4 v0, 0x7

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mImei:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 436
    const/16 v0, 0x8

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mMsisdnNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 437
    const/16 v0, 0x9

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mCountryCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 438
    const/16 v0, 0xa

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mDeviceModelName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 439
    const/16 v0, 0xb

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mUTCTimestamp:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 440
    const/16 v0, 0xc

    const-string v1, "user@companyx.com"

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 441
    const/16 v0, 0xd

    const-string v1, "Y"

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 442
    const/16 v0, 0xe

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->mChecksum:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/exchange/adapter/EasActivator;->buffer_append_elem(ILjava/lang/String;)V

    .line 443
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 444
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 445
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 446
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 447
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    sget-object v1, Lcom/android/exchange/adapter/EasActivator;->Xmlreq_elem:[Ljava/lang/String;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 449
    sget-object v0, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public actionActivateDevice()Ljava/lang/String;
    .locals 4

    .prologue
    .line 90
    const-string v1, "https://api.samsungapps.com/activesync/activate/activesync"

    .line 91
    .local v1, "urlServerToConnect":Ljava/lang/String;
    const/4 v0, 0x0

    .line 94
    .local v0, "license":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasActivator;->getDeviceInfo()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 95
    invoke-direct {p0}, Lcom/android/exchange/adapter/EasActivator;->getChecksum()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 96
    invoke-static {}, Lcom/android/exchange/adapter/EasActivator;->xmlReqBuffer_setup()Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "xmlContentToSend":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 98
    invoke-direct {p0, v1, v2}, Lcom/android/exchange/adapter/EasActivator;->getLicenseKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .end local v2    # "xmlContentToSend":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :cond_1
    const-string v0, "NoDeviceID"

    goto :goto_0
.end method
