.class public Lcom/android/exchange/irm/IRMSettingsAdapter;
.super Lcom/android/exchange/adapter/AbstractSyncAdapter;
.source "IRMSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;
    }
.end annotation


# instance fields
.field acc:Lcom/android/emailcommon/provider/EmailContent$Account;

.field public mIrmStatus:I


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V
    .locals 1
    .param p1, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "service"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 52
    invoke-direct {p0, p2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 53
    iget-object v0, p2, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 54
    return-void
.end method


# virtual methods
.method public buildSettingsRequest()Lcom/android/exchange/adapter/Serializer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 92
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v1, 0x485

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x4ab

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x487

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 94
    return-object v0
.end method

.method public cleanup()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method public getCollectionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "Email"

    return-object v0
.end method

.method public isSyncable()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Z
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    new-instance v0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;

    invoke-direct {v0, p0, p1, p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;-><init>(Lcom/android/exchange/irm/IRMSettingsAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 64
    .local v0, "p":Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;
    invoke-virtual {v0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->parse()Z

    move-result v1

    .line 65
    .local v1, "success":Z
    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->commit()V

    .line 67
    :cond_0
    return v1
.end method

.method public sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z
    .locals 1
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V
    .locals 0
    .param p1, "protocolVersion"    # Ljava/lang/Double;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p3, "initialSync"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    return-void
.end method

.method public wipe()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method
