.class Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;
.super Lcom/android/exchange/adapter/AbstractSyncParser;
.source "IRMSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/irm/IRMSettingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IRMSettingsParser"
.end annotation


# instance fields
.field mStatus:I

.field mTemplate:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;


# direct methods
.method public constructor <init>(Lcom/android/exchange/irm/IRMSettingsAdapter;Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 0
    .param p2, "in"    # Ljava/io/InputStream;
    .param p3, "adapter"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    .line 108
    invoke-direct {p0, p2, p3}, Lcom/android/exchange/adapter/AbstractSyncParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 110
    return-void
.end method

.method private checkIRMSettingStatus(I)Z
    .locals 3
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 242
    const/16 v1, 0xa8

    if-ne p1, v1, :cond_0

    .line 243
    const-string v1, "IRMSettingsParser"

    const-string v2, "IRM feature is disabled"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :goto_0
    return v0

    .line 246
    :cond_0
    const/16 v1, 0xa9

    if-ne p1, v1, :cond_1

    .line 247
    const-string v1, "IRMSettingsParser"

    const-string v2, "IRM encountered an error"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 249
    :cond_1
    const/16 v1, 0xaa

    if-ne p1, v1, :cond_2

    .line 250
    const-string v1, "IRMSettingsParser"

    const-string v2, "IRM encountered permanent error"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getArrayListOfTemplates()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    return-object v0
.end method

.method private parseIRMStatus()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 199
    :cond_0
    :goto_0
    const/16 v0, 0x4ab

    invoke-virtual {p0, v0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->nextTag(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    .line 200
    iget v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v1, 0x486

    if-ne v0, v1, :cond_1

    .line 201
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getValueInt()I

    move-result v1

    iput v1, v0, Lcom/android/exchange/irm/IRMSettingsAdapter;->mIrmStatus:I

    .line 202
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget v0, v0, Lcom/android/exchange/irm/IRMSettingsAdapter;->mIrmStatus:I

    invoke-direct {p0, v0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->checkIRMSettingStatus(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 208
    :goto_1
    return v0

    .line 204
    :cond_1
    iget v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v1, 0x606

    if-ne v0, v1, :cond_0

    .line 205
    invoke-direct {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->parseTemplates()V

    goto :goto_0

    .line 208
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private parseTemplates()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x607

    const/4 v3, 0x3

    .line 216
    invoke-direct {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getArrayListOfTemplates()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    .line 217
    iget-object v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 218
    invoke-direct {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->setArrayListOfTemplates()V

    .line 221
    :cond_0
    :goto_0
    const/16 v1, 0x606

    invoke-virtual {p0, v1}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->nextTag(I)I

    move-result v1

    if-eq v1, v3, :cond_5

    .line 222
    iget v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    if-ne v1, v4, :cond_0

    .line 223
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;-><init>()V

    .line 224
    .local v0, "temp":Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;
    :goto_1
    invoke-virtual {p0, v4}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->nextTag(I)I

    move-result v1

    if-eq v1, v3, :cond_4

    .line 226
    iget v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v2, 0x614

    if-ne v1, v2, :cond_1

    .line 227
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateId:Ljava/lang/String;

    goto :goto_1

    .line 228
    :cond_1
    iget v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v2, 0x615

    if-ne v1, v2, :cond_2

    .line 229
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateName:Ljava/lang/String;

    goto :goto_1

    .line 230
    :cond_2
    iget v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v2, 0x616

    if-ne v1, v2, :cond_3

    .line 231
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateDescription:Ljava/lang/String;

    goto :goto_1

    .line 233
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->skipTag()V

    goto :goto_1

    .line 235
    :cond_4
    iget-object v1, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    .end local v0    # "temp":Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;
    :cond_5
    return-void
.end method

.method private setArrayListOfTemplates()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mTemplate:Ljava/util/ArrayList;

    .line 178
    :cond_0
    return-void
.end method


# virtual methods
.method public commandsParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    return-void
.end method

.method public commit()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x0

    .line 126
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->CONTENT_URI:Landroid/net/Uri;

    .line 127
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 129
    .local v6, "checkCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "IRMTemplateId"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AccountKey = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget-object v4, v4, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 132
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AccountKey = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget-object v3, v3, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 135
    .local v8, "count":I
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_0

    .line 136
    const-string v0, "IRM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IRMSettingAdapter: deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " old IRMTemplates in DB "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v8    # "count":I
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 142
    :cond_1
    invoke-direct {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getArrayListOfTemplates()Ljava/util/ArrayList;

    move-result-object v13

    .line 143
    .local v13, "template":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    if-eqz v13, :cond_5

    .line 144
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 145
    .local v9, "listIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    :cond_2
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 146
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 147
    .local v7, "contentValues":Landroid/content/ContentValues;
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;

    .line 148
    .local v12, "t":Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;
    const-string v0, "IRMTemplateId"

    iget-object v2, v12, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateId:Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "IRMTemplateName"

    iget-object v2, v12, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateName:Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "IRMTemplateDescription"

    iget-object v2, v12, Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;->mIRMTemplateDescription:Ljava/lang/String;

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "AccountKey"

    iget-object v2, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget-object v2, v2, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 153
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    .line 154
    .local v8, "count":Landroid/net/Uri;
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_2

    .line 155
    const-string v0, "IRM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IRMSettingAdapter: inserting"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IRMTemplates in DB"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 140
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v8    # "count":Landroid/net/Uri;
    .end local v9    # "listIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    .end local v12    # "t":Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;
    .end local v13    # "template":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 158
    .restart local v9    # "listIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    .restart local v13    # "template":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 159
    .local v10, "currentTime":J
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 160
    .restart local v7    # "contentValues":Landroid/content/ContentValues;
    const-string v0, "IRMTemplateTimeStamp"

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->this$0:Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget-object v4, v4, Lcom/android/exchange/irm/IRMSettingsAdapter;->acc:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v7, v3, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 164
    .end local v7    # "contentValues":Landroid/content/ContentValues;
    .end local v9    # "listIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/emailcommon/provider/EmailContent$IRMTemplate;>;"
    .end local v10    # "currentTime":J
    :cond_5
    return-void
.end method

.method public parse()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    const/16 v2, 0x485

    invoke-virtual {p0, v2}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->nextTag(I)I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 182
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->nextTag(I)I

    move-result v2

    if-eq v2, v4, :cond_1

    .line 183
    iget v2, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v3, 0x486

    if-ne v2, v3, :cond_2

    .line 184
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->getValueInt()I

    move-result v2

    iput v2, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mStatus:I

    .line 186
    iget v2, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->mStatus:I

    if-eq v2, v0, :cond_0

    move v0, v1

    .line 195
    :cond_1
    :goto_1
    return v0

    .line 188
    :cond_2
    iget v2, p0, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->tag:I

    const/16 v3, 0x4ab

    if-ne v2, v3, :cond_3

    .line 189
    invoke-direct {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->parseIRMStatus()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 190
    goto :goto_1

    .line 192
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/irm/IRMSettingsAdapter$IRMSettingsParser;->skipTag()V

    goto :goto_0
.end method

.method public responsesParser()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 122
    return-void
.end method
