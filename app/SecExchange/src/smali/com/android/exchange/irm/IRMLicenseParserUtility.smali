.class public Lcom/android/exchange/irm/IRMLicenseParserUtility;
.super Ljava/lang/Object;
.source "IRMLicenseParserUtility.java"


# static fields
.field public static mRenewLicense:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/exchange/irm/IRMLicenseParserUtility;->mRenewLicense:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I
    .locals 1
    .param p0, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .param p1, "flag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .end local p1    # "flag":I
    :cond_0
    return p1
.end method

.method public static getIRMLicenseFlag(Landroid/content/Context;J)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "msgId"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 155
    const/4 v7, -0x1

    .line 156
    .local v7, "flag":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "IRMLicenseFlag"

    aput-object v3, v2, v8

    const-string v3, "_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 162
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 163
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 164
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 165
    const-string v0, "IRMLicenseFlag"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 167
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 169
    :cond_1
    return v7
.end method

.method public static getIRMRemovalFlag(Landroid/content/Context;J)I
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "msgId"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 179
    const/4 v7, -0x1

    .line 180
    .local v7, "flag":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "IRMRemovalFlag"

    aput-object v3, v2, v8

    const-string v3, "_id=?"

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 186
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 187
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 189
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 190
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 192
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 194
    :cond_1
    return v7
.end method

.method public static parseLicense(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/exchange/adapter/AbstractSyncParser;)V
    .locals 6
    .param p0, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p1, "parser"    # Lcom/android/exchange/adapter/AbstractSyncParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 54
    iput v3, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    .line 55
    iput v5, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMRemovalFlag:I

    .line 57
    :goto_0
    const/16 v1, 0x608

    invoke-virtual {p1, v1}, Lcom/android/exchange/adapter/AbstractSyncParser;->nextTag(I)I

    move-result v1

    const/4 v4, 0x3

    if-eq v1, v4, :cond_1

    .line 58
    const/4 v0, 0x0

    .line 59
    .local v0, "_flag":I
    iget v1, p1, Lcom/android/exchange/adapter/AbstractSyncParser;->tag:I

    packed-switch v1, :pswitch_data_0

    .line 122
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->skipTag()V

    .line 124
    :goto_1
    iget v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMLicenseFlag:I

    goto :goto_0

    .line 61
    :pswitch_0
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValueInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMOwner:I

    goto :goto_1

    .line 65
    :pswitch_1
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateId:Ljava/lang/String;

    goto :goto_1

    .line 69
    :pswitch_2
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentOwner:Ljava/lang/String;

    goto :goto_1

    .line 73
    :pswitch_3
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 74
    goto :goto_1

    .line 77
    :pswitch_4
    invoke-static {p1, v2}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 78
    goto :goto_1

    .line 81
    :pswitch_5
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 82
    goto :goto_1

    .line 85
    :pswitch_6
    invoke-static {p1, v5}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 86
    goto :goto_1

    .line 89
    :pswitch_7
    const/16 v1, 0x10

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 90
    goto :goto_1

    .line 93
    :pswitch_8
    const/16 v1, 0x20

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 94
    goto :goto_1

    .line 97
    :pswitch_9
    const/16 v1, 0x80

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 98
    goto :goto_1

    .line 101
    :pswitch_a
    const/16 v1, 0x40

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 102
    goto :goto_1

    .line 105
    :pswitch_b
    const/16 v1, 0x100

    invoke-static {p1, v1}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->addFlag(Lcom/android/exchange/adapter/AbstractSyncParser;I)I

    move-result v0

    .line 106
    goto :goto_1

    .line 109
    :pswitch_c
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    .line 110
    iget-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMContentExpiryDate:Ljava/lang/String;

    const-string v4, "12082020t6789407z"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_2
    sput-boolean v1, Lcom/android/exchange/irm/IRMLicenseParserUtility;->mRenewLicense:Z

    goto :goto_1

    :cond_0
    move v1, v3

    goto :goto_2

    .line 114
    :pswitch_d
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateName:Ljava/lang/String;

    goto :goto_1

    .line 118
    :pswitch_e
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractSyncParser;->getValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/emailcommon/provider/EmailContent$Message;->mIRMTemplateDescription:Ljava/lang/String;

    goto :goto_1

    .line 126
    .end local v0    # "_flag":I
    :cond_1
    return-void

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x609
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_1
        :pswitch_d
        :pswitch_e
        :pswitch_2
    .end packed-switch
.end method

.method public static renewLicense(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p0, "mExpiryDate"    # Ljava/lang/String;
    .param p1, "mMessageId"    # Ljava/lang/String;
    .param p2, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 138
    const-string v5, "IRMParserUtility"

    const-string v6, "Inside renew license if IRM"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 140
    .local v0, "expiryIntent":Landroid/content/Intent;
    const-string v5, "expiry"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v5, "MessageId"

    invoke-virtual {v0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    invoke-static {p2, v7, v0, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 143
    .local v4, "sender":Landroid/app/PendingIntent;
    const-string v5, "alarm"

    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 144
    .local v1, "manager":Landroid/app/AlarmManager;
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v2

    .line 145
    .local v2, "mExpiryDateInMillis":J
    invoke-virtual {v1, v7, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 146
    return-void
.end method
