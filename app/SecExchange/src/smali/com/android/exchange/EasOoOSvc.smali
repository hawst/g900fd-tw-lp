.class public Lcom/android/exchange/EasOoOSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasOoOSvc.java"


# instance fields
.field private mEndDate:Ljava/util/Date;

.field private mExternalKnownMsg:Ljava/lang/String;

.field private mExternalUnKnownMsg:Ljava/lang/String;

.field private mInternalMsg:Ljava/lang/String;

.field private mIsExtKnown:Z

.field private mIsExtUnKnown:Z

.field private mIsGlobal:Z

.field private mIsInternal:Z

.field private mIsTimeBased:Z

.field private mStartDate:Ljava/util/Date;

.field private mSvcData:Lcom/android/emailcommon/service/OoODataList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/service/OoODataList;)V
    .locals 4
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p3, "data"    # Lcom/android/emailcommon/service/OoODataList;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 80
    iput-object p3, p0, Lcom/android/exchange/EasOoOSvc;->mSvcData:Lcom/android/emailcommon/service/OoODataList;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    iput-boolean v0, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    iput-boolean v0, p0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    iput-boolean v0, p0, Lcom/android/exchange/EasOoOSvc;->mIsGlobal:Z

    iput-boolean v0, p0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    .line 82
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mStartDate:Ljava/util/Date;

    .line 83
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mEndDate:Ljava/util/Date;

    .line 85
    iget-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mProtocolVersion:Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 89
    iget-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    sget-wide v2, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_OoO:J

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    .line 90
    iget-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v0, p0, Lcom/android/exchange/EasOoOSvc;->mMailboxId:J

    .line 91
    iget-object v0, p0, Lcom/android/exchange/EasOoOSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    const-string v1, "OoO"

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 93
    return-void
.end method

.method private convertLocalToUTC(J)Ljava/lang/String;
    .locals 9
    .param p1, "millis"    # J

    .prologue
    const/4 v8, 0x0

    .line 96
    new-instance v5, Landroid/text/format/Time;

    const-string v6, "UTC"

    invoke-direct {v5, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 97
    .local v5, "utcTime":Landroid/text/format/Time;
    invoke-virtual {v5, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 100
    const-string v3, "yyyy-MM-dd\'T\'HH:mm:ss."

    .line 101
    .local v3, "pattern":Ljava/lang/String;
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p1, p2}, Ljava/util/Date;-><init>(J)V

    .line 103
    .local v1, "date":Ljava/util/Date;
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v2, v3, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 104
    .local v2, "df":Ljava/text/SimpleDateFormat;
    const-string v6, "UTC"

    invoke-static {v6}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 106
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "result":Ljava/lang/String;
    const-string v6, "000"

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "actualResult":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 110
    aget-object v6, v0, v8

    if-eqz v6, :cond_0

    .line 111
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "000Z"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v8

    .line 112
    aget-object v6, v0, v8

    .line 115
    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private getOoO()I
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V

    .line 355
    const/4 v12, 0x0

    .line 357
    .local v12, "result":I
    new-instance v26, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v26 .. v26}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 359
    .local v26, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v3, 0x485

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x489

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x487

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 363
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v3, :cond_0

    .line 364
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "getOoO(): Wbxml:"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 365
    invoke-virtual/range {v26 .. v26}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v2

    .line 366
    .local v2, "b":[B
    new-instance v19, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 367
    .local v19, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v23, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 368
    .local v23, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 372
    .end local v2    # "b":[B
    .end local v19    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v23    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_0
    const-string v3, "Settings"

    new-instance v4, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual/range {v26 .. v26}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sget v5, Lcom/android/exchange/EasOoOSvc;->COMMAND_TIMEOUT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Lcom/android/exchange/EasOoOSvc;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v24

    .line 375
    .local v24, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v28

    .line 378
    .local v28, "status":I
    const-string v3, "getOoO(): sendHttpClientPost HTTP response code: "

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, Lcom/android/exchange/EasOoOSvc;->userLog(Ljava/lang/String;I)V

    .line 380
    const/16 v3, 0xc8

    move/from16 v0, v28

    if-ne v0, v3, :cond_8

    .line 381
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v22

    .line 382
    .local v22, "len":I
    if-eqz v22, :cond_6

    .line 383
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v21

    .line 384
    .local v21, "in":Ljava/io/InputStream;
    new-instance v27, Lcom/android/exchange/adapter/OoOCommandParser;

    new-instance v3, Lcom/android/exchange/adapter/SettingsCommandAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, p0

    invoke-direct {v3, v4, v0}, Lcom/android/exchange/adapter/SettingsCommandAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v3}, Lcom/android/exchange/adapter/OoOCommandParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    .local v27, "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :try_start_1
    invoke-virtual/range {v27 .. v27}, Lcom/android/exchange/adapter/OoOCommandParser;->parse()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 388
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 393
    .local v8, "parserResult":Landroid/os/Bundle;
    sget-object v3, Lcom/android/emailcommon/utility/OoOConstants;->OOO_GET_DATA:Ljava/lang/String;

    invoke-virtual/range {v27 .. v27}, Lcom/android/exchange/adapter/OoOCommandParser;->getParsedData()Lcom/android/emailcommon/service/OoODataList;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x0

    const/16 v7, 0x64

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    .end local v8    # "parserResult":Landroid/os/Bundle;
    .end local v21    # "in":Ljava/io/InputStream;
    .end local v22    # "len":I
    .end local v27    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :goto_0
    if-eqz v24, :cond_1

    .line 432
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->close()V

    :cond_1
    move/from16 v25, v12

    .line 435
    .end local v12    # "result":I
    .end local v28    # "status":I
    .local v25, "result":I
    :goto_1
    return v25

    .line 399
    .end local v25    # "result":I
    .restart local v12    # "result":I
    .restart local v21    # "in":Ljava/io/InputStream;
    .restart local v22    # "len":I
    .restart local v27    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v28    # "status":I
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasOoOSvc;->isProvisionError(I)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual/range {v27 .. v27}, Lcom/android/exchange/adapter/OoOCommandParser;->getStatus()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/exchange/EasOoOSvc;->isProvisioningStatus(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 400
    :cond_3
    const/16 v12, 0x17

    .line 405
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v13, 0x64

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 408
    :catch_0
    move-exception v20

    .line 409
    .local v20, "e":Ljava/lang/Exception;
    const/4 v12, 0x0

    .line 410
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v16, -0x9

    const/16 v17, 0x64

    const/16 v18, 0x0

    move-object/from16 v13, p0

    invoke-direct/range {v13 .. v18}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 426
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "in":Ljava/io/InputStream;
    .end local v22    # "len":I
    .end local v27    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .end local v28    # "status":I
    :catch_1
    move-exception v20

    .line 427
    .restart local v20    # "e":Ljava/lang/Exception;
    const/4 v12, 0x0

    .line 428
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v16, -0x3

    const/16 v17, 0x64

    const/16 v18, 0x0

    move-object/from16 v13, p0

    invoke-direct/range {v13 .. v18}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 431
    if-eqz v24, :cond_4

    .line 432
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->close()V

    :cond_4
    move/from16 v25, v12

    .end local v12    # "result":I
    .restart local v25    # "result":I
    goto :goto_1

    .line 402
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v25    # "result":I
    .restart local v12    # "result":I
    .restart local v21    # "in":Ljava/io/InputStream;
    .restart local v22    # "len":I
    .restart local v27    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v28    # "status":I
    :cond_5
    const/4 v12, -0x8

    goto :goto_2

    .line 414
    .end local v21    # "in":Ljava/io/InputStream;
    .end local v27    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v14, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v16, 0x0

    const/16 v17, 0x64

    const/16 v18, 0x0

    move-object/from16 v13, p0

    invoke-direct/range {v13 .. v18}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 431
    .end local v22    # "len":I
    .end local v28    # "status":I
    :catchall_0
    move-exception v3

    if-eqz v24, :cond_7

    .line 432
    invoke-virtual/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->close()V

    :cond_7
    throw v3

    .line 417
    .restart local v28    # "status":I
    :cond_8
    const/16 v12, 0x15

    .line 418
    :try_start_6
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasOoOSvc;->isProvisionError(I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 419
    const/16 v12, 0x17

    .line 423
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v13, 0x64

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/android/exchange/EasOoOSvc;->getOutOfOfficeCb(JIILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 420
    :cond_a
    invoke-static/range {v28 .. v28}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v3

    if-eqz v3, :cond_9

    .line 421
    const/16 v12, 0x16

    goto :goto_3
.end method

.method private getOutOfOfficeCb(JIILandroid/os/Bundle;)V
    .locals 7
    .param p1, "accId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I
    .param p5, "results"    # Landroid/os/Bundle;

    .prologue
    .line 132
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->oOOfStatus(JIILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    return-void

    .line 133
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private isProvisioningStatus(I)Z
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 439
    const/4 v0, 0x0

    .line 441
    .local v0, "mProvisioningRequired":Z
    packed-switch p1, :pswitch_data_0

    .line 462
    :goto_0
    :pswitch_0
    return v0

    .line 456
    :pswitch_1
    const/4 v0, 0x1

    .line 457
    goto :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x8b
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private prepareSetCommand()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 139
    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsGlobal:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    .line 141
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mSvcData:Lcom/android/emailcommon/service/OoODataList;

    invoke-virtual {v2}, Lcom/android/emailcommon/service/OoODataList;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 142
    iget-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mSvcData:Lcom/android/emailcommon/service/OoODataList;

    invoke-virtual {v2, v0}, Lcom/android/emailcommon/service/OoODataList;->getItem(I)Lcom/android/emailcommon/service/OoOData;

    move-result-object v1

    .line 145
    .local v1, "singleData":Lcom/android/emailcommon/service/OoOData;
    if-eqz v1, :cond_0

    .line 147
    iget v2, v1, Lcom/android/emailcommon/service/OoOData;->state:I

    packed-switch v2, :pswitch_data_0

    .line 141
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :pswitch_0
    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsGlobal:Z

    iput-boolean v4, p0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    goto :goto_1

    .line 154
    :pswitch_1
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsGlobal:Z

    .line 155
    iget v2, v1, Lcom/android/emailcommon/service/OoOData;->type:I

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 157
    :pswitch_2
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    .line 158
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 159
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mInternalMsg:Ljava/lang/String;

    goto :goto_1

    .line 162
    :pswitch_3
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    .line 163
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 164
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mExternalKnownMsg:Ljava/lang/String;

    goto :goto_1

    .line 167
    :pswitch_4
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    .line 168
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 169
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mExternalUnKnownMsg:Ljava/lang/String;

    goto :goto_1

    .line 176
    :pswitch_5
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    .line 177
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->start:Ljava/util/Date;

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->start:Ljava/util/Date;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mStartDate:Ljava/util/Date;

    .line 179
    :cond_1
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->end:Ljava/util/Date;

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->end:Ljava/util/Date;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mEndDate:Ljava/util/Date;

    .line 182
    :cond_2
    iget v2, v1, Lcom/android/emailcommon/service/OoOData;->type:I

    packed-switch v2, :pswitch_data_2

    goto :goto_1

    .line 184
    :pswitch_6
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    .line 185
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mInternalMsg:Ljava/lang/String;

    goto :goto_1

    .line 189
    :pswitch_7
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    .line 190
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mExternalKnownMsg:Ljava/lang/String;

    goto :goto_1

    .line 194
    :pswitch_8
    iput-boolean v3, p0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    .line 195
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 196
    iget-object v2, v1, Lcom/android/emailcommon/service/OoOData;->msg:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasOoOSvc;->mExternalUnKnownMsg:Ljava/lang/String;

    goto :goto_1

    .line 208
    .end local v1    # "singleData":Lcom/android/emailcommon/service/OoOData;
    :cond_3
    return-void

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
    .end packed-switch

    .line 155
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 182
    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private setOoO()I
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 211
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V

    .line 213
    const/16 v21, 0x0

    .line 214
    .local v21, "result":I
    new-instance v22, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 216
    .local v22, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOoOSvc;->prepareSetCommand()V

    .line 218
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsGlobal:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    if-nez v3, :cond_2

    .line 219
    const/16 v3, 0x485

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x489

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x488

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48a

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 282
    :goto_0
    const/16 v20, 0x0

    .line 285
    .local v20, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v3, :cond_0

    .line 286
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "setOoO(): Wbxml:"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 287
    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v2

    .line 288
    .local v2, "b":[B
    new-instance v15, Ljava/io/ByteArrayInputStream;

    invoke-direct {v15, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 289
    .local v15, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v19, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 290
    .local v19, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 293
    .end local v2    # "b":[B
    .end local v15    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v19    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_0
    const-string v3, "Settings"

    new-instance v4, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sget v5, Lcom/android/exchange/EasOoOSvc;->COMMAND_TIMEOUT:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Lcom/android/exchange/EasOoOSvc;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v20

    .line 296
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v24

    .line 299
    .local v24, "status":I
    const-string v3, "setOoO(): sendHttpClientPost HTTP response code: "

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, Lcom/android/exchange/EasOoOSvc;->userLog(Ljava/lang/String;I)V

    .line 301
    const/16 v3, 0xc8

    move/from16 v0, v24

    if-ne v0, v3, :cond_b

    .line 302
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v18

    .line 303
    .local v18, "len":I
    if-eqz v18, :cond_a

    .line 304
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    .line 305
    .local v17, "in":Ljava/io/InputStream;
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 306
    .local v8, "bundle":Landroid/os/Bundle;
    new-instance v23, Lcom/android/exchange/adapter/OoOCommandParser;

    new-instance v3, Lcom/android/exchange/adapter/SettingsCommandAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, p0

    invoke-direct {v3, v4, v0}, Lcom/android/exchange/adapter/SettingsCommandAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v3}, Lcom/android/exchange/adapter/OoOCommandParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 308
    .local v23, "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    invoke-virtual/range {v23 .. v23}, Lcom/android/exchange/adapter/OoOCommandParser;->parse()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 309
    sget-object v3, Lcom/android/emailcommon/utility/OoOConstants;->OOO_SET_DATA:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v8, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x0

    const/16 v7, 0x64

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move/from16 v6, v21

    .line 340
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v18    # "len":I
    .end local v21    # "result":I
    .end local v23    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .local v6, "result":I
    :goto_1
    if-eqz v20, :cond_1

    .line 341
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    :cond_1
    move/from16 v21, v6

    .line 345
    .end local v6    # "result":I
    .end local v24    # "status":I
    .restart local v21    # "result":I
    :goto_2
    return v21

    .line 223
    .end local v20    # "resp":Lcom/android/exchange/EasResponse;
    :cond_2
    const/16 v3, 0x485

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x489

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x488

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 225
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsTimeBased:Z

    if-eqz v3, :cond_3

    .line 226
    const/16 v3, 0x48a

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "2"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 229
    const/16 v3, 0x48b

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mStartDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/EasOoOSvc;->convertLocalToUTC(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 231
    const/16 v3, 0x48c

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mEndDate:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/EasOoOSvc;->convertLocalToUTC(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 236
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsInternal:Z

    if-eqz v3, :cond_4

    .line 237
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48e

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x492

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mInternalMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 248
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsExtKnown:Z

    if-eqz v3, :cond_5

    .line 250
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48f

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x492

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mExternalKnownMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 255
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x490

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 278
    :goto_5
    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 279
    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->done()V

    goto/16 :goto_0

    .line 234
    :cond_3
    const/16 v3, 0x48a

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    goto/16 :goto_3

    .line 242
    :cond_4
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48e

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x492

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    goto/16 :goto_4

    .line 258
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasOoOSvc;->mIsExtUnKnown:Z

    if-eqz v3, :cond_6

    .line 259
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48f

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x492

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mExternalUnKnownMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 264
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x490

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x492

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasOoOSvc;->mExternalUnKnownMsg:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x493

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    goto/16 :goto_5

    .line 270
    :cond_6
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x48f

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    .line 273
    const/16 v3, 0x48d

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x490

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const/16 v4, 0x491

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    goto/16 :goto_5

    .line 313
    .restart local v8    # "bundle":Landroid/os/Bundle;
    .restart local v17    # "in":Ljava/io/InputStream;
    .restart local v18    # "len":I
    .restart local v20    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v23    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v24    # "status":I
    :cond_7
    :try_start_1
    sget-object v3, Lcom/android/emailcommon/utility/OoOConstants;->OOO_SET_DATA:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v8, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 314
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasOoOSvc;->isProvisionError(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-eqz v3, :cond_9

    .line 315
    const/16 v6, 0x17

    .line 319
    .end local v21    # "result":I
    .restart local v6    # "result":I
    :goto_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v7, 0x64

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 335
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v18    # "len":I
    .end local v23    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :catch_0
    move-exception v16

    .line 336
    .end local v24    # "status":I
    .local v16, "e":Ljava/lang/Exception;
    :goto_7
    const/4 v6, 0x0

    .line 337
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v12, -0x3

    const/16 v13, 0x64

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 340
    if-eqz v20, :cond_8

    .line 341
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    :cond_8
    move/from16 v21, v6

    .end local v6    # "result":I
    .restart local v21    # "result":I
    goto/16 :goto_2

    .line 317
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v8    # "bundle":Landroid/os/Bundle;
    .restart local v17    # "in":Ljava/io/InputStream;
    .restart local v18    # "len":I
    .restart local v23    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v24    # "status":I
    :cond_9
    const/4 v6, -0x7

    .end local v21    # "result":I
    .restart local v6    # "result":I
    goto :goto_6

    .line 323
    .end local v6    # "result":I
    .end local v8    # "bundle":Landroid/os/Bundle;
    .end local v17    # "in":Ljava/io/InputStream;
    .end local v23    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v21    # "result":I
    :cond_a
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v12, 0x0

    const/16 v13, 0x64

    const/4 v14, 0x0

    move-object/from16 v9, p0

    invoke-direct/range {v9 .. v14}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move/from16 v6, v21

    .end local v21    # "result":I
    .restart local v6    # "result":I
    goto/16 :goto_1

    .line 326
    .end local v6    # "result":I
    .end local v18    # "len":I
    .restart local v21    # "result":I
    :cond_b
    const/16 v6, 0x15

    .line 327
    .end local v21    # "result":I
    .restart local v6    # "result":I
    :try_start_5
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasOoOSvc;->isProvisionError(I)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 328
    const/16 v6, 0x17

    .line 332
    :cond_c
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v13, 0x64

    const/4 v14, 0x0

    move-object/from16 v9, p0

    move v12, v6

    invoke-direct/range {v9 .. v14}, Lcom/android/exchange/EasOoOSvc;->setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 340
    .end local v24    # "status":I
    :catchall_0
    move-exception v3

    :goto_9
    if-eqz v20, :cond_d

    .line 341
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    :cond_d
    throw v3

    .line 329
    .restart local v24    # "status":I
    :cond_e
    :try_start_6
    invoke-static/range {v24 .. v24}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v3

    if-eqz v3, :cond_c

    .line 330
    const/16 v6, 0x16

    goto :goto_8

    .line 340
    .end local v6    # "result":I
    .end local v24    # "status":I
    .restart local v21    # "result":I
    :catchall_1
    move-exception v3

    move/from16 v6, v21

    .end local v21    # "result":I
    .restart local v6    # "result":I
    goto :goto_9

    .line 335
    .end local v6    # "result":I
    .restart local v21    # "result":I
    :catch_1
    move-exception v16

    move/from16 v6, v21

    .end local v21    # "result":I
    .restart local v6    # "result":I
    goto :goto_7
.end method

.method private setOutOfOfficeCb(JIILandroid/os/Bundle;)V
    .locals 7
    .param p1, "accId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I
    .param p5, "results"    # Landroid/os/Bundle;

    .prologue
    .line 119
    if-nez p5, :cond_0

    .line 120
    new-instance p5, Landroid/os/Bundle;

    .end local p5    # "results":Landroid/os/Bundle;
    invoke-direct {p5}, Landroid/os/Bundle;-><init>()V

    .line 122
    .restart local p5    # "results":Landroid/os/Bundle;
    :cond_0
    sget-object v0, Lcom/android/emailcommon/utility/OoOConstants;->OOO_TYPE_SET:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p5, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 124
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->oOOfStatus(JIILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/16 v4, 0x17

    const/4 v6, 0x0

    const/4 v8, 0x1

    .line 467
    invoke-virtual {p0}, Lcom/android/exchange/EasOoOSvc;->setupService()Z

    .line 469
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasOoOSvc;->mDeviceId:Ljava/lang/String;

    .line 471
    const/4 v1, 0x0

    .line 472
    .local v1, "result":I
    iget-object v3, p0, Lcom/android/exchange/EasOoOSvc;->mSvcData:Lcom/android/emailcommon/service/OoODataList;

    if-nez v3, :cond_1

    .line 473
    invoke-direct {p0}, Lcom/android/exchange/EasOoOSvc;->getOoO()I

    move-result v1

    .line 478
    :goto_0
    if-ne v1, v4, :cond_0

    .line 479
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    .line 482
    :cond_0
    const/16 v3, 0x16

    if-ne v1, v3, :cond_3

    .line 483
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_0

    .line 525
    .end local v1    # "result":I
    :goto_1
    return-void

    .line 475
    .restart local v1    # "result":I
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/android/exchange/EasOoOSvc;->setOoO()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 510
    :pswitch_0
    const/16 v2, 0x17

    .line 515
    .local v2, "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_2

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto :goto_1

    .line 518
    :cond_2
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto :goto_1

    .line 485
    .end local v2    # "status":I
    :cond_3
    const/16 v3, 0x15

    if-ne v1, v3, :cond_5

    .line 486
    const/4 v3, 0x3

    :try_start_2
    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_1

    goto :goto_1

    .line 510
    :pswitch_1
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_4

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto :goto_1

    .line 518
    :cond_4
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto :goto_1

    .line 491
    .end local v2    # "status":I
    :cond_5
    if-ne v1, v4, :cond_7

    .line 492
    const/4 v3, 0x4

    :try_start_3
    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_2

    goto :goto_1

    .line 510
    :pswitch_2
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_6

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 518
    :cond_6
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 497
    .end local v2    # "status":I
    :cond_7
    const/4 v3, 0x0

    :try_start_4
    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_3

    goto/16 :goto_1

    .line 510
    :pswitch_3
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_8

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 518
    :cond_8
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 498
    .end local v1    # "result":I
    .end local v2    # "status":I
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Ljava/io/IOException;
    const/4 v3, 0x1

    :try_start_5
    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_4

    goto/16 :goto_1

    .line 510
    :pswitch_4
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_9

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 518
    :cond_9
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 500
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "status":I
    :catch_1
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    const-string v3, "Exception caught in EasOOFSvc"

    invoke-virtual {p0, v3, v0}, Lcom/android/exchange/EasOoOSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 502
    const/4 v3, 0x3

    iput v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 506
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "OoO finished"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v3, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_5

    goto/16 :goto_1

    .line 510
    :pswitch_5
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_a

    .line 516
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 518
    :cond_a
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v3, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 505
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "status":I
    :catchall_0
    move-exception v3

    .line 506
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "OoO finished"

    aput-object v5, v4, v6

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 507
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 508
    iget v4, p0, Lcom/android/exchange/EasOoOSvc;->mExitStatus:I

    packed-switch v4, :pswitch_data_6

    .line 524
    :goto_2
    throw v3

    .line 510
    :pswitch_6
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v4, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v4, :cond_b

    .line 516
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v5, v4, v6

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    goto :goto_2

    .line 518
    :cond_b
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v5, v4, v6

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasOoOSvc;->userLog([Ljava/lang/String;)V

    .line 519
    sget-object v4, Lcom/android/exchange/EasOoOSvc;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/EasOoOSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 520
    sput-boolean v8, Lcom/android/exchange/EasOoOSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto :goto_2

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x4
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x4
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x4
        :pswitch_6
    .end packed-switch
.end method
