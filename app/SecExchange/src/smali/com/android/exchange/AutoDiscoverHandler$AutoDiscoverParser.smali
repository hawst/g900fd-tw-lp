.class Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/AutoDiscoverHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AutoDiscoverParser"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$1700(Lorg/apache/http/HttpResponse;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lorg/apache/http/client/methods/HttpPost;
    .locals 1
    .param p0, "x0"    # Lorg/apache/http/HttpResponse;
    .param p1, "x1"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p2, "x2"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1307
    invoke-static {p0, p1, p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseAutoDiscoverResponse(Lorg/apache/http/HttpResponse;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    return-object v0
.end method

.method private static parseAction(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 6
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p3, "autoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1582
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1586
    .local v1, "type":I
    if-ne v1, v3, :cond_2

    .line 1652
    :cond_1
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 1592
    :cond_2
    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Action"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1597
    :cond_3
    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    .line 1599
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1601
    .local v0, "name":Ljava/lang/String;
    const-string v4, "Error"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1605
    invoke-static {p0, p1, p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseError(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V

    goto :goto_0

    .line 1609
    :cond_4
    const-string v4, "Redirect"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1611
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v2

    .line 1613
    .local v2, "val":Ljava/lang/String;
    if-eqz v2, :cond_0

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1619
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p2, v2}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;

    .line 1632
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z
    invoke-static {p3, v3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    goto :goto_1

    .line 1643
    .end local v2    # "val":Ljava/lang/String;
    :cond_5
    const-string v4, "Settings"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1645
    invoke-static {p0, p1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseSettings(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0
.end method

.method private static parseAutoDiscoverResponse(Lorg/apache/http/HttpResponse;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lorg/apache/http/client/methods/HttpPost;
    .locals 11
    .param p0, "resp"    # Lorg/apache/http/HttpResponse;
    .param p1, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p2, "autoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1314
    const/4 v2, 0x0

    .line 1316
    .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v6, 0x0

    .line 1320
    .local v6, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v4, 0x0

    .line 1323
    .local v4, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1325
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_0

    .line 1326
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 1330
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 1332
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 1334
    const-string v8, "UTF-8"

    invoke-interface {v6, v4, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 1336
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v7

    .line 1338
    .local v7, "type":I
    if-nez v7, :cond_0

    .line 1340
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v7

    .line 1342
    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 1344
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1346
    .local v5, "name":Ljava/lang/String;
    const-string v8, "Autodiscover"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1349
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v3

    .line 1351
    .local v3, "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    invoke-static {v6, v3, p1, p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseAutodiscover(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    .line 1356
    iget-object v8, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    if-eqz v8, :cond_3

    .line 1367
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 1370
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\\"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    .line 1381
    :goto_0
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    .line 1385
    const-string v8, "eas"

    iput-object v8, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    .line 1400
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "autodiscover_host_auth"

    invoke-virtual {v8, v9, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1433
    .end local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_0
    :goto_1
    if-eqz v4, :cond_1

    .line 1434
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 1437
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    :cond_1
    :goto_2
    const/4 v8, 0x0

    return-object v8

    .line 1375
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v7    # "type":I
    :cond_2
    :try_start_1
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;
    invoke-static {p1}, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v3, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1428
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "type":I
    :catch_0
    move-exception v0

    .line 1430
    .local v0, "e1":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_2
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1433
    if-eqz v4, :cond_1

    .line 1434
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    goto :goto_2

    .line 1406
    .end local v0    # "e1":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v7    # "type":I
    :cond_3
    :try_start_3
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1408
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "autodiscover_error_code"

    const/4 v10, 0x5

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 1433
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "type":I
    :catchall_0
    move-exception v8

    if-eqz v4, :cond_4

    .line 1434
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v8

    .line 1414
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v3    # "hostAuth":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v5    # "name":Ljava/lang/String;
    .restart local v7    # "type":I
    :cond_5
    :try_start_4
    # getter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;
    invoke-static {p2}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "autodiscover_error_code"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method private static parseAutodiscover(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p3, "autoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1449
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v0

    .line 1453
    .local v0, "type":I
    if-ne v0, v1, :cond_2

    .line 1474
    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 1459
    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Autodiscover"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1464
    :cond_3
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Response"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1467
    invoke-static {p0, p1, p2, p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseResponse(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method private static parseError(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    .locals 10
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "autoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1663
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .line 1667
    .local v4, "type":I
    if-ne v4, v8, :cond_2

    .line 1708
    :cond_1
    return-void

    .line 1673
    :cond_2
    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Error"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1678
    :cond_3
    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 1680
    # setter for: Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z
    invoke-static {p2, v8}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->access$2702(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z

    .line 1682
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1684
    .local v2, "name":Ljava/lang/String;
    const-string v5, "Status"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1686
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    .line 1688
    .local v3, "status":Ljava/lang/String;
    new-array v5, v8, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Autodiscover, Error Status : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto :goto_0

    .line 1690
    .end local v3    # "status":Ljava/lang/String;
    :cond_4
    const-string v5, "Message"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1692
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 1694
    .local v1, "message":Ljava/lang/String;
    new-array v5, v8, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Autodiscover, Error Message : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto :goto_0

    .line 1696
    .end local v1    # "message":Ljava/lang/String;
    :cond_5
    const-string v5, "DebugData"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1698
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 1700
    .local v0, "debugData":Ljava/lang/String;
    new-array v5, v8, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Autodiscover, Error DebugData : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v5}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static parseResponse(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 5
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .param p2, "userInfo"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p3, "autoDiscInfo"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1485
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1489
    .local v1, "type":I
    if-ne v1, v2, :cond_2

    .line 1520
    :cond_1
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 1495
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Response"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1500
    :cond_3
    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 1502
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1504
    .local v0, "name":Ljava/lang/String;
    const-string v3, "User"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1506
    invoke-static {p0, p1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseUser(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0

    .line 1508
    :cond_4
    const-string v3, "Action"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1510
    invoke-static {p0, p1, p2, p3}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseAction(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method private static parseServer(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 11
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1753
    const/4 v1, 0x0

    .line 1757
    .local v1, "mobileSync":Z
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    .line 1761
    .local v4, "type":I
    if-ne v4, v9, :cond_2

    .line 1838
    :cond_1
    return-void

    .line 1767
    :cond_2
    const/4 v6, 0x3

    if-ne v4, v6, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "Server"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1772
    :cond_3
    const/4 v6, 0x2

    if-ne v4, v6, :cond_0

    .line 1774
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1776
    .local v2, "name":Ljava/lang/String;
    const-string v6, "Type"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1778
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    const-string v7, "MobileSync"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1780
    const/4 v1, 0x1

    goto :goto_0

    .line 1788
    :cond_4
    if-eqz v1, :cond_0

    const-string v6, "Url"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1790
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 1792
    .local v5, "url":Ljava/lang/String;
    new-array v6, v9, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Autodiscover, server URL :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    .line 1806
    const-string v6, "https://"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1807
    const/16 v3, 0x8

    .line 1808
    .local v3, "serverFirstIndex":I
    const/16 v6, 0x1bb

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 1809
    const/4 v6, 0x5

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    .line 1819
    :goto_1
    const-string v6, "/microsoft-server-activesync"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1821
    const/16 v6, 0x2f

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1822
    .local v0, "lastSlash":I
    invoke-virtual {v5, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    .line 1829
    .end local v0    # "lastSlash":I
    :goto_2
    new-array v6, v9, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Autodiscover, hostAuth.mAddress : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v6}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1813
    .end local v3    # "serverFirstIndex":I
    :cond_5
    const/4 v3, 0x7

    .line 1814
    .restart local v3    # "serverFirstIndex":I
    const/4 v6, -0x1

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    .line 1815
    const/4 v6, 0x4

    iput v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    goto :goto_1

    .line 1826
    :cond_6
    invoke-virtual {v5, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    goto :goto_2
.end method

.method private static parseSettings(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1717
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1721
    .local v1, "type":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1746
    :cond_1
    return-void

    .line 1727
    :cond_2
    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Settings"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1732
    :cond_3
    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1734
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1736
    .local v0, "name":Ljava/lang/String;
    const-string v2, "Server"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1738
    invoke-static {p0, p1}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverParser;->parseServer(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V

    goto :goto_0
.end method

.method private static parseUser(Lorg/xmlpull/v1/XmlPullParser;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)V
    .locals 9
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "hostAuth"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1537
    :cond_0
    :goto_0
    if-nez p0, :cond_1

    goto :goto_0

    .line 1538
    :cond_1
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 1541
    .local v3, "type":I
    if-ne v3, v7, :cond_3

    .line 1572
    :cond_2
    return-void

    .line 1545
    :cond_3
    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "User"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1550
    :cond_4
    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 1552
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1554
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_5

    const-string v4, "EMailAddress"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1556
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v0

    .line 1558
    .local v0, "addr":Ljava/lang/String;
    new-array v4, v7, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Autodiscover, email: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto :goto_0

    .line 1560
    .end local v0    # "addr":Ljava/lang/String;
    :cond_5
    if-eqz v2, :cond_0

    const-string v4, "DisplayName"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1562
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v1

    .line 1564
    .local v1, "dn":Ljava/lang/String;
    new-array v4, v7, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Autodiscover, user: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    # invokes: Lcom/android/exchange/AutoDiscoverHandler;->userLog([Ljava/lang/String;)V
    invoke-static {v4}, Lcom/android/exchange/AutoDiscoverHandler;->access$1900([Ljava/lang/String;)V

    goto :goto_0
.end method
