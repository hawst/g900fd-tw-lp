.class public Lcom/android/exchange/EasFetchDocumentSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasFetchDocumentSvc.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

.field mContentUriString:Ljava/lang/String;

.field mDestinationFile:Ljava/lang/String;

.field private msg:Lcom/android/emailcommon/provider/EmailContent$Message;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "EasFetchDocumentSvc"

    sput-object v0, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_m"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p3, "_msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p4, "_att"    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p5, "destinationFile"    # Ljava/lang/String;
    .param p6, "contentUriString"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 53
    iput-object p4, p0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .line 54
    iput-object p3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 55
    iput-object p5, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mDestinationFile:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mContentUriString:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mProtocolVersion:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 62
    :cond_0
    return-void
.end method

.method private fetchDocument()I
    .locals 47
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    const/16 v41, 0x0

    .line 74
    .local v41, "result":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 76
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4028000000000000L    # 12.0

    cmpg-double v5, v6, v8

    if-gez v5, :cond_12

    .line 79
    const/16 v10, 0x14

    .line 80
    .end local v41    # "result":I
    .local v10, "result":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/4 v11, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 81
    const/4 v10, 0x0

    .line 84
    :goto_0
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->mContext:Landroid/content/Context;

    const-string v6, "device_policy"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/app/admin/DevicePolicyManager;

    .line 102
    .local v34, "mDPM":Landroid/app/admin/DevicePolicyManager;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "fetchDocument() from SharePoing/UNC at "

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 103
    new-instance v42, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v42 .. v42}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 104
    .local v42, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v21, 0x0

    .line 105
    .local v21, "commandType":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "0-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    .line 108
    .local v43, "sizeStr":Ljava/lang/String;
    const-string v21, "ItemOperations"

    .line 109
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-boolean v5, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagFavorite:Z

    if-eqz v5, :cond_3

    .line 110
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";MULTIPART"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 116
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    const-string v6, "\\."

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 117
    .local v26, "fileTypes":[Ljava/lang/String;
    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v27, v0

    .line 118
    .local v27, "fileTypesLen":I
    const/4 v5, 0x2

    move/from16 v0, v27

    if-lt v0, v5, :cond_1

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "txt"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "html"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "doc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "ppt"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "xls"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "rtf"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "xml"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v5, v27, -0x1

    aget-object v5, v26, v5

    const-string v6, "bmp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 127
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";GZIP"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 130
    :cond_1
    const/16 v5, 0x505

    move-object/from16 v0, v42

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x506

    invoke-virtual {v5, v6}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x507

    const-string v7, "DocumentLibrary"

    invoke-virtual {v5, v6, v7}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 132
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 133
    const/16 v5, 0x4c5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v42

    invoke-virtual {v0, v5, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 135
    :cond_2
    const/16 v5, 0x508

    move-object/from16 v0, v42

    invoke-virtual {v0, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    const/16 v6, 0x509

    move-object/from16 v0, v43

    invoke-virtual {v5, v6, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 138
    const/16 v39, 0x0

    .line 140
    .local v39, "res":Lcom/android/exchange/EasResponse;
    const/4 v5, 0x1

    :try_start_0
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fetchDocument() - cmd: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 141
    invoke-virtual/range {v42 .. v42}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v39

    .line 144
    if-eqz v39, :cond_4

    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v20

    .line 145
    .local v20, "code":I
    :goto_2
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fetchDocument() - resp code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 147
    if-eqz v39, :cond_10

    const/16 v5, 0xc8

    move/from16 v0, v20

    if-ne v0, v5, :cond_10

    .line 148
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v32

    .line 151
    .local v32, "len":I
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v31

    .line 154
    .local v31, "is":Ljava/io/InputStream;
    const/16 v44, 0x0

    .line 155
    .local v44, "startPos":I
    const/16 v33, 0x0

    .line 156
    .local v33, "length":I
    if-eqz v31, :cond_b

    .line 157
    const/16 v45, 0x1

    .line 161
    .local v45, "successp":Z
    const-string v5, "Content-Type"

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v40

    .line 162
    .local v40, "respContentType":Lorg/apache/http/Header;
    if-eqz v40, :cond_6

    invoke-interface/range {v40 .. v40}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    const-string v6, "application/vnd.ms-sync.multipart"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 166
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "fetchDocument() - resp: multipart"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 168
    const/16 v5, 0x14

    new-array v0, v5, [B

    move-object/from16 v18, v0

    .line 169
    .local v18, "b":[B
    move-object/from16 v28, v31

    .line 171
    .local v28, "instream":Ljava/io/InputStream;
    const/16 v5, 0x14

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v38

    .line 172
    .local v38, "read":I
    const/16 v5, 0x14

    move/from16 v0, v38

    if-ge v0, v5, :cond_5

    .line 174
    const/16 v45, 0x0

    .line 175
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v16, 0x40000

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 177
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    const-string v6, "ERROR_FETCH_FAILURE "

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 178
    const/4 v5, 0x0

    .line 322
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    .line 325
    .end local v18    # "b":[B
    .end local v20    # "code":I
    .end local v28    # "instream":Ljava/io/InputStream;
    .end local v31    # "is":Ljava/io/InputStream;
    .end local v32    # "len":I
    .end local v33    # "length":I
    .end local v38    # "read":I
    .end local v40    # "respContentType":Lorg/apache/http/Header;
    .end local v44    # "startPos":I
    .end local v45    # "successp":Z
    :goto_3
    return v5

    .line 112
    .end local v26    # "fileTypes":[Ljava/lang/String;
    .end local v27    # "fileTypesLen":I
    .end local v39    # "res":Lcom/android/exchange/EasResponse;
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ";INLINE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_1

    .line 144
    .restart local v26    # "fileTypes":[Ljava/lang/String;
    .restart local v27    # "fileTypesLen":I
    .restart local v39    # "res":Lcom/android/exchange/EasResponse;
    :cond_4
    const/16 v20, -0x1

    goto/16 :goto_2

    .line 180
    .restart local v18    # "b":[B
    .restart local v20    # "code":I
    .restart local v28    # "instream":Ljava/io/InputStream;
    .restart local v31    # "is":Ljava/io/InputStream;
    .restart local v32    # "len":I
    .restart local v33    # "length":I
    .restart local v38    # "read":I
    .restart local v40    # "respContentType":Lorg/apache/http/Header;
    .restart local v44    # "startPos":I
    .restart local v45    # "successp":Z
    :cond_5
    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->byteArrayToInt([BI)I

    move-result v35

    .line 181
    .local v35, "numParts":I
    const/16 v5, 0xc

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->byteArrayToInt([BI)I

    move-result v5

    add-int/lit8 v44, v5, -0x14

    .line 182
    const/16 v5, 0x10

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/Utility;->byteArrayToInt([BI)I

    move-result v33

    .line 183
    const/4 v5, 0x2

    move/from16 v0, v35

    if-ge v0, v5, :cond_7

    .line 185
    const/16 v45, 0x0

    .line 186
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v16, 0x40000

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 188
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    const-string v6, "ERROR_FETCH_FAILURE "

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 189
    const/4 v5, 0x0

    .line 322
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_3

    .line 193
    .end local v18    # "b":[B
    .end local v28    # "instream":Ljava/io/InputStream;
    .end local v35    # "numParts":I
    .end local v38    # "read":I
    :cond_6
    const/4 v5, 0x1

    :try_start_2
    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "fetchDocument() - resp: inLine"

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 195
    :try_start_3
    new-instance v30, Lcom/android/exchange/adapter/ItemOperationsParser;

    new-instance v5, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/android/exchange/adapter/ItemOperationsAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 197
    .local v30, "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z

    move-result v45

    .line 198
    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/android/exchange/adapter/ItemOperationsParser;->outStream:Ljava/io/InputStream;

    move-object/from16 v31, v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 226
    .end local v30    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    :cond_7
    :goto_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-static {v6, v7, v8, v9}, Lcom/android/emailcommon/utility/AttachmentUtilities;->getAttachmentUri(JJ)Landroid/net/Uri;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v4

    .line 227
    .local v4, "attachmentUri":Landroid/net/Uri;
    const/16 v37, 0x0

    .line 230
    .local v37, "os":Ljava/io/OutputStream;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v5, v4}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v37

    .line 231
    const/16 v46, 0x0

    .line 235
    .local v46, "totalRead":I
    if-eqz v32, :cond_8

    .line 237
    const/16 v5, 0x4000

    :try_start_6
    new-array v0, v5, [B

    move-object/from16 v19, v0

    .line 241
    .local v19, "bytes":[B
    const-string v5, "fetchDocument() - resp: content-length "

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 243
    :goto_5
    const/4 v5, 0x0

    const/16 v6, 0x4000

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v38

    .line 245
    .restart local v38    # "read":I
    if-gez v38, :cond_c

    .line 246
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fetchDocument() - Document load reached EOF,  totalRead: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v46

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " length: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 267
    :try_start_7
    const-string v5, "fetchDocument() - total bytes: "

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 269
    if-eqz v37, :cond_8

    .line 270
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->flush()V

    .line 271
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 272
    const/16 v37, 0x0

    .line 278
    .end local v19    # "bytes":[B
    .end local v38    # "read":I
    :cond_8
    if-eqz v37, :cond_9

    .line 279
    :try_start_8
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->flush()V

    .line 280
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->close()V

    .line 285
    :cond_9
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 286
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 287
    .local v23, "cva":Landroid/content/ContentValues;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v5, v4, v0, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 292
    .end local v23    # "cva":Landroid/content/ContentValues;
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    invoke-virtual {v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->isSaved()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 297
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 298
    .local v22, "cv":Landroid/content/ContentValues;
    const-string v5, "contentUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    sget-object v6, Lcom/android/exchange/EasFetchDocumentSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-virtual {v5, v6, v0}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/content/ContentValues;)I

    .line 301
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/16 v16, 0x0

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 305
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v22    # "cv":Landroid/content/ContentValues;
    .end local v37    # "os":Ljava/io/OutputStream;
    .end local v40    # "respContentType":Lorg/apache/http/Header;
    .end local v45    # "successp":Z
    .end local v46    # "totalRead":I
    :cond_b
    const/4 v10, 0x0

    .line 322
    .end local v31    # "is":Ljava/io/InputStream;
    .end local v32    # "len":I
    .end local v33    # "length":I
    .end local v44    # "startPos":I
    :goto_6
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    move v5, v10

    .line 325
    goto/16 :goto_3

    .line 199
    .restart local v31    # "is":Ljava/io/InputStream;
    .restart local v32    # "len":I
    .restart local v33    # "length":I
    .restart local v40    # "respContentType":Lorg/apache/http/Header;
    .restart local v44    # "startPos":I
    .restart local v45    # "successp":Z
    :catch_0
    move-exception v29

    .line 200
    .local v29, "ioe":Ljava/io/IOException;
    const/16 v45, 0x0

    .line 202
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v16, 0x30000

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 204
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ERROR_FETCH_RESPONSE_PARSE: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v29 .. v29}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 205
    const/4 v5, 0x0

    .line 322
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_3

    .line 206
    .end local v29    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v36

    .line 207
    .local v36, "ome":Ljava/lang/OutOfMemoryError;
    const/16 v45, 0x0

    .line 210
    :try_start_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/high16 v16, 0x50000

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V

    .line 212
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ERROR_FETCH_OUTOFMEMORY: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v36 .. v36}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 213
    const/4 v5, 0x0

    .line 322
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_3

    .line 214
    .end local v36    # "ome":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v24

    .line 216
    .local v24, "dae":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_b
    invoke-virtual/range {v24 .. v24}, Lcom/android/emailcommon/utility/DeviceAccessException;->printStackTrace()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto/16 :goto_4

    .line 317
    .end local v20    # "code":I
    .end local v24    # "dae":Lcom/android/emailcommon/utility/DeviceAccessException;
    .end local v31    # "is":Ljava/io/InputStream;
    .end local v32    # "len":I
    .end local v33    # "length":I
    .end local v40    # "respContentType":Lorg/apache/http/Header;
    .end local v44    # "startPos":I
    .end local v45    # "successp":Z
    :catch_3
    move-exception v25

    .line 318
    .local v25, "e":Ljava/lang/Exception;
    :try_start_c
    sget-object v5, Lcom/android/exchange/EasFetchDocumentSvc;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fetchDocument() - resp exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v12, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/16 v16, -0x3

    const/16 v17, 0x64

    move-object/from16 v11, p0

    invoke-direct/range {v11 .. v17}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 322
    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    move v5, v10

    goto/16 :goto_3

    .line 250
    .end local v25    # "e":Ljava/lang/Exception;
    .restart local v4    # "attachmentUri":Landroid/net/Uri;
    .restart local v19    # "bytes":[B
    .restart local v20    # "code":I
    .restart local v31    # "is":Ljava/io/InputStream;
    .restart local v32    # "len":I
    .restart local v33    # "length":I
    .restart local v37    # "os":Ljava/io/OutputStream;
    .restart local v38    # "read":I
    .restart local v40    # "respContentType":Lorg/apache/http/Header;
    .restart local v44    # "startPos":I
    .restart local v45    # "successp":Z
    .restart local v46    # "totalRead":I
    :cond_c
    :try_start_d
    const-string v5, "fetchDocument() - looping read bytes: "

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 251
    move/from16 v0, v38

    move/from16 v1, v44

    if-lt v0, v1, :cond_d

    .line 252
    sub-int v38, v38, v44

    .line 256
    add-int v46, v46, v38

    .line 259
    move-object/from16 v0, v37

    move-object/from16 v1, v19

    move/from16 v2, v44

    move/from16 v3, v38

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 260
    const/16 v44, 0x0

    goto/16 :goto_5

    .line 263
    :cond_d
    sub-int v44, v44, v38

    goto/16 :goto_5

    .line 267
    .end local v19    # "bytes":[B
    .end local v38    # "read":I
    :catchall_0
    move-exception v5

    :try_start_e
    const-string v6, "fetchDocument() - total bytes: "

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v6, v1}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 269
    if-eqz v37, :cond_e

    .line 270
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->flush()V

    .line 271
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->close()V

    .line 272
    const/16 v37, 0x0

    :cond_e
    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 278
    .end local v46    # "totalRead":I
    :catchall_1
    move-exception v5

    if-eqz v37, :cond_f

    .line 279
    :try_start_f
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->flush()V

    .line 280
    invoke-virtual/range {v37 .. v37}, Ljava/io/OutputStream;->close()V

    :cond_f
    throw v5
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 322
    .end local v4    # "attachmentUri":Landroid/net/Uri;
    .end local v20    # "code":I
    .end local v31    # "is":Ljava/io/InputStream;
    .end local v32    # "len":I
    .end local v33    # "length":I
    .end local v37    # "os":Ljava/io/OutputStream;
    .end local v40    # "respContentType":Lorg/apache/http/Header;
    .end local v44    # "startPos":I
    .end local v45    # "successp":Z
    :catchall_2
    move-exception v5

    invoke-virtual/range {v39 .. v39}, Lcom/android/exchange/EasResponse;->close()V

    throw v5

    .line 308
    .restart local v20    # "code":I
    :cond_10
    const/16 v10, 0x15

    .line 310
    :try_start_10
    invoke-static/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 311
    const/16 v10, 0x16

    .line 313
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->msg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    const/4 v11, 0x0

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v11}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocumentCb(JJII)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 314
    const/4 v10, 0x0

    goto/16 :goto_6

    .end local v10    # "result":I
    .end local v20    # "code":I
    .end local v21    # "commandType":Ljava/lang/String;
    .end local v26    # "fileTypes":[Ljava/lang/String;
    .end local v27    # "fileTypesLen":I
    .end local v34    # "mDPM":Landroid/app/admin/DevicePolicyManager;
    .end local v39    # "res":Lcom/android/exchange/EasResponse;
    .end local v42    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v43    # "sizeStr":Ljava/lang/String;
    .restart local v41    # "result":I
    :cond_12
    move/from16 v10, v41

    .end local v41    # "result":I
    .restart local v10    # "result":I
    goto/16 :goto_0
.end method

.method private fetchDocumentCb(JJII)V
    .locals 9
    .param p1, "msgId"    # J
    .param p3, "attId"    # J
    .param p5, "status"    # I
    .param p6, "progress"    # I

    .prologue
    .line 66
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadAttachmentStatus(JJII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 330
    invoke-virtual {p0}, Lcom/android/exchange/EasFetchDocumentSvc;->setupService()Z

    .line 333
    :try_start_0
    sget-object v2, Lcom/android/exchange/EasFetchDocumentSvc;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mDeviceId:Ljava/lang/String;

    .line 335
    invoke-direct {p0}, Lcom/android/exchange/EasFetchDocumentSvc;->fetchDocument()I

    move-result v1

    .line 336
    .local v1, "result":I
    packed-switch v1, :pswitch_data_0

    .line 344
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    :goto_0
    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "fetch document finished: "

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 354
    const-string v2, "fetch document exit status: "

    iget v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I

    invoke-virtual {p0, v2, v3}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 355
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 357
    .end local v1    # "result":I
    :goto_1
    return-void

    .line 338
    .restart local v1    # "result":I
    :pswitch_0
    const/4 v2, 0x2

    :try_start_1
    iput v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 347
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 348
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x1

    :try_start_2
    iput v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 353
    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "fetch document finished: "

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 354
    const-string v2, "fetch document exit status: "

    iget v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I

    invoke-virtual {p0, v2, v3}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 355
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_1

    .line 341
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "result":I
    :pswitch_1
    const/4 v2, 0x3

    :try_start_3
    iput v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 349
    .end local v1    # "result":I
    :catch_1
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "EasFetchDocumentSvc exception: "

    invoke-virtual {p0, v2, v0}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 351
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 353
    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "fetch document finished: "

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 354
    const-string v2, "fetch document exit status: "

    iget v3, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I

    invoke-virtual {p0, v2, v3}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 355
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_1

    .line 353
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "fetch document finished: "

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/android/exchange/EasFetchDocumentSvc;->att:Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog([Ljava/lang/String;)V

    .line 354
    const-string v3, "fetch document exit status: "

    iget v4, p0, Lcom/android/exchange/EasFetchDocumentSvc;->mExitStatus:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasFetchDocumentSvc;->userLog(Ljava/lang/String;I)V

    .line 355
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    throw v2

    .line 336
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
