.class public Lcom/android/exchange/EasDevInfoSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasDevInfoSvc.java"


# instance fields
.field private isIrmtemplateRefreshNeeded:Z

.field private mEnableOutboundSMS:Z

.field private mFriendlyName:Ljava/lang/String;

.field private mImei:Ljava/lang/String;

.field private mMobileOperator:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mOS:Ljava/lang/String;

.field private mOSLanguage:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mTeleohonyManager:Landroid/telephony/TelephonyManager;

.field private mUserAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/EasDevInfoSvc;->isIrmtemplateRefreshNeeded:Z

    .line 84
    iget-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersion:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 87
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mTeleohonyManager:Landroid/telephony/TelephonyManager;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Z)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p3, "isIrmtemplateRefreshNeeded"    # Z

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/EasDevInfoSvc;->isIrmtemplateRefreshNeeded:Z

    .line 76
    iput-boolean p3, p0, Lcom/android/exchange/EasDevInfoSvc;->isIrmtemplateRefreshNeeded:Z

    .line 77
    return-void
.end method

.method private collectDeviceInformation()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 100
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mTeleohonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mImei:Ljava/lang/String;

    .line 106
    sget-object v2, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mModel:Ljava/lang/String;

    .line 107
    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mFriendlyName:Ljava/lang/String;

    .line 108
    const-string v2, "Android"

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mOS:Ljava/lang/String;

    .line 110
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mOSLanguage:Ljava/lang/String;

    .line 111
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mTeleohonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    .line 113
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 114
    const-string v2, "EasDevInfoSvc"

    const-string v3, "mPhoneNumber is null"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mTeleohonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "imsi":Ljava/lang/String;
    const-string v2, "EasDevInfoSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "imsi is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    if-eqz v0, :cond_3

    .line 118
    iput-object v0, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    .line 121
    :goto_0
    const-string v2, "EasDevInfoSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPhoneNumber is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .end local v0    # "imsi":Ljava/lang/String;
    :cond_0
    const-string v2, "EasDevInfoSvc"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mModel="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mModel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mImei="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mImei:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mFriendlyName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mPhoneNumber="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "; mOSLanguage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mOSLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x402c000000000000L    # 14.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_2

    .line 135
    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mUserAgent:Ljava/lang/String;

    .line 136
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v2, :cond_4

    .line 137
    iget-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/android/exchange/EasDevInfoSvc;->mEnableOutboundSMS:Z

    .line 141
    :goto_1
    iget-object v1, p0, Lcom/android/exchange/EasDevInfoSvc;->mTeleohonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasDevInfoSvc;->mMobileOperator:Ljava/lang/String;

    .line 142
    const-string v1, "EasDevInfoSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMobileOperator="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mMobileOperator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_2
    return-void

    .line 120
    .restart local v0    # "imsi":Ljava/lang/String;
    :cond_3
    const-string v2, ""

    iput-object v2, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 139
    .end local v0    # "imsi":Ljava/lang/String;
    :cond_4
    iput-boolean v1, p0, Lcom/android/exchange/EasDevInfoSvc;->mEnableOutboundSMS:Z

    goto :goto_1
.end method

.method private deviceInfoCb(JII)V
    .locals 1
    .param p1, "accId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 92
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->setDeviceInfoStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private refreshIRMTemplatesCb(JII)V
    .locals 1
    .param p1, "accId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 229
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->refreshIRMTemplatesStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setDeviceInformation()I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 149
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v9, v11, v10}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V

    .line 151
    const/4 v3, 0x0

    .line 152
    .local v3, "result":I
    new-instance v4, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v4}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 154
    .local v4, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-direct {p0}, Lcom/android/exchange/EasDevInfoSvc;->collectDeviceInformation()V

    .line 156
    const/16 v7, 0x485

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    const/16 v8, 0x496

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    const/16 v8, 0x488

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 159
    const/16 v7, 0x497

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mModel:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 160
    const/16 v7, 0x498

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mImei:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 161
    const/16 v7, 0x499

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 162
    const/16 v7, 0x49a

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mOS:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 163
    const/16 v7, 0x49b

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mOSLanguage:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 164
    const/16 v7, 0x49c

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 166
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersion:Ljava/lang/String;

    const-string v8, "14.0"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 167
    const/16 v7, 0x4a0

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 168
    const/16 v7, 0x4a1

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v8

    iget-boolean v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mEnableOutboundSMS:Z

    if-ne v7, v11, :cond_2

    const-string v7, "1"

    :goto_0
    invoke-virtual {v8, v7}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 169
    const/16 v7, 0x4a2

    invoke-virtual {v4, v7}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mMobileOperator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 172
    :cond_0
    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    .line 173
    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 175
    const-string v7, "Settings"

    new-instance v8, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const/16 v9, 0x4e20

    invoke-virtual {p0, v7, v8, v9}, Lcom/android/exchange/EasDevInfoSvc;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v2

    .line 179
    .local v2, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v6

    .line 180
    .local v6, "status":I
    const/16 v7, 0xc8

    if-ne v6, v7, :cond_7

    .line 182
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v1

    .line 184
    .local v1, "len":I
    if-eqz v1, :cond_6

    .line 185
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 187
    .local v0, "in":Ljava/io/InputStream;
    new-instance v5, Lcom/android/exchange/adapter/OoOCommandParser;

    new-instance v7, Lcom/android/exchange/adapter/SettingsCommandAdapter;

    iget-object v8, p0, Lcom/android/exchange/EasDevInfoSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v7, v8, p0}, Lcom/android/exchange/adapter/SettingsCommandAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    invoke-direct {v5, v0, v7}, Lcom/android/exchange/adapter/OoOCommandParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 189
    .local v5, "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    invoke-virtual {v5}, Lcom/android/exchange/adapter/OoOCommandParser;->parse()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 190
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v7, 0x0

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v7, v10}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    .end local v0    # "in":Ljava/io/InputStream;
    .end local v1    # "len":I
    .end local v5    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :goto_1
    if-eqz v2, :cond_1

    .line 217
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    .line 220
    :cond_1
    return v3

    .line 168
    .end local v2    # "resp":Lcom/android/exchange/EasResponse;
    .end local v6    # "status":I
    :cond_2
    const-string v7, "0"

    goto :goto_0

    .line 194
    .restart local v0    # "in":Ljava/io/InputStream;
    .restart local v1    # "len":I
    .restart local v2    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v5    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v6    # "status":I
    :cond_3
    :try_start_1
    invoke-virtual {p0, v6}, Lcom/android/exchange/EasDevInfoSvc;->isProvisionError(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 195
    const/16 v3, 0x17

    .line 199
    :goto_2
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v7, 0x64

    invoke-direct {p0, v8, v9, v3, v7}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 216
    .end local v0    # "in":Ljava/io/InputStream;
    .end local v1    # "len":I
    .end local v5    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .end local v6    # "status":I
    :catchall_0
    move-exception v7

    if-eqz v2, :cond_4

    .line 217
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    :cond_4
    throw v7

    .line 197
    .restart local v0    # "in":Ljava/io/InputStream;
    .restart local v1    # "len":I
    .restart local v5    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    .restart local v6    # "status":I
    :cond_5
    const/4 v3, -0x7

    goto :goto_2

    .line 203
    .end local v0    # "in":Ljava/io/InputStream;
    .end local v5    # "sParser":Lcom/android/exchange/adapter/OoOCommandParser;
    :cond_6
    :try_start_2
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v7, 0x0

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v7, v10}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V

    goto :goto_1

    .line 206
    .end local v1    # "len":I
    :cond_7
    const/16 v3, 0x15

    .line 207
    invoke-virtual {p0, v6}, Lcom/android/exchange/EasDevInfoSvc;->isProvisionError(I)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 208
    const/16 v3, 0x17

    .line 212
    :cond_8
    :goto_3
    iget-object v7, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v7, 0x64

    invoke-direct {p0, v8, v9, v3, v7}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V

    goto :goto_1

    .line 209
    :cond_9
    invoke-static {v6}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v7

    if-eqz v7, :cond_8

    .line 210
    const/16 v3, 0x16

    goto :goto_3
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 238
    invoke-virtual {p0}, Lcom/android/exchange/EasDevInfoSvc;->setupService()Z

    .line 240
    iget-boolean v3, p0, Lcom/android/exchange/EasDevInfoSvc;->isIrmtemplateRefreshNeeded:Z

    if-ne v3, v8, :cond_0

    .line 241
    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v8, v9}, Lcom/android/exchange/EasDevInfoSvc;->refreshIRMTemplatesCb(JII)V

    .line 243
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mDeviceId:Ljava/lang/String;

    .line 244
    invoke-virtual {p0}, Lcom/android/exchange/EasDevInfoSvc;->getIRMTemplates()I

    move-result v2

    .line 245
    .local v2, "status":I
    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v3, 0x64

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/android/exchange/EasDevInfoSvc;->refreshIRMTemplatesCb(JII)V

    .line 246
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 329
    .end local v2    # "status":I
    :goto_0
    return-void

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, -0x3

    const/16 v6, 0x64

    invoke-direct {p0, v4, v5, v3, v6}, Lcom/android/exchange/EasDevInfoSvc;->refreshIRMTemplatesCb(JII)V

    .line 250
    const/4 v3, 0x1

    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    throw v3

    .line 258
    :cond_0
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mDeviceId:Ljava/lang/String;

    .line 260
    const/4 v1, 0x0

    .line 264
    .local v1, "result":I
    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 266
    const/4 v1, -0x2

    .line 267
    iget-object v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v3, 0x64

    invoke-direct {p0, v4, v5, v1, v3}, Lcom/android/exchange/EasDevInfoSvc;->deviceInfoCb(JII)V

    .line 271
    :goto_1
    const/16 v3, 0x17

    if-ne v1, v3, :cond_2

    .line 272
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 291
    :goto_2
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "DevInfo finished"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 292
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 293
    iget v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 295
    :pswitch_0
    const/16 v2, 0x17

    .line 301
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_5

    .line 302
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 269
    .end local v2    # "status":I
    :cond_1
    :try_start_3
    invoke-direct {p0}, Lcom/android/exchange/EasDevInfoSvc;->setDeviceInformation()I

    move-result v1

    goto :goto_1

    .line 273
    :cond_2
    const/16 v3, 0x16

    if-ne v1, v3, :cond_3

    .line 274
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 283
    .end local v1    # "result":I
    :catch_1
    move-exception v0

    .line 284
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v3, 0x1

    :try_start_4
    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 291
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "DevInfo finished"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 292
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 293
    iget v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_1

    goto/16 :goto_0

    .line 295
    :pswitch_1
    const/16 v2, 0x17

    .line 301
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_6

    .line 302
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 276
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "status":I
    .restart local v1    # "result":I
    :cond_3
    const/16 v3, 0x15

    if-ne v1, v3, :cond_4

    .line 277
    const/4 v3, 0x3

    :try_start_5
    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 285
    .end local v1    # "result":I
    :catch_2
    move-exception v0

    .line 286
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    const-string v3, "Exception caught in EasOOFSvc"

    invoke-virtual {p0, v3, v0}, Lcom/android/exchange/EasDevInfoSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 287
    const/4 v3, 0x3

    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 291
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "DevInfo finished"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 292
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 293
    iget v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_2

    goto/16 :goto_0

    .line 295
    :pswitch_2
    const/16 v2, 0x17

    .line 301
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_7

    .line 302
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 280
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "status":I
    .restart local v1    # "result":I
    :cond_4
    const/4 v3, 0x0

    :try_start_7
    iput v3, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_2

    .line 290
    .end local v1    # "result":I
    :catchall_1
    move-exception v3

    .line 291
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "DevInfo finished"

    aput-object v5, v4, v9

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 292
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 293
    iget v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mExitStatus:I

    packed-switch v4, :pswitch_data_3

    .line 328
    :goto_3
    throw v3

    .line 304
    .restart local v1    # "result":I
    .restart local v2    # "status":I
    :cond_5
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 305
    sget-object v3, Lcom/android/exchange/EasDevInfoSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 306
    sput-boolean v8, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_0

    .line 304
    .end local v1    # "result":I
    .local v0, "e":Ljava/io/IOException;
    :cond_6
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 305
    sget-object v3, Lcom/android/exchange/EasDevInfoSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 306
    sput-boolean v8, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_0

    .line 304
    .local v0, "e":Ljava/lang/Exception;
    :cond_7
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 305
    sget-object v3, Lcom/android/exchange/EasDevInfoSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 306
    sput-boolean v8, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_0

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "status":I
    :pswitch_3
    const/16 v2, 0x17

    .line 301
    .restart local v2    # "status":I
    sget-boolean v4, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v4, :cond_8

    .line 302
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v5, v4, v9

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    goto :goto_3

    .line 304
    :cond_8
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v5, v4, v9

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasDevInfoSvc;->userLog([Ljava/lang/String;)V

    .line 305
    sget-object v4, Lcom/android/exchange/EasDevInfoSvc;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/EasDevInfoSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 306
    sput-boolean v8, Lcom/android/exchange/EasDevInfoSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto :goto_3

    .line 293
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_3
    .end packed-switch
.end method
