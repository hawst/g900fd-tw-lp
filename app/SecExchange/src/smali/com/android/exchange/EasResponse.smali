.class public Lcom/android/exchange/EasResponse;
.super Ljava/lang/Object;
.source "EasResponse.java"


# instance fields
.field private mClosed:Z

.field private final mEntity:Lorg/apache/http/HttpEntity;

.field private mInputStream:Ljava/io/InputStream;

.field private final mLength:I

.field final mResponse:Lorg/apache/http/HttpResponse;


# direct methods
.method private constructor <init>(Lorg/apache/http/HttpResponse;)V
    .locals 2
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    .line 49
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    .line 50
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/android/exchange/EasResponse;->mLength:I

    .line 55
    :goto_1
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/EasResponse;->mLength:I

    goto :goto_1
.end method

.method public static fromHttpRequest(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/android/exchange/EasResponse;
    .locals 2
    .param p0, "connManager"    # Lorg/apache/http/conn/ClientConnectionManager;
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-interface {p1, p2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 62
    .local v0, "response":Lorg/apache/http/HttpResponse;
    new-instance v1, Lcom/android/exchange/EasResponse;

    invoke-direct {v1, v0}, Lcom/android/exchange/EasResponse;-><init>(Lorg/apache/http/HttpResponse;)V

    .line 64
    .local v1, "result":Lcom/android/exchange/EasResponse;
    return-object v1
.end method

.method public static isAuthError(I)Z
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 73
    const/16 v0, 0x191

    if-eq p0, v0, :cond_0

    const/16 v0, 0x193

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/android/exchange/EasResponse;->mClosed:Z

    if-nez v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    if-eqz v0, :cond_0

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 140
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mInputStream:Ljava/io/InputStream;

    instance-of v0, v0, Ljava/util/zip/GZIPInputStream;

    if-eqz v0, :cond_1

    .line 142
    :try_start_1
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 147
    :cond_1
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/EasResponse;->mClosed:Z

    .line 149
    :cond_2
    return-void

    .line 143
    :catch_0
    move-exception v0

    goto :goto_1

    .line 136
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public getHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0, p1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 6

    .prologue
    .line 91
    iget-object v4, p0, Lcom/android/exchange/EasResponse;->mInputStream:Ljava/io/InputStream;

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/exchange/EasResponse;->mClosed:Z

    if-eqz v4, :cond_1

    .line 92
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Can\'t reuse stream or get closed stream"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 93
    :cond_1
    iget-object v4, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    if-nez v4, :cond_2

    .line 94
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Can\'t get input stream without entity"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 96
    :cond_2
    const/4 v2, 0x0

    .line 99
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v4, p0, Lcom/android/exchange/EasResponse;->mEntity:Lorg/apache/http/HttpEntity;

    invoke-interface {v4}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 100
    iget-object v4, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    const-string v5, "Content-Encoding"

    invoke-interface {v4, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 101
    .local v0, "ceHeader":Lorg/apache/http/Header;
    if-eqz v0, :cond_3

    .line 102
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "encoding":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "gzip"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 105
    new-instance v3, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v3, v2}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "is":Ljava/io/InputStream;
    .local v3, "is":Ljava/io/InputStream;
    move-object v2, v3

    .line 111
    .end local v0    # "ceHeader":Lorg/apache/http/Header;
    .end local v1    # "encoding":Ljava/lang/String;
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :cond_3
    :goto_0
    iput-object v2, p0, Lcom/android/exchange/EasResponse;->mInputStream:Ljava/io/InputStream;

    .line 112
    return-object v2

    .line 109
    :catch_0
    move-exception v4

    goto :goto_0

    .line 108
    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/android/exchange/EasResponse;->mLength:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/android/exchange/EasResponse;->mResponse:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    return v0
.end method
