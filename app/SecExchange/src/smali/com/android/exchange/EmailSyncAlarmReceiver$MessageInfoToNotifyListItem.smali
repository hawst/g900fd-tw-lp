.class Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;
.super Ljava/lang/Object;
.source "EmailSyncAlarmReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/EmailSyncAlarmReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MessageInfoToNotifyListItem"
.end annotation


# instance fields
.field public mailboxId:J

.field public messageId:J

.field final synthetic this$0:Lcom/android/exchange/EmailSyncAlarmReceiver;


# direct methods
.method constructor <init>(Lcom/android/exchange/EmailSyncAlarmReceiver;JJ)V
    .locals 2
    .param p2, "boxId"    # J
    .param p4, "msgId"    # J

    .prologue
    const-wide/16 v0, -0x1

    .line 74
    iput-object p1, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->this$0:Lcom/android/exchange/EmailSyncAlarmReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-wide v0, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->mailboxId:J

    .line 72
    iput-wide v0, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->messageId:J

    .line 75
    iput-wide p2, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->mailboxId:J

    .line 76
    iput-wide p4, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->messageId:J

    .line 77
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 81
    instance-of v0, p1, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 82
    check-cast v0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    iget-wide v0, v0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->mailboxId:J

    iget-wide v2, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->mailboxId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    check-cast p1, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v0, p1, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->messageId:J

    iget-wide v2, p0, Lcom/android/exchange/EmailSyncAlarmReceiver$MessageInfoToNotifyListItem;->messageId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 89
    :goto_0
    return v0

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
