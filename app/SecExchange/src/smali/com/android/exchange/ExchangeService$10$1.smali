.class Lcom/android/exchange/ExchangeService$10$1;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService$10;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/exchange/ExchangeService$10;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService$10;)V
    .locals 0

    .prologue
    .line 5108
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$10$1;->this$1:Lcom/android/exchange/ExchangeService$10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 5112
    const-wide/16 v2, 0x1388

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5115
    :goto_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.exchange"

    const-class v2, Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5117
    .local v0, "compName":Landroid/content/ComponentName;
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$10$1;->this$1:Lcom/android/exchange/ExchangeService$10;

    iget-object v1, v1, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.email.EXCHANGE_INTENT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/ExchangeService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-nez v1, :cond_0

    .line 5120
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$10$1;->this$1:Lcom/android/exchange/ExchangeService$10;

    iget-object v1, v1, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5121
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$10$1;->this$1:Lcom/android/exchange/ExchangeService$10;

    iget-object v1, v1, Lcom/android/exchange/ExchangeService$10;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/emailcommon/service/AccountServiceProxy;->mBindable:Z

    .line 5123
    :cond_0
    return-void

    .line 5113
    .end local v0    # "compName":Landroid/content/ComponentName;
    :catch_0
    move-exception v1

    goto :goto_0
.end method
