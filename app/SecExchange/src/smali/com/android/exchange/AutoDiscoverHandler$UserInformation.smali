.class Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/AutoDiscoverHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserInformation"
.end annotation


# instance fields
.field private mDomain:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mUserName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "domain_user"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;

    .line 140
    iput-object p2, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;

    .line 141
    iput-object p3, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/exchange/AutoDiscoverHandler$UserInformation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$UserInformation;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "autodiscover user info : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 149
    const-string v1, " user:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 151
    const-string v1, "domain:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$UserInformation;->mDomain:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
