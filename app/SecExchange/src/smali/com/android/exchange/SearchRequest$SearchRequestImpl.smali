.class Lcom/android/exchange/SearchRequest$SearchRequestImpl;
.super Lcom/android/exchange/SearchRequest;
.source "SearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/SearchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchRequestImpl"
.end annotation


# instance fields
.field private mAccountId:J

.field private mConversationId:Ljava/lang/String;

.field private mOptionsBodyPreferenceTruncationSize:I

.field private mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

.field private mOptionsDeepTraversal:Z

.field private mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

.field private mOptionsRange:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mOptionsRebuildResults:Z

.field private mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

.field private mQueryCollectionIds:[J

.field private mQueryFreeText:Ljava/lang/String;

.field private mQueryGreaterThan:Ljava/util/Date;

.field private mQueryLessThan:Ljava/util/Date;

.field private mQueryText:Ljava/lang/String;

.field private mStoreName:Lcom/android/exchange/SearchRequest$StoreName;


# direct methods
.method public constructor <init>(JLcom/android/exchange/SearchRequest$StoreName;Ljava/lang/String;Ljava/lang/String;Lcom/android/exchange/SearchRequest$QueryClass;[JLjava/util/Date;Ljava/util/Date;Landroid/util/Pair;ZZLcom/android/exchange/SearchRequest$BodyPreferenceType;ILcom/android/exchange/SearchRequest$OptionsMIMESupport;Ljava/lang/String;)V
    .locals 3
    .param p1, "accountId"    # J
    .param p3, "storeName"    # Lcom/android/exchange/SearchRequest$StoreName;
    .param p4, "queryText"    # Ljava/lang/String;
    .param p5, "queryFreeText"    # Ljava/lang/String;
    .param p6, "queryClass"    # Lcom/android/exchange/SearchRequest$QueryClass;
    .param p7, "queryCollectionIds"    # [J
    .param p8, "queryGreaterThan"    # Ljava/util/Date;
    .param p9, "queryLessThan"    # Ljava/util/Date;
    .param p11, "optionsDeepTraversal"    # Z
    .param p12, "optionsRebuildResults"    # Z
    .param p13, "optionsBodyPreferenceType"    # Lcom/android/exchange/SearchRequest$BodyPreferenceType;
    .param p14, "optionsBodyPreferenceTruncationSize"    # I
    .param p15, "optionsMIMESupport"    # Lcom/android/exchange/SearchRequest$OptionsMIMESupport;
    .param p16, "convId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/android/exchange/SearchRequest$StoreName;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/android/exchange/SearchRequest$QueryClass;",
            "[J",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;ZZ",
            "Lcom/android/exchange/SearchRequest$BodyPreferenceType;",
            "I",
            "Lcom/android/exchange/SearchRequest$OptionsMIMESupport;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    .local p10, "optionsRange":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/exchange/SearchRequest;-><init>(Lcom/android/exchange/SearchRequest$1;)V

    .line 129
    iput-wide p1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mAccountId:J

    .line 130
    iput-object p3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    .line 131
    iput-object p4, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryText:Ljava/lang/String;

    .line 132
    iput-object p5, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryFreeText:Ljava/lang/String;

    .line 133
    iput-object p6, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    .line 134
    iput-object p7, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    .line 135
    iput-object p8, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryGreaterThan:Ljava/util/Date;

    .line 136
    iput-object p9, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryLessThan:Ljava/util/Date;

    .line 137
    iput-object p10, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    .line 138
    iput-boolean p11, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsDeepTraversal:Z

    .line 139
    iput-boolean p12, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRebuildResults:Z

    .line 140
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .line 141
    move/from16 v0, p14

    iput v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceTruncationSize:I

    .line 142
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .line 144
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mConversationId:Ljava/lang/String;

    .line 146
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 251
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/android/exchange/SearchRequest;-><init>(Lcom/android/exchange/SearchRequest$1;)V

    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mAccountId:J

    .line 253
    invoke-static {}, Lcom/android/exchange/SearchRequest$StoreName;->values()[Lcom/android/exchange/SearchRequest$StoreName;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryText:Ljava/lang/String;

    .line 255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryFreeText:Ljava/lang/String;

    .line 256
    invoke-static {}, Lcom/android/exchange/SearchRequest$QueryClass;->values()[Lcom/android/exchange/SearchRequest$QueryClass;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 258
    .local v0, "collectionIdsSize":I
    new-array v3, v0, [J

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    .line 259
    iget-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readLongArray([J)V

    .line 262
    new-instance v3, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryGreaterThan:Ljava/util/Date;

    .line 263
    new-instance v3, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryLessThan:Ljava/util/Date;

    .line 264
    new-instance v3, Landroid/util/Pair;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    .line 265
    new-array v1, v7, [Z

    .line 266
    .local v1, "deepTraversalVal":[Z
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 267
    aget-boolean v3, v1, v6

    iput-boolean v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsDeepTraversal:Z

    .line 268
    new-array v2, v7, [Z

    .line 269
    .local v2, "rebuildResultsVal":[Z
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 270
    aget-boolean v3, v2, v6

    iput-boolean v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRebuildResults:Z

    .line 271
    invoke-static {}, Lcom/android/exchange/SearchRequest$BodyPreferenceType;->values()[Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceTruncationSize:I

    .line 273
    invoke-static {}, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->values()[Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mConversationId:Ljava/lang/String;

    .line 277
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/android/exchange/SearchRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/android/exchange/SearchRequest$1;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/exchange/SearchRequest$SearchRequestImpl;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public getOptionsDeepTraversal()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsDeepTraversal:Z

    return v0
.end method

.method public getOptionsOptionsMIMESupport()Ljava/lang/String;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    invoke-virtual {v0}, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->INVALID:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    invoke-virtual {v1}, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 248
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    invoke-virtual {v1}, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOptionsRange()Ljava/lang/String;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    if-nez v0, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getOptionsRebuildResults()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRebuildResults:Z

    return v0
.end method

.method public getQueryClass()Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    sget-object v0, Lcom/android/exchange/SearchRequest$2;->$SwitchMap$com$android$exchange$SearchRequest$QueryClass:[I

    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    invoke-virtual {v1}, Lcom/android/exchange/SearchRequest$QueryClass;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 181
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 179
    :pswitch_0
    const-string v0, "Email"

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getQueryCollectionIds()[J
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    return-object v0
.end method

.method public getQueryConvIdText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mConversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryFreeText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryFreeText:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryGreaterThan()Ljava/lang/String;
    .locals 4

    .prologue
    .line 190
    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryGreaterThan:Ljava/util/Date;

    if-nez v1, :cond_0

    .line 191
    const/4 v1, 0x0

    .line 195
    :goto_0
    return-object v1

    .line 193
    :cond_0
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/GregorianCalendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 194
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryGreaterThan:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 195
    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->formatDateTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getQueryLessThan()Ljava/lang/String;
    .locals 4

    .prologue
    .line 199
    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryLessThan:Ljava/util/Date;

    if-nez v1, :cond_0

    .line 200
    const/4 v1, 0x0

    .line 204
    :goto_0
    return-object v1

    .line 202
    :cond_0
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/GregorianCalendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 203
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryLessThan:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 204
    invoke-static {v0}, Lcom/android/exchange/utility/CalendarUtilities;->formatDateTime(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method public getStoreName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 153
    sget-object v0, Lcom/android/exchange/SearchRequest$2;->$SwitchMap$com$android$exchange$SearchRequest$StoreName:[I

    iget-object v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    invoke-virtual {v1}, Lcom/android/exchange/SearchRequest$StoreName;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 157
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 155
    :pswitch_0
    const-string v0, "Mailbox"

    goto :goto_0

    .line 153
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 284
    iget-wide v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mAccountId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 285
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    invoke-virtual {v0}, Lcom/android/exchange/SearchRequest$StoreName;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 286
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryFreeText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    invoke-virtual {v0}, Lcom/android/exchange/SearchRequest$QueryClass;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 289
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryCollectionIds:[J

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 291
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryGreaterThan:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 292
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mQueryLessThan:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 293
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRange:Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 295
    new-array v0, v3, [Z

    iget-boolean v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsDeepTraversal:Z

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 298
    new-array v0, v3, [Z

    iget-boolean v1, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsRebuildResults:Z

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 301
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    invoke-virtual {v0}, Lcom/android/exchange/SearchRequest$BodyPreferenceType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 302
    iget v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsBodyPreferenceTruncationSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    invoke-virtual {v0}, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    iget-object v0, p0, Lcom/android/exchange/SearchRequest$SearchRequestImpl;->mConversationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 307
    return-void
.end method
