.class public Lcom/android/exchange/EasFolderOperationSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasFolderOperationSvc.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mDeleteContext:Landroid/content/Context;

.field private mIds:[J

.field private mIdsFlag:[Z

.field private mNewFolderName:Ljava/lang/String;

.field private mOperation:I

.field private mParentFolderID:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;JI)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;
    .param p3, "folderName"    # Ljava/lang/String;
    .param p4, "parentFolderId"    # J
    .param p6, "operation"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 26
    const-string v0, "Folder Operation"

    iput-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    const-string v1, "EasFolderOperationSvc"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iput p6, p0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    .line 42
    iput-object p3, p0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    .line 43
    iput-wide p4, p0, Lcom/android/exchange/EasFolderOperationSvc;->mParentFolderID:J

    .line 44
    iget-wide v0, p2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v2, 0x44

    invoke-static {p1, v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    .line 46
    iget-wide v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    invoke-static {p1, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p3, "operation"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 26
    const-string v0, "Folder Operation"

    iput-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    .line 57
    iput p3, p0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p3, "newName"    # Ljava/lang/String;
    .param p4, "operation"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 26
    const-string v0, "Folder Operation"

    iput-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    .line 51
    iput p4, p0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    .line 52
    iput-object p3, p0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[J[ZLjava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ids"    # [J
    .param p3, "IdsFalg"    # [Z
    .param p4, "newName"    # Ljava/lang/String;
    .param p5, "operation"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    aget-wide v0, p2, v0

    invoke-direct {p0, p1, v0, v1}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;J)V

    .line 26
    const-string v0, "Folder Operation"

    iput-object v0, p0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    .line 63
    iput-object p1, p0, Lcom/android/exchange/EasFolderOperationSvc;->mDeleteContext:Landroid/content/Context;

    .line 64
    iput p5, p0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    .line 65
    iput-object p4, p0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/android/exchange/EasFolderOperationSvc;->mIds:[J

    .line 67
    iput-object p3, p0, Lcom/android/exchange/EasFolderOperationSvc;->mIdsFlag:[Z

    .line 68
    return-void
.end method


# virtual methods
.method getMailboxOpFlag()I
    .locals 2

    .prologue
    .line 290
    const/4 v0, 0x4

    .line 291
    .local v0, "mailboxFlag":I
    iget v1, p0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    packed-switch v1, :pswitch_data_0

    .line 307
    :goto_0
    return v0

    .line 293
    :pswitch_0
    const/4 v0, 0x4

    .line 294
    goto :goto_0

    .line 296
    :pswitch_1
    const/4 v0, 0x2

    .line 297
    goto :goto_0

    .line 301
    :pswitch_2
    const/4 v0, 0x1

    .line 302
    goto :goto_0

    .line 304
    :pswitch_3
    const/16 v0, 0x10

    goto :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public run()V
    .locals 20

    .prologue
    .line 72
    const/16 v17, -0x1

    .line 73
    .local v17, "resultStatus":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    const-string v4, "EasFolderOperationSvc.run"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->setupService()Z

    .line 77
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mDeviceId:Ljava/lang/String;

    .line 78
    const-string v14, ""

    .line 79
    .local v14, "operation":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_7

    .line 80
    new-instance v2, Lcom/android/exchange/adapter/FolderCreateAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/exchange/EasFolderOperationSvc;->mParentFolderID:J

    move-object/from16 v4, p0

    invoke-direct/range {v2 .. v7}, Lcom/android/exchange/adapter/FolderCreateAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;Ljava/lang/String;J)V

    .line 82
    .local v2, "createAdapter":Lcom/android/exchange/adapter/FolderCreateAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 83
    invoke-virtual {v2}, Lcom/android/exchange/adapter/FolderCreateAdapter;->getSatus()I
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    .line 84
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_0

    .line 86
    :try_start_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :cond_0
    :goto_0
    :try_start_2
    const-string v14, "folder create"

    .line 196
    .end local v2    # "createAdapter":Lcom/android/exchange/adapter/FolderCreateAdapter;
    :cond_1
    :goto_1
    const-string v16, ""

    .line 197
    .local v16, "resultDef":Ljava/lang/String;
    if-nez v17, :cond_19

    .line 198
    const-string v16, "Success"

    .line 207
    :cond_2
    :goto_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "thread="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->getThreadId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAccount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " command="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " result="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/exchange/ServiceLogger;->logFolderOperationStats(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 220
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-nez v3, :cond_1c

    .line 221
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync finished exit status :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 222
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 225
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_0

    .line 273
    :pswitch_0
    const/16 v18, 0x15

    .line 274
    .local v18, "status":I
    const-string v3, "Sync ended due to an exception."

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->errorLog(Ljava/lang/String;)V

    .line 282
    .end local v18    # "status":I
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-eqz v3, :cond_4

    .line 283
    const-string v3, "sync finished"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 284
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exit EasFolderOperationSvc Thread id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .end local v14    # "operation":Ljava/lang/String;
    .end local v16    # "resultDef":Ljava/lang/String;
    :goto_4
    return-void

    .line 88
    .restart local v2    # "createAdapter":Lcom/android/exchange/adapter/FolderCreateAdapter;
    .restart local v14    # "operation":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 89
    .local v9, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 211
    .end local v2    # "createAdapter":Lcom/android/exchange/adapter/FolderCreateAdapter;
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v14    # "operation":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 212
    .local v9, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_4
    const-string v3, "DeviceAccessException"

    const-string v4, "EasFolderOperationSvc::run"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const/4 v3, 0x6

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    .line 214
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v3, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v4, v5, v3}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 220
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-nez v3, :cond_1d

    .line 221
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync finished exit status :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 222
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 225
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_1

    .line 273
    :pswitch_1
    const/16 v18, 0x15

    .line 274
    .restart local v18    # "status":I
    const-string v3, "Sync ended due to an exception."

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->errorLog(Ljava/lang/String;)V

    .line 282
    .end local v9    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .end local v18    # "status":I
    :cond_5
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-eqz v3, :cond_6

    .line 283
    const-string v3, "sync finished"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 284
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exit EasFolderOperationSvc Thread id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 93
    .restart local v14    # "operation":Ljava/lang/String;
    :cond_7
    :try_start_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xb

    if-ne v3, v4, :cond_c

    .line 94
    new-instance v19, Lcom/android/exchange/adapter/FolderRenameAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/android/exchange/adapter/FolderRenameAdapter;-><init>(Lcom/android/exchange/EasSyncService;Ljava/lang/String;)V

    .line 96
    .local v19, "updateAdapter":Lcom/android/exchange/adapter/FolderRenameAdapter;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 97
    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/FolderRenameAdapter;->getSatus()I
    :try_end_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v17

    .line 98
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_8

    .line 100
    :try_start_6
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 106
    :cond_8
    :goto_6
    :try_start_7
    const-string v14, "folder rename"

    .line 107
    goto/16 :goto_1

    .line 102
    :catch_2
    move-exception v9

    .line 103
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_6

    .line 215
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v14    # "operation":Ljava/lang/String;
    .end local v19    # "updateAdapter":Lcom/android/exchange/adapter/FolderRenameAdapter;
    :catch_3
    move-exception v9

    .line 216
    .local v9, "e":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    .line 217
    .local v12, "message":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Caught IOException: "

    aput-object v5, v3, v4

    const/4 v4, 0x1

    if-nez v12, :cond_9

    const-string v12, "No message"

    .end local v12    # "message":Ljava/lang/String;
    :cond_9
    aput-object v12, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->userLog([Ljava/lang/String;)V

    .line 218
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 220
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-nez v3, :cond_1e

    .line 221
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync finished exit status :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 222
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 225
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_2

    .line 273
    :pswitch_2
    const/16 v18, 0x15

    .line 274
    .restart local v18    # "status":I
    const-string v3, "Sync ended due to an exception."

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->errorLog(Ljava/lang/String;)V

    .line 282
    .end local v9    # "e":Ljava/io/IOException;
    .end local v18    # "status":I
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-eqz v3, :cond_b

    .line 283
    const-string v3, "sync finished"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 284
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exit EasFolderOperationSvc Thread id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 107
    .restart local v14    # "operation":Ljava/lang/String;
    :cond_c
    :try_start_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xc

    if-ne v3, v4, :cond_10

    .line 108
    new-instance v13, Lcom/android/exchange/adapter/FolderMoveAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3, v4}, Lcom/android/exchange/adapter/FolderMoveAdapter;-><init>(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Z)V

    .line 109
    .local v13, "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 110
    invoke-virtual {v13}, Lcom/android/exchange/adapter/FolderMoveAdapter;->getSatus()I
    :try_end_9
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v17

    .line 111
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_d

    .line 113
    :try_start_a
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 119
    :cond_d
    :goto_8
    :try_start_b
    const-string v14, "folder move"

    .line 121
    goto/16 :goto_1

    .line 115
    :catch_4
    move-exception v9

    .line 116
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_b
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_8

    .line 220
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v13    # "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    .end local v14    # "operation":Ljava/lang/String;
    :catchall_0
    move-exception v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-nez v4, :cond_1f

    .line 221
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync finished exit status :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 222
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 225
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mExitStatus:I

    packed-switch v4, :pswitch_data_3

    .line 273
    :pswitch_3
    const/16 v18, 0x15

    .line 274
    .restart local v18    # "status":I
    const-string v4, "Sync ended due to an exception."

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasFolderOperationSvc;->errorLog(Ljava/lang/String;)V

    .line 282
    .end local v18    # "status":I
    :cond_e
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mStop:Z

    if-eqz v4, :cond_f

    .line 283
    const-string v4, "sync finished"

    invoke-static {v4}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 284
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exit EasFolderOperationSvc Thread id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasFolderOperationSvc;->mThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 121
    .restart local v14    # "operation":Ljava/lang/String;
    :cond_10
    :try_start_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xe

    if-ne v3, v4, :cond_12

    .line 122
    new-instance v13, Lcom/android/exchange/adapter/FolderMoveAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3, v4}, Lcom/android/exchange/adapter/FolderMoveAdapter;-><init>(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Z)V

    .line 123
    .restart local v13    # "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 124
    invoke-virtual {v13}, Lcom/android/exchange/adapter/FolderMoveAdapter;->getSatus()I
    :try_end_c
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result v17

    .line 125
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_11

    .line 127
    :try_start_d
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_d .. :try_end_d} :catch_1
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 133
    :cond_11
    :goto_a
    :try_start_e
    const-string v14, "folder move to trash"

    .line 134
    goto/16 :goto_1

    .line 129
    :catch_5
    move-exception v9

    .line 130
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_a

    .line 134
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v13    # "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xd

    if-ne v3, v4, :cond_14

    .line 135
    new-instance v8, Lcom/android/exchange/adapter/FolderDeleteAdapter;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/android/exchange/adapter/FolderDeleteAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 136
    .local v8, "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 137
    invoke-virtual {v8}, Lcom/android/exchange/adapter/FolderDeleteAdapter;->getSatus()I
    :try_end_e
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_e .. :try_end_e} :catch_1
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result v17

    .line 138
    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_13

    .line 140
    :try_start_f
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_6
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 146
    :cond_13
    :goto_b
    :try_start_10
    const-string v14, "folder delete"

    .line 147
    goto/16 :goto_1

    .line 142
    :catch_6
    move-exception v9

    .line 143
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_b

    .line 147
    .end local v8    # "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    .end local v9    # "e":Landroid/os/RemoteException;
    :cond_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xf

    if-ne v3, v4, :cond_1

    .line 148
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->TAG:Ljava/lang/String;

    const-string v4, "FO_MULTIDELETE_Easfolderoperationsvc"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v15, 0x0

    .line 150
    .local v15, "operationResert":I
    const/4 v10, 0x0

    .line 151
    .local v10, "inDex":I
    const/4 v10, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mIds:[J

    array-length v3, v3

    if-ge v10, v3, :cond_15

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mIdsFlag:[Z

    aget-boolean v3, v3, v10

    const/4 v4, 0x1

    if-ne v3, v4, :cond_17

    .line 153
    new-instance v8, Lcom/android/exchange/adapter/FolderDeleteAdapter;

    new-instance v3, Lcom/android/exchange/EasSyncService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mDeleteContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mIds:[J

    aget-wide v6, v5, v10

    invoke-direct {v3, v4, v6, v7}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;J)V

    invoke-direct {v8, v3}, Lcom/android/exchange/adapter/FolderDeleteAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 154
    .restart local v8    # "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 155
    invoke-virtual {v8}, Lcom/android/exchange/adapter/FolderDeleteAdapter;->getSatus()I
    :try_end_10
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    move-result v17

    .line 156
    if-eqz v17, :cond_18

    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_18

    .line 158
    add-int/lit8 v15, v15, 0x1

    .line 160
    :try_start_11
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_3
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 186
    .end local v8    # "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    :cond_15
    :goto_d
    :try_start_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mIds:[J

    array-length v3, v3
    :try_end_12
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    if-ne v10, v3, :cond_16

    if-nez v15, :cond_16

    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_16

    .line 188
    :try_start_13
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    const/4 v5, 0x0

    invoke-interface {v3, v4, v6, v7, v5}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 194
    :cond_16
    :goto_e
    :try_start_14
    const-string v14, "folder delete"

    goto/16 :goto_1

    .line 162
    .restart local v8    # "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    :catch_7
    move-exception v9

    .line 163
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_d

    .line 170
    .end local v8    # "deleteAdapter":Lcom/android/exchange/adapter/FolderDeleteAdapter;
    .end local v9    # "e":Landroid/os/RemoteException;
    :cond_17
    new-instance v13, Lcom/android/exchange/adapter/FolderMoveAdapter;

    new-instance v3, Lcom/android/exchange/EasSyncService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mDeleteContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mIds:[J

    aget-wide v6, v5, v10

    invoke-direct {v3, v4, v6, v7}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;J)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mNewFolderName:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct {v13, v3, v4, v5}, Lcom/android/exchange/adapter/FolderMoveAdapter;-><init>(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Z)V

    .line 171
    .restart local v13    # "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/android/exchange/EasFolderOperationSvc;->processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V

    .line 172
    invoke-virtual {v13}, Lcom/android/exchange/adapter/FolderMoveAdapter;->getSatus()I
    :try_end_14
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_3
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    move-result v17

    .line 173
    if-eqz v17, :cond_18

    const/4 v3, -0x1

    move/from16 v0, v17

    if-eq v0, v3, :cond_18

    .line 175
    add-int/lit8 v15, v15, 0x1

    .line 177
    :try_start_15
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v17

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_8
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_15 .. :try_end_15} :catch_1
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_d

    .line 179
    :catch_8
    move-exception v9

    .line 180
    .restart local v9    # "e":Landroid/os/RemoteException;
    :try_start_16
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_d

    .line 151
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v13    # "moveAdapter":Lcom/android/exchange/adapter/FolderMoveAdapter;
    :cond_18
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_c

    .line 190
    :catch_9
    move-exception v9

    .line 191
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_e

    .line 199
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v10    # "inDex":I
    .end local v15    # "operationResert":I
    .restart local v16    # "resultDef":Ljava/lang/String;
    :cond_19
    const/16 v3, 0x20

    move/from16 v0, v17

    if-ne v0, v3, :cond_1a

    .line 200
    const-string v16, "Connection error"

    goto/16 :goto_2

    .line 201
    :cond_1a
    const/16 v3, 0x22

    move/from16 v0, v17

    if-ne v0, v3, :cond_1b

    .line 202
    const-string v16, "server error"

    goto/16 :goto_2

    .line 203
    :cond_1b
    const/16 v3, 0x25

    move/from16 v0, v17

    if-ne v0, v3, :cond_2

    .line 204
    const-string v16, "alread exsist error"
    :try_end_16
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_16 .. :try_end_16} :catch_1
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_3
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_2

    .line 227
    :pswitch_4
    const/16 v18, 0x20

    .line 229
    .restart local v18    # "status":I
    :try_start_17
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->getMailboxOpFlag()I

    move-result v11

    .line 230
    .local v11, "mailboxFlag":I
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v11, v4, v5, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_a

    goto/16 :goto_3

    .line 232
    .end local v11    # "mailboxFlag":I
    :catch_a
    move-exception v9

    .line 233
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_3

    .line 237
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v18    # "status":I
    :pswitch_5
    const/16 v18, 0x0

    .line 239
    .restart local v18    # "status":I
    :try_start_18
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_3

    .line 240
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    const/4 v6, 0x0

    move/from16 v0, v18

    invoke-interface {v3, v4, v5, v0, v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_b

    goto/16 :goto_3

    .line 242
    :catch_b
    move-exception v9

    .line 243
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_3

    .line 247
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v18    # "status":I
    :pswitch_6
    const/16 v18, 0x16

    .line 248
    .restart local v18    # "status":I
    goto/16 :goto_3

    .line 250
    .end local v18    # "status":I
    :pswitch_7
    const/16 v18, 0x17

    .line 251
    .restart local v18    # "status":I
    sget-object v3, Lcom/android/exchange/EasFolderOperationSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_3

    .line 254
    .end local v18    # "status":I
    :pswitch_8
    const/16 v18, 0x3

    .line 255
    .restart local v18    # "status":I
    goto/16 :goto_3

    .line 258
    .end local v18    # "status":I
    :pswitch_9
    const/16 v18, 0x4

    .line 259
    .restart local v18    # "status":I
    goto/16 :goto_3

    .line 262
    .end local v18    # "status":I
    :pswitch_a
    const/16 v18, 0x14

    .line 264
    .restart local v18    # "status":I
    :try_start_19
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_c

    goto/16 :goto_3

    .line 267
    :catch_c
    move-exception v3

    goto/16 :goto_3

    .line 280
    .end local v18    # "status":I
    :cond_1c
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Stopped FolderCreateSvc finished"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_3

    .line 227
    .end local v14    # "operation":Ljava/lang/String;
    .end local v16    # "resultDef":Ljava/lang/String;
    .local v9, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :pswitch_b
    const/16 v18, 0x20

    .line 229
    .restart local v18    # "status":I
    :try_start_1a
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->getMailboxOpFlag()I

    move-result v11

    .line 230
    .restart local v11    # "mailboxFlag":I
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v11, v4, v5, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_1a
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1a} :catch_d

    goto/16 :goto_5

    .line 232
    .end local v11    # "mailboxFlag":I
    :catch_d
    move-exception v9

    .line 233
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_5

    .line 237
    .end local v18    # "status":I
    .local v9, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :pswitch_c
    const/16 v18, 0x0

    .line 239
    .restart local v18    # "status":I
    :try_start_1b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_5

    .line 240
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    const/4 v6, 0x0

    move/from16 v0, v18

    invoke-interface {v3, v4, v5, v0, v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_e

    goto/16 :goto_5

    .line 242
    :catch_e
    move-exception v9

    .line 243
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_5

    .line 247
    .end local v18    # "status":I
    .local v9, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :pswitch_d
    const/16 v18, 0x16

    .line 248
    .restart local v18    # "status":I
    goto/16 :goto_5

    .line 250
    .end local v18    # "status":I
    :pswitch_e
    const/16 v18, 0x17

    .line 251
    .restart local v18    # "status":I
    sget-object v3, Lcom/android/exchange/EasFolderOperationSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_5

    .line 254
    .end local v18    # "status":I
    :pswitch_f
    const/16 v18, 0x3

    .line 255
    .restart local v18    # "status":I
    goto/16 :goto_5

    .line 258
    .end local v18    # "status":I
    :pswitch_10
    const/16 v18, 0x4

    .line 259
    .restart local v18    # "status":I
    goto/16 :goto_5

    .line 262
    .end local v18    # "status":I
    :pswitch_11
    const/16 v18, 0x14

    .line 264
    .restart local v18    # "status":I
    :try_start_1c
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_f

    goto/16 :goto_5

    .line 267
    :catch_f
    move-exception v3

    goto/16 :goto_5

    .line 280
    .end local v18    # "status":I
    :cond_1d
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Stopped FolderCreateSvc finished"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_5

    .line 227
    .local v9, "e":Ljava/io/IOException;
    :pswitch_12
    const/16 v18, 0x20

    .line 229
    .restart local v18    # "status":I
    :try_start_1d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->getMailboxOpFlag()I

    move-result v11

    .line 230
    .restart local v11    # "mailboxFlag":I
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v11, v4, v5, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_10

    goto/16 :goto_7

    .line 232
    .end local v11    # "mailboxFlag":I
    :catch_10
    move-exception v9

    .line 233
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_7

    .line 237
    .end local v18    # "status":I
    .local v9, "e":Ljava/io/IOException;
    :pswitch_13
    const/16 v18, 0x0

    .line 239
    .restart local v18    # "status":I
    :try_start_1e
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_a

    .line 240
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    const/4 v6, 0x0

    move/from16 v0, v18

    invoke-interface {v3, v4, v5, v0, v6}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_11

    goto/16 :goto_7

    .line 242
    :catch_11
    move-exception v9

    .line 243
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_7

    .line 247
    .end local v18    # "status":I
    .local v9, "e":Ljava/io/IOException;
    :pswitch_14
    const/16 v18, 0x16

    .line 248
    .restart local v18    # "status":I
    goto/16 :goto_7

    .line 250
    .end local v18    # "status":I
    :pswitch_15
    const/16 v18, 0x17

    .line 251
    .restart local v18    # "status":I
    sget-object v3, Lcom/android/exchange/EasFolderOperationSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_7

    .line 254
    .end local v18    # "status":I
    :pswitch_16
    const/16 v18, 0x3

    .line 255
    .restart local v18    # "status":I
    goto/16 :goto_7

    .line 258
    .end local v18    # "status":I
    :pswitch_17
    const/16 v18, 0x4

    .line 259
    .restart local v18    # "status":I
    goto/16 :goto_7

    .line 262
    .end local v18    # "status":I
    :pswitch_18
    const/16 v18, 0x14

    .line 264
    .restart local v18    # "status":I
    :try_start_1f
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v3

    const/16 v4, 0x20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v3, v4, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_1f} :catch_12

    goto/16 :goto_7

    .line 267
    :catch_12
    move-exception v3

    goto/16 :goto_7

    .line 280
    .end local v18    # "status":I
    :cond_1e
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "Stopped FolderCreateSvc finished"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasFolderOperationSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_7

    .line 227
    .end local v9    # "e":Ljava/io/IOException;
    :pswitch_19
    const/16 v18, 0x20

    .line 229
    .restart local v18    # "status":I
    :try_start_20
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasFolderOperationSvc;->getMailboxOpFlag()I

    move-result v11

    .line 230
    .restart local v11    # "mailboxFlag":I
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v4, v11, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_20} :catch_13

    goto/16 :goto_9

    .line 232
    .end local v11    # "mailboxFlag":I
    :catch_13
    move-exception v9

    .line 233
    .local v9, "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_9

    .line 237
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v18    # "status":I
    :pswitch_1a
    const/16 v18, 0x0

    .line 239
    .restart local v18    # "status":I
    :try_start_21
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/exchange/EasFolderOperationSvc;->mOperation:I

    const/16 v5, 0xa

    if-ne v4, v5, :cond_e

    .line 240
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailboxId:J

    const/4 v5, 0x0

    move/from16 v0, v18

    invoke-interface {v4, v6, v7, v0, v5}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_21} :catch_14

    goto/16 :goto_9

    .line 242
    :catch_14
    move-exception v9

    .line 243
    .restart local v9    # "e":Landroid/os/RemoteException;
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_9

    .line 247
    .end local v9    # "e":Landroid/os/RemoteException;
    .end local v18    # "status":I
    :pswitch_1b
    const/16 v18, 0x16

    .line 248
    .restart local v18    # "status":I
    goto/16 :goto_9

    .line 250
    .end local v18    # "status":I
    :pswitch_1c
    const/16 v18, 0x17

    .line 251
    .restart local v18    # "status":I
    sget-object v4, Lcom/android/exchange/EasFolderOperationSvc;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasFolderOperationSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v5, 0x1

    invoke-static {v4, v6, v7, v5}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_9

    .line 254
    .end local v18    # "status":I
    :pswitch_1d
    const/16 v18, 0x3

    .line 255
    .restart local v18    # "status":I
    goto/16 :goto_9

    .line 258
    .end local v18    # "status":I
    :pswitch_1e
    const/16 v18, 0x4

    .line 259
    .restart local v18    # "status":I
    goto/16 :goto_9

    .line 262
    .end local v18    # "status":I
    :pswitch_1f
    const/16 v18, 0x14

    .line 264
    .restart local v18    # "status":I
    :try_start_22
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    const/16 v5, 0x20

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasFolderOperationSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move/from16 v0, v18

    invoke-interface {v4, v5, v6, v7, v0}, Lcom/android/emailcommon/service/IEmailServiceCallback;->folderCommandStatus(IJI)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_22} :catch_15

    goto/16 :goto_9

    .line 267
    :catch_15
    move-exception v4

    goto/16 :goto_9

    .line 280
    .end local v18    # "status":I
    :cond_1f
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Stopped FolderCreateSvc finished"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasFolderOperationSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_9

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_1
        :pswitch_e
        :pswitch_1
        :pswitch_f
        :pswitch_10
        :pswitch_1
        :pswitch_1
        :pswitch_11
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_13
        :pswitch_12
        :pswitch_14
        :pswitch_2
        :pswitch_15
        :pswitch_2
        :pswitch_16
        :pswitch_17
        :pswitch_2
        :pswitch_2
        :pswitch_18
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_1a
        :pswitch_19
        :pswitch_1b
        :pswitch_3
        :pswitch_1c
        :pswitch_3
        :pswitch_1d
        :pswitch_1e
        :pswitch_3
        :pswitch_3
        :pswitch_1f
    .end packed-switch
.end method
