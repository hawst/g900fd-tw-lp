.class public Lcom/android/exchange/Preferences;
.super Ljava/lang/Object;
.source "Preferences.java"


# static fields
.field private static sPreferences:Lcom/android/exchange/Preferences;


# instance fields
.field final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v0, "AndroidMail.Main"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 42
    return-void
.end method

.method public static declared-synchronized getPreferences(Landroid/content/Context;)Lcom/android/exchange/Preferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const-class v1, Lcom/android/exchange/Preferences;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/exchange/Preferences;->sPreferences:Lcom/android/exchange/Preferences;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/android/exchange/Preferences;

    invoke-direct {v0, p0}, Lcom/android/exchange/Preferences;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/exchange/Preferences;->sPreferences:Lcom/android/exchange/Preferences;

    .line 54
    :cond_0
    sget-object v0, Lcom/android/exchange/Preferences;->sPreferences:Lcom/android/exchange/Preferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getActivationLicense()Ljava/lang/String;
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/exchange/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "activationLicense"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDebugBits()I
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/exchange/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "DebugBits"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public setActivationLicense(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/exchange/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "activationLicense"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 67
    return-void
.end method

.method public setDebugBits(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/exchange/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DebugBits"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 77
    return-void
.end method
