.class Lcom/android/exchange/ExchangeService$TaskObserver;
.super Landroid/database/ContentObserver;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskObserver"
.end annotation


# instance fields
.field bSync:Z

.field mAccountId:J

.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# virtual methods
.method public declared-synchronized onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 2772
    monitor-enter p0

    :try_start_0
    const-string v0, "TASK OBSERVER"

    const-string v1, "Task Obeserver onChange"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/exchange/ExchangeService$TaskObserver$1;

    invoke-direct {v1, p0}, Lcom/android/exchange/ExchangeService$TaskObserver$1;-><init>(Lcom/android/exchange/ExchangeService$TaskObserver;)V

    const-string v2, "Task Observer"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2823
    monitor-exit p0

    return-void

    .line 2772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
