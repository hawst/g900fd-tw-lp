.class public Lcom/android/exchange/EasEmptyTrashSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasEmptyTrashSvc.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 2
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 39
    iget-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersion:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 43
    return-void
.end method

.method private emptyTrash()I
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/16 v12, 0xc8

    .line 297
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    .line 299
    const/4 v4, 0x0

    .line 301
    .local v4, "result":I
    invoke-direct {p0, v13, v13}, Lcom/android/exchange/EasEmptyTrashSvc;->makeEmptyTrashRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;

    move-result-object v2

    .line 304
    .local v2, "res":Lcom/android/exchange/EasResponse;
    if-nez v2, :cond_1

    .line 305
    const/4 v4, 0x0

    .line 397
    if-eqz v2, :cond_0

    .line 398
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    :cond_0
    move v5, v4

    .line 401
    .end local v4    # "result":I
    .local v5, "result":I
    :goto_0
    return v5

    .line 310
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_1
    :try_start_0
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 312
    .local v0, "code":I
    const-string v8, "emptyTrash(): sendHttpClientPost HTTP response code: "

    invoke-virtual {p0, v8, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 313
    if-ne v0, v12, :cond_c

    .line 314
    invoke-direct {p0, v2}, Lcom/android/exchange/EasEmptyTrashSvc;->parseEmptyTrashResponse(Lcom/android/exchange/EasResponse;)I

    move-result v6

    .line 315
    .local v6, "status":I
    sparse-switch v6, :sswitch_data_0

    .line 373
    invoke-virtual {p0, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->isProvisionError(I)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 374
    const/16 v4, 0x17

    .line 382
    :goto_1
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v4, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    .end local v6    # "status":I
    :goto_2
    if-eqz v2, :cond_2

    .line 398
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    :cond_2
    move v5, v4

    .line 401
    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 318
    .end local v5    # "result":I
    .restart local v4    # "result":I
    .restart local v6    # "status":I
    :sswitch_0
    :try_start_1
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mUserName:Ljava/lang/String;

    iget-object v9, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mPassword:Ljava/lang/String;

    invoke-direct {p0, v8, v9}, Lcom/android/exchange/EasEmptyTrashSvc;->makeEmptyTrashRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 320
    .local v3, "resSecondRequest":Lcom/android/exchange/EasResponse;
    if-nez v3, :cond_4

    .line 321
    const/4 v4, 0x0

    .line 397
    if-eqz v2, :cond_3

    .line 398
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    :cond_3
    move v5, v4

    .end local v4    # "result":I
    .restart local v5    # "result":I
    goto :goto_0

    .line 324
    .end local v5    # "result":I
    .restart local v4    # "result":I
    :cond_4
    :try_start_2
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v1

    .line 325
    .local v1, "codeSecondRequest":I
    const-string v8, "emptyTrash(): sendHttpClientPost HTTP response code: "

    invoke-virtual {p0, v8, v1}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 327
    if-ne v0, v12, :cond_8

    .line 328
    invoke-direct {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->parseEmptyTrashResponse(Lcom/android/exchange/EasResponse;)I

    move-result v7

    .line 329
    .local v7, "statusSecondRequest":I
    packed-switch v7, :pswitch_data_0

    .line 342
    invoke-virtual {p0, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->isProvisionError(I)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 343
    const/16 v4, 0x17

    .line 351
    :goto_3
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v4, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 397
    .end local v0    # "code":I
    .end local v1    # "codeSecondRequest":I
    .end local v3    # "resSecondRequest":Lcom/android/exchange/EasResponse;
    .end local v6    # "status":I
    .end local v7    # "statusSecondRequest":I
    :catchall_0
    move-exception v8

    if-eqz v2, :cond_5

    .line 398
    invoke-virtual {v2}, Lcom/android/exchange/EasResponse;->close()V

    :cond_5
    throw v8

    .line 331
    .restart local v0    # "code":I
    .restart local v1    # "codeSecondRequest":I
    .restart local v3    # "resSecondRequest":Lcom/android/exchange/EasResponse;
    .restart local v6    # "status":I
    .restart local v7    # "statusSecondRequest":I
    :pswitch_0
    const/4 v4, 0x0

    .line 332
    goto :goto_2

    .line 344
    :cond_6
    :try_start_3
    invoke-static {v0}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 345
    const/16 v4, 0x16

    goto :goto_3

    .line 347
    :cond_7
    const/16 v4, 0x15

    goto :goto_3

    .line 355
    .end local v7    # "statusSecondRequest":I
    :cond_8
    const/16 v4, 0x15

    .line 356
    invoke-static {v0}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 357
    const/16 v4, 0x16

    .line 359
    :cond_9
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v4, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto :goto_2

    .line 364
    .end local v1    # "codeSecondRequest":I
    .end local v3    # "resSecondRequest":Lcom/android/exchange/EasResponse;
    :sswitch_1
    const/4 v4, 0x0

    .line 365
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v4, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto :goto_2

    .line 375
    :cond_a
    invoke-static {v0}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 376
    const/16 v4, 0x16

    goto/16 :goto_1

    .line 378
    :cond_b
    const/16 v4, 0x15

    goto/16 :goto_1

    .line 385
    .end local v6    # "status":I
    :cond_c
    const/16 v4, 0x15

    .line 387
    invoke-virtual {p0, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->isProvisionError(I)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 388
    const/16 v4, 0x17

    .line 393
    :cond_d
    :goto_4
    iget-object v8, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v4, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    .line 394
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 389
    :cond_e
    invoke-static {v0}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v8

    if-eqz v8, :cond_d

    .line 390
    const/16 v4, 0x16

    goto :goto_4

    .line 315
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x19 -> :sswitch_0
    .end sparse-switch

    .line 329
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private emptyTrashCb(JII)V
    .locals 1
    .param p1, "accId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 49
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->emptyTrashStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private makeEmptyTrashRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;
    .locals 2
    .param p1, "login"    # Ljava/lang/String;
    .param p2, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 287
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-direct {p0, v0, p1, p2}, Lcom/android/exchange/EasEmptyTrashSvc;->prepareCommand(Lcom/android/exchange/adapter/Serializer;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    invoke-direct {p0, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->sendCommand(Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/EasResponse;

    move-result-object v1

    return-object v1
.end method

.method private parseEmptyTrashResponse(Lcom/android/exchange/EasResponse;)I
    .locals 10
    .param p1, "res"    # Lcom/android/exchange/EasResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 203
    const/16 v4, 0x15

    .line 205
    .local v4, "ret_value":I
    if-eqz p1, :cond_0

    .line 219
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v3

    .line 221
    .local v3, "len":I
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 223
    .local v2, "is":Ljava/io/InputStream;
    sget-object v6, Lcom/android/exchange/EasEmptyTrashSvc;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "contentLength = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-ltz v3, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    if-eqz v2, :cond_5

    .line 233
    :try_start_0
    new-instance v1, Lcom/android/exchange/adapter/ItemOperationsParser;

    new-instance v5, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    invoke-direct {v5, p0}, Lcom/android/exchange/adapter/ItemOperationsAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-direct {v1, v2, v5}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 237
    .local v1, "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    invoke-virtual {v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 239
    invoke-virtual {v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I

    move-result v5

    const/16 v6, 0x12

    if-ne v5, v6, :cond_2

    iget-object v5, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    const-wide/high16 v8, 0x402c000000000000L    # 14.0

    cmpl-double v5, v6, v8

    if-ltz v5, :cond_2

    .line 243
    const/16 v4, 0x19

    .line 277
    .end local v1    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "len":I
    :cond_0
    :goto_1
    return v4

    .line 223
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "len":I
    :cond_1
    const-string v5, "Unknown"

    goto :goto_0

    .line 245
    .restart local v1    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 247
    const/4 v4, 0x0

    goto :goto_1

    .line 251
    :cond_3
    const/high16 v4, 0x10000

    goto :goto_1

    .line 259
    :cond_4
    const/high16 v4, 0x10000

    goto :goto_1

    .line 263
    .end local v1    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    :catch_0
    move-exception v0

    .line 265
    .local v0, "ioe":Ljava/io/IOException;
    const/high16 v4, 0x10000

    .line 267
    goto :goto_1

    .line 271
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_5
    const/16 v4, 0x15

    goto :goto_1
.end method

.method private prepareCommand(Lcom/android/exchange/adapter/Serializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;
    .param p2, "login"    # Ljava/lang/String;
    .param p3, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    if-eqz p1, :cond_1

    .line 65
    iget-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    .line 69
    const/16 v0, 0x505

    invoke-virtual {p1, v0}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x512

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x508

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    const/16 v1, 0x513

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->tag(I)Lcom/android/exchange/adapter/Serializer;

    .line 77
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x402c000000000000L    # 14.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 79
    const/16 v0, 0x514

    invoke-virtual {p1, v0, p2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 81
    const/16 v0, 0x515

    invoke-virtual {p1, v0, p3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 95
    :cond_1
    return-void
.end method

.method private sendCommand(Lcom/android/exchange/adapter/Serializer;)Lcom/android/exchange/EasResponse;
    .locals 12
    .param p1, "s"    # Lcom/android/exchange/adapter/Serializer;

    .prologue
    .line 99
    const/4 v5, 0x0

    .line 101
    .local v5, "ret_value":Lcom/android/exchange/EasResponse;
    const/4 v2, 0x0

    .line 103
    .local v2, "commandType":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    const-wide/high16 v10, 0x4028000000000000L    # 12.0

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_0

    .line 105
    const-string v2, "ItemOperations"

    .line 109
    :cond_0
    if-eqz p1, :cond_3

    .line 115
    :try_start_0
    sget-boolean v7, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v7, :cond_1

    .line 117
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "emptyTrash(): Wbxml:"

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 119
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v0

    .line 121
    .local v0, "b":[B
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 123
    .local v1, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v4, Lcom/android/exchange/adapter/LogAdapter;

    invoke-direct {v4, p0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 125
    .local v4, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v4, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 131
    .end local v0    # "b":[B
    .end local v1    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v4    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_1
    if-nez v2, :cond_2

    .line 132
    const/4 v7, 0x0

    .line 190
    :goto_0
    return-object v7

    .line 134
    :cond_2
    invoke-virtual {p1}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p0, v2, v7}, Lcom/android/exchange/EasEmptyTrashSvc;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    :cond_3
    :goto_1
    move-object v7, v5

    .line 190
    goto :goto_0

    .line 138
    :catch_0
    move-exception v6

    .line 152
    .local v6, "ste":Ljava/net/SocketTimeoutException;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "emptyTrash(): Exception obtained: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 154
    iget-object v7, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/high16 v7, 0x60000

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v7, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    .line 161
    const/4 v5, 0x0

    .line 186
    goto :goto_1

    .line 167
    .end local v6    # "ste":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v3

    .line 173
    .local v3, "e":Ljava/lang/Exception;
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "emptyTrash(): Exception obtained: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 175
    iget-object v7, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v7, -0x3

    const/16 v10, 0x64

    invoke-direct {p0, v8, v9, v7, v10}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    .line 182
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/16 v11, 0x64

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 409
    invoke-virtual {p0}, Lcom/android/exchange/EasEmptyTrashSvc;->setupService()Z

    .line 413
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mDeviceId:Ljava/lang/String;

    .line 417
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    .line 425
    const/4 v1, 0x0

    .line 427
    .local v1, "result":I
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, -0x2

    const/16 v6, 0x64

    invoke-direct {p0, v4, v5, v3, v6}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    .line 435
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 461
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    :goto_1
    new-array v3, v10, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v9

    const-string v4, ": sync finished"

    aput-object v4, v3, v8

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 493
    const-string v3, "OoB exited with status "

    iget v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 497
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 499
    iget v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_1

    .line 545
    .end local v1    # "result":I
    :goto_2
    :pswitch_0
    return-void

    .line 431
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrash()I

    move-result v1

    .restart local v1    # "result":I
    goto :goto_0

    .line 441
    :pswitch_1
    const/4 v3, 0x4

    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 467
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 469
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_2
    const-string v3, "DeviceAccessPermission"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Caught Exceptoin, Device is blocked or quarantined "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/DeviceAccessException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/4 v3, 0x6

    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    .line 475
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v3, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v4, v5, v3}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 491
    new-array v3, v10, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v9

    const-string v4, ": sync finished"

    aput-object v4, v3, v8

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 493
    const-string v3, "OoB exited with status "

    iget v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 497
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 499
    iget v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_2

    :pswitch_2
    goto :goto_2

    .line 503
    :pswitch_3
    const/16 v2, 0x17

    .line 515
    .local v2, "status":I
    sget-boolean v3, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_2

    .line 517
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    goto :goto_2

    .line 449
    .end local v0    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .end local v2    # "status":I
    .restart local v1    # "result":I
    :pswitch_4
    const/4 v3, 0x2

    :try_start_3
    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 477
    .end local v1    # "result":I
    :catch_1
    move-exception v0

    .line 479
    .local v0, "e":Ljava/io/IOException;
    const/4 v3, 0x1

    :try_start_4
    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 491
    new-array v3, v10, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v9

    const-string v4, ": sync finished"

    aput-object v4, v3, v8

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 493
    const-string v3, "OoB exited with status "

    iget v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 497
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 499
    iget v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_3

    :pswitch_5
    goto/16 :goto_2

    .line 503
    :pswitch_6
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_3

    .line 517
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 455
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "status":I
    .restart local v1    # "result":I
    :pswitch_7
    const/4 v3, 0x3

    :try_start_5
    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 481
    .end local v1    # "result":I
    :catch_2
    move-exception v0

    .line 483
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    const-string v3, "Exception caught in EasEmptyTrashSvc"

    invoke-virtual {p0, v3, v0}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 485
    const/4 v3, 0x3

    iput v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 491
    new-array v3, v10, [Ljava/lang/String;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v9

    const-string v4, ": sync finished"

    aput-object v4, v3, v8

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 493
    const-string v3, "OoB exited with status "

    iget v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    invoke-virtual {p0, v3, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 497
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 499
    iget v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    packed-switch v3, :pswitch_data_4

    :pswitch_8
    goto/16 :goto_2

    .line 503
    :pswitch_9
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_4

    .line 517
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 503
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "status":I
    .restart local v1    # "result":I
    :pswitch_a
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v3, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v3, :cond_1

    .line 517
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 521
    :cond_1
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 523
    sget-object v3, Lcom/android/exchange/EasEmptyTrashSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 525
    sput-boolean v8, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_2

    .line 535
    .end local v2    # "status":I
    :pswitch_b
    const/4 v2, 0x3

    .line 537
    .restart local v2    # "status":I
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v2, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto/16 :goto_2

    .line 521
    .end local v1    # "result":I
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :cond_2
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 523
    sget-object v3, Lcom/android/exchange/EasEmptyTrashSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 525
    sput-boolean v8, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_2

    .line 535
    .end local v2    # "status":I
    :pswitch_c
    const/4 v2, 0x3

    .line 537
    .restart local v2    # "status":I
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v2, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto/16 :goto_2

    .line 521
    .local v0, "e":Ljava/io/IOException;
    :cond_3
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 523
    sget-object v3, Lcom/android/exchange/EasEmptyTrashSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 525
    sput-boolean v8, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_2

    .line 535
    .end local v2    # "status":I
    :pswitch_d
    const/4 v2, 0x3

    .line 537
    .restart local v2    # "status":I
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v2, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto/16 :goto_2

    .line 521
    .local v0, "e":Ljava/lang/Exception;
    :cond_4
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v4, v3, v9

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 523
    sget-object v3, Lcom/android/exchange/EasEmptyTrashSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 525
    sput-boolean v8, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_2

    .line 535
    .end local v2    # "status":I
    :pswitch_e
    const/4 v2, 0x3

    .line 537
    .restart local v2    # "status":I
    iget-object v3, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v2, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto/16 :goto_2

    .line 489
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "status":I
    :catchall_0
    move-exception v3

    .line 491
    new-array v4, v10, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    aput-object v5, v4, v9

    const-string v5, ": sync finished"

    aput-object v5, v4, v8

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 493
    const-string v4, "OoB exited with status "

    iget v5, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    invoke-virtual {p0, v4, v5}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog(Ljava/lang/String;I)V

    .line 497
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 499
    iget v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mExitStatus:I

    packed-switch v4, :pswitch_data_5

    .line 543
    :goto_3
    :pswitch_f
    throw v3

    .line 503
    :pswitch_10
    const/16 v2, 0x17

    .line 515
    .restart local v2    # "status":I
    sget-boolean v4, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-eqz v4, :cond_5

    .line 517
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS true case!!!"

    aput-object v5, v4, v9

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    goto :goto_3

    .line 521
    :cond_5
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS false case!!!"

    aput-object v5, v4, v9

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasEmptyTrashSvc;->userLog([Ljava/lang/String;)V

    .line 523
    sget-object v4, Lcom/android/exchange/EasEmptyTrashSvc;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v4, v6, v7, v8}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 525
    sput-boolean v8, Lcom/android/exchange/EasEmptyTrashSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto :goto_3

    .line 535
    .end local v2    # "status":I
    :pswitch_11
    const/4 v2, 0x3

    .line 537
    .restart local v2    # "status":I
    iget-object v4, p0, Lcom/android/exchange/EasEmptyTrashSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v4, v5, v2, v11}, Lcom/android/exchange/EasEmptyTrashSvc;->emptyTrashCb(JII)V

    goto :goto_3

    .line 435
    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_7
        :pswitch_4
        :pswitch_1
    .end packed-switch

    .line 499
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_a
        :pswitch_0
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_3
        :pswitch_2
        :pswitch_c
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_6
        :pswitch_5
        :pswitch_d
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x4
        :pswitch_9
        :pswitch_8
        :pswitch_e
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x4
        :pswitch_10
        :pswitch_f
        :pswitch_11
    .end packed-switch
.end method
