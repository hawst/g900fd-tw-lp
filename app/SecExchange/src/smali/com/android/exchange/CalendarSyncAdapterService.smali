.class public Lcom/android/exchange/CalendarSyncAdapterService;
.super Landroid/app/Service;
.source "CalendarSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final ID_SERVER_ID_TYPE_PROJECTION:[Ljava/lang/String;

.field private static final ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

.field private static sSyncAdapter:Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

.field private static final sSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "serverId"

    aput-object v1, v0, v3

    const-string v1, "type"

    aput-object v1, v0, v4

    const-string v1, "displayName"

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->ID_SERVER_ID_TYPE_PROJECTION:[Ljava/lang/String;

    .line 75
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "syncKey"

    aput-object v1, v0, v3

    const-string v1, "syncInterval"

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 88
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/accounts/Account;
    .param p2, "x2"    # Landroid/os/Bundle;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Landroid/content/ContentProviderClient;
    .param p5, "x5"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-static/range {p0 .. p5}, Lcom/android/exchange/CalendarSyncAdapterService;->performSync(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V

    return-void
.end method

.method private static performSync(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 136
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-boolean v13, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    .line 137
    .local v13, "logging":Z
    const-string v3, "upload"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 138
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const-string v5, "dirty=1 AND account_name=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 143
    .local v9, "c":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 144
    const-string v3, "Exchange"

    const-string v4, "Calendar Sync , Cursor is null, maybe bind transaction failed"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 148
    .restart local v9    # "c":Landroid/database/Cursor;
    :cond_1
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 161
    if-eqz v9, :cond_0

    .line 162
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 161
    :cond_2
    if-eqz v9, :cond_3

    .line 162
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 168
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_3
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent;->CONTROLED_SYNC_PROJECTION:[Ljava/lang/String;

    const-string v5, "emailAddress=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v21, v0

    aput-object v21, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 180
    .local v8, "accountCursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    .line 184
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 185
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 190
    .local v10, "accountId":J
    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 192
    .local v18, "syncInterval":J
    const-wide/16 v4, -0x1

    cmp-long v3, v18, v4

    if-nez v3, :cond_5

    .line 193
    const-string v3, "force"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "ignore_settings"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v3

    if-nez v3, :cond_5

    .line 263
    if-eqz v8, :cond_0

    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 161
    .end local v8    # "accountCursor":Landroid/database/Cursor;
    .end local v10    # "accountId":J
    .end local v18    # "syncInterval":J
    .restart local v9    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_4

    .line 162
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v3

    .line 209
    .end local v9    # "c":Landroid/database/Cursor;
    .restart local v8    # "accountCursor":Landroid/database/Cursor;
    .restart local v10    # "accountId":J
    .restart local v18    # "syncInterval":J
    :cond_5
    const/4 v3, 0x6

    :try_start_2
    move-object/from16 v0, p0

    invoke-static {v0, v10, v11, v3}, Lcom/android/exchange/ExchangeService;->getFolderServerId(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v20

    .line 210
    .local v20, "trashServerId":Ljava/lang/String;
    if-nez v20, :cond_6

    .line 211
    const-string v3, "EAS CalendarSyncAdapterService"

    const-string v4, "Calendar SubFolder, TRASH serverId coming as NULL"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 263
    if-eqz v8, :cond_0

    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 214
    :cond_6
    :try_start_3
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/exchange/CalendarSyncAdapterService;->ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

    const-string v5, "accountKey=? AND ( type=65 OR ( type=82 AND parentServerId !=? ))"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v6, v7

    const/4 v7, 0x1

    aput-object v20, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v15

    .line 219
    .local v15, "mailboxCursor":Landroid/database/Cursor;
    :goto_1
    if-eqz v15, :cond_d

    :try_start_4
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 223
    const/4 v3, 0x1

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v17

    .line 226
    .local v17, "syncKey":Ljava/lang/String;
    const/4 v14, -0x2

    .line 228
    .local v14, "mSyncInterval":I
    const/4 v3, 0x2

    :try_start_5
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v14

    .line 232
    :goto_2
    :try_start_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "performSync() for Calendar: mSyncInterval :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 239
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->checkServiceExist(J)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 240
    if-eqz v13, :cond_7

    .line 241
    const-string v3, "EAS CalendarSyncAdapterService"

    const-string v4, "Can\'t sync; mailbox in initial state, or manual sync already active"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 257
    :cond_7
    if-eqz v15, :cond_8

    .line 258
    :try_start_7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 263
    :cond_8
    if-eqz v8, :cond_0

    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 229
    :catch_0
    move-exception v16

    .line 230
    .local v16, "ne":Ljava/lang/NullPointerException;
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    .line 253
    .end local v14    # "mSyncInterval":I
    .end local v16    # "ne":Ljava/lang/NullPointerException;
    .end local v17    # "syncKey":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 254
    .local v12, "e":Ljava/lang/Exception;
    :try_start_9
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 257
    if-eqz v15, :cond_9

    .line 258
    :try_start_a
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 263
    .end local v10    # "accountId":J
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v15    # "mailboxCursor":Landroid/database/Cursor;
    .end local v18    # "syncInterval":J
    .end local v20    # "trashServerId":Ljava/lang/String;
    :cond_9
    :goto_3
    if-eqz v8, :cond_0

    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 248
    .restart local v10    # "accountId":J
    .restart local v14    # "mSyncInterval":I
    .restart local v15    # "mailboxCursor":Landroid/database/Cursor;
    .restart local v17    # "syncKey":Ljava/lang/String;
    .restart local v18    # "syncInterval":J
    .restart local v20    # "trashServerId":Ljava/lang/String;
    :cond_a
    const/4 v3, 0x0

    :try_start_b
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v3, 0x0

    invoke-static {v4, v5, v3}, Lcom/android/exchange/ExchangeService;->serviceRequest(JI)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_1

    .line 257
    .end local v14    # "mSyncInterval":I
    .end local v17    # "syncKey":Ljava/lang/String;
    :catchall_1
    move-exception v3

    if-eqz v15, :cond_b

    .line 258
    :try_start_c
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v3
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 263
    .end local v10    # "accountId":J
    .end local v15    # "mailboxCursor":Landroid/database/Cursor;
    .end local v18    # "syncInterval":J
    .end local v20    # "trashServerId":Ljava/lang/String;
    :catchall_2
    move-exception v3

    if-eqz v8, :cond_c

    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v3

    .line 257
    .restart local v10    # "accountId":J
    .restart local v15    # "mailboxCursor":Landroid/database/Cursor;
    .restart local v18    # "syncInterval":J
    .restart local v20    # "trashServerId":Ljava/lang/String;
    :cond_d
    if-eqz v15, :cond_9

    .line 258
    :try_start_d
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto :goto_3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 123
    sget-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 114
    sget-object v1, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    sget-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {p0}, Lcom/android/exchange/CalendarSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/exchange/CalendarSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/CalendarSyncAdapterService$SyncAdapterImpl;

    .line 118
    :cond_0
    monitor-exit v1

    .line 119
    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
