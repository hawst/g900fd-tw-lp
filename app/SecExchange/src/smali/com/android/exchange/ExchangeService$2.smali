.class Lcom/android/exchange/ExchangeService$2;
.super Lcom/android/emailcommon/service/IEmailService$Stub;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;)V
    .locals 0

    .prologue
    .line 890
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Lcom/android/emailcommon/service/IEmailService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public OoOffice(JLcom/android/emailcommon/service/OoODataList;)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "data"    # Lcom/android/emailcommon/service/OoODataList;

    .prologue
    .line 1408
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1410
    .local v0, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_0

    .line 1411
    sget-wide v4, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_OoO:J

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->removeFromSyncErrorMap(J)V

    .line 1412
    new-instance v1, Lcom/android/exchange/EasOoOSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v1, v3, v0, p3}, Lcom/android/exchange/EasOoOSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/emailcommon/service/OoODataList;)V

    .line 1413
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "(OoOSvc)"

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1415
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1421
    .end local v1    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v2    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method

.method public autoDiscover(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 9
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "domain"    # Ljava/lang/String;
    .param p4, "bTrustCert"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 976
    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    iget-object v8, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v1 .. v8}, Lcom/android/exchange/AutoDiscoverHandler;->tryAutodiscoverWithLock(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLandroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public cancelAutoDiscover(Ljava/lang/String;)V
    .locals 4
    .param p1, "username"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2026
    const-string v1, "ExchangeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Canceling autodiscover for user : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2027
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/AutoDiscoverHandler;->getLock(Ljava/lang/String;)Lcom/android/exchange/AutoDiscoverHandler;

    move-result-object v0

    .line 2029
    .local v0, "handler":Lcom/android/exchange/AutoDiscoverHandler;
    if-eqz v0, :cond_0

    .line 2030
    invoke-virtual {v0}, Lcom/android/exchange/AutoDiscoverHandler;->cancelAutodiscover()V

    .line 2036
    :goto_0
    return-void

    .line 2032
    :cond_0
    const-string v1, "ExchangeService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not able to cancel autodiscover handler is null for user : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cancelLoadAttachmentEAS(J)V
    .locals 3
    .param p1, "attachmentId"    # J

    .prologue
    const/4 v2, 0x0

    .line 1327
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v1, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v0

    .line 1328
    .local v0, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v0, :cond_0

    .line 1329
    new-instance v1, Lcom/android/exchange/PartRequest;

    invoke-direct {v1, v0, v2, v2}, Lcom/android/exchange/PartRequest;-><init>(Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->sendMessageRequestCancel(Lcom/android/exchange/PartRequest;)V

    .line 1331
    :cond_0
    return-void
.end method

.method public changeSmsSettings(J)V
    .locals 11
    .param p1, "accountId"    # J

    .prologue
    .line 1865
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1866
    .local v0, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_0

    .line 1867
    new-instance v1, Lcom/android/exchange/EasSyncService;

    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v1, v5, v0}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 1868
    .local v1, "deviceInfoSvc":Lcom/android/exchange/EasSyncService;
    if-eqz v1, :cond_0

    .line 1869
    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v1, v5}, Lcom/android/exchange/EasSyncService;->setupAdhocService(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1871
    const-wide/16 v6, 0x0

    .line 1873
    .local v6, "protocolVersion":D
    :try_start_0
    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v6

    .line 1877
    :goto_0
    :try_start_1
    new-instance v4, Lcom/android/exchange/DeviceInformation;

    invoke-direct {v4, v6, v7}, Lcom/android/exchange/DeviceInformation;-><init>(D)V

    .line 1878
    .local v4, "mDInfo":Lcom/android/exchange/DeviceInformation;
    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v5, v8, v0}, Lcom/android/exchange/DeviceInformation;->prepareDeviceInformation(Landroid/content/Context;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1880
    new-instance v2, Lcom/android/exchange/DeviceInformationTask;

    invoke-direct {v2, v4}, Lcom/android/exchange/DeviceInformationTask;-><init>(Lcom/android/exchange/DeviceInformation;)V

    .line 1882
    .local v2, "deviceInfoTask":Lcom/android/exchange/DeviceInformationTask;
    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v2, v5, v1}, Lcom/android/exchange/DeviceInformationTask;->setUpService(Landroid/content/Context;Lcom/android/exchange/EasSyncService;)V

    .line 1884
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v5}, Lcom/android/exchange/DeviceInformationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1895
    .end local v1    # "deviceInfoSvc":Lcom/android/exchange/EasSyncService;
    .end local v2    # "deviceInfoTask":Lcom/android/exchange/DeviceInformationTask;
    .end local v4    # "mDInfo":Lcom/android/exchange/DeviceInformation;
    .end local v6    # "protocolVersion":D
    :cond_0
    :goto_1
    return-void

    .line 1874
    .restart local v1    # "deviceInfoSvc":Lcom/android/exchange/EasSyncService;
    .restart local v6    # "protocolVersion":D
    :catch_0
    move-exception v3

    .line 1875
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 1886
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 1887
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1888
    const-string v5, "Email"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Illegal argument: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public deleteAccountPIMData(J)V
    .locals 1
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1807
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->stopAccountSyncs(J)V

    .line 1809
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->deleteAccountPIMData(J)V

    .line 1810
    return-void
.end method

.method public emptyTrash(J)V
    .locals 9
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1724
    iget-object v5, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v6, 0x6

    invoke-static {v5, p1, p2, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v2

    .line 1726
    .local v2, "mailboxKey":J
    iget-object v5, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v5, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1727
    .local v0, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_0

    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v5, :cond_0

    .line 1728
    new-instance v1, Lcom/android/exchange/EasEmptyTrashSvc;

    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v1, v5, v0}, Lcom/android/exchange/EasEmptyTrashSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 1729
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "(EasEmptyTrashSvc)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1730
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 1735
    .end local v1    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v4    # "thread":Ljava/lang/Thread;
    :goto_0
    return-void

    .line 1732
    :cond_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v5

    const/16 v6, 0x20

    const/16 v7, 0x64

    invoke-interface {v5, p1, p2, v6, v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->emptyTrashStatus(JII)V

    goto :goto_0
.end method

.method public folderCreate(JLjava/lang/String;J)V
    .locals 8
    .param p1, "accountId"    # J
    .param p3, "folderName"    # Ljava/lang/String;
    .param p4, "parentMailboxId"    # J

    .prologue
    .line 1425
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v1, :cond_1

    .line 1439
    :cond_0
    :goto_0
    return-void

    .line 1427
    :cond_1
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v2

    .line 1428
    .local v2, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v2, :cond_0

    .line 1429
    new-instance v0, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/16 v6, 0xa

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;JI)V

    .line 1434
    .local v0, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v7, Ljava/lang/Thread;

    const-string v1, "(FolderCreate)"

    invoke-direct {v7, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1437
    .local v7, "thread":Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public folderDelete(J)V
    .locals 11
    .param p1, "mailboxId"    # J

    .prologue
    const/4 v10, 0x6

    .line 1470
    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v7, :cond_1

    .line 1496
    :cond_0
    :goto_0
    return-void

    .line 1472
    :cond_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1473
    .local v0, "mbx":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, p1, p2}, Lcom/android/exchange/provider/MailboxUtilities;->getParentMailboxId(Landroid/content/Context;J)J

    move-result-wide v2

    .line 1474
    .local v2, "parentId":J
    const/4 v6, 0x1

    .line 1475
    .local v6, "type":I
    const-wide/16 v8, -0x1

    cmp-long v7, v2, v8

    if-eqz v7, :cond_2

    .line 1476
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->getMailboxType(Landroid/content/Context;J)I

    move-result v6

    .line 1477
    :cond_2
    if-eqz v0, :cond_0

    .line 1478
    if-ne v6, v10, :cond_3

    .line 1479
    new-instance v1, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/16 v8, 0xd

    invoke-direct {v1, v7, v0, v8}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;I)V

    .line 1481
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v4, Ljava/lang/Thread;

    const-string v7, "(folderDelete)"

    invoke-direct {v4, v1, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1484
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1486
    .end local v1    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v4    # "thread":Ljava/lang/Thread;
    :cond_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v7, v8, v9, v10}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v5

    .line 1488
    .local v5, "trash":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v5, :cond_0

    .line 1489
    new-instance v1, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v7, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-object v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    const/16 v9, 0xe

    invoke-direct {v1, v7, v0, v8, v9}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/lang/String;I)V

    .line 1491
    .restart local v1    # "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v4, Ljava/lang/Thread;

    const-string v7, "(folderDeleteMove)"

    invoke-direct {v4, v1, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1492
    .restart local v4    # "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public folderMove(JLjava/lang/String;)V
    .locals 5
    .param p1, "mailboxId"    # J
    .param p3, "toMailboxServerId"    # Ljava/lang/String;

    .prologue
    .line 1456
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_1

    .line 1467
    :cond_0
    :goto_0
    return-void

    .line 1458
    :cond_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1459
    .local v0, "mbx":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_0

    .line 1460
    new-instance v1, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/16 v4, 0xc

    invoke-direct {v1, v3, v0, p3, v4}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/lang/String;I)V

    .line 1462
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "(folderMove)"

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1465
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public folderRename(JLjava/lang/String;)V
    .locals 5
    .param p1, "mailboxId"    # J
    .param p3, "toName"    # Ljava/lang/String;

    .prologue
    .line 1442
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_1

    .line 1453
    :cond_0
    :goto_0
    return-void

    .line 1444
    :cond_1
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1445
    .local v0, "mbx":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_0

    .line 1446
    new-instance v1, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/16 v4, 0xb

    invoke-direct {v1, v3, v0, p3, v4}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Ljava/lang/String;I)V

    .line 1448
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "(folderRename)"

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1451
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public foldersDelete([J)V
    .locals 19
    .param p1, "deleteMailboxIds"    # [J

    .prologue
    .line 1499
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_1

    .line 1550
    :cond_0
    :goto_0
    return-void

    .line 1501
    :cond_1
    move-object/from16 v0, p1

    array-length v3, v0

    new-array v5, v3, [Z

    .line 1502
    .local v5, "deleteFlag":[Z
    move-object/from16 v0, p1

    array-length v3, v0

    new-array v0, v3, [Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v16, v0

    .line 1506
    .local v16, "mmbx":[Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const-string v3, "ExchangeService"

    const-string v4, "foldersdelete command_exchangservice"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    move-object/from16 v0, p1

    array-length v3, v0

    if-ge v11, v3, :cond_7

    .line 1509
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    aget-wide v6, p1, v11

    invoke-static {v3, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    aput-object v3, v16, v11

    .line 1514
    aget-wide v8, p1, v11

    .line 1515
    .local v8, "checkParentId":J
    const/4 v10, 0x1

    .line 1516
    .local v10, "flagIsroot":Z
    const/4 v12, 0x0

    .line 1517
    .local v12, "loopEndcnt":I
    :cond_2
    if-eqz v10, :cond_3

    .line 1518
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8, v9}, Lcom/android/exchange/provider/MailboxUtilities;->getParentMailboxId(Landroid/content/Context;J)J

    move-result-wide v8

    .line 1520
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8, v9}, Lcom/android/exchange/provider/MailboxUtilities;->isRootMailboxId(Landroid/content/Context;J)Z

    move-result v10

    .line 1521
    add-int/lit8 v12, v12, 0x1

    .line 1522
    const/16 v3, 0x2710

    if-le v12, v3, :cond_2

    .line 1526
    :cond_3
    const-string v3, "ExchangeService"

    const-string v4, "foldersdelete check root end"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    move-wide v14, v8

    .line 1529
    .local v14, "mailboxParentId":J
    const/4 v13, 0x1

    .line 1530
    .local v13, "mailboxType":I
    const-wide/16 v6, -0x1

    cmp-long v3, v14, v6

    if-eqz v3, :cond_4

    .line 1531
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->getMailboxType(Landroid/content/Context;J)I

    move-result v13

    .line 1532
    :cond_4
    aget-object v3, v16, v11

    if-eqz v3, :cond_5

    .line 1533
    const/4 v3, 0x6

    if-ne v13, v3, :cond_6

    .line 1534
    const/4 v3, 0x1

    aput-boolean v3, v5, v11

    .line 1507
    :cond_5
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1536
    :cond_6
    const/4 v3, 0x0

    aput-boolean v3, v5, v11

    goto :goto_2

    .line 1540
    .end local v8    # "checkParentId":J
    .end local v10    # "flagIsroot":Z
    .end local v12    # "loopEndcnt":I
    .end local v13    # "mailboxType":I
    .end local v14    # "mailboxParentId":J
    :cond_7
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v16, v4

    iget-wide v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/4 v4, 0x6

    invoke-static {v3, v6, v7, v4}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v18

    .line 1542
    .local v18, "trash":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v18, :cond_0

    .line 1545
    new-instance v2, Lcom/android/exchange/EasFolderOperationSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    move-object/from16 v0, v18

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    const/16 v7, 0xf

    move-object/from16 v4, p1

    invoke-direct/range {v2 .. v7}, Lcom/android/exchange/EasFolderOperationSvc;-><init>(Landroid/content/Context;[J[ZLjava/lang/String;I)V

    .line 1547
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v17, Ljava/lang/Thread;

    const-string v3, "(folderDeleteMulti)"

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1548
    .local v17, "thread":Ljava/lang/Thread;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0
.end method

.method public getAliasFromMap(J)Ljava/lang/String;
    .locals 7
    .param p1, "tId"    # J

    .prologue
    .line 898
    const-string v3, "exchange"

    const-string v4, "getAliasFromMap"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 899
    const/4 v1, 0x0

    .line 900
    .local v1, "alias":Ljava/lang/String;
    # getter for: Lcom/android/exchange/ExchangeService;->THREAD_MAP_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$100()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 902
    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->mThreadIdMap:Ljava/util/HashMap;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$200()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 906
    :goto_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 907
    const-string v3, "SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Returning Alias "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    return-object v1

    .line 903
    :catch_0
    move-exception v2

    .line 904
    .local v2, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0

    .line 906
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getApiLevel()I
    .locals 1

    .prologue
    .line 893
    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized getLicenseKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 942
    monitor-enter p0

    const/4 v1, 0x0

    .line 943
    .local v1, "license":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$300()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 944
    # getter for: Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$300()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 967
    :goto_0
    monitor-exit p0

    return-object v3

    .line 945
    :cond_0
    if-nez p1, :cond_1

    .line 946
    :try_start_1
    const-string v3, "getLicenseKey"

    const-string v4, "No context for getLicenseKey"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    .line 947
    goto :goto_0

    .line 950
    :cond_1
    invoke-static {p1}, Lcom/android/exchange/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/exchange/Preferences;

    move-result-object v2

    .line 951
    .local v2, "prefs":Lcom/android/exchange/Preferences;
    invoke-virtual {v2}, Lcom/android/exchange/Preferences;->getActivationLicense()Ljava/lang/String;

    move-result-object v1

    .line 952
    # setter for: Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$302(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 953
    if-nez v1, :cond_3

    .line 955
    :try_start_2
    const-class v3, Lcom/android/exchange/EasSyncService;

    iget-object v4, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v3, v4}, Lcom/android/exchange/AbstractSyncService;->activate(Ljava/lang/Class;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 957
    if-eqz v1, :cond_2

    const-string v3, "NoDeviceID"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 958
    invoke-virtual {v2, v1}, Lcom/android/exchange/Preferences;->setActivationLicense(Ljava/lang/String;)V

    .line 959
    # setter for: Lcom/android/exchange/ExchangeService;->sLicenseKey:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$302(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    move-object v3, v1

    .line 961
    goto :goto_0

    .line 962
    :catch_0
    move-exception v0

    .line 963
    .local v0, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_3
    const-string v3, "Activation"

    const-string v4, "Can\'t get server eas license"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v3, v1

    .line 964
    goto :goto_0

    .end local v0    # "e":Lcom/android/emailcommon/mail/MessagingException;
    :cond_3
    move-object v3, v1

    .line 967
    goto :goto_0

    .line 942
    .end local v2    # "prefs":Lcom/android/exchange/Preferences;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public hostChanged(J)V
    .locals 11
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1610
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 1611
    .local v1, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v1, :cond_0

    .line 1634
    :goto_0
    return-void

    .line 1613
    :cond_0
    iget-object v6, v1, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 1615
    .local v6, "syncErrorMap":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/Long;Lcom/android/exchange/ExchangeService$SyncError;>;"
    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1616
    .local v4, "mailboxId":J
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/exchange/ExchangeService$SyncError;

    .line 1618
    .local v0, "error":Lcom/android/exchange/ExchangeService$SyncError;
    invoke-static {v1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    .line 1623
    .local v3, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v3, :cond_2

    .line 1624
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1625
    :cond_2
    if-eqz v0, :cond_1

    iget-wide v8, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    cmp-long v7, v8, p1

    if-nez v7, :cond_1

    .line 1626
    const/4 v7, 0x0

    iput-boolean v7, v0, Lcom/android/exchange/ExchangeService$SyncError;->fatal:Z

    .line 1627
    const-wide/16 v8, 0x0

    iput-wide v8, v0, Lcom/android/exchange/ExchangeService$SyncError;->holdEndTime:J

    goto :goto_1

    .line 1631
    .end local v0    # "error":Lcom/android/exchange/ExchangeService$SyncError;
    .end local v3    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v4    # "mailboxId":J
    :cond_3
    const/4 v7, 0x1

    # invokes: Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V
    invoke-static {v1, p1, p2, v7}, Lcom/android/exchange/ExchangeService;->access$1200(Lcom/android/exchange/ExchangeService;JZ)V

    .line 1633
    const-string v7, "host changed"

    invoke-static {v7}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadAttachment(JZ)V
    .locals 17
    .param p1, "attachmentId"    # J
    .param p3, "background"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1335
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadAttachment : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p1

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1337
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v6

    .line 1338
    .local v6, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-nez v6, :cond_1

    .line 1404
    :cond_0
    :goto_0
    return-void

    .line 1341
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v14, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    invoke-static {v3, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v5

    .line 1342
    .local v5, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v5, :cond_0

    .line 1345
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v14, v5, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {v3, v14, v15}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v4

    .line 1346
    .local v4, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v4, :cond_0

    .line 1349
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadAttachment() - Mailbox Type = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v7, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1351
    iget v3, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v7, 0x62

    if-ne v3, v7, :cond_2

    .line 1352
    const-string v3, "loadAttachment() - Fetch Document from SharePoing or UNC"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1353
    new-instance v2, Lcom/android/exchange/EasFetchDocumentSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/exchange/EasFetchDocumentSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v13, Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "(EasFetchDocumentSvc)"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v13, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1355
    .local v13, "thread":Ljava/lang/Thread;
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 1360
    .end local v2    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v13    # "thread":Ljava/lang/Thread;
    :cond_2
    const-string v3, "loadAttachment() mailboxType != Mailbox.TYPE_SEARCH_DOCS and check if exist service that request same attachment"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1364
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v14, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    # invokes: Lcom/android/exchange/ExchangeService;->getDownloadServiceFromMap(J)Lcom/android/exchange/EasDownLoadAttachmentSvc;
    invoke-static {v3, v14, v15}, Lcom/android/exchange/ExchangeService;->access$800(Lcom/android/exchange/ExchangeService;J)Lcom/android/exchange/EasDownLoadAttachmentSvc;

    move-result-object v12

    .line 1367
    .local v12, "service":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    const/4 v9, 0x0

    .line 1369
    .local v9, "bIsMakeNewService":Z
    if-nez v12, :cond_3

    .line 1370
    const-string v3, "loadAttachment() service not create thus we make new service."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1371
    const/4 v9, 0x1

    .line 1394
    :goto_1
    if-eqz v9, :cond_0

    .line 1395
    new-instance v12, Lcom/android/exchange/EasDownLoadAttachmentSvc;

    .end local v12    # "service":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    new-instance v7, Lcom/android/exchange/PartRequest;

    const/4 v8, 0x0

    const/4 v14, 0x0

    invoke-direct {v7, v6, v8, v14}, Lcom/android/exchange/PartRequest;-><init>(Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v12, v3, v4, v7}, Lcom/android/exchange/EasDownLoadAttachmentSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/PartRequest;)V

    .line 1398
    .restart local v12    # "service":Lcom/android/exchange/EasDownLoadAttachmentSvc;
    new-instance v13, Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "(EasDownLoadAttachmentSvc)"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v13, v12, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1399
    .restart local v13    # "thread":Ljava/lang/Thread;
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    .line 1400
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "loadAttachment() - Issuing attachment download request of : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " as threadId : "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v13}, Ljava/lang/Thread;->getId()J

    move-result-wide v14

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1402
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v14, v6, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    # invokes: Lcom/android/exchange/ExchangeService;->setDownloadServiceInMap(JLcom/android/exchange/EasDownLoadAttachmentSvc;)V
    invoke-static {v3, v14, v15, v12}, Lcom/android/exchange/ExchangeService;->access$900(Lcom/android/exchange/ExchangeService;JLcom/android/exchange/EasDownLoadAttachmentSvc;)V

    goto/16 :goto_0

    .line 1373
    .end local v13    # "thread":Ljava/lang/Thread;
    :cond_3
    const-string v3, "loadAttachment() service in request queue is cancelled..."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1374
    invoke-virtual {v12}, Lcom/android/exchange/EasDownLoadAttachmentSvc;->getCancelledFlag()Z

    move-result v10

    .line 1376
    .local v10, "cancelled":Z
    if-eqz v10, :cond_4

    .line 1377
    const-string v3, "loadAttachment() service canceled but not removed in request queue"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1379
    new-instance v11, Lcom/android/exchange/PartRequest;

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-direct {v11, v6, v3, v7}, Lcom/android/exchange/PartRequest;-><init>(Lcom/android/emailcommon/provider/EmailContent$Attachment;Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    .local v11, "req":Lcom/android/exchange/Request;
    check-cast v11, Lcom/android/exchange/PartRequest;

    .end local v11    # "req":Lcom/android/exchange/Request;
    invoke-static {v11}, Lcom/android/exchange/ExchangeService;->clearAttRqFromQueue(Lcom/android/exchange/PartRequest;)V

    .line 1382
    const-string v3, "loadAttachment() service removed and update to new service"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1383
    const/4 v9, 0x1

    .line 1384
    goto :goto_1

    .line 1385
    :cond_4
    const-string v3, "loadAttachment() service was not cancelled, keep using current service."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public loadMore(J)V
    .locals 5
    .param p1, "messageId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1697
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_0

    .line 1698
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ExchangeService::loadMore(messageId["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]) start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugStartTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 1702
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    .line 1703
    .local v0, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v0, :cond_2

    .line 1704
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v2

    const/high16 v3, 0x70000

    const/16 v4, 0x64

    invoke-interface {v2, p1, p2, v3, v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadMoreStatus(JII)V

    .line 1719
    :cond_1
    :goto_0
    return-void

    .line 1709
    :cond_2
    sget-object v2, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    if-eqz v2, :cond_3

    .line 1710
    const/4 v2, 0x0

    sput-object v2, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    .line 1711
    :cond_3
    new-instance v2, Lcom/android/exchange/EasLoadMoreSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v2, v3, v0}, Lcom/android/exchange/EasLoadMoreSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    sput-object v2, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    .line 1712
    new-instance v1, Ljava/lang/Thread;

    sget-object v2, Lcom/android/exchange/ExchangeService;->elmsvc:Lcom/android/exchange/EasLoadMoreSvc;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(EasLoadMoreSvc)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1713
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1715
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_1

    .line 1716
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ExchangeService::loadMore(messageId["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]) end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadMoreCancel()V
    .locals 5

    .prologue
    .line 1814
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getElmSvc()Lcom/android/exchange/EasLoadMoreSvc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/EasLoadMoreSvc;->reset()V

    .line 1815
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getElmSvc()Lcom/android/exchange/EasLoadMoreSvc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/exchange/EasLoadMoreSvc;->userCancelled()V

    .line 1816
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-static {}, Lcom/android/exchange/ExchangeService;->getElmSvcMsg()J

    move-result-wide v2

    const/high16 v1, 0x100000

    const/16 v4, 0x64

    invoke-interface {v0, v2, v3, v1, v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadMoreStatus(JII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1821
    :goto_0
    return-void

    .line 1819
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1818
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public moveConversationAlways(JJ[BI)V
    .locals 9
    .param p1, "mailboxId"    # J
    .param p3, "toMailboxId"    # J
    .param p5, "conversationId"    # [B
    .param p6, "ignore"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1558
    new-instance v0, Lcom/android/exchange/EasConvSvc;

    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/android/exchange/EasConvSvc;-><init>(Landroid/content/Context;JJ[BI)V

    .line 1560
    .local v0, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v8, Ljava/lang/Thread;

    const-string v1, "(EasConvSvc)"

    invoke-direct {v8, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1561
    .local v8, "thread":Ljava/lang/Thread;
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 1562
    return-void
.end method

.method public moveMessage(JJ)V
    .locals 1
    .param p1, "messageId"    # J
    .param p3, "mailboxId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1796
    new-instance v0, Lcom/android/exchange/MessageMoveRequest;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/android/exchange/MessageMoveRequest;-><init>(JJ)V

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->sendMessageRequest(Lcom/android/exchange/Request;)V

    .line 1797
    return-void
.end method

.method public readyToSend()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2009
    const-string v0, "ExchangeService"

    const-string v1, "readyToSend is called."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2010
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v0}, Lcom/android/exchange/ExchangeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->initCAC(Landroid/content/Context;)V

    .line 2011
    return-void
.end method

.method public refreshIRMTemplates(J)V
    .locals 7
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1781
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v4, :cond_0

    .line 1782
    const-string v4, "IRM"

    const-string v5, "ExchangeService: refreshing templates"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    :cond_0
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1785
    .local v0, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/4 v1, 0x1

    .line 1786
    .local v1, "isIrmtemplateRefreshNeeded":Z
    new-instance v2, Lcom/android/exchange/EasDevInfoSvc;

    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v2, v4, v0, v1}, Lcom/android/exchange/EasDevInfoSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Z)V

    .line 1787
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v3, Ljava/lang/Thread;

    const-string v4, "(EasDevInfoSvc IRM)"

    invoke-direct {v3, v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1790
    .local v3, "thread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 1791
    return-void
.end method

.method public refreshMessageBody(JZZ)V
    .locals 5
    .param p1, "messageId"    # J
    .param p3, "withMimeData"    # Z
    .param p4, "isSaveBody"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1676
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->REFRESH_BODY_TEST_ENABLE:Z

    if-nez v2, :cond_0

    .line 1694
    :goto_0
    return-void

    .line 1680
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    .line 1681
    .local v0, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v0, :cond_1

    .line 1682
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v2

    const/high16 v3, 0x70000

    const/16 v4, 0x64

    invoke-interface {v2, p1, p2, v3, v4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadMoreStatus(JII)V

    goto :goto_0

    .line 1687
    :cond_1
    sget-object v2, Lcom/android/exchange/ExchangeService;->elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

    if-eqz v2, :cond_2

    .line 1688
    const/4 v2, 0x0

    sput-object v2, Lcom/android/exchange/ExchangeService;->elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

    .line 1690
    :cond_2
    new-instance v2, Lcom/android/exchange/utility/MessageBodyRefresher;

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {v2, v3, v0, p3, p4}, Lcom/android/exchange/utility/MessageBodyRefresher;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;ZZ)V

    sput-object v2, Lcom/android/exchange/ExchangeService;->elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

    .line 1692
    new-instance v1, Ljava/lang/Thread;

    sget-object v2, Lcom/android/exchange/ExchangeService;->elmsvcForDebug:Lcom/android/exchange/EasLoadMoreSvc;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(MessageBodyRefresher)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1693
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public removeCallback(Lcom/android/emailcommon/service/IEmailServiceCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/android/emailcommon/service/IEmailServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1742
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 1743
    return-void
.end method

.method public searchMessage(J[JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 28
    .param p1, "accId"    # J
    .param p3, "mailboxIds"    # [J
    .param p4, "foldId"    # J
    .param p6, "searchTextString"    # Ljava/lang/String;
    .param p7, "greaterThanDateString"    # Ljava/lang/String;
    .param p8, "lessThanDateString"    # Ljava/lang/String;
    .param p9, "optionsDeepTraversalStr"    # Ljava/lang/String;
    .param p10, "conversationIdStr"    # Ljava/lang/String;

    .prologue
    .line 1906
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 1907
    .local v5, "accountIdStr":Ljava/lang/String;
    invoke-static/range {p4 .. p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v23

    .line 1908
    .local v23, "foldIdString":Ljava/lang/String;
    move-object/from16 v0, p3

    array-length v4, v0

    new-array v9, v4, [Ljava/lang/String;

    .line 1911
    .local v9, "mailboxIdStrings":[Ljava/lang/String;
    if-eqz p10, :cond_0

    const-string v4, "null"

    move-object/from16 v0, p10

    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 1912
    const/16 p10, 0x0

    .line 1916
    :cond_0
    :try_start_0
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 1921
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_0
    :try_start_1
    move-object/from16 v0, p3

    array-length v4, v0

    move/from16 v0, v24

    if-ge v0, v4, :cond_1

    .line 1922
    aget-wide v6, p3, v24

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v24

    .line 1923
    aget-object v4, v9, v24

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    aput-wide v6, p3, v24
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1921
    add-int/lit8 v24, v24, 0x1

    goto :goto_0

    .line 1917
    .end local v24    # "i":I
    :catch_0
    move-exception v21

    .line 1918
    .local v21, "e":Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v6, "Illegal value in URI"

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1925
    .end local v21    # "e":Ljava/lang/NumberFormatException;
    .restart local v24    # "i":I
    :catch_1
    move-exception v21

    .line 1926
    .restart local v21    # "e":Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v6, "Illegal value in URI"

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1929
    .end local v21    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    :try_start_2
    invoke-static/range {v23 .. v23}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide p4

    .line 1933
    const-string v4, "ExchangeProvider"

    const-string v6, "before buildSearchRequest"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1935
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v20

    .line 1937
    .local v20, "context":Landroid/content/Context;
    const/16 v22, 0x0

    .line 1939
    .local v22, "emailResult":Lcom/android/exchange/provider/EmailResult;
    const/16 v25, 0x0

    .line 1941
    .local v25, "intent":Landroid/content/Intent;
    move-object/from16 v0, v20

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v26

    .line 1943
    .local v26, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v26, :cond_2

    move-object/from16 v0, v26

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v6, 0x62

    if-ne v4, v6, :cond_2

    .line 1947
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/16 v12, 0x3e7

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    # invokes: Lcom/android/exchange/ExchangeService;->buildSearchRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;
    invoke-static/range {v4 .. v12}, Lcom/android/exchange/ExchangeService;->access$1400(Lcom/android/exchange/ExchangeService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;

    move-result-object v18

    .line 1951
    .local v18, "request":Lcom/android/exchange/SearchRequest;
    const-string v4, "ExchangeProvider"

    const-string v6, "after buildSearchRequest"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1953
    const/4 v4, 0x0

    aget-wide v14, p3, v4

    move-object/from16 v11, v20

    move-wide/from16 v12, p1

    move-wide/from16 v16, p4

    invoke-static/range {v11 .. v18}, Lcom/android/exchange/EasSyncService;->searchDocument(Landroid/content/Context;JJJLcom/android/exchange/SearchRequest;)Lcom/android/exchange/provider/EmailResult;

    move-result-object v22

    .line 1956
    new-instance v25, Landroid/content/Intent;

    .end local v25    # "intent":Landroid/content/Intent;
    const-string v4, "android.intent.action.EMAILDOCSEARCH_COMPLETED"

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1957
    .restart local v25    # "intent":Landroid/content/Intent;
    const-string v4, "MailboxId"

    move-object/from16 v0, v25

    move-wide/from16 v1, p4

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1958
    const-string v4, "LinkId"

    move-object/from16 v0, v25

    move-object/from16 v1, p6

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1974
    :goto_1
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 1975
    .local v19, "b":Ljava/lang/StringBuilder;
    const/16 v24, 0x0

    :goto_2
    move-object/from16 v0, p3

    array-length v4, v0

    move/from16 v0, v24

    if-ge v0, v4, :cond_3

    .line 1976
    aget-wide v6, p3, v24

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " ,"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1975
    add-int/lit8 v24, v24, 0x1

    goto :goto_2

    .line 1930
    .end local v18    # "request":Lcom/android/exchange/SearchRequest;
    .end local v19    # "b":Ljava/lang/StringBuilder;
    .end local v20    # "context":Landroid/content/Context;
    .end local v22    # "emailResult":Lcom/android/exchange/provider/EmailResult;
    .end local v25    # "intent":Landroid/content/Intent;
    .end local v26    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_2
    move-exception v21

    .line 1931
    .restart local v21    # "e":Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v6, "Illegal value in URI"

    invoke-direct {v4, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1963
    .end local v21    # "e":Ljava/lang/NumberFormatException;
    .restart local v20    # "context":Landroid/content/Context;
    .restart local v22    # "emailResult":Lcom/android/exchange/provider/EmailResult;
    .restart local v25    # "intent":Landroid/content/Intent;
    .restart local v26    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/16 v12, 0x63

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    # invokes: Lcom/android/exchange/ExchangeService;->buildSearchRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;
    invoke-static/range {v4 .. v12}, Lcom/android/exchange/ExchangeService;->access$1400(Lcom/android/exchange/ExchangeService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/android/exchange/SearchRequest;

    move-result-object v18

    .line 1967
    .restart local v18    # "request":Lcom/android/exchange/SearchRequest;
    const-string v4, "ExchangeProvider"

    const-string v6, "after buildSearchRequest"

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1969
    const/4 v4, 0x0

    aget-wide v14, p3, v4

    move-object/from16 v11, v20

    move-wide/from16 v12, p1

    move-wide/from16 v16, p4

    invoke-static/range {v11 .. v18}, Lcom/android/exchange/EasSyncService;->searchEmail(Landroid/content/Context;JJJLcom/android/exchange/SearchRequest;)Lcom/android/exchange/provider/EmailResult;

    move-result-object v22

    .line 1971
    new-instance v25, Landroid/content/Intent;

    .end local v25    # "intent":Landroid/content/Intent;
    const-string v4, "android.intent.action.EMAILSEARCH_COMPLETED"

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v25    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 1979
    .restart local v19    # "b":Ljava/lang/StringBuilder;
    :cond_3
    const-string v4, "ExchangeProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "after searchEmail , mailbox id = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Folder ID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p4

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1985
    if-eqz v22, :cond_4

    .line 1986
    :try_start_3
    const-string v4, "SearchResult"

    move-object/from16 v0, v22

    iget v6, v0, Lcom/android/exchange/provider/EmailResult;->result:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1987
    const-string v4, "Total"

    move-object/from16 v0, v22

    iget v6, v0, Lcom/android/exchange/provider/EmailResult;->total:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1988
    const-string v4, "StartRange"

    move-object/from16 v0, v22

    iget v6, v0, Lcom/android/exchange/provider/EmailResult;->startRange:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1989
    const-string v4, "EndRange"

    move-object/from16 v0, v22

    iget v6, v0, Lcom/android/exchange/provider/EmailResult;->endRange:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1990
    const-string v4, "MailboxId"

    move-object/from16 v0, v25

    move-wide/from16 v1, p4

    invoke-virtual {v0, v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1993
    :cond_4
    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1998
    :try_start_4
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    const/4 v6, 0x0

    const/16 v7, 0x64

    move-wide/from16 v0, p4

    invoke-interface {v4, v0, v1, v6, v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    .line 2005
    :goto_3
    return-void

    .line 2000
    :catch_3
    move-exception v21

    .line 2002
    .local v21, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 1994
    .end local v21    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v21

    .line 1995
    .local v21, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "ExchangeProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fzhang exception = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1998
    :try_start_6
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    const/4 v6, 0x0

    const/16 v7, 0x64

    move-wide/from16 v0, p4

    invoke-interface {v4, v0, v1, v6, v7}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_3

    .line 2000
    :catch_5
    move-exception v21

    .line 2002
    .local v21, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 1997
    .end local v21    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v4

    .line 1998
    :try_start_7
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    move-wide/from16 v0, p4

    invoke-interface {v6, v0, v1, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_6

    .line 2003
    :goto_4
    throw v4

    .line 2000
    :catch_6
    move-exception v21

    .line 2002
    .restart local v21    # "e":Landroid/os/RemoteException;
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4
.end method

.method public sendMeetingEditedResponse(JJI)V
    .locals 7
    .param p1, "messageId"    # J
    .param p3, "draftMessageId"    # J
    .param p5, "response"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1667
    new-instance v1, Lcom/android/exchange/MeetingResponseRequest;

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/android/exchange/MeetingResponseRequest;-><init>(JJI)V

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->sendMessageRequest(Lcom/android/exchange/Request;)V

    .line 1668
    return-void
.end method

.method public sendMeetingResponse(JI)V
    .locals 1
    .param p1, "messageId"    # J
    .param p3, "response"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1660
    new-instance v0, Lcom/android/exchange/MeetingResponseRequest;

    invoke-direct {v0, p1, p2, p3}, Lcom/android/exchange/MeetingResponseRequest;-><init>(JI)V

    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->sendMessageRequest(Lcom/android/exchange/Request;)V

    .line 1661
    return-void
.end method

.method public sendMessageCancel([JJJ)V
    .locals 14
    .param p1, "messageIds"    # [J
    .param p2, "outboxId"    # J
    .param p4, "accountId"    # J

    .prologue
    .line 1826
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageCancel : start. outboxId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1828
    sget-object v9, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 1829
    .local v9, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v9, :cond_0

    .line 1830
    const-string v2, "sendMessageCancel : Fail to Cancel. exchangeService == null"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1862
    :goto_0
    return-void

    .line 1834
    :cond_0
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v12

    monitor-enter v12

    .line 1835
    :try_start_0
    move-wide/from16 v0, p2

    invoke-static {v9, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v10

    .line 1836
    .local v10, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v10, :cond_1

    iget-wide v2, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    cmp-long v2, p4, v2

    if-eqz v2, :cond_2

    .line 1837
    :cond_1
    const-string v2, "sendMessageCancel : Fail to Cancel. m == null || accountId != m.mAccountKey"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1838
    monitor-exit v12

    goto :goto_0

    .line 1861
    .end local v10    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catchall_0
    move-exception v2

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1840
    .restart local v10    # "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    :try_start_1
    iget-wide v2, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v9, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v8

    .line 1841
    .local v8, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v8, :cond_5

    .line 1842
    # getter for: Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;
    invoke-static {v9}, Lcom/android/exchange/ExchangeService;->access$1000(Lcom/android/exchange/ExchangeService;)Ljava/util/HashMap;

    move-result-object v2

    iget-wide v4, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/android/exchange/AbstractSyncService;

    .line 1843
    .local v11, "service":Lcom/android/exchange/AbstractSyncService;
    if-eqz v11, :cond_4

    move-object v0, v11

    check-cast v0, Lcom/android/exchange/EasSyncService;

    move-object v2, v0

    iget-boolean v2, v2, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    if-eqz v2, :cond_4

    .line 1844
    instance-of v2, v11, Lcom/android/exchange/EasOutboxService;

    if-eqz v2, :cond_3

    .line 1845
    const-string v2, "sendMessageCancel : service instanceof EasOutboxService"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1846
    move-object v0, v11

    check-cast v0, Lcom/android/exchange/EasOutboxService;

    move-object v2, v0

    move-object v3, p1

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasOutboxService;->sendMessageCancel([JJJ)V

    .line 1861
    monitor-exit v12

    goto :goto_0

    .line 1849
    :cond_3
    const-string v2, "sendMessageCancel : service is not instanceof EasOutboxService"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1850
    monitor-exit v12

    goto :goto_0

    .line 1853
    :cond_4
    const-string v2, "sendMessageCancel : Fail to Cancel. service is not valid"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1854
    monitor-exit v12

    goto :goto_0

    .line 1857
    .end local v11    # "service":Lcom/android/exchange/AbstractSyncService;
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendMessageCancel : Fail to Cancel. acct == null. accountId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1859
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public sendRecoveryPassword(JLjava/lang/String;)V
    .locals 7
    .param p1, "accountId"    # J
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1747
    const-string v4, "ExchangeService"

    const-string v5, "sendRecoveryPassword"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1748
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1749
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v0, :cond_0

    .line 1750
    const-string v4, "ExchangeService"

    const-string v5, "acccount is null"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    :goto_0
    return-void

    .line 1754
    :cond_0
    :try_start_0
    new-instance v2, Lcom/android/exchange/PasswordRecoveryService;

    sget-object v4, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v2, v4, v0, p3}, Lcom/android/exchange/PasswordRecoveryService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)V

    .line 1755
    .local v2, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v3, Ljava/lang/Thread;

    const-string v4, "(PasswordRecovery)"

    invoke-direct {v3, v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1758
    .local v3, "thread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1759
    .end local v2    # "svc":Lcom/android/exchange/AbstractSyncService;
    .end local v3    # "thread":Ljava/lang/Thread;
    :catch_0
    move-exception v1

    .line 1760
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setCallback(Lcom/android/emailcommon/service/IEmailServiceCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/android/emailcommon/service/IEmailServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1738
    # getter for: Lcom/android/exchange/ExchangeService;->mCallbackList:Landroid/os/RemoteCallbackList;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$000()Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 1739
    return-void
.end method

.method public setDeviceInfo(J)V
    .locals 5
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1772
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1773
    .local v0, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    new-instance v1, Lcom/android/exchange/EasDevInfoSvc;

    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-direct {v1, v3, v0}, Lcom/android/exchange/EasDevInfoSvc;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 1774
    .local v1, "svc":Lcom/android/exchange/AbstractSyncService;
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(DevInfoSvc)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1775
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1776
    return-void
.end method

.method public setLogging(I)V
    .locals 3
    .param p1, "on"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1637
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_0

    .line 1638
    sput-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    .line 1642
    :goto_0
    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_1

    .line 1643
    sput-boolean v2, Lcom/android/emailcommon/EasRefs;->VIEW_FILE_LOG:Z

    .line 1647
    :goto_1
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_2

    .line 1648
    sput-boolean v2, Lcom/android/emailcommon/EasRefs;->REFRESH_BODY_TEST_ENABLE:Z

    .line 1653
    :goto_2
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-object v1, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v1}, Lcom/android/exchange/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/exchange/Preferences;

    move-result-object v1

    # setter for: Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;
    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->access$1302(Lcom/android/exchange/ExchangeService;Lcom/android/exchange/Preferences;)Lcom/android/exchange/Preferences;

    .line 1655
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mPreferences:Lcom/android/exchange/Preferences;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$1300(Lcom/android/exchange/ExchangeService;)Lcom/android/exchange/Preferences;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/exchange/Preferences;->setDebugBits(I)V

    .line 1656
    invoke-static {p1}, Lcom/android/emailcommon/EasRefs;->setUserDebug(I)V

    .line 1657
    return-void

    .line 1640
    :cond_0
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    goto :goto_0

    .line 1645
    :cond_1
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->VIEW_FILE_LOG:Z

    goto :goto_1

    .line 1650
    :cond_2
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->REFRESH_BODY_TEST_ENABLE:Z

    goto :goto_2
.end method

.method public sslCertValidationFinished(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "result"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2017
    invoke-static {p3}, Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;->get(I)Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/android/exchange/utility/SSLCertVerificationHandler;->finished(Ljava/lang/String;Lcom/android/exchange/utility/SSLCertVerificationHandler$SSLCertVerificationResult;)V

    .line 2019
    const-string v0, "ExchangeService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Verifying SSL Certificate finished for user "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2022
    return-void
.end method

.method public startSync(JZ)V
    .locals 13
    .param p1, "mailboxId"    # J
    .param p3, "userRequest"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 983
    const-string v6, "ExchangeService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "startSync() called. mailboxId = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 986
    .local v3, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v3, :cond_1

    .line 987
    const-string v6, "ExchangeService"

    const-string v7, "startSync() exchangeService == null. return"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    :cond_0
    :goto_0
    return-void

    .line 990
    :cond_1
    invoke-static {v3, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v4

    .line 992
    .local v4, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v4, :cond_2

    .line 993
    const-string v6, "ExchangeService"

    const-string v7, "startSync() m == null. return"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto :goto_0

    .line 999
    :cond_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-wide v8, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v6, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v1

    .line 1002
    .local v1, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez p3, :cond_3

    .line 1003
    const-string v6, "ExchangeService"

    const-string v7, "startSync : automatic sync. check roaming"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1004
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncScheduleData()Lcom/android/emailcommon/utility/SyncScheduleData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/emailcommon/utility/SyncScheduleData;->getRoamingSchedule()I

    move-result v6

    if-nez v6, :cond_3

    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1007
    const-string v6, "ExchangeService"

    const-string v7, "startSync : romaing schedule is manual : automatic sync blocked"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1009
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto :goto_0

    .line 1015
    :cond_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->checkExchangeServiceServiceRunning()V

    .line 1017
    sget-boolean v6, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    if-eqz v6, :cond_6

    if-eqz p3, :cond_6

    .line 1018
    const-string v6, "ExchangeService"

    const-string v7, "startSync() sConnectivityHold && userRequest. return"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$400()Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->syncMailboxStatus(JII)V

    .line 1022
    # getter for: Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$400()Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    move-result-object v6

    const/16 v7, 0x20

    const/4 v8, 0x0

    invoke-virtual {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->syncMailboxStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1027
    :goto_1
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_6

    .line 1028
    const-string v6, "ExchangeService"

    const-string v7, "startSync() sConnectivityHold && userRequest. m.mType == Mailbox.TYPE_OUTBOX"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    if-eqz p3, :cond_4

    .line 1032
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1033
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v6, "syncServerId"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1034
    const-string v6, "retrySendTimes"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1035
    const-string v6, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1036
    sget-boolean v6, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-eqz v6, :cond_5

    .line 1037
    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "mailboxKey=? and (syncServerId!=-1)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1048
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_4
    :goto_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1042
    .restart local v2    # "cv":Landroid/content/ContentValues;
    :cond_5
    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "mailboxKey=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2

    .line 1054
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_6
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_a

    .line 1055
    const-string v6, "ExchangeService"

    const-string v7, "startSync() m.mType == Mailbox.TYPE_OUTBOX"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    if-eqz p3, :cond_7

    .line 1058
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1059
    .restart local v2    # "cv":Landroid/content/ContentValues;
    const-string v6, "syncServerId"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1060
    const-string v6, "retrySendTimes"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1061
    const-string v6, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1062
    sget-boolean v6, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-eqz v6, :cond_8

    .line 1063
    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "mailboxKey=? and (syncServerId!=-1)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1077
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_7
    :goto_3
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1078
    :try_start_1
    iget-object v6, v3, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1079
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1081
    const-string v6, "start outbox"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 1090
    if-eqz v1, :cond_9

    .line 1091
    iget-object v6, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-wide v8, 0x4028333333333333L    # 12.1

    cmpg-double v6, v6, v8

    if-gtz v6, :cond_c

    .line 1092
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1068
    .restart local v2    # "cv":Landroid/content/ContentValues;
    :cond_8
    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "mailboxKey=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v2, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    .line 1079
    .end local v2    # "cv":Landroid/content/ContentValues;
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 1097
    :cond_9
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1105
    :cond_a
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v7, 0x8

    if-ne v6, v7, :cond_b

    .line 1107
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1110
    :cond_b
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v7, 0x62

    if-ne v6, v7, :cond_c

    .line 1112
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x64

    invoke-interface {v6, p1, p2, v7, v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1124
    :cond_c
    iget-wide v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/16 v8, 0x44

    invoke-static {v3, v6, v7, v8}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v5

    .line 1126
    .local v5, "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v5, :cond_d

    .line 1127
    iget-object v6, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v7, 0x4

    iget-wide v8, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    # invokes: Lcom/android/exchange/ExchangeService;->releaseSyncHoldMailbox(Landroid/content/Context;IJ)V
    invoke-static {v6, v3, v7, v8, v9}, Lcom/android/exchange/ExchangeService;->access$600(Lcom/android/exchange/ExchangeService;Landroid/content/Context;IJ)V

    .line 1130
    :cond_d
    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_e

    .line 1131
    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-static {p1, p2, v6, v7}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    .line 1138
    :goto_4
    if-eqz v1, :cond_0

    iget v6, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v6, :cond_0

    iget-object v6, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-wide/high16 v8, 0x402c000000000000L    # 14.0

    cmpl-double v6, v6, v8

    if-ltz v6, :cond_0

    .line 1141
    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1142
    .local v0, "RIC_Id":Ljava/lang/Long;
    iget-object v6, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v8, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    # invokes: Lcom/android/exchange/ExchangeService;->getMailBoxIdRIC(J)Ljava/lang/Long;
    invoke-static {v6, v8, v9}, Lcom/android/exchange/ExchangeService;->access$700(Lcom/android/exchange/ExchangeService;J)Ljava/lang/Long;

    move-result-object v0

    .line 1143
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 1144
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x5

    const/4 v9, 0x0

    invoke-static {v6, v7, v8, v9}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    goto/16 :goto_0

    .line 1133
    .end local v0    # "RIC_Id":Ljava/lang/Long;
    :cond_e
    const-string v6, "startManualSync should not be called at startSync() in the case of OutBox"

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_4

    .line 1024
    .end local v5    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_0
    move-exception v6

    goto/16 :goto_1
.end method

.method public startSyncForMessageId(J[JZ)V
    .locals 19
    .param p1, "mailboxId"    # J
    .param p3, "messageIds"    # [J
    .param p4, "userRequest"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1152
    const-string v10, "ExchangeService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "startSyncForMessageId() called. mailboxId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p1

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    sget-object v5, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    .line 1156
    .local v5, "exchangeService":Lcom/android/exchange/ExchangeService;
    if-nez v5, :cond_1

    .line 1157
    const-string v10, "ExchangeService"

    const-string v11, "startSyncForMessageId() exchangeService == null. return"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    :cond_0
    :goto_0
    return-void

    .line 1162
    :cond_1
    move-wide/from16 v0, p1

    invoke-static {v5, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v7

    .line 1165
    .local v7, "m":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-nez v7, :cond_2

    .line 1166
    const-string v10, "ExchangeService"

    const-string v11, "startSyncForMessages() m == null. return"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto :goto_0

    .line 1172
    :cond_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v10

    iget-wide v12, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v10, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v3

    .line 1175
    .local v3, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->checkExchangeServiceServiceRunning()V

    .line 1177
    sget-boolean v10, Lcom/android/exchange/ExchangeService;->sConnectivityHold:Z

    if-eqz v10, :cond_4

    if-eqz p4, :cond_4

    .line 1178
    const-string v10, "ExchangeService"

    const-string v11, "startSyncForMessageId() sConnectivityHold && userRequest. return"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    :try_start_0
    # getter for: Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$400()Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    move-result-object v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->syncMailboxStatus(JII)V

    .line 1184
    # getter for: Lcom/android/exchange/ExchangeService;->sCallbackProxy:Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$400()Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;

    move-result-object v10

    const/16 v11, 0x20

    const/4 v12, 0x0

    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback$Stub;->syncMailboxStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1189
    :goto_1
    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_4

    .line 1190
    const-string v10, "ExchangeService"

    const-string v11, "startSyncForMessageId() sConnectivityHold && userRequest. m.mType == Mailbox.TYPE_OUTBOX"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1193
    if-eqz p4, :cond_3

    .line 1194
    const/4 v9, 0x0

    .line 1195
    .local v9, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    move-object/from16 v0, p3

    array-length v10, v0

    if-ge v6, v10, :cond_3

    .line 1196
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1197
    .local v4, "cv":Landroid/content/ContentValues;
    const-string v10, "syncServerId"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1198
    const-string v10, "retrySendTimes"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1199
    const-string v10, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1201
    invoke-virtual {v5}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v12, "_id=?"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aget-wide v16, p3, v6

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v10, v11, v4, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1206
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v10

    aget-wide v12, p3, v6

    invoke-static {v10, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v9

    .line 1207
    sget-object v10, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    iget-wide v12, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1208
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v10, v12, v13, v9}, Lcom/android/exchange/EasOutboxService;->sendMessage(Landroid/content/Context;JLcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 1195
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1211
    .end local v4    # "cv":Landroid/content/ContentValues;
    .end local v6    # "i":I
    .end local v9    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1217
    :cond_4
    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_8

    .line 1218
    const-string v10, "ExchangeService"

    const-string v11, "startSyncForMessageId() m.mType == Mailbox.TYPE_OUTBOX"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    if-eqz p4, :cond_6

    .line 1221
    const/4 v9, 0x0

    .line 1222
    .restart local v9    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_3
    move-object/from16 v0, p3

    array-length v10, v0

    if-ge v6, v10, :cond_6

    .line 1223
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1224
    .restart local v4    # "cv":Landroid/content/ContentValues;
    const-string v10, "syncServerId"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1225
    const-string v10, "retrySendTimes"

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1226
    const-string v10, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1227
    sget-boolean v10, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-eqz v10, :cond_5

    .line 1228
    invoke-virtual {v5}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v12, "mailboxKey=? and (syncServerId!=-1)"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v10, v11, v4, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1239
    :goto_4
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v10

    aget-wide v12, p3, v6

    invoke-static {v10, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v9

    .line 1241
    sget-object v10, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    iget-wide v12, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1242
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v10

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v10, v12, v13, v9}, Lcom/android/exchange/EasOutboxService;->sendMessage(Landroid/content/Context;JLcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 1222
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 1233
    :cond_5
    invoke-virtual {v5}, Lcom/android/exchange/ExchangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v12, "_id=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aget-wide v16, p3, v6

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v10, v11, v4, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_4

    .line 1249
    .end local v4    # "cv":Landroid/content/ContentValues;
    .end local v6    # "i":I
    .end local v9    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_6
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    .line 1250
    :try_start_1
    iget-object v10, v5, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1251
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1253
    const-string v10, "start outbox"

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    .line 1262
    if-eqz v3, :cond_7

    .line 1263
    iget-object v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    const-wide v12, 0x4028333333333333L    # 12.1

    cmpg-double v10, v10, v12

    if-gtz v10, :cond_a

    .line 1264
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1251
    :catchall_0
    move-exception v10

    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .line 1269
    :cond_7
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1277
    :cond_8
    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v11, 0x8

    if-ne v10, v11, :cond_9

    .line 1279
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1282
    :cond_9
    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v11, 0x62

    if-ne v10, v11, :cond_a

    .line 1284
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x64

    move-wide/from16 v0, p1

    invoke-interface {v10, v0, v1, v11, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V

    goto/16 :goto_0

    .line 1296
    :cond_a
    iget-wide v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/16 v12, 0x44

    invoke-static {v5, v10, v11, v12}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v8

    .line 1298
    .local v8, "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v8, :cond_b

    .line 1299
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v11, 0x4

    iget-wide v12, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    # invokes: Lcom/android/exchange/ExchangeService;->releaseSyncHoldMailbox(Landroid/content/Context;IJ)V
    invoke-static {v10, v5, v11, v12, v13}, Lcom/android/exchange/ExchangeService;->access$600(Lcom/android/exchange/ExchangeService;Landroid/content/Context;IJ)V

    .line 1302
    :cond_b
    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/4 v11, 0x4

    if-eq v10, v11, :cond_c

    .line 1303
    const/4 v10, 0x5

    const/4 v11, 0x0

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v10, v11}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    .line 1310
    :goto_5
    if-eqz v3, :cond_0

    iget v10, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v10, :cond_0

    iget-object v10, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    const-wide/high16 v12, 0x402c000000000000L    # 14.0

    cmpl-double v10, v10, v12

    if-ltz v10, :cond_0

    .line 1313
    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1314
    .local v2, "RIC_Id":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v12, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    # invokes: Lcom/android/exchange/ExchangeService;->getMailBoxIdRIC(J)Ljava/lang/Long;
    invoke-static {v10, v12, v13}, Lcom/android/exchange/ExchangeService;->access$700(Lcom/android/exchange/ExchangeService;J)Ljava/lang/Long;

    move-result-object v2

    .line 1315
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-eqz v10, :cond_0

    .line 1316
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v12, 0x5

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V

    goto/16 :goto_0

    .line 1305
    .end local v2    # "RIC_Id":Ljava/lang/Long;
    :cond_c
    const-string v10, "startManualSync should not be called at startSyncForMessageId() in the case of OutBox"

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_5

    .line 1186
    .end local v8    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_0
    move-exception v10

    goto/16 :goto_1
.end method

.method public stopSync(J)V
    .locals 1
    .param p1, "mailboxId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1323
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->stopManualSync(J)V

    .line 1324
    return-void
.end method

.method public updateFolderList(J)V
    .locals 7
    .param p1, "accountId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x44

    .line 1576
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v1, :cond_0

    .line 1577
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-static {v1, p1, p2, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1579
    .local v0, "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_0

    .line 1580
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    sget-object v2, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/4 v3, 0x4

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    # invokes: Lcom/android/exchange/ExchangeService;->releaseSyncHoldMailbox(Landroid/content/Context;IJ)V
    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/exchange/ExchangeService;->access$600(Lcom/android/exchange/ExchangeService;Landroid/content/Context;IJ)V

    .line 1588
    .end local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v1, :cond_2

    .line 1589
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    invoke-static {v1, p1, p2, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v0

    .line 1591
    .restart local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_2

    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1592
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 1593
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$1000(Lcom/android/exchange/ExchangeService;)Ljava/util/HashMap;

    move-result-object v1

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1594
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-object v1, v1, Lcom/android/exchange/ExchangeService;->mSyncErrorMap:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1595
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFolderList(): Starting sync for "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 1596
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    const/4 v3, 0x5

    const/4 v4, 0x0

    # invokes: Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    invoke-static {v1, v0, v3, v4}, Lcom/android/exchange/ExchangeService;->access$1100(Lcom/android/exchange/ExchangeService;Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V

    .line 1597
    monitor-exit v2

    .line 1607
    .end local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :goto_0
    return-void

    .line 1599
    .restart local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SyncService for mailbox:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " already running. Cannot start FolderSync thread"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->errorlog(Ljava/lang/String;)V

    .line 1601
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1606
    .end local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v2, 0x0

    invoke-static {v1, p1, p2, v2}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto :goto_0

    .line 1601
    .restart local v0    # "m2":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public validate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)Landroid/os/Bundle;
    .locals 11
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "userName"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "port"    # I
    .param p6, "ssl"    # Z
    .param p7, "trustCertificates"    # Z
    .param p8, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 917
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 918
    .local v9, "bundle":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p0, v0}, Lcom/android/exchange/ExchangeService$2;->getLicenseKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    .line 919
    .local v10, "license":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 920
    const-string v0, "validate_result_code"

    const/16 v1, 0x10

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 930
    .end local v9    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-object v9

    .line 923
    .restart local v9    # "bundle":Landroid/os/Bundle;
    :cond_0
    const-string v0, "NoDeviceID"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 924
    const-string v0, "validate_result_code"

    const/16 v1, 0x12

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 930
    :cond_1
    const-class v0, Lcom/android/exchange/EasSyncService;

    iget-object v8, p0, Lcom/android/exchange/ExchangeService$2;->this$0:Lcom/android/exchange/ExchangeService;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object/from16 v7, p8

    invoke-static/range {v0 .. v8}, Lcom/android/exchange/AbstractSyncService;->validate(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v9

    goto :goto_0
.end method
