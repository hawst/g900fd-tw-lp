.class public Lcom/android/exchange/DeviceInformation;
.super Ljava/lang/Object;
.source "DeviceInformation.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field public mFriendlyName:Ljava/lang/String;

.field public mId:Ljava/lang/String;

.field public mModel:Ljava/lang/String;

.field public mNetworkOperator:Ljava/lang/String;

.field public mOs:Ljava/lang/String;

.field public mOsLanguage:Ljava/lang/String;

.field public mOutBoundSms:Z

.field public mPhoneNumber:Ljava/lang/String;

.field public mProtocolVersion:D

.field public mUserAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(D)V
    .locals 3
    .param p1, "protocolVersion"    # D

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "DeviceInformation"

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->TAG:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mModel:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mId:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mFriendlyName:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mOs:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mOsLanguage:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mUserAgent:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/DeviceInformation;->mOutBoundSms:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/DeviceInformation;->mNetworkOperator:Ljava/lang/String;

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/exchange/DeviceInformation;->mProtocolVersion:D

    .line 51
    iput-wide p1, p0, Lcom/android/exchange/DeviceInformation;->mProtocolVersion:D

    .line 52
    return-void
.end method


# virtual methods
.method public buildCommand()Lcom/android/exchange/adapter/Serializer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/exchange/DeviceInformation;->buildCommand(Z)Lcom/android/exchange/adapter/Serializer;

    move-result-object v0

    return-object v0
.end method

.method public buildCommand(Z)Lcom/android/exchange/adapter/Serializer;
    .locals 6
    .param p1, "isProvision"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    iget-wide v2, p0, Lcom/android/exchange/DeviceInformation;->mProtocolVersion:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_1

    .line 146
    const/4 v0, 0x0

    .line 171
    :cond_0
    :goto_0
    return-object v0

    .line 148
    :cond_1
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 150
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    if-eqz p1, :cond_3

    .line 151
    const/16 v1, 0x385

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 155
    :goto_1
    const/16 v1, 0x496

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x488

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x497

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mModel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x498

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x499

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mFriendlyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x49a

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mOs:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x49b

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mOsLanguage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x49c

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 160
    iget-wide v2, p0, Lcom/android/exchange/DeviceInformation;->mProtocolVersion:D

    const-wide/high16 v4, 0x402c000000000000L    # 14.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_2

    .line 161
    const/16 v1, 0x4a0

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mUserAgent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x4a1

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    iget-boolean v1, p0, Lcom/android/exchange/DeviceInformation;->mOutBoundSms:Z

    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    const-string v1, "1"

    :goto_2
    invoke-virtual {v2, v1}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x4a2

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/DeviceInformation;->mNetworkOperator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 166
    :cond_2
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 168
    if-nez p1, :cond_0

    .line 169
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->done()V

    goto/16 :goto_0

    .line 153
    :cond_3
    const/16 v1, 0x485

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    goto/16 :goto_1

    .line 161
    :cond_4
    const-string v1, "0"

    goto :goto_2
.end method

.method public prepareDeviceInformation(Landroid/content/Context;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    if-nez p1, :cond_0

    .line 60
    const-string v4, "DeviceInformation"

    const-string v5, "context is null"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :goto_0
    return v3

    .line 64
    :cond_0
    if-nez p2, :cond_1

    .line 65
    const-string v5, "DeviceInformation"

    const-string v6, "userAgent is null"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string p2, ""

    .line 70
    :cond_1
    const-string v5, "phone"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 72
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_2

    .line 73
    const-string v4, "DeviceInformation"

    const-string v5, "tm is null"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_2
    iput-object p2, p0, Lcom/android/exchange/DeviceInformation;->mUserAgent:Ljava/lang/String;

    .line 81
    :try_start_0
    sget-object v5, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mModel:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mId:Ljava/lang/String;

    .line 90
    iget-object v5, p0, Lcom/android/exchange/DeviceInformation;->mId:Ljava/lang/String;

    if-nez v5, :cond_3

    .line 91
    const-string v5, ""

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mId:Ljava/lang/String;

    .line 96
    :cond_3
    :try_start_1
    sget-object v5, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mFriendlyName:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    const-string v5, "Android"

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mOs:Ljava/lang/String;

    .line 104
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mOsLanguage:Ljava/lang/String;

    .line 106
    iget-object v5, p0, Lcom/android/exchange/DeviceInformation;->mOsLanguage:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 107
    const-string v5, "English"

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mOsLanguage:Ljava/lang/String;

    .line 109
    :cond_4
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    .line 110
    iget-object v5, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    if-nez v5, :cond_5

    .line 111
    const-string v5, "DeviceInformation"

    const-string v6, "mPhoneNumber is null"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "imsi":Ljava/lang/String;
    const-string v5, "DeviceInformation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "imsi is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    if-eqz v1, :cond_7

    .line 116
    iput-object v1, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    .line 119
    :goto_1
    const-string v5, "DeviceInformation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mPhoneNumber is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    .end local v1    # "imsi":Ljava/lang/String;
    :cond_5
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mNetworkOperator:Ljava/lang/String;

    .line 125
    if-eqz p3, :cond_8

    .line 126
    iget v5, p3, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v5, v5, 0x800

    if-eqz v5, :cond_6

    move v3, v4

    :cond_6
    iput-boolean v3, p0, Lcom/android/exchange/DeviceInformation;->mOutBoundSms:Z

    .line 134
    :goto_2
    const/4 v2, 0x0

    move v3, v4

    .line 136
    goto/16 :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "DeviceInformation"

    const-string v5, "Accessing Build.Model caused Exception"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 97
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 98
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v4, "DeviceInformation"

    const-string v5, "Accessing Build.Product caused Exception"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 118
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "imsi":Ljava/lang/String;
    :cond_7
    const-string v5, ""

    iput-object v5, p0, Lcom/android/exchange/DeviceInformation;->mPhoneNumber:Ljava/lang/String;

    goto :goto_1

    .line 128
    .end local v1    # "imsi":Ljava/lang/String;
    :cond_8
    iput-boolean v3, p0, Lcom/android/exchange/DeviceInformation;->mOutBoundSms:Z

    goto :goto_2
.end method
