.class public Lcom/android/exchange/TasksSyncAdapterService;
.super Landroid/app/Service;
.source "TasksSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final ID_PROJECTION:[Ljava/lang/String;

.field private static final ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

.field private static sSyncAdapter:Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

.field private static final sSyncAdapterLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    .line 50
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/TasksSyncAdapterService;->ID_PROJECTION:[Ljava/lang/String;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "syncKey"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "syncInterval"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/exchange/TasksSyncAdapterService;->ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 59
    return-void
.end method

.method static performSync(Landroid/content/Context;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 32
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    .line 135
    const-string v3, "EAS TasksSyncAdapterService"

    const-string v6, "Inside TaskSyncADapterService"

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 140
    .local v2, "cr":Landroid/content/ContentResolver;
    if-nez v2, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    const/4 v14, 0x0

    .line 148
    .local v14, "accountCursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent;->CONTROLED_SYNC_PROJECTION:[Ljava/lang/String;

    const-string v5, "emailAddress=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 155
    if-eqz v14, :cond_1b

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_1b

    .line 157
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 158
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 162
    .local v16, "accountId":J
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 164
    .local v30, "syncInterval":J
    const-wide/16 v6, -0x1

    cmp-long v3, v30, v6

    if-nez v3, :cond_2

    .line 165
    const-string v3, "force"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "ignore_settings"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result v3

    if-nez v3, :cond_2

    .line 410
    if-eqz v14, :cond_0

    .line 411
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 186
    :cond_2
    const/4 v3, 0x1

    :try_start_1
    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "accountKey"

    aput-object v6, v4, v3

    .line 189
    .local v4, "TASK_ACCOUNT_KEY_PROJECTION":[Ljava/lang/String;
    const/4 v3, 0x1

    new-array v12, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "_id"

    aput-object v6, v12, v3

    .line 192
    .local v12, "MAILBOX_ID_PROJECTION":[Ljava/lang/String;
    const-string v13, "accountKey="

    .line 193
    .local v13, "TASK_ACCOUNT_KEY_SELECTION":Ljava/lang/String;
    const-string v5, "_sync_dirty=1"

    .line 194
    .local v5, "sync_dirty":Ljava/lang/String;
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 198
    .local v26, "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    const/4 v15, 0x0

    .line 200
    .local v15, "c":Landroid/database/Cursor;
    :try_start_2
    sget-object v3, Lcom/android/emailcommon/provider/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v15

    .line 210
    :goto_1
    if-eqz v15, :cond_9

    .line 211
    :cond_3
    :goto_2
    :try_start_3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 212
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 220
    .local v18, "accountKey":J
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v8, Lcom/android/exchange/TasksSyncAdapterService;->ID_PROJECTION:[Ljava/lang/String;

    const-string v9, "accountKey=? AND type=67"

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v3

    const/4 v11, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v23

    .line 225
    .local v23, "mailboxCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_8

    .line 226
    :cond_4
    :goto_3
    :try_start_4
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 227
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 230
    .local v24, "mailboxId":J
    const-string v3, "EAS TasksSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " The mailbox id for newly inserted tasks to sync is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 233
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 238
    .end local v24    # "mailboxId":J
    :catchall_0
    move-exception v3

    if-eqz v23, :cond_5

    .line 239
    :try_start_5
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 245
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v3

    if-eqz v15, :cond_6

    .line 246
    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 410
    .end local v4    # "TASK_ACCOUNT_KEY_PROJECTION":[Ljava/lang/String;
    .end local v5    # "sync_dirty":Ljava/lang/String;
    .end local v12    # "MAILBOX_ID_PROJECTION":[Ljava/lang/String;
    .end local v13    # "TASK_ACCOUNT_KEY_SELECTION":Ljava/lang/String;
    .end local v15    # "c":Landroid/database/Cursor;
    .end local v16    # "accountId":J
    .end local v26    # "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v30    # "syncInterval":J
    :catchall_2
    move-exception v3

    if-eqz v14, :cond_7

    .line 411
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v3

    .line 202
    .restart local v4    # "TASK_ACCOUNT_KEY_PROJECTION":[Ljava/lang/String;
    .restart local v5    # "sync_dirty":Ljava/lang/String;
    .restart local v12    # "MAILBOX_ID_PROJECTION":[Ljava/lang/String;
    .restart local v13    # "TASK_ACCOUNT_KEY_SELECTION":Ljava/lang/String;
    .restart local v15    # "c":Landroid/database/Cursor;
    .restart local v16    # "accountId":J
    .restart local v26    # "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v30    # "syncInterval":J
    :catch_0
    move-exception v28

    .line 203
    .local v28, "sqex":Landroid/database/sqlite/SQLiteException;
    const/4 v15, 0x0

    .line 204
    :try_start_7
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_1

    .line 238
    .end local v28    # "sqex":Landroid/database/sqlite/SQLiteException;
    .restart local v18    # "accountKey":J
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_8
    if-eqz v23, :cond_3

    .line 239
    :try_start_8
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    .line 245
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_9
    if-eqz v15, :cond_a

    .line 246
    :try_start_9
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 252
    :cond_a
    :try_start_a
    sget-object v7, Lcom/android/emailcommon/provider/Tasks;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v2

    move-object v8, v4

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v15

    .line 261
    :goto_4
    if-eqz v15, :cond_10

    .line 262
    :cond_b
    :goto_5
    :try_start_b
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 263
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 264
    .restart local v18    # "accountKey":J
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v8, Lcom/android/exchange/TasksSyncAdapterService;->ID_PROJECTION:[Ljava/lang/String;

    const-string v9, "accountKey=? AND type=67"

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v3

    const/4 v11, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-result-object v23

    .line 269
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_f

    .line 270
    :cond_c
    :goto_6
    :try_start_c
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 271
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 272
    .restart local v24    # "mailboxId":J
    const-string v3, "EAS TasksSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " The mailbox id for newly deleted tasks to sync is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 275
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    goto :goto_6

    .line 280
    .end local v24    # "mailboxId":J
    :catchall_3
    move-exception v3

    if-eqz v23, :cond_d

    .line 281
    :try_start_d
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    .line 287
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :catchall_4
    move-exception v3

    if-eqz v15, :cond_e

    .line 288
    :try_start_e
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v3

    .line 254
    :catch_1
    move-exception v28

    .line 255
    .restart local v28    # "sqex":Landroid/database/sqlite/SQLiteException;
    const/4 v15, 0x0

    .line 256
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    goto :goto_4

    .line 280
    .end local v28    # "sqex":Landroid/database/sqlite/SQLiteException;
    .restart local v18    # "accountKey":J
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_f
    if-eqz v23, :cond_b

    .line 281
    :try_start_f
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto :goto_5

    .line 287
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_10
    if-eqz v15, :cond_11

    .line 288
    :try_start_10
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 294
    :cond_11
    :try_start_11
    sget-object v7, Lcom/android/emailcommon/provider/Tasks;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v2

    move-object v8, v4

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_11
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_11 .. :try_end_11} :catch_2
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    move-result-object v15

    .line 303
    :goto_7
    if-eqz v15, :cond_17

    .line 304
    :cond_12
    :goto_8
    :try_start_12
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 305
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 306
    .restart local v18    # "accountKey":J
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v8, Lcom/android/exchange/TasksSyncAdapterService;->ID_PROJECTION:[Ljava/lang/String;

    const-string v9, "accountKey=? AND type=67"

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v3

    const/4 v11, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    move-result-object v23

    .line 311
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_16

    .line 312
    :cond_13
    :goto_9
    :try_start_13
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 313
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 314
    .restart local v24    # "mailboxId":J
    const-string v3, "EAS TasksSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " The mailbox id for newly updated tasks to sync is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    .line 317
    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    goto :goto_9

    .line 322
    .end local v24    # "mailboxId":J
    :catchall_5
    move-exception v3

    if-eqz v23, :cond_14

    .line 323
    :try_start_14
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_14
    throw v3
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 329
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :catchall_6
    move-exception v3

    if-eqz v15, :cond_15

    .line 330
    :try_start_15
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_15
    throw v3

    .line 296
    :catch_2
    move-exception v28

    .line 297
    .restart local v28    # "sqex":Landroid/database/sqlite/SQLiteException;
    const/4 v15, 0x0

    .line 298
    invoke-virtual/range {v28 .. v28}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    goto :goto_7

    .line 322
    .end local v28    # "sqex":Landroid/database/sqlite/SQLiteException;
    .restart local v18    # "accountKey":J
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_16
    if-eqz v23, :cond_12

    .line 323
    :try_start_16
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    goto :goto_8

    .line 329
    .end local v18    # "accountKey":J
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    :cond_17
    if-eqz v15, :cond_18

    .line 330
    :try_start_17
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 335
    :cond_18
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Long;

    .line 337
    .local v24, "mailboxId":Ljava/lang/Long;
    const-string v3, "EAS TasksSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "goign to notifify tasks mailboxes "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v3, 0x0

    invoke-static {v6, v7, v3}, Lcom/android/exchange/ExchangeService;->serviceRequest(JI)V

    goto :goto_a

    .line 340
    .end local v24    # "mailboxId":Ljava/lang/Long;
    :cond_19
    const-string v3, "EAS TasksSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Tasks Changed/Deleted messages: , mailboxes: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    sget-object v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v8, Lcom/android/exchange/TasksSyncAdapterService;->ID_SYNC_KEY_PROJECTION:[Ljava/lang/String;

    const-string v9, "accountKey=? AND type=67"

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v10, v3

    const/4 v11, 0x0

    move-object v6, v2

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    move-result-object v23

    .line 356
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    if-eqz v23, :cond_1d

    :try_start_18
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 358
    const-string v3, "EAS TasksSyncAdapterService"

    const-string v6, " Manual sync for Tasks "

    invoke-static {v3, v6}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const/4 v3, 0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_4
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    move-result-object v29

    .line 362
    .local v29, "syncKey":Ljava/lang/String;
    const/16 v22, -0x2

    .line 366
    .local v22, "mSyncInterval":I
    const/4 v3, 0x2

    :try_start_19
    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_19
    .catch Ljava/lang/NullPointerException; {:try_start_19 .. :try_end_19} :catch_3
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_4
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    move-result v22

    .line 374
    :goto_b
    :try_start_1a
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "performSync() for Tasks: mSyncInterval :"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 377
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/exchange/ExchangeService;->checkServiceExist(J)Z
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_4
    .catchall {:try_start_1a .. :try_end_1a} :catchall_7

    move-result v3

    if-eqz v3, :cond_1c

    .line 398
    if-eqz v23, :cond_1a

    .line 400
    :try_start_1b
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    .line 410
    :cond_1a
    if-eqz v14, :cond_0

    .line 411
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 368
    :catch_3
    move-exception v27

    .line 370
    .local v27, "ne":Ljava/lang/NullPointerException;
    :try_start_1c
    invoke-virtual/range {v27 .. v27}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_4
    .catchall {:try_start_1c .. :try_end_1c} :catchall_7

    goto :goto_b

    .line 393
    .end local v22    # "mSyncInterval":I
    .end local v27    # "ne":Ljava/lang/NullPointerException;
    .end local v29    # "syncKey":Ljava/lang/String;
    :catch_4
    move-exception v20

    .line 394
    .local v20, "e":Ljava/lang/Exception;
    :try_start_1d
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_7

    .line 398
    if-eqz v23, :cond_1b

    .line 400
    :try_start_1e
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_2

    .line 410
    .end local v4    # "TASK_ACCOUNT_KEY_PROJECTION":[Ljava/lang/String;
    .end local v5    # "sync_dirty":Ljava/lang/String;
    .end local v12    # "MAILBOX_ID_PROJECTION":[Ljava/lang/String;
    .end local v13    # "TASK_ACCOUNT_KEY_SELECTION":Ljava/lang/String;
    .end local v15    # "c":Landroid/database/Cursor;
    .end local v16    # "accountId":J
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v23    # "mailboxCursor":Landroid/database/Cursor;
    .end local v26    # "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v30    # "syncInterval":J
    :cond_1b
    :goto_c
    if-eqz v14, :cond_0

    .line 411
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 384
    .restart local v4    # "TASK_ACCOUNT_KEY_PROJECTION":[Ljava/lang/String;
    .restart local v5    # "sync_dirty":Ljava/lang/String;
    .restart local v12    # "MAILBOX_ID_PROJECTION":[Ljava/lang/String;
    .restart local v13    # "TASK_ACCOUNT_KEY_SELECTION":Ljava/lang/String;
    .restart local v15    # "c":Landroid/database/Cursor;
    .restart local v16    # "accountId":J
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v22    # "mSyncInterval":I
    .restart local v23    # "mailboxCursor":Landroid/database/Cursor;
    .restart local v26    # "mailboxesToNotify":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .restart local v29    # "syncKey":Ljava/lang/String;
    .restart local v30    # "syncInterval":J
    :cond_1c
    const/4 v3, 0x0

    :try_start_1f
    move-object/from16 v0, v23

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v3, 0x0

    invoke-static {v6, v7, v3}, Lcom/android/exchange/ExchangeService;->serviceRequest(JI)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_4
    .catchall {:try_start_1f .. :try_end_1f} :catchall_7

    .line 398
    .end local v22    # "mSyncInterval":I
    .end local v29    # "syncKey":Ljava/lang/String;
    :cond_1d
    if-eqz v23, :cond_1b

    .line 400
    :try_start_20
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto :goto_c

    .line 398
    :catchall_7
    move-exception v3

    if-eqz v23, :cond_1e

    .line 400
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_1e
    throw v3
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_2
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 108
    sget-object v0, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 99
    sget-object v1, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    sget-object v0, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {p0}, Lcom/android/exchange/TasksSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/exchange/TasksSyncAdapterService;->sSyncAdapter:Lcom/android/exchange/TasksSyncAdapterService$SyncAdapterImpl;

    .line 103
    :cond_0
    monitor-exit v1

    .line 104
    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
