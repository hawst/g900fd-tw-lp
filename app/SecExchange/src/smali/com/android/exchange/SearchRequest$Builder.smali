.class public final Lcom/android/exchange/SearchRequest$Builder;
.super Ljava/lang/Object;
.source "SearchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/SearchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private mAccountId:J

.field private mConversationId:Ljava/lang/String;

.field private mOptionsBodyPreferenceTruncationSize:I

.field private mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

.field private mOptionsDeepTraversal:Z

.field private mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

.field private mOptionsRange:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mOptionsRebuildResults:Z

.field private mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

.field private mQueryCollectionIds:[J

.field private mQueryFreeText:Ljava/lang/String;

.field private mQueryGreaterThan:Ljava/util/Date;

.field private mQueryLessThan:Ljava/util/Date;

.field private mQueryText:Ljava/lang/String;

.field private mStoreName:Lcom/android/exchange/SearchRequest$StoreName;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mAccountId:J

    .line 356
    sget-object v0, Lcom/android/exchange/SearchRequest$StoreName;->INVALID:Lcom/android/exchange/SearchRequest$StoreName;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    .line 357
    sget-object v0, Lcom/android/exchange/SearchRequest$QueryClass;->INVALID:Lcom/android/exchange/SearchRequest$QueryClass;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    .line 358
    sget-object v0, Lcom/android/exchange/SearchRequest$BodyPreferenceType;->INVALID:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .line 359
    sget-object v0, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->INVALID:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .line 360
    sget-object v0, Lcom/android/exchange/SearchRequest$BodyPreferenceType;->INVALID:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .line 361
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceTruncationSize:I

    .line 362
    sget-object v0, Lcom/android/exchange/SearchRequest$OptionsMIMESupport;->INVALID:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .line 363
    return-void
.end method


# virtual methods
.method public accountId(J)Lcom/android/exchange/SearchRequest$Builder;
    .locals 1
    .param p1, "accountId"    # J

    .prologue
    .line 366
    iput-wide p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mAccountId:J

    .line 367
    return-object p0
.end method

.method public build()Lcom/android/exchange/SearchRequest;
    .locals 20

    .prologue
    .line 443
    new-instance v3, Lcom/android/exchange/SearchRequest$SearchRequestImpl;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/exchange/SearchRequest$Builder;->mAccountId:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/SearchRequest$Builder;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryText:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryFreeText:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryCollectionIds:[J

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryGreaterThan:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/exchange/SearchRequest$Builder;->mQueryLessThan:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsRange:Landroid/util/Pair;

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsDeepTraversal:Z

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsRebuildResults:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceTruncationSize:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/SearchRequest$Builder;->mConversationId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v3 .. v19}, Lcom/android/exchange/SearchRequest$SearchRequestImpl;-><init>(JLcom/android/exchange/SearchRequest$StoreName;Ljava/lang/String;Ljava/lang/String;Lcom/android/exchange/SearchRequest$QueryClass;[JLjava/util/Date;Ljava/util/Date;Landroid/util/Pair;ZZLcom/android/exchange/SearchRequest$BodyPreferenceType;ILcom/android/exchange/SearchRequest$OptionsMIMESupport;Ljava/lang/String;)V

    return-object v3
.end method

.method public optionsBodyPreferenceTruncationSize(I)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "optionsBodyPreferenceTruncationSize"    # I

    .prologue
    .line 433
    iput p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceTruncationSize:I

    .line 434
    return-object p0
.end method

.method public optionsBodyPreferenceType(Lcom/android/exchange/SearchRequest$BodyPreferenceType;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "optionsBodyPreferenceType"    # Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .prologue
    .line 428
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsBodyPreferenceType:Lcom/android/exchange/SearchRequest$BodyPreferenceType;

    .line 429
    return-object p0
.end method

.method public optionsDeepTraversal()Lcom/android/exchange/SearchRequest$Builder;
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsDeepTraversal:Z

    .line 419
    return-object p0
.end method

.method public optionsMIMESupport(Lcom/android/exchange/SearchRequest$OptionsMIMESupport;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "optionsMIMESupport"    # Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .prologue
    .line 438
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsMIMESupport:Lcom/android/exchange/SearchRequest$OptionsMIMESupport;

    .line 439
    return-object p0
.end method

.method public optionsRange(II)Lcom/android/exchange/SearchRequest$Builder;
    .locals 3
    .param p1, "startRange"    # I
    .param p2, "endRange"    # I

    .prologue
    .line 413
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsRange:Landroid/util/Pair;

    .line 414
    return-object p0
.end method

.method public optionsRebuildResults()Lcom/android/exchange/SearchRequest$Builder;
    .locals 1

    .prologue
    .line 423
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/SearchRequest$Builder;->mOptionsRebuildResults:Z

    .line 424
    return-object p0
.end method

.method public queryClass(Lcom/android/exchange/SearchRequest$QueryClass;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "queryClass"    # Lcom/android/exchange/SearchRequest$QueryClass;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryClass:Lcom/android/exchange/SearchRequest$QueryClass;

    .line 394
    return-object p0
.end method

.method public queryCollectionIds([J)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "queryCollectionIds"    # [J

    .prologue
    .line 398
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryCollectionIds:[J

    .line 399
    return-object p0
.end method

.method public queryConvId(Ljava/lang/String;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "convId"    # Ljava/lang/String;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mConversationId:Ljava/lang/String;

    .line 388
    return-object p0
.end method

.method public queryFreeText(Ljava/lang/String;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryFreeText:Ljava/lang/String;

    .line 382
    return-object p0
.end method

.method public queryGreaterThan(Ljava/util/Date;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "queryGreaterThan"    # Ljava/util/Date;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryGreaterThan:Ljava/util/Date;

    .line 404
    return-object p0
.end method

.method public queryLessThan(Ljava/util/Date;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "queryLessThan"    # Ljava/util/Date;

    .prologue
    .line 408
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mQueryLessThan:Ljava/util/Date;

    .line 409
    return-object p0
.end method

.method public storeName(Lcom/android/exchange/SearchRequest$StoreName;)Lcom/android/exchange/SearchRequest$Builder;
    .locals 0
    .param p1, "storeName"    # Lcom/android/exchange/SearchRequest$StoreName;

    .prologue
    .line 371
    iput-object p1, p0, Lcom/android/exchange/SearchRequest$Builder;->mStoreName:Lcom/android/exchange/SearchRequest$StoreName;

    .line 372
    return-object p0
.end method
