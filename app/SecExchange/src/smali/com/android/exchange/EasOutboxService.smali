.class public Lcom/android/exchange/EasOutboxService;
.super Lcom/android/exchange/EasSyncService;
.source "EasOutboxService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    }
.end annotation


# static fields
.field public static final BODY_HTML_CONTENT_PROJECTION:[Ljava/lang/String;

.field public static final BODY_SOURCE_PROJECTION:[Ljava/lang/String;

.field public static seletedMsgIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static serviceLogger:Lcom/android/exchange/ServiceLogger;


# instance fields
.field private bMessageSent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    .line 129
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "sourceMessageKey"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/EasOutboxService;->BODY_SOURCE_PROJECTION:[Ljava/lang/String;

    .line 134
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "htmlContent"

    aput-object v1, v0, v2

    sput-object v0, Lcom/android/exchange/EasOutboxService;->BODY_HTML_CONTENT_PROJECTION:[Ljava/lang/String;

    .line 150
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    .line 153
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/exchange/EasOutboxService;->bMessageSent:Z

    .line 154
    return-void
.end method

.method private cancelMessageSeletion()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1792
    const/4 v7, 0x1

    .line 1799
    .local v7, "first":Z
    const/4 v6, 0x0

    .line 1800
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x1

    .line 1801
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, "mailboxKey in ("

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1803
    .local v8, "sb1":Ljava/lang/StringBuilder;
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/EasOutboxService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "type = 4"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1806
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1808
    :cond_0
    if-nez v7, :cond_3

    .line 1809
    const/16 v0, 0x2c

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1813
    :goto_0
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1814
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1815
    const/16 v0, 0x29

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1820
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1824
    :cond_1
    const-string v0, " and "

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_1
    return-object v0

    .line 1811
    :cond_3
    const/4 v7, 0x0

    goto :goto_0

    .line 1817
    :cond_4
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1820
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1820
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1821
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private createEntityForForward(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;
    .locals 36
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "response"    # I

    .prologue
    .line 751
    new-instance v23, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 752
    .local v23, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    const-string v31, "ORGMAIL"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v7

    .line 753
    .local v7, "addrs":[Lcom/android/emailcommon/mail/Address;
    array-length v0, v7

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_1

    .line 754
    const/16 v19, 0x0

    .line 847
    :cond_0
    :goto_0
    return-object v19

    .line 755
    :cond_1
    const/16 v31, 0x0

    aget-object v31, v7, v31

    invoke-virtual/range {v31 .. v31}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v25

    .line 756
    .local v25, "organizerEmail":Ljava/lang/String;
    const-string v31, "DTSTAMP"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 757
    .local v14, "dtStamp":Ljava/lang/String;
    const-string v31, "DTSTART"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 758
    .local v15, "dtStart":Ljava/lang/String;
    const-string v31, "DTEND"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 759
    .local v13, "dtEnd":Ljava/lang/String;
    if-nez v13, :cond_2

    if-eqz v15, :cond_2

    .line 760
    const-string v31, "DURATION"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 761
    .local v17, "duration":Ljava/lang/String;
    if-eqz v17, :cond_2

    .line 762
    new-instance v16, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct/range {v16 .. v16}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V

    .line 764
    .local v16, "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_0
    invoke-virtual/range {v16 .. v17}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 765
    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J

    move-result-wide v34

    add-long v32, v32, v34

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 766
    .local v12, "d":Ljava/lang/Long;
    invoke-virtual {v12}, Ljava/lang/Long;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 772
    .end local v12    # "d":Ljava/lang/Long;
    .end local v16    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v17    # "duration":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v14, :cond_3

    if-eqz v15, :cond_3

    if-nez v13, :cond_4

    .line 773
    :cond_3
    const/16 v19, 0x0

    goto :goto_0

    .line 767
    .restart local v16    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .restart local v17    # "duration":Ljava/lang/String;
    :catch_0
    move-exception v18

    .line 768
    .local v18, "e":Ljava/text/ParseException;
    const-string v31, "[Eas]OutboxService"

    const-string v32, "ParseException in createEntityForForward"

    invoke-static/range {v31 .. v32}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 775
    .end local v16    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v17    # "duration":Ljava/lang/String;
    .end local v18    # "e":Ljava/text/ParseException;
    :cond_4
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 776
    .local v20, "entityValues":Landroid/content/ContentValues;
    new-instance v19, Landroid/content/Entity;

    invoke-direct/range {v19 .. v20}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    .line 778
    .local v19, "entity":Landroid/content/Entity;
    const-string v31, "-"

    move-object/from16 v0, v31

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_5

    .line 779
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Lcom/android/exchange/utility/CalendarUtilities;->millisToInstanceId(J)Ljava/lang/String;

    move-result-object v14

    .line 782
    :cond_5
    const-string v31, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    const-string v31, "-"

    move-object/from16 v0, v31

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_6

    const-string v31, "T"

    move-object/from16 v0, v31

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 786
    :cond_6
    const-string v31, "dtstart"

    invoke-static {v15}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 790
    :goto_2
    const-string v31, "-"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-nez v31, :cond_7

    const-string v31, "T"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 791
    :cond_7
    const-string v31, "dtend"

    invoke-static {v13}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 796
    :goto_3
    const-string v31, "IS_ALLDAY"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 797
    .local v9, "alldayValue":Ljava/lang/String;
    if-eqz v9, :cond_b

    .line 798
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 799
    .local v8, "allday":I
    const-string v31, "allDay"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 803
    .end local v8    # "allday":I
    :goto_4
    const-string v31, "SENSITIVITY"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 804
    .local v30, "sensitivityValue":Ljava/lang/String;
    if-eqz v30, :cond_c

    .line 805
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v29

    .line 806
    .local v29, "sensitivity":I
    const-string v31, "calendar_access_level"

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 810
    .end local v29    # "sensitivity":I
    :goto_5
    const-string v31, "eventLocation"

    const-string v32, "LOC"

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    const-string v31, "title"

    const-string v32, "TITLE"

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    const-string v31, "organizer"

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v31, "RRULE"

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 814
    .local v28, "rRule":Ljava/lang/String;
    if-eqz v28, :cond_8

    .line 815
    const-string v31, "rrule"

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_8
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 820
    .local v26, "organizerValues":Landroid/content/ContentValues;
    const-string v31, "attendeeRelationship"

    const/16 v32, 0x2

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 821
    const-string v31, "attendeeEmail"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v32, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const-string v31, "attendeeStatus"

    const/16 v32, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 824
    sget-object v31, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 826
    new-instance v27, Landroid/content/ContentValues;

    invoke-direct/range {v27 .. v27}, Landroid/content/ContentValues;-><init>()V

    .line 827
    .local v27, "organizerValues1":Landroid/content/ContentValues;
    const-string v31, "attendeeRelationship"

    const/16 v32, 0x2

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 828
    const-string v31, "attendeeEmail"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const-string v31, "attendeeStatus"

    const/16 v32, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 831
    sget-object v31, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 835
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v5

    .line 836
    .local v5, "addr":[Lcom/android/emailcommon/mail/Address;
    move-object v10, v5

    .local v10, "arr$":[Lcom/android/emailcommon/mail/Address;
    array-length v0, v10

    move/from16 v22, v0

    .local v22, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_6
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    aget-object v4, v10, v21

    .line 837
    .local v4, "add":Lcom/android/emailcommon/mail/Address;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 838
    .local v11, "attendeeValues":Landroid/content/ContentValues;
    invoke-virtual {v4}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v6

    .line 839
    .local v6, "address":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v24

    .line 840
    .local v24, "name":Ljava/lang/String;
    const-string v31, "attendeeRelationship"

    const/16 v32, 0x1

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 841
    const-string v31, "attendeeEmail"

    move-object/from16 v0, v31

    invoke-virtual {v11, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    const-string v31, "attendeeName"

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    const-string v31, "attendeeStatus"

    const/16 v32, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 844
    sget-object v31, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v11}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 836
    add-int/lit8 v21, v21, 0x1

    goto :goto_6

    .line 788
    .end local v4    # "add":Lcom/android/emailcommon/mail/Address;
    .end local v5    # "addr":[Lcom/android/emailcommon/mail/Address;
    .end local v6    # "address":Ljava/lang/String;
    .end local v9    # "alldayValue":Ljava/lang/String;
    .end local v10    # "arr$":[Lcom/android/emailcommon/mail/Address;
    .end local v11    # "attendeeValues":Landroid/content/ContentValues;
    .end local v21    # "i$":I
    .end local v22    # "len$":I
    .end local v24    # "name":Ljava/lang/String;
    .end local v26    # "organizerValues":Landroid/content/ContentValues;
    .end local v27    # "organizerValues1":Landroid/content/ContentValues;
    .end local v28    # "rRule":Ljava/lang/String;
    .end local v30    # "sensitivityValue":Ljava/lang/String;
    :cond_9
    const-string v31, "dtstart"

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    .line 793
    :cond_a
    const-string v31, "dtend"

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    .line 801
    .restart local v9    # "alldayValue":Ljava/lang/String;
    :cond_b
    const-string v31, "allDay"

    const/16 v32, 0x0

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 808
    .restart local v30    # "sensitivityValue":Ljava/lang/String;
    :cond_c
    const-string v31, "calendar_access_level"

    const/16 v32, 0x3

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    move-object/from16 v0, v20

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_5
.end method

.method private createIcsNAttachForForward(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V
    .locals 26
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "isFromEmail"    # Z

    .prologue
    .line 661
    const/16 v7, 0x10

    .line 662
    .local v7, "flag":I
    sget-object v4, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v4, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v16

    .line 663
    .local v16, "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 664
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 667
    :cond_0
    if-eqz v16, :cond_2

    .line 668
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_0
    move-object/from16 v0, v16

    array-length v4, v0

    move/from16 v0, v21

    if-ge v0, v4, :cond_2

    .line 669
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v4, :cond_1

    .line 670
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iput-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 672
    :cond_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    aget-object v5, v16, v21

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 668
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 676
    .end local v21    # "i":I
    :cond_2
    if-eqz p2, :cond_5

    .line 677
    new-instance v22, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 678
    .local v22, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    const-string v4, "RESPONSE"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 679
    .local v23, "resp":Ljava/lang/String;
    if-eqz v23, :cond_4

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 680
    .local v24, "response":I
    :goto_1
    const/4 v6, 0x0

    .line 681
    .local v6, "entity":Landroid/content/Entity;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasOutboxService;->createEntityForForward(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;

    move-result-object v6

    .line 683
    if-eqz v6, :cond_3

    .line 684
    sget-object v4, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    const-string v5, "UID"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v9}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForForwardEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 700
    .end local v23    # "resp":Ljava/lang/String;
    .end local v24    # "response":I
    :cond_3
    :goto_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 701
    .local v15, "attSize":I
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 702
    .local v17, "cv":Landroid/content/ContentValues;
    const-string v5, "accountKey"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v10, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mAccountKey:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 703
    const-string v4, "messageKey"

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 704
    const-string v5, "content_bytes"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 705
    const-string v5, "fileName"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const-string v5, "mimeType"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-object v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMimeType:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const-string v5, "size"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget-wide v10, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mSize:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 708
    const-string v5, "flags"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    add-int/lit8 v9, v15, -0x1

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 709
    sget-object v4, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v17

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 710
    return-void

    .line 679
    .end local v6    # "entity":Landroid/content/Entity;
    .end local v15    # "attSize":I
    .end local v17    # "cv":Landroid/content/ContentValues;
    .restart local v23    # "resp":Ljava/lang/String;
    :cond_4
    const/16 v24, 0x1

    goto/16 :goto_1

    .line 687
    .end local v22    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    .end local v23    # "resp":Ljava/lang/String;
    :cond_5
    new-instance v22, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 688
    .restart local v22    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    const-string v4, "EVENT_ID"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 689
    .local v18, "eventID":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    .line 690
    .local v20, "eventid":Ljava/lang/Long;
    const-string v4, "UID"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 691
    .local v25, "uid":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 692
    .local v8, "cr":Landroid/content/ContentResolver;
    sget-object v4, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    invoke-static {v4, v8}, Landroid/provider/CalendarContract$EventsEntity;->newEntityIterator(Landroid/database/Cursor;Landroid/content/ContentResolver;)Landroid/content/EntityIterator;

    move-result-object v19

    .line 693
    .local v19, "eventIterator":Landroid/content/EntityIterator;
    invoke-interface/range {v19 .. v19}, Landroid/content/EntityIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Entity;

    .line 694
    .restart local v6    # "entity":Landroid/content/Entity;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v6, v1}, Lcom/android/exchange/EasOutboxService;->updateEntityForForward(Landroid/content/Entity;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 695
    sget-object v9, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v10, p1

    move-object v11, v6

    move v12, v7

    move-object/from16 v13, v25

    invoke-static/range {v9 .. v14}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForForwardEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 696
    invoke-interface/range {v19 .. v19}, Landroid/content/EntityIterator;->close()V

    goto/16 :goto_2
.end method

.method public static dumpOutboxMessages(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/emailcommon/provider/EmailContent$Account;)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "outbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .param p2, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 1736
    const-string v0, "[Eas]OutboxService"

    const-string v1, "BEGIN: dumpOutboxMessages()"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1739
    .local v8, "sb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1740
    :cond_0
    const-string v7, "outbox or account can not be null while invoking the method dumpOutboxMessages"

    .line 1741
    .local v7, "errorMsg":Ljava/lang/String;
    const-string v0, "[Eas]OutboxService"

    invoke-static {v0, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    .end local v7    # "errorMsg":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 1745
    :cond_1
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "syncServerId"

    aput-object v0, v2, v9

    const-string v0, "flagAttachment"

    aput-object v0, v2, v10

    const-string v0, "messageType"

    aput-object v0, v2, v11

    const-string v0, "retrySendTimes"

    aput-object v0, v2, v12

    .line 1749
    .local v2, "MESSAGE_PROJECTION":[Ljava/lang/String;
    const-string v3, "mailboxKey=? and accountKey=?"

    .line 1750
    .local v3, "SELECTION":Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    iget-wide v0, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    iget-wide v0, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    .line 1755
    .local v4, "SELECTION_ARGS":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1757
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1759
    if-eqz v6, :cond_6

    .line 1760
    const-string v0, " Outbox details:\n"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  mDisplayName="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  mAccountKey="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  mAutoRetryTimes="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAutoRetryTimes()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1764
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 1765
    const-string v0, "\tNo email in outbox\n\n"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1782
    :cond_2
    :goto_1
    if-eqz v6, :cond_3

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1786
    :cond_3
    :goto_2
    const-string v0, "[Eas]OutboxService"

    const-string v1, "END: dumpOutboxMessages()"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1787
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 1767
    :cond_4
    :goto_3
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1768
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  msgId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " syncServerId="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x1

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " flagAttachment="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x2

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " messageType="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x3

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " retrySendTimes="

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v5, 0x4

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    .line 1781
    :catchall_0
    move-exception v0

    .line 1782
    if-eqz v6, :cond_5

    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 1783
    :cond_5
    :goto_4
    throw v0

    .line 1776
    :cond_6
    :try_start_4
    const-string v7, "Error occured while trying to get a cursor for Messages table"

    .line 1777
    .restart local v7    # "errorMsg":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1778
    const-string v0, "[Eas]OutboxService"

    invoke-static {v0, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1783
    .end local v7    # "errorMsg":Ljava/lang/String;
    :catch_0
    move-exception v0

    goto/16 :goto_2

    :catch_1
    move-exception v1

    goto :goto_4
.end method

.method public static findOrCreateMailboxOfOutBoxType(Landroid/content/Context;JI)J
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxType"    # I

    .prologue
    const-wide/16 v4, -0x1

    .line 1830
    const-wide/16 v6, 0x0

    cmp-long v1, p1, v6

    if-ltz v1, :cond_0

    if-gez p3, :cond_2

    :cond_0
    move-wide v2, v4

    .line 1845
    :cond_1
    :goto_0
    return-wide v2

    .line 1833
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v2

    .line 1834
    .local v2, "mailboxId":J
    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1835
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 1836
    .local v0, "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    iput-wide p1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 1837
    iput p3, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 1838
    const/4 v1, -0x1

    iput v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 1839
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlagVisible:Z

    .line 1840
    const v1, 0x7f06001f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 1841
    iput-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mParentKey:J

    .line 1842
    invoke-virtual {v0, p0}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->save(Landroid/content/Context;)Landroid/net/Uri;

    .line 1843
    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    goto :goto_0
.end method

.method private static getOriginalMessageInfo(Landroid/content/Context;J)Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .locals 35
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "msgId"    # J

    .prologue
    .line 563
    const/4 v8, 0x0

    .line 564
    .local v8, "itemId":Ljava/lang/String;
    const/4 v9, 0x0

    .line 565
    .local v9, "collectionId":Ljava/lang/String;
    const/4 v10, 0x0

    .line 566
    .local v10, "calRecEventInstanceId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 567
    .local v11, "reply":Z
    const/4 v12, 0x0

    .line 568
    .local v12, "forward":Z
    const/4 v13, 0x0

    .line 570
    .local v13, "smartsend":Z
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x2

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const-string v32, "flags"

    aput-object v32, v30, v31

    const/16 v31, 0x1

    const-string v32, "subject"

    aput-object v32, v30, v31

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, v30

    invoke-static {v0, v6, v1, v2, v3}, Lcom/android/emailcommon/utility/Utility;->getRowColumns(Landroid/content/Context;Landroid/net/Uri;J[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 572
    .local v15, "cols":[Ljava/lang/String;
    const/16 v20, 0x0

    .line 573
    .local v20, "flags":I
    const/4 v7, 0x0

    .line 574
    .local v7, "subject":Ljava/lang/String;
    if-eqz v15, :cond_0

    const/4 v6, 0x0

    aget-object v6, v15, v6

    if-eqz v6, :cond_0

    .line 575
    const/4 v6, 0x0

    aget-object v6, v15, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    .line 576
    const/4 v6, 0x1

    aget-object v7, v15, v6

    .line 579
    :cond_0
    and-int/lit8 v6, v20, 0x1

    if-eqz v6, :cond_7

    const/4 v11, 0x1

    .line 580
    :goto_0
    and-int/lit8 v6, v20, 0x2

    if-eqz v6, :cond_8

    const/4 v12, 0x1

    .line 582
    :goto_1
    const-wide/16 v24, 0x0

    .line 583
    .local v24, "refId":J
    if-nez v11, :cond_1

    if-eqz v12, :cond_5

    .line 585
    :cond_1
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Body;->CONTENT_URI:Landroid/net/Uri;

    sget-object v30, Lcom/android/exchange/EasOutboxService;->BODY_SOURCE_PROJECTION:[Ljava/lang/String;

    const-string v31, "messageKey=?"

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v34

    aput-object v34, v32, v33

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    move-object/from16 v3, v32

    invoke-static {v0, v6, v1, v2, v3}, Lcom/android/emailcommon/utility/Utility;->getRowColumns(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 589
    if-eqz v15, :cond_2

    const/4 v6, 0x0

    aget-object v6, v15, v6

    if-eqz v6, :cond_2

    .line 590
    const/4 v6, 0x0

    aget-object v6, v15, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    .line 593
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x2

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const-string v32, "syncServerId"

    aput-object v32, v30, v31

    const/16 v31, 0x1

    const-string v32, "mailboxKey"

    aput-object v32, v30, v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v24

    move-object/from16 v3, v30

    invoke-static {v0, v6, v1, v2, v3}, Lcom/android/emailcommon/utility/Utility;->getRowColumns(Landroid/content/Context;Landroid/net/Uri;J[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 595
    if-eqz v15, :cond_2

    .line 596
    const/4 v6, 0x0

    aget-object v8, v15, v6

    .line 597
    const/4 v6, 0x1

    aget-object v6, v15, v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 599
    .local v16, "boxId":J
    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const-string v32, "serverId"

    aput-object v32, v30, v31

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    move-object/from16 v3, v30

    invoke-static {v0, v6, v1, v2, v3}, Lcom/android/emailcommon/utility/Utility;->getRowColumns(Landroid/content/Context;Landroid/net/Uri;J[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 601
    if-eqz v15, :cond_2

    .line 602
    const/4 v6, 0x0

    aget-object v9, v15, v6

    .line 608
    .end local v16    # "boxId":J
    :cond_2
    :try_start_0
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v28

    .line 609
    .local v28, "tempMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v28, :cond_5

    move-object/from16 v0, v28

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    if-eqz v6, :cond_5

    .line 610
    new-instance v23, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, v28

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-direct {v0, v6}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 611
    .local v23, "packedString":Lcom/android/emailcommon/mail/PackedString;
    const-string v6, "MEETING_FORWARD"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 612
    .local v22, "meetingForward":Ljava/lang/String;
    const-string v6, "1"

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 614
    const-string v6, "EVENT_ID"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 615
    .local v19, "eventId":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v30

    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    move-wide/from16 v3, p1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageBodyforEventId(Landroid/content/Context;JJ)V

    .line 616
    move-object/from16 v0, v28

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v30, v0

    const/16 v6, 0x41

    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-static {v0, v1, v2, v6}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v21

    .line 617
    .local v21, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v21, :cond_3

    .line 618
    move-object/from16 v0, v21

    iget-object v9, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 619
    :cond_3
    const-string v6, "SERVER_ID"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 620
    const-string v6, "DTSTART"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_9

    const-wide/16 v26, 0x0

    .line 622
    .local v26, "startTime":J
    :goto_2
    const-wide/16 v30, 0x0

    cmp-long v6, v26, v30

    if-eqz v6, :cond_5

    .line 623
    const-string v6, "IS_ALLDAY"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_a

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    :goto_3
    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 626
    .local v14, "allDay":Z
    if-eqz v14, :cond_4

    .line 627
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v29

    .line 628
    .local v29, "tz":Ljava/util/TimeZone;
    move-wide/from16 v0, v26

    move-object/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/android/exchange/utility/CalendarUtilities;->getLocalAllDayCalendarTime(JLjava/util/TimeZone;)J

    move-result-wide v26

    .line 630
    .end local v29    # "tz":Ljava/util/TimeZone;
    :cond_4
    invoke-static/range {v26 .. v27}, Lcom/android/exchange/utility/CalendarUtilities;->millisToInstanceId(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 640
    .end local v14    # "allDay":Z
    .end local v19    # "eventId":Ljava/lang/String;
    .end local v21    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v22    # "meetingForward":Ljava/lang/String;
    .end local v23    # "packedString":Lcom/android/emailcommon/mail/PackedString;
    .end local v26    # "startTime":J
    .end local v28    # "tempMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_5
    :goto_4
    if-eqz v8, :cond_b

    if-eqz v9, :cond_b

    const/4 v13, 0x1

    .line 642
    :goto_5
    sget-object v6, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v6}, Lcom/android/exchange/utility/CalendarUtilities;->isMeetingForwardWithInline(JLandroid/content/Context;)I

    move-result v6

    const/16 v30, -0x1

    move/from16 v0, v30

    if-eq v6, v0, :cond_6

    .line 643
    const/4 v13, 0x0

    .line 647
    :cond_6
    new-instance v6, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;

    invoke-direct/range {v6 .. v13}, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    return-object v6

    .line 579
    .end local v24    # "refId":J
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 580
    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 620
    .restart local v19    # "eventId":Ljava/lang/String;
    .restart local v21    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v22    # "meetingForward":Ljava/lang/String;
    .restart local v23    # "packedString":Lcom/android/emailcommon/mail/PackedString;
    .restart local v24    # "refId":J
    .restart local v28    # "tempMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_9
    :try_start_1
    const-string v6, "DTSTART"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v26

    goto :goto_2

    .line 623
    .restart local v26    # "startTime":J
    :cond_a
    const-string v6, "IS_ALLDAY"

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto :goto_3

    .line 634
    .end local v19    # "eventId":Ljava/lang/String;
    .end local v21    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v22    # "meetingForward":Ljava/lang/String;
    .end local v23    # "packedString":Lcom/android/emailcommon/mail/PackedString;
    .end local v26    # "startTime":J
    .end local v28    # "tempMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catch_0
    move-exception v18

    .line 635
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 640
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_b
    const/4 v13, 0x0

    goto :goto_5
.end method

.method private getParseRespCode(Lcom/android/exchange/EasResponse;ZLcom/android/exchange/adapter/ComposeMailAdapter;)I
    .locals 8
    .param p1, "resp"    # Lcom/android/exchange/EasResponse;
    .param p2, "isEAS14"    # Z
    .param p3, "rfAdapter"    # Lcom/android/exchange/adapter/ComposeMailAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 351
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 352
    .local v0, "code":I
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): getParseRespCode() HTTP code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    if-eqz p2, :cond_0

    .line 355
    const/16 v5, 0xc8

    if-ne v0, v5, :cond_0

    .line 356
    move v4, v0

    .line 357
    .local v4, "tmp_code":I
    const/16 v0, 0xc8

    .line 359
    :try_start_0
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v3

    .line 360
    .local v3, "len":I
    if-eqz v3, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 362
    .local v2, "is":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 363
    invoke-virtual {p3, v0}, Lcom/android/exchange/adapter/ComposeMailAdapter;->setHttpCode(I)V

    .line 364
    invoke-virtual {p3, v2}, Lcom/android/exchange/adapter/ComposeMailAdapter;->parse(Ljava/io/InputStream;)Z

    .line 365
    invoke-virtual {p3}, Lcom/android/exchange/adapter/ComposeMailAdapter;->getHttpCode()I

    move-result v0

    .line 366
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): getParseRespCode() status HTTP code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 378
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "len":I
    .end local v4    # "tmp_code":I
    :cond_0
    :goto_0
    return v0

    .line 369
    .restart local v4    # "tmp_code":I
    :catch_0
    move-exception v1

    .line 370
    .local v1, "e":Ljava/io/IOException;
    if-eqz v1, :cond_1

    .line 371
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 373
    :cond_1
    move v0, v4

    goto :goto_0
.end method

.method private getbMessageSent()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/android/exchange/EasOutboxService;->bMessageSent:Z

    return v0
.end method

.method private handleSendResultCode(IJLjava/lang/String;Z)I
    .locals 20
    .param p1, "code"    # I
    .param p2, "msgId"    # J
    .param p4, "subject"    # Ljava/lang/String;
    .param p5, "finaltry"    # Z

    .prologue
    .line 382
    const/4 v13, 0x0

    .line 383
    .local v13, "result":I
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/exchange/EasOutboxService;->setbMessageSent(Z)Z

    .line 384
    const/16 v15, 0xc8

    move/from16 v0, p1

    if-ne v0, v15, :cond_3

    .line 385
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sendmessage(): send complete. Deleting message..."

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 386
    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/android/exchange/EasOutboxService;->setbMessageSent(Z)Z

    .line 389
    :try_start_0
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v15, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v6

    .line 390
    .local v6, "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    move-object v4, v6

    .local v4, "arr$":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    array-length v12, v4

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_2

    aget-object v5, v4, v11

    .line 392
    .local v5, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    iget-object v15, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    if-nez v15, :cond_1

    .line 390
    :cond_0
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 394
    :cond_1
    iget-object v15, v5, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 396
    .local v14, "uri":Landroid/net/Uri;
    if-eqz v14, :cond_0

    const-string v15, "file"

    invoke-virtual {v14}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 397
    invoke-virtual {v14}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    .line 399
    .local v10, "filePath":Ljava/lang/String;
    if-eqz v10, :cond_0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/.EmailTempImage"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_0

    const-string v15, "signature_handWriting_"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    const-string v15, "/TempSignature/"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 402
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 404
    .local v8, "delFile":Ljava/io/File;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 408
    .end local v4    # "arr$":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v5    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v6    # "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v8    # "delFile":Ljava/io/File;
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v14    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 409
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 412
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_2
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v15}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    .line 413
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v15, v0, v1}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMessageBodyFilesUri(Landroid/content/Context;J)V

    .line 414
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/EasOutboxService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v16, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 416
    const/4 v13, 0x0

    .line 480
    :goto_2
    return v13

    .line 417
    :cond_3
    const/16 v15, 0x1f7

    move/from16 v0, p1

    if-ne v0, v15, :cond_5

    .line 418
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "sendMessage(): Send Failed.. provision error. code : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 419
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sendMessage(): This means HTTP_NEED_RETRYLATER...."

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 421
    if-eqz p5, :cond_4

    .line 427
    :cond_4
    const/4 v13, 0x0

    .line 428
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    goto :goto_2

    .line 429
    :cond_5
    const/16 v15, 0x1fb

    move/from16 v0, p1

    if-ne v0, v15, :cond_6

    .line 430
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "sendMessage(): Send Failed.. provision error. code : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 431
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sendMessage(): This means HTTP_MAILBOXQUOTA_EXCEEDED...."

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 433
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sendMessage(): it\'s final try. SEND_FAILED_MAILBOXQUOTA_EXCEEDED flag is setted"

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 434
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 435
    .local v7, "cv":Landroid/content/ContentValues;
    const-string v15, "syncServerId"

    const/16 v16, -0x5

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 436
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v16, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-wide/from16 v1, p2

    invoke-static {v15, v0, v1, v2, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I

    .line 438
    const/4 v13, 0x0

    .line 439
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    goto/16 :goto_2

    .line 443
    .end local v7    # "cv":Landroid/content/ContentValues;
    :cond_6
    const/16 v15, 0x1c1

    move/from16 v0, p1

    if-ne v0, v15, :cond_7

    .line 444
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "sendMessage(): Send Failed.. provision error. code : "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 450
    const/4 v13, 0x4

    .line 451
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    goto/16 :goto_2

    .line 456
    :cond_7
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "sendMessage(): Send Failed.. SEND_FAILED_SERVER_ERROR flag is setted"

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 459
    invoke-virtual/range {p0 .. p1}, Lcom/android/exchange/EasOutboxService;->isIrmError(I)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_8

    .line 460
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "sendMessage(): IrmError founded. code = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 472
    :cond_8
    invoke-static/range {p1 .. p1}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 473
    const/16 v13, 0x16

    .line 477
    :goto_3
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    goto/16 :goto_2

    .line 475
    :cond_9
    const/4 v13, 0x0

    goto :goto_3
.end method

.method private handleSendResultIOException(J)Z
    .locals 9
    .param p1, "msgId"    # J

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 484
    const/4 v2, 0x1

    .line 485
    .local v2, "isNetworkEnable":Z
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_2

    .line 488
    :try_start_0
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 490
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    .line 491
    .local v3, "mNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    .line 492
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendmessage() : ActiveNetworkInfo:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", State:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/NetworkInfo$State;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", DetailedState:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/NetworkInfo$DetailedState;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v5, v6, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v5

    sget-object v6, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, v6, :cond_1

    .line 501
    const/4 v2, 0x1

    .line 513
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "mNetworkInfo":Landroid/net/NetworkInfo;
    :goto_0
    if-nez v2, :cond_3

    .line 514
    const-string v5, "[Eas]OutboxService"

    const-string v6, "sendmessage() : isNetworkEnable is false. stop sending. try later."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-direct {p0, p1, p2, v8, v4}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    .line 537
    :goto_1
    return v4

    .line 503
    .restart local v0    # "cm":Landroid/net/ConnectivityManager;
    .restart local v3    # "mNetworkInfo":Landroid/net/NetworkInfo;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 505
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v3    # "mNetworkInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 506
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 507
    const/4 v2, 0x0

    .line 508
    goto :goto_0

    .line 510
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_2
    const-string v5, "[Eas]OutboxService"

    const-string v6, "sendmessage() : mContext is null. abnormal. cannot check network info."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 521
    :cond_3
    const-string v5, "[Eas]OutboxService"

    const-string v6, "sendmessage() : isNetworkEnable is true. keep trying next message if exist."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    :try_start_1
    invoke-virtual {p0}, Lcom/android/exchange/EasOutboxService;->isCBA()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {}, Lcom/android/exchange/ExchangeService;->shutdownConnectionManager()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 535
    :goto_2
    invoke-direct {p0, p1, p2, v8, v4}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    .line 537
    const/4 v4, 0x1

    goto :goto_1

    .line 526
    :cond_4
    :try_start_2
    iget-object v5, p0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {p0, v6, v7}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 527
    :catch_1
    move-exception v1

    .line 528
    .restart local v1    # "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private isMessageInOutbox(J)Z
    .locals 5
    .param p1, "messageId"    # J

    .prologue
    .line 1314
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v0

    .line 1315
    .local v0, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxType:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 1317
    .local v1, "result":Z
    :goto_0
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EasOutboxService() : Cancel Sending isMessageInOutbox "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    return v1

    .line 1315
    .end local v1    # "result":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private sendCallback(JLjava/lang/String;I)V
    .locals 9
    .param p1, "msgId"    # J
    .param p3, "subject"    # Ljava/lang/String;
    .param p4, "status"    # I

    .prologue
    .line 167
    const-wide/16 v2, -0x1

    .line 168
    .local v2, "accountId":J
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v1, :cond_2

    .line 169
    iget-object v1, p0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    .line 173
    :cond_0
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 174
    const-string v1, "[Eas]OutboxService"

    const-string v4, "EasOutboxService() : sendCallback. accountId = -1"

    invoke-static {v1, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_1
    const-string v1, "[Eas]OutboxService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : sendCallback. msgId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v1, "[Eas]OutboxService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : sendCallback. status = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v1

    const/4 v8, 0x0

    move-wide v4, p1

    move-object v6, p3

    move v7, p4

    invoke-interface/range {v1 .. v8}, Lcom/android/emailcommon/service/IEmailServiceCallback;->sendMessageStatus(JJLjava/lang/String;II)V

    .line 182
    :goto_1
    return-void

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static sendMessage(Landroid/content/Context;JLcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    const/4 v8, 0x4

    .line 1671
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 1672
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/4 v1, 0x0

    .line 1676
    .local v1, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/emailcommon/EasRefs;->getProtocolVersionDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_1

    .line 1677
    invoke-static {p0, p1, p2, v8}, Lcom/android/exchange/EasOutboxService;->findOrCreateMailboxOfOutBoxType(Landroid/content/Context;JI)J

    move-result-wide v2

    .line 1678
    .local v2, "outboxId":J
    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    .line 1682
    .end local v2    # "outboxId":J
    :goto_0
    if-eqz v1, :cond_0

    .line 1683
    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    iput-wide v4, p3, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 1684
    iput-wide p1, p3, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    .line 1685
    invoke-virtual {p3, p0}, Lcom/android/emailcommon/provider/EmailContent$Message;->save(Landroid/content/Context;)Landroid/net/Uri;

    .line 1687
    :cond_0
    return-void

    .line 1680
    :cond_1
    invoke-static {p0, p1, p2, v8}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v1

    goto :goto_0
.end method

.method private setbMessageSent(Z)Z
    .locals 1
    .param p1, "set"    # Z

    .prologue
    .line 157
    iput-boolean p1, p0, Lcom/android/exchange/EasOutboxService;->bMessageSent:Z

    .line 158
    iget-boolean v0, p0, Lcom/android/exchange/EasOutboxService;->bMessageSent:Z

    return v0
.end method

.method private updateEntityForForward(Landroid/content/Entity;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 12
    .param p1, "entity"    # Landroid/content/Entity;
    .param p2, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    .line 719
    invoke-virtual {p1}, Landroid/content/Entity;->getSubValues()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Entity$NamedContentValues;

    .line 720
    .local v8, "ncv":Landroid/content/Entity$NamedContentValues;
    iget-object v9, v8, Landroid/content/Entity$NamedContentValues;->uri:Landroid/net/Uri;

    sget-object v10, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 721
    iget-object v9, v8, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    const-string v10, "attendeeEmail"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iget-object v10, p0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v10, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 722
    iget-object v9, v8, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    const-string v10, "attendeeRelationship"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 723
    iget-object v9, v8, Landroid/content/Entity$NamedContentValues;->values:Landroid/content/ContentValues;

    const-string v10, "attendeeRelationship"

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    .line 729
    .end local v8    # "ncv":Landroid/content/Entity$NamedContentValues;
    :cond_1
    iget-object v9, p2, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {v9}, Lcom/android/emailcommon/mail/Address;->unpack(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v1

    .line 730
    .local v1, "addr":[Lcom/android/emailcommon/mail/Address;
    move-object v3, v1

    .local v3, "arr$":[Lcom/android/emailcommon/mail/Address;
    array-length v6, v3

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_2

    aget-object v0, v3, v5

    .line 731
    .local v0, "add":Lcom/android/emailcommon/mail/Address;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 732
    .local v4, "attendeeValues":Landroid/content/ContentValues;
    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 733
    .local v2, "address":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v7

    .line 734
    .local v7, "name":Ljava/lang/String;
    const-string v9, "attendeeRelationship"

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 735
    const-string v9, "attendeeEmail"

    invoke-virtual {v4, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v9, "attendeeName"

    invoke-virtual {v4, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const-string v9, "attendeeStatus"

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 738
    sget-object v9, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v9, v4}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 730
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 740
    .end local v0    # "add":Lcom/android/emailcommon/mail/Address;
    .end local v2    # "address":Ljava/lang/String;
    .end local v4    # "attendeeValues":Landroid/content/ContentValues;
    .end local v7    # "name":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private updateNextSendTimestamp(JIJ)V
    .locals 6
    .param p1, "msgId"    # J
    .param p3, "retrytimes"    # I
    .param p4, "mimeSize"    # J

    .prologue
    .line 1252
    const/high16 v2, 0xa00000

    .line 1254
    .local v2, "maxAttachmentSize":I
    :try_start_0
    iget-object v3, p0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-static {v3}, Lcom/android/emailcommon/utility/Utility;->isHotmailAccount(Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1255
    mul-int/lit8 v2, v2, 0x3

    .line 1258
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1259
    .local v0, "cv":Landroid/content/ContentValues;
    int-to-long v4, v2

    cmp-long v3, p4, v4

    if-lez v3, :cond_1

    .line 1260
    const-string v3, "syncServerId"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1263
    :cond_1
    const-string v3, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {p3, v4, v5}, Lcom/android/emailcommon/utility/Utility;->nextTimeAfterNextSendDuration(IJ)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1266
    sget-object v3, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3, v4, p1, p2, v0}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1270
    .end local v0    # "cv":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 1267
    :catch_0
    move-exception v1

    .line 1268
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private writeInteger(Ljava/io/RandomAccessFile;I)I
    .locals 7
    .param p1, "out"    # Ljava/io/RandomAccessFile;
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1713
    const/4 v4, 0x5

    new-array v0, v4, [B

    .line 1714
    .local v0, "buf":[B
    const/4 v2, 0x0

    .line 1715
    .local v2, "idx":I
    const/4 v1, 0x0

    .line 1718
    .local v1, "cnt":I
    :goto_0
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "idx":I
    .local v3, "idx":I
    and-int/lit8 v4, p2, 0x7f

    int-to-byte v4, v4

    aput-byte v4, v0, v2

    .line 1719
    shr-int/lit8 p2, p2, 0x7

    .line 1720
    if-nez p2, :cond_1

    move v2, v3

    .line 1722
    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    :goto_1
    const/4 v4, 0x1

    if-le v2, v4, :cond_0

    .line 1723
    add-int/lit8 v2, v2, -0x1

    aget-byte v4, v0, v2

    or-int/lit16 v4, v4, 0x80

    invoke-virtual {p1, v4}, Ljava/io/RandomAccessFile;->write(I)V

    .line 1724
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1726
    :cond_0
    const/4 v4, 0x0

    aget-byte v4, v0, v4

    invoke-virtual {p1, v4}, Ljava/io/RandomAccessFile;->write(I)V

    .line 1727
    add-int/lit8 v1, v1, 0x1

    .line 1729
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writeInteger : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1730
    const-string v4, "TAG"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732
    return v1

    .end local v2    # "idx":I
    .restart local v3    # "idx":I
    :cond_1
    move v2, v3

    .end local v3    # "idx":I
    .restart local v2    # "idx":I
    goto :goto_0
.end method


# virtual methods
.method generateSmartSendCmd(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "reply"    # Z
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "collectionId"    # Ljava/lang/String;
    .param p4, "calRecEventInstanceId"    # Ljava/lang/String;

    .prologue
    .line 326
    if-eqz p4, :cond_1

    .line 327
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    const-string v0, "SmartReply"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&ItemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {p2, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&CollectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {p3, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&InstanceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {p4, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 332
    :goto_1
    return-object v0

    .line 327
    :cond_0
    const-string v0, "SmartForward"

    goto :goto_0

    .line 332
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_2

    const-string v0, "SmartReply"

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&ItemId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {p2, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&CollectionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-static {p3, v1}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v0, "SmartForward"

    goto :goto_2
.end method

.method getNumbers(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p1, "to"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v6, "toNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "numberTo":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 191
    .local v3, "number":Ljava/lang/String;
    const-string v7, "MOBILE:"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "numberParts":[Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v3, v4, v7

    .line 193
    const-string v7, "]"

    const-string v8, " "

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 194
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 195
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    .end local v3    # "number":Ljava/lang/String;
    .end local v4    # "numberParts":[Ljava/lang/String;
    :cond_0
    return-object v6
.end method

.method relaySms(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 12
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    const/4 v2, 0x0

    .line 202
    const-string v4, "[Eas]OutboxService"

    const-string v5, "sendSMS(): relaySms() called"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    sget-object v4, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    iget-wide v10, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v4, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyWithMessageId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Body;

    move-result-object v7

    .line 205
    .local v7, "restoreBody":Lcom/android/emailcommon/provider/EmailContent$Body;
    if-eqz v7, :cond_0

    .line 206
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    move-result-object v0

    .line 207
    .local v0, "sm":Landroid/telephony/SmsManager;
    iget-object v4, p1, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/android/exchange/EasOutboxService;->getNumbers(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 208
    .local v8, "toNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 209
    iget-object v4, v7, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 210
    .local v3, "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .local v1, "number":Ljava/lang/String;
    move-object v4, v2

    move-object v5, v2

    .line 211
    invoke-virtual/range {v0 .. v5}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 212
    const-string v4, "[Eas]OutboxService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendSMS(): Sending SMS to: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 217
    .end local v0    # "sm":Landroid/telephony/SmsManager;
    .end local v1    # "number":Ljava/lang/String;
    .end local v3    # "parts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "toNumbers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method public run()V
    .locals 33

    .prologue
    .line 1325
    const-string v2, "[Eas]OutboxService"

    const-string v3, "start EasOutboxService run() ========================="

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1326
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->setupService()Z

    .line 1327
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v15

    .line 1328
    .local v15, "cacheDir":Ljava/io/File;
    const-wide/16 v28, -0x1

    .line 1330
    .local v28, "msgId":J
    :try_start_0
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/exchange/EasOutboxService;->mDeviceId:Ljava/lang/String;

    .line 1331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mDeviceId:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 1332
    const-string v2, "EAS"

    const-string v3, "mDeviceId is null."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    :cond_0
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "messageType"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "retrySendTimes"

    aput-object v3, v4, v2

    .line 1339
    .local v4, "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    const/4 v14, 0x0

    .line 1340
    .local v14, "c":Landroid/database/Cursor;
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "mailboxKey=? and (syncServerId is null or (syncServerId!=-1 and syncServerId!=-6 and syncServerId!=-2 and syncServerId!=-5 and syncServerId!=-3))"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    const-string v7, "retrySendTimes ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 1346
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-static {v2, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v13

    .line 1350
    .local v13, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    const/16 v22, 0x0

    .line 1352
    .local v22, "isSendingMessage":Z
    const-wide/16 v18, -0x1

    .line 1353
    .local v18, "cancelSendingMessageId":J
    iget-wide v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mCancelSendingMessageTimeout:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    .line 1354
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->cancelMessageSeletion()Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_18
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_17
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v17

    .line 1356
    .local v17, "cancelSendingMessageSelection":Ljava/lang/String;
    const/16 v16, 0x0

    .line 1358
    .local v16, "cancelSendingMessageCursor":Landroid/database/Cursor;
    :try_start_1
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v7, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "messageType"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "flags"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " & "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x1f0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") = 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "meetingInfo"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IS NULL"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v2

    const-string v10, "_id DESC"

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1379
    if-eqz v16, :cond_1

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1381
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v18

    .line 1389
    :cond_1
    if-eqz v16, :cond_2

    :try_start_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_13
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_18
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1396
    .end local v16    # "cancelSendingMessageCursor":Landroid/database/Cursor;
    .end local v17    # "cancelSendingMessageSelection":Ljava/lang/String;
    :cond_2
    :goto_0
    const/4 v12, -0x1

    .line 1397
    .local v12, "MaxRetrySendTimesSetting":I
    if-eqz v14, :cond_20

    move-wide/from16 v8, v28

    .line 1399
    .end local v28    # "msgId":J
    .local v8, "msgId":J
    :cond_3
    :goto_1
    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1400
    const/4 v2, 0x0

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v8

    .line 1401
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-eqz v2, :cond_3

    .line 1403
    const/16 v26, 0x0

    .line 1405
    .local v26, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :try_start_4
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v2, v8, v9}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v26

    .line 1406
    if-eqz v26, :cond_10

    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-eq v2, v3, :cond_10

    .line 1419
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit16 v2, v2, 0x1f0

    if-nez v2, :cond_a

    cmp-long v2, v18, v8

    if-nez v2, :cond_4

    move-object/from16 v0, v26

    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mCancelSendingMessageTimeout:J

    add-long/2addr v2, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v2, v2, v6

    if-gtz v2, :cond_5

    :cond_4
    iget-wide v2, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mCancelSendingMessageTimeout:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/android/exchange/EasOutboxService;->isMessageInOutbox(J)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1423
    :cond_5
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : Cancel sending message timeout not completed. Do not try to send this message now. msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 1517
    :catch_0
    move-exception v21

    .line 1518
    .local v21, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 1522
    .end local v21    # "e":Ljava/lang/Exception;
    :goto_2
    const/16 v27, 0x0

    .line 1523
    .local v27, "result":I
    if-eqz v26, :cond_15

    .line 1524
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 1525
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1528
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1530
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpl-double v2, v2, v6

    if-lez v2, :cond_13

    .line 1531
    const/4 v2, 0x1

    invoke-interface {v14, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 1532
    .local v24, "messageType":J
    const-wide/16 v2, 0x100

    and-long v2, v2, v24

    const-wide/16 v6, 0x100

    cmp-long v2, v2, v6

    if-nez v2, :cond_11

    .line 1533
    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Lcom/android/exchange/EasOutboxService;->sendSMS(J)I

    move-result v27

    .line 1554
    .end local v24    # "messageType":J
    :goto_3
    const/16 v2, 0x16

    move/from16 v0, v27

    if-ne v0, v2, :cond_16

    .line 1555
    const-string v2, "[Eas]OutboxService"

    const-string v3, "stop sending other message in outbox. LOGIN_FAILED"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1556
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    .line 1557
    sget-object v2, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "info=LOGIN_FAILED for msgId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mAccount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1572
    :try_start_6
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1573
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_d
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 1638
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8

    .line 1643
    :goto_4
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_7
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v12    # "MaxRetrySendTimesSetting":I
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    .end local v26    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v27    # "result":I
    :goto_5
    return-void

    .line 1384
    .end local v8    # "msgId":J
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v16    # "cancelSendingMessageCursor":Landroid/database/Cursor;
    .restart local v17    # "cancelSendingMessageSelection":Ljava/lang/String;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v28    # "msgId":J
    :catch_1
    move-exception v21

    .line 1386
    .restart local v21    # "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1389
    if-eqz v16, :cond_2

    :try_start_9
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_18
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto/16 :goto_0

    .line 1390
    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 1388
    .end local v21    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    .line 1389
    if-eqz v16, :cond_8

    :try_start_a
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_14
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_18
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 1390
    :cond_8
    :goto_6
    :try_start_b
    throw v2
    :try_end_b
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_18
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_17
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1613
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v16    # "cancelSendingMessageCursor":Landroid/database/Cursor;
    .end local v17    # "cancelSendingMessageSelection":Ljava/lang/String;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    :catch_3
    move-exception v21

    move-wide/from16 v8, v28

    .line 1614
    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    .local v21, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :goto_7
    :try_start_c
    const-string v2, "[Eas]OutboxService"

    const-string v3, "stop sending other message in outbox. DEVICE_BLOCKED"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    const/16 v27, 0x3

    .line 1620
    .restart local v27    # "result":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v5, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v2, v3, v5}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    .line 1622
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-direct {v0, v8, v9, v2, v1}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 1638
    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_f

    .line 1643
    .end local v21    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :goto_8
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_9
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1428
    .end local v27    # "result":I
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v12    # "MaxRetrySendTimesSetting":I
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v26    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_a
    :try_start_e
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : Cancel sending message timeout completed. Try to send this message now. msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", account.mCancelSendingMessageTimeout = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v6, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mCancelSendingMessageTimeout:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1434
    const/16 v22, 0x1

    .line 1436
    if-gez v12, :cond_b

    .line 1447
    :try_start_f
    iget v12, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mAutoRetryTimes:I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 1454
    :cond_b
    :goto_9
    :try_start_10
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    if-le v2, v12, :cond_e

    if-lez v12, :cond_e

    .line 1456
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : Max Auto resend times exceeded. Do not try to send this message now. msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", msg.mRetrySendTimes = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", MaxRetrySendTimesSetting = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1463
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : set msg.mServerId as SEND_FAILED_NETWORK_ERROR. msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 1467
    const/16 v32, 0x0

    .line 1469
    .local v32, "serverId":I
    :try_start_11
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v2, :cond_c

    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_c

    .line 1470
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_7
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-result v32

    .line 1477
    :cond_c
    :goto_a
    if-ltz v32, :cond_d

    .line 1478
    :try_start_12
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 1479
    .local v20, "cv":Landroid/content/ContentValues;
    const-string v2, "syncServerId"

    const/4 v3, -0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1480
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v2, v3, v8, v9, v0}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I

    .line 1483
    .end local v20    # "cv":Landroid/content/ContentValues;
    :cond_d
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v2, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    .line 1485
    sget-object v2, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mAccount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "info=Max Auto resend times exceeded for msgId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "serverId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v32

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " accId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mRetrySendTimes="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mAutoRetryTimes="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 1490
    :try_start_13
    new-instance v5, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v2}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v10, 0x1

    move-object/from16 v0, v26

    iget-object v11, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSubject:Ljava/lang/String;

    invoke-virtual/range {v5 .. v11}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_4
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/16 :goto_1

    .line 1492
    :catch_4
    move-exception v21

    .line 1493
    .local v21, "e":Landroid/os/RemoteException;
    :try_start_14
    invoke-virtual/range {v21 .. v21}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto/16 :goto_1

    .line 1572
    .end local v21    # "e":Landroid/os/RemoteException;
    .end local v26    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v32    # "serverId":I
    :catchall_1
    move-exception v2

    :try_start_15
    sget-object v3, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1573
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_15
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_15 .. :try_end_15} :catch_5
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_b
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_d
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 1613
    :catch_5
    move-exception v21

    goto/16 :goto_7

    .line 1449
    .restart local v26    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catch_6
    move-exception v21

    .line 1450
    .local v21, "e":Ljava/lang/Exception;
    :try_start_16
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_9

    .line 1472
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v32    # "serverId":I
    :catch_7
    move-exception v21

    .line 1473
    .restart local v21    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 1474
    const/16 v32, 0x0

    goto/16 :goto_a

    .line 1498
    .end local v21    # "e":Ljava/lang/Exception;
    .end local v32    # "serverId":I
    :cond_e
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move-object/from16 v0, v26

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v2, v6, v7, v10, v11}, Lcom/android/emailcommon/utility/Utility;->remainedTimeUntilNextSendDuration(IJJ)J

    move-result-wide v30

    .line 1499
    .local v30, "remainedTime":J
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    if-lez v2, :cond_f

    const-wide/16 v2, 0x0

    cmp-long v2, v30, v2

    if-lez v2, :cond_f

    .line 1500
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1501
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "send try later. remainedTime : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v30

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    sget-object v2, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mAccount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mMailbox="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "msgId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " accId="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " info=try sending later. remainedTime="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v30

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1504
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9, v2, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    goto/16 :goto_1

    .line 1508
    :cond_f
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, v26

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    .line 1509
    new-instance v20, Landroid/content/ContentValues;

    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    .line 1510
    .restart local v20    # "cv":Landroid/content/ContentValues;
    const-string v2, "retrySendTimes"

    move-object/from16 v0, v26

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1511
    const-string v2, "timeStamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1512
    const-string v2, "syncServerId"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1513
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v2, v3, v8, v9, v0}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I

    goto/16 :goto_2

    .line 1515
    .end local v20    # "cv":Landroid/content/ContentValues;
    .end local v30    # "remainedTime":J
    :cond_10
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : msg is null or SMS type msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_0
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto/16 :goto_2

    .line 1535
    .restart local v24    # "messageType":J
    .restart local v27    # "result":I
    :cond_11
    :try_start_17
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    if-le v2, v12, :cond_12

    if-lez v12, :cond_12

    const/4 v2, 0x1

    :goto_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v8, v9, v2}, Lcom/android/exchange/EasOutboxService;->sendMessage(Ljava/io/File;JZ)I

    move-result v27

    goto/16 :goto_3

    :cond_12
    const/4 v2, 0x0

    goto :goto_b

    .line 1541
    .end local v24    # "messageType":J
    :cond_13
    move-object/from16 v0, v26

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    if-le v2, v12, :cond_14

    if-lez v12, :cond_14

    const/4 v2, 0x1

    :goto_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v8, v9, v2}, Lcom/android/exchange/EasOutboxService;->sendMessage(Ljava/io/File;JZ)I

    move-result v27

    goto/16 :goto_3

    :cond_14
    const/4 v2, 0x0

    goto :goto_c

    .line 1548
    :cond_15
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "EasOutboxService() : msg is null. msgId = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    goto/16 :goto_3

    .line 1639
    :catch_8
    move-exception v21

    .line 1640
    .restart local v21    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 1562
    .end local v21    # "e":Ljava/lang/Exception;
    :cond_16
    const/4 v2, 0x4

    move/from16 v0, v27

    if-ne v0, v2, :cond_3

    .line 1563
    :try_start_18
    const-string v2, "[Eas]OutboxService"

    const-string v3, "stop sending other message in outbox. EXIT_SECURITY_FAILURE"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .line 1572
    :try_start_19
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1573
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_19
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_19 .. :try_end_19} :catch_5
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_b
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    .line 1638
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_9

    .line 1643
    :goto_d
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_17

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_17
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1639
    :catch_9
    move-exception v21

    .line 1640
    .restart local v21    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d

    .line 1572
    .end local v21    # "e":Ljava/lang/Exception;
    .end local v26    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v27    # "result":I
    :cond_18
    :try_start_1b
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1573
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 1576
    :goto_e
    sget-object v2, Lcom/android/exchange/EasOutboxService;->seletedMsgIdList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1577
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I
    :try_end_1b
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1b .. :try_end_1b} :catch_5
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_d
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    .line 1581
    const/16 v23, 0x0

    .line 1583
    .local v23, "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_start_1c
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    const/4 v3, 0x5

    invoke-static {v2, v6, v7, v3}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_a
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1c .. :try_end_1c} :catch_5
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_b
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    move-result-object v23

    .line 1588
    :goto_f
    if-eqz v23, :cond_19

    .line 1590
    const-wide/16 v2, 0x3e8

    :try_start_1d
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1d
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_1d} :catch_15
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_16
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1d .. :try_end_1d} :catch_5
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_b
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    .line 1599
    :goto_10
    if-eqz v22, :cond_19

    .line 1600
    :try_start_1e
    invoke-static {}, Lcom/android/exchange/ExchangeService;->checkExchangeServiceServiceRunning()V

    .line 1601
    move-object/from16 v0, v23

    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-static {v2, v3, v5, v6}, Lcom/android/exchange/ExchangeService;->startManualSync(JILcom/android/exchange/Request;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_c
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1e .. :try_end_1e} :catch_5
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_b
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    .line 1638
    :cond_19
    :goto_11
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_e

    .line 1643
    :goto_12
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1a

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_1a
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1585
    :catch_a
    move-exception v21

    .line 1586
    .restart local v21    # "e":Ljava/lang/Exception;
    :try_start_20
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_20
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_20 .. :try_end_20} :catch_5
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_b
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_d
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    goto :goto_f

    .line 1624
    .end local v21    # "e":Ljava/lang/Exception;
    .end local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_b
    move-exception v21

    .line 1625
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v12    # "MaxRetrySendTimesSetting":I
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    .local v21, "e":Ljava/io/IOException;
    :goto_13
    :try_start_21
    const-string v2, "[Eas]OutboxService"

    const-string v3, "stop sending other message in outbox. IOException"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    if-eqz v21, :cond_1b

    .line 1627
    invoke-virtual/range {v21 .. v21}, Ljava/io/IOException;->printStackTrace()V

    .line 1629
    :cond_1b
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    .line 1638
    :try_start_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_10

    .line 1643
    .end local v21    # "e":Ljava/io/IOException;
    :goto_14
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1c

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_1c
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1606
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v12    # "MaxRetrySendTimesSetting":I
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_c
    move-exception v21

    .line 1607
    .local v21, "e":Ljava/lang/Exception;
    :try_start_23
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_23
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_23 .. :try_end_23} :catch_5
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_23} :catch_b
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_d
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    goto/16 :goto_11

    .line 1630
    .end local v21    # "e":Ljava/lang/Exception;
    .end local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_d
    move-exception v21

    .line 1631
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v12    # "MaxRetrySendTimesSetting":I
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    .restart local v21    # "e":Ljava/lang/Exception;
    :goto_15
    :try_start_24
    const-string v2, "[Eas]OutboxService"

    const-string v3, "stop sending other message in outbox. Exception"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1632
    if-eqz v21, :cond_1d

    .line 1633
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    .line 1635
    :cond_1d
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    .line 1638
    :try_start_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_11

    .line 1643
    :goto_16
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1e

    .line 1648
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "[Eas]OutboxService"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v5, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v2, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v3, 0x1

    invoke-static {v2, v6, v7, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_1e
    const-string v2, "[Eas]OutboxService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "end EasOutboxService : exit status = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1639
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v12    # "MaxRetrySendTimesSetting":I
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_e
    move-exception v21

    .line 1640
    .restart local v21    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_12

    .line 1639
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v12    # "MaxRetrySendTimesSetting":I
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    .end local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .local v21, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v27    # "result":I
    :catch_f
    move-exception v21

    .line 1640
    .local v21, "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_8

    .line 1639
    .end local v27    # "result":I
    .local v21, "e":Ljava/io/IOException;
    :catch_10
    move-exception v21

    .line 1640
    .local v21, "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_14

    .line 1639
    :catch_11
    move-exception v21

    .line 1640
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_16

    .line 1637
    .end local v8    # "msgId":J
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v28    # "msgId":J
    :catchall_2
    move-exception v2

    move-wide/from16 v8, v28

    .line 1638
    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    :goto_17
    :try_start_26
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->shutdownConnectionManagerForSendMessage(J)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_12

    .line 1643
    :goto_18
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 1647
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    const/4 v5, 0x4

    if-ne v3, v5, :cond_1f

    .line 1648
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "[Eas]OutboxService"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "EasOutboxService : call SyncManager.reloadFolderList()"

    aput-object v6, v3, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1651
    sget-object v3, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v5, 0x1

    invoke-static {v3, v6, v7, v5}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1659
    :cond_1f
    const-string v3, "[Eas]OutboxService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end EasOutboxService : exit status = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasOutboxService;->mExitStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 1639
    :catch_12
    move-exception v21

    .line 1640
    .restart local v21    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_18

    .line 1390
    .end local v8    # "msgId":J
    .end local v21    # "e":Ljava/lang/Exception;
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v16    # "cancelSendingMessageCursor":Landroid/database/Cursor;
    .restart local v17    # "cancelSendingMessageSelection":Ljava/lang/String;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v28    # "msgId":J
    :catch_13
    move-exception v2

    goto/16 :goto_0

    :catch_14
    move-exception v3

    goto/16 :goto_6

    .line 1591
    .end local v16    # "cancelSendingMessageCursor":Landroid/database/Cursor;
    .end local v17    # "cancelSendingMessageSelection":Ljava/lang/String;
    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    .restart local v12    # "MaxRetrySendTimesSetting":I
    .restart local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catch_15
    move-exception v2

    goto/16 :goto_10

    .line 1592
    :catch_16
    move-exception v2

    goto/16 :goto_10

    .line 1637
    .end local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .end local v12    # "MaxRetrySendTimesSetting":I
    .end local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v18    # "cancelSendingMessageId":J
    .end local v22    # "isSendingMessage":Z
    .end local v23    # "mSentbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catchall_3
    move-exception v2

    goto :goto_17

    .line 1630
    .end local v8    # "msgId":J
    .restart local v28    # "msgId":J
    :catch_17
    move-exception v21

    move-wide/from16 v8, v28

    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    goto/16 :goto_15

    .line 1624
    .end local v8    # "msgId":J
    .restart local v28    # "msgId":J
    :catch_18
    move-exception v21

    move-wide/from16 v8, v28

    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    goto/16 :goto_13

    .end local v8    # "msgId":J
    .restart local v4    # "MESSAGE_COLUMN_PROJECTION":[Ljava/lang/String;
    .restart local v12    # "MaxRetrySendTimesSetting":I
    .restart local v13    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v18    # "cancelSendingMessageId":J
    .restart local v22    # "isSendingMessage":Z
    .restart local v28    # "msgId":J
    :cond_20
    move-wide/from16 v8, v28

    .end local v28    # "msgId":J
    .restart local v8    # "msgId":J
    goto/16 :goto_e
.end method

.method sendMessage(Ljava/io/File;JZ)I
    .locals 70
    .param p1, "cacheDir"    # Ljava/io/File;
    .param p2, "msgId"    # J
    .param p4, "finaltry"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 862
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start sendMessage. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    const/16 v63, 0x0

    .line 864
    .local v63, "result":I
    const/16 v24, 0x0

    .line 865
    .local v24, "subject":Ljava/lang/String;
    const/4 v13, 0x0

    .line 866
    .local v13, "itemId":Ljava/lang/String;
    const/4 v14, 0x0

    .line 867
    .local v14, "collectionId":Ljava/lang/String;
    const/16 v19, 0x0

    .line 868
    .local v19, "calRecEventInstanceId":Ljava/lang/String;
    const/16 v17, 0x0

    .line 869
    .local v17, "reply":Z
    const/16 v43, 0x0

    .line 870
    .local v43, "forward":Z
    const/16 v16, 0x0

    .line 871
    .local v16, "smartSend":Z
    const-string v37, "SendMail"

    .line 873
    .local v37, "cmd":Ljava/lang/String;
    const-string v5, "eas_"

    const-string v6, "tmp"

    move-object/from16 v0, p1

    invoke-static {v5, v6, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v66

    .line 874
    .local v66, "tmpFile":Ljava/io/File;
    const-string v5, "eas_"

    const-string v6, "tmp"

    move-object/from16 v0, p1

    invoke-static {v5, v6, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v67

    .line 877
    .local v67, "tmpFile2":Ljava/io/File;
    const/16 v42, 0x0

    .line 878
    .local v42, "fileStream":Ljava/io/FileOutputStream;
    const/16 v49, 0x0

    .line 880
    .local v49, "inputStream":Ljava/io/FileInputStream;
    const/16 v60, 0x0

    .line 881
    .local v60, "randFile":Ljava/io/RandomAccessFile;
    const/16 v40, 0x0

    .line 882
    .local v40, "file":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 884
    .local v4, "bis":Ljava/io/BufferedInputStream;
    const-wide/16 v54, -0x1

    .line 885
    .local v54, "mimeSize":J
    const/16 v36, 0x0

    .line 887
    .local v36, "checkmessage":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V

    .line 888
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/exchange/EasOutboxService;->setbMessageSent(Z)Z

    .line 889
    const-wide/16 v32, -0x1

    .line 891
    .local v32, "accId":J
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v5, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v56

    .line 893
    .local v56, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v0, v1, v5}, Lcom/android/exchange/utility/CalendarUtilities;->isMeetingForwardWithInline(JLandroid/content/Context;)I

    move-result v52

    .line 894
    .local v52, "meetingFwd":I
    if-eqz v56, :cond_1

    .line 895
    const/4 v5, 0x1

    move/from16 v0, v52

    if-ne v0, v5, :cond_5

    .line 896
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/EasOutboxService;->createIcsNAttachForForward(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V

    .line 903
    :cond_0
    :goto_0
    move-object/from16 v0, v56

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v32, v0

    .line 909
    :cond_1
    :try_start_0
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-virtual {v6}, Lcom/android/emailcommon/provider/EmailContent$Account;->getEmailAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/cac/CACManager;->isCredentialAccount(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 910
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/emailcommon/cac/CACManager;->getCACState(Landroid/content/Context;)I

    move-result v5

    if-eqz v5, :cond_2

    .line 914
    :cond_2
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v5, v0, v1}, Lcom/android/exchange/EasOutboxService;->getOriginalMessageInfo(Landroid/content/Context;J)Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;

    move-result-object v47

    .line 915
    .local v47, "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    if-eqz v47, :cond_3

    .line 916
    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mSubject:Ljava/lang/String;

    move-object/from16 v24, v0

    .line 917
    move-object/from16 v0, v47

    iget-object v13, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mItemId:Ljava/lang/String;

    .line 918
    move-object/from16 v0, v47

    iget-object v14, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mCollectionId:Ljava/lang/String;

    .line 919
    move-object/from16 v0, v47

    iget-object v0, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mcalRecEventInstanceId:Ljava/lang/String;

    move-object/from16 v19, v0

    .line 920
    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mReply:Z

    move/from16 v17, v0

    .line 921
    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mForward:Z

    move/from16 v43, v0

    .line 922
    move-object/from16 v0, v47

    iget-boolean v0, v0, Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;->mSmartSend:Z

    move/from16 v16, v0

    .line 925
    :cond_3
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {v5, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v44

    .line 926
    .local v44, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    move-object/from16 v0, v44

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v45, v0

    .line 927
    .local v45, "host":Ljava/lang/String;
    const-string v5, "m.google.com"

    move-object/from16 v0, v45

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 929
    const/16 v16, 0x0

    .line 933
    :cond_4
    new-instance v8, Ljava/io/FileOutputStream;

    move-object/from16 v0, v66

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_50
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_4b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_14
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1b
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_0 .. :try_end_0} :catch_22
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_29
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 935
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .local v8, "fileStream":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v5, v6, v7}, Lcom/android/exchange/SecurityPolicyDelegate;->getAccountPolicy(Landroid/content/Context;J)Lcom/android/emailcommon/service/PolicySet;

    move-result-object v59

    .line 937
    .local v59, "ps":Lcom/android/emailcommon/service/PolicySet;
    const/4 v5, 0x1

    sget-object v6, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v59

    invoke-static {v5, v0, v6}, Lcom/android/emailcommon/internet/Rfc822Output;->setIsExchangeAccount(ZLcom/android/emailcommon/service/PolicySet;Ljava/lang/String;)V

    .line 938
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    if-nez v16, :cond_6

    const/4 v9, 0x1

    :goto_1
    const/4 v10, 0x1

    move-wide/from16 v6, p2

    invoke-static/range {v5 .. v10}, Lcom/android/emailcommon/internet/Rfc822Output;->writeTo(Landroid/content/Context;JLjava/io/OutputStream;ZZ)V

    .line 940
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "protocol version: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-wide/high16 v10, 0x402c000000000000L    # 14.0

    cmpl-double v5, v6, v10

    if-ltz v5, :cond_7

    const/16 v51, 0x1

    .line 943
    .local v51, "isEas14":Z
    :goto_2
    const/4 v9, 0x0

    .line 944
    .local v9, "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->length()J

    move-result-wide v54

    .line 945
    if-eqz v51, :cond_d

    .line 946
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V

    .line 947
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimesize: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v54

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 949
    new-instance v9, Lcom/android/exchange/adapter/ComposeMailAdapter;

    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    const/4 v15, 0x1

    if-nez v19, :cond_8

    const/16 v18, 0x0

    :goto_3
    move-object/from16 v11, p0

    invoke-direct/range {v9 .. v19}, Lcom/android/exchange/adapter/ComposeMailAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;)V

    .line 952
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    new-instance v64, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v64 .. v64}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 953
    .local v64, "serializer":Lcom/android/exchange/adapter/Serializer;
    move-object/from16 v0, v64

    invoke-virtual {v9, v0}, Lcom/android/exchange/adapter/ComposeMailAdapter;->sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z

    .line 954
    invoke-virtual/range {v64 .. v64}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 955
    new-instance v61, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    move-object/from16 v0, v61

    move-object/from16 v1, v67

    invoke-direct {v0, v1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_51
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_4c
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_46
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_41
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_1 .. :try_end_1} :catch_3b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_36
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 957
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .local v61, "randFile":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual/range {v64 .. v64}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v69

    .line 958
    .local v69, "wbxmlData":[B
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wbxml size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v69

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 959
    const/16 v46, 0x0

    .local v46, "i":I
    :goto_4
    move-object/from16 v0, v69

    array-length v5, v0

    add-int/lit8 v5, v5, -0x4

    move/from16 v0, v46

    if-ge v0, v5, :cond_9

    .line 960
    aget-byte v5, v69, v46

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Ljava/io/RandomAccessFile;->write(I)V
    :try_end_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_2 .. :try_end_2} :catch_52
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_4d
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_47
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_42
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_2 .. :try_end_2} :catch_3c
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_37
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 959
    add-int/lit8 v46, v46, 0x1

    goto :goto_4

    .line 898
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v46    # "i":I
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    :cond_5
    const/4 v5, 0x2

    move/from16 v0, v52

    if-ne v0, v5, :cond_0

    .line 899
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/EasOutboxService;->createIcsNAttachForForward(Lcom/android/emailcommon/provider/EmailContent$Message;Z)V

    goto/16 :goto_0

    .line 938
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 941
    :cond_7
    const/16 v51, 0x0

    goto/16 :goto_2

    .line 949
    .restart local v51    # "isEas14":Z
    :cond_8
    const/16 v18, 0x1

    goto :goto_3

    .line 963
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v46    # "i":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .restart local v69    # "wbxmlData":[B
    :cond_9
    const/16 v5, 0xc3

    :try_start_3
    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Ljava/io/RandomAccessFile;->write(I)V

    .line 964
    move-wide/from16 v0, v54

    long-to-int v5, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-direct {v0, v1, v5}, Lcom/android/exchange/EasOutboxService;->writeInteger(Ljava/io/RandomAccessFile;I)I

    move-result v58

    .line 967
    .local v58, "opbytes":I
    new-instance v41, Ljava/io/FileInputStream;

    move-object/from16 v0, v41

    move-object/from16 v1, v66

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_52
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_4d
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_47
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_42
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_3 .. :try_end_3} :catch_3c
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_37
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 970
    .end local v40    # "file":Ljava/io/FileInputStream;
    .local v41, "file":Ljava/io/FileInputStream;
    const/16 v53, 0x0

    .line 971
    .local v53, "mimeSize2":I
    if-eqz v41, :cond_60

    .line 972
    const/16 v46, 0x0

    .line 973
    const/16 v5, 0x2000

    :try_start_4
    new-array v0, v5, [B

    move-object/from16 v35, v0

    .line 974
    .local v35, "buf":[B
    new-instance v34, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v34

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_53
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_4 .. :try_end_4} :catch_4e
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_48
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_43
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_4 .. :try_end_4} :catch_3d
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_38
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 975
    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .local v34, "bis":Ljava/io/BufferedInputStream;
    :goto_5
    const/4 v5, 0x0

    const/16 v6, 0x2000

    :try_start_5
    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v5, v6}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v46

    const/4 v5, -0x1

    move/from16 v0, v46

    if-eq v0, v5, :cond_a

    .line 976
    const/4 v5, 0x0

    move-object/from16 v0, v61

    move-object/from16 v1, v35

    move/from16 v2, v46

    invoke-virtual {v0, v1, v5, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 977
    add-int v53, v53, v46

    goto :goto_5

    .line 979
    :cond_a
    invoke-virtual/range {v34 .. v34}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_54
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_4f
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_49
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_44
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_5 .. :try_end_5} :catch_3e
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_39
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 980
    const/4 v4, 0x0

    .line 981
    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    :try_start_6
    invoke-virtual/range {v41 .. v41}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_6 .. :try_end_6} :catch_53
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_6 .. :try_end_6} :catch_4e
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_48
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_43
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_6 .. :try_end_6} :catch_3d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_38
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 982
    const/16 v40, 0x0

    .line 984
    .end local v35    # "buf":[B
    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    :goto_6
    :try_start_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mimeSize2: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v53

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/exchange/adapter/ComposeMailAdapter;->log(Ljava/lang/String;)V

    .line 987
    move/from16 v0, v53

    int-to-long v6, v0

    cmp-long v5, v54, v6

    if-eqz v5, :cond_b

    .line 988
    const-string v5, "[Eas]OutboxService"

    const-string v6, "mimeSize is not equal to mimeSize2. Please check this out."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    :cond_b
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 992
    const/4 v8, 0x0

    .line 993
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 994
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 996
    :cond_c
    move-object/from16 v66, v67

    .line 999
    move-object/from16 v0, v69

    array-length v5, v0

    add-int/lit8 v5, v5, -0x4

    add-int/lit8 v5, v5, 0x1

    add-int v5, v5, v58

    int-to-long v6, v5

    add-long v6, v6, v54

    move-object/from16 v0, v61

    invoke-virtual {v0, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1002
    move-object/from16 v0, v69

    array-length v5, v0

    add-int/lit8 v5, v5, -0x2

    aget-byte v5, v69, v5

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Ljava/io/RandomAccessFile;->write(I)V

    .line 1003
    move-object/from16 v0, v69

    array-length v5, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, v69, v5

    move-object/from16 v0, v61

    invoke-virtual {v0, v5}, Ljava/io/RandomAccessFile;->write(I)V

    .line 1004
    invoke-virtual/range {v61 .. v61}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_52
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_7 .. :try_end_7} :catch_4d
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_47
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_42
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_7 .. :try_end_7} :catch_3c
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_37
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 1005
    const/16 v60, 0x0

    .line 1010
    .end local v46    # "i":I
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    :cond_d
    :try_start_8
    new-instance v50, Ljava/io/FileInputStream;

    move-object/from16 v0, v50

    move-object/from16 v1, v66

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_8 .. :try_end_8} :catch_51
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_8 .. :try_end_8} :catch_4c
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_46
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_41
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_8 .. :try_end_8} :catch_3b
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_36
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1011
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .local v50, "inputStream":Ljava/io/FileInputStream;
    :try_start_9
    new-instance v48, Lorg/apache/http/entity/InputStreamEntity;

    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-direct {v0, v1, v6, v7}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 1014
    .local v48, "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    if-eqz v16, :cond_e

    .line 1016
    if-eqz v51, :cond_18

    .line 1017
    if-eqz v17, :cond_17

    const-string v37, "SmartReply"

    .line 1024
    :cond_e
    :goto_7
    if-nez v51, :cond_f

    .line 1025
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v37

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "&SaveInSent=T"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    .line 1029
    :cond_f
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    invoke-static {v5, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v36

    .line 1030
    if-nez v36, :cond_19

    .line 1031
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendMessage(): message is not exist. sending is cancelled. msgId = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1033
    new-instance v5, Ljava/io/IOException;

    const-string v6, "message is not exist. sending cancelled"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_9
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_9 .. :try_end_9} :catch_45
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_9 .. :try_end_9} :catch_3f
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3a
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 1070
    .end local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    :catch_0
    move-exception v39

    move-object/from16 v49, v50

    .line 1071
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .local v39, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    :goto_8
    if-eqz v39, :cond_30

    .line 1072
    :try_start_a
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): Send Failed.. Caught DeviceAccessException: e.getMessage:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Lcom/android/emailcommon/utility/DeviceAccessException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    invoke-virtual/range {v39 .. v39}, Lcom/android/emailcommon/utility/DeviceAccessException;->printStackTrace()V

    .line 1077
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sendMessage() failed for msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Lcom/android/emailcommon/utility/DeviceAccessException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1082
    throw v39
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1186
    .end local v39    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catchall_0
    move-exception v5

    :goto_9
    const-string v6, "[Eas]OutboxService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendMessage(): finally. msgId = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_b
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v6, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v6}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v6

    if-nez v6, :cond_5f

    const/16 v30, 0x1

    :goto_a
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_30

    .line 1197
    :goto_b
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v6

    if-nez v6, :cond_10

    if-eqz v36, :cond_10

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_10
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_11
    if-eqz v8, :cond_12

    .line 1207
    :try_start_c
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_31

    .line 1212
    :cond_12
    :goto_c
    if-eqz v49, :cond_13

    .line 1215
    :try_start_d
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_32

    .line 1220
    :cond_13
    :goto_d
    if-eqz v60, :cond_14

    .line 1222
    :try_start_e
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_33

    .line 1228
    :cond_14
    :goto_e
    if-eqz v40, :cond_15

    .line 1230
    :try_start_f
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_34

    .line 1237
    :cond_15
    :goto_f
    if-eqz v4, :cond_16

    .line 1239
    :try_start_10
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_35

    .line 1245
    :cond_16
    :goto_10
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    throw v5

    .line 1017
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .restart local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_17
    :try_start_11
    const-string v37, "SmartForward"

    goto/16 :goto_7

    .line 1019
    :cond_18
    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v13, v14, v2}, Lcom/android/exchange/EasOutboxService;->generateSmartSendCmd(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    goto/16 :goto_7

    .line 1036
    :cond_19
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Send cmd: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasOutboxService;->userLog([Ljava/lang/String;)V

    .line 1037
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    .line 1039
    const v5, 0xdbba0

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v48

    invoke-virtual {v0, v1, v2, v5}, Lcom/android/exchange/EasOutboxService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;
    :try_end_11
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_11 .. :try_end_11} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_11 .. :try_end_11} :catch_45
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_11 .. :try_end_11} :catch_3f
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_3a
    .catchall {:try_start_11 .. :try_end_11} :catchall_6

    move-result-object v62

    .line 1041
    .local v62, "resp":Lcom/android/exchange/EasResponse;
    :try_start_12
    move-object/from16 v0, p0

    move-object/from16 v1, v62

    move/from16 v2, v51

    invoke-direct {v0, v1, v2, v9}, Lcom/android/exchange/EasOutboxService;->getParseRespCode(Lcom/android/exchange/EasResponse;ZLcom/android/exchange/adapter/ComposeMailAdapter;)I

    move-result v21

    .line 1042
    .local v21, "code":I
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " res="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1045
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): sendHttpClientPost HTTP response code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const/16 v5, 0x1f4

    move/from16 v0, v21

    if-eq v0, v5, :cond_1a

    const/16 v5, 0x96

    move/from16 v0, v21

    if-ne v0, v5, :cond_1d

    :cond_1a
    if-eqz v16, :cond_1d

    .line 1051
    const-string v5, "[Eas]OutboxService"

    const-string v6, "sendMessage(): Smartsend failed. Try SendMail later."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed for msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " res="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " info=Try SendMail later."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1057
    if-nez v51, :cond_1b

    move-object/from16 v0, v36

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    const/4 v6, 0x1

    if-gt v5, v6, :cond_1c

    :cond_1b
    const/16 v5, 0x96

    move/from16 v0, v21

    if-ne v0, v5, :cond_1d

    .line 1059
    :cond_1c
    new-instance v68, Landroid/content/ContentValues;

    invoke-direct/range {v68 .. v68}, Landroid/content/ContentValues;-><init>()V

    .line 1060
    .local v68, "values":Landroid/content/ContentValues;
    const-string v5, "sourceMessageKey"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v68

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1061
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p2

    move-object/from16 v2, v68

    invoke-static {v5, v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Body;->updateBodyWithMessageId(Landroid/content/Context;JLandroid/content/ContentValues;)V

    .end local v68    # "values":Landroid/content/ContentValues;
    :cond_1d
    move-object/from16 v20, p0

    move-wide/from16 v22, p2

    move/from16 v25, p4

    .line 1065
    invoke-direct/range {v20 .. v25}, Lcom/android/exchange/EasOutboxService;->handleSendResultCode(IJLjava/lang/String;Z)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-result v63

    .line 1067
    if-eqz v62, :cond_1e

    .line 1068
    :try_start_13
    invoke-virtual/range {v62 .. v62}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_13
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_13 .. :try_end_13} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_4a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_13 .. :try_end_13} :catch_45
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_13 .. :try_end_13} :catch_3f
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_3a
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 1186
    :cond_1e
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_14
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_2f

    const/16 v30, 0x1

    :goto_11
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_14} :catch_2

    .line 1197
    :goto_12
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_1f

    if-eqz v36, :cond_1f

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_1f
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_20

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_20
    if-eqz v8, :cond_21

    .line 1207
    :try_start_15
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3

    .line 1212
    :cond_21
    :goto_13
    if-eqz v50, :cond_22

    .line 1215
    :try_start_16
    invoke-virtual/range {v50 .. v50}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_4

    .line 1220
    :cond_22
    :goto_14
    if-eqz v60, :cond_23

    .line 1222
    :try_start_17
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_5

    .line 1228
    :cond_23
    :goto_15
    if-eqz v40, :cond_24

    .line 1230
    :try_start_18
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_6

    .line 1237
    :cond_24
    :goto_16
    if-eqz v4, :cond_25

    .line 1239
    :try_start_19
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_7

    .line 1245
    :cond_25
    :goto_17
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    move-object/from16 v49, v50

    .line 1247
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v21    # "code":I
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .end local v62    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    :goto_18
    return v63

    .line 1067
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .restart local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v62    # "resp":Lcom/android/exchange/EasResponse;
    :catchall_1
    move-exception v5

    if-eqz v62, :cond_26

    .line 1068
    :try_start_1a
    invoke-virtual/range {v62 .. v62}, Lcom/android/exchange/EasResponse;->close()V

    :cond_26
    throw v5
    :try_end_1a
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1a .. :try_end_1a} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1a .. :try_end_1a} :catch_1
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_4a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1a .. :try_end_1a} :catch_45
    .catch Lcom/android/emailcommon/cac/CACException; {:try_start_1a .. :try_end_1a} :catch_3f
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_3a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_6

    .line 1084
    .end local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .end local v62    # "resp":Lcom/android/exchange/EasResponse;
    :catch_1
    move-exception v39

    move-object/from16 v49, v50

    .line 1085
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .local v39, "e":Lorg/apache/http/client/ClientProtocolException;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    :goto_19
    if-eqz v39, :cond_27

    .line 1086
    :try_start_1b
    const-string v5, "[Eas]OutboxService"

    const-string v6, "sendMessage(): Send Failed.. org.apache.http.client.ClientProtocolException. SEND_FAILED_SERVER_ERROR flag is setted"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sendMessage() failed for msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Lorg/apache/http/client/ClientProtocolException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1093
    invoke-virtual/range {v39 .. v39}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 1104
    :cond_27
    const/16 v63, 0x0

    .line 1105
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    move/from16 v3, v63

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 1186
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_1c
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_39

    const/16 v30, 0x1

    :goto_1a
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_e

    .line 1197
    .end local v39    # "e":Lorg/apache/http/client/ClientProtocolException;
    :goto_1b
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_28

    if-eqz v36, :cond_28

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_28
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_29

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_29
    if-eqz v8, :cond_2a

    .line 1207
    :try_start_1d
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_f

    .line 1212
    :cond_2a
    :goto_1c
    if-eqz v49, :cond_2b

    .line 1215
    :try_start_1e
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_10

    .line 1220
    :cond_2b
    :goto_1d
    if-eqz v60, :cond_2c

    .line 1222
    :try_start_1f
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_11

    .line 1228
    :cond_2c
    :goto_1e
    if-eqz v40, :cond_2d

    .line 1230
    :try_start_20
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_12

    .line 1237
    :cond_2d
    :goto_1f
    if-eqz v4, :cond_2e

    .line 1239
    :try_start_21
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_13

    .line 1245
    :cond_2e
    :goto_20
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1189
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v21    # "code":I
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .restart local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v62    # "resp":Lcom/android/exchange/EasResponse;
    :cond_2f
    const/16 v30, 0x0

    goto/16 :goto_11

    .line 1191
    :catch_2
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_12

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_13

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_14

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_15

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_16

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_17

    .line 1186
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v21    # "code":I
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v48    # "inputEntity":Lorg/apache/http/entity/InputStreamEntity;
    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .end local v62    # "resp":Lcom/android/exchange/EasResponse;
    .local v39, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    :cond_30
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_22
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_38

    const/16 v30, 0x1

    :goto_21
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_22
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_22} :catch_8

    .line 1197
    .end local v39    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :goto_22
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_31

    if-eqz v36, :cond_31

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_31
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_32
    if-eqz v8, :cond_33

    .line 1207
    :try_start_23
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_9

    .line 1212
    :cond_33
    :goto_23
    if-eqz v49, :cond_34

    .line 1215
    :try_start_24
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_a

    .line 1220
    :cond_34
    :goto_24
    if-eqz v60, :cond_35

    .line 1222
    :try_start_25
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_b

    .line 1228
    :cond_35
    :goto_25
    if-eqz v40, :cond_36

    .line 1230
    :try_start_26
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_c

    .line 1237
    :cond_36
    :goto_26
    if-eqz v4, :cond_37

    .line 1239
    :try_start_27
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_d

    .line 1245
    :cond_37
    :goto_27
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1189
    .restart local v39    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :cond_38
    const/16 v30, 0x0

    goto :goto_21

    .line 1191
    :catch_8
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_22

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_9
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_23

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_a
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_b
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_25

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_26

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_27

    .line 1189
    .local v39, "e":Lorg/apache/http/client/ClientProtocolException;
    :cond_39
    const/16 v30, 0x0

    goto/16 :goto_1a

    .line 1191
    :catch_e
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1b

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_f
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1c

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_10
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1d

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_11
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1e

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_12
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1f

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_13
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_20

    .line 1106
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v39    # "e":Ljava/lang/Exception;
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_14
    move-exception v39

    move-object/from16 v8, v42

    .line 1107
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    .local v39, "e":Ljava/io/IOException;
    :goto_28
    if-eqz v39, :cond_3a

    .line 1108
    :try_start_28
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): Caught IOException: e.getMessage:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    invoke-virtual/range {v39 .. v39}, Ljava/io/IOException;->printStackTrace()V

    .line 1110
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " info=sendMessage() failed for msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1116
    const/16 v63, 0x0

    .line 1117
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasOutboxService;->handleSendResultIOException(J)Z

    move-result v5

    if-nez v5, :cond_3a

    .line 1118
    throw v39
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_0

    .line 1186
    :cond_3a
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_29
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_42

    const/16 v30, 0x1

    :goto_29
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_29
    .catch Landroid/os/RemoteException; {:try_start_29 .. :try_end_29} :catch_15

    .line 1197
    .end local v39    # "e":Ljava/io/IOException;
    :goto_2a
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_3b

    if-eqz v36, :cond_3b

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_3b
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3c

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_3c
    if-eqz v8, :cond_3d

    .line 1207
    :try_start_2a
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_16

    .line 1212
    :cond_3d
    :goto_2b
    if-eqz v49, :cond_3e

    .line 1215
    :try_start_2b
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_17

    .line 1220
    :cond_3e
    :goto_2c
    if-eqz v60, :cond_3f

    .line 1222
    :try_start_2c
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_18

    .line 1228
    :cond_3f
    :goto_2d
    if-eqz v40, :cond_40

    .line 1230
    :try_start_2d
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_19

    .line 1237
    :cond_40
    :goto_2e
    if-eqz v4, :cond_41

    .line 1239
    :try_start_2e
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_1a

    .line 1245
    :cond_41
    :goto_2f
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1189
    .restart local v39    # "e":Ljava/io/IOException;
    :cond_42
    const/16 v30, 0x0

    goto :goto_29

    .line 1191
    :catch_15
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2a

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_16
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2b

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_17
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2c

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_18
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2d

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_19
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2e

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_1a
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2f

    .line 1122
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v39    # "e":Ljava/lang/Exception;
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_1b
    move-exception v57

    move-object/from16 v8, v42

    .line 1123
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    .local v57, "oe":Ljava/lang/OutOfMemoryError;
    :goto_30
    :try_start_2f
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send Failed.. Exception caught in sendMessage() :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v57 .. v57}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " info=sendMessage() failed for msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Exception:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v57 .. v57}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1127
    if-eqz v57, :cond_43

    .line 1128
    invoke-virtual/range {v57 .. v57}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_0

    .line 1132
    :cond_43
    :try_start_30
    new-instance v5, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v6, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v0, p2

    invoke-virtual {v5, v6, v7, v0, v1}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendFailedOutOfMemoryError(JJ)V
    :try_end_30
    .catch Landroid/os/RemoteException; {:try_start_30 .. :try_end_30} :catch_40
    .catchall {:try_start_30 .. :try_end_30} :catchall_0

    .line 1142
    :goto_31
    const/4 v5, 0x0

    const/high16 v6, 0x50000

    :try_start_31
    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_0

    .line 1145
    const/16 v63, 0x0

    .line 1186
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_32
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_4b

    const/16 v30, 0x1

    :goto_32
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_32 .. :try_end_32} :catch_1c

    .line 1197
    :goto_33
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_44

    if-eqz v36, :cond_44

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_44
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_45

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_45
    if-eqz v8, :cond_46

    .line 1207
    :try_start_33
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_1d

    .line 1212
    :cond_46
    :goto_34
    if-eqz v49, :cond_47

    .line 1215
    :try_start_34
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_34} :catch_1e

    .line 1220
    :cond_47
    :goto_35
    if-eqz v60, :cond_48

    .line 1222
    :try_start_35
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_1f

    .line 1228
    :cond_48
    :goto_36
    if-eqz v40, :cond_49

    .line 1230
    :try_start_36
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_36} :catch_20

    .line 1237
    :cond_49
    :goto_37
    if-eqz v4, :cond_4a

    .line 1239
    :try_start_37
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_21

    .line 1245
    :cond_4a
    :goto_38
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1189
    :cond_4b
    const/16 v30, 0x0

    goto :goto_32

    .line 1191
    :catch_1c
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_33

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_1d
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_34

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_1e
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_35

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_1f
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_36

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_20
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_37

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_21
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_38

    .line 1146
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v39    # "e":Ljava/lang/Exception;
    .end local v57    # "oe":Ljava/lang/OutOfMemoryError;
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_22
    move-exception v39

    move-object/from16 v8, v42

    .line 1148
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    .local v39, "e":Lcom/android/emailcommon/cac/CACException;
    :goto_39
    :try_start_38
    invoke-virtual/range {v39 .. v39}, Lcom/android/emailcommon/cac/CACException;->getStatus()I

    move-result v65

    .line 1149
    .local v65, "status":I
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CACException, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v65 .. v65}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    const/4 v5, 0x1

    move/from16 v0, v65

    if-ne v0, v5, :cond_54

    .line 1165
    :cond_4c
    :goto_3a
    const/16 v63, 0x5e

    .line 1166
    const/4 v5, 0x0

    const/16 v6, 0x5e

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_0

    .line 1186
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_39
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_56

    const/16 v30, 0x1

    :goto_3b
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_39
    .catch Landroid/os/RemoteException; {:try_start_39 .. :try_end_39} :catch_23

    .line 1197
    .end local v39    # "e":Lcom/android/emailcommon/cac/CACException;
    :goto_3c
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_4d

    if-eqz v36, :cond_4d

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_4d
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4e

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_4e
    if-eqz v8, :cond_4f

    .line 1207
    :try_start_3a
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3a} :catch_24

    .line 1212
    :cond_4f
    :goto_3d
    if-eqz v49, :cond_50

    .line 1215
    :try_start_3b
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3b} :catch_25

    .line 1220
    :cond_50
    :goto_3e
    if-eqz v60, :cond_51

    .line 1222
    :try_start_3c
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3c} :catch_26

    .line 1228
    :cond_51
    :goto_3f
    if-eqz v40, :cond_52

    .line 1230
    :try_start_3d
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_3d} :catch_27

    .line 1237
    :cond_52
    :goto_40
    if-eqz v4, :cond_53

    .line 1239
    :try_start_3e
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3e} :catch_28

    .line 1245
    :cond_53
    :goto_41
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1152
    .restart local v39    # "e":Lcom/android/emailcommon/cac/CACException;
    :cond_54
    const/4 v5, 0x3

    move/from16 v0, v65

    if-ne v0, v5, :cond_55

    .line 1153
    :try_start_3f
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/emailcommon/cac/CACManager;->initCAC(Landroid/content/Context;)V

    goto/16 :goto_3a

    .line 1155
    :cond_55
    new-instance v38, Landroid/content/ContentValues;

    invoke-direct/range {v38 .. v38}, Landroid/content/ContentValues;-><init>()V

    .line 1156
    .local v38, "cv":Landroid/content/ContentValues;
    const-string v5, "syncServerId"

    const/4 v6, -0x6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v38

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1157
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p2

    move-object/from16 v2, v38

    invoke-static {v5, v6, v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I

    .line 1160
    const-string v24, "Certificates are not ready, try again in some seconds"

    .line 1161
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/emailcommon/cac/CACManager;->getCACState(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_4c

    .line 1162
    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/emailcommon/cac/CACManager;->initCAC(Landroid/content/Context;)V
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_0

    goto/16 :goto_3a

    .line 1189
    .end local v38    # "cv":Landroid/content/ContentValues;
    :cond_56
    const/16 v30, 0x0

    goto/16 :goto_3b

    .line 1191
    :catch_23
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_3c

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_24
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3d

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_25
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3e

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_26
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3f

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_27
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_40

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_28
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_41

    .line 1169
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v39    # "e":Ljava/lang/Exception;
    .end local v65    # "status":I
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_29
    move-exception v39

    move-object/from16 v8, v42

    .line 1170
    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v39    # "e":Ljava/lang/Exception;
    :goto_42
    :try_start_40
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): Send Failed.. Exception caught in sendMessage()"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    sget-object v5, Lcom/android/exchange/EasOutboxService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "thread="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mAccount="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " mMailbox="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasOutboxService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v7, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " cmd="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v37

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " accId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " msgId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Exception="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/exchange/ServiceLogger;->logEasOutboxServiceStats(Ljava/lang/String;)V

    .line 1176
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    .line 1182
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p2

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_0

    .line 1183
    const/16 v63, 0x0

    .line 1186
    const-string v5, "[Eas]OutboxService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendMessage(): finally. msgId = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :try_start_41
    new-instance v25, Lcom/android/emailcommon/service/AccountServiceProxy;

    sget-object v5, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasOutboxService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v26, v0

    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_5e

    const/16 v30, 0x1

    :goto_43
    move-wide/from16 v28, p2

    move-object/from16 v31, v24

    invoke-virtual/range {v25 .. v31}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifySendResult(JJZLjava/lang/String;)V
    :try_end_41
    .catch Landroid/os/RemoteException; {:try_start_41 .. :try_end_41} :catch_2a

    .line 1197
    .end local v39    # "e":Ljava/lang/Exception;
    :goto_44
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasOutboxService;->getbMessageSent()Z

    move-result v5

    if-nez v5, :cond_57

    if-eqz v36, :cond_57

    .line 1198
    move-object/from16 v0, v36

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mRetrySendTimes:I

    move/from16 v29, v0

    move-object/from16 v26, p0

    move-wide/from16 v27, p2

    move-wide/from16 v30, v54

    invoke-direct/range {v26 .. v31}, Lcom/android/exchange/EasOutboxService;->updateNextSendTimestamp(JIJ)V

    .line 1202
    :cond_57
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_58

    .line 1203
    invoke-virtual/range {v66 .. v66}, Ljava/io/File;->delete()Z

    .line 1205
    :cond_58
    if-eqz v8, :cond_59

    .line 1207
    :try_start_42
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_42} :catch_2b

    .line 1212
    :cond_59
    :goto_45
    if-eqz v49, :cond_5a

    .line 1215
    :try_start_43
    invoke-virtual/range {v49 .. v49}, Ljava/io/FileInputStream;->close()V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_43} :catch_2c

    .line 1220
    :cond_5a
    :goto_46
    if-eqz v60, :cond_5b

    .line 1222
    :try_start_44
    invoke-virtual/range {v60 .. v60}, Ljava/io/RandomAccessFile;->close()V
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_44} :catch_2d

    .line 1228
    :cond_5b
    :goto_47
    if-eqz v40, :cond_5c

    .line 1230
    :try_start_45
    invoke-virtual/range {v40 .. v40}, Ljava/io/FileInputStream;->close()V
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_45 .. :try_end_45} :catch_2e

    .line 1237
    :cond_5c
    :goto_48
    if-eqz v4, :cond_5d

    .line 1239
    :try_start_46
    invoke-virtual {v4}, Ljava/io/BufferedInputStream;->close()V
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_46} :catch_2f

    .line 1245
    :cond_5d
    :goto_49
    const-wide/16 v6, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/android/exchange/EasOutboxService;->setmPendingMessageId(J)V

    goto/16 :goto_18

    .line 1189
    .restart local v39    # "e":Ljava/lang/Exception;
    :cond_5e
    const/16 v30, 0x0

    goto :goto_43

    .line 1191
    :catch_2a
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_44

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_2b
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_45

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_2c
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_46

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_2d
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_47

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_2e
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_48

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_2f
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_49

    .line 1189
    .end local v39    # "e":Ljava/lang/Exception;
    :cond_5f
    const/16 v30, 0x0

    goto/16 :goto_a

    .line 1191
    :catch_30
    move-exception v39

    .line 1192
    .local v39, "e":Landroid/os/RemoteException;
    invoke-virtual/range {v39 .. v39}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_b

    .line 1208
    .end local v39    # "e":Landroid/os/RemoteException;
    :catch_31
    move-exception v39

    .line 1209
    .local v39, "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_c

    .line 1216
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_32
    move-exception v39

    .line 1217
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_d

    .line 1223
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_33
    move-exception v39

    .line 1224
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_e

    .line 1231
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_34
    move-exception v39

    .line 1232
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_f

    .line 1240
    .end local v39    # "e":Ljava/lang/Exception;
    :catch_35
    move-exception v39

    .line 1241
    .restart local v39    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v39 .. v39}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_10

    .line 1186
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v39    # "e":Ljava/lang/Exception;
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v5

    move-object/from16 v8, v42

    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    goto/16 :goto_9

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catchall_3
    move-exception v5

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_9

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catchall_4
    move-exception v5

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_9

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catchall_5
    move-exception v5

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_9

    .end local v35    # "buf":[B
    .end local v46    # "i":I
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    :catchall_6
    move-exception v5

    move-object/from16 v49, v50

    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_9

    .line 1169
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :catch_36
    move-exception v39

    goto/16 :goto_42

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_37
    move-exception v39

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_42

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_38
    move-exception v39

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_42

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_39
    move-exception v39

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_42

    .end local v35    # "buf":[B
    .end local v46    # "i":I
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    :catch_3a
    move-exception v39

    move-object/from16 v49, v50

    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_42

    .line 1146
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :catch_3b
    move-exception v39

    goto/16 :goto_39

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_3c
    move-exception v39

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_39

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_3d
    move-exception v39

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_39

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_3e
    move-exception v39

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_39

    .end local v35    # "buf":[B
    .end local v46    # "i":I
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    :catch_3f
    move-exception v39

    move-object/from16 v49, v50

    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_39

    .line 1134
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v57    # "oe":Ljava/lang/OutOfMemoryError;
    :catch_40
    move-exception v5

    goto/16 :goto_31

    .line 1122
    .end local v57    # "oe":Ljava/lang/OutOfMemoryError;
    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    :catch_41
    move-exception v57

    goto/16 :goto_30

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_42
    move-exception v57

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_30

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_43
    move-exception v57

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_30

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_44
    move-exception v57

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_30

    .end local v35    # "buf":[B
    .end local v46    # "i":I
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    :catch_45
    move-exception v57

    move-object/from16 v49, v50

    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_30

    .line 1106
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :catch_46
    move-exception v39

    goto/16 :goto_28

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_47
    move-exception v39

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_28

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_48
    move-exception v39

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_28

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_49
    move-exception v39

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_28

    .end local v35    # "buf":[B
    .end local v46    # "i":I
    .end local v49    # "inputStream":Ljava/io/FileInputStream;
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v50    # "inputStream":Ljava/io/FileInputStream;
    :catch_4a
    move-exception v39

    move-object/from16 v49, v50

    .end local v50    # "inputStream":Ljava/io/FileInputStream;
    .restart local v49    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_28

    .line 1084
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v51    # "isEas14":Z
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_4b
    move-exception v39

    move-object/from16 v8, v42

    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    goto/16 :goto_19

    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    :catch_4c
    move-exception v39

    goto/16 :goto_19

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_4d
    move-exception v39

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_19

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_4e
    move-exception v39

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_19

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_4f
    move-exception v39

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_19

    .line 1070
    .end local v8    # "fileStream":Ljava/io/FileOutputStream;
    .end local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .end local v35    # "buf":[B
    .end local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v45    # "host":Ljava/lang/String;
    .end local v46    # "i":I
    .end local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    .end local v51    # "isEas14":Z
    .end local v53    # "mimeSize2":I
    .end local v58    # "opbytes":I
    .end local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .end local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    .end local v69    # "wbxmlData":[B
    .restart local v42    # "fileStream":Ljava/io/FileOutputStream;
    :catch_50
    move-exception v39

    move-object/from16 v8, v42

    .end local v42    # "fileStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileStream":Ljava/io/FileOutputStream;
    goto/16 :goto_8

    .restart local v44    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v45    # "host":Ljava/lang/String;
    .restart local v47    # "info":Lcom/android/exchange/EasOutboxService$OriginalMessageInfo;
    :catch_51
    move-exception v39

    goto/16 :goto_8

    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v9    # "rfAdapter":Lcom/android/exchange/adapter/ComposeMailAdapter;
    .restart local v51    # "isEas14":Z
    .restart local v59    # "ps":Lcom/android/emailcommon/service/PolicySet;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v64    # "serializer":Lcom/android/exchange/adapter/Serializer;
    :catch_52
    move-exception v39

    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_8

    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v46    # "i":I
    .restart local v53    # "mimeSize2":I
    .restart local v58    # "opbytes":I
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v69    # "wbxmlData":[B
    :catch_53
    move-exception v39

    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_8

    .end local v4    # "bis":Ljava/io/BufferedInputStream;
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v35    # "buf":[B
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :catch_54
    move-exception v39

    move-object/from16 v4, v34

    .end local v34    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "bis":Ljava/io/BufferedInputStream;
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    move-object/from16 v60, v61

    .end local v61    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v60    # "randFile":Ljava/io/RandomAccessFile;
    goto/16 :goto_8

    .end local v35    # "buf":[B
    .end local v40    # "file":Ljava/io/FileInputStream;
    .end local v60    # "randFile":Ljava/io/RandomAccessFile;
    .restart local v41    # "file":Ljava/io/FileInputStream;
    .restart local v61    # "randFile":Ljava/io/RandomAccessFile;
    :cond_60
    move-object/from16 v40, v41

    .end local v41    # "file":Ljava/io/FileInputStream;
    .restart local v40    # "file":Ljava/io/FileInputStream;
    goto/16 :goto_6
.end method

.method public sendMessageCancel([JJJ)V
    .locals 4
    .param p1, "messageIds"    # [J
    .param p2, "outboxId"    # J
    .param p4, "accountId"    # J

    .prologue
    .line 338
    const-string v0, "[Eas]OutboxService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendMessageCancel : start. accountId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    if-eqz p1, :cond_0

    array-length v0, p1

    if-gtz v0, :cond_1

    .line 341
    :cond_0
    const-string v0, "[Eas]OutboxService"

    const-string v1, "sendMessageCancel : messageIds == null || messageIds.length <= 0 "

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    :goto_0
    return-void

    .line 345
    :cond_1
    invoke-virtual/range {p0 .. p5}, Lcom/android/exchange/EasOutboxService;->abortPendingPost([JJJ)V

    goto :goto_0
.end method

.method sendSMS(J)I
    .locals 19
    .param p1, "msgId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 220
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): start sendSMS. msgid = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v13, 0x0

    .line 223
    .local v13, "result":I
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    move-wide/from16 v0, p1

    invoke-static {v15, v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v11

    .line 224
    .local v11, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v11, :cond_9

    .line 225
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    iget-wide v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    move-wide/from16 v16, v0

    invoke-static/range {v15 .. v17}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v10

    .line 226
    .local v10, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v10, :cond_8

    .line 227
    new-instance v14, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v14}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 229
    .local v14, "s":Lcom/android/exchange/adapter/Serializer;
    const/4 v15, 0x5

    invoke-virtual {v14, v15}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0x1c

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0xf

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0xb

    iget-object v0, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0x12

    iget-object v0, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0x1e

    const-string v17, "0"

    invoke-virtual/range {v15 .. v17}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0x16

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0x9

    invoke-virtual/range {v15 .. v16}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    const/16 v16, 0xd

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 236
    const/4 v12, 0x0

    .line 238
    .local v12, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    const-string v15, "Sync"

    invoke-virtual {v14}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Lcom/android/exchange/EasOutboxService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v12

    .line 240
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 241
    .local v5, "code":I
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): code:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/16 v15, 0xc8

    if-ne v5, v15, :cond_2

    .line 244
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 245
    .local v9, "is":Ljava/io/InputStream;
    new-instance v4, Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    .local v4, "adapter":Lcom/android/exchange/adapter/EmailSyncAdapter;
    :try_start_1
    invoke-virtual {v4, v9}, Lcom/android/exchange/adapter/EmailSyncAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v11, v10}, Lcom/android/exchange/EasOutboxService;->relaySms(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 254
    const-string v15, "[Eas]OutboxService"

    const-string v16, "sendSMS() : Deleting message..."

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/EasOutboxService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v16, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 259
    const/4 v13, 0x0

    .line 260
    const-wide/16 v16, -0x1

    const/4 v15, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v15, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 308
    .end local v4    # "adapter":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .end local v9    # "is":Ljava/io/InputStream;
    :goto_1
    if-eqz v12, :cond_0

    .line 309
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    .line 310
    :cond_0
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): end sendSMS. msgId = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .end local v5    # "code":I
    .end local v10    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v12    # "resp":Lcom/android/exchange/EasResponse;
    .end local v14    # "s":Lcom/android/exchange/adapter/Serializer;
    :goto_2
    return v13

    .line 249
    .restart local v4    # "adapter":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .restart local v5    # "code":I
    .restart local v9    # "is":Ljava/io/InputStream;
    .restart local v10    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v12    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v14    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v8

    .line 250
    .local v8, "ex":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 271
    .end local v4    # "adapter":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .end local v5    # "code":I
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v9    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v7

    .line 272
    .local v7, "e":Ljava/io/IOException;
    :try_start_4
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): SMS Sending to: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " failed"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 277
    :try_start_5
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 278
    .local v6, "cv":Landroid/content/ContentValues;
    const-string v15, "syncServerId"

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 279
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v16, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-static {v15, v0, v1, v2, v6}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 284
    .end local v6    # "cv":Landroid/content/ContentValues;
    :goto_3
    const/4 v15, 0x0

    const/16 v16, 0x0

    :try_start_6
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v15, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 286
    const/4 v13, 0x0

    .line 308
    if-eqz v12, :cond_1

    .line 309
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    .line 310
    :cond_1
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): end sendSMS. msgId = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 261
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v5    # "code":I
    :cond_2
    :try_start_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasOutboxService;->isProvisionError(I)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 262
    const/4 v13, 0x4

    .line 263
    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 287
    .end local v5    # "code":I
    :catch_2
    move-exception v7

    .line 288
    .local v7, "e":Ljava/lang/Exception;
    if-eqz v7, :cond_3

    .line 289
    :try_start_8
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): SMS Send Failed.. Caught Exception: Message:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 295
    :cond_3
    :try_start_9
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 296
    .restart local v6    # "cv":Landroid/content/ContentValues;
    const-string v15, "syncServerId"

    const/16 v16, -0x2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 297
    sget-object v15, Lcom/android/exchange/EasOutboxService;->mContext:Landroid/content/Context;

    sget-object v16, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-static {v15, v0, v1, v2, v6}, Lcom/android/emailcommon/provider/EmailContent$Message;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 302
    .end local v6    # "cv":Landroid/content/ContentValues;
    :goto_4
    const/4 v15, 0x0

    const/16 v16, 0x0

    :try_start_a
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v15, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 306
    const/4 v13, 0x0

    .line 308
    if-eqz v12, :cond_4

    .line 309
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    .line 310
    :cond_4
    const-string v15, "[Eas]OutboxService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "sendSMS(): end sendSMS. msgId = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 264
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v5    # "code":I
    :cond_5
    const/16 v15, 0x191

    if-ne v5, v15, :cond_7

    .line 265
    const/4 v13, 0x2

    .line 266
    const/4 v15, 0x0

    :try_start_b
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2, v15, v13}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 308
    .end local v5    # "code":I
    :catchall_0
    move-exception v15

    if-eqz v12, :cond_6

    .line 309
    invoke-virtual {v12}, Lcom/android/exchange/EasResponse;->close()V

    .line 310
    :cond_6
    const-string v16, "[Eas]OutboxService"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "sendSMS(): end sendSMS. msgId = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    throw v15

    .line 268
    .restart local v5    # "code":I
    :cond_7
    const/4 v13, 0x0

    .line 269
    const/4 v15, 0x0

    const/16 v16, 0x0

    :try_start_c
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v16

    invoke-direct {v0, v1, v2, v15, v3}, Lcom/android/exchange/EasOutboxService;->sendCallback(JLjava/lang/String;I)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_1

    .line 280
    .end local v5    # "code":I
    .local v7, "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 281
    .restart local v8    # "ex":Ljava/lang/Exception;
    :try_start_d
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 298
    .end local v8    # "ex":Ljava/lang/Exception;
    .local v7, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v8

    .line 299
    .restart local v8    # "ex":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto :goto_4

    .line 313
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "ex":Ljava/lang/Exception;
    .end local v12    # "resp":Lcom/android/exchange/EasResponse;
    .end local v14    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_8
    const-string v15, "[Eas]OutboxService"

    const-string v16, "sendSMS(): fail to sendSMS. mailbox is null"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 316
    .end local v10    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_9
    const-string v15, "[Eas]OutboxService"

    const-string v16, "sendSMS(): fail to sendSMS. msg is null"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
