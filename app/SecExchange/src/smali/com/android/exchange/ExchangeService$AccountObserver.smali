.class Lcom/android/exchange/ExchangeService$AccountObserver;
.super Landroid/database/ContentObserver;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AccountObserver"
.end annotation


# instance fields
.field mEasAccountSelector:Ljava/lang/String;

.field mSyncableEasMailboxSelector:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method public constructor <init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;)V
    .locals 16
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 2298
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    .line 2299
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 2294
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    .line 2296
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    .line 2301
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2303
    .local v4, "context":Landroid/content/Context;
    sget-object v9, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v9

    .line 2304
    :try_start_0
    const-string v8, "accountobserver contructor"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2305
    sget-object v8, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    # invokes: Lcom/android/exchange/ExchangeService;->collectEasAccounts(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;
    invoke-static {v4, v8}, Lcom/android/exchange/ExchangeService;->access$1500(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;

    .line 2307
    sget-object v8, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2319
    .local v2, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    sget-object v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v10, "accountKey =? AND type =?"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-wide v14, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/16 v13, 0x44

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v4, v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 2332
    .local v3, "cnt":I
    if-nez v3, :cond_0

    .line 2333
    const-string v8, "Adding __eas mailbox"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2334
    iget-wide v10, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/android/exchange/ExchangeService$AccountObserver;->addAccountMailbox(J)V

    goto :goto_0

    .line 2339
    .end local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v3    # "cnt":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 2338
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    sget-object v8, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v8}, Lcom/android/exchange/ExchangeService$AccountList;->clone()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/exchange/ExchangeService$AccountList;

    .line 2339
    .local v7, "tmpAccountList":Lcom/android/exchange/ExchangeService$AccountList;
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2344
    if-eqz v7, :cond_2

    .line 2347
    :try_start_2
    new-instance v8, Lcom/android/exchange/ExchangeService$AccountObserver$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v8, v0, v1, v7}, Lcom/android/exchange/ExchangeService$AccountObserver$1;-><init>(Lcom/android/exchange/ExchangeService$AccountObserver;Lcom/android/exchange/ExchangeService;Lcom/android/exchange/ExchangeService$AccountList;)V

    invoke-static {v8}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2379
    :cond_2
    :goto_1
    return-void

    .line 2372
    :catch_0
    move-exception v5

    .line 2373
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic access$1700(Lcom/android/exchange/ExchangeService$AccountObserver;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/ExchangeService$AccountObserver;

    .prologue
    .line 2290
    invoke-direct {p0}, Lcom/android/exchange/ExchangeService$AccountObserver;->onAccountChanged()V

    return-void
.end method

.method private addAccountMailbox(J)V
    .locals 7
    .param p1, "acctId"    # J

    .prologue
    .line 2653
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 2654
    .local v0, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_0

    .line 2655
    new-instance v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;-><init>()V

    .line 2656
    .local v1, "main":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    const-string v2, "__eas"

    iput-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    .line 2657
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "__eas"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    .line 2658
    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    iput-wide v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    .line 2659
    const/16 v2, 0x44

    iput v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    .line 2660
    const/4 v2, -0x2

    iput v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncInterval:I

    .line 2661
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mFlagVisible:Z

    .line 2662
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->save(Landroid/content/Context;)Landroid/net/Uri;

    .line 2666
    .end local v1    # "main":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_0
    return-void
.end method

.method private onAccountChanged()V
    .locals 26

    .prologue
    .line 2463
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService;->maybeStartExchangeServiceThread()V

    .line 2464
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v13

    .line 2469
    .local v13, "context":Landroid/content/Context;
    new-instance v3, Lcom/android/exchange/ExchangeService$AccountList;

    invoke-direct {v3}, Lcom/android/exchange/ExchangeService$AccountList;-><init>()V

    # invokes: Lcom/android/exchange/ExchangeService;->collectEasAccounts(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;
    invoke-static {v13, v3}, Lcom/android/exchange/ExchangeService;->access$1500(Landroid/content/Context;Lcom/android/exchange/ExchangeService$AccountList;)Lcom/android/exchange/ExchangeService$AccountList;

    move-result-object v14

    .line 2474
    .local v14, "currentAccounts":Lcom/android/exchange/ExchangeService$AccountList;
    if-nez v14, :cond_0

    .line 2475
    const-string v3, "onAccountChanged(): empty accounts list was returned from collectEasAccounts()! Exit immediately."

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2641
    :goto_0
    return-void

    .line 2481
    :cond_0
    sget-boolean v3, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v3, :cond_1

    .line 2482
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAccountChanged(): currentAccounts size is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v14}, Lcom/android/exchange/ExchangeService$AccountList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 2483
    invoke-virtual {v14}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2484
    .local v2, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAccountChanged():  current accounts "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 2487
    .end local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_1
    sget-object v22, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v22

    .line 2492
    :try_start_0
    sget-object v3, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-nez v3, :cond_2

    .line 2493
    const-string v3, "!!! EAS ExchangeService  -----------  Quit onAccountChanged() : null INSTANCE "

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2494
    monitor-exit v22

    goto :goto_0

    .line 2637
    :catchall_0
    move-exception v3

    monitor-exit v22
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 2498
    :cond_2
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAccountChanged(): mAccountList size is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v4}, Lcom/android/exchange/ExchangeService$AccountList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2499
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_3
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2502
    .restart local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v14, v4, v5}, Lcom/android/exchange/ExchangeService$AccountList;->contains(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-nez v3, :cond_4

    .line 2504
    :try_start_2
    new-instance v3, Lcom/android/emailcommon/service/AccountServiceProxy;

    invoke-direct {v3, v13}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v4, v5}, Lcom/android/emailcommon/service/AccountServiceProxy;->resetNewMessageCount(J)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2509
    :cond_4
    :goto_3
    :try_start_3
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit8 v3, v3, 0x10

    if-eqz v3, :cond_5

    const/4 v9, 0x1

    .line 2513
    .local v9, "accountIncomplete":Z
    :goto_4
    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v14, v4, v5}, Lcom/android/exchange/ExchangeService$AccountList;->contains(J)Z

    move-result v3

    if-nez v3, :cond_6

    if-nez v9, :cond_6

    .line 2514
    iget-object v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/exchange/provider/ExchangeDirectoryProvider;->deleteAccountFromMap(Ljava/lang/String;)V

    .line 2516
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    # invokes: Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V
    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->access$1200(Lcom/android/exchange/ExchangeService;JZ)V

    .line 2521
    new-instance v10, Landroid/accounts/Account;

    iget-object v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v4, "com.android.exchange"

    invoke-direct {v10, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523
    .local v10, "acct":Landroid/accounts/Account;
    const-string v3, "onAccountChanged(), removing from acount manger db"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 2524
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v10, v4, v5}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 2525
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    .line 2526
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    goto :goto_2

    .line 2509
    .end local v9    # "accountIncomplete":Z
    .end local v10    # "acct":Landroid/accounts/Account;
    :cond_5
    const/4 v9, 0x0

    goto :goto_4

    .line 2529
    .restart local v9    # "accountIncomplete":Z
    :cond_6
    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v13, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v21

    .line 2530
    .local v21, "updatedAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v21, :cond_3

    .line 2535
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    if-nez v3, :cond_b

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit16 v3, v3, 0x4000

    if-eqz v3, :cond_b

    .line 2537
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2538
    :try_start_4
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 2539
    .local v15, "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->access$1000(Lcom/android/exchange/ExchangeService;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    .line 2540
    .local v19, "mid":Ljava/lang/Long;
    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v13, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v11

    .line 2541
    .local v11, "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v11, :cond_7

    iget v3, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x44

    if-ne v3, v5, :cond_7

    iget-wide v6, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    cmp-long v3, v6, v24

    if-nez v3, :cond_7

    .line 2544
    move-object/from16 v0, v19

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2557
    .end local v11    # "box":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v15    # "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "mid":Ljava/lang/Long;
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2547
    .restart local v15    # "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .restart local v18    # "i$":Ljava/util/Iterator;
    :cond_8
    :try_start_6
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_6
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    .line 2548
    .restart local v19    # "mid":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mServiceMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->access$1000(Lcom/android/exchange/ExchangeService;)Ljava/util/HashMap;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/exchange/AbstractSyncService;

    .line 2549
    .local v20, "svc":Lcom/android/exchange/AbstractSyncService;
    if-eqz v20, :cond_9

    .line 2550
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PW Expired!! Stopping sync for "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/android/exchange/AbstractSyncService;->mMailboxName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 2551
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/AbstractSyncService;->stop()V

    .line 2552
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v3, :cond_9

    .line 2553
    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/android/exchange/AbstractSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    .line 2555
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    # invokes: Lcom/android/exchange/ExchangeService;->releaseMailbox(J)V
    invoke-static {v3, v6, v7}, Lcom/android/exchange/ExchangeService;->access$1600(Lcom/android/exchange/ExchangeService;J)V

    goto :goto_6

    .line 2557
    .end local v19    # "mid":Ljava/lang/Long;
    .end local v20    # "svc":Lcom/android/exchange/AbstractSyncService;
    :cond_a
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2561
    .end local v15    # "deletedBoxes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_b
    :try_start_7
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    if-eq v3, v4, :cond_c

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_d

    :cond_c
    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    if-ne v3, v4, :cond_d

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mCalendarSyncLookback:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mCalendarSyncLookback:I

    if-ne v3, v4, :cond_d

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailSize:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailSize:I

    if-ne v3, v4, :cond_d

    invoke-static {v13}, Lcom/android/emailcommon/utility/Utility;->isRoaming(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_e

    iget v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mRoamingEmailSize:I

    move-object/from16 v0, v21

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mRoamingEmailSize:I

    if-eq v3, v4, :cond_e

    .line 2577
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v0, v21

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/android/exchange/ExchangeService;->setEasSyncIntervals(Landroid/content/Context;Ljava/lang/String;IJZ)V

    .line 2586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v6, 0x1

    # invokes: Lcom/android/exchange/ExchangeService;->stopAccountSyncs(JZ)V
    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->access$1200(Lcom/android/exchange/ExchangeService;JZ)V

    .line 2589
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/android/exchange/ExchangeService$AccountObserver;->onSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/android/exchange/ExchangeService$AccountObserver;->onSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 2590
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5, v2}, Lcom/android/exchange/ExchangeService;->releaseSyncHolds(Landroid/content/Context;ILcom/android/emailcommon/provider/EmailContent$Account;)Z

    .line 2595
    :cond_f
    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    .line 2596
    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncLookback:I

    .line 2597
    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    .line 2598
    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailSize:I

    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailSize:I

    .line 2599
    move-object/from16 v0, v21

    iget v3, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mRoamingEmailSize:I

    iput v3, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mRoamingEmailSize:I

    goto/16 :goto_2

    .line 2603
    .end local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v9    # "accountIncomplete":Z
    .end local v21    # "updatedAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_10
    invoke-virtual {v14}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .restart local v17    # "i$":Ljava/util/Iterator;
    :cond_11
    :goto_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2606
    .restart local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v4, v5}, Lcom/android/exchange/ExchangeService$AccountList;->contains(J)Z

    move-result v3

    if-nez v3, :cond_11

    .line 2608
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v16

    .line 2610
    .local v16, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v16, :cond_11

    .line 2612
    move-object/from16 v0, v16

    iput-object v0, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 2617
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const-string v4, "accountKey =? AND type =?"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v0, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/16 v7, 0x44

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v13, v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->count(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v12

    .line 2626
    .local v12, "cnt":I
    if-nez v12, :cond_12

    .line 2627
    iget-wide v4, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/android/exchange/ExchangeService$AccountObserver;->addAccountMailbox(J)V

    .line 2629
    :cond_12
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v3, v2}, Lcom/android/exchange/ExchangeService$AccountList;->add(Lcom/android/emailcommon/provider/EmailContent$Account;)Z

    .line 2630
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    .line 2631
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    goto :goto_7

    .line 2635
    .end local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v12    # "cnt":I
    .end local v16    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    :cond_13
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v3}, Lcom/android/exchange/ExchangeService$AccountList;->clear()V

    .line 2636
    sget-object v3, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v3, v14}, Lcom/android/exchange/ExchangeService$AccountList;->addAll(Ljava/util/Collection;)Z

    .line 2637
    monitor-exit v22
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2640
    const-string v3, "account changed"

    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2505
    .end local v17    # "i$":Ljava/util/Iterator;
    .restart local v2    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :catch_0
    move-exception v3

    goto/16 :goto_3
.end method

.method private onSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 1
    .param p1, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 2459
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getAccountKeyWhere()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2415
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 2416
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "accountKey in ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2417
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 2418
    .local v1, "first":Z
    sget-object v5, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v5

    .line 2419
    :try_start_0
    sget-object v4, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v4}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2420
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v1, :cond_0

    .line 2421
    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2425
    :goto_1
    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2427
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 2423
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 2427
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2428
    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2429
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    .line 2431
    .end local v1    # "first":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mEasAccountSelector:Ljava/lang/String;

    return-object v4
.end method

.method public getSyncableEasMailboxWhere()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2388
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 2389
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "(type=4 or type=68 or syncInterval!=-1) and accountKey in ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2390
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 2391
    .local v1, "first":Z
    sget-object v5, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    monitor-enter v5

    .line 2392
    :try_start_0
    sget-object v4, Lcom/android/exchange/ExchangeService;->mAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v4}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2393
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v1, :cond_0

    .line 2394
    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2398
    :goto_1
    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2400
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 2396
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 2400
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2401
    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2402
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    .line 2404
    .end local v1    # "first":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    iget-object v4, p0, Lcom/android/exchange/ExchangeService$AccountObserver;->mSyncableEasMailboxSelector:Ljava/lang/String;

    return-object v4
.end method

.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 2645
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/exchange/ExchangeService$AccountObserver$2;

    invoke-direct {v1, p0}, Lcom/android/exchange/ExchangeService$AccountObserver$2;-><init>(Lcom/android/exchange/ExchangeService$AccountObserver;)V

    const-string v2, "Account Observer"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2650
    return-void
.end method
