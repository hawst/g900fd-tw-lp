.class Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;
.super Ljava/lang/Object;
.source "SmsRelayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/SmsRelayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RelaySmsRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/SmsRelayService;


# direct methods
.method private constructor <init>(Lcom/android/exchange/SmsRelayService;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/exchange/SmsRelayService;Lcom/android/exchange/SmsRelayService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/exchange/SmsRelayService;
    .param p2, "x1"    # Lcom/android/exchange/SmsRelayService$1;

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;-><init>(Lcom/android/exchange/SmsRelayService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mSyncObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$100(Lcom/android/exchange/SmsRelayService;)Ljava/lang/Object;

    move-result-object v27

    monitor-enter v27

    .line 128
    const/16 v24, 0x0

    .line 129
    .local v24, "ownNumber":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$200(Lcom/android/exchange/SmsRelayService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$200(Lcom/android/exchange/SmsRelayService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v24

    .line 131
    if-nez v24, :cond_0

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mTm:Landroid/telephony/TelephonyManager;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$200(Lcom/android/exchange/SmsRelayService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v15

    .line 133
    .local v15, "imsi":Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 134
    move-object/from16 v24, v15

    .line 141
    .end local v15    # "imsi":Ljava/lang/String;
    :cond_0
    :goto_0
    const-wide/16 v18, -0x1

    .line 142
    .local v18, "mAccountId":J
    const-wide/16 v20, -0x1

    .line 143
    .local v20, "mLatestMsgId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$300(Lcom/android/exchange/SmsRelayService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 145
    .local v8, "msgCount":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$300(Lcom/android/exchange/SmsRelayService;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mStop:Z
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$400(Lcom/android/exchange/SmsRelayService;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$300(Lcom/android/exchange/SmsRelayService;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/android/exchange/SmsRelayService$SmsPayload;

    .line 147
    .local v25, "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mSmsQueue:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$300(Lcom/android/exchange/SmsRelayService;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # getter for: Lcom/android/exchange/SmsRelayService;->mStop:Z
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$400(Lcom/android/exchange/SmsRelayService;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 156
    const/4 v10, 0x0

    .line 158
    .local v10, "cursorAccount":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    invoke-virtual {v2}, Lcom/android/exchange/SmsRelayService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "emailAddress"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "flags"

    aput-object v6, v4, v5

    const-string v5, "protocolVersion NOT IN (\'2.5\',\'12.0\',\'12.1\')"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 169
    if-eqz v10, :cond_9

    .line 170
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Accounts found = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 175
    :cond_2
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Value of Flag = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    and-int/lit16 v2, v2, 0x800

    if-eqz v2, :cond_8

    .line 178
    new-instance v23, Lcom/android/emailcommon/provider/EmailContent$Message;

    invoke-direct/range {v23 .. v23}, Lcom/android/emailcommon/provider/EmailContent$Message;-><init>()V

    .line 179
    .local v23, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, v23

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    .line 180
    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v18, v0

    .line 181
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/android/exchange/SmsRelayService$SmsPayload;->mMessages:[Landroid/telephony/SmsMessage;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getOriginatingAddress()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/exchange/SmsRelayService;->getMobileAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/android/exchange/SmsRelayService;->access$500(Lcom/android/exchange/SmsRelayService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    .line 183
    if-eqz v24, :cond_4

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    move-object/from16 v0, v24

    # invokes: Lcom/android/exchange/SmsRelayService;->getMobileAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/android/exchange/SmsRelayService;->access$500(Lcom/android/exchange/SmsRelayService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    .line 185
    if-eqz v24, :cond_5

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    move-object/from16 v0, v24

    # invokes: Lcom/android/exchange/SmsRelayService;->getMobileAddress(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/android/exchange/SmsRelayService;->access$500(Lcom/android/exchange/SmsRelayService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mReplyTo:Ljava/lang/String;

    .line 189
    const-string v2, ""

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 190
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_4
    move-object/from16 v0, v25

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$SmsPayload;->mMessages:[Landroid/telephony/SmsMessage;

    array-length v2, v2

    if-ge v14, v2, :cond_6

    .line 191
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/android/exchange/SmsRelayService$SmsPayload;->mMessages:[Landroid/telephony/SmsMessage;

    aget-object v3, v3, v14

    invoke-virtual {v3}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 190
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 136
    .end local v8    # "msgCount":I
    .end local v10    # "cursorAccount":Landroid/database/Cursor;
    .end local v14    # "i":I
    .end local v18    # "mAccountId":J
    .end local v20    # "mLatestMsgId":J
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v25    # "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    .restart local v15    # "imsi":Ljava/lang/String;
    :cond_3
    :try_start_2
    const-string v24, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 183
    .end local v15    # "imsi":Ljava/lang/String;
    .restart local v8    # "msgCount":I
    .restart local v10    # "cursorAccount":Landroid/database/Cursor;
    .restart local v18    # "mAccountId":J
    .restart local v20    # "mLatestMsgId":J
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v25    # "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    :cond_4
    const/4 v2, 0x1

    :try_start_3
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 185
    :cond_5
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 195
    .restart local v14    # "i":I
    :cond_6
    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    const-string v3, "\u0000"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 197
    move-object/from16 v0, v23

    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    or-int/lit16 v2, v2, 0x100

    move-object/from16 v0, v23

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageType:I

    .line 198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, v23

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    .line 199
    const/4 v2, 0x0

    move-object/from16 v0, v23

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagRead:Z

    .line 200
    const/4 v2, 0x1

    move-object/from16 v0, v23

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagLoaded:I

    .line 201
    const/4 v2, 0x0

    move-object/from16 v0, v23

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagTruncated:I

    .line 202
    const/4 v2, 0x0

    move-object/from16 v0, v23

    iput-boolean v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlagAttachment:Z

    .line 203
    const/4 v2, 0x1

    move-object/from16 v0, v23

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mImportance:I

    .line 205
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SMS_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mClientId:Ljava/lang/String;

    .line 207
    const/4 v2, 0x1

    move-object/from16 v0, v23

    iput v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMessageDirty:I

    .line 210
    move-object/from16 v0, v23

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mDisplayName:Ljava/lang/String;

    .line 211
    const-string v2, "eas"

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountSchema:Ljava/lang/String;

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    invoke-static {v2}, Lcom/android/emailcommon/CursorManager;->inst(Landroid/content/Context;)Lcom/android/emailcommon/CursorManager;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->ID_PROJECTION:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "type=0 AND accountKey="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v23

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/android/emailcommon/CursorManager;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v11

    .line 219
    .local v11, "cursorMailbox":Landroid/database/Cursor;
    if-eqz v11, :cond_8

    .line 221
    :try_start_4
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Mailbox found = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 224
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 225
    .local v22, "mailboxId":I
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mailboxId found = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    move/from16 v0, v22

    int-to-long v2, v0

    move-object/from16 v0, v23

    iput-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    .line 228
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Lcom/android/emailcommon/provider/EmailContent$Message;->save(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v26

    .line 230
    .local v26, "url":Landroid/net/Uri;
    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v20, v0

    .line 231
    if-eqz v26, :cond_a

    .line 232
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SmS Successfully stored@ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    invoke-virtual {v2}, Lcom/android/exchange/SmsRelayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    .line 238
    .local v17, "mContext":Landroid/content/Context;
    move-object/from16 v0, v23

    iget-wide v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-object/from16 v0, v17

    invoke-static {v0, v2, v3}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v16

    .line 239
    .local v16, "mAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    const-string v9, "accountKey=? and type=?"

    .line 241
    .local v9, "WHERE_ACCOUNT_ID_AND_MAILBOXTYPE":Ljava/lang/String;
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 242
    .local v12, "cv":Landroid/content/ContentValues;
    const-string v2, "syncInterval"

    invoke-virtual/range {v16 .. v16}, Lcom/android/emailcommon/provider/EmailContent$Account;->getSyncInterval()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 243
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v23

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v12, v9, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 256
    .end local v9    # "WHERE_ACCOUNT_ID_AND_MAILBOXTYPE":Ljava/lang/String;
    .end local v12    # "cv":Landroid/content/ContentValues;
    .end local v16    # "mAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v17    # "mContext":Landroid/content/Context;
    .end local v22    # "mailboxId":I
    .end local v26    # "url":Landroid/net/Uri;
    :cond_7
    :goto_5
    if-eqz v11, :cond_8

    .line 257
    :try_start_5
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 262
    .end local v11    # "cursorMailbox":Landroid/database/Cursor;
    .end local v14    # "i":I
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_8
    :goto_6
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v2

    if-nez v2, :cond_2

    .line 268
    :cond_9
    if-eqz v10, :cond_1

    :try_start_6
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 269
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 285
    .end local v8    # "msgCount":I
    .end local v10    # "cursorAccount":Landroid/database/Cursor;
    .end local v18    # "mAccountId":J
    .end local v20    # "mLatestMsgId":J
    .end local v25    # "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    :catchall_0
    move-exception v2

    monitor-exit v27
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v2

    .line 248
    .restart local v8    # "msgCount":I
    .restart local v10    # "cursorAccount":Landroid/database/Cursor;
    .restart local v11    # "cursorMailbox":Landroid/database/Cursor;
    .restart local v14    # "i":I
    .restart local v18    # "mAccountId":J
    .restart local v20    # "mLatestMsgId":J
    .restart local v22    # "mailboxId":I
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    .restart local v25    # "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    .restart local v26    # "url":Landroid/net/Uri;
    :cond_a
    :try_start_7
    const-string v2, "SmsRelayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to save SmS in mailBox = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_5

    .line 253
    .end local v22    # "mailboxId":I
    .end local v26    # "url":Landroid/net/Uri;
    :catch_0
    move-exception v13

    .line 254
    .local v13, "e":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 256
    if-eqz v11, :cond_8

    .line 257
    :try_start_9
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_6

    .line 265
    .end local v11    # "cursorMailbox":Landroid/database/Cursor;
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v14    # "i":I
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catch_1
    move-exception v13

    .line 266
    .restart local v13    # "e":Ljava/lang/Exception;
    :try_start_a
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 268
    if-eqz v10, :cond_1

    :try_start_b
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 269
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 256
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v11    # "cursorMailbox":Landroid/database/Cursor;
    .restart local v14    # "i":I
    .restart local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catchall_1
    move-exception v2

    if-eqz v11, :cond_b

    .line 257
    :try_start_c
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v2
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 268
    .end local v11    # "cursorMailbox":Landroid/database/Cursor;
    .end local v14    # "i":I
    .end local v23    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :catchall_2
    move-exception v2

    if-eqz v10, :cond_c

    :try_start_d
    invoke-interface {v10}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_c

    .line 269
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v2

    .line 275
    .end local v10    # "cursorAccount":Landroid/database/Cursor;
    .end local v25    # "payload":Lcom/android/exchange/SmsRelayService$SmsPayload;
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    invoke-virtual {v2}, Lcom/android/exchange/SmsRelayService;->getApplicationContext()Landroid/content/Context;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result-object v17

    .line 277
    .restart local v17    # "mContext":Landroid/content/Context;
    :try_start_e
    new-instance v3, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-wide/from16 v4, v18

    move-wide/from16 v6, v20

    invoke-virtual/range {v3 .. v8}, Lcom/android/emailcommon/service/AccountServiceProxy;->notifyNewMessages(JJI)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 283
    :goto_7
    :try_start_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/SmsRelayService$RelaySmsRunnable;->this$0:Lcom/android/exchange/SmsRelayService;

    # invokes: Lcom/android/exchange/SmsRelayService;->onDone()V
    invoke-static {v2}, Lcom/android/exchange/SmsRelayService;->access$600(Lcom/android/exchange/SmsRelayService;)V

    .line 285
    monitor-exit v27

    .line 286
    return-void

    .line 280
    :catch_2
    move-exception v13

    .line 281
    .local v13, "e":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto :goto_7
.end method
