.class Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;
.super Landroid/telephony/PhoneStateListener;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DuosPhoneStateListener"
.end annotation


# instance fields
.field private mSlotId:I

.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 5
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    .line 7697
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ">> onServiceStateChanged() : mPhoneType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->mSlotId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : roaming -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 7700
    iget v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->mSlotId:I

    if-nez v2, :cond_3

    .line 7701
    const-string v2, "onServiceStateChanged : 0"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7702
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v0

    .line 7703
    .local v0, "oldRoamingType":Z
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2800(Lcom/android/exchange/ExchangeService;)I

    move-result v1

    .line 7705
    .local v1, "oldServiceState":I
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    # setter for: Lcom/android/exchange/ExchangeService;->mNetworkState:I
    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->access$2802(Lcom/android/exchange/ExchangeService;I)I

    .line 7706
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-nez v2, :cond_0

    .line 7707
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v3

    # setter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->access$2602(Lcom/android/exchange/ExchangeService;Z)Z

    .line 7708
    :cond_0
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2800(Lcom/android/exchange/ExchangeService;)I

    move-result v2

    if-ne v2, v1, :cond_1

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2800(Lcom/android/exchange/ExchangeService;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 7710
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Roaming State changed: mIsNetworkRoaming:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming:Z
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->access$2600(Lcom/android/exchange/ExchangeService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7714
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # invokes: Lcom/android/exchange/ExchangeService;->updateAccountDb()V
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2400(Lcom/android/exchange/ExchangeService;)V

    .line 7733
    :cond_2
    :goto_0
    return-void

    .line 7717
    .end local v0    # "oldRoamingType":Z
    .end local v1    # "oldServiceState":I
    :cond_3
    const-string v2, "onServiceStateChanged : 1"

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7718
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2700(Lcom/android/exchange/ExchangeService;)Z

    move-result v0

    .line 7719
    .restart local v0    # "oldRoamingType":Z
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState2:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$3600(Lcom/android/exchange/ExchangeService;)I

    move-result v1

    .line 7721
    .restart local v1    # "oldServiceState":I
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    # setter for: Lcom/android/exchange/ExchangeService;->mNetworkState2:I
    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->access$3602(Lcom/android/exchange/ExchangeService;I)I

    .line 7722
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v2

    if-nez v2, :cond_4

    .line 7723
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v3

    # setter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->access$2702(Lcom/android/exchange/ExchangeService;Z)Z

    .line 7724
    :cond_4
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState2:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$3600(Lcom/android/exchange/ExchangeService;)I

    move-result v2

    if-ne v2, v1, :cond_5

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mNetworkState2:I
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$3600(Lcom/android/exchange/ExchangeService;)I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2700(Lcom/android/exchange/ExchangeService;)Z

    move-result v2

    if-eq v0, v2, :cond_2

    .line 7726
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Roaming State changed: mIsNetworkRoaming2:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mIsNetworkRoaming2:Z
    invoke-static {v3}, Lcom/android/exchange/ExchangeService;->access$2700(Lcom/android/exchange/ExchangeService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7730
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$DuosPhoneStateListener;->this$0:Lcom/android/exchange/ExchangeService;

    # invokes: Lcom/android/exchange/ExchangeService;->updateAccountDb()V
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->access$2400(Lcom/android/exchange/ExchangeService;)V

    goto :goto_0
.end method
