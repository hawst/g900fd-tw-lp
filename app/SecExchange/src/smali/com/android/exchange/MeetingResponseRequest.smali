.class public Lcom/android/exchange/MeetingResponseRequest;
.super Lcom/android/exchange/Request;
.source "MeetingResponseRequest.java"


# instance fields
.field public final mDraftMessageId:J

.field public final mResponse:I


# direct methods
.method constructor <init>(JI)V
    .locals 3
    .param p1, "messageId"    # J
    .param p3, "response"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/Request;-><init>(J)V

    .line 29
    iput p3, p0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    .line 32
    return-void
.end method

.method constructor <init>(JJI)V
    .locals 1
    .param p1, "messageId"    # J
    .param p3, "draftMessageId"    # J
    .param p5, "response"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/Request;-><init>(J)V

    .line 35
    iput p5, p0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    .line 36
    iput-wide p3, p0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 44
    instance-of v1, p1, Lcom/android/exchange/MeetingResponseRequest;

    if-nez v1, :cond_1

    .line 46
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    check-cast p1, Lcom/android/exchange/MeetingResponseRequest;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v2, p1, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    iget-wide v4, p0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    long-to-int v0, v0

    return v0
.end method
