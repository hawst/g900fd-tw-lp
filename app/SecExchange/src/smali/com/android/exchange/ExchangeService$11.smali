.class Lcom/android/exchange/ExchangeService$11;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService;->onDestroy()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/ExchangeService;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService;)V
    .locals 0

    .prologue
    .line 5199
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$11;->this$0:Lcom/android/exchange/ExchangeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 5202
    # getter for: Lcom/android/exchange/ExchangeService;->sSyncLock:Ljava/lang/Object;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$500()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 5204
    :try_start_0
    sget-object v1, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v1, :cond_0

    # getter for: Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3300()Ljava/lang/Thread;

    move-result-object v1

    if-nez v1, :cond_1

    .line 5205
    :cond_0
    monitor-exit v2

    .line 5224
    :goto_0
    return-void

    .line 5206
    :cond_1
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$11;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 5207
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$11;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3000(Lcom/android/exchange/ExchangeService;)Lcom/android/emailcommon/service/AccountServiceProxy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/emailcommon/service/AccountServiceProxy;->endTask()V

    .line 5208
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$11;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v3, 0x0

    # setter for: Lcom/android/exchange/ExchangeService;->mAccountServiceProxy:Lcom/android/emailcommon/service/AccountServiceProxy;
    invoke-static {v1, v3}, Lcom/android/exchange/ExchangeService;->access$3002(Lcom/android/exchange/ExchangeService;Lcom/android/emailcommon/service/AccountServiceProxy;)Lcom/android/emailcommon/service/AccountServiceProxy;

    .line 5212
    :cond_2
    # getter for: Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3300()Ljava/lang/Thread;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 5213
    const/4 v1, 0x1

    # setter for: Lcom/android/exchange/ExchangeService;->sStop:Z
    invoke-static {v1}, Lcom/android/exchange/ExchangeService;->access$3102(Z)Z

    .line 5214
    # getter for: Lcom/android/exchange/ExchangeService;->sServiceThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3300()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5217
    :cond_3
    :try_start_1
    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 5218
    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5223
    :cond_4
    :goto_1
    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 5220
    :catch_0
    move-exception v0

    .line 5221
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
