.class public Lcom/android/exchange/EasSyncService;
.super Lcom/android/exchange/AbstractSyncService;
.source "EasSyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;,
        Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;
    }
.end annotation


# static fields
.field protected static CHECK_PROVISIONING_IN_PROGRESS:Z

.field public static final COMMAND_TIMEOUT:I

.field public static final CONNECTION_TIMEOUT:I

.field private static deviceType:Ljava/lang/String;

.field private static flagPerformWipeCalled:Z

.field private static flagRemoteWipe:Z

.field private static mCurrentUserId:I

.field private static mFormatExtStorageThread:Ljava/lang/Thread;

.field public static modelNameForEAS:Ljava/lang/String;

.field public static protocolVersion:D

.field private static serviceLogger:Lcom/android/exchange/ServiceLogger;

.field private static userAgent:Ljava/lang/String;


# instance fields
.field public CollectionId:Ljava/lang/String;

.field public SyncKey:Ljava/lang/String;

.field public commandFinished:Z

.field protected iop:Lcom/android/exchange/adapter/ItemOperationsParser;

.field mAuthString:Ljava/lang/String;

.field private mBindArguments:[Ljava/lang/String;

.field mCmdString:Ljava/lang/String;

.field public mContentResolver:Landroid/content/ContentResolver;

.field protected mDeviceId:Ljava/lang/String;

.field public mDomain:Ljava/lang/String;

.field public mEasNeedsProvisioning:Z

.field public mHostAddress:Ljava/lang/String;

.field protected mIsSyncWithHBICmd:Z

.field public mIsValid:Z

.field public mPassword:Ljava/lang/String;

.field public mPath:Ljava/lang/String;

.field public volatile mPendingMessageId:J

.field private volatile mPendingPost:Lorg/apache/http/client/methods/HttpPost;

.field protected mPingOnHold:Z

.field protected mPort:I

.field protected mPostAborted:Z

.field protected mPostReset:Z

.field mProtocolVersion:Ljava/lang/String;

.field public mProtocolVersionDouble:Ljava/lang/Double;

.field protected mSsl:Z

.field private mTrustSsl:Z

.field public mUserName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceCommandTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_0
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/android/exchange/EasSyncService;->COMMAND_TIMEOUT:I

    .line 200
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Email_EasSyncServiceConnectionTimeoutValue"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_1
    mul-int/lit16 v0, v0, 0x3e8

    sput v0, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    .line 267
    sput-boolean v2, Lcom/android/exchange/EasSyncService;->CHECK_PROVISIONING_IN_PROGRESS:Z

    .line 325
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    sput-object v0, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    .line 408
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    sput-wide v0, Lcom/android/exchange/EasSyncService;->protocolVersion:D

    .line 418
    sput-boolean v2, Lcom/android/exchange/EasSyncService;->flagRemoteWipe:Z

    .line 420
    sput-boolean v2, Lcom/android/exchange/EasSyncService;->flagPerformWipeCalled:Z

    .line 422
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/EasSyncService;->mFormatExtStorageThread:Ljava/lang/Thread;

    .line 427
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v0

    sput-object v0, Lcom/android/exchange/EasSyncService;->serviceLogger:Lcom/android/exchange/ServiceLogger;

    .line 431
    const/4 v0, -0x1

    sput v0, Lcom/android/exchange/EasSyncService;->mCurrentUserId:I

    return-void

    .line 187
    :cond_0
    const/16 v0, 0x50

    goto :goto_0

    .line 200
    :cond_1
    const/16 v0, 0x28

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 532
    const-string v0, "EAS Validation"

    invoke-direct {p0, v0}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 534
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 8
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailboxId"    # J

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 510
    invoke-direct {p0, p1, p2, p3}, Lcom/android/exchange/AbstractSyncService;-><init>(Landroid/content/Context;J)V

    .line 265
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 290
    const-string v1, "2.5"

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 320
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 322
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 333
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 335
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 339
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 353
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 356
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 359
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 364
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mBindArguments:[Ljava/lang/String;

    .line 370
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 373
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 375
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 382
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 386
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 389
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    .line 393
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 424
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    .line 512
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 514
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 516
    .local v0, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 518
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 520
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 522
    return-void

    :cond_0
    move v1, v3

    .line 516
    goto :goto_0

    :cond_1
    move v2, v3

    .line 518
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 472
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/AbstractSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 265
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 290
    const-string v1, "2.5"

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 320
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 322
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 333
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 335
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 339
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 353
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 356
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 359
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 364
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mBindArguments:[Ljava/lang/String;

    .line 370
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 373
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 375
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 382
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 386
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 389
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    .line 393
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 424
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    .line 474
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 476
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 478
    .local v0, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 480
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 482
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 484
    return-void

    :cond_0
    move v1, v3

    .line 478
    goto :goto_0

    :cond_1
    move v2, v3

    .line 480
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_mailbox"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 435
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/AbstractSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    .line 265
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 290
    const-string v1, "2.5"

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 320
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 322
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 333
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 335
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 339
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 353
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 356
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 359
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 364
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mBindArguments:[Ljava/lang/String;

    .line 370
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 373
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 375
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 382
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 386
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 389
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    .line 393
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 424
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    .line 437
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 439
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-nez v1, :cond_0

    .line 441
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 463
    :goto_0
    return-void

    .line 447
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 449
    .local v0, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-nez v0, :cond_1

    .line 451
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    goto :goto_0

    .line 457
    :cond_1
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 459
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    :goto_2
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 461
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    goto :goto_0

    :cond_2
    move v1, v3

    .line 457
    goto :goto_1

    :cond_3
    move v2, v3

    .line 459
    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 7
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 492
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/AbstractSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 265
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 290
    const-string v1, "2.5"

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 320
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 322
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 333
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 335
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 339
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 353
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 356
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 359
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 364
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mBindArguments:[Ljava/lang/String;

    .line 370
    iput-object v6, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 373
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 375
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 382
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 386
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 389
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    .line 393
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 424
    iput-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    .line 494
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    .line 496
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v1, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p1, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v0

    .line 498
    .local v0, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 500
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 502
    iget v1, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 504
    return-void

    :cond_0
    move v1, v3

    .line 498
    goto :goto_0

    :cond_1
    move v2, v3

    .line 500
    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 526
    invoke-direct {p0, p1}, Lcom/android/exchange/AbstractSyncService;-><init>(Ljava/lang/String;)V

    .line 265
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    .line 290
    const-string v0, "2.5"

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 294
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 320
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 322
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->iop:Lcom/android/exchange/adapter/ItemOperationsParser;

    .line 333
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 335
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 339
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 353
    iput-boolean v4, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 356
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 359
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 364
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mBindArguments:[Ljava/lang/String;

    .line 370
    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 373
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 375
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 382
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 386
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 389
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    .line 393
    iput-boolean v4, p0, Lcom/android/exchange/EasSyncService;->mIsValid:Z

    .line 424
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    .line 528
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/EasSyncService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    return v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 139
    sget-boolean v0, Lcom/android/exchange/EasSyncService;->flagRemoteWipe:Z

    return v0
.end method

.method static synthetic access$202(Lcom/android/exchange/EasSyncService;Lorg/apache/http/client/methods/HttpPost;)Lorg/apache/http/client/methods/HttpPost;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/EasSyncService;
    .param p1, "x1"    # Lorg/apache/http/client/methods/HttpPost;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/exchange/EasSyncService;)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method private static acknowledgeProvision(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .param p1, "tempKey"    # Ljava/lang/String;
    .param p2, "result"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4682
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/android/exchange/EasSyncService;->acknowledgeProvisionImpl(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static acknowledgeProvisionImpl(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .param p1, "tempKey"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "remoteWipe"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x38b

    .line 4690
    new-instance v8, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v8}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 4692
    .local v8, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v9, 0x385

    invoke-virtual {v8, v9}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v9

    const/16 v10, 0x386

    invoke-virtual {v9, v10}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4694
    const/16 v9, 0x387

    invoke-virtual {v8, v9}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4698
    const/16 v9, 0x388

    invoke-static {p0}, Lcom/android/exchange/EasSyncService;->getPolicyType(Lcom/android/exchange/EasSyncService;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4700
    const/16 v9, 0x389

    invoke-virtual {v8, v9, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4702
    invoke-virtual {v8, v11, p2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4704
    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4706
    if-eqz p3, :cond_0

    .line 4708
    const/16 v9, 0x38c

    invoke-virtual {v8, v9}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4710
    const-string v9, "1"

    invoke-virtual {v8, v11, v9}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 4712
    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 4716
    :cond_0
    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 4720
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v9, :cond_1

    .line 4722
    const-string v9, "acknowledgeProvisionImpl(): Wbxml:"

    invoke-static {v9}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4724
    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v0

    .line 4726
    .local v0, "b":[B
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 4728
    .local v1, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v4, Lcom/android/exchange/adapter/LogAdapter;

    invoke-direct {v4, p0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 4730
    .local v4, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v4, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 4736
    .end local v0    # "b":[B
    .end local v1    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v4    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_1
    const-string v9, "Provision"

    invoke-virtual {v8}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v10

    invoke-virtual {p0, v9, v10}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v7

    .line 4737
    .local v7, "resp":Lcom/android/exchange/EasResponse;
    const/4 v5, 0x0

    .line 4740
    .local v5, "mInputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 4741
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v2

    .line 4744
    .local v2, "code":I
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v9, :cond_2

    .line 4745
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "acknowledgeProvisionImpl():Provision command response code:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4748
    :cond_2
    const/16 v9, 0xc8

    if-ne v2, v9, :cond_4

    .line 4749
    move-object v3, v5

    .line 4750
    .local v3, "is":Ljava/io/InputStream;
    new-instance v6, Lcom/android/exchange/adapter/ProvisionParser;

    invoke-direct {v6, v3, p0}, Lcom/android/exchange/adapter/ProvisionParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 4751
    .local v6, "pp":Lcom/android/exchange/adapter/ProvisionParser;
    invoke-virtual {v6}, Lcom/android/exchange/adapter/ProvisionParser;->parse()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 4753
    invoke-virtual {v6}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicyKey()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 4757
    if-eqz v7, :cond_3

    .line 4758
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    .line 4762
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v6    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    :cond_3
    :goto_0
    return-object v9

    .line 4757
    :cond_4
    if-eqz v7, :cond_5

    .line 4758
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    .line 4762
    :cond_5
    const/4 v9, 0x0

    goto :goto_0

    .line 4757
    .end local v2    # "code":I
    :catchall_0
    move-exception v9

    if-eqz v7, :cond_6

    .line 4758
    invoke-virtual {v7}, Lcom/android/exchange/EasResponse;->close()V

    :cond_6
    throw v9
.end method

.method private static acknowledgeRemoteWipe(Lcom/android/exchange/EasSyncService;Ljava/lang/String;)V
    .locals 2
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .param p1, "tempKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4676
    const-string v0, "1"

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/android/exchange/EasSyncService;->acknowledgeProvisionImpl(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 4678
    return-void
.end method

.method public static addProxyParamsIfProxySet(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "request"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p3, "proxyHost"    # Ljava/lang/String;
    .param p4, "proxyPort"    # I

    .prologue
    .line 5779
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5780
    invoke-interface {p1}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 5782
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    new-instance v1, Lorg/apache/http/HttpHost;

    invoke-direct {v1, p3, p4}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 5784
    invoke-virtual {p2, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 5786
    const-string v1, "PROXY4"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added param: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5789
    .end local v0    # "params":Lorg/apache/http/params/HttpParams;
    :cond_0
    return-void
.end method

.method private cacheAuthAndCmdString()V
    .locals 5

    .prologue
    const/16 v4, 0x3a

    .line 2946
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    invoke-static {v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2950
    .local v1, "safeUserName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2952
    .local v0, "cs":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mDomain:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2954
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mDomain:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2964
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Basic "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    .line 2968
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "&User="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&DeviceId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&DeviceType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    .line 2972
    return-void

    .line 2958
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static canProvision(Lcom/android/exchange/EasSyncService;)Lcom/android/exchange/adapter/ProvisionParser;
    .locals 18
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4568
    const/4 v12, 0x0

    .line 4570
    .local v12, "s":Lcom/android/exchange/adapter/Serializer;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 4572
    .local v10, "protocolVersion":Ljava/lang/Double;
    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    const-wide v16, 0x402c333333333333L    # 14.1

    cmpl-double v13, v14, v16

    if-ltz v13, :cond_5

    .line 4573
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->sendDeviceInfomationProvision(Lcom/android/exchange/EasSyncService;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v12

    .line 4574
    if-nez v12, :cond_1

    .line 4575
    const/4 v9, 0x0

    .line 4662
    :cond_0
    :goto_0
    return-object v9

    .line 4577
    :cond_1
    const/16 v13, 0x386

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4578
    const/16 v13, 0x387

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    const/16 v14, 0x388

    invoke-static/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getPolicyType(Lcom/android/exchange/EasSyncService;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 4587
    :goto_1
    sget-boolean v13, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v13, :cond_2

    .line 4588
    const-string v13, "canProvision(): Wbxml:"

    invoke-static {v13}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4589
    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v2

    .line 4590
    .local v2, "b":[B
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 4591
    .local v3, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v6, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 4592
    .local v6, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v6, v3}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 4596
    .end local v2    # "b":[B
    .end local v3    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v6    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_2
    const-string v13, "Provision"

    invoke-virtual {v12}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v11

    .line 4600
    .local v11, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 4601
    .local v5, "is":Ljava/io/InputStream;
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v4

    .line 4603
    .local v4, "code":I
    sget-boolean v13, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v13, :cond_3

    .line 4604
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "canProvision(): Provision command response code:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4607
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v13, :cond_4

    .line 4608
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "thread="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " mAccount="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v14, v14, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " cmd=Provision res="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 4610
    .local v7, "loggingPhrase":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v13

    invoke-virtual {v13, v7}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 4613
    .end local v7    # "loggingPhrase":Ljava/lang/String;
    :cond_4
    const/16 v13, 0xc8

    if-ne v4, v13, :cond_8

    .line 4614
    new-instance v9, Lcom/android/exchange/adapter/ProvisionParser;

    move-object/from16 v0, p0

    invoke-direct {v9, v5, v0}, Lcom/android/exchange/adapter/ProvisionParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 4615
    .local v9, "pp":Lcom/android/exchange/adapter/ProvisionParser;
    invoke-virtual {v9}, Lcom/android/exchange/adapter/ProvisionParser;->parse()Z

    move-result v13

    if-eqz v13, :cond_8

    .line 4627
    invoke-virtual {v9}, Lcom/android/exchange/adapter/ProvisionParser;->hasSupportablePolicySet()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    if-eqz v13, :cond_6

    .line 4657
    if-eqz v11, :cond_0

    .line 4658
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 4581
    .end local v4    # "code":I
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v9    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    :cond_5
    new-instance v12, Lcom/android/exchange/adapter/Serializer;

    .end local v12    # "s":Lcom/android/exchange/adapter/Serializer;
    invoke-direct {v12}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 4582
    .restart local v12    # "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v13, 0x385

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    const/16 v14, 0x386

    invoke-virtual {v13, v14}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 4583
    const/16 v13, 0x387

    invoke-virtual {v12, v13}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    const/16 v14, 0x388

    invoke-static/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getPolicyType(Lcom/android/exchange/EasSyncService;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->done()V

    goto/16 :goto_1

    .line 4641
    .restart local v4    # "code":I
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v9    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    :cond_6
    :try_start_1
    invoke-virtual {v9}, Lcom/android/exchange/adapter/ProvisionParser;->hasSupportablePolicySet()Z

    move-result v13

    if-nez v13, :cond_7

    .line 4644
    const-string v13, "PolicySet is NOT fully supportable"

    invoke-static {v13}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 4645
    invoke-virtual {v9}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicyKey()Ljava/lang/String;

    move-result-object v13

    const-string v14, "2"

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/android/exchange/EasSyncService;->acknowledgeProvision(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4649
    .local v8, "policyKey":Ljava/lang/String;
    if-eqz v8, :cond_7

    .line 4650
    invoke-virtual {v9}, Lcom/android/exchange/adapter/ProvisionParser;->clearUnsupportedPolicies()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4657
    .end local v8    # "policyKey":Ljava/lang/String;
    :cond_7
    if-eqz v11, :cond_0

    .line 4658
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 4657
    .end local v9    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    :cond_8
    if-eqz v11, :cond_9

    .line 4658
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    .line 4662
    :cond_9
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 4657
    .end local v4    # "code":I
    .end local v5    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v13

    if-eqz v11, :cond_a

    .line 4658
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    :cond_a
    throw v13
.end method

.method public static doResolveRecipients(Landroid/content/Context;J[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "to"    # [Ljava/lang/String;
    .param p4, "certificateRetrieval"    # I
    .param p5, "startTime"    # Ljava/lang/String;
    .param p6, "endTime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J[",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/provider/RRResponse;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1933
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] start"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1934
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1935
    .local v4, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v4, :cond_f

    if-eqz p3, :cond_f

    move-object/from16 v0, p3

    array-length v15, v0

    if-lez v15, :cond_f

    .line 1936
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v8

    .line 1937
    .local v8, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    new-instance v14, Lcom/android/exchange/EasSyncService;

    const-string v15, "%ResolveRecipients%"

    invoke-direct {v14, v15}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1939
    .local v14, "svc":Lcom/android/exchange/EasSyncService;
    :try_start_0
    sput-object p0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1940
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1941
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1942
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1943
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x1

    if-eqz v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1944
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x8

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    :goto_1
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1945
    const/4 v15, 0x0

    invoke-static {v15}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1946
    iput-object v4, v14, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1948
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v15, v14, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1949
    new-instance v13, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v13}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 1950
    .local v13, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v15, 0x285

    invoke-virtual {v13, v15}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1951
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    move-object/from16 v0, p3

    array-length v15, v0

    if-ge v9, v15, :cond_3

    .line 1952
    aget-object v15, p3, v9

    if-eqz v15, :cond_0

    .line 1953
    const/16 v15, 0x290

    aget-object v16, p3, v9

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1951
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 1943
    .end local v9    # "i":I
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 1944
    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    .line 1958
    .restart local v9    # "i":I
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_3
    const/16 v15, 0x28f

    invoke-virtual {v13, v15}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1965
    const/4 v15, 0x2

    move/from16 v0, p4

    if-eq v0, v15, :cond_4

    const/4 v15, 0x3

    move/from16 v0, p4

    if-ne v0, v15, :cond_5

    .line 1966
    :cond_4
    const/16 v15, 0x291

    invoke-static/range {p4 .. p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1968
    const/16 v15, 0x293

    const-string v16, "19"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1975
    :cond_5
    const/16 v15, 0x294

    const-string v16, "19"

    move-object/from16 v0, v16

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1979
    if-eqz p5, :cond_6

    if-eqz p6, :cond_6

    .line 1980
    const/16 v15, 0x296

    invoke-virtual {v13, v15}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1981
    const/16 v15, 0x297

    move-object/from16 v0, p5

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1982
    const/16 v15, 0x298

    move-object/from16 v0, p6

    invoke-virtual {v13, v15, v0}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1984
    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1987
    :cond_6
    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v15

    invoke-virtual {v15}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1990
    const-string v15, "ResolveRecipients"

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    :try_end_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 1993
    .local v11, "resp":Lcom/android/exchange/EasResponse;
    :try_start_1
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 1995
    .local v5, "code":I
    const-string v15, "EasSyncService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[doResolveRecipients] http status code = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1997
    const/16 v15, 0x1c1

    if-eq v5, v15, :cond_7

    const/16 v15, 0x193

    if-ne v5, v15, :cond_9

    .line 1998
    :cond_7
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] code == HTTP_NEED_PROVISIONING || code == HttpStatus.SC_FORBIDDEN. return CommandStatusException"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000
    new-instance v15, Lcom/android/exchange/CommandStatusException;

    const/16 v16, 0x8e

    invoke-direct/range {v15 .. v16}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v15
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2066
    .end local v5    # "code":I
    :catchall_0
    move-exception v15

    if-eqz v11, :cond_8

    .line 2067
    :try_start_2
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    :cond_8
    throw v15
    :try_end_2
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2071
    .end local v9    # "i":I
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v6

    .line 2072
    .local v6, "cse":Lcom/android/exchange/CommandStatusException;
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] Exception caughted"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    invoke-virtual {v6}, Lcom/android/exchange/CommandStatusException;->printStackTrace()V

    .line 2075
    const/4 v15, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-static {v0, v1, v2, v15}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 2076
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "CommandStatusException during ValidateCert: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2077
    new-instance v15, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v16, 0x7

    invoke-direct/range {v15 .. v16}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v15

    .line 2001
    .end local v6    # "cse":Lcom/android/exchange/CommandStatusException;
    .restart local v5    # "code":I
    .restart local v9    # "i":I
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_9
    const/16 v15, 0xc8

    if-ne v5, v15, :cond_13

    .line 2003
    :try_start_3
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    .line 2004
    .local v10, "is":Ljava/io/InputStream;
    if-nez v10, :cond_a

    .line 2005
    const-string v15, "ExchangeService"

    const-string v16, "doResolveRecipients http response has no content"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2009
    :cond_a
    new-instance v12, Lcom/android/exchange/adapter/ResolveRecipientsParser;

    invoke-direct {v12, v10, v14}, Lcom/android/exchange/adapter/ResolveRecipientsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 2010
    .local v12, "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->parse()Z

    move-result v15

    if-eqz v15, :cond_c

    .line 2014
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] return result"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2015
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->getResult()Ljava/util/ArrayList;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v15

    .line 2066
    if-eqz v11, :cond_b

    .line 2067
    :try_start_4
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_4
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 2089
    .end local v5    # "code":I
    .end local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v9    # "i":I
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_b
    :goto_3
    return-object v15

    .line 2018
    .restart local v5    # "code":I
    .restart local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v9    # "i":I
    .restart local v10    # "is":Ljava/io/InputStream;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_c
    :try_start_5
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->getStatus()I

    move-result v15

    const/16 v16, 0x8c

    move/from16 v0, v16

    if-lt v15, v0, :cond_d

    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->getStatus()I

    move-result v15

    const/16 v16, 0x90

    move/from16 v0, v16

    if-gt v15, v0, :cond_d

    .line 2019
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] rrp.getStatus() >= 140 && rrp.getStatus() <= 144. return CommandStatusException"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2021
    new-instance v15, Lcom/android/exchange/CommandStatusException;

    const/16 v16, 0x8e

    invoke-direct/range {v15 .. v16}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v15

    .line 2023
    :cond_d
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->getStatus()I

    move-result v15

    const/16 v16, 0x6

    move/from16 v0, v16

    if-ne v15, v0, :cond_11

    .line 2024
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] rrp.getStatus() == 0x06"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2026
    const-string v15, "ResolveRecipients"

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v11

    .line 2027
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 2029
    const-string v15, "EasSyncService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[doResolveRecipients] rrp HTTP POST status code = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2031
    const/16 v15, 0xc8

    if-ne v5, v15, :cond_12

    .line 2033
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    .line 2034
    if-nez v10, :cond_e

    .line 2035
    const-string v15, "ExchangeService"

    const-string v16, "doResolveRecipients repeat http response has no content"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 2039
    :cond_e
    new-instance v12, Lcom/android/exchange/adapter/ResolveRecipientsParser;

    .end local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    invoke-direct {v12, v10, v14}, Lcom/android/exchange/adapter/ResolveRecipientsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 2040
    .restart local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->parse()Z

    move-result v15

    if-eqz v15, :cond_10

    .line 2044
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] rrp HTTP POST. return result."

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2046
    invoke-virtual {v12}, Lcom/android/exchange/adapter/ResolveRecipientsParser;->getResult()Ljava/util/ArrayList;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v15

    .line 2066
    if-eqz v11, :cond_b

    .line 2067
    :try_start_6
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_6
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_3

    .line 2078
    .end local v5    # "code":I
    .end local v9    # "i":I
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_1
    move-exception v7

    .line 2079
    .local v7, "e":Ljava/io/IOException;
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] Exception caughted"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2081
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 2083
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Resolve Recipients "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2087
    .end local v7    # "e":Ljava/io/IOException;
    .end local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_f
    :goto_4
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] return null"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2089
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 2049
    .restart local v5    # "code":I
    .restart local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v9    # "i":I
    .restart local v10    # "is":Ljava/io/InputStream;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_10
    :try_start_7
    const-string v15, "EasSyncService"

    const-string v16, "[doResolveRecipients] rrp HTTP POST. returned no matches"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2066
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    :cond_11
    :goto_5
    if-eqz v11, :cond_f

    .line 2067
    :try_start_8
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_8
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_4

    .line 2055
    .restart local v10    # "is":Ljava/io/InputStream;
    .restart local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    :cond_12
    const/4 v15, 0x1

    :try_start_9
    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Resolve Recipients returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_5

    .line 2063
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v12    # "rrp":Lcom/android/exchange/adapter/ResolveRecipientsParser;
    :cond_13
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Resolve Recipients returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5
.end method

.method public static doValidateCert(Landroid/content/Context;J[Ljava/lang/String;[Ljava/lang/String;Z)Ljava/util/ArrayList;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "certificates"    # [Ljava/lang/String;
    .param p4, "certificatesChain"    # [Ljava/lang/String;
    .param p5, "isCheckCRL"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1433
    const-string v18, "EasSyncService"

    const-string v19, " [doValidateCert] called"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1436
    .local v4, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v4, :cond_d

    if-eqz p3, :cond_d

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v18, v0

    if-lez v18, :cond_d

    .line 1437
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v10

    .line 1438
    .local v10, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    new-instance v17, Lcom/android/exchange/EasSyncService;

    const-string v18, "%ValidateCert%"

    invoke-direct/range {v17 .. v18}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1440
    .local v17, "svc":Lcom/android/exchange/EasSyncService;
    :try_start_0
    sput-object p0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1441
    iget-object v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1442
    iget-object v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1443
    iget-object v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1444
    iget v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x1

    if-eqz v18, :cond_0

    const/16 v18, 0x1

    :goto_0
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1445
    iget v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    move/from16 v18, v0

    and-int/lit8 v18, v18, 0x8

    if-eqz v18, :cond_1

    const/16 v18, 0x1

    :goto_1
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1446
    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1447
    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1449
    iget v0, v10, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1450
    new-instance v16, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v16 .. v16}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 1451
    .local v16, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v18, 0x2c5

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1452
    if-eqz p4, :cond_3

    move-object/from16 v0, p4

    array-length v0, v0

    move/from16 v18, v0

    if-lez v18, :cond_3

    .line 1453
    const/16 v18, 0x2c8

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1454
    move-object/from16 v5, p4

    .local v5, "arr$":[Ljava/lang/String;
    array-length v13, v5

    .local v13, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_2
    if-ge v11, v13, :cond_2

    aget-object v6, v5, v11

    .line 1455
    .local v6, "cert":Ljava/lang/String;
    const/16 v18, 0x2c7

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1454
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1444
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v6    # "cert":Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v13    # "len$":I
    .end local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_0
    const/16 v18, 0x0

    goto :goto_0

    .line 1445
    :cond_1
    const/16 v18, 0x0

    goto :goto_1

    .line 1456
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v11    # "i$":I
    .restart local v13    # "len$":I
    .restart local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_2
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1458
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v13    # "len$":I
    :cond_3
    const/16 v18, 0x2c6

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1459
    move-object/from16 v5, p3

    .restart local v5    # "arr$":[Ljava/lang/String;
    array-length v13, v5

    .restart local v13    # "len$":I
    const/4 v11, 0x0

    .restart local v11    # "i$":I
    :goto_3
    if-ge v11, v13, :cond_4

    aget-object v6, v5, v11

    .line 1460
    .restart local v6    # "cert":Ljava/lang/String;
    const/16 v18, 0x2c7

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1459
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1461
    .end local v6    # "cert":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1462
    if-eqz p5, :cond_5

    .line 1463
    const/16 v18, 0x2c9

    const-string v19, "1"

    move-object/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1464
    :cond_5
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1465
    const-string v18, "ValidateCert"

    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    :try_end_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 1468
    .local v15, "resp":Lcom/android/exchange/EasResponse;
    :try_start_1
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v7

    .line 1470
    .local v7, "code":I
    const-string v18, "EasSyncService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[doValidateCert] Http Status code for ValidateCert: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    const/16 v18, 0xc8

    move/from16 v0, v18

    if-ne v7, v0, :cond_a

    .line 1473
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 1474
    .local v12, "is":Ljava/io/InputStream;
    new-instance v14, Lcom/android/exchange/adapter/ValidateCertParser;

    invoke-direct {v14, v12}, Lcom/android/exchange/adapter/ValidateCertParser;-><init>(Ljava/io/InputStream;)V

    .line 1475
    .local v14, "parser":Lcom/android/exchange/adapter/ValidateCertParser;
    invoke-virtual {v14}, Lcom/android/exchange/adapter/ValidateCertParser;->parse()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1476
    invoke-virtual {v14}, Lcom/android/exchange/adapter/ValidateCertParser;->getResult()Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v18

    .line 1509
    if-eqz v15, :cond_6

    .line 1510
    :try_start_2
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_2
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1529
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v7    # "code":I
    .end local v10    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v11    # "i$":I
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len$":I
    .end local v14    # "parser":Lcom/android/exchange/adapter/ValidateCertParser;
    .end local v15    # "resp":Lcom/android/exchange/EasResponse;
    .end local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v17    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_6
    :goto_4
    return-object v18

    .line 1478
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v7    # "code":I
    .restart local v10    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v11    # "i$":I
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len$":I
    .restart local v14    # "parser":Lcom/android/exchange/adapter/ValidateCertParser;
    .restart local v15    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v17    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_7
    :try_start_3
    const-string v18, "EasSyncService"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[doValidateCert] ValidateCert request is not successful. Status: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1509
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v14    # "parser":Lcom/android/exchange/adapter/ValidateCertParser;
    :cond_8
    if-eqz v15, :cond_9

    .line 1510
    :try_start_4
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_4
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 1529
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v7    # "code":I
    .end local v10    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v11    # "i$":I
    .end local v13    # "len$":I
    .end local v15    # "resp":Lcom/android/exchange/EasResponse;
    .end local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v17    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_9
    :goto_5
    const/16 v18, 0x0

    goto :goto_4

    .line 1500
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v7    # "code":I
    .restart local v10    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v11    # "i$":I
    .restart local v13    # "len$":I
    .restart local v15    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v17    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_a
    const/16 v18, 0x1c1

    move/from16 v0, v18

    if-eq v7, v0, :cond_b

    const/16 v18, 0x193

    move/from16 v0, v18

    if-ne v7, v0, :cond_8

    .line 1501
    :cond_b
    const/16 v18, 0x1

    :try_start_5
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v18

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1502
    new-instance v18, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v19, 0x7

    invoke-direct/range {v18 .. v19}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v18
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1509
    .end local v7    # "code":I
    :catchall_0
    move-exception v18

    if-eqz v15, :cond_c

    .line 1510
    :try_start_6
    invoke-virtual {v15}, Lcom/android/exchange/EasResponse;->close()V

    :cond_c
    throw v18
    :try_end_6
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 1514
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v13    # "len$":I
    .end local v15    # "resp":Lcom/android/exchange/EasResponse;
    .end local v16    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v8

    .line 1515
    .local v8, "cse":Lcom/android/exchange/CommandStatusException;
    invoke-virtual {v8}, Lcom/android/exchange/CommandStatusException;->printStackTrace()V

    .line 1516
    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move/from16 v3, v18

    invoke-static {v0, v1, v2, v3}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 1517
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[doValidateCert] CommandStatusException during ValidateCert: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1518
    new-instance v18, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v19, 0x7

    invoke-direct/range {v18 .. v19}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v18

    .line 1521
    .end local v8    # "cse":Lcom/android/exchange/CommandStatusException;
    :catch_1
    move-exception v9

    .line 1522
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 1523
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "[doValidateCert] IOException during ValidateCert: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1526
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v17    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_d
    const-string v18, "EasSyncService"

    const-string v19, "[doValidateCert] acct == null || certificates == null || certificates.length <= 0"

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 2

    .prologue
    .line 3141
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    iget v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->getClientConnectionManager(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method private getClientConnectionManagerForLoadMore()Lorg/apache/http/conn/ClientConnectionManager;
    .locals 2

    .prologue
    .line 3149
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    iget v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    invoke-static {v0, v1}, Lcom/android/exchange/ExchangeService;->getClientConnectionManagerForLoadMore(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method private getClientConnectionManagerForSendMessage(J)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 3
    .param p1, "accountId"    # J

    .prologue
    .line 3155
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    iget v1, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    invoke-static {p1, p2, v0, v1}, Lcom/android/exchange/ExchangeService;->getClientConnectionManagerForSendMessage(JZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultRedirectHandler()Lorg/apache/http/impl/client/DefaultRedirectHandler;
    .locals 1

    .prologue
    .line 3461
    new-instance v0, Lcom/android/exchange/EasSyncService$4;

    invoke-direct {v0}, Lcom/android/exchange/EasSyncService$4;-><init>()V

    return-object v0
.end method

.method private static getDeviceType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 5887
    sget-object v2, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 5889
    sget-object v2, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    const-string v3, "[^\uac00-\ud7a3xfe0-9a-zA-Z\\s]"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "\\p{Space}"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 5892
    .local v0, "modelName":Ljava/lang/String;
    sget-object v2, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 5893
    .local v1, "upper":Ljava/lang/String;
    const-string v2, "SAMSUNG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5894
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SAMSUNG"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    .line 5898
    :goto_0
    const-string v2, "EasSynService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create and return deviceType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5899
    sget-object v2, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    .line 5904
    .end local v0    # "modelName":Ljava/lang/String;
    .end local v1    # "upper":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 5896
    .restart local v0    # "modelName":Ljava/lang/String;
    .restart local v1    # "upper":Ljava/lang/String;
    :cond_0
    sput-object v0, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    goto :goto_0

    .line 5902
    .end local v0    # "modelName":Ljava/lang/String;
    .end local v1    # "upper":Ljava/lang/String;
    :cond_1
    const-string v2, "EasSynService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDeviceType() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5904
    sget-object v2, Lcom/android/exchange/EasSyncService;->deviceType:Ljava/lang/String;

    goto :goto_1
.end method

.method private getHttpClient(I)Lorg/apache/http/client/HttpClient;
    .locals 7
    .param p1, "timeout"    # I

    .prologue
    .line 3267
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3269
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3271
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3273
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 3277
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 3284
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3285
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3287
    .local v2, "proxyHost":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 3289
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 3291
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 3293
    const-string v4, "PROXY"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3299
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {p0}, Lcom/android/exchange/EasSyncService;->getClientConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 3300
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v4, Lcom/android/exchange/EasSyncService$1;

    invoke-direct {v4, p0}, Lcom/android/exchange/EasSyncService$1;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    .line 3308
    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDefaultRedirectHandler()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 3313
    return-object v0
.end method

.method private getLoadMoreClient(I)Lorg/apache/http/client/HttpClient;
    .locals 7
    .param p1, "timeout"    # I

    .prologue
    .line 3321
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3323
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3325
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3327
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 3331
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 3338
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3339
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3341
    .local v2, "proxyHost":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 3343
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 3345
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 3347
    const-string v4, "PROXY"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3354
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {p0}, Lcom/android/exchange/EasSyncService;->getClientConnectionManagerForLoadMore()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 3355
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v4, Lcom/android/exchange/EasSyncService$2;

    invoke-direct {v4, p0}, Lcom/android/exchange/EasSyncService$2;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    .line 3363
    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDefaultRedirectHandler()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 3366
    const-string v4, "Email"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getClientConnectionManagerForLoadMore : client : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3370
    return-object v0
.end method

.method private getPingClient(I)Lorg/apache/http/client/HttpClient;
    .locals 7
    .param p1, "timeout"    # I

    .prologue
    .line 3221
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3223
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3225
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3227
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 3229
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 3236
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3237
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3239
    .local v2, "proxyHost":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 3241
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 3243
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 3245
    const-string v4, "PROXY"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3254
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    iget-boolean v4, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    iget v5, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->newClientConnectionManager(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 3255
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDefaultRedirectHandler()Lorg/apache/http/impl/client/DefaultRedirectHandler;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 3256
    return-object v0
.end method

.method private static getPolicyType(Lcom/android/exchange/EasSyncService;)Ljava/lang/String;
    .locals 6
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;

    .prologue
    .line 4551
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 4553
    .local v0, "protocolVersionDouble":Ljava/lang/Double;
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/high16 v4, 0x4028000000000000L    # 12.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    const-string v1, "MS-EAS-Provisioning-WBXML"

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "MS-WAP-Provisioning-XML"

    goto :goto_0
.end method

.method private getSendMessageClient(IJ)Lorg/apache/http/client/HttpClient;
    .locals 8
    .param p1, "timeout"    # I
    .param p2, "accountId"    # J

    .prologue
    .line 3378
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 3380
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3382
    invoke-static {v1, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 3384
    const/16 v4, 0x2000

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 3388
    sget v4, Lcom/android/exchange/EasSyncService;->CONNECTION_TIMEOUT:I

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    .line 3395
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Email_EasDoNotUseProxy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 3396
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 3398
    .local v2, "proxyHost":Ljava/lang/String;
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    .line 3400
    .local v3, "proxyPort":I
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-ltz v3, :cond_0

    .line 3402
    new-instance v4, Lorg/apache/http/HttpHost;

    invoke-direct {v4, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    invoke-static {v1, v4}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 3404
    const-string v4, "PROXY"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Added proxy param host: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " port: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3410
    .end local v2    # "proxyHost":Ljava/lang/String;
    .end local v3    # "proxyPort":I
    :cond_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {p0, p2, p3}, Lcom/android/exchange/EasSyncService;->getClientConnectionManagerForSendMessage(J)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 3411
    .local v0, "client":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v4, Lcom/android/exchange/EasSyncService$3;

    invoke-direct {v4, p0}, Lcom/android/exchange/EasSyncService$3;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-virtual {v0, v4}, Lorg/apache/http/impl/client/DefaultHttpClient;->setKeepAliveStrategy(Lorg/apache/http/conn/ConnectionKeepAliveStrategy;)V

    .line 3420
    const-string v4, "Email"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSendMessageClient : client : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3424
    return-object v0
.end method

.method public static getServiceForMailbox(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)Lcom/android/exchange/EasSyncService;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "m"    # Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .prologue
    .line 545
    iget v0, p1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 549
    new-instance v0, Lcom/android/exchange/EasSyncService;

    invoke-direct {v0, p0, p1}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    :goto_0
    return-object v0

    .line 547
    :pswitch_0
    new-instance v0, Lcom/android/exchange/EasAccountService;

    invoke-direct {v0, p0, p1}, Lcom/android/exchange/EasAccountService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Mailbox;)V

    goto :goto_0

    .line 545
    nop

    :pswitch_data_0
    .packed-switch 0x44
        :pswitch_0
    .end packed-switch
.end method

.method public static getUserAgent()Ljava/lang/String;
    .locals 4

    .prologue
    .line 5909
    sget-object v2, Lcom/android/exchange/EasSyncService;->userAgent:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 5911
    sget-object v2, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 5914
    .local v1, "upper":Ljava/lang/String;
    const-string v2, "SAMSUNG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5915
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SAMSUNG-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5919
    .local v0, "deviceTypeForUserAgent":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Android-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "101"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/emailcommon/EasRefs;->PLATFORM_VERSION:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/exchange/EasSyncService;->userAgent:Ljava/lang/String;

    .line 5921
    sget-object v2, Lcom/android/exchange/EasSyncService;->userAgent:Ljava/lang/String;

    .line 5925
    .end local v0    # "deviceTypeForUserAgent":Ljava/lang/String;
    .end local v1    # "upper":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 5917
    .restart local v1    # "upper":Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/android/exchange/EasSyncService;->modelNameForEAS:Ljava/lang/String;

    .restart local v0    # "deviceTypeForUserAgent":Ljava/lang/String;
    goto :goto_0

    .line 5923
    .end local v0    # "deviceTypeForUserAgent":Ljava/lang/String;
    .end local v1    # "upper":Ljava/lang/String;
    :cond_1
    const-string v2, "EasSynService"

    const-string v3, "getUserAgent()"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5925
    sget-object v2, Lcom/android/exchange/EasSyncService;->userAgent:Ljava/lang/String;

    goto :goto_1
.end method

.method public static searchDocument(Landroid/content/Context;JJJLcom/android/exchange/SearchRequest;)Lcom/android/exchange/provider/EmailResult;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxId"    # J
    .param p5, "foldId"    # J
    .param p7, "request"    # Lcom/android/exchange/SearchRequest;

    .prologue
    .line 1536
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1539
    .local v4, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    new-instance v7, Lcom/android/exchange/provider/EmailResult;

    invoke-direct {v7}, Lcom/android/exchange/provider/EmailResult;-><init>()V

    .line 1540
    .local v7, "error":Lcom/android/exchange/provider/EmailResult;
    const/16 v15, 0xa

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 1541
    const/4 v15, 0x0

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->startRange:I

    .line 1542
    const/4 v15, 0x0

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->endRange:I

    .line 1543
    const/4 v15, 0x0

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->total:I

    .line 1545
    if-eqz v4, :cond_0

    .line 1546
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v8

    .line 1547
    .local v8, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-eqz v8, :cond_9

    .line 1548
    new-instance v14, Lcom/android/exchange/EasSyncService;

    const-string v15, "%EmailDocSearch%"

    invoke-direct {v14, v15}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1550
    .local v14, "svc":Lcom/android/exchange/EasSyncService;
    :try_start_0
    sput-object p0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1551
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1552
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1553
    iget-object v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1554
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x1

    if-eqz v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1555
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x8

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    :goto_1
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1556
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1557
    iput-object v4, v14, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1558
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v15

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 1560
    iget v15, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v15, v14, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1561
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "searchDocument: mailboxID = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1562
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v4, v1}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->buildEasDocSearchRequest(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    .line 1565
    .local v13, "s":Lcom/android/exchange/adapter/Serializer;
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Start Doc Search "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v8, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1566
    const-string v15, "Search"

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v11

    .line 1567
    .local v11, "resp":Lcom/android/exchange/EasResponse;
    if-nez v11, :cond_3

    .line 1568
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "searchDocument: sendHttpClientPost returns null"

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1620
    .end local v7    # "error":Lcom/android/exchange/provider/EmailResult;
    .end local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_0
    :goto_2
    return-object v7

    .line 1554
    .restart local v7    # "error":Lcom/android/exchange/provider/EmailResult;
    .restart local v8    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_1
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1555
    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    .line 1572
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_3
    :try_start_1
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 1573
    .local v5, "code":I
    const/16 v15, 0xc8

    if-ne v5, v15, :cond_8

    .line 1575
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 1576
    .local v9, "is":Ljava/io/InputStream;
    if-nez v9, :cond_4

    .line 1577
    const-string v15, "ExchangeService"

    const-string v16, "searchDocument http response has no content"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    const/4 v15, 0x7

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->result:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1606
    if-eqz v11, :cond_0

    .line 1607
    :try_start_2
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1609
    .end local v5    # "code":I
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v6

    .line 1611
    .local v6, "e":Ljava/io/IOException;
    const-string v15, "EasSyncService"

    invoke-virtual {v14, v15, v6}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1612
    const/4 v15, 0x7

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->result:I

    goto :goto_2

    .line 1584
    .end local v6    # "e":Ljava/io/IOException;
    .restart local v5    # "code":I
    .restart local v9    # "is":Ljava/io/InputStream;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_4
    const/4 v12, 0x0

    .line 1585
    .local v12, "result":Lcom/android/exchange/provider/EmailResult;
    if-eqz v9, :cond_5

    .line 1586
    :try_start_3
    new-instance v10, Lcom/android/exchange/adapter/EasDocSearchParser;

    new-instance v15, Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-direct {v15, v14}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-direct {v10, v9, v15}, Lcom/android/exchange/adapter/EasDocSearchParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 1588
    .local v10, "parse":Lcom/android/exchange/adapter/EasDocSearchParser;
    move-wide/from16 v0, p5

    invoke-virtual {v10, v0, v1}, Lcom/android/exchange/adapter/EasDocSearchParser;->setFoldId(J)V

    .line 1590
    const-string v15, "ExchangeService"

    const-string v16, "searchDocument - parsing response"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    invoke-virtual {v10}, Lcom/android/exchange/adapter/EasDocSearchParser;->parse_doc_response()Lcom/android/exchange/provider/EmailResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    .line 1595
    .end local v10    # "parse":Lcom/android/exchange/adapter/EasDocSearchParser;
    :cond_5
    const-wide/16 v16, 0x7d0

    :try_start_4
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1600
    :goto_3
    :try_start_5
    const-string v15, "ExchangeService"

    const-string v16, "searchDocument - parsing done"

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1606
    if-eqz v11, :cond_6

    .line 1607
    :try_start_6
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    :cond_6
    move-object v7, v12

    goto :goto_2

    .line 1596
    :catch_1
    move-exception v6

    .line 1598
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_7
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 1606
    .end local v5    # "code":I
    .end local v6    # "e":Ljava/lang/InterruptedException;
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v12    # "result":Lcom/android/exchange/provider/EmailResult;
    :catchall_0
    move-exception v15

    if-eqz v11, :cond_7

    .line 1607
    :try_start_8
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    :cond_7
    throw v15
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    .line 1603
    .restart local v5    # "code":I
    :cond_8
    const/4 v15, 0x1

    :try_start_9
    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Search Email returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1606
    if-eqz v11, :cond_0

    .line 1607
    :try_start_a
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_2

    .line 1615
    .end local v5    # "code":I
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_9
    const-string v15, "searchDocument: ha is null"

    invoke-static {v15}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 1616
    const/4 v15, 0x5

    iput v15, v7, Lcom/android/exchange/provider/EmailResult;->result:I

    goto/16 :goto_2
.end method

.method public static searchEmail(Landroid/content/Context;JJJLcom/android/exchange/SearchRequest;)Lcom/android/exchange/provider/EmailResult;
    .locals 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxId"    # J
    .param p5, "foldId"    # J
    .param p7, "request"    # Lcom/android/exchange/SearchRequest;

    .prologue
    .line 1365
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1367
    .local v4, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v4, :cond_4

    .line 1368
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v7

    .line 1369
    .local v7, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    new-instance v14, Lcom/android/exchange/EasSyncService;

    const-string v15, "%EmailSearch%"

    invoke-direct {v14, v15}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1371
    .local v14, "svc":Lcom/android/exchange/EasSyncService;
    const/4 v11, 0x0

    .line 1373
    .local v11, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    sput-object p0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1374
    iget-object v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1375
    iget-object v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1376
    iget-object v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1377
    iget v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x1

    if-eqz v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1378
    iget v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v15, v15, 0x8

    if-eqz v15, :cond_2

    const/4 v15, 0x1

    :goto_1
    iput-boolean v15, v14, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1379
    const/4 v15, 0x0

    invoke-static {v15}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1380
    iput-object v4, v14, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1381
    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v15

    iput-object v15, v14, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    .line 1382
    iget v15, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v15, v14, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1383
    const-string v15, "EasSyncService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "fzhang search mailboxID = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v4, v1}, Lcom/android/exchange/adapter/EasEmailSearchAdapter;->buildEasEmailSearchRequest(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Lcom/android/exchange/SearchRequest;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v13

    .line 1387
    .local v13, "s":Lcom/android/exchange/adapter/Serializer;
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Start Email Search "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    iget-object v0, v7, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1388
    const-string v15, "Search"

    invoke-virtual {v13}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v11

    .line 1389
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v5

    .line 1390
    .local v5, "code":I
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 1391
    .local v8, "is":Ljava/io/InputStream;
    new-instance v9, Lcom/android/exchange/adapter/EasEmailSearchParser;

    new-instance v15, Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-direct {v15, v14}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    invoke-direct {v9, v8, v15}, Lcom/android/exchange/adapter/EasEmailSearchParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 1393
    .local v9, "parse":Lcom/android/exchange/adapter/EasEmailSearchParser;
    const/16 v15, 0xc8

    if-ne v5, v15, :cond_3

    .line 1394
    invoke-virtual {v9}, Lcom/android/exchange/adapter/EasEmailSearchParser;->parse_email_response()Lcom/android/exchange/provider/EmailResult;
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 1412
    .local v10, "res":Lcom/android/exchange/provider/EmailResult;
    if-eqz v11, :cond_0

    .line 1413
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    .line 1424
    .end local v5    # "code":I
    .end local v7    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "parse":Lcom/android/exchange/adapter/EasEmailSearchParser;
    .end local v10    # "res":Lcom/android/exchange/provider/EmailResult;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_0
    :goto_2
    return-object v10

    .line 1377
    .restart local v7    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_1
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 1378
    :cond_2
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 1397
    .restart local v5    # "code":I
    .restart local v8    # "is":Ljava/io/InputStream;
    .restart local v9    # "parse":Lcom/android/exchange/adapter/EasEmailSearchParser;
    .restart local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_3
    :try_start_1
    const-string v15, "EasSyncService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "fzhang search response is not mormal, return code = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Search Email returned "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1412
    if-eqz v11, :cond_4

    .line 1413
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    .line 1418
    .end local v5    # "code":I
    .end local v7    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v9    # "parse":Lcom/android/exchange/adapter/EasEmailSearchParser;
    .end local v11    # "resp":Lcom/android/exchange/EasResponse;
    .end local v13    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_4
    :goto_3
    new-instance v10, Lcom/android/exchange/provider/EmailResult;

    invoke-direct {v10}, Lcom/android/exchange/provider/EmailResult;-><init>()V

    .line 1419
    .restart local v10    # "res":Lcom/android/exchange/provider/EmailResult;
    const/16 v15, 0xa

    iput v15, v10, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 1420
    const/4 v15, 0x0

    iput v15, v10, Lcom/android/exchange/provider/EmailResult;->startRange:I

    .line 1421
    const/4 v15, 0x0

    iput v15, v10, Lcom/android/exchange/provider/EmailResult;->endRange:I

    .line 1422
    const/4 v15, 0x0

    iput v15, v10, Lcom/android/exchange/provider/EmailResult;->total:I

    goto :goto_2

    .line 1401
    .end local v10    # "res":Lcom/android/exchange/provider/EmailResult;
    .restart local v7    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v11    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v14    # "svc":Lcom/android/exchange/EasSyncService;
    :catch_0
    move-exception v6

    .line 1402
    .local v6, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_2
    new-instance v12, Lcom/android/exchange/provider/EmailResult;

    invoke-direct {v12}, Lcom/android/exchange/provider/EmailResult;-><init>()V

    .line 1403
    .local v12, "result":Lcom/android/exchange/provider/EmailResult;
    const/16 v15, 0xd

    iput v15, v12, Lcom/android/exchange/provider/EmailResult;->result:I

    .line 1404
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v16, v0

    sget v15, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v15}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1412
    if-eqz v11, :cond_5

    .line 1413
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    :cond_5
    move-object v10, v12

    goto :goto_2

    .line 1407
    .end local v6    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .end local v12    # "result":Lcom/android/exchange/provider/EmailResult;
    :catch_1
    move-exception v6

    .line 1409
    .local v6, "e":Ljava/io/IOException;
    :try_start_3
    const-string v15, "EasSyncService"

    const-string v16, "fzhang search fail "

    invoke-static/range {v15 .. v16}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "EasSyncService"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v14, v15}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1412
    if-eqz v11, :cond_4

    .line 1413
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_3

    .line 1412
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v15

    if-eqz v11, :cond_6

    .line 1413
    invoke-virtual {v11}, Lcom/android/exchange/EasResponse;->close()V

    :cond_6
    throw v15
.end method

.method public static searchGal(Landroid/content/Context;JLjava/lang/String;II)Lcom/android/exchange/provider/GalResult;
    .locals 27
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "startRange"    # I
    .param p5, "endRange"    # I

    .prologue
    .line 1775
    invoke-static/range {p1 .. p2}, Lcom/android/exchange/ExchangeService;->getAccountById(J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1776
    .local v4, "acct":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v4, :cond_0

    .line 1779
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v4

    .line 1781
    :cond_0
    if-eqz v4, :cond_e

    .line 1783
    iget v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x20

    if-eqz v22, :cond_1

    .line 1784
    const/16 v22, 0x0

    .line 1910
    :goto_0
    return-object v22

    .line 1786
    :cond_1
    const-string v22, "user"

    sget-object v23, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " queryLength="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1787
    .local v15, "query":Ljava/lang/String;
    :goto_1
    iget-wide v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v11

    .line 1788
    .local v11, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    new-instance v19, Lcom/android/exchange/EasSyncService;

    const-string v22, "%GalLookupk%"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1793
    .local v19, "svc":Lcom/android/exchange/EasSyncService;
    :try_start_0
    iget-object v14, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1794
    .local v14, "protocolVersion":Ljava/lang/String;
    if-nez v14, :cond_3

    .line 1795
    const/16 v22, 0x0

    .line 1906
    invoke-static/range {v19 .. v19}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    goto :goto_0

    .line 1786
    .end local v11    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v14    # "protocolVersion":Ljava/lang/String;
    .end local v15    # "query":Ljava/lang/String;
    .end local v19    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_2
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " query="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_1

    .line 1797
    .restart local v11    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v14    # "protocolVersion":Ljava/lang/String;
    .restart local v15    # "query":Ljava/lang/String;
    .restart local v19    # "svc":Lcom/android/exchange/EasSyncService;
    :cond_3
    :try_start_1
    move-object/from16 v0, v19

    iput-object v14, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 1798
    invoke-static {v14}, Lcom/android/emailcommon/EasRefs;->getProtocolVersionDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 1800
    sput-object p0, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1801
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1802
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1803
    iget-object v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1804
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x1

    if-eqz v22, :cond_9

    const/16 v22, 0x1

    :goto_2
    move/from16 v0, v22

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1805
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x8

    if-eqz v22, :cond_a

    const/16 v22, 0x1

    :goto_3
    move/from16 v0, v22

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1806
    invoke-static/range {p0 .. p0}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1807
    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1809
    sget-wide v22, Lcom/android/exchange/ExchangeService;->MAILBOX_DUMMY_GALSEARCH:J

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    .line 1811
    iget v0, v11, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1812
    new-instance v18, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 1813
    .local v18, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v22, 0x3c5

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    const/16 v23, 0x3c7

    invoke-virtual/range {v22 .. v23}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1814
    const/16 v22, 0x3c8

    const-string v23, "GAL"

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    const/16 v23, 0x3c9

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1815
    const/16 v22, 0x3ca

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1817
    const/16 v16, 0x0

    .line 1821
    .local v16, "requestSize":I
    if-ltz p4, :cond_4

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    const-wide/high16 v24, 0x4028000000000000L    # 12.0

    cmpg-double v22, v22, v24

    if-gez v22, :cond_b

    .line 1823
    :cond_4
    const/16 v22, 0x3cb

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "0-"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    add-int/lit8 v24, p5, -0x1

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1824
    move/from16 v16, p5

    .line 1833
    :goto_4
    iget-object v0, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v22

    const-wide v24, 0x402c333333333333L    # 14.1

    cmpl-double v22, v22, v24

    if-ltz v22, :cond_5

    .line 1834
    const/16 v22, 0x3e1

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 1835
    const/16 v22, 0x3e2

    const-string v23, "102400"

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1838
    const/16 v22, 0x3e3

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 1839
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 1842
    :cond_5
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1846
    sget-boolean v22, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v22, :cond_6

    .line 1847
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const-string v24, "searchGal(): Wbxml:"

    aput-object v24, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1848
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    .line 1849
    .local v5, "b":[B
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1850
    .local v6, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v13, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v19

    invoke-direct {v13, v0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 1851
    .local v13, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v13, v6}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1854
    .end local v5    # "b":[B
    .end local v6    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v13    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_6
    const/16 v17, 0x0

    .line 1856
    .local v17, "resp":Lcom/android/exchange/EasResponse;
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 1857
    .local v20, "timeMs":J
    const-string v22, "Search"

    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v17

    .line 1859
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v7

    .line 1862
    .local v7, "code":I
    sget-boolean v22, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v22, :cond_7

    .line 1863
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "searchGal() Search command response code:"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1866
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    sub-long v20, v22, v20

    .line 1867
    const/16 v22, 0xc8

    move/from16 v0, v22

    if-ne v7, v0, :cond_f

    .line 1870
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, v19

    iput v0, v1, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 1872
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 1873
    .local v12, "is":Ljava/io/InputStream;
    new-instance v10, Lcom/android/exchange/adapter/GalParser;

    move-object/from16 v0, v19

    invoke-direct {v10, v12, v0}, Lcom/android/exchange/adapter/GalParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 1874
    .local v10, "gp":Lcom/android/exchange/adapter/GalParser;
    invoke-virtual {v10}, Lcom/android/exchange/adapter/GalParser;->parse()Z

    move-result v22

    if-eqz v22, :cond_c

    .line 1877
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "thread="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " matched="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v10}, Lcom/android/exchange/adapter/GalParser;->getGalResult()Lcom/android/exchange/provider/GalResult;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/android/exchange/provider/GalResult;->total:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mAccountId="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " res="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " responseTime="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "ms"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1880
    .local v9, "galLog":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/exchange/ServiceLogger;->logGALSearchStats(Ljava/lang/String;)V

    .line 1881
    invoke-virtual {v10}, Lcom/android/exchange/adapter/GalParser;->getGalResult()Lcom/android/exchange/provider/GalResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v22

    .line 1894
    if-eqz v17, :cond_8

    .line 1895
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1906
    :cond_8
    invoke-static/range {v19 .. v19}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    goto/16 :goto_0

    .line 1804
    .end local v7    # "code":I
    .end local v9    # "galLog":Ljava/lang/String;
    .end local v10    # "gp":Lcom/android/exchange/adapter/GalParser;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v16    # "requestSize":I
    .end local v17    # "resp":Lcom/android/exchange/EasResponse;
    .end local v18    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v20    # "timeMs":J
    :cond_9
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 1805
    :cond_a
    const/16 v22, 0x0

    goto/16 :goto_3

    .line 1826
    .restart local v16    # "requestSize":I
    .restart local v18    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_b
    const/16 v22, 0x3cb

    :try_start_4
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, "-"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    add-int/lit8 v24, p5, -0x1

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v18

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1828
    sub-int v16, p5, p4

    goto/16 :goto_4

    .line 1885
    .restart local v7    # "code":I
    .restart local v10    # "gp":Lcom/android/exchange/adapter/GalParser;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v17    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v20    # "timeMs":J
    :cond_c
    :try_start_5
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "thread="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " matched=0"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mAccountId="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " res="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " responseTime="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "ms"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1888
    .restart local v9    # "galLog":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/exchange/ServiceLogger;->logGALSearchStats(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1894
    .end local v9    # "galLog":Ljava/lang/String;
    .end local v10    # "gp":Lcom/android/exchange/adapter/GalParser;
    .end local v12    # "is":Ljava/io/InputStream;
    :goto_5
    if-eqz v17, :cond_d

    .line 1895
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1906
    :cond_d
    invoke-static/range {v19 .. v19}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 1910
    .end local v7    # "code":I
    .end local v11    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .end local v14    # "protocolVersion":Ljava/lang/String;
    .end local v15    # "query":Ljava/lang/String;
    .end local v16    # "requestSize":I
    .end local v17    # "resp":Lcom/android/exchange/EasResponse;
    .end local v18    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v19    # "svc":Lcom/android/exchange/EasSyncService;
    .end local v20    # "timeMs":J
    :cond_e
    :goto_6
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 1891
    .restart local v7    # "code":I
    .restart local v11    # "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .restart local v14    # "protocolVersion":Ljava/lang/String;
    .restart local v15    # "query":Ljava/lang/String;
    .restart local v16    # "requestSize":I
    .restart local v17    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v18    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v19    # "svc":Lcom/android/exchange/EasSyncService;
    .restart local v20    # "timeMs":J
    :cond_f
    const/16 v22, 0x1

    :try_start_7
    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "GAL lookup returned "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_5

    .line 1894
    .end local v7    # "code":I
    .end local v20    # "timeMs":J
    :catchall_0
    move-exception v22

    if-eqz v17, :cond_10

    .line 1895
    :try_start_8
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->close()V

    :cond_10
    throw v22
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1897
    .end local v14    # "protocolVersion":Ljava/lang/String;
    .end local v16    # "requestSize":I
    .end local v17    # "resp":Lcom/android/exchange/EasResponse;
    .end local v18    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v8

    .line 1899
    .local v8, "e":Ljava/lang/Exception;
    const/16 v22, 0x1

    :try_start_9
    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "GAL lookup exception "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1900
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "thread="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mAccountId="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " exception="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1902
    .restart local v9    # "galLog":Ljava/lang/String;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Lcom/android/exchange/ServiceLogger;->logGALSearchStats(Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1906
    invoke-static/range {v19 .. v19}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    goto/16 :goto_6

    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "galLog":Ljava/lang/String;
    :catchall_1
    move-exception v22

    invoke-static/range {v19 .. v19}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    throw v22
.end method

.method private sendBroadcastSyncCompleted(Landroid/content/Context;J)V
    .locals 6
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "accountId"    # J

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 5959
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.socialhub.action.EXTERNAL_DB_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5960
    .local v0, "tIntent":Landroid/content/Intent;
    const-string v1, "action"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5961
    const-string v1, "id_array"

    new-array v2, v4, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 5962
    const-string v1, "intentType"

    const/16 v2, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5963
    const-string v1, "status"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5964
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 5965
    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "sendBroadcastSyncCompleted(): send intent to SocialHub "

    aput-object v2, v1, v5

    invoke-virtual {p0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5966
    return-void
.end method

.method public static sendDeviceInfomationProvision(Lcom/android/exchange/EasSyncService;)Lcom/android/exchange/adapter/Serializer;
    .locals 6
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 5933
    sget-object v1, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 5934
    .local v1, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 5936
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v1, :cond_0

    .line 5937
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Context cannot be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 5940
    :cond_0
    new-instance v3, Lcom/android/exchange/DeviceInformation;

    sget-wide v4, Lcom/android/exchange/EasSyncService;->protocolVersion:D

    invoke-direct {v3, v4, v5}, Lcom/android/exchange/DeviceInformation;-><init>(D)V

    .line 5943
    .local v3, "mDInfo":Lcom/android/exchange/DeviceInformation;
    :try_start_0
    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4, v0}, Lcom/android/exchange/DeviceInformation;->prepareDeviceInformation(Landroid/content/Context;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5949
    :goto_0
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/exchange/DeviceInformation;->buildCommand(Z)Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    return-object v4

    .line 5944
    :catch_0
    move-exception v2

    .line 5945
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private sendMeetingResponseMail(Lcom/android/emailcommon/provider/EmailContent$Message;I)V
    .locals 18
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "response"    # I

    .prologue
    .line 2384
    new-instance v10, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    invoke-direct {v10, v14}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 2386
    .local v10, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    if-nez v10, :cond_1

    .line 2517
    :cond_0
    :goto_0
    return-void

    .line 2398
    :cond_1
    const-string v14, "ORGMAIL"

    invoke-virtual {v10, v14}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v2

    .line 2402
    .local v2, "addrs":[Lcom/android/emailcommon/mail/Address;
    array-length v14, v2

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 2406
    const/4 v14, 0x0

    aget-object v14, v2, v14

    invoke-virtual {v14}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v11

    .line 2408
    .local v11, "organizerEmail":Ljava/lang/String;
    const-string v14, "DTSTAMP"

    invoke-virtual {v10, v14}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2410
    .local v5, "dtStamp":Ljava/lang/String;
    const-string v14, "DTSTART"

    invoke-virtual {v10, v14}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2412
    .local v6, "dtStart":Ljava/lang/String;
    const-string v14, "DTEND"

    invoke-virtual {v10, v14}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2414
    .local v4, "dtEnd":Ljava/lang/String;
    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    if-eqz v4, :cond_0

    .line 2425
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 2427
    .local v8, "entityValues":Landroid/content/ContentValues;
    new-instance v7, Landroid/content/Entity;

    invoke-direct {v7, v8}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    .line 2432
    .local v7, "entity":Landroid/content/Entity;
    const-string v14, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2435
    const-string v14, "dtstart"

    invoke-static {v6}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2437
    const-string v14, "dtend"

    invoke-static {v4}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2439
    const-string v14, "eventLocation"

    const-string v15, "LOC"

    invoke-virtual {v10, v15}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441
    const-string v14, "title"

    const-string v15, "TITLE"

    invoke-virtual {v10, v15}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443
    const-string v14, "organizer"

    invoke-virtual {v8, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2447
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 2449
    .local v3, "attendeeValues":Landroid/content/ContentValues;
    const-string v14, "attendeeRelationship"

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2451
    const-string v14, "attendeeEmail"

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v15, v15, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v3, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2453
    sget-object v14, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v14, v3}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 2457
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 2459
    .local v12, "organizerValues":Landroid/content/ContentValues;
    const-string v14, "attendeeRelationship"

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v12, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2461
    const-string v14, "attendeeEmail"

    invoke-virtual {v12, v14, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2463
    sget-object v14, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v7, v14, v12}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 2479
    and-int/lit8 v14, p2, 0x3

    packed-switch v14, :pswitch_data_0

    .line 2497
    :pswitch_0
    const/16 v9, 0x100

    .line 2503
    .local v9, "flag":I
    :goto_1
    sget-object v14, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const-string v15, "UID"

    invoke-virtual {v10, v15}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v14, v7, v9, v15, v0}, Lcom/android/exchange/utility/CalendarUtilities;->createMessageForEntity(Landroid/content/Context;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v13

    .line 2511
    .local v13, "outgoingMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v13, :cond_0

    .line 2513
    sget-object v14, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v0, v15, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1, v13}, Lcom/android/exchange/EasOutboxService;->sendMessage(Landroid/content/Context;JLcom/android/emailcommon/provider/EmailContent$Message;)V

    goto/16 :goto_0

    .line 2483
    .end local v9    # "flag":I
    .end local v13    # "outgoingMsg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :pswitch_1
    const/16 v9, 0x40

    .line 2485
    .restart local v9    # "flag":I
    goto :goto_1

    .line 2489
    .end local v9    # "flag":I
    :pswitch_2
    const/16 v9, 0x80

    .line 2491
    .restart local v9    # "flag":I
    goto :goto_1

    .line 2479
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private sendMeetingResponseMail(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Message;I)V
    .locals 15
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "draft"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p3, "response"    # I

    .prologue
    .line 2281
    const/4 v4, 0x0

    .line 2282
    .local v4, "entity":Landroid/content/Entity;
    and-int/lit8 v2, p3, 0x8

    if-nez v2, :cond_2

    .line 2283
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/EasSyncService;->createResponseEntity(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;

    move-result-object v4

    .line 2288
    :goto_0
    new-instance v14, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    invoke-direct {v14, v2}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 2292
    .local v14, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    and-int/lit8 v2, p3, 0x3

    packed-switch v2, :pswitch_data_0

    .line 2310
    :pswitch_0
    const/16 v5, 0x100

    .line 2316
    .local v5, "flag":I
    :goto_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 2318
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2322
    :cond_0
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v2, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v12

    .line 2323
    .local v12, "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v12, :cond_3

    .line 2324
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    array-length v2, v12

    if-ge v13, v2, :cond_3

    .line 2326
    aget-object v2, v12, v13

    const-wide/16 v6, 0x0

    iput-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    .line 2328
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 2330
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    .line 2334
    :cond_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAttachments:Ljava/util/ArrayList;

    aget-object v3, v12, v13

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2324
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 2285
    .end local v5    # "flag":I
    .end local v12    # "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v13    # "i":I
    .end local v14    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    :cond_2
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {p0, v0, v1}, Lcom/android/exchange/EasSyncService;->createResponseEntity(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;

    move-result-object v4

    goto :goto_0

    .line 2296
    .restart local v14    # "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    :pswitch_1
    const/16 v5, 0x40

    .line 2298
    .restart local v5    # "flag":I
    goto :goto_1

    .line 2302
    .end local v5    # "flag":I
    :pswitch_2
    const/16 v5, 0x80

    .line 2304
    .restart local v5    # "flag":I
    goto :goto_1

    .line 2338
    .restart local v12    # "attachments":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_3
    if-eqz v4, :cond_4

    .line 2340
    and-int/lit8 v2, p3, 0x8

    if-nez v2, :cond_5

    .line 2341
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const-string v3, "UID"

    invoke-virtual {v14, v3}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    const/4 v8, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v2 .. v8}, Lcom/android/exchange/utility/CalendarUtilities;->updateMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;ILjava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 2357
    :cond_4
    :goto_3
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v2, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyTextWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mText:Ljava/lang/String;

    .line 2359
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v2, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreBodyHtmlWithMessageId(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    iput-object v2, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mHtml:Ljava/lang/String;

    .line 2361
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-object/from16 v0, p2

    invoke-static {v2, v6, v7, v0}, Lcom/android/exchange/EasOutboxService;->sendMessage(Landroid/content/Context;JLcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 2363
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mBaseUri:Landroid/net/Uri;

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v3, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2368
    return-void

    .line 2344
    :cond_5
    sget-object v6, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const-string v2, "UID"

    invoke-virtual {v14, v2}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    const/4 v11, 0x0

    move-object/from16 v7, p2

    move-object v8, v4

    invoke-static/range {v6 .. v11}, Lcom/android/exchange/utility/CalendarUtilities;->updateProposeNewTimeMessageForEntity(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Landroid/content/Entity;Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Message;

    goto :goto_3

    .line 2292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private shutdownConnectionManagerForLoadMore()V
    .locals 3

    .prologue
    .line 3193
    sget-object v1, Lcom/android/exchange/ExchangeService;->lmClientConnectionManagers:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3195
    const-string v1, "Email"

    const-string v2, "shutdownConnectionManagerForLoadMore"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3197
    iget-boolean v1, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    iget v2, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    invoke-static {v1, v2}, Lcom/android/exchange/ExchangeService;->getClientConnectionManagerForLoadMore(ZI)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    .line 3199
    .local v0, "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 3205
    .end local v0    # "mgr":Lorg/apache/http/conn/ClientConnectionManager;
    :cond_0
    return-void
.end method

.method public static tryProvision(Lcom/android/exchange/EasSyncService;)Z
    .locals 14
    .param p0, "svc"    # Lcom/android/exchange/EasSyncService;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 4461
    invoke-static {p0}, Lcom/android/exchange/EasSyncService;->canProvision(Lcom/android/exchange/EasSyncService;)Lcom/android/exchange/adapter/ProvisionParser;

    move-result-object v5

    .line 4463
    .local v5, "pp":Lcom/android/exchange/adapter/ProvisionParser;
    sget-object v1, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 4464
    .local v1, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 4466
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/android/exchange/adapter/ProvisionParser;->hasSupportablePolicySet()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 4469
    invoke-virtual {v5}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicySet()Lcom/android/emailcommon/service/PolicySet;

    move-result-object v7

    .line 4473
    .local v7, "ps":Lcom/android/emailcommon/service/PolicySet;
    if-eqz v7, :cond_0

    .line 4474
    const/4 v11, 0x0

    invoke-virtual {v7, v0, v11, v10, v1}, Lcom/android/emailcommon/service/PolicySet;->writeAccount(Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;ZLandroid/content/Context;)Z

    .line 4475
    :cond_0
    iget-wide v12, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v1, v12, v13}, Lcom/android/exchange/SecurityPolicyDelegate;->updatePolicies(Landroid/content/Context;J)V

    .line 4477
    invoke-virtual {v5}, Lcom/android/exchange/adapter/ProvisionParser;->getRemoteWipe()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 4479
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "<"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Thread;->getId()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ">"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Remote Wipe got from Provision Parser"

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4482
    sput-boolean v10, Lcom/android/exchange/EasSyncService;->flagRemoteWipe:Z

    .line 4484
    invoke-static {v1, v0, v10}, Lcom/android/exchange/SecurityPolicyDelegate;->setAccountHoldFlag(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Z)V

    .line 4487
    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v10, v11}, Lcom/android/exchange/ExchangeService;->stopNonAccountMailboxSyncsForAccount(J)V

    .line 4492
    :try_start_0
    const-string v10, "!!! Acknowledging remote wipe to server"

    invoke-static {v10}, Lcom/android/exchange/ExchangeService;->alwaysLog(Ljava/lang/String;)V

    .line 4493
    invoke-virtual {v5}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicyKey()Ljava/lang/String;

    move-result-object v4

    .line 4494
    .local v4, "policyKey":Ljava/lang/String;
    if-eqz v4, :cond_2

    .end local v4    # "policyKey":Ljava/lang/String;
    :goto_0
    invoke-static {p0, v4}, Lcom/android/exchange/EasSyncService;->acknowledgeRemoteWipe(Lcom/android/exchange/EasSyncService;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4504
    :goto_1
    invoke-static {v1}, Lcom/android/exchange/SecurityPolicyDelegate;->remoteWipe(Landroid/content/Context;)V

    .line 4547
    .end local v7    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_1
    :goto_2
    return v9

    .line 4494
    .restart local v4    # "policyKey":Ljava/lang/String;
    .restart local v7    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_2
    :try_start_1
    const-string v4, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 4495
    .end local v4    # "policyKey":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 4499
    .local v2, "e":Ljava/lang/Exception;
    sget-object v10, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v11, "acknowledgeRemoteWipe exception"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 4506
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-static {v1, v7}, Lcom/android/exchange/SecurityPolicyDelegate;->isActive(Landroid/content/Context;Lcom/android/emailcommon/service/PolicySet;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 4510
    invoke-virtual {v5}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicyKey()Ljava/lang/String;

    move-result-object v11

    const-string v12, "1"

    invoke-static {p0, v11, v12}, Lcom/android/exchange/EasSyncService;->acknowledgeProvision(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4511
    .restart local v4    # "policyKey":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 4514
    if-eqz v7, :cond_4

    .line 4515
    invoke-virtual {v7, v0, v4, v10, v1}, Lcom/android/emailcommon/service/PolicySet;->writeAccount(Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;ZLandroid/content/Context;)Z

    .line 4517
    :cond_4
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->releaseSecurityHold(Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 4521
    invoke-static {v1}, Lcom/android/exchange/SecurityPolicyDelegate;->reducePolicies(Landroid/content/Context;)V

    .line 4523
    if-eqz v7, :cond_5

    iget-boolean v9, v7, Lcom/android/emailcommon/service/PolicySet;->mPasswordRecoverable:Z

    if-eqz v9, :cond_5

    iget v9, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    const v11, 0x8000

    and-int/2addr v9, v11

    if-eqz v9, :cond_5

    .line 4526
    :try_start_2
    invoke-static {v1}, Lcom/android/emailcommon/variant/DPMWraper;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/variant/DPMWraper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/emailcommon/variant/DPMWraper;->getRecoveryPassword()Ljava/lang/String;

    move-result-object v3

    .line 4528
    .local v3, "password":Ljava/lang/String;
    new-instance v6, Lcom/android/exchange/PasswordRecoveryService;

    invoke-direct {v6, v1, v0, v3}, Lcom/android/exchange/PasswordRecoveryService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Ljava/lang/String;)V

    .line 4530
    .local v6, "prsvc":Lcom/android/exchange/AbstractSyncService;
    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, "(PasswordRecovery)"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v6, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 4532
    .local v8, "thread":Ljava/lang/Thread;
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .end local v3    # "password":Ljava/lang/String;
    .end local v6    # "prsvc":Lcom/android/exchange/AbstractSyncService;
    .end local v8    # "thread":Ljava/lang/Thread;
    :cond_5
    :goto_3
    move v9, v10

    .line 4537
    goto :goto_2

    .line 4533
    :catch_1
    move-exception v2

    .line 4534
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 4541
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "policyKey":Ljava/lang/String;
    :cond_6
    iget-wide v10, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v1, v10, v11}, Lcom/android/exchange/SecurityPolicyDelegate;->policiesRequired(Landroid/content/Context;J)V

    goto :goto_2

    .line 4545
    .end local v7    # "ps":Lcom/android/emailcommon/service/PolicySet;
    :cond_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "<"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->getId()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "pp is null. Or, has unsupported policies. CANNOT PROVISION"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method


# virtual methods
.method protected abortPendingPost()V
    .locals 1

    .prologue
    .line 6036
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6037
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 6038
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 6040
    :cond_0
    return-void
.end method

.method public abortPendingPost([JJJ)V
    .locals 16
    .param p1, "messageIds"    # [J
    .param p2, "outboxId"    # J
    .param p4, "accountId"    # J

    .prologue
    .line 786
    const-string v10, "abortPendingPost"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "start. accountId = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p4

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    if-eqz p1, :cond_0

    move-object/from16 v0, p1

    array-length v10, v0

    if-gtz v10, :cond_1

    .line 789
    :cond_0
    const-string v10, "abortPendingPost"

    const-string v11, "do nothing. messageIds == null || messageIds.length <=0"

    invoke-static {v10, v11}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    :goto_0
    return-void

    .line 792
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    .line 793
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v10, :cond_5

    .line 794
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v9

    .line 795
    .local v9, "uri":Ljava/net/URI;
    if-eqz v9, :cond_3

    .line 796
    invoke-virtual {v9}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v8

    .line 797
    .local v8, "query":Ljava/lang/String;
    move-object/from16 v0, p0

    instance-of v10, v0, Lcom/android/exchange/EasOutboxService;

    if-eqz v10, :cond_3

    .line 799
    const-string v10, "Cmd=SendMail"

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "Cmd=SmartReply"

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "Cmd=SmartForward"

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 801
    :cond_2
    const-string v10, "abortPendingPost"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Reset, aborting sendMessage. for thread: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v13}, Ljava/lang/Thread;->getId()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804
    move-object/from16 v2, p1

    .local v2, "arr$":[J
    :try_start_1
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_3

    aget-wide v6, v2, v4

    .line 805
    .local v6, "id":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    cmp-long v10, v12, v6

    if-nez v10, :cond_4

    .line 806
    const-string v10, "abortPendingPost"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "abort for mPendingMessageId =  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpPost;->abort()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 822
    .end local v2    # "arr$":[J
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "id":J
    .end local v8    # "query":Ljava/lang/String;
    .end local v9    # "uri":Ljava/net/URI;
    :cond_3
    :goto_2
    :try_start_2
    monitor-exit v11

    goto/16 :goto_0

    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v10

    .line 804
    .restart local v2    # "arr$":[J
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "id":J
    .restart local v8    # "query":Ljava/lang/String;
    .restart local v9    # "uri":Ljava/net/URI;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 813
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "id":J
    :catch_0
    move-exception v3

    .line 814
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v10, "abortPendingPost"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Caught Exception "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 820
    .end local v2    # "arr$":[J
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v8    # "query":Ljava/lang/String;
    .end local v9    # "uri":Ljava/net/URI;
    :cond_5
    const-string v10, "abortPendingPost"

    const-string v12, "mPendingPost = null "

    invoke-static {v10, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public activateDevice(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 5743
    new-instance v0, Lcom/android/exchange/adapter/EasActivator;

    invoke-direct {v0, p1}, Lcom/android/exchange/adapter/EasActivator;-><init>(Landroid/content/Context;)V

    .line 5745
    .local v0, "activator":Lcom/android/exchange/adapter/EasActivator;
    invoke-virtual {v0}, Lcom/android/exchange/adapter/EasActivator;->actionActivateDevice()Ljava/lang/String;

    move-result-object v1

    .line 5747
    .local v1, "license":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 5749
    const-string v2, "ExchangeActivation"

    const-string v3, "Activation failed"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 5760
    :goto_0
    return-object v1

    .line 5752
    :cond_0
    const-string v2, "NoDeviceID"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5753
    const-string v2, "ExchangeActivation"

    const-string v3, "Activation failed due to NoDeviceID"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5755
    :cond_1
    const-string v2, "ExchangeActivation"

    const-string v3, "Activation successful "

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5756
    const-string v2, "com.android.email"

    const-string v3, "EASL"

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/utility/AppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public addRequest(Lcom/android/exchange/Request;)V
    .locals 1
    .param p1, "request"    # Lcom/android/exchange/Request;

    .prologue
    .line 879
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 886
    :goto_0
    return-void

    .line 884
    :cond_0
    invoke-super {p0, p1}, Lcom/android/exchange/AbstractSyncService;->addRequest(Lcom/android/exchange/Request;)V

    goto :goto_0
.end method

.method public alarm()Z
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 583
    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    if-nez v5, :cond_0

    move v5, v6

    .line 710
    :goto_0
    return v5

    .line 587
    :cond_0
    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    .line 589
    .local v3, "threadName":Ljava/lang/String;
    new-array v8, v6, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "alarm() called for for thread:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    :goto_1
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v7

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 603
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 607
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 609
    .local v0, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz v0, :cond_6

    iget-boolean v5, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    if-nez v5, :cond_6

    .line 615
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_2

    .line 617
    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v4

    .line 619
    .local v4, "uri":Ljava/net/URI;
    if-eqz v4, :cond_5

    .line 621
    invoke-virtual {v4}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v1

    .line 623
    .local v1, "query":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 625
    const-string v1, "POST"

    .line 629
    :cond_1
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v3, v5, v9

    const/4 v9, 0x1

    const-string v10, ": Alert, aborting "

    aput-object v10, v5, v9

    const/4 v9, 0x2

    aput-object v1, v5, v9

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 641
    .end local v1    # "query":Ljava/lang/String;
    .end local v4    # "uri":Ljava/net/URI;
    :cond_2
    :goto_2
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    .line 643
    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 662
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    const-wide/16 v8, 0x2710

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 674
    :goto_3
    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v2

    .line 676
    .local v2, "s":Ljava/lang/Thread$State;
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_3

    .line 678
    new-array v5, v6, [Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": State = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Thread$State;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 682
    :cond_3
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 690
    :try_start_2
    sget-object v5, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    invoke-virtual {v5, v2}, Ljava/lang/Thread$State;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 694
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    .line 696
    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 698
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "Interrupting..."

    aput-object v9, v5, v6

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 702
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v5, v7

    goto/16 :goto_0

    .line 589
    .end local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v2    # "s":Ljava/lang/Thread$State;
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 633
    .restart local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v4    # "uri":Ljava/net/URI;
    :cond_5
    const/4 v5, 0x2

    :try_start_3
    new-array v5, v5, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v3, v5, v9

    const/4 v9, 0x1

    const-string v10, ": Alert, no URI?"

    aput-object v10, v5, v9

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_2

    .line 662
    .end local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    .end local v4    # "uri":Ljava/net/URI;
    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v5

    .line 646
    .restart local v0    # "post":Lorg/apache/http/client/methods/HttpPost;
    :cond_6
    :try_start_4
    iget-boolean v5, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    if-eqz v5, :cond_7

    .line 647
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    .line 648
    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->interrupt()V

    .line 649
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "alarm() | mPingOnHold="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/exchange/EasSyncService;->mPingOnHold:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | Interrupting..."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 650
    monitor-exit v8

    move v5, v7

    goto/16 :goto_0

    .line 655
    :cond_7
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "Alert, no pending POST"

    aput-object v9, v5, v7

    invoke-virtual {p0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 657
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v5, v6

    goto/16 :goto_0

    .line 706
    .restart local v2    # "s":Ljava/lang/Thread$State;
    :cond_8
    :try_start_5
    monitor-exit v8

    move v5, v6

    .line 710
    goto/16 :goto_0

    .line 706
    :catchall_1
    move-exception v5

    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5

    .line 670
    .end local v2    # "s":Ljava/lang/Thread$State;
    :catch_0
    move-exception v5

    goto/16 :goto_3
.end method

.method protected checkLoadAttachmentRequest(J)Z
    .locals 7
    .param p1, "attId"    # J

    .prologue
    const/4 v2, 0x0

    .line 5976
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-nez v3, :cond_1

    .line 5995
    :cond_0
    :goto_0
    return v2

    .line 5979
    :cond_1
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v3, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v0

    .line 5981
    .local v0, "attachment":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v0, :cond_0

    .line 5984
    if-eqz v0, :cond_2

    .line 5986
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v1

    .line 5987
    .local v1, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v1, :cond_2

    .line 5990
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    and-int/lit16 v3, v3, 0x200

    if-nez v3, :cond_0

    .line 5995
    .end local v1    # "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public createResponseEntity(Lcom/android/emailcommon/provider/EmailContent$Message;I)Landroid/content/Entity;
    .locals 26
    .param p1, "msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "response"    # I

    .prologue
    .line 2137
    new-instance v15, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    .line 2139
    .local v15, "meetingInfo":Lcom/android/emailcommon/mail/PackedString;
    if-nez v15, :cond_0

    .line 2141
    const/4 v13, 0x0

    .line 2272
    :goto_0
    return-object v13

    .line 2151
    :cond_0
    const-string v22, "ORGMAIL"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/android/emailcommon/mail/Address;->parse(Ljava/lang/String;)[Lcom/android/emailcommon/mail/Address;

    move-result-object v4

    .line 2155
    .local v4, "addrs":[Lcom/android/emailcommon/mail/Address;
    array-length v0, v4

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 2157
    const/4 v13, 0x0

    goto :goto_0

    .line 2159
    :cond_1
    const/16 v22, 0x0

    aget-object v22, v4, v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/emailcommon/mail/Address;->getAddress()Ljava/lang/String;

    move-result-object v16

    .line 2161
    .local v16, "organizerEmail":Ljava/lang/String;
    const/16 v22, 0x0

    aget-object v22, v4, v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/emailcommon/mail/Address;->getPersonal()Ljava/lang/String;

    move-result-object v17

    .line 2163
    .local v17, "organizerName":Ljava/lang/String;
    const-string v22, "DTSTAMP"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2165
    .local v8, "dtStamp":Ljava/lang/String;
    const-string v22, "DTSTART"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2167
    .local v9, "dtStart":Ljava/lang/String;
    const-string v22, "DTEND"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2168
    .local v7, "dtEnd":Ljava/lang/String;
    if-nez v7, :cond_2

    if-eqz v9, :cond_2

    .line 2169
    const-string v22, "DURATION"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 2170
    .local v11, "duration":Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 2171
    new-instance v10, Lcom/android/emailcommon/utility/calendar/Duration;

    invoke-direct {v10}, Lcom/android/emailcommon/utility/calendar/Duration;-><init>()V

    .line 2173
    .local v10, "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    :try_start_0
    invoke-virtual {v10, v11}, Lcom/android/emailcommon/utility/calendar/Duration;->parse(Ljava/lang/String;)V

    .line 2174
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    invoke-virtual {v10}, Lcom/android/emailcommon/utility/calendar/Duration;->getMillis()J

    move-result-wide v24

    add-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 2175
    .local v6, "d":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/Long;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 2182
    .end local v6    # "d":Ljava/lang/Long;
    .end local v10    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v11    # "duration":Ljava/lang/String;
    :cond_2
    :goto_1
    if-eqz v8, :cond_3

    if-eqz v9, :cond_3

    if-nez v7, :cond_4

    .line 2184
    :cond_3
    const/4 v13, 0x0

    goto :goto_0

    .line 2176
    .restart local v10    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .restart local v11    # "duration":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 2177
    .local v12, "e":Ljava/text/ParseException;
    sget-object v22, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v23, "ParseException in createResponseEntity"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2187
    .end local v10    # "dur":Lcom/android/emailcommon/utility/calendar/Duration;
    .end local v11    # "duration":Ljava/lang/String;
    .end local v12    # "e":Ljava/text/ParseException;
    :cond_4
    const-string v22, "PROPOSED_START_TIME"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 2189
    .local v21, "proposedStartTime":Ljava/lang/String;
    const-string v22, "PROPOSED_END_TIME"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2191
    .local v19, "proposedEndTime":Ljava/lang/String;
    const-string v22, "LOC"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2200
    .local v20, "proposedLocaion":Ljava/lang/String;
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 2202
    .local v14, "entityValues":Landroid/content/ContentValues;
    new-instance v13, Landroid/content/Entity;

    invoke-direct {v13, v14}, Landroid/content/Entity;-><init>(Landroid/content/ContentValues;)V

    .line 2206
    .local v13, "entity":Landroid/content/Entity;
    const-string v22, "-"

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_5

    .line 2207
    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v22

    invoke-static/range {v22 .. v23}, Lcom/android/exchange/utility/CalendarUtilities;->millisToInstanceId(J)Ljava/lang/String;

    move-result-object v8

    .line 2211
    :cond_5
    const-string v22, "DTSTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Lcom/android/exchange/utility/CalendarUtilities;->millisToEasDateTime(J)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2214
    const-string v22, "-"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_6

    const-string v22, "T"

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 2215
    :cond_6
    const-string v22, "dtstart"

    invoke-static {v9}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2220
    :goto_2
    const-string v22, "-"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-nez v22, :cond_7

    const-string v22, "T"

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 2221
    :cond_7
    const-string v22, "dtend"

    invoke-static {v7}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2226
    :goto_3
    const-string v22, "eventLocation"

    const-string v23, "LOC"

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2228
    const-string v22, "title"

    const-string v23, "TITLE"

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2230
    const-string v22, "organizer"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2233
    and-int/lit8 v22, p2, 0x8

    if-eqz v22, :cond_8

    .line 2235
    const-string v22, "PROPOSED_START_TIME"

    invoke-static/range {v21 .. v21}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2237
    const-string v22, "PROPOSED_END_TIME"

    invoke-static/range {v19 .. v19}, Lcom/android/emailcommon/utility/Utility;->parseEmailDateTimeToMillis(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2239
    const-string v22, "LOC"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2244
    :cond_8
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2246
    .local v5, "attendeeValues":Landroid/content/ContentValues;
    const-string v22, "attendeeRelationship"

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2248
    const-string v22, "attendeeEmail"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    sget-object v22, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    invoke-virtual {v13, v0, v5}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 2254
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 2256
    .local v18, "organizerValues":Landroid/content/ContentValues;
    const-string v22, "attendeeRelationship"

    const/16 v23, 0x2

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2258
    const-string v22, "attendeeEmail"

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2260
    sget-object v22, Landroid/provider/CalendarContract$Attendees;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Landroid/content/Entity;->addSubValue(Landroid/net/Uri;Landroid/content/ContentValues;)V

    goto/16 :goto_0

    .line 2217
    .end local v5    # "attendeeValues":Landroid/content/ContentValues;
    .end local v18    # "organizerValues":Landroid/content/ContentValues;
    :cond_9
    const-string v22, "dtstart"

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_2

    .line 2223
    :cond_a
    const-string v22, "dtend"

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3
.end method

.method protected executePostWithTimeout(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;
    .locals 6
    .param p1, "client"    # Lorg/apache/http/client/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpPost;
    .param p3, "timeout"    # I
    .param p4, "isPingCommand"    # Z
    .param p5, "isAbortNotNeeded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3927
    new-instance v0, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;-><init>(Lcom/android/exchange/EasSyncService;Lcom/android/exchange/EasSyncService$1;)V

    .local v0, "redirectionHandler":Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;
    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 3928
    invoke-virtual/range {v0 .. v5}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->execPost(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;

    move-result-object v1

    return-object v1
.end method

.method protected getClient(Ljava/lang/String;IZ)Lorg/apache/http/client/HttpClient;
    .locals 10
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "timeout"    # I
    .param p3, "isPingCommand"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 6009
    const/4 v0, 0x0

    .line 6010
    .local v0, "client":Lorg/apache/http/client/HttpClient;
    const-string v5, "ItemOperations"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "Sync"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    const-wide/high16 v8, 0x4004000000000000L    # 2.5

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_2

    :cond_0
    move v1, v4

    .line 6011
    .local v1, "isLoadMoreCommand":Z
    :goto_0
    const-string v5, "SendMail"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "SmartReply"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "SmartForward"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_1
    move v2, v4

    .line 6013
    .local v2, "isOutboxSendMailCommand":Z
    :goto_1
    if-eqz p3, :cond_4

    .line 6014
    invoke-direct {p0, p2}, Lcom/android/exchange/EasSyncService;->getPingClient(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    .line 6031
    :goto_2
    return-object v0

    .end local v1    # "isLoadMoreCommand":Z
    .end local v2    # "isOutboxSendMailCommand":Z
    :cond_2
    move v1, v3

    .line 6010
    goto :goto_0

    .restart local v1    # "isLoadMoreCommand":Z
    :cond_3
    move v2, v3

    .line 6011
    goto :goto_1

    .line 6015
    .restart local v2    # "isOutboxSendMailCommand":Z
    :cond_4
    if-eqz v1, :cond_5

    .line 6016
    invoke-direct {p0, p2}, Lcom/android/exchange/EasSyncService;->getLoadMoreClient(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    goto :goto_2

    .line 6019
    :cond_5
    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v3

    if-nez v3, :cond_7

    .line 6020
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v3, :cond_6

    .line 6021
    sget-object v3, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendHttpClientPost(). isOutboxSendMailCommand. mAccount.mId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6022
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, p2, v4, v5}, Lcom/android/exchange/EasSyncService;->getSendMessageClient(IJ)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    goto :goto_2

    .line 6024
    :cond_6
    sget-object v3, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v4, "sendHttpClientPost(). isOutboxSendMailCommand. mAccount is null. abnormal case."

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6025
    const-wide/16 v4, -0x1

    invoke-direct {p0, p2, v4, v5}, Lcom/android/exchange/EasSyncService;->getSendMessageClient(IJ)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    goto :goto_2

    .line 6029
    :cond_7
    invoke-direct {p0, p2}, Lcom/android/exchange/EasSyncService;->getHttpClient(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    goto :goto_2
.end method

.method public getIRMTemplates()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4772
    const/4 v0, -0x1

    .line 4774
    .local v0, "code":I
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_0

    .line 4775
    const-string v6, "IRM"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":Inside getIRMTemplates"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4778
    :cond_0
    new-instance v2, Lcom/android/exchange/irm/IRMSettingsAdapter;

    iget-object v6, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v2, v6, p0}, Lcom/android/exchange/irm/IRMSettingsAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .line 4779
    .local v2, "irmSettingAdapter":Lcom/android/exchange/irm/IRMSettingsAdapter;
    invoke-virtual {v2}, Lcom/android/exchange/irm/IRMSettingsAdapter;->buildSettingsRequest()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    .line 4780
    .local v1, "irmSerializer":Lcom/android/exchange/adapter/Serializer;
    const-string v6, "Settings"

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v5

    .line 4783
    .local v5, "resp":Lcom/android/exchange/EasResponse;
    :try_start_0
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 4785
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_1

    .line 4786
    const-string v6, "IRM"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":getIRMTemplates response:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4789
    :cond_1
    const/16 v6, 0xc8

    if-ne v0, v6, :cond_5

    .line 4790
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v4

    .line 4792
    .local v4, "len":I
    if-eqz v4, :cond_5

    .line 4793
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 4794
    .local v3, "is":Ljava/io/InputStream;
    invoke-virtual {v2, v3}, Lcom/android/exchange/irm/IRMSettingsAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    .line 4795
    const/4 v0, 0x0

    .line 4808
    .end local v0    # "code":I
    if-eqz v5, :cond_2

    .line 4809
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->close()V

    .line 4812
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    :cond_2
    :goto_0
    return v0

    .line 4798
    .restart local v0    # "code":I
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "len":I
    :cond_3
    :try_start_1
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v6, :cond_4

    .line 4799
    const-string v6, "IRM"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":getIRMTemplates parse status:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Lcom/android/exchange/irm/IRMSettingsAdapter;->mIrmStatus:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4803
    :cond_4
    iget v0, v2, Lcom/android/exchange/irm/IRMSettingsAdapter;->mIrmStatus:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4808
    .end local v0    # "code":I
    if-eqz v5, :cond_2

    .line 4809
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_0

    .line 4808
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "len":I
    .restart local v0    # "code":I
    :cond_5
    if-eqz v5, :cond_2

    .line 4809
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_0

    .line 4808
    :catchall_0
    move-exception v6

    if-eqz v5, :cond_6

    .line 4809
    invoke-virtual {v5}, Lcom/android/exchange/EasResponse;->close()V

    :cond_6
    throw v6
.end method

.method public getPath(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hostAuthKey"    # J

    .prologue
    .line 2978
    const-string v0, "/Microsoft-Server-ActiveSync"

    .line 2979
    .local v0, "path":Ljava/lang/String;
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_2

    .line 2980
    invoke-static {p1, p2, p3}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->getPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 2981
    .local v1, "temp":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2982
    const-string v2, "Microsoft-Server-ActiveSync"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2991
    .end local v1    # "temp":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 2983
    .restart local v1    # "temp":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2986
    .end local v1    # "temp":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2987
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    const-string v3, "Microsoft-Server-ActiveSync"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2988
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method getTargetCollectionClassFromCursor(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 4409
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 4411
    .local v0, "type":I
    const/16 v1, 0x42

    if-eq v0, v1, :cond_0

    const/16 v1, 0x53

    if-ne v0, v1, :cond_1

    .line 4413
    :cond_0
    const-string v1, "Contacts"

    .line 4435
    :goto_0
    return-object v1

    .line 4415
    :cond_1
    const/16 v1, 0x41

    if-eq v0, v1, :cond_2

    const/16 v1, 0x52

    if-ne v0, v1, :cond_3

    .line 4417
    :cond_2
    const-string v1, "Calendar"

    goto :goto_0

    .line 4421
    :cond_3
    const/16 v1, 0x43

    if-ne v0, v1, :cond_4

    .line 4423
    const-string v1, "Tasks"

    goto :goto_0

    .line 4427
    :cond_4
    const/16 v1, 0x45

    if-ne v0, v1, :cond_5

    .line 4431
    const-string v1, "Notes"

    goto :goto_0

    .line 4435
    :cond_5
    const-string v1, "Email"

    goto :goto_0
.end method

.method protected isCBA()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3094
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-nez v2, :cond_2

    .line 3095
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 3102
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 3099
    goto :goto_0

    .line 3102
    :cond_2
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    const/high16 v3, 0x10000

    and-int/2addr v2, v3

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public isIrmError(I)I
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 4817
    packed-switch p1, :pswitch_data_0

    .line 4833
    const/4 p1, -0x1

    .end local p1    # "code":I
    :pswitch_0
    return p1

    .line 4817
    nop

    :pswitch_data_0
    .packed-switch 0xa8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected isProvisionError(I)Z
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 899
    const/16 v0, 0x1c1

    if-eq p1, v0, :cond_0

    const/16 v0, 0x193

    if-ne p1, v0, :cond_1

    .line 901
    :cond_0
    const/4 v0, 0x1

    .line 905
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mEasNeedsProvisioning:Z

    goto :goto_0
.end method

.method protected isProvisionError(Ljava/io/InputStream;I)Z
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/utility/DeviceAccessException;,
            Lcom/android/exchange/CommandStatusException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 917
    const/16 v2, 0x1c1

    if-eq p2, v2, :cond_0

    const/16 v2, 0x193

    if-ne p2, v2, :cond_1

    .line 939
    :cond_0
    :goto_0
    return v1

    .line 925
    :cond_1
    if-eqz p1, :cond_2

    :try_start_0
    new-instance v2, Lcom/android/exchange/adapter/FourteenProvisionParser;

    invoke-direct {v2, p1}, Lcom/android/exchange/adapter/FourteenProvisionParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2}, Lcom/android/exchange/adapter/FourteenProvisionParser;->checkForProvisioning()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    .line 939
    :cond_2
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 931
    :catch_0
    move-exception v0

    .line 933
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method protected loadServiceData()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 5380
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 5381
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-nez v3, :cond_1

    .line 5421
    :cond_0
    :goto_0
    return v2

    .line 5385
    :cond_1
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    new-instance v4, Landroid/accounts/Account;

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    const-string v6, "com.android.exchange"

    invoke-direct {v4, v5, v6}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mAmAccount:Landroid/accounts/Account;

    .line 5390
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v1

    .line 5391
    .local v1, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-nez v1, :cond_2

    .line 5392
    sget-object v3, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v4, "ha is null for account"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5396
    :cond_2
    iget-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 5397
    iget-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 5398
    iget-object v3, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 5399
    iget v3, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v3, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 5402
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 5405
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 5406
    const-string v3, "2.5"

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 5409
    :cond_3
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 5412
    :try_start_0
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v3

    iput-object v3, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5418
    :goto_1
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 5421
    const/4 v2, 0x1

    goto :goto_0

    .line 5413
    :catch_0
    move-exception v0

    .line 5414
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v4, "error occruerd here "

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5415
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method makeUriString()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2999
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 3001
    :cond_0
    invoke-direct {p0}, Lcom/android/exchange/EasSyncService;->cacheAuthAndCmdString()V

    .line 3012
    :cond_1
    const/4 v0, 0x0

    .line 3013
    .local v0, "proxyAndSSLEnabled":Z
    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 3015
    .local v1, "tempHost":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    sget-object v3, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v3

    if-ltz v3, :cond_2

    .line 3019
    iget-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    if-eqz v3, :cond_2

    .line 3021
    const/4 v0, 0x1

    .line 3033
    :cond_2
    new-instance v2, Ljava/lang/StringBuffer;

    iget-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    if-eqz v3, :cond_5

    const-string v3, "httpts"

    :goto_0
    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3036
    .local v2, "us":Ljava/lang/StringBuffer;
    const-string v3, "://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    if-eqz v0, :cond_7

    const-string v3, ":443"

    :goto_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    sget-object v6, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    :goto_2
    invoke-virtual {p0, v6, v4, v5}, Lcom/android/exchange/EasSyncService;->getPath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3038
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 3040
    const-string v3, "?Cmd="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, p0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3044
    :cond_3
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mASCmdParams:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 3046
    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mASCmdParams:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 3050
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 3033
    .end local v2    # "us":Ljava/lang/StringBuffer;
    :cond_5
    const-string v3, "https"

    goto :goto_0

    :cond_6
    const-string v3, "http"

    goto :goto_0

    .line 3036
    .restart local v2    # "us":Ljava/lang/StringBuffer;
    :cond_7
    const-string v3, ""

    goto :goto_1

    :cond_8
    const-wide/16 v4, -0x1

    goto :goto_2
.end method

.method protected messageMoveRequest(Lcom/android/exchange/MessageMoveRequest;)V
    .locals 21
    .param p1, "req"    # Lcom/android/exchange/MessageMoveRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 2696
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v14

    .line 2698
    .local v14, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v14, :cond_1

    .line 2807
    :cond_0
    :goto_0
    return-void

    .line 2702
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "mailboxKey"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2710
    .local v8, "c":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 2711
    .local v18, "srcMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v8, :cond_3

    .line 2714
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 2722
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2718
    :cond_2
    :try_start_1
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v18

    .line 2722
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2726
    :cond_3
    if-eqz v18, :cond_0

    .line 2730
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MessageMoveRequest;->mMailboxId:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v10

    .line 2732
    .local v10, "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v10, :cond_0

    .line 2736
    new-instance v17, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v17 .. v17}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 2738
    .local v17, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v2, 0x145

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x146

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2739
    const/16 v2, 0x147

    iget-object v3, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2740
    const/16 v2, 0x148

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2741
    const/16 v2, 0x149

    iget-object v3, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2742
    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 2744
    const/16 v16, 0x0

    .line 2747
    .local v16, "res":Lcom/android/exchange/EasResponse;
    :try_start_2
    const-string v2, "MoveItems"

    invoke-virtual/range {v17 .. v17}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v16

    .line 2749
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v19

    .line 2750
    .local v19, "status":I
    const/16 v2, 0xc8

    move/from16 v0, v19

    if-ne v0, v2, :cond_7

    .line 2751
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v13

    .line 2752
    .local v13, "len":I
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 2753
    .local v12, "is":Ljava/io/InputStream;
    if-eqz v13, :cond_4

    .line 2754
    new-instance v15, Lcom/android/exchange/adapter/MoveItemsParser;

    move-object/from16 v0, p0

    invoke-direct {v15, v12, v0}, Lcom/android/exchange/adapter/MoveItemsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 2755
    .local v15, "p":Lcom/android/exchange/adapter/MoveItemsParser;
    invoke-virtual {v15}, Lcom/android/exchange/adapter/MoveItemsParser;->parse()Z

    .line 2756
    invoke-virtual {v15}, Lcom/android/exchange/adapter/MoveItemsParser;->getStatusCode()I

    move-result v20

    .line 2757
    .local v20, "statusCode":I
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 2758
    .local v9, "cv":Landroid/content/ContentValues;
    const/4 v2, 0x2

    move/from16 v0, v20

    if-ne v0, v2, :cond_6

    .line 2764
    const-string v2, "mailboxKey"

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2766
    const-string v2, "flagMoved"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2768
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->MOVE_ITEM_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v9, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2772
    :try_start_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v2

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    const/16 v3, 0x42

    invoke-interface {v2, v4, v5, v3}, Lcom/android/emailcommon/service/IEmailServiceCallback;->moveItemStatus(JI)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2804
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v15    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .end local v20    # "statusCode":I
    :cond_4
    :goto_1
    if-eqz v16, :cond_0

    .line 2805
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 2722
    .end local v10    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v16    # "res":Lcom/android/exchange/EasResponse;
    .end local v17    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v19    # "status":I
    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2774
    .restart local v9    # "cv":Landroid/content/ContentValues;
    .restart local v10    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v15    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .restart local v16    # "res":Lcom/android/exchange/EasResponse;
    .restart local v17    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v19    # "status":I
    .restart local v20    # "statusCode":I
    :catch_0
    move-exception v11

    .line 2775
    .local v11, "e":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 2804
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v11    # "e":Landroid/os/RemoteException;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v15    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .end local v19    # "status":I
    .end local v20    # "statusCode":I
    :catchall_1
    move-exception v2

    if-eqz v16, :cond_5

    .line 2805
    invoke-virtual/range {v16 .. v16}, Lcom/android/exchange/EasResponse;->close()V

    :cond_5
    throw v2

    .line 2777
    .restart local v9    # "cv":Landroid/content/ContentValues;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v13    # "len":I
    .restart local v15    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .restart local v19    # "status":I
    .restart local v20    # "statusCode":I
    :cond_6
    const/4 v2, 0x1

    move/from16 v0, v20

    if-ne v0, v2, :cond_4

    .line 2779
    :try_start_5
    const-string v2, "syncServerId"

    invoke-virtual {v15}, Lcom/android/exchange/adapter/MoveItemsParser;->getNewServerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2780
    const-string v2, "flags"

    iget v3, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    or-int/lit16 v3, v3, 0x200

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2782
    const-string v2, "flagMoved"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2784
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->MOVE_ITEM_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v9, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2789
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 2790
    const-string v2, "syncServerId"

    invoke-virtual {v15}, Lcom/android/exchange/adapter/MoveItemsParser;->getNewServerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2791
    const-string v2, "mailboxKey"

    iget-wide v4, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, v14, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v9, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 2797
    .end local v9    # "cv":Landroid/content/ContentValues;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v15    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .end local v20    # "statusCode":I
    :cond_7
    invoke-static/range {v19 .. v19}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2798
    new-instance v2, Lcom/android/exchange/EasAuthenticationException;

    invoke-direct {v2}, Lcom/android/exchange/EasAuthenticationException;-><init>()V

    throw v2

    .line 2800
    :cond_8
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Move items request failed, code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2801
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
.end method

.method protected messageMoveRequest(Ljava/util/ArrayList;)V
    .locals 36
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/exchange/MessageMoveRequest;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 2537
    .local p1, "msgMoveRequestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/MessageMoveRequest;>;"
    new-instance v31, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 2539
    .local v31, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v24, 0x0

    .line 2541
    .local v24, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    const/16 v33, 0x0

    .line 2543
    .local v33, "srcMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    .line 2545
    .local v25, "originalMsgMoveRequestMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;>;"
    const/16 v32, 0x0

    .line 2547
    .local v32, "sendCmd":Z
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/exchange/MessageMoveRequest;

    .line 2551
    .local v21, "moveRequest":Lcom/android/exchange/MessageMoveRequest;
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v24

    .line 2553
    if-eqz v24, :cond_0

    .line 2557
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v24

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "mailboxKey"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 2565
    .local v13, "c":Landroid/database/Cursor;
    if-eqz v13, :cond_2

    .line 2568
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    .line 2576
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2572
    :cond_1
    :try_start_1
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v33

    .line 2576
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 2580
    :cond_2
    if-eqz v33, :cond_0

    .line 2584
    sget-object v4, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/android/exchange/MessageMoveRequest;->mMailboxId:J

    invoke-static {v4, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v15

    .line 2586
    .local v15, "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v15, :cond_0

    .line 2590
    if-nez v32, :cond_3

    .line 2591
    const/16 v4, 0x145

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2592
    const/16 v32, 0x1

    .line 2594
    :cond_3
    const/16 v4, 0x146

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2595
    const/16 v4, 0x147

    move-object/from16 v0, v24

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2596
    const/16 v4, 0x148

    move-object/from16 v0, v33

    iget-object v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2597
    const/16 v4, 0x149

    iget-object v5, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2598
    invoke-virtual/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 2599
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    new-instance v5, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;

    move-object/from16 v0, v24

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-object/from16 v0, v33

    iget-wide v8, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    move-object/from16 v0, v24

    iget v10, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    iget-wide v11, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-direct/range {v5 .. v12}, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;-><init>(JJIJ)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2576
    .end local v15    # "dstMailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catchall_0
    move-exception v4

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v4

    .line 2602
    .end local v13    # "c":Landroid/database/Cursor;
    .end local v21    # "moveRequest":Lcom/android/exchange/MessageMoveRequest;
    :cond_4
    if-eqz v32, :cond_c

    .line 2604
    invoke-virtual/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 2609
    const/16 v29, 0x0

    .line 2612
    .local v29, "res":Lcom/android/exchange/EasResponse;
    :try_start_2
    const-string v4, "MoveItems"

    invoke-virtual/range {v31 .. v31}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v29

    .line 2614
    invoke-virtual/range {v29 .. v29}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v34

    .line 2615
    .local v34, "status":I
    const/16 v4, 0xc8

    move/from16 v0, v34

    if-ne v0, v4, :cond_8

    .line 2616
    invoke-virtual/range {v29 .. v29}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v20

    .line 2617
    .local v20, "len":I
    invoke-virtual/range {v29 .. v29}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v19

    .line 2618
    .local v19, "is":Ljava/io/InputStream;
    if-eqz v20, :cond_a

    .line 2619
    new-instance v28, Lcom/android/exchange/adapter/MoveItemsParser;

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    move-object/from16 v2, p0

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/adapter/MoveItemsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    .line 2620
    .local v28, "p":Lcom/android/exchange/adapter/MoveItemsParser;
    invoke-virtual/range {v28 .. v28}, Lcom/android/exchange/adapter/MoveItemsParser;->parse()Z

    .line 2621
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 2622
    .local v14, "cv":Landroid/content/ContentValues;
    invoke-virtual/range {v28 .. v28}, Lcom/android/exchange/adapter/MoveItemsParser;->getMoveItemsResponseList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_5
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;

    .line 2623
    .local v30, "response":Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getStatusCode()I

    move-result v35

    .line 2624
    .local v35, "statusCode":I
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getOriginalServerId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;

    iget-wide v0, v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mId:J

    move-wide/from16 v26, v0

    .line 2625
    .local v26, "originalMsgId":J
    const/4 v4, 0x2

    move/from16 v0, v35

    if-ne v0, v4, :cond_7

    .line 2631
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getOriginalServerId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;

    iget-wide v0, v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mSrcMailboxKey:J

    move-wide/from16 v22, v0

    .line 2632
    .local v22, "mailboxKey":J
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 2633
    const-string v4, "mailboxKey"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635
    const-string v4, "flagMoved"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2637
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->MOVE_ITEM_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v26

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v14, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2641
    :try_start_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v4

    const/16 v5, 0x42

    move-wide/from16 v0, v22

    invoke-interface {v4, v0, v1, v5}, Lcom/android/emailcommon/service/IEmailServiceCallback;->moveItemStatus(JI)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 2643
    :catch_0
    move-exception v16

    .line 2644
    .local v16, "e":Landroid/os/RemoteException;
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 2676
    .end local v14    # "cv":Landroid/content/ContentValues;
    .end local v16    # "e":Landroid/os/RemoteException;
    .end local v19    # "is":Ljava/io/InputStream;
    .end local v20    # "len":I
    .end local v22    # "mailboxKey":J
    .end local v26    # "originalMsgId":J
    .end local v28    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .end local v30    # "response":Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    .end local v34    # "status":I
    .end local v35    # "statusCode":I
    :catchall_1
    move-exception v4

    if-eqz v29, :cond_6

    .line 2677
    invoke-virtual/range {v29 .. v29}, Lcom/android/exchange/EasResponse;->close()V

    .line 2678
    :cond_6
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->clear()V

    throw v4

    .line 2646
    .restart local v14    # "cv":Landroid/content/ContentValues;
    .restart local v19    # "is":Ljava/io/InputStream;
    .restart local v20    # "len":I
    .restart local v26    # "originalMsgId":J
    .restart local v28    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .restart local v30    # "response":Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    .restart local v34    # "status":I
    .restart local v35    # "statusCode":I
    :cond_7
    const/4 v4, 0x1

    move/from16 v0, v35

    if-ne v0, v4, :cond_5

    .line 2647
    :try_start_5
    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getOriginalServerId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;

    iget v0, v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mFlags:I

    move/from16 v17, v0

    .line 2648
    .local v17, "flags":I
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 2650
    const-string v4, "syncServerId"

    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getNewServerId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2651
    const-string v4, "flags"

    move/from16 v0, v17

    or-int/lit16 v5, v0, 0x200

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2653
    const-string v4, "flagMoved"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2655
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->MOVE_ITEM_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v26

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v14, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2660
    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    .line 2661
    const-string v4, "syncServerId"

    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getNewServerId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662
    const-string v5, "mailboxKey"

    invoke-virtual/range {v30 .. v30}, Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;->getOriginalServerId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;

    iget-wide v6, v4, Lcom/android/exchange/EasSyncService$OriginalMsgMoveRequest;->mDstMailboxKey:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v14, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2663
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v26

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v14, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2669
    .end local v14    # "cv":Landroid/content/ContentValues;
    .end local v17    # "flags":I
    .end local v19    # "is":Ljava/io/InputStream;
    .end local v20    # "len":I
    .end local v26    # "originalMsgId":J
    .end local v28    # "p":Lcom/android/exchange/adapter/MoveItemsParser;
    .end local v30    # "response":Lcom/android/exchange/adapter/MoveItemsParser$MoveItemsResponse;
    .end local v35    # "statusCode":I
    :cond_8
    invoke-static/range {v34 .. v34}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2670
    new-instance v4, Lcom/android/exchange/EasAuthenticationException;

    invoke-direct {v4}, Lcom/android/exchange/EasAuthenticationException;-><init>()V

    throw v4

    .line 2672
    :cond_9
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Move items request failed, code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v34

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2673
    new-instance v4, Ljava/io/IOException;

    invoke-direct {v4}, Ljava/io/IOException;-><init>()V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2676
    .restart local v19    # "is":Ljava/io/InputStream;
    .restart local v20    # "len":I
    :cond_a
    if-eqz v29, :cond_b

    .line 2677
    invoke-virtual/range {v29 .. v29}, Lcom/android/exchange/EasResponse;->close()V

    .line 2678
    :cond_b
    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->clear()V

    .line 2680
    .end local v19    # "is":Ljava/io/InputStream;
    .end local v20    # "len":I
    .end local v29    # "res":Lcom/android/exchange/EasResponse;
    .end local v34    # "status":I
    :cond_c
    return-void
.end method

.method protected declared-synchronized processCommand(Lcom/android/exchange/adapter/AbstractCommandAdapter;)V
    .locals 13
    .param p1, "target"    # Lcom/android/exchange/adapter/AbstractCommandAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 5283
    monitor-enter p0

    const/4 v4, 0x0

    .line 5284
    .local v4, "iterationCount":I
    const/4 v9, 0x0

    :try_start_0
    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 5287
    :goto_0
    add-int/lit8 v4, v4, 0x1

    const/4 v9, 0x5

    if-ge v4, v9, :cond_0

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v9, :cond_0

    .line 5288
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->hasChangedItems()Z

    move-result v9

    if-nez v9, :cond_1

    .line 5289
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "processCommand(): No changed items."

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5374
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    .line 5292
    :cond_1
    const/4 v9, 0x1

    :try_start_1
    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "processCommand(): Iteration: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5295
    :try_start_2
    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    sget-object v10, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v10}, Lcom/android/emailcommon/provider/EmailContent$Account;->refresh(Landroid/content/Context;)V

    .line 5297
    new-instance v7, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v7}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 5298
    .local v7, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-virtual {p1, v7}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 5299
    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v10, "sendLocalChanges returned false. Returning ..."

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 5358
    .end local v7    # "s":Lcom/android/exchange/adapter/Serializer;
    :catch_0
    move-exception v1

    .line 5359
    .local v1, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_3
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->cleanup()V

    .line 5360
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5283
    .end local v1    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 5302
    .restart local v7    # "s":Lcom/android/exchange/adapter/Serializer;
    :cond_2
    :try_start_4
    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 5303
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->getCommandName()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v7}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v11

    invoke-direct {v10, v11}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    const/16 v11, 0x7530

    invoke-virtual {p0, v9, v10, v11}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;
    :try_end_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v6

    .line 5307
    .local v6, "resp":Lcom/android/exchange/EasResponse;
    :try_start_5
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 5308
    .local v0, "code":I
    const/16 v9, 0xc8

    if-ne v0, v9, :cond_b

    .line 5310
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v5

    .line 5311
    .local v5, "len":I
    if-eqz v5, :cond_3

    .line 5312
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v3

    .line 5313
    .local v3, "is":Ljava/io/InputStream;
    if-eqz v3, :cond_a

    .line 5316
    :try_start_6
    invoke-virtual {p1, v3}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_6
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 5355
    .end local v3    # "is":Ljava/io/InputStream;
    :cond_3
    :goto_2
    if-eqz v6, :cond_4

    .line 5356
    :try_start_7
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 5372
    :cond_4
    const/4 v9, 0x0

    :try_start_8
    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 5317
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 5320
    .local v1, "e":Lcom/android/exchange/CommandStatusException;
    :try_start_9
    iget v8, v1, Lcom/android/exchange/CommandStatusException;->mStatus:I

    .line 5321
    .local v8, "status":I
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 5322
    const/4 v9, 0x4

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 5355
    :goto_3
    if-eqz v6, :cond_0

    .line 5356
    :try_start_a
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_a
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_1

    .line 5361
    .end local v0    # "code":I
    .end local v1    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "len":I
    .end local v6    # "resp":Lcom/android/exchange/EasResponse;
    .end local v7    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v8    # "status":I
    :catch_2
    move-exception v2

    .line 5362
    .local v2, "ioe":Ljava/io/IOException;
    :try_start_b
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "processCommand(): Caught IOException:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5363
    const/16 v9, 0x21

    invoke-virtual {p1, v9}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->callback(I)V

    .line 5364
    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->cleanup()V

    .line 5365
    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v9, :cond_5

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mPostAborted:Z

    if-eqz v9, :cond_f

    .line 5366
    :cond_5
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "processCommand(): Post is stopped or aborted. Throwing IOException back to caller"

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5367
    throw v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 5323
    .end local v2    # "ioe":Ljava/io/IOException;
    .restart local v0    # "code":I
    .restart local v1    # "e":Lcom/android/exchange/CommandStatusException;
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v5    # "len":I
    .restart local v6    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v7    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v8    # "status":I
    :cond_6
    :try_start_c
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTooManyPartnerships(I)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 5324
    const/16 v9, 0xb

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_3

    .line 5355
    .end local v0    # "code":I
    .end local v1    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "len":I
    .end local v8    # "status":I
    :catchall_1
    move-exception v9

    if-eqz v6, :cond_7

    .line 5356
    :try_start_d
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->close()V

    :cond_7
    throw v9
    :try_end_d
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 5327
    .restart local v0    # "code":I
    .restart local v1    # "e":Lcom/android/exchange/CommandStatusException;
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v5    # "len":I
    .restart local v8    # "status":I
    :cond_8
    :try_start_e
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 5328
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto :goto_3

    .line 5330
    :cond_9
    const/4 v9, 0x3

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto :goto_3

    .line 5336
    .end local v1    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v8    # "status":I
    :cond_a
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->getCommandName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": Empty input stream"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5340
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v5    # "len":I
    :cond_b
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/android/exchange/adapter/AbstractCommandAdapter;->getCommandName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": Response error: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9, v0}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 5341
    invoke-static {v0}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 5342
    const/4 v9, 0x2

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 5355
    :goto_4
    if-eqz v6, :cond_0

    .line 5356
    :try_start_f
    invoke-virtual {v6}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_f
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_f .. :try_end_f} :catch_0
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_2
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_1

    .line 5344
    :cond_c
    :try_start_10
    invoke-virtual {p0, v0}, Lcom/android/exchange/EasSyncService;->isProvisionError(I)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 5345
    const/4 v9, 0x4

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto :goto_4

    .line 5346
    :cond_d
    const/16 v9, 0x1f5

    if-ne v0, v9, :cond_e

    .line 5348
    const/16 v9, 0xa

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto :goto_4

    .line 5350
    :cond_e
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto :goto_4

    .line 5369
    .end local v0    # "code":I
    .end local v6    # "resp":Lcom/android/exchange/EasResponse;
    .end local v7    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v2    # "ioe":Ljava/io/IOException;
    :cond_f
    const/4 v9, 0x0

    :try_start_11
    iput v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_1
.end method

.method public reset()V
    .locals 8

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 719
    :try_start_0
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    if-nez v2, :cond_1

    .line 725
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 727
    .local v1, "uri":Ljava/net/URI;
    if-eqz v1, :cond_1

    .line 729
    invoke-virtual {v1}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 731
    .local v0, "query":Ljava/lang/String;
    const-string v2, "Cmd=Ping"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mIsSyncWithHBICmd:Z

    if-eqz v2, :cond_2

    .line 733
    :cond_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Reset, aborting Ping. for thread: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 735
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 737
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 775
    .end local v0    # "query":Ljava/lang/String;
    .end local v1    # "uri":Ljava/net/URI;
    :cond_1
    :goto_0
    monitor-exit v3

    .line 777
    return-void

    .line 755
    .restart local v0    # "query":Ljava/lang/String;
    .restart local v1    # "uri":Ljava/net/URI;
    :cond_2
    const-string v2, "Cmd=ItemOperations"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Cmd=Sync"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4004000000000000L    # 2.5

    cmpg-double v2, v4, v6

    if-gtz v2, :cond_1

    .line 759
    :cond_3
    const-string v2, "Email"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Reset, aborting loadMore. for thread: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mPostReset:Z

    .line 763
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 765
    invoke-direct {p0}, Lcom/android/exchange/EasSyncService;->shutdownConnectionManagerForLoadMore()V

    goto :goto_0

    .line 775
    .end local v0    # "query":Ljava/lang/String;
    .end local v1    # "uri":Ljava/net/URI;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public run()V
    .locals 14

    .prologue
    .line 5442
    const/4 v6, 0x0

    .line 5443
    .local v6, "status":I
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v8

    sput v8, Lcom/android/exchange/EasSyncService;->mCurrentUserId:I

    .line 5446
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->setupService()Z

    move-result v8

    if-nez v8, :cond_1

    .line 5725
    :cond_0
    :goto_0
    return-void

    .line 5450
    :cond_1
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_0

    .line 5453
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v8, :cond_0

    .line 5467
    :try_start_0
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 5469
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v8, :cond_c

    .line 5577
    :cond_2
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_4

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .local v3, "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_7

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 5584
    :cond_3
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_9

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_0

    .line 5649
    :pswitch_0
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_2
    :try_start_1
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_5

    const/16 v8, 0x20

    if-ne v6, v8, :cond_5

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_5

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_5
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_b

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 5716
    :goto_3
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_6

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_6
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5586
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_7
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_1
    if-nez v6, :cond_8

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_8
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 5608
    :pswitch_2
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .local v5, "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_2

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_3
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_2

    .line 5621
    :pswitch_4
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_2

    .line 5629
    :pswitch_5
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_2

    .line 5633
    :pswitch_6
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_2

    .line 5637
    :pswitch_7
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_2

    .line 5642
    :pswitch_8
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_2

    .line 5646
    :pswitch_9
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_2

    .line 5654
    :cond_9
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_a

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_a

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_a
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_1

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 5663
    :pswitch_a
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5669
    :pswitch_b
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5677
    :pswitch_c
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_b
    :try_start_2
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    .line 5711
    :catch_0
    move-exception v8

    goto/16 :goto_3

    .line 5474
    :cond_c
    :try_start_3
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x42

    if-eq v8, v9, :cond_d

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x53

    if-ne v8, v9, :cond_16

    .line 5475
    :cond_d
    new-instance v7, Lcom/android/exchange/adapter/ContactsSyncAdapter;

    invoke-direct {v7, p0}, Lcom/android/exchange/adapter/ContactsSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 5499
    .local v7, "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_e
    :goto_4
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mRequestTime:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_f

    .line 5500
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Looping for user request..."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5501
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/android/exchange/EasSyncService;->mRequestTime:J

    .line 5504
    :cond_f
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_10

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_10

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x4

    if-eq v8, v9, :cond_10

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x3

    if-eq v8, v9, :cond_10

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    const/4 v9, 0x1

    if-ne v8, v9, :cond_11

    .line 5508
    :cond_10
    :try_start_4
    const-string v8, "Deepak"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "EasSyncService run mailboxId "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " progress IN_PROGRESS 0"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mSyncReason "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5510
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x1

    const/4 v12, 0x0

    invoke-interface {v8, v10, v11, v9, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 5519
    :cond_11
    :goto_5
    :try_start_5
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x53

    if-ne v8, v9, :cond_20

    .line 5520
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Contact SubFolder, BEFORE lock mailboxId"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5521
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->getSubFolderContactAccountObjectFromMap(J)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 5523
    :try_start_6
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Contact SubFolder, AFTER lock mailboxId "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5524
    invoke-virtual {p0, v7}, Lcom/android/exchange/EasSyncService;->sync(Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 5525
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Contact SubFolder, AFTER SYNC complete mailboxId "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5526
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 5543
    :goto_6
    :try_start_7
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mRequestTime:J
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_7 .. :try_end_7} :catch_8
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-nez v8, :cond_e

    .line 5577
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_13

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_2d

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_2d

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_12

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2d

    .line 5584
    :cond_12
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_13
    :goto_7
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_2f

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_2

    .line 5649
    :pswitch_d
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_8
    :try_start_8
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_14

    const/16 v8, 0x20

    if-ne v6, v8, :cond_14

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_14

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_14
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_31

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_4

    .line 5716
    :goto_9
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_15

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_15
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5476
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_16
    :try_start_9
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x41

    if-eq v8, v9, :cond_17

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x52

    if-ne v8, v9, :cond_18

    .line 5478
    :cond_17
    new-instance v7, Lcom/android/exchange/adapter/CalendarSyncAdapter;

    invoke-direct {v7, p0}, Lcom/android/exchange/adapter/CalendarSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto/16 :goto_4

    .line 5479
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_18
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x43

    if-ne v8, v9, :cond_19

    .line 5480
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v9, "Syncing Tasks.."

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5481
    new-instance v7, Lcom/android/exchange/adapter/TasksSyncAdapter;

    invoke-direct {v7, p0}, Lcom/android/exchange/adapter/TasksSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto/16 :goto_4

    .line 5485
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_19
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x45

    if-ne v8, v9, :cond_1a

    .line 5486
    new-instance v7, Lcom/android/exchange/adapter/NotesSyncAdapter;

    invoke-direct {v7, p0}, Lcom/android/exchange/adapter/NotesSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto/16 :goto_4

    .line 5489
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_1a
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x61

    if-ne v8, v9, :cond_1b

    .line 5490
    new-instance v7, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    invoke-direct {v7, v8, p0}, Lcom/android/exchange/adapter/RecipientInformationcacheSyncAdapter;-><init>(Lcom/android/emailcommon/provider/EmailContent$Mailbox;Lcom/android/exchange/EasSyncService;)V

    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto/16 :goto_4

    .line 5493
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_1b
    new-instance v7, Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-direct {v7, p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V
    :try_end_9
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    goto/16 :goto_4

    .line 5526
    :catchall_0
    move-exception v8

    :try_start_a
    monitor-exit v9
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    throw v8
    :try_end_b
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_b .. :try_end_b} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_b .. :try_end_b} :catch_8
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 5545
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :catch_1
    move-exception v2

    .line 5546
    .local v2, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :try_start_c
    const-string v8, "DeviceAccessPermission"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Caught Exceptoin, Device is blocked or quarantined "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/android/emailcommon/utility/DeviceAccessException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 5548
    const/4 v8, 0x6

    iput v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 5549
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 5577
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_1d

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_32

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_32

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1c

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_32

    .line 5584
    :cond_1c
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_1d
    :goto_a
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_34

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_3

    .line 5649
    :pswitch_e
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_b
    :try_start_d
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_1e

    const/16 v8, 0x20

    if-ne v6, v8, :cond_1e

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_1e

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_1e
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_36

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_5

    .line 5716
    :goto_c
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_1f

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_1f
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5530
    .end local v2    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_20
    :try_start_e
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x52

    if-ne v8, v9, :cond_25

    .line 5531
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Calendar SubFolder, BEFORE lock mailboxId"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5532
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v9}, Lcom/android/exchange/ExchangeService;->getSubFolderContactAccountObjectFromMap(J)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_e
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_e .. :try_end_e} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_e .. :try_end_e} :catch_8
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_a
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 5534
    :try_start_f
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Calendar SubFolder, AFTER lock mailboxId "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5535
    invoke-virtual {p0, v7}, Lcom/android/exchange/EasSyncService;->sync(Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 5536
    sget-object v8, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Calendar SubFolder, AFTER SYNC complete mailboxId "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v12, v11, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5537
    monitor-exit v9

    goto/16 :goto_6

    :catchall_1
    move-exception v8

    monitor-exit v9
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    throw v8
    :try_end_10
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_10 .. :try_end_10} :catch_8
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 5550
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :catch_2
    move-exception v2

    .line 5551
    .local v2, "e":Lcom/android/exchange/EasAuthenticationException;
    const/4 v8, 0x1

    :try_start_11
    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Caught authentication error"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5552
    const/4 v8, 0x2

    iput v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 5577
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_22

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_37

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_37

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_21

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_37

    .line 5584
    :cond_21
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_22
    :goto_d
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_39

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_4

    .line 5649
    :pswitch_f
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_e
    :try_start_12
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_23

    const/16 v8, 0x20

    if-ne v6, v8, :cond_23

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_23

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_23
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_3b

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_12} :catch_6

    .line 5716
    :goto_f
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_24

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_24
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5540
    .end local v2    # "e":Lcom/android/exchange/EasAuthenticationException;
    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_25
    :try_start_13
    invoke-virtual {p0, v7}, Lcom/android/exchange/EasSyncService;->sync(Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    :try_end_13
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_13 .. :try_end_13} :catch_1
    .catch Lcom/android/exchange/EasAuthenticationException; {:try_start_13 .. :try_end_13} :catch_2
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_3
    .catch Landroid/database/sqlite/SQLiteDiskIOException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_a
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    goto/16 :goto_6

    .line 5553
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :catch_3
    move-exception v2

    .line 5554
    .local v2, "e":Ljava/io/IOException;
    :try_start_14
    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 5555
    .local v4, "message":Ljava/lang/String;
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Caught IOException: "

    aput-object v10, v8, v9

    const/4 v9, 0x1

    if-nez v4, :cond_26

    const-string v4, "No message"

    .end local v4    # "message":Ljava/lang/String;
    :cond_26
    aput-object v4, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5559
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-lt v8, v9, :cond_27

    .line 5560
    invoke-static {v2}, Lcom/android/emailcommon/service/EmailServiceStatus;->decodeIOException(Ljava/io/IOException;)I

    move-result v6

    .line 5563
    :cond_27
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_28

    .line 5564
    const/4 v8, 0x1

    iput v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 5577
    :cond_28
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_2a

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_3c

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_3c

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_29

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3c

    .line 5584
    :cond_29
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_2a
    :goto_10
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_3e

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_5

    .line 5649
    :pswitch_10
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_11
    :try_start_15
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_2b

    const/16 v8, 0x20

    if-ne v6, v8, :cond_2b

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_2b

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_2b
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_40

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_7

    .line 5716
    :goto_12
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_2c

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_2c
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5586
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :cond_2d
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_11
    if-nez v6, :cond_2e

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_2e
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 5608
    :pswitch_12
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_8

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_13
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_8

    .line 5621
    :pswitch_14
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_8

    .line 5629
    :pswitch_15
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_8

    .line 5633
    :pswitch_16
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_8

    .line 5637
    :pswitch_17
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_8

    .line 5642
    :pswitch_18
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_8

    .line 5646
    :pswitch_19
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_8

    .line 5654
    :cond_2f
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_30

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_30

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_30
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_6

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 5663
    :pswitch_1a
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 5669
    :pswitch_1b
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 5677
    :pswitch_1c
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_31
    :try_start_16
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_16} :catch_4

    goto/16 :goto_9

    .line 5711
    :catch_4
    move-exception v8

    goto/16 :goto_9

    .line 5586
    .end local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .local v2, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_32
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_1d
    if-nez v6, :cond_33

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_33
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 5608
    :pswitch_1e
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_b

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_1f
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_b

    .line 5621
    :pswitch_20
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_b

    .line 5629
    :pswitch_21
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_b

    .line 5633
    :pswitch_22
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_b

    .line 5637
    :pswitch_23
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_b

    .line 5642
    :pswitch_24
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_b

    .line 5646
    :pswitch_25
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_b

    .line 5654
    :cond_34
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_35

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_35

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_35
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_7

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_b

    .line 5663
    :pswitch_26
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 5669
    :pswitch_27
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 5677
    :pswitch_28
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_36
    :try_start_17
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_5

    goto/16 :goto_c

    .line 5711
    :catch_5
    move-exception v8

    goto/16 :goto_c

    .line 5586
    .local v2, "e":Lcom/android/exchange/EasAuthenticationException;
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_37
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_d

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_29
    if-nez v6, :cond_38

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_38
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 5608
    :pswitch_2a
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_e

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_2b
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_e

    .line 5621
    :pswitch_2c
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_e

    .line 5629
    :pswitch_2d
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_e

    .line 5633
    :pswitch_2e
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_e

    .line 5637
    :pswitch_2f
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_e

    .line 5642
    :pswitch_30
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_e

    .line 5646
    :pswitch_31
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_e

    .line 5654
    :cond_39
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_3a

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_3a

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_3a
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_8

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_e

    .line 5663
    :pswitch_32
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 5669
    :pswitch_33
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 5677
    :pswitch_34
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_e

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_3b
    :try_start_18
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_6

    goto/16 :goto_f

    .line 5711
    :catch_6
    move-exception v8

    goto/16 :goto_f

    .line 5586
    .local v2, "e":Ljava/io/IOException;
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_3c
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_35
    if-nez v6, :cond_3d

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_3d
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 5608
    :pswitch_36
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_11

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_37
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_11

    .line 5621
    :pswitch_38
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_11

    .line 5629
    :pswitch_39
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_11

    .line 5633
    :pswitch_3a
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_11

    .line 5637
    :pswitch_3b
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_11

    .line 5642
    :pswitch_3c
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_11

    .line 5646
    :pswitch_3d
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_11

    .line 5654
    :cond_3e
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_3f

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_3f

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_3f
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_9

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_11

    .line 5663
    :pswitch_3e
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 5669
    :pswitch_3f
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 5677
    :pswitch_40
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_40
    :try_start_19
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_7

    goto/16 :goto_12

    .line 5711
    :catch_7
    move-exception v8

    goto/16 :goto_12

    .line 5569
    .end local v2    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v2

    .line 5570
    .local v2, "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :try_start_1a
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDiskIOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 5571
    .restart local v4    # "message":Ljava/lang/String;
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Caught SQLiteDisk IOException: "

    aput-object v10, v8, v9

    const/4 v9, 0x1

    if-nez v4, :cond_41

    const-string v4, "No message"

    .end local v4    # "message":Ljava/lang/String;
    :cond_41
    aput-object v4, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5572
    const/16 v8, 0x9

    iput v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    .line 5577
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_43

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_46

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_46

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_42

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_46

    .line 5584
    :cond_42
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_43
    :goto_13
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_48

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_a

    .line 5649
    :pswitch_41
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_14
    :try_start_1b
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_44

    const/16 v8, 0x20

    if-ne v6, v8, :cond_44

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_44

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_44
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_4a

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_9

    .line 5716
    :goto_15
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_45

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_45
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5586
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_46
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_42
    if-nez v6, :cond_47

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_47
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_14

    .line 5608
    :pswitch_43
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_14

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_44
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_14

    .line 5621
    :pswitch_45
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_14

    .line 5629
    :pswitch_46
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_14

    .line 5633
    :pswitch_47
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_14

    .line 5637
    :pswitch_48
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_14

    .line 5642
    :pswitch_49
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_14

    .line 5646
    :pswitch_4a
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_14

    .line 5654
    :cond_48
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_49

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_49

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_49
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_b

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_14

    .line 5663
    :pswitch_4b
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 5669
    :pswitch_4c
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 5677
    :pswitch_4d
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_4a
    :try_start_1c
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1c
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_9

    goto/16 :goto_15

    .line 5711
    :catch_9
    move-exception v8

    goto/16 :goto_15

    .line 5574
    .end local v2    # "e":Landroid/database/sqlite/SQLiteDiskIOException;
    :catch_a
    move-exception v2

    .line 5575
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1d
    const-string v8, "Uncaught exception in EasSyncService"

    invoke-virtual {p0, v8, v2}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    .line 5577
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v8, :cond_4c

    .line 5578
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "thread="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mAccount="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mMailbox="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cmd="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mSyncReason="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " syncKey="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " exception="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mStop="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v9, 0x44

    if-ne v8, v9, :cond_4f

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v8, :cond_4f

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Ping"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4b

    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v9, "Sync"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4f

    .line 5584
    :cond_4b
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_4c
    :goto_16
    iget-boolean v8, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v8, :cond_51

    .line 5591
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Sync finished"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "<"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ">"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Sync finished exit status :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_c

    .line 5649
    :pswitch_4e
    const/16 v6, 0x15

    .line 5650
    const-string v8, "Sync ended due to an exception."

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_17
    :try_start_1e
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v9, 0x5

    if-ge v8, v9, :cond_4d

    const/16 v8, 0x20

    if-ne v6, v8, :cond_4d

    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_4d

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_4d
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v9, 0x8

    if-ne v8, v9, :cond_53

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v9, -0x20

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1e
    .catch Landroid/os/RemoteException; {:try_start_1e .. :try_end_1e} :catch_b

    .line 5716
    :goto_18
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Send intent about finished sync to SocialHub"

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v8, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v8, :cond_4e

    .line 5718
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v8, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_4e
    const-string v8, "sync finished"

    invoke-static {v8}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5586
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_4f
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v8

    invoke-virtual {v8, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_16

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_4f
    if-nez v6, :cond_50

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_50
    const-string v8, "InitialSync"

    const-string v9, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_17

    .line 5608
    :pswitch_50
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v8, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0x3a

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v8, "syncStatus"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v9, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v1, v10, v11}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v8, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v8, v8, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v10, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v8, v9, v10}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_17

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_51
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_17

    .line 5621
    :pswitch_52
    const/16 v6, 0x17

    .line 5626
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v9, 0x1

    invoke-static {v8, v10, v11, v9}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_17

    .line 5629
    :pswitch_53
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_17

    .line 5633
    :pswitch_54
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_17

    .line 5637
    :pswitch_55
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_17

    .line 5642
    :pswitch_56
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_17

    .line 5646
    :pswitch_57
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_17

    .line 5654
    :cond_51
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "Stopped sync finished."

    aput-object v10, v8, v9

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v8, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_52

    iget v8, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v9, -0x2

    if-eq v8, v9, :cond_52

    .line 5659
    iget-wide v8, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v10, 0x1f40

    invoke-static {v8, v9, v10, v11}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_52
    iget v8, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v8, :pswitch_data_d

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_17

    .line 5663
    :pswitch_58
    const/4 v6, 0x3

    .line 5664
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server block this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 5669
    :pswitch_59
    const/4 v6, 0x4

    .line 5670
    const-string v8, "DeviceAccessPermission"

    const-string v9, "Service is stopped as server qurantined this device"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 5677
    :pswitch_5a
    const/16 v6, 0x20

    .line 5678
    const-string v8, "Connection Error "

    const-string v9, "Service is stopped as Network Connectivity Failed"

    invoke-static {v8, v9}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_17

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_53
    :try_start_1f
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v6, v9}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_1f
    .catch Landroid/os/RemoteException; {:try_start_1f .. :try_end_1f} :catch_b

    goto/16 :goto_18

    .line 5711
    :catch_b
    move-exception v8

    goto/16 :goto_18

    .line 5577
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v8

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    if-eqz v9, :cond_55

    .line 5578
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "thread="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mAccount="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v10, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mMailbox="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v10, v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " cmd="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mSyncReason="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " syncKey="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " exception="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mExceptionString:[Ljava/lang/String;

    iget v11, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mStop="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-boolean v10, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5582
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v10, 0x44

    if-ne v9, v10, :cond_58

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    if-eqz v9, :cond_58

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v10, "Ping"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_54

    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    const-string v10, "Sync"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_58

    .line 5584
    :cond_54
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/android/exchange/ServiceLogger;->logPingStats(Ljava/lang/String;)V

    .line 5590
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_55
    :goto_19
    iget-boolean v9, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v9, :cond_5a

    .line 5591
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "Sync finished"

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5592
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->done(Lcom/android/exchange/AbstractSyncService;)V

    .line 5594
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "<"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ">"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Sync finished exit status :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5597
    iget v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v9, :pswitch_data_e

    .line 5649
    :pswitch_5b
    const/16 v6, 0x15

    .line 5650
    const-string v9, "Sync ended due to an exception."

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 5696
    :goto_1a
    :try_start_20
    iget v9, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    const/4 v10, 0x5

    if-ge v9, v10, :cond_56

    const/16 v9, 0x20

    if-ne v6, v9, :cond_56

    iget v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v10, 0x8

    if-eq v9, v10, :cond_56

    .line 5699
    const/4 v6, 0x0

    .line 5704
    :cond_56
    iget v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    const/16 v10, 0x8

    if-ne v9, v10, :cond_5c

    .line 5705
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v9

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/16 v12, -0x20

    invoke-interface {v9, v10, v11, v6, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_20
    .catch Landroid/os/RemoteException; {:try_start_20 .. :try_end_20} :catch_c

    .line 5716
    :goto_1b
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "Send intent about finished sync to SocialHub"

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5717
    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v9, v9, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v9, :cond_57

    .line 5718
    sget-object v9, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-direct {p0, v9, v10, v11}, Lcom/android/exchange/EasSyncService;->sendBroadcastSyncCompleted(Landroid/content/Context;J)V

    .line 5722
    :cond_57
    const-string v9, "sync finished"

    invoke-static {v9}, Lcom/android/exchange/ExchangeService;->kick(Ljava/lang/String;)V

    throw v8

    .line 5586
    .restart local v3    # "loggingPhrase":Ljava/lang/String;
    :cond_58
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v9

    invoke-virtual {v9, v3}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto/16 :goto_19

    .line 5601
    .end local v3    # "loggingPhrase":Ljava/lang/String;
    :pswitch_5c
    if-nez v6, :cond_59

    .line 5602
    const/16 v6, 0x20

    .line 5605
    :cond_59
    const-string v9, "InitialSync"

    const-string v10, "In EasSyncService: Inside run(): EXIT_IO_ERROR"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1a

    .line 5608
    :pswitch_5d
    const/4 v6, 0x0

    .line 5609
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 5610
    .restart local v1    # "cv":Landroid/content/ContentValues;
    const-string v9, "syncTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 5611
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "S"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/android/exchange/EasSyncService;->mChangeCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5612
    .restart local v5    # "s":Ljava/lang/String;
    const-string v9, "syncStatus"

    invoke-virtual {v1, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5613
    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v10, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v10, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v1, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5615
    iget-object v9, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v9, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_ALLOWED:I

    invoke-static {v10, v11, v9}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V

    goto/16 :goto_1a

    .line 5618
    .end local v1    # "cv":Landroid/content/ContentValues;
    .end local v5    # "s":Ljava/lang/String;
    :pswitch_5e
    const/16 v6, 0x16

    .line 5619
    goto/16 :goto_1a

    .line 5621
    :pswitch_5f
    const/16 v6, 0x17

    .line 5626
    sget-object v9, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    const/4 v12, 0x1

    invoke-static {v9, v10, v11, v12}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    goto/16 :goto_1a

    .line 5629
    :pswitch_60
    const/4 v6, 0x3

    .line 5630
    goto/16 :goto_1a

    .line 5633
    :pswitch_61
    const/4 v6, 0x4

    .line 5634
    goto/16 :goto_1a

    .line 5637
    :pswitch_62
    const/16 v6, 0xad

    .line 5638
    goto/16 :goto_1a

    .line 5642
    :pswitch_63
    const/high16 v6, 0x110000

    .line 5643
    goto/16 :goto_1a

    .line 5646
    :pswitch_64
    const/16 v6, 0x56

    .line 5647
    goto/16 :goto_1a

    .line 5654
    :cond_5a
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "Stopped sync finished."

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5656
    sget-object v9, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v9, v10, v11}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    .line 5657
    .restart local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v0, :cond_5b

    iget v9, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncInterval:I

    const/4 v10, -0x2

    if-eq v9, v10, :cond_5b

    .line 5659
    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const-wide/16 v12, 0x1f40

    invoke-static {v10, v11, v12, v13}, Lcom/android/exchange/ExchangeService;->runAsleep(JJ)V

    .line 5661
    :cond_5b
    iget v9, p0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    packed-switch v9, :pswitch_data_f

    .line 5685
    const/4 v6, 0x0

    goto/16 :goto_1a

    .line 5663
    :pswitch_65
    const/4 v6, 0x3

    .line 5664
    const-string v9, "DeviceAccessPermission"

    const-string v10, "Service is stopped as server block this device"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 5669
    :pswitch_66
    const/4 v6, 0x4

    .line 5670
    const-string v9, "DeviceAccessPermission"

    const-string v10, "Service is stopped as server qurantined this device"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 5677
    :pswitch_67
    const/16 v6, 0x20

    .line 5678
    const-string v9, "Connection Error "

    const-string v10, "Service is stopped as Network Connectivity Failed"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 5708
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_5c
    :try_start_21
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v9

    iget-wide v10, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    const/4 v12, 0x0

    invoke-interface {v9, v10, v11, v6, v12}, Lcom/android/emailcommon/service/IEmailServiceCallback;->syncMailboxStatus(JII)V
    :try_end_21
    .catch Landroid/os/RemoteException; {:try_start_21 .. :try_end_21} :catch_c

    goto/16 :goto_1b

    .line 5711
    :catch_c
    move-exception v9

    goto/16 :goto_1b

    .line 5512
    .restart local v7    # "target":Lcom/android/exchange/adapter/AbstractSyncAdapter;
    :catch_d
    move-exception v8

    goto/16 :goto_5

    .line 5597
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_7
        :pswitch_9
    .end packed-switch

    .line 5661
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 5597
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_12
        :pswitch_11
        :pswitch_13
        :pswitch_d
        :pswitch_14
        :pswitch_d
        :pswitch_15
        :pswitch_16
        :pswitch_d
        :pswitch_18
        :pswitch_d
        :pswitch_17
        :pswitch_19
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_1d
        :pswitch_1f
        :pswitch_e
        :pswitch_20
        :pswitch_e
        :pswitch_21
        :pswitch_22
        :pswitch_e
        :pswitch_24
        :pswitch_e
        :pswitch_23
        :pswitch_25
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_29
        :pswitch_2b
        :pswitch_f
        :pswitch_2c
        :pswitch_f
        :pswitch_2d
        :pswitch_2e
        :pswitch_f
        :pswitch_30
        :pswitch_f
        :pswitch_2f
        :pswitch_31
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_36
        :pswitch_35
        :pswitch_37
        :pswitch_10
        :pswitch_38
        :pswitch_10
        :pswitch_39
        :pswitch_3a
        :pswitch_10
        :pswitch_3c
        :pswitch_10
        :pswitch_3b
        :pswitch_3d
    .end packed-switch

    .line 5661
    :pswitch_data_6
    .packed-switch 0x6
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x6
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x6
        :pswitch_32
        :pswitch_33
        :pswitch_34
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x6
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
    .end packed-switch

    .line 5597
    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_43
        :pswitch_42
        :pswitch_44
        :pswitch_41
        :pswitch_45
        :pswitch_41
        :pswitch_46
        :pswitch_47
        :pswitch_41
        :pswitch_49
        :pswitch_41
        :pswitch_48
        :pswitch_4a
    .end packed-switch

    .line 5661
    :pswitch_data_b
    .packed-switch 0x6
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
    .end packed-switch

    .line 5597
    :pswitch_data_c
    .packed-switch 0x0
        :pswitch_50
        :pswitch_4f
        :pswitch_51
        :pswitch_4e
        :pswitch_52
        :pswitch_4e
        :pswitch_53
        :pswitch_54
        :pswitch_4e
        :pswitch_56
        :pswitch_4e
        :pswitch_55
        :pswitch_57
    .end packed-switch

    .line 5661
    :pswitch_data_d
    .packed-switch 0x6
        :pswitch_58
        :pswitch_59
        :pswitch_5a
    .end packed-switch

    .line 5597
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_5d
        :pswitch_5c
        :pswitch_5e
        :pswitch_5b
        :pswitch_5f
        :pswitch_5b
        :pswitch_60
        :pswitch_61
        :pswitch_5b
        :pswitch_63
        :pswitch_5b
        :pswitch_62
        :pswitch_64
    .end packed-switch

    .line 5661
    :pswitch_data_f
    .packed-switch 0x6
        :pswitch_65
        :pswitch_66
        :pswitch_67
    .end packed-switch
.end method

.method public sendDeviceInformation(Landroid/content/Context;Lcom/android/exchange/adapter/Serializer;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "s"    # Lcom/android/exchange/adapter/Serializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 5848
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 5849
    :cond_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Context cannot be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 5851
    :cond_1
    const-string v4, "Settings"

    invoke-virtual {p2}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v3

    .line 5853
    .local v3, "resp":Lcom/android/exchange/EasResponse;
    if-nez v3, :cond_3

    .line 5854
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "unable to get response from the server"

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 5855
    const/4 v0, -0x1

    .line 5883
    :cond_2
    :goto_0
    return v0

    .line 5858
    :cond_3
    const/4 v0, -0x1

    .line 5861
    .local v0, "code":I
    :try_start_0
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v0

    .line 5862
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Settings response = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 5863
    const/16 v4, 0xc8

    if-ne v0, v4, :cond_4

    .line 5864
    new-instance v2, Lcom/android/exchange/adapter/SettingsParser;

    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/android/exchange/adapter/SettingsParser;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5866
    .local v2, "p":Lcom/android/exchange/adapter/SettingsParser;
    :try_start_1
    invoke-virtual {v2}, Lcom/android/exchange/adapter/SettingsParser;->parse()Z

    move-result v4

    if-nez v4, :cond_5

    .line 5867
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Server responded with error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/exchange/adapter/SettingsParser;->getStatus()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for settings command"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 5869
    invoke-virtual {v2}, Lcom/android/exchange/adapter/SettingsParser;->getStatus()I
    :try_end_1
    .catch Lcom/android/exchange/EasException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 5879
    .end local v0    # "code":I
    if-eqz v3, :cond_2

    .line 5880
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_0

    .line 5871
    .restart local v0    # "code":I
    :catch_0
    move-exception v1

    .line 5872
    .local v1, "e":Lcom/android/exchange/EasException;
    :try_start_2
    invoke-virtual {v1}, Lcom/android/exchange/EasException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5873
    const/4 v0, -0x2

    .line 5879
    .end local v0    # "code":I
    if-eqz v3, :cond_2

    .line 5880
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_0

    .line 5876
    .end local v1    # "e":Lcom/android/exchange/EasException;
    .end local v2    # "p":Lcom/android/exchange/adapter/SettingsParser;
    .restart local v0    # "code":I
    :cond_4
    :try_start_3
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "unable to send device information to the server"

    invoke-static {v4, v5}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 5879
    :cond_5
    if-eqz v3, :cond_2

    .line 5880
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    goto :goto_0

    .line 5879
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_6

    .line 5880
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    :cond_6
    throw v4
.end method

.method protected sendHttpClientOptions()Lcom/android/exchange/EasResponse;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 4350
    sget v8, Lcom/android/exchange/EasSyncService;->COMMAND_TIMEOUT:I

    invoke-direct {p0, v8}, Lcom/android/exchange/EasSyncService;->getHttpClient(I)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    .line 4353
    .local v0, "client":Lorg/apache/http/client/HttpClient;
    const-string v8, "OPTIONS"

    iput-object v8, p0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    .line 4354
    iput-object v13, p0, Lcom/android/exchange/EasSyncService;->mASCmdParams:Ljava/lang/String;

    .line 4356
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->makeUriString()Ljava/lang/String;

    move-result-object v7

    .line 4358
    .local v7, "us":Ljava/lang/String;
    new-instance v3, Lorg/apache/http/client/methods/HttpOptions;

    invoke-static {v7}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v8

    invoke-direct {v3, v8}, Lorg/apache/http/client/methods/HttpOptions;-><init>(Ljava/net/URI;)V

    .line 4360
    .local v3, "method":Lorg/apache/http/client/methods/HttpOptions;
    invoke-virtual {p0, v3, v11}, Lcom/android/exchange/EasSyncService;->setHeaders(Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 4364
    sget-boolean v8, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v8, :cond_2

    .line 4366
    new-array v8, v12, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendHttpClientOptions(): URI String:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4368
    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpOptions;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v2

    .line 4370
    .local v2, "it":Lorg/apache/http/HeaderIterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 4372
    invoke-interface {v2}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v1

    .line 4374
    .local v1, "ht":Lorg/apache/http/Header;
    if-eqz v1, :cond_1

    const-string v8, "Authorization"

    invoke-interface {v1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 4375
    new-array v8, v12, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "***********"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 4378
    :cond_1
    if-eqz v1, :cond_0

    .line 4379
    new-array v8, v12, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {p0, v8}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_0

    .line 4391
    .end local v1    # "ht":Lorg/apache/http/Header;
    .end local v2    # "it":Lorg/apache/http/HeaderIterator;
    :cond_2
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 4393
    .local v4, "proxyHost":Ljava/lang/String;
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v5

    .line 4395
    .local v5, "proxyPort":I
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    if-ltz v5, :cond_3

    .line 4397
    sget-object v8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v8, v0, v3, v4, v5}, Lcom/android/exchange/EasSyncService;->addProxyParamsIfProxySet(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V

    .line 4402
    :cond_3
    new-instance v6, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;

    invoke-direct {v6, p0, v13}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;-><init>(Lcom/android/exchange/EasSyncService;Lcom/android/exchange/EasSyncService$1;)V

    .line 4403
    .local v6, "redirectionHandler":Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;
    invoke-virtual {v6, v0, v3}, Lcom/android/exchange/EasSyncService$ASHTTPRedirectHandler;->execOption(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpOptions;)Lcom/android/exchange/EasResponse;

    move-result-object v8

    return-object v8
.end method

.method protected sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;
    .locals 1
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .param p3, "timeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3934
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;IZ)Lcom/android/exchange/EasResponse;

    move-result-object v0

    return-object v0
.end method

.method protected sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;IZ)Lcom/android/exchange/EasResponse;
    .locals 36
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/HttpEntity;
    .param p3, "timeout"    # I
    .param p4, "isHBISyncCommand"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3941
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendHttpClientPost: isHBISyncCommand = "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p4

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 3944
    const-string v5, "Ping"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz p4, :cond_15

    :cond_0
    const/4 v14, 0x1

    .line 3947
    .local v14, "isPingCommand":Z
    :goto_0
    const-string v5, "ItemOperations"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "Sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v32

    const-wide/high16 v34, 0x4004000000000000L    # 2.5

    cmpg-double v5, v32, v34

    if-gtz v5, :cond_16

    :cond_1
    const/16 v21, 0x1

    .line 3950
    .local v21, "isLoadMoreCommand":Z
    :goto_1
    const-string v5, "SendMail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "SmartReply"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "SmartForward"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_17

    :cond_2
    const/16 v23, 0x1

    .line 3957
    .local v23, "isOutboxSendMailCommand":Z
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v14}, Lcom/android/exchange/EasSyncService;->getClient(Ljava/lang/String;IZ)Lorg/apache/http/client/HttpClient;

    move-result-object v11

    .line 3958
    .local v11, "client":Lorg/apache/http/client/HttpClient;
    if-nez v21, :cond_3

    if-nez v23, :cond_3

    const-string v5, "ItemOperations"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3960
    const-string p1, "ItemOperations"

    .line 3968
    :cond_3
    const-string v5, "SendMail"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "SmartReply"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "SmartForward"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    move-object v5, v11

    .line 3969
    check-cast v5, Lorg/apache/http/impl/client/AbstractHttpClient;

    new-instance v6, Lcom/android/exchange/EasSyncService$5;

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v8, v10}, Lcom/android/exchange/EasSyncService$5;-><init>(Lcom/android/exchange/EasSyncService;IZ)V

    invoke-virtual {v5, v6}, Lorg/apache/http/impl/client/AbstractHttpClient;->setHttpRequestRetryHandler(Lorg/apache/http/client/HttpRequestRetryHandler;)V

    .line 3996
    :cond_5
    const-string v5, "GetAttachment&AttachmentName="

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    .line 3998
    .local v18, "isGetAttachmentCommand":Z
    const/16 v19, 0x0

    .line 4004
    .local v19, "isGzipRequired":Z
    const-string v5, "ItemOperations"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    .line 4006
    .local v20, "isItemOperationsCommand":Z
    const/16 v22, 0x0

    .line 4008
    .local v22, "isMultipartOn":Z
    if-eqz v20, :cond_7

    .line 4012
    const-string v5, ";"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v30

    .line 4014
    .local v30, "temp":[Ljava/lang/String;
    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v29, v0

    .line 4016
    .local v29, "size":I
    const/4 v5, 0x0

    aget-object p1, v30, v5

    .line 4018
    const/4 v5, 0x2

    move/from16 v0, v29

    if-lt v0, v5, :cond_6

    const/4 v5, 0x1

    aget-object v5, v30, v5

    const-string v6, "MULTIPART"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 4020
    const/16 v22, 0x1

    .line 4022
    :cond_6
    const/4 v5, 0x3

    move/from16 v0, v29

    if-lt v0, v5, :cond_7

    .line 4024
    const/16 v19, 0x1

    .line 4030
    .end local v29    # "size":I
    .end local v30    # "temp":[Ljava/lang/String;
    :cond_7
    const-string v5, "Search"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 4032
    const/16 v19, 0x1

    .line 4037
    :cond_8
    const/16 v28, 0x1

    .line 4041
    .local v28, "setPolicyHeader":Z
    const/4 v7, 0x0

    .line 4043
    .local v7, "extra":Ljava/lang/String;
    const/16 v25, 0x0

    .line 4045
    .local v25, "msg":Z
    const-string v5, "SmartForward&"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_9

    const-string v5, "SmartReply&"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 4047
    :cond_9
    const/16 v5, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v16

    .line 4049
    .local v16, "cmdLength":I
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 4051
    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 4055
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v32

    const-wide/high16 v34, 0x402c000000000000L    # 14.0

    cmpl-double v5, v32, v34

    if-ltz v5, :cond_18

    .line 4057
    const/16 v25, 0x0

    .line 4085
    .end local v16    # "cmdLength":I
    :cond_a
    :goto_3
    const-string v5, "Ping"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "Options"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 4087
    :cond_b
    const/16 v28, 0x0

    .line 4107
    :cond_c
    :goto_4
    if-eqz v18, :cond_d

    .line 4109
    sget-object v5, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sendHttpClientPost cmd for attachment "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 4115
    :cond_d
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v32

    const-string v5, "2.5"

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v34

    cmpl-double v5, v32, v34

    if-nez v5, :cond_e

    .line 4117
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v5, :cond_e

    .line 4119
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    if-eqz v5, :cond_e

    .line 4121
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 4123
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v32

    invoke-static/range {v32 .. v33}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 4131
    :cond_e
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "mProtocolVersion: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " - cmd: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4135
    const-string v9, "0"

    .line 4137
    .local v9, "policyKey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSecuritySyncKey:Ljava/lang/String;

    if-eqz v5, :cond_f

    .line 4139
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v9, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mSecuritySyncKey:Ljava/lang/String;

    .line 4141
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PolicyKey is set. PolicyKey:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4149
    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v32

    const-wide v34, 0x402c333333333333L    # 14.1

    cmpg-double v5, v32, v34

    if-lez v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x10

    if-gt v5, v6, :cond_10

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDeviceType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x10

    if-le v5, v6, :cond_1d

    .line 4171
    :cond_10
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    .line 4172
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/android/exchange/EasSyncService;->mASCmdParams:Ljava/lang/String;

    .line 4174
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->makeUriString()Ljava/lang/String;

    move-result-object v31

    .line 4178
    .local v31, "us":Ljava/lang/String;
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_11

    .line 4180
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendHttpClientPost:URI:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4184
    :cond_11
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    invoke-static/range {v31 .. v31}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v5

    invoke-direct {v12, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 4189
    .local v12, "method":Lorg/apache/http/client/methods/HttpPost;
    if-eqz v14, :cond_1c

    .line 4190
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v12, v1}, Lcom/android/exchange/EasSyncService;->setHeadersPingCommand(Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    .line 4240
    :goto_5
    if-eqz v18, :cond_12

    .line 4241
    const-string v5, "Accept-Encoding"

    const-string v6, "gzip"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4246
    :cond_12
    if-eqz v25, :cond_24

    .line 4248
    const-string v5, "Content-Type"

    const-string v6, "message/rfc822"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4264
    :cond_13
    :goto_6
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 4268
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_26

    .line 4270
    invoke-virtual {v12}, Lorg/apache/http/client/methods/HttpPost;->headerIterator()Lorg/apache/http/HeaderIterator;

    move-result-object v24

    .line 4272
    .local v24, "it":Lorg/apache/http/HeaderIterator;
    :cond_14
    :goto_7
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_26

    .line 4274
    invoke-interface/range {v24 .. v24}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v17

    .line 4276
    .local v17, "ht":Lorg/apache/http/Header;
    if-eqz v17, :cond_25

    const-string v5, "Authorization"

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_25

    .line 4277
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "***********"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_7

    .line 3944
    .end local v7    # "extra":Ljava/lang/String;
    .end local v9    # "policyKey":Ljava/lang/String;
    .end local v11    # "client":Lorg/apache/http/client/HttpClient;
    .end local v12    # "method":Lorg/apache/http/client/methods/HttpPost;
    .end local v14    # "isPingCommand":Z
    .end local v17    # "ht":Lorg/apache/http/Header;
    .end local v18    # "isGetAttachmentCommand":Z
    .end local v19    # "isGzipRequired":Z
    .end local v20    # "isItemOperationsCommand":Z
    .end local v21    # "isLoadMoreCommand":Z
    .end local v22    # "isMultipartOn":Z
    .end local v23    # "isOutboxSendMailCommand":Z
    .end local v24    # "it":Lorg/apache/http/HeaderIterator;
    .end local v25    # "msg":Z
    .end local v28    # "setPolicyHeader":Z
    .end local v31    # "us":Ljava/lang/String;
    :cond_15
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 3947
    .restart local v14    # "isPingCommand":Z
    :cond_16
    const/16 v21, 0x0

    goto/16 :goto_1

    .line 3950
    .restart local v21    # "isLoadMoreCommand":Z
    :cond_17
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 4061
    .restart local v7    # "extra":Ljava/lang/String;
    .restart local v11    # "client":Lorg/apache/http/client/HttpClient;
    .restart local v16    # "cmdLength":I
    .restart local v18    # "isGetAttachmentCommand":Z
    .restart local v19    # "isGzipRequired":Z
    .restart local v20    # "isItemOperationsCommand":Z
    .restart local v22    # "isMultipartOn":Z
    .restart local v23    # "isOutboxSendMailCommand":Z
    .restart local v25    # "msg":Z
    .restart local v28    # "setPolicyHeader":Z
    :cond_18
    const/16 v25, 0x1

    goto/16 :goto_3

    .line 4067
    .end local v16    # "cmdLength":I
    :cond_19
    const-string v5, "SendMail&"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 4071
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v5, v5, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v32

    const-wide/high16 v34, 0x402c000000000000L    # 14.0

    cmpl-double v5, v32, v34

    if-ltz v5, :cond_1a

    .line 4073
    const/16 v25, 0x0

    goto/16 :goto_3

    .line 4077
    :cond_1a
    const/16 v25, 0x1

    goto/16 :goto_3

    .line 4089
    :cond_1b
    const-string v5, "NHProvision"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 4095
    const/16 v28, 0x0

    .line 4101
    const-string p1, "Provision"

    goto/16 :goto_4

    .line 4192
    .restart local v9    # "policyKey":Ljava/lang/String;
    .restart local v12    # "method":Lorg/apache/http/client/methods/HttpPost;
    .restart local v31    # "us":Ljava/lang/String;
    :cond_1c
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v12, v1}, Lcom/android/exchange/EasSyncService;->setHeaders(Lorg/apache/http/client/methods/HttpRequestBase;Z)V

    goto/16 :goto_5

    .line 4198
    .end local v12    # "method":Lorg/apache/http/client/methods/HttpPost;
    .end local v31    # "us":Ljava/lang/String;
    :cond_1d
    new-instance v4, Lcom/android/exchange/adapter/EasBase64EncodedURI;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v8, v10}, Lcom/android/exchange/adapter/EasBase64EncodedURI;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    .line 4202
    .local v4, "base64URI":Lcom/android/exchange/adapter/EasBase64EncodedURI;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getDeviceType()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v6, p1

    invoke-virtual/range {v4 .. v10}, Lcom/android/exchange/adapter/EasBase64EncodedURI;->getUriString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 4208
    .restart local v31    # "us":Ljava/lang/String;
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v5, :cond_1e

    .line 4210
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendHttpClientPost:URI:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 4214
    :cond_1e
    new-instance v12, Lorg/apache/http/client/methods/HttpPost;

    invoke-static/range {v31 .. v31}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v5

    invoke-direct {v12, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    .line 4217
    .restart local v12    # "method":Lorg/apache/http/client/methods/HttpPost;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    if-eqz v5, :cond_1f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mCmdString:Ljava/lang/String;

    if-nez v5, :cond_20

    .line 4218
    :cond_1f
    invoke-direct/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->cacheAuthAndCmdString()V

    .line 4223
    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v5

    if-eqz v5, :cond_21

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    if-eqz v5, :cond_22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_22

    .line 4224
    :cond_21
    const-string v5, "Authorization"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4230
    :cond_22
    if-eqz v14, :cond_23

    .line 4231
    const-string v5, "Connection"

    const-string v6, "close"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 4233
    :cond_23
    const-string v5, "Connection"

    const-string v6, "keep-alive"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 4250
    .end local v4    # "base64URI":Lcom/android/exchange/adapter/EasBase64EncodedURI;
    :cond_24
    if-eqz p2, :cond_13

    .line 4252
    const-string v5, "Content-Type"

    const-string v6, "application/vnd.ms-sync.wbxml"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 4254
    if-eqz v22, :cond_13

    .line 4256
    const-string v5, "MS-ASAcceptMultiPart"

    const-string v6, "T"

    invoke-virtual {v12, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 4280
    .restart local v17    # "ht":Lorg/apache/http/Header;
    .restart local v24    # "it":Lorg/apache/http/HeaderIterator;
    :cond_25
    if-eqz v17, :cond_14

    .line 4281
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_7

    .line 4292
    .end local v17    # "ht":Lorg/apache/http/Header;
    .end local v24    # "it":Lorg/apache/http/HeaderIterator;
    :cond_26
    const/4 v15, 0x0

    .line 4294
    .local v15, "isAbortNotNeeded":Z
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    .line 4298
    const-string v5, "Sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_27

    const-string v5, "FolderSync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_27

    const-string v5, "Provision"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_27

    const-string v5, "Ping"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_27

    .line 4302
    const/4 v15, 0x1

    .line 4310
    :cond_27
    sget-object v5, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/exchange/utility/ProxyUtils;->getHost(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    .line 4312
    .local v26, "proxyHost":Ljava/lang/String;
    sget-object v5, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/android/exchange/utility/ProxyUtils;->getPort(Landroid/content/Context;)I

    move-result v27

    .line 4314
    .local v27, "proxyPort":I
    if-eqz v26, :cond_28

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_28

    if-ltz v27, :cond_28

    .line 4316
    sget-object v5, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-static {v5, v11, v12, v0, v1}, Lcom/android/exchange/EasSyncService;->addProxyParamsIfProxySet(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpRequestBase;Ljava/lang/String;I)V

    :cond_28
    move-object/from16 v10, p0

    move/from16 v13, p3

    .line 4322
    invoke-virtual/range {v10 .. v15}, Lcom/android/exchange/EasSyncService;->executePostWithTimeout(Lorg/apache/http/client/HttpClient;Lorg/apache/http/client/methods/HttpPost;IZZ)Lcom/android/exchange/EasResponse;

    move-result-object v5

    return-object v5
.end method

.method public sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3432
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v0, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sget v1, Lcom/android/exchange/EasSyncService;->COMMAND_TIMEOUT:I

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v0

    return-object v0
.end method

.method sendMeetingEditedResponseCallback(ZJJ)V
    .locals 8
    .param p1, "success"    # Z
    .param p2, "messageId"    # J
    .param p4, "draftId"    # J

    .prologue
    .line 2916
    if-nez p1, :cond_0

    .line 2917
    :try_start_0
    sget-object v0, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v1, "sending Edit response was failed"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2918
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, p4, p5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2921
    :cond_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/android/emailcommon/service/IEmailServiceCallback;->sendMeetingEditedResponseCallback(ZJJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2933
    :goto_0
    return-void

    .line 2925
    :catch_0
    move-exception v6

    .line 2929
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected sendMeetingResponse(Lcom/android/exchange/MeetingResponseRequest;)V
    .locals 22
    .param p1, "req"    # Lcom/android/exchange/MeetingResponseRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2819
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v17

    .line 2820
    .local v17, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v17, :cond_1

    .line 2909
    :cond_0
    :goto_0
    return-void

    .line 2822
    :cond_1
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMailboxKey:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v15

    .line 2823
    .local v15, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v15, :cond_0

    .line 2825
    new-instance v20, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 2826
    .local v20, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v2, 0x207

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x209

    invoke-virtual {v2, v3}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 2828
    const/16 v2, 0x20c

    move-object/from16 v0, p1

    iget v3, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    and-int/lit8 v3, v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2832
    const/16 v2, 0x206

    iget-object v3, v15, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2833
    const/16 v2, 0x208

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 2834
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 2836
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v2, :cond_2

    .line 2837
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "sendMeetingResponse(): Wbxml:"

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2838
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v8

    .line 2839
    .local v8, "b":[B
    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-direct {v9, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2840
    .local v9, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v14, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 2841
    .local v14, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v14, v9}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 2846
    .end local v8    # "b":[B
    .end local v9    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v14    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_2
    const/16 v18, 0x0

    .line 2848
    .local v18, "res":Lcom/android/exchange/EasResponse;
    :try_start_0
    const-string v2, "MeetingResponse"

    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v18

    .line 2850
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v21

    .line 2852
    .local v21, "status":I
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v2, :cond_3

    .line 2853
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sendMeetingResponse(): MeetingResponse http response code:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2856
    :cond_3
    const/16 v2, 0xc8

    move/from16 v0, v21

    if-ne v0, v2, :cond_a

    .line 2857
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v13

    .line 2858
    .local v13, "len":I
    if-eqz v13, :cond_9

    .line 2859
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 2860
    .local v12, "is":Ljava/io/InputStream;
    new-instance v2, Lcom/android/exchange/adapter/MeetingResponseParser;

    move-object/from16 v0, p0

    invoke-direct {v2, v12, v0}, Lcom/android/exchange/adapter/MeetingResponseParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/EasSyncService;)V

    invoke-virtual {v2}, Lcom/android/exchange/adapter/MeetingResponseParser;->parse()Z

    .line 2861
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mMeetingInfo:Ljava/lang/String;

    move-object/from16 v16, v0

    .line 2862
    .local v16, "meetingInfo":Ljava/lang/String;
    if-eqz v16, :cond_4

    .line 2863
    new-instance v2, Lcom/android/emailcommon/mail/PackedString;

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Lcom/android/emailcommon/mail/PackedString;-><init>(Ljava/lang/String;)V

    const-string v3, "RESPONSE"

    invoke-virtual {v2, v3}, Lcom/android/emailcommon/mail/PackedString;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2868
    .local v19, "responseRequested":Ljava/lang/String;
    const-string v2, "0"

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2869
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2906
    if-eqz v18, :cond_0

    .line 2907
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 2874
    .end local v19    # "responseRequested":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    :try_start_1
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2906
    if-eqz v18, :cond_0

    .line 2907
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 2878
    .restart local v19    # "responseRequested":Ljava/lang/String;
    :cond_5
    :try_start_2
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    and-int/lit8 v2, v2, 0x10

    if-nez v2, :cond_6

    .line 2879
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    and-int/lit8 v2, v2, 0x40

    if-eqz v2, :cond_7

    .line 2880
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v2}, Lcom/android/exchange/EasSyncService;->sendMeetingResponseMail(Lcom/android/emailcommon/provider/EmailContent$Message;I)V

    .line 2888
    :cond_6
    :goto_1
    const/4 v3, 0x1

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2906
    if-eqz v18, :cond_0

    .line 2907
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 2882
    :cond_7
    :try_start_3
    sget-object v2, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v10

    .line 2884
    .local v10, "draft":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-eqz v10, :cond_6

    .line 2885
    move-object/from16 v0, p1

    iget v2, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v10, v2}, Lcom/android/exchange/EasSyncService;->sendMeetingResponseMail(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Message;I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2901
    .end local v10    # "draft":Lcom/android/emailcommon/provider/EmailContent$Message;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v13    # "len":I
    .end local v16    # "meetingInfo":Ljava/lang/String;
    .end local v19    # "responseRequested":Ljava/lang/String;
    .end local v21    # "status":I
    :catch_0
    move-exception v11

    .line 2902
    .local v11, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    :try_start_4
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V

    .line 2903
    throw v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2906
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_8

    .line 2907
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    :cond_8
    throw v2

    .line 2890
    .restart local v13    # "len":I
    .restart local v21    # "status":I
    :cond_9
    const/4 v3, 0x0

    :try_start_5
    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2906
    if-eqz v18, :cond_0

    .line 2907
    invoke-virtual/range {v18 .. v18}, Lcom/android/exchange/EasResponse;->close()V

    goto/16 :goto_0

    .line 2893
    .end local v13    # "len":I
    :cond_a
    :try_start_6
    invoke-static/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2894
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V

    .line 2895
    new-instance v2, Lcom/android/exchange/EasAuthenticationException;

    invoke-direct {v2}, Lcom/android/exchange/EasAuthenticationException;-><init>()V

    throw v2

    .line 2897
    :cond_b
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Meeting response request failed, code: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 2898
    const/4 v3, 0x0

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V

    .line 2899
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    throw v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method protected sendPing([BI)Lcom/android/exchange/EasResponse;
    .locals 3
    .param p1, "bytes"    # [B
    .param p2, "heartbeat"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3444
    if-eqz p1, :cond_0

    .line 3445
    const-string v0, "Ping"

    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v1, p1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    add-int/lit8 v2, p2, 0x5

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v0

    .line 3448
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Ping"

    const/4 v1, 0x0

    add-int/lit8 v2, p2, 0x5

    mul-int/lit16 v2, v2, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v0

    goto :goto_0
.end method

.method setHeaders(Lorg/apache/http/client/methods/HttpRequestBase;Z)V
    .locals 4
    .param p1, "method"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "usePolicyKey"    # Z

    .prologue
    .line 3067
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3068
    :cond_0
    const-string v2, "Authorization"

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3070
    :cond_1
    const-string v2, "MS-ASProtocolVersion"

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3071
    const-string v2, "Connection"

    const-string v3, "keep-alive"

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3072
    const-string v2, "User-Agent"

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3073
    if-eqz p2, :cond_3

    .line 3077
    const-string v1, "0"

    .line 3078
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v2, :cond_2

    .line 3079
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSecuritySyncKey:Ljava/lang/String;

    .line 3080
    .local v0, "accountKey":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3081
    move-object v1, v0

    .line 3084
    .end local v0    # "accountKey":Ljava/lang/String;
    :cond_2
    const-string v2, "X-MS-PolicyKey"

    invoke-virtual {p1, v2, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3086
    .end local v1    # "key":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method setHeadersPingCommand(Lorg/apache/http/client/methods/HttpRequestBase;Z)V
    .locals 4
    .param p1, "method"    # Lorg/apache/http/client/methods/HttpRequestBase;
    .param p2, "usePolicyKey"    # Z

    .prologue
    .line 3113
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->isCBA()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3114
    :cond_0
    const-string v2, "Authorization"

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mAuthString:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3116
    :cond_1
    const-string v2, "MS-ASProtocolVersion"

    iget-object v3, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3117
    const-string v2, "Connection"

    const-string v3, "close"

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3118
    const-string v2, "User-Agent"

    invoke-static {}, Lcom/android/exchange/EasSyncService;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3119
    if-eqz p2, :cond_3

    .line 3125
    const-string v1, "0"

    .line 3126
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v2, :cond_2

    .line 3127
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v0, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mSecuritySyncKey:Ljava/lang/String;

    .line 3128
    .local v0, "accountKey":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3129
    move-object v1, v0

    .line 3132
    .end local v0    # "accountKey":Ljava/lang/String;
    :cond_2
    const-string v2, "X-MS-PolicyKey"

    invoke-virtual {p1, v2, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 3134
    .end local v1    # "key":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public setmPendingMessageId(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 781
    const-string v0, "setmPendingMessageId"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPendingMessageId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "new id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    iput-wide p1, p0, Lcom/android/exchange/EasSyncService;->mPendingMessageId:J

    .line 783
    return-void
.end method

.method public setupAdhocService(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5799
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v6, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    invoke-static {p1, v6, v7}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->restoreHostAuthWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-result-object v1

    .line 5801
    .local v1, "ha":Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    if-nez v1, :cond_0

    .line 5837
    :goto_0
    return v4

    .line 5805
    :cond_0
    sput-object p1, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 5807
    iget-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 5809
    iget-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mLogin:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 5811
    iget-object v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPassword:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 5813
    iget v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mPort:I

    iput v2, p0, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 5815
    iget v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 5817
    iget v2, v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mFlags:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    iput-boolean v2, p0, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 5819
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 5821
    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 5825
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v4, v3

    .line 5837
    goto :goto_0

    :cond_1
    move v2, v4

    .line 5815
    goto :goto_1

    :cond_2
    move v2, v4

    .line 5817
    goto :goto_2

    .line 5827
    :catch_0
    move-exception v0

    .line 5829
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v3, "Unable to get deviceId"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 5831
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method setupProtocolVersion(Lcom/android/exchange/EasSyncService;Lorg/apache/http/Header;)V
    .locals 10
    .param p1, "service"    # Lcom/android/exchange/EasSyncService;
    .param p2, "versionHeader"    # Lorg/apache/http/Header;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 1022
    invoke-interface {p2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 1024
    .local v4, "supportedVersions":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "Server supports versions: "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-virtual {p0, v7}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1026
    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1028
    .local v5, "supportedVersionsArray":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 1032
    .local v3, "ourVersion":Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v6, v0, v1

    .line 1034
    .local v6, "version":Ljava/lang/String;
    const-string v7, "2.5"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "12.0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "12.1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "14.0"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "14.1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1050
    :cond_0
    move-object v3, v6

    .line 1032
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1062
    .end local v6    # "version":Ljava/lang/String;
    :cond_2
    if-nez v3, :cond_3

    .line 1064
    sget-object v7, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "No supported EAS versions: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    new-instance v7, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v8, 0x9

    invoke-direct {v7, v8}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v7

    .line 1070
    :cond_3
    iput-object v3, p1, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    .line 1072
    invoke-static {v3}, Lcom/android/emailcommon/EasRefs;->getProtocolVersionDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, p1, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 1074
    iget-object v7, p1, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    if-eqz v7, :cond_4

    .line 1076
    iget-object v7, p1, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iput-object v3, v7, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    .line 1084
    :cond_4
    iget-object v7, p1, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    sput-wide v8, Lcom/android/exchange/EasSyncService;->protocolVersion:D

    .line 1105
    return-void
.end method

.method protected setupService()Z
    .locals 2

    .prologue
    .line 5426
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 5427
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    .line 5428
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 5430
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5432
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->loadServiceData()Z

    move-result v0

    return v0

    .line 5430
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected shutdownConnectionManagerForSendMessage(J)V
    .locals 1
    .param p1, "accountId"    # J

    .prologue
    .line 3209
    invoke-static {p1, p2}, Lcom/android/exchange/ExchangeService;->shutdownSendMessageConnection(J)V

    .line 3211
    return-void
.end method

.method public stop()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 828
    iput-boolean v0, p0, Lcom/android/exchange/EasSyncService;->mStop:Z

    .line 830
    new-array v1, v0, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Stop() called for thread:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-virtual {p0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 836
    invoke-virtual {p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 838
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/exchange/EasSyncService;->commandFinished:Z

    if-nez v0, :cond_0

    .line 846
    iget-object v0, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 856
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stop(): Clearing WatchdogAlarm for mailbox:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0, v0}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 858
    iget-wide v2, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-static {v2, v3}, Lcom/android/exchange/ExchangeService;->clearWatchdogAlarm(J)V

    .line 864
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 868
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    if-eqz v0, :cond_1

    .line 870
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-wide v2, p0, Lcom/android/exchange/EasSyncService;->mMailboxId:J

    invoke-virtual {v0, v2, v3}, Lcom/android/exchange/ExchangeService;->releaseWakeLock(J)V

    .line 872
    :cond_1
    return-void

    .line 830
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 864
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public sync(Lcom/android/exchange/adapter/AbstractSyncAdapter;)V
    .locals 42
    .param p1, "target"    # Lcom/android/exchange/adapter/AbstractSyncAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 4847
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-object/from16 v27, v0

    .line 4848
    .local v27, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start Sync ! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4852
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v16

    .line 4854
    .local v16, "className":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v38

    .line 4856
    .local v38, "syncKey":Ljava/lang/String;
    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->SyncKey:Ljava/lang/String;

    .line 4858
    if-eqz v38, :cond_0

    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 4859
    :cond_0
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/exchange/EasSyncService;->mIsInitialSyncThread:Z

    .line 4862
    :cond_1
    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->CollectionId:Ljava/lang/String;

    .line 4867
    const/4 v4, 0x2

    new-array v11, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v11, v4

    const/4 v4, 0x1

    const-string v5, "mailboxKey"

    aput-object v5, v11, v4

    .line 4870
    .local v11, "MESSAGE_LIST_PROJECTION":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 4871
    .local v10, "MESSAGE_LIST_ID":I
    const/4 v12, 0x1

    .line 4874
    .local v12, "MESSAGE_MAILBOX_KEY":I
    const/16 v32, 0x1

    .line 4876
    .local v32, "moreAvailable":Z
    const/16 v26, 0x0

    .line 4879
    .local v26, "loopingCount":I
    if-eqz v16, :cond_4

    const-string v4, "Email"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 4880
    const/16 v24, 0x0

    .line 4881
    .local v24, "isBlackList":Z
    const/4 v14, 0x0

    .line 4883
    .local v14, "blackListCursor":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$BlackList;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "emailAddress"

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 4886
    if-eqz v14, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    .line 4887
    const/16 v24, 0x1

    .line 4890
    :cond_2
    if-eqz v14, :cond_3

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_3

    .line 4891
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 4894
    :cond_3
    :goto_0
    if-eqz v24, :cond_4

    .line 4895
    const/4 v4, 0x1

    move-object/from16 v0, p1

    iput-boolean v4, v0, Lcom/android/exchange/adapter/AbstractSyncAdapter;->isBlackList:Z

    .line 4899
    .end local v14    # "blackListCursor":Landroid/database/Cursor;
    .end local v24    # "isBlackList":Z
    :cond_4
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_5

    if-eqz v27, :cond_5

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_5

    .line 4900
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : +++++"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4902
    :cond_5
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 4903
    .local v33, "msgMoveRequestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/MessageMoveRequest;>;"
    :cond_6
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/exchange/EasSyncService;->mStop:Z

    if-nez v4, :cond_6a

    if-nez v32, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->hasPendingRequests()Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 4907
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->hasConnectivity()Z

    move-result v4

    if-nez v4, :cond_c

    .line 4908
    const-string v4, "No connectivity in sync; finishing sync"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    .line 4910
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/android/exchange/Request;

    .line 4911
    .local v34, "req":Lcom/android/exchange/Request;
    move-object/from16 v0, v34

    instance-of v4, v0, Lcom/android/exchange/MeetingResponseRequest;

    if-eqz v4, :cond_8

    move-object/from16 v39, v34

    .line 4912
    check-cast v39, Lcom/android/exchange/MeetingResponseRequest;

    .line 4913
    .local v39, "tReq":Lcom/android/exchange/MeetingResponseRequest;
    move-object/from16 v0, v39

    iget v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    and-int/lit8 v4, v4, 0x20

    if-nez v4, :cond_9

    move-object/from16 v0, v39

    iget v4, v0, Lcom/android/exchange/MeetingResponseRequest;->mResponse:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_8

    .line 4915
    :cond_9
    const/4 v5, 0x0

    move-object/from16 v0, v39

    iget-wide v6, v0, Lcom/android/exchange/MeetingResponseRequest;->mMessageId:J

    move-object/from16 v0, v39

    iget-wide v8, v0, Lcom/android/exchange/MeetingResponseRequest;->mDraftMessageId:J

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/android/exchange/EasSyncService;->sendMeetingEditedResponseCallback(ZJJ)V

    goto :goto_2

    .line 4888
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v33    # "msgMoveRequestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/MessageMoveRequest;>;"
    .end local v34    # "req":Lcom/android/exchange/Request;
    .end local v39    # "tReq":Lcom/android/exchange/MeetingResponseRequest;
    .restart local v14    # "blackListCursor":Landroid/database/Cursor;
    .restart local v24    # "isBlackList":Z
    :catch_0
    move-exception v4

    .line 4890
    if-eqz v14, :cond_3

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_3

    .line 4891
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 4890
    :catchall_0
    move-exception v4

    if-eqz v14, :cond_a

    invoke-interface {v14}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_a

    .line 4891
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v4

    .line 4919
    .end local v14    # "blackListCursor":Landroid/database/Cursor;
    .end local v24    # "isBlackList":Z
    .restart local v21    # "i$":Ljava/util/Iterator;
    .restart local v33    # "msgMoveRequestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/exchange/MessageMoveRequest;>;"
    :cond_b
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 5272
    .end local v21    # "i$":Ljava/util/Iterator;
    :goto_3
    return-void

    .line 4925
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->isSyncable()Z

    move-result v4

    if-nez v4, :cond_d

    .line 4926
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 4927
    const-string v4, "Ignore request. Target is not syncable; finishing sync"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->errorLog(Ljava/lang/String;)V

    goto :goto_3

    .line 4933
    :cond_d
    if-eqz v16, :cond_14

    const-string v4, "Email"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 4934
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mailboxKey="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-wide v8, v7, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v6, v11

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v41

    .line 4939
    .local v41, "updatedMessagesCursor":Landroid/database/Cursor;
    if-eqz v41, :cond_13

    :try_start_1
    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-eqz v4, :cond_13

    .line 4941
    const/4 v4, -0x1

    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 4942
    :cond_e
    :goto_4
    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 4943
    const/4 v4, 0x0

    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 4945
    .local v30, "messageID":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, v30

    invoke-static {v5, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v6, v11

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 4949
    .local v19, "currentCursor":Landroid/database/Cursor;
    if-eqz v19, :cond_e

    .line 4951
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_10

    .line 4952
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 4974
    .end local v19    # "currentCursor":Landroid/database/Cursor;
    .end local v30    # "messageID":J
    :catchall_1
    move-exception v4

    if-eqz v41, :cond_f

    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_f

    .line 4975
    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->close()V

    :cond_f
    throw v4

    .line 4957
    .restart local v19    # "currentCursor":Landroid/database/Cursor;
    .restart local v30    # "messageID":J
    :cond_10
    const/4 v4, 0x1

    :try_start_2
    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v28

    .line 4960
    .local v28, "mailboxID":J
    if-eqz v19, :cond_11

    :try_start_3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_11

    .line 4961
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 4963
    :cond_11
    const/4 v4, 0x1

    move-object/from16 v0, v41

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v4, v28, v4

    if-eqz v4, :cond_e

    .line 4965
    new-instance v4, Lcom/android/exchange/MessageMoveRequest;

    move-wide/from16 v0, v30

    move-wide/from16 v2, v28

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/android/exchange/MessageMoveRequest;-><init>(JJ)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4966
    const/16 v4, 0x3e7

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v4, v5, :cond_e

    .line 4967
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->messageMoveRequest(Ljava/util/ArrayList;)V

    .line 4968
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->clear()V

    goto :goto_4

    .line 4960
    .end local v28    # "mailboxID":J
    :catchall_2
    move-exception v4

    if-eqz v19, :cond_12

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_12

    .line 4961
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_12
    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4974
    .end local v19    # "currentCursor":Landroid/database/Cursor;
    .end local v30    # "messageID":J
    :cond_13
    if-eqz v41, :cond_14

    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_14

    .line 4975
    invoke-interface/range {v41 .. v41}, Landroid/database/Cursor;->close()V

    .line 4981
    .end local v41    # "updatedMessagesCursor":Landroid/database/Cursor;
    :cond_14
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_15

    .line 4982
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->messageMoveRequest(Ljava/util/ArrayList;)V

    .line 4983
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->clear()V

    .line 4987
    :cond_15
    :goto_5
    const/16 v34, 0x0

    .line 4988
    .restart local v34    # "req":Lcom/android/exchange/Request;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    monitor-enter v5

    .line 4989
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 4990
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    .line 5018
    :goto_6
    if-eqz v32, :cond_6

    .line 5022
    new-instance v36, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v36 .. v36}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 5024
    .local v36, "s":Lcom/android/exchange/adapter/Serializer;
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getCollectionName()Ljava/lang/String;

    move-result-object v16

    .line 5026
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->getSyncKey()Ljava/lang/String;

    move-result-object v38

    .line 5028
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "sync, sending "

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v16, v4, v5

    const/4 v5, 0x2

    const-string v6, " syncKey: "

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v38, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5032
    const/4 v4, 0x5

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    const/16 v5, 0x1c

    invoke-virtual {v4, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 5036
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpg-double v4, v4, v6

    if-gez v4, :cond_16

    .line 5037
    const/16 v4, 0x10

    move-object/from16 v0, v36

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 5039
    :cond_16
    const/16 v4, 0xb

    move-object/from16 v0, v36

    move-object/from16 v1, v38

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    const/16 v5, 0x12

    move-object/from16 v0, v27

    iget-object v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 5043
    sget v40, Lcom/android/exchange/EasSyncService;->COMMAND_TIMEOUT:I

    .line 5044
    .local v40, "timeout":I
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    .line 5050
    .local v22, "initialSync":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    move/from16 v2, v22

    invoke-virtual {v0, v4, v1, v2}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->sendSyncOptions(Ljava/lang/Double;Lcom/android/exchange/adapter/Serializer;Z)V

    .line 5051
    if-eqz v22, :cond_17

    .line 5053
    const v40, 0x1d4c0

    .line 5057
    :cond_17
    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->sendLocalChanges(Lcom/android/exchange/adapter/Serializer;)Z

    .line 5059
    invoke-virtual/range {v36 .. v36}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 5062
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v4, :cond_18

    .line 5063
    invoke-virtual/range {v36 .. v36}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v13

    .line 5064
    .local v13, "b":[B
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "sync(): Wbxml:"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5065
    new-instance v15, Ljava/io/ByteArrayInputStream;

    invoke-direct {v15, v13}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 5066
    .local v15, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v25, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 5067
    .local v25, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 5071
    .end local v13    # "b":[B
    .end local v15    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v25    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_18
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_19

    .line 5072
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SEND_HTTP"

    const-string v6, "Performance Check - sendHttpClientPost +++++"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5073
    :cond_19
    const-string v4, "Sync"

    new-instance v5, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual/range {v36 .. v36}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-virtual {v0, v4, v5, v1}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;I)Lcom/android/exchange/EasResponse;

    move-result-object v35

    .line 5074
    .local v35, "resp":Lcom/android/exchange/EasResponse;
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_1a

    .line 5075
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SEND_HTTP"

    const-string v6, "Performance Check - sendHttpClientPost -----"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5078
    :cond_1a
    :try_start_5
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v17

    .line 5081
    .local v17, "code":I
    const-string v4, "sync(): sendHttpClientPost HTTP response code: "

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 5082
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "thread="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getThreadId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " mAccount="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " mMailbox="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v6, v6, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " cmd="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mASCmd:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " class="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " mSyncReason="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/exchange/EasSyncService;->mSyncReason:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " syncKey="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " timeout="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v40

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " res="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " content="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v4, "Content-Length"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    if-eqz v4, :cond_22

    const-string v4, "Content-Length"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    :goto_7
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " bytes"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    .line 5092
    const/16 v4, 0xc8

    move/from16 v0, v17

    if-ne v0, v4, :cond_57

    .line 5094
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x4028333333333333L    # 12.1

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_27

    .line 5095
    const-string v4, "Content-Length"

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v18

    .line 5096
    .local v18, "contentLength":Lorg/apache/http/Header;
    if-eqz v18, :cond_27

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 5097
    const-string v4, "EasSynService"

    const-string v5, "Sync Response has a header [Content-Length: 0]"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5098
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->setIntervalPing()V

    .line 5099
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_23

    .line 5100
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_1b

    if-eqz v27, :cond_1b

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_1b

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_1b
    if-eqz v35, :cond_1c

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_1d

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_1d
    monitor-exit v5

    goto/16 :goto_3

    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v4

    .line 4992
    .end local v17    # "code":I
    .end local v18    # "contentLength":Lorg/apache/http/Header;
    .end local v22    # "initialSync":Z
    .end local v35    # "resp":Lcom/android/exchange/EasResponse;
    .end local v36    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v40    # "timeout":I
    :cond_1e
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/android/exchange/Request;

    move-object/from16 v34, v0

    .line 4994
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 4998
    move-object/from16 v0, v34

    instance-of v4, v0, Lcom/android/exchange/MeetingResponseRequest;

    if-eqz v4, :cond_20

    move-object/from16 v4, v34

    .line 4999
    check-cast v4, Lcom/android/exchange/MeetingResponseRequest;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->sendMeetingResponse(Lcom/android/exchange/MeetingResponseRequest;)V

    .line 5005
    :cond_1f
    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    monitor-enter v5

    .line 5007
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->remove()Ljava/lang/Object;

    .line 5008
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mRequestQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z
    :try_end_8
    .catch Ljava/util/NoSuchElementException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result v4

    if-eqz v4, :cond_21

    .line 5009
    const/16 v32, 0x0

    .line 5010
    :try_start_9
    monitor-exit v5

    goto/16 :goto_6

    .line 5014
    :catchall_4
    move-exception v4

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v4

    .line 4994
    :catchall_5
    move-exception v4

    :try_start_a
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    throw v4

    .line 5000
    :cond_20
    move-object/from16 v0, v34

    instance-of v4, v0, Lcom/android/exchange/MessageMoveRequest;

    if-eqz v4, :cond_1f

    move-object/from16 v4, v34

    .line 5001
    check-cast v4, Lcom/android/exchange/MessageMoveRequest;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->messageMoveRequest(Lcom/android/exchange/MessageMoveRequest;)V

    goto :goto_8

    .line 5012
    :catch_1
    move-exception v4

    .line 5014
    :cond_21
    :try_start_b
    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    goto/16 :goto_5

    .line 5082
    .restart local v17    # "code":I
    .restart local v22    # "initialSync":Z
    .restart local v35    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v36    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v40    # "timeout":I
    :cond_22
    :try_start_c
    const-string v4, "0"
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_a

    goto/16 :goto_7

    .line 5255
    .restart local v18    # "contentLength":Lorg/apache/http/Header;
    :cond_23
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_24

    if-eqz v27, :cond_24

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_24

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_24
    if-eqz v35, :cond_25

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_26

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_26
    monitor-exit v5

    goto/16 :goto_1

    :catchall_6
    move-exception v4

    monitor-exit v5
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v4

    .line 5108
    .end local v18    # "contentLength":Lorg/apache/http/Header;
    :cond_27
    :try_start_e
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_a

    move-result-object v23

    .line 5110
    .local v23, "is":Ljava/io/InputStream;
    if-eqz v23, :cond_56

    .line 5113
    :try_start_f
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_28

    .line 5114
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_PARSE"

    const-string v6, "Performance Check - parse +++++"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5115
    :cond_28
    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->parse(Ljava/io/InputStream;)Z

    move-result v32

    .line 5116
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget v4, v4, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_29

    .line 5117
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_PARSE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Performance Check - parse ----- : moreAvailable = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v32

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5118
    :cond_29
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->isLooping()Z

    move-result v4

    if-eqz v4, :cond_31

    .line 5119
    add-int/lit8 v26, v26, 0x1

    .line 5120
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "** Looping: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5124
    if-eqz v32, :cond_2a

    const/16 v4, 0x64

    move/from16 v0, v26

    if-le v0, v4, :cond_2a

    .line 5125
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "** Looping force stopped"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 5126
    const/16 v32, 0x0

    .line 5127
    move-object/from16 v0, p1

    instance-of v4, v0, Lcom/android/exchange/adapter/EmailSyncAdapter;

    if-eqz v4, :cond_2a

    .line 5128
    move-object/from16 v0, p1

    check-cast v0, Lcom/android/exchange/adapter/EmailSyncAdapter;

    move-object v4, v0

    invoke-virtual {v4}, Lcom/android/exchange/adapter/EmailSyncAdapter;->forceStopLooping()V

    .line 5135
    :cond_2a
    :goto_9
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    if-nez v4, :cond_2c

    if-eqz v16, :cond_2c

    .line 5136
    const-string v4, "Calendar"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x41

    if-ne v4, v5, :cond_3a

    .line 5137
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2b

    if-nez v38, :cond_32

    :cond_2b
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_32

    .line 5138
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z
    :try_end_f
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_a

    .line 5216
    :cond_2c
    :goto_a
    :try_start_10
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->cleanup()V

    .line 5217
    if-nez v32, :cond_2d

    .line 5218
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->syncHasPendingRequests()Z

    move-result v32

    .line 5224
    :cond_2d
    :goto_b
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->isProvisionError(I)Z

    move-result v4

    if-eqz v4, :cond_66

    .line 5225
    const/4 v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_2e

    if-eqz v27, :cond_2e

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_2e

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_2e
    if-eqz v35, :cond_2f

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_2f
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_30

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_30
    monitor-exit v5

    goto/16 :goto_3

    :catchall_7
    move-exception v4

    monitor-exit v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_7

    throw v4

    .line 5131
    :cond_31
    const/16 v26, 0x0

    goto :goto_9

    .line 5139
    :cond_32
    :try_start_12
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_36

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_36

    .line 5140
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z
    :try_end_12
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_12 .. :try_end_12} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_a

    goto :goto_a

    .line 5186
    :catch_2
    move-exception v20

    .line 5188
    .local v20, "e":Lcom/android/exchange/CommandStatusException;
    :try_start_13
    move-object/from16 v0, v20

    iget v0, v0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    move/from16 v37, v0

    .line 5190
    .local v37, "status":I
    invoke-static/range {v37 .. v37}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v4

    if-eqz v4, :cond_51

    .line 5191
    const/4 v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_a

    .line 5255
    :goto_c
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_33

    if-eqz v27, :cond_33

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_33

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_33
    if-eqz v35, :cond_34

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_34
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_35

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_35
    monitor-exit v5

    goto/16 :goto_3

    :catchall_8
    move-exception v4

    monitor-exit v5
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_8

    throw v4

    .line 5142
    .end local v20    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v37    # "status":I
    :cond_36
    const/4 v4, 0x0

    :try_start_15
    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z
    :try_end_15
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_a

    goto/16 :goto_a

    .line 5209
    :catch_3
    move-exception v20

    .line 5210
    .local v20, "e":Ljava/lang/OutOfMemoryError;
    :try_start_16
    invoke-virtual/range {p1 .. p1}, Lcom/android/exchange/adapter/AbstractSyncAdapter;->cleanup()V

    .line 5211
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_37

    if-eqz v27, :cond_37

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_37

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_37
    if-eqz v35, :cond_38

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_38
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_39

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_39
    monitor-exit v5

    goto/16 :goto_3

    :catchall_9
    move-exception v4

    monitor-exit v5
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_9

    throw v4

    .line 5144
    .end local v20    # "e":Ljava/lang/OutOfMemoryError;
    :cond_3a
    :try_start_18
    const-string v4, "Contacts"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_41

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x42

    if-ne v4, v5, :cond_41

    .line 5145
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3b

    if-nez v38, :cond_3f

    :cond_3b
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_3f

    .line 5146
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z
    :try_end_18
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_18 .. :try_end_18} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_18 .. :try_end_18} :catch_3
    .catchall {:try_start_18 .. :try_end_18} :catchall_a

    goto/16 :goto_a

    .line 5255
    .end local v17    # "code":I
    .end local v23    # "is":Ljava/io/InputStream;
    :catchall_a
    move-exception v4

    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v5, :cond_3c

    if-eqz v27, :cond_3c

    move-object/from16 v0, v27

    iget v5, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v5, :cond_3c

    .line 5256
    sget-object v5, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v6, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v7, "Performance Check - Sync() : finally"

    invoke-static {v5, v6, v7}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_3c
    if-eqz v35, :cond_3d

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_3d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_19
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v6, :cond_3e

    .line 5262
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v6}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_3e
    monitor-exit v5
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_10

    throw v4

    .line 5147
    .restart local v17    # "code":I
    .restart local v23    # "is":Ljava/io/InputStream;
    :cond_3f
    :try_start_1a
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_40

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_40

    .line 5148
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    goto/16 :goto_a

    .line 5150
    :cond_40
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    goto/16 :goto_a

    .line 5152
    :cond_41
    const-string v4, "Tasks"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 5153
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_42

    if-nez v38, :cond_43

    :cond_42
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_43

    .line 5154
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    goto/16 :goto_a

    .line 5155
    :cond_43
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_44

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_44

    .line 5156
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    goto/16 :goto_a

    .line 5158
    :cond_44
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncTask:Z

    goto/16 :goto_a

    .line 5160
    :cond_45
    const-string v4, "IPM.StickyNote"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_49

    .line 5161
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_46

    if-nez v38, :cond_47

    :cond_46
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_47

    .line 5162
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    goto/16 :goto_a

    .line 5163
    :cond_47
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_48

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_48

    .line 5164
    const/4 v4, 0x1

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    goto/16 :goto_a

    .line 5166
    :cond_48
    const/4 v4, 0x0

    sput-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncNote:Z

    goto/16 :goto_a

    .line 5168
    :cond_49
    const-string v4, "Contacts"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4d

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x53

    if-ne v4, v5, :cond_4d

    .line 5169
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4a

    if-nez v38, :cond_4b

    :cond_4a
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_4b

    .line 5170
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 5171
    :cond_4b
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncContact:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4c

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_4c

    .line 5172
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 5174
    :cond_4c
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncContactSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 5176
    :cond_4d
    const-string v4, "Calendar"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    const/16 v5, 0x52

    if-ne v4, v5, :cond_2c

    .line 5177
    const-string v4, "0"

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4e

    if-nez v38, :cond_4f

    :cond_4e
    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_4f

    .line 5178
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 5179
    :cond_4f
    sget-boolean v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendar:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_50

    const/4 v4, 0x1

    move/from16 v0, v32

    if-ne v0, v4, :cond_50

    .line 5180
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_a

    .line 5182
    :cond_50
    sget-object v4, Lcom/android/exchange/ExchangeService;->isInitSyncCalendarSubFolerMap:Ljava/util/HashMap;

    move-object/from16 v0, v27

    iget-wide v6, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1a
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_1a .. :try_end_1a} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1a .. :try_end_1a} :catch_3
    .catchall {:try_start_1a .. :try_end_1a} :catchall_a

    goto/16 :goto_a

    .line 5194
    .local v20, "e":Lcom/android/exchange/CommandStatusException;
    .restart local v37    # "status":I
    :cond_51
    :try_start_1b
    invoke-static/range {v37 .. v37}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTooManyPartnerships(I)Z

    move-result v4

    if-eqz v4, :cond_52

    .line 5195
    const/16 v4, 0xb

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_c

    .line 5197
    :cond_52
    invoke-static/range {v37 .. v37}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isServerError(I)Z

    move-result v4

    if-eqz v4, :cond_53

    .line 5198
    const/16 v4, 0xc

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_c

    .line 5199
    :cond_53
    invoke-static/range {v37 .. v37}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v4

    if-eqz v4, :cond_54

    .line 5200
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_c

    .line 5202
    :cond_54
    invoke-static/range {v37 .. v37}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientServerError(I)Z

    move-result v4

    if-eqz v4, :cond_55

    .line 5203
    const/16 v4, 0xd

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_c

    .line 5205
    :cond_55
    const/4 v4, 0x3

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_c

    .line 5221
    .end local v20    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v37    # "status":I
    :cond_56
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "Empty input stream in sync command response"

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto/16 :goto_b

    .line 5230
    .end local v23    # "is":Ljava/io/InputStream;
    :cond_57
    const-string v4, "Sync response error: "

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 5231
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->isProvisionError(I)Z

    move-result v4

    if-eqz v4, :cond_5b

    .line 5232
    const/4 v4, 0x4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_58

    if-eqz v27, :cond_58

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_58

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_58
    if-eqz v35, :cond_59

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_59
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_5a

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_5a
    monitor-exit v5

    goto/16 :goto_3

    :catchall_b
    move-exception v4

    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_b

    throw v4

    .line 5234
    :cond_5b
    :try_start_1d
    invoke-static/range {v17 .. v17}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v4

    if-eqz v4, :cond_5f

    .line 5235
    const/4 v4, 0x2

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_5c

    if-eqz v27, :cond_5c

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_5c

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_5c
    if-eqz v35, :cond_5d

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_5d
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_5e

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_5e
    monitor-exit v5

    goto/16 :goto_3

    :catchall_c
    move-exception v4

    monitor-exit v5
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_c

    throw v4

    .line 5238
    :cond_5f
    packed-switch v17, :pswitch_data_0

    .line 5248
    const/4 v4, 0x1

    :try_start_1f
    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_60

    if-eqz v27, :cond_60

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_60

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_60
    if-eqz v35, :cond_61

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_61
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_62

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_62
    monitor-exit v5

    goto/16 :goto_3

    :catchall_d
    move-exception v4

    monitor-exit v5
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_d

    throw v4

    .line 5243
    :pswitch_0
    :try_start_21
    const-string v4, "Sync request failed, reporting Server error: "

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 5244
    const/16 v4, 0xc

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_a

    .line 5255
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_63

    if-eqz v27, :cond_63

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_63

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_63
    if-eqz v35, :cond_64

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_64
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_65

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_65
    monitor-exit v5

    goto/16 :goto_3

    :catchall_e
    move-exception v4

    monitor-exit v5
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_e

    throw v4

    .line 5255
    .restart local v23    # "is":Ljava/io/InputStream;
    :cond_66
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_67

    if-eqz v27, :cond_67

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_67

    .line 5256
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : finally"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5257
    :cond_67
    if-eqz v35, :cond_68

    .line 5258
    invoke-virtual/range {v35 .. v35}, Lcom/android/exchange/EasResponse;->close()V

    .line 5260
    :cond_68
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 5261
    :try_start_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v4, :cond_69

    .line 5262
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 5263
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 5265
    :cond_69
    monitor-exit v5

    goto/16 :goto_1

    :catchall_f
    move-exception v4

    monitor-exit v5
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_f

    throw v4

    .end local v17    # "code":I
    .end local v23    # "is":Ljava/io/InputStream;
    :catchall_10
    move-exception v4

    :try_start_24
    monitor-exit v5
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_10

    throw v4

    .line 5269
    .end local v22    # "initialSync":Z
    .end local v34    # "req":Lcom/android/exchange/Request;
    .end local v35    # "resp":Lcom/android/exchange/EasResponse;
    .end local v36    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v40    # "timeout":I
    :cond_6a
    sget-boolean v4, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v4, :cond_6b

    if-eqz v27, :cond_6b

    move-object/from16 v0, v27

    iget v4, v0, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mType:I

    if-nez v4, :cond_6b

    .line 5270
    sget-object v4, Lcom/android/exchange/EasSyncService;->TAG:Ljava/lang/String;

    const-string v5, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    const-string v6, "Performance Check - Sync() : -----"

    invoke-static {v4, v5, v6}, Lcom/android/emailcommon/utility/Utility;->debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 5271
    :cond_6b
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    goto/16 :goto_3

    .line 5238
    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 555
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    if-eqz v1, :cond_0

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/android/exchange/AbstractSyncService;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPendingPost="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Lorg/apache/http/client/methods/HttpPost;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 560
    :goto_0
    return-object v1

    .line 557
    :catch_0
    move-exception v0

    .line 558
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 560
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-super {p0}, Lcom/android/exchange/AbstractSyncService;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public validateAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Landroid/content/Context;)Landroid/os/Bundle;
    .locals 32
    .param p1, "hostAddress"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;
    .param p4, "port"    # I
    .param p5, "ssl"    # Z
    .param p6, "trustCertificates"    # Z
    .param p7, "path"    # Ljava/lang/String;
    .param p8, "context"    # Landroid/content/Context;

    .prologue
    .line 1111
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1112
    .local v7, "bundle":Landroid/os/Bundle;
    const/16 v22, -0x1

    .line 1116
    .local v22, "resultCode":I
    :try_start_0
    invoke-static/range {p8 .. p8}, Lcom/android/exchange/cba/SSLCBAClient;->setStaticContext(Landroid/content/Context;)V

    .line 1118
    const/16 v28, 0x6

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v28, 0x0

    const-string v30, "Testing EAS: "

    aput-object v30, v29, v28

    const/16 v28, 0x1

    aput-object p1, v29, v28

    const/16 v28, 0x2

    const-string v30, ", "

    aput-object v30, v29, v28

    const/16 v28, 0x3

    aput-object p2, v29, v28

    const/16 v28, 0x4

    const-string v30, ", ssl = "

    aput-object v30, v29, v28

    const/16 v30, 0x5

    if-eqz p5, :cond_5

    const-string v28, "1"

    :goto_0
    aput-object v28, v29, v30

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1119
    new-instance v25, Lcom/android/exchange/EasSyncService;

    const-string v28, "%TestAccount%"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/android/exchange/EasSyncService;-><init>(Ljava/lang/String;)V

    .line 1121
    .local v25, "svc":Lcom/android/exchange/EasSyncService;
    const-string v28, ";"

    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    .line 1122
    .local v17, "loc":I
    add-int/lit8 v28, v17, 0x1

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v29

    move-object/from16 v0, p2

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    .line 1123
    const/16 v28, -0x1

    move/from16 v0, v17

    move/from16 v1, v28

    if-eq v0, v1, :cond_0

    .line 1124
    const/16 v28, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v28

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    .line 1125
    :cond_0
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    move-object/from16 v28, v0

    const-string v29, "null"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 1126
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    .line 1127
    :cond_1
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mAlias:Ljava/lang/String;

    .line 1130
    sput-object p8, Lcom/android/exchange/EasSyncService;->mContext:Landroid/content/Context;

    .line 1131
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mHostAddress:Ljava/lang/String;

    .line 1132
    move-object/from16 v0, p7

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPath:Ljava/lang/String;

    .line 1133
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mUserName:Ljava/lang/String;

    .line 1134
    move-object/from16 v0, p3

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPassword:Ljava/lang/String;

    .line 1135
    move/from16 v0, p5

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mSsl:Z

    .line 1136
    move/from16 v0, p6

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/android/exchange/EasSyncService;->mTrustSsl:Z

    .line 1137
    move/from16 v0, p4

    move-object/from16 v1, v25

    iput v0, v1, Lcom/android/exchange/EasSyncService;->mPort:I

    .line 1142
    invoke-static/range {p8 .. p8}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mDeviceId:Ljava/lang/String;

    .line 1144
    invoke-virtual/range {v25 .. v25}, Lcom/android/exchange/EasSyncService;->sendHttpClientOptions()Lcom/android/exchange/EasResponse;
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v20

    .line 1145
    .local v20, "resp":Lcom/android/exchange/EasResponse;
    const/16 v21, 0x0

    .line 1148
    .local v21, "respFolderSync":Lcom/android/exchange/EasResponse;
    :try_start_1
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v10

    .line 1149
    .local v10, "code":I
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Validation (OPTIONS) response: "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1150
    const/4 v14, 0x0

    .line 1151
    .local v14, "is":Ljava/io/InputStream;
    const/16 v28, 0xc8

    move/from16 v0, v28

    if-ne v10, v0, :cond_17

    .line 1153
    const-string v28, "MS-ASProtocolCommands"

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v11

    .line 1154
    .local v11, "commands":Lorg/apache/http/Header;
    const-string v28, "ms-asprotocolversions"

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;
    :try_end_1
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v27

    .line 1157
    .local v27, "versions":Lorg/apache/http/Header;
    if-eqz v11, :cond_2

    if-nez v27, :cond_6

    .line 1158
    :cond_2
    const/16 v28, 0x1

    :try_start_2
    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "OPTIONS response without commands or versions"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1160
    new-instance v28, Lcom/android/emailcommon/mail/MessagingException;

    const/16 v29, 0x0

    invoke-direct/range {v28 .. v29}, Lcom/android/emailcommon/mail/MessagingException;-><init>(I)V

    throw v28
    :try_end_2
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1167
    :catch_0
    move-exception v12

    .line 1168
    .local v12, "e":Lcom/android/emailcommon/mail/MessagingException;
    :try_start_3
    const-string v28, "validate_result_code"

    const/16 v29, 0x9

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_3
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1319
    if-eqz v20, :cond_3

    .line 1320
    :try_start_4
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    .line 1321
    :cond_3
    if-eqz v21, :cond_4

    .line 1322
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->close()V

    .line 1350
    .end local v10    # "code":I
    .end local v11    # "commands":Lorg/apache/http/Header;
    .end local v12    # "e":Lcom/android/emailcommon/mail/MessagingException;
    .end local v14    # "is":Ljava/io/InputStream;
    .end local v17    # "loc":I
    .end local v20    # "resp":Lcom/android/exchange/EasResponse;
    .end local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .end local v25    # "svc":Lcom/android/exchange/EasSyncService;
    .end local v27    # "versions":Lorg/apache/http/Header;
    :cond_4
    :goto_1
    return-object v7

    .line 1118
    :cond_5
    const-string v28, "0"
    :try_end_4
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 1162
    .restart local v10    # "code":I
    .restart local v11    # "commands":Lorg/apache/http/Header;
    .restart local v14    # "is":Ljava/io/InputStream;
    .restart local v17    # "loc":I
    .restart local v20    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .restart local v25    # "svc":Lcom/android/exchange/EasSyncService;
    .restart local v27    # "versions":Lorg/apache/http/Header;
    :cond_6
    :try_start_5
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasSyncService;->setupProtocolVersion(Lcom/android/exchange/EasSyncService;Lorg/apache/http/Header;)V

    .line 1163
    const-string v28, "validate_protocol_version"

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mProtocolVersion:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1174
    const/16 v28, 0x1

    :try_start_6
    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Try folder sync"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1177
    const-string v26, "0"

    .line 1178
    .local v26, "syncKey":Ljava/lang/String;
    const-wide/16 v28, -0x1

    move-object/from16 v0, p8

    move-wide/from16 v1, v28

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/utility/Utility;->findExistingAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v13

    .line 1180
    .local v13, "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v13, :cond_7

    iget-object v0, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    move-object/from16 v28, v0

    if-eqz v28, :cond_7

    .line 1181
    iget-object v0, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mSyncKey:Ljava/lang/String;

    move-object/from16 v26, v0

    .line 1184
    const-wide/16 v28, -0x1

    move-wide/from16 v0, v28

    iput-wide v0, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeyRecv:J

    .line 1185
    const-wide/16 v28, -0x1

    move-wide/from16 v0, v28

    iput-wide v0, v13, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthKeySend:J

    .line 1186
    move-object/from16 v0, v25

    iput-object v13, v0, Lcom/android/exchange/EasSyncService;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 1188
    :cond_7
    new-instance v23, Lcom/android/exchange/adapter/Serializer;

    invoke-direct/range {v23 .. v23}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 1190
    .local v23, "s":Lcom/android/exchange/adapter/Serializer;
    const/16 v28, 0x1d6

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v28

    const/16 v29, 0x1d2

    invoke-virtual/range {v28 .. v29}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->text(Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 1194
    sget-boolean v28, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v28, :cond_8

    .line 1195
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "validateAccount(): Wbxml:"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1196
    invoke-virtual/range {v23 .. v23}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v6

    .line 1197
    .local v6, "b":[B
    new-instance v8, Ljava/io/ByteArrayInputStream;

    invoke-direct {v8, v6}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1198
    .local v8, "byTe":Ljava/io/ByteArrayInputStream;
    new-instance v18, Lcom/android/exchange/adapter/LogAdapter;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 1199
    .local v18, "logA":Lcom/android/exchange/adapter/LogAdapter;
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_6
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1204
    .end local v6    # "b":[B
    .end local v8    # "byTe":Ljava/io/ByteArrayInputStream;
    .end local v18    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_8
    :try_start_7
    const-string v28, "FolderSync"

    invoke-virtual/range {v23 .. v23}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/EasSyncService;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;

    move-result-object v21

    .line 1205
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v10

    .line 1206
    const/16 v28, 0xc8

    move/from16 v0, v28

    if-ne v10, v0, :cond_9

    .line 1207
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->getLength()I

    move-result v16

    .line 1208
    .local v16, "len":I
    if-eqz v16, :cond_9

    .line 1209
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    .line 1213
    .end local v16    # "len":I
    :cond_9
    sget-boolean v28, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v28, :cond_a

    .line 1214
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "validateAccount(): FolderSync response code:"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1220
    :cond_a
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v29

    monitor-enter v29
    :try_end_8
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1221
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v28, v0

    if-eqz v28, :cond_b

    .line 1222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 1223
    const/16 v28, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 1225
    :cond_b
    monitor-exit v29
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1231
    :try_start_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v10}, Lcom/android/exchange/EasSyncService;->isProvisionError(Ljava/io/InputStream;I)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 1233
    new-instance v28, Lcom/android/exchange/CommandStatusException;

    const/16 v29, 0x8e

    invoke-direct/range {v28 .. v29}, Lcom/android/exchange/CommandStatusException;-><init>(I)V

    throw v28
    :try_end_a
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1276
    .end local v10    # "code":I
    .end local v11    # "commands":Lorg/apache/http/Header;
    .end local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "is":Ljava/io/InputStream;
    .end local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v26    # "syncKey":Ljava/lang/String;
    .end local v27    # "versions":Lorg/apache/http/Header;
    :catch_1
    move-exception v12

    .line 1277
    .local v12, "e":Lcom/android/exchange/CommandStatusException;
    :try_start_b
    iget v0, v12, Lcom/android/exchange/CommandStatusException;->mStatus:I

    move/from16 v24, v0

    .line 1278
    .local v24, "status":I
    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v28

    if-eqz v28, :cond_1c

    .line 1280
    invoke-static/range {v25 .. v25}, Lcom/android/exchange/EasSyncService;->canProvision(Lcom/android/exchange/EasSyncService;)Lcom/android/exchange/adapter/ProvisionParser;

    move-result-object v19

    .line 1281
    .local v19, "pp":Lcom/android/exchange/adapter/ProvisionParser;
    if-eqz v19, :cond_1a

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->hasSupportablePolicySet()Z

    move-result v28

    if-eqz v28, :cond_1a

    .line 1284
    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicyKey()Ljava/lang/String;

    move-result-object v28

    const-string v29, "1"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/android/exchange/EasSyncService;->acknowledgeProvision(Lcom/android/exchange/EasSyncService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1285
    .local v15, "lastSecuritySynckey":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/android/exchange/adapter/ProvisionParser;->updateSecuritySyncKey(Ljava/lang/String;)V

    .line 1286
    const/16 v22, 0x7

    .line 1287
    const-string v28, "validate_policy_set"

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->getPolicySet()Lcom/android/emailcommon/service/PolicySet;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1314
    .end local v15    # "lastSecuritySynckey":Ljava/lang/String;
    .end local v19    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    :goto_2
    const-string v28, "validate_result_code"

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1319
    if-eqz v20, :cond_c

    .line 1320
    :try_start_c
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    .line 1321
    :cond_c
    if-eqz v21, :cond_4

    .line 1322
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_c
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_1

    .line 1325
    .end local v12    # "e":Lcom/android/exchange/CommandStatusException;
    .end local v17    # "loc":I
    .end local v20    # "resp":Lcom/android/exchange/EasResponse;
    .end local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .end local v24    # "status":I
    .end local v25    # "svc":Lcom/android/exchange/EasSyncService;
    :catch_2
    move-exception v12

    .line 1327
    .local v12, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "DeviceAccessException caught: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-virtual {v12}, Lcom/android/emailcommon/utility/DeviceAccessException;->getMessage()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1329
    const/16 v22, 0x6

    .line 1348
    .end local v12    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :cond_d
    :goto_3
    const-string v28, "validate_result_code"

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 1225
    .restart local v10    # "code":I
    .restart local v11    # "commands":Lorg/apache/http/Header;
    .restart local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "is":Ljava/io/InputStream;
    .restart local v17    # "loc":I
    .restart local v20    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .restart local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v25    # "svc":Lcom/android/exchange/EasSyncService;
    .restart local v26    # "syncKey":Ljava/lang/String;
    .restart local v27    # "versions":Lorg/apache/http/Header;
    :catchall_0
    move-exception v28

    :try_start_d
    monitor-exit v29
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :try_start_e
    throw v28
    :try_end_e
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 1319
    .end local v10    # "code":I
    .end local v11    # "commands":Lorg/apache/http/Header;
    .end local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v14    # "is":Ljava/io/InputStream;
    .end local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v26    # "syncKey":Ljava/lang/String;
    .end local v27    # "versions":Lorg/apache/http/Header;
    :catchall_1
    move-exception v28

    if-eqz v20, :cond_e

    .line 1320
    :try_start_f
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    .line 1321
    :cond_e
    if-eqz v21, :cond_f

    .line 1322
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->close()V

    :cond_f
    throw v28
    :try_end_f
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_3

    .line 1331
    .end local v17    # "loc":I
    .end local v20    # "resp":Lcom/android/exchange/EasResponse;
    .end local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .end local v25    # "svc":Lcom/android/exchange/EasSyncService;
    :catch_3
    move-exception v12

    .line 1333
    .local v12, "e":Ljava/io/IOException;
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "IOException caught: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1334
    const/16 v22, 0x1

    .line 1336
    invoke-virtual {v12}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    .line 1337
    .local v9, "cause":Ljava/lang/Throwable;
    if-eqz v9, :cond_d

    instance-of v0, v9, Ljava/security/cert/CertificateException;

    move/from16 v28, v0

    if-nez v28, :cond_10

    instance-of v0, v9, Ljavax/net/ssl/SSLException;

    move/from16 v28, v0

    if-eqz v28, :cond_d

    .line 1339
    :cond_10
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "CertificateException or SSLException caught: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-virtual {v12}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1341
    const/16 v22, 0x4

    goto :goto_3

    .line 1220
    .end local v9    # "cause":Ljava/lang/Throwable;
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v10    # "code":I
    .restart local v11    # "commands":Lorg/apache/http/Header;
    .restart local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v14    # "is":Ljava/io/InputStream;
    .restart local v17    # "loc":I
    .restart local v20    # "resp":Lcom/android/exchange/EasResponse;
    .restart local v21    # "respFolderSync":Lcom/android/exchange/EasResponse;
    .restart local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v25    # "svc":Lcom/android/exchange/EasSyncService;
    .restart local v26    # "syncKey":Ljava/lang/String;
    .restart local v27    # "versions":Lorg/apache/http/Header;
    :catchall_2
    move-exception v28

    :try_start_10
    invoke-virtual/range {p0 .. p0}, Lcom/android/exchange/EasSyncService;->getSynchronizer()Ljava/lang/Object;

    move-result-object v29

    monitor-enter v29
    :try_end_10
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_10 .. :try_end_10} :catch_1
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 1221
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v30, v0

    if-eqz v30, :cond_11

    .line 1222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lorg/apache/http/client/methods/HttpPost;->abort()V

    .line 1223
    const/16 v30, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/exchange/EasSyncService;->mPendingPost:Lorg/apache/http/client/methods/HttpPost;

    .line 1225
    :cond_11
    monitor-exit v29
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    :try_start_12
    throw v28
    :try_end_12
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_12 .. :try_end_12} :catch_1
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :catchall_3
    move-exception v28

    :try_start_13
    monitor-exit v29
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    :try_start_14
    throw v28
    :try_end_14
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 1247
    :cond_12
    const/16 v28, 0x194

    move/from16 v0, v28

    if-ne v10, v0, :cond_14

    .line 1250
    const/16 v22, 0x9

    .line 1319
    .end local v11    # "commands":Lorg/apache/http/Header;
    .end local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v26    # "syncKey":Ljava/lang/String;
    .end local v27    # "versions":Lorg/apache/http/Header;
    :goto_4
    if-eqz v20, :cond_13

    .line 1320
    :try_start_15
    invoke-virtual/range {v20 .. v20}, Lcom/android/exchange/EasResponse;->close()V

    .line 1321
    :cond_13
    if-eqz v21, :cond_d

    .line 1322
    invoke-virtual/range {v21 .. v21}, Lcom/android/exchange/EasResponse;->close()V
    :try_end_15
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_15 .. :try_end_15} :catch_2
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_3

    goto/16 :goto_3

    .line 1251
    .restart local v11    # "commands":Lorg/apache/http/Header;
    .restart local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .restart local v26    # "syncKey":Ljava/lang/String;
    .restart local v27    # "versions":Lorg/apache/http/Header;
    :cond_14
    :try_start_16
    invoke-static {v10}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v28

    if-eqz v28, :cond_15

    .line 1252
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Authentication failed for foldersync in validateAccount"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1253
    const/16 v22, 0x5

    goto :goto_4

    .line 1254
    :cond_15
    const/16 v28, 0xc8

    move/from16 v0, v28

    if-eq v10, v0, :cond_16

    .line 1256
    const-string v28, "Unexpected response for FolderSync: "

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v10}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V

    .line 1257
    const/16 v22, 0x0

    goto :goto_4

    .line 1259
    :cond_16
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Validation successful"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    goto :goto_4

    .line 1261
    .end local v11    # "commands":Lorg/apache/http/Header;
    .end local v13    # "existingAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v23    # "s":Lcom/android/exchange/adapter/Serializer;
    .end local v26    # "syncKey":Ljava/lang/String;
    .end local v27    # "versions":Lorg/apache/http/Header;
    :cond_17
    invoke-static {v10}, Lcom/android/exchange/EasResponse;->isAuthError(I)Z

    move-result v28

    if-eqz v28, :cond_18

    .line 1262
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Authentication failed"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1263
    const/16 v22, 0x5

    goto :goto_4

    .line 1264
    :cond_18
    const/16 v28, 0x1f4

    move/from16 v0, v28

    if-ne v10, v0, :cond_19

    .line 1267
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Internal server error"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1268
    const/16 v22, 0x4d

    goto/16 :goto_4

    .line 1272
    :cond_19
    const-string v28, "Validation failed, reporting I/O error: "

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1, v10}, Lcom/android/exchange/EasSyncService;->userLog(Ljava/lang/String;I)V
    :try_end_16
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 1273
    const/16 v22, 0x1

    goto/16 :goto_4

    .line 1289
    .end local v10    # "code":I
    .end local v14    # "is":Ljava/io/InputStream;
    .local v12, "e":Lcom/android/exchange/CommandStatusException;
    .restart local v19    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    .restart local v24    # "status":I
    :cond_1a
    if-eqz v19, :cond_1b

    :try_start_17
    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->getUnsupportedPolicies()[Ljava/lang/String;

    move-result-object v28

    if-eqz v28, :cond_1b

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->getUnsupportedPolicies()[Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    if-lez v28, :cond_1b

    .line 1292
    const/16 v22, 0x8

    .line 1293
    const-string v28, "validate_unsupported_policies"

    invoke-virtual/range {v19 .. v19}, Lcom/android/exchange/adapter/ProvisionParser;->getUnsupportedPolicies()[Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1297
    :cond_1b
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "We have no supported policy set, but unsupported policies list is null too. This is unusual and invalid situation. Return UNSPECIFIED_EXCEPTION"

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1298
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 1301
    .end local v19    # "pp":Lcom/android/exchange/adapter/ProvisionParser;
    :cond_1c
    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTooManyPartnerships(I)Z

    move-result v28

    if-eqz v28, :cond_1d

    .line 1302
    const/16 v28, 0xb

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/exchange/EasSyncService;->mExitStatus:I

    .line 1303
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Denied access: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->toString(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1304
    const/16 v22, 0x61

    goto/16 :goto_2

    .line 1306
    :cond_1d
    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v28

    if-eqz v28, :cond_1e

    .line 1307
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Transient error: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->toString(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V

    .line 1308
    const/16 v22, 0x1

    goto/16 :goto_2

    .line 1310
    :cond_1e
    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const-string v30, "Unexpected response: "

    aput-object v30, v28, v29

    const/16 v29, 0x1

    invoke-static/range {v24 .. v24}, Lcom/android/exchange/CommandStatusException$CommandStatus;->toString(I)Ljava/lang/String;

    move-result-object v30

    aput-object v30, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/exchange/EasSyncService;->userLog([Ljava/lang/String;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 1311
    const/16 v22, 0x0

    goto/16 :goto_2
.end method
