.class public Lcom/android/exchange/ExchangeService$UpdateHistory;
.super Ljava/lang/Thread;
.source "ExchangeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/ExchangeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateHistory"
.end annotation


# static fields
.field private static INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;


# instance fields
.field mAuth:Ljava/lang/String;

.field mCountOps:I

.field mUpdateHistoryOps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7447
    const/4 v0, 0x0

    sput-object v0, Lcom/android/exchange/ExchangeService$UpdateHistory;->INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7453
    const-string v0, "UpdateHistory"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 7448
    const-string v0, "content://logs/email"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mAuth:Ljava/lang/String;

    .line 7454
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    .line 7455
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    .line 7456
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/android/exchange/ExchangeService$UpdateHistory;
    .locals 2

    .prologue
    .line 7505
    const-class v1, Lcom/android/exchange/ExchangeService$UpdateHistory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/exchange/ExchangeService$UpdateHistory;->INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;

    if-nez v0, :cond_0

    .line 7506
    new-instance v0, Lcom/android/exchange/ExchangeService$UpdateHistory;

    invoke-direct {v0}, Lcom/android/exchange/ExchangeService$UpdateHistory;-><init>()V

    sput-object v0, Lcom/android/exchange/ExchangeService$UpdateHistory;->INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;

    .line 7508
    :cond_0
    sget-object v0, Lcom/android/exchange/ExchangeService$UpdateHistory;->INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;

    invoke-static {v0}, Lcom/android/exchange/ExchangeService$UpdateHistory;->maybeStartUpdateHistoryThread(Lcom/android/exchange/ExchangeService$UpdateHistory;)V

    .line 7509
    sget-object v0, Lcom/android/exchange/ExchangeService$UpdateHistory;->INSTANCE:Lcom/android/exchange/ExchangeService$UpdateHistory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 7505
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static maybeStartUpdateHistoryThread(Lcom/android/exchange/ExchangeService$UpdateHistory;)V
    .locals 2
    .param p0, "updateHistory"    # Lcom/android/exchange/ExchangeService$UpdateHistory;

    .prologue
    .line 7522
    :try_start_0
    new-instance v1, Lcom/android/exchange/ExchangeService$UpdateHistory$1;

    invoke-direct {v1, p0}, Lcom/android/exchange/ExchangeService$UpdateHistory$1;-><init>(Lcom/android/exchange/ExchangeService$UpdateHistory;)V

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 7536
    :goto_0
    return-void

    .line 7533
    :catch_0
    move-exception v0

    .line 7534
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execBatchOpsInQueue(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 7495
    .local p1, "ops":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    monitor-enter v1

    .line 7496
    :try_start_0
    const-string v0, "UpdateHistory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Batch operation added into into Q at \' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \' position"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 7498
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 7499
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 7500
    monitor-exit v1

    .line 7502
    return-void

    .line 7500
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 7460
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 7462
    :try_start_0
    iget v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    iget-object v2, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 7463
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mAuth:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    iget v4, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 7465
    const-string v1, "UpdateHistory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Batch operation in Q executed  at \' "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \' position"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 7480
    :catch_0
    move-exception v0

    .line 7481
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 7468
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_3

    .line 7471
    :try_start_2
    iget v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    iget-object v3, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v1, v3, :cond_1

    .line 7472
    const-string v1, "UpdateHistory"

    const-string v3, "Batch operation in Q completed, clearing the Q "

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 7474
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 7475
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mCountOps:I

    .line 7476
    iget-object v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory;->mUpdateHistoryOps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 7478
    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 7482
    :catch_1
    move-exception v0

    .line 7483
    .local v0, "e":Landroid/content/OperationApplicationException;
    invoke-virtual {v0}, Landroid/content/OperationApplicationException;->printStackTrace()V

    goto :goto_0

    .line 7484
    .end local v0    # "e":Landroid/content/OperationApplicationException;
    :catch_2
    move-exception v0

    .line 7485
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 7492
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    return-void

    .line 7488
    :catch_3
    move-exception v0

    .line 7489
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public declared-synchronized start()V
    .locals 2

    .prologue
    .line 7514
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/exchange/ExchangeService$UpdateHistory;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    .line 7515
    invoke-super {p0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7517
    :cond_0
    monitor-exit p0

    return-void

    .line 7514
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
