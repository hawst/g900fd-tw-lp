.class Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
.super Ljava/lang/Object;
.source "AutoDiscoverHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/exchange/AutoDiscoverHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AutoDiscoverInfo"
.end annotation


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mDnsQueryResponse:Ljava/lang/String;

.field private mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

.field private mIsErrorTagPresent:Z

.field private mIsRedirectTagPresent:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->init()V

    .line 171
    return-void
.end method

.method static synthetic access$2200(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->isRedirected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->init()V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z

    return p1
.end method

.method static synthetic access$300(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z

    return v0
.end method

.method static synthetic access$402(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z

    return p1
.end method

.method static synthetic access$700(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->available()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;Lcom/android/emailcommon/provider/EmailContent$HostAuth;)Lcom/android/emailcommon/provider/EmailContent$HostAuth;
    .locals 0
    .param p0, "x0"    # Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;
    .param p1, "x1"    # Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    return-object p1
.end method

.method private available()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 174
    iput-boolean v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z

    .line 175
    iput-boolean v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z

    .line 176
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;

    .line 177
    iget-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;

    const-string v1, "autodiscover_error_code"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    new-instance v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/EmailContent$HostAuth;-><init>()V

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    .line 180
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;

    .line 181
    return-void
.end method

.method private isRedirected()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 195
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "autodiscover info : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    if-eqz v1, :cond_0

    .line 197
    const-string v1, "HostAuth : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mHostAuth:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 201
    const-string v1, "dnsQueryResponse :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mDnsQueryResponse:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_1
    const-string v1, "Bundle Error code"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mBundle:Landroid/os/Bundle;

    const-string v3, "autodiscover_error_code"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    const-string v1, "isRedirectTagPresent :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsRedirectTagPresent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    const-string v1, "isErrorTagPresent :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/exchange/AutoDiscoverHandler$AutoDiscoverInfo;->mIsErrorTagPresent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
