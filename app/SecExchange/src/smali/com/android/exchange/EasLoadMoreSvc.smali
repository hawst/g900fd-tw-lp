.class public Lcom/android/exchange/EasLoadMoreSvc;
.super Lcom/android/exchange/EasSyncService;
.source "EasLoadMoreSvc.java"


# instance fields
.field protected mIs2007support:Z

.field protected mIs2010EXsupport:Z

.field protected mIs2010support:Z

.field private mIsCancelledByUser:Z

.field public mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

.field protected mRemoveIrmProtectionFlag:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V
    .locals 1
    .param p1, "_context"    # Landroid/content/Context;
    .param p2, "_msg"    # Lcom/android/emailcommon/provider/EmailContent$Message;

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/EasSyncService;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;)V

    .line 32
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIsCancelledByUser:Z

    .line 34
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    .line 35
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010support:Z

    .line 36
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2007support:Z

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mRemoveIrmProtectionFlag:I

    .line 44
    iput-object p2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    .line 46
    iget-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    invoke-direct {p0, v0}, Lcom/android/exchange/EasLoadMoreSvc;->setProtocolInfo(Lcom/android/emailcommon/provider/EmailContent$Account;)V

    .line 47
    return-void
.end method

.method private loadMore()I
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    .line 260
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_0

    .line 261
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::loadMore() start"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_0
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/4 v9, 0x1

    const/4 v12, 0x0

    invoke-direct {p0, v10, v11, v9, v12}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    .line 266
    const/4 v5, 0x0

    .line 270
    .local v5, "result":I
    iget-boolean v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    if-eqz v9, :cond_1

    .line 271
    sget-object v9, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v9, v10, v11}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->getIRMRemovalFlag(Landroid/content/Context;J)I

    move-result v9

    iput v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mRemoveIrmProtectionFlag:I

    .line 274
    :cond_1
    const/4 v9, 0x0

    invoke-static {v9}, Lcom/android/exchange/ExchangeService;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mDeviceId:Ljava/lang/String;

    .line 277
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {p0, v9, v10}, Lcom/android/exchange/EasLoadMoreSvc;->makeLoadMoreRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;

    move-result-object v3

    .line 280
    .local v3, "res":Lcom/android/exchange/EasResponse;
    if-nez v3, :cond_4

    .line 281
    iget-boolean v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIsCancelledByUser:Z

    if-eqz v9, :cond_2

    .line 282
    const/high16 v5, 0x100000

    .line 284
    :cond_2
    iget v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mRemoveIrmProtectionFlag:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 285
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v9, 0x27

    const/16 v12, 0x64

    invoke-direct {p0, v10, v11, v9, v12}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    :cond_3
    move v6, v5

    .line 398
    .end local v5    # "result":I
    .local v6, "result":I
    :goto_0
    return v6

    .line 292
    .end local v6    # "result":I
    .restart local v5    # "result":I
    :cond_4
    :try_start_0
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v1

    .line 293
    .local v1, "code":I
    const-string v9, "loadMore(): sendHttpClientPost HTTP response code: "

    invoke-virtual {p0, v9, v1}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;I)V

    .line 295
    const/16 v9, 0xc8

    if-ne v1, v9, :cond_a

    .line 296
    invoke-direct {p0, v3}, Lcom/android/exchange/EasLoadMoreSvc;->parseLoadMoreResponse(Lcom/android/exchange/EasResponse;)I

    move-result v7

    .line 299
    .local v7, "status":I
    invoke-direct {p0, v3, v7}, Lcom/android/exchange/EasLoadMoreSvc;->writeToServiceLogger(Lcom/android/exchange/EasResponse;I)V

    .line 301
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_5

    .line 302
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::loadMore() - writeToServiceLogger() end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_5
    sparse-switch v7, :sswitch_data_0

    .line 359
    const/high16 v5, 0x30000

    .line 360
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v9, 0x64

    invoke-direct {p0, v10, v11, v5, v9}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    .line 389
    .end local v7    # "status":I
    :cond_6
    :goto_1
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v9, 0x64

    invoke-direct {p0, v10, v11, v5, v9}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    .line 394
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_7

    .line 395
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::loadMore() end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v6, v5

    .line 398
    .end local v5    # "result":I
    .restart local v6    # "result":I
    goto :goto_0

    .line 307
    .end local v6    # "result":I
    .restart local v5    # "result":I
    .restart local v7    # "status":I
    :sswitch_0
    const/4 v5, 0x0

    .line 311
    :try_start_1
    iget-boolean v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    if-eqz v9, :cond_6

    iget v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mRemoveIrmProtectionFlag:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_6

    .line 312
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v9, :cond_8

    .line 313
    const-string v9, "EasLoadMoreSvc"

    const-string v10, "Removing irm: done"

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_8
    sget-object v9, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v10, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-static {v9, v10, v11}, Lcom/android/exchange/irm/IRMLicenseParserUtility;->getIRMLicenseFlag(Landroid/content/Context;J)I

    move-result v0

    .line 319
    .local v0, "IRMLicenseFlag":I
    const/4 v9, -0x1

    if-eq v0, v9, :cond_6

    .line 320
    const/16 v5, 0x27

    goto :goto_1

    .line 326
    .end local v0    # "IRMLicenseFlag":I
    :sswitch_1
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {p0, v9, v10}, Lcom/android/exchange/EasLoadMoreSvc;->makeLoadMoreRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;

    move-result-object v4

    .line 328
    .local v4, "resSecondRequest":Lcom/android/exchange/EasResponse;
    if-nez v4, :cond_9

    .line 329
    const/4 v5, 0x0

    .line 330
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v9, 0x64

    invoke-direct {p0, v10, v11, v5, v9}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 391
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    move v6, v5

    .end local v5    # "result":I
    .restart local v6    # "result":I
    goto/16 :goto_0

    .line 334
    .end local v6    # "result":I
    .restart local v5    # "result":I
    :cond_9
    :try_start_2
    invoke-virtual {v4}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v2

    .line 335
    .local v2, "codeSecondRequest":I
    const-string v9, "loadMore(): sendHttpClientPost HTTP response code: "

    invoke-virtual {p0, v9, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;I)V

    .line 338
    const/16 v9, 0xc8

    if-ne v2, v9, :cond_6

    .line 339
    invoke-direct {p0, v4}, Lcom/android/exchange/EasLoadMoreSvc;->parseLoadMoreResponse(Lcom/android/exchange/EasResponse;)I

    move-result v8

    .line 341
    .local v8, "statusSecondRequest":I
    packed-switch v8, :pswitch_data_0

    .line 347
    const/high16 v5, 0x30000

    .line 348
    iget-object v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v10, v9, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v9, 0x64

    invoke-direct {p0, v10, v11, v5, v9}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    move v6, v5

    .end local v5    # "result":I
    .restart local v6    # "result":I
    goto/16 :goto_0

    .line 343
    .end local v6    # "result":I
    .restart local v5    # "result":I
    :pswitch_0
    const/4 v5, 0x0

    .line 344
    goto :goto_1

    .line 363
    .end local v2    # "codeSecondRequest":I
    .end local v4    # "resSecondRequest":Lcom/android/exchange/EasResponse;
    .end local v7    # "status":I
    .end local v8    # "statusSecondRequest":I
    :cond_a
    sparse-switch v1, :sswitch_data_1

    .line 384
    :try_start_3
    const-string v9, "EasLoadMoreSvc - Remote Exception: "

    invoke-virtual {p0, v9, v1}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;I)V

    .line 385
    const/16 v5, 0x15

    goto/16 :goto_1

    .line 366
    :sswitch_2
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "EasLoadMoreSvc - Authentication failed"

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 367
    const/16 v5, 0x16

    .line 368
    goto/16 :goto_1

    .line 371
    :sswitch_3
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "EasLoadMoreSvc - Provisioning Needed"

    aput-object v11, v9, v10

    invoke-virtual {p0, v9}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 372
    const/16 v5, 0x17

    .line 373
    goto/16 :goto_1

    .line 379
    :sswitch_4
    const-string v9, "EasLoadMoreSvc - Server error: "

    invoke-virtual {p0, v9, v1}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 380
    const/16 v5, 0x56

    .line 381
    goto/16 :goto_1

    .line 391
    .end local v1    # "code":I
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Lcom/android/exchange/EasResponse;->close()V

    throw v9

    .line 305
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch

    .line 341
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 363
    :sswitch_data_1
    .sparse-switch
        0x191 -> :sswitch_2
        0x193 -> :sswitch_2
        0x1c1 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x1f5 -> :sswitch_4
        0x1f6 -> :sswitch_4
        0x1f7 -> :sswitch_4
    .end sparse-switch
.end method

.method private loadMoreCb(JII)V
    .locals 1
    .param p1, "msgId"    # J
    .param p3, "status"    # I
    .param p4, "progress"    # I

    .prologue
    .line 533
    :try_start_0
    invoke-static {}, Lcom/android/exchange/ExchangeService;->callback()Lcom/android/emailcommon/service/IEmailServiceCallback;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/android/emailcommon/service/IEmailServiceCallback;->loadMoreStatus(JII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :goto_0
    return-void

    .line 534
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private makeLoadMoreRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/EasResponse;
    .locals 10
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_0

    .line 121
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "EasLoadMoreSvc::makeLoadMoreRequest() - start"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/android/exchange/EasLoadMoreSvc;->prepareCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v5

    .line 125
    .local v5, "s":Lcom/android/exchange/adapter/Serializer;
    const/4 v4, 0x0

    .line 127
    .local v4, "ret_value":Lcom/android/exchange/EasResponse;
    invoke-virtual {v5}, Lcom/android/exchange/adapter/Serializer;->toByteArray()[B

    move-result-object v0

    .line 129
    .local v0, "bArr":[B
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v6, :cond_1

    .line 130
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "loadMore(): Wbxml:"

    aput-object v8, v6, v7

    invoke-virtual {p0, v6}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 131
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 132
    .local v1, "byteArrIn":Ljava/io/ByteArrayInputStream;
    new-instance v3, Lcom/android/exchange/adapter/LogAdapter;

    invoke-direct {v3, p0}, Lcom/android/exchange/adapter/LogAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 133
    .local v3, "logA":Lcom/android/exchange/adapter/LogAdapter;
    invoke-virtual {v3, v1}, Lcom/android/exchange/adapter/LogAdapter;->parse(Ljava/io/InputStream;)Z

    .line 137
    .end local v1    # "byteArrIn":Ljava/io/ByteArrayInputStream;
    .end local v3    # "logA":Lcom/android/exchange/adapter/LogAdapter;
    :cond_1
    :try_start_0
    iget-boolean v6, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2007support:Z

    if-eqz v6, :cond_4

    const-string v6, "ItemOperations"

    :goto_0
    invoke-virtual {p0, v6, v0}, Lcom/android/exchange/EasLoadMoreSvc;->sendHttpClientPost(Ljava/lang/String;[B)Lcom/android/exchange/EasResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 149
    :cond_2
    :goto_1
    sget-boolean v6, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v6, :cond_3

    .line 150
    const-string v6, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v7, "EasLoadMoreSvc::makeLoadMoreRequest() end"

    invoke-static {v6, v7}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_3
    return-object v4

    .line 137
    :cond_4
    :try_start_1
    const-string v6, "Sync"
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 138
    :catch_0
    move-exception v2

    .line 143
    .local v2, "e":Ljava/io/IOException;
    iget-boolean v6, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIsCancelledByUser:Z

    if-nez v6, :cond_2

    .line 145
    iget-object v6, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v6, v6, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/16 v8, 0x20

    const/16 v9, 0x64

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto :goto_1
.end method

.method private parseLoadMoreResponse(Lcom/android/exchange/EasResponse;)I
    .locals 12
    .param p1, "res"    # Lcom/android/exchange/EasResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/utility/DeviceAccessException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 163
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_0

    .line 164
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - start"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    const/4 v7, 0x0

    .line 169
    .local v7, "ret_value":I
    if-nez p1, :cond_1

    move v9, v7

    .line 256
    :goto_0
    return v9

    .line 173
    :cond_1
    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 175
    .local v5, "is":Ljava/io/InputStream;
    if-nez v5, :cond_2

    .line 176
    const/16 v9, 0x15

    goto :goto_0

    .line 179
    :cond_2
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_3

    .line 180
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - res.getInputStream() - end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_3
    :try_start_0
    iget-boolean v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2007support:Z

    if-eqz v9, :cond_b

    .line 186
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_4

    .line 187
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - ItemOperationsAdapter - start"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_4
    new-instance v4, Lcom/android/exchange/adapter/ItemOperationsAdapter;

    invoke-direct {v4, p0}, Lcom/android/exchange/adapter/ItemOperationsAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V

    .line 191
    .local v4, "iot":Lcom/android/exchange/adapter/ItemOperationsAdapter;
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Lcom/android/exchange/adapter/ItemOperationsAdapter;->setMIMERequested(Z)V

    .line 193
    new-instance v3, Lcom/android/exchange/adapter/ItemOperationsParser;

    invoke-direct {v3, v5, v4}, Lcom/android/exchange/adapter/ItemOperationsParser;-><init>(Ljava/io/InputStream;Lcom/android/exchange/adapter/AbstractSyncAdapter;)V

    .line 195
    .local v3, "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    invoke-virtual {v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->parse()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 196
    const/high16 v7, 0x30000

    .line 197
    invoke-virtual {v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I

    move-result v9

    const/16 v10, 0x12

    if-ne v9, v10, :cond_8

    iget-boolean v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010support:Z

    if-eqz v9, :cond_8

    .line 199
    const/16 v7, 0x19

    .line 209
    :cond_5
    :goto_1
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_6

    .line 210
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - ItemOperationsAdapter - end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    .end local v3    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    .end local v4    # "iot":Lcom/android/exchange/adapter/ItemOperationsAdapter;
    :cond_6
    :goto_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 253
    :goto_3
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_7

    .line 254
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v9, v7

    .line 256
    goto :goto_0

    .line 200
    .restart local v3    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    .restart local v4    # "iot":Lcom/android/exchange/adapter/ItemOperationsAdapter;
    :cond_8
    :try_start_1
    invoke-virtual {v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I

    move-result v9

    if-eq v9, v11, :cond_9

    invoke-virtual {v3}, Lcom/android/exchange/adapter/ItemOperationsParser;->getStatus()I

    move-result v9

    if-nez v9, :cond_5

    .line 202
    :cond_9
    const/4 v7, 0x0

    goto :goto_1

    .line 206
    :cond_a
    const/4 v7, 0x0

    goto :goto_1

    .line 214
    .end local v3    # "iop":Lcom/android/exchange/adapter/ItemOperationsParser;
    .end local v4    # "iot":Lcom/android/exchange/adapter/ItemOperationsAdapter;
    :cond_b
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_c

    .line 215
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - EmailSyncAdapter - start"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_c
    new-instance v1, Lcom/android/exchange/adapter/EmailSyncAdapter;

    invoke-direct {v1, p0}, Lcom/android/exchange/adapter/EmailSyncAdapter;-><init>(Lcom/android/exchange/EasSyncService;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    .local v1, "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    :try_start_2
    invoke-virtual {v1, v5}, Lcom/android/exchange/adapter/EmailSyncAdapter;->parse(Ljava/io/InputStream;)Z
    :try_end_2
    .catch Lcom/android/exchange/CommandStatusException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 223
    const/4 v7, 0x0

    .line 239
    :cond_d
    :goto_4
    :try_start_3
    sget-boolean v9, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v9, :cond_6

    .line 240
    const-string v9, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v10, "EasLoadMoreSvc::parseLoadMoreResponse() - EmailSyncAdapter - end"

    invoke-static {v9, v10}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 245
    .end local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    :catch_0
    move-exception v2

    .line 246
    .local v2, "ioe":Ljava/io/IOException;
    const/high16 v7, 0x30000

    .line 250
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_3

    .line 224
    .end local v2    # "ioe":Ljava/io/IOException;
    .restart local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    :catch_1
    move-exception v0

    .line 225
    .local v0, "e1":Lcom/android/exchange/CommandStatusException;
    :try_start_4
    iget v8, v0, Lcom/android/exchange/CommandStatusException;->mStatus:I

    .line 226
    .local v8, "status":I
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isNeedsProvisioning(I)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 227
    const/4 v9, 0x4

    iput v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 247
    .end local v0    # "e1":Lcom/android/exchange/CommandStatusException;
    .end local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .end local v8    # "status":I
    :catch_2
    move-exception v6

    .line 248
    .local v6, "ome":Ljava/lang/OutOfMemoryError;
    const/high16 v7, 0x50000

    .line 250
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_3

    .line 228
    .end local v6    # "ome":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "e1":Lcom/android/exchange/CommandStatusException;
    .restart local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .restart local v8    # "status":I
    :cond_e
    :try_start_5
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isDeniedAccess(I)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 229
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTooManyPartnerships(I)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 230
    const/16 v9, 0xb

    iput v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_4

    .line 250
    .end local v0    # "e1":Lcom/android/exchange/CommandStatusException;
    .end local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .end local v8    # "status":I
    :catchall_0
    move-exception v9

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    throw v9

    .line 232
    .restart local v0    # "e1":Lcom/android/exchange/CommandStatusException;
    .restart local v1    # "esa":Lcom/android/exchange/adapter/EmailSyncAdapter;
    .restart local v8    # "status":I
    :cond_f
    :try_start_6
    invoke-static {v8}, Lcom/android/exchange/CommandStatusException$CommandStatus;->isTransientError(I)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 233
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    goto :goto_4

    .line 235
    :cond_10
    const/4 v9, 0x3

    iput v9, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4
.end method

.method private setProtocolInfo(Lcom/android/emailcommon/provider/EmailContent$Account;)V
    .locals 8
    .param p1, "acc"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 506
    if-nez p1, :cond_0

    .line 507
    sget-object v0, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    invoke-static {v0, v4, v5}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object p1

    .line 509
    :cond_0
    iget-object v0, p1, Lcom/android/emailcommon/provider/EmailContent$Account;->mProtocolVersion:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersion:Ljava/lang/String;

    .line 510
    iget-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersion:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    .line 512
    iget-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2007support:Z

    .line 513
    iget-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x402c000000000000L    # 14.0

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010support:Z

    .line 514
    iget-object v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mProtocolVersionDouble:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide v6, 0x402c333333333333L    # 14.1

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    .line 515
    return-void

    :cond_1
    move v0, v2

    .line 512
    goto :goto_0

    :cond_2
    move v0, v2

    .line 513
    goto :goto_1

    :cond_3
    move v1, v2

    .line 514
    goto :goto_2
.end method

.method private writeToServiceLogger(Lcom/android/exchange/EasResponse;I)V
    .locals 4
    .param p1, "res"    # Lcom/android/exchange/EasResponse;
    .param p2, "parseStatus"    # I

    .prologue
    .line 518
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "thread="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/exchange/EasLoadMoreSvc;->getThreadId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mAccount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMailbox="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cmd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mASCmd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    .local v0, "_msg":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 522
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " res has null value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    .line 529
    :goto_0
    return-void

    .line 525
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " res="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/android/exchange/EasResponse;->getStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LoadMoreStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " content="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 526
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "Content-Length"

    invoke-virtual {p1, v1}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "Content-Length"

    invoke-virtual {p1, v1}, Lcom/android/exchange/EasResponse;->getHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 528
    invoke-static {}, Lcom/android/exchange/ExchangeService;->getServiceLogger()Lcom/android/exchange/ServiceLogger;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/exchange/ServiceLogger;->logSyncStats(Ljava/lang/String;)V

    goto :goto_0

    .line 526
    :cond_1
    const-string v1, "0"

    goto :goto_1
.end method


# virtual methods
.method protected prepareCommand(Ljava/lang/String;Ljava/lang/String;)Lcom/android/exchange/adapter/Serializer;
    .locals 7
    .param p1, "login"    # Ljava/lang/String;
    .param p2, "pass"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x22

    const/16 v5, 0x12

    const/16 v4, 0xd

    .line 55
    new-instance v0, Lcom/android/exchange/adapter/Serializer;

    invoke-direct {v0}, Lcom/android/exchange/adapter/Serializer;-><init>()V

    .line 57
    .local v0, "s":Lcom/android/exchange/adapter/Serializer;
    iget-boolean v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2007support:Z

    if-eqz v1, :cond_7

    .line 61
    const/16 v1, 0x505

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 62
    const/16 v1, 0x506

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x507

    const-string v3, "Mailbox"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 64
    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 65
    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v5, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 66
    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 71
    :cond_0
    :goto_0
    const/16 v1, 0x508

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v6, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 74
    const/16 v1, 0x445

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v2

    const/16 v3, 0x446

    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-boolean v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSigned:Z

    if-eqz v1, :cond_6

    const-string v1, "4"

    :goto_1
    invoke-virtual {v2, v3, v1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 75
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 77
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-boolean v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010support:Z

    if-eqz v1, :cond_1

    .line 78
    const/16 v1, 0x514

    invoke-virtual {v0, v1, p1}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 79
    const/16 v1, 0x515

    invoke-virtual {v0, v1, p2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 81
    :cond_1
    iget-boolean v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    if-eqz v1, :cond_2

    .line 82
    const/16 v1, 0x605

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 84
    :cond_2
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 86
    iget-boolean v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIs2010EXsupport:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mRemoveIrmProtectionFlag:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 87
    sget-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v1, :cond_3

    .line 88
    const-string v1, "EasLoadMoreSvc"

    const-string v2, "Removing irm, adding tag "

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_3
    const/16 v1, 0x618

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 91
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 93
    :cond_4
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 115
    :goto_2
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->done()V

    .line 116
    return-object v0

    .line 67
    :cond_5
    iget-object v1, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v1, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 68
    const/16 v1, 0x3d8

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    goto :goto_0

    .line 74
    :cond_6
    const-string v1, "2"

    goto :goto_1

    .line 98
    :cond_7
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 99
    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 100
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x10

    const-string v3, "Email"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v3, v3, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mSyncKey:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMailbox:Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 104
    const/16 v1, 0x17

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const/16 v2, 0x23

    const-string v3, "8"

    invoke-virtual {v1, v2, v3}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    const-string v2, "2"

    invoke-virtual {v1, v6, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 107
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 109
    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    .line 110
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/android/exchange/adapter/Serializer;->start(I)Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-object v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mServerId:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/android/exchange/adapter/Serializer;->data(ILjava/lang/String;)Lcom/android/exchange/adapter/Serializer;

    .line 111
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    .line 113
    invoke-virtual {v0}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/exchange/adapter/Serializer;->end()Lcom/android/exchange/adapter/Serializer;

    goto :goto_2
.end method

.method public run()V
    .locals 11

    .prologue
    const/high16 v10, 0x30000

    const/4 v9, 0x3

    const/16 v8, 0x64

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 403
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_0

    .line 404
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v3, "EasLoadMoreSvc::run() start"

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/android/exchange/EasLoadMoreSvc;->setupService()Z

    .line 409
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_1

    .line 410
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v3, "EasLoadMoreSvc::run() - setupService() end"

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    :cond_1
    :try_start_0
    invoke-direct {p0}, Lcom/android/exchange/EasLoadMoreSvc;->loadMore()I

    move-result v1

    .line 415
    .local v1, "result":I
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LoadMore returned the result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 416
    sparse-switch v1, :sswitch_data_0

    .line 442
    const/4 v2, 0x0

    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_0
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :goto_0
    :sswitch_0
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LoadMore finished mExitStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 457
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 458
    iget v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    sparse-switch v2, :sswitch_data_1

    .line 495
    const-string v2, "Sync ended due to an exception."

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->errorLog(Ljava/lang/String;)V

    .line 500
    .end local v1    # "result":I
    :cond_2
    :goto_1
    sget-boolean v2, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    if-eqz v2, :cond_3

    .line 501
    const-string v2, "DEBUG_VIEW_LOADMORE_EAS_TIME"

    const-string v3, "EasLoadMoreSvc::run() end"

    invoke-static {v2, v3}, Lcom/android/exchange/utility/EASLogger;->debugTime(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_3
    return-void

    .line 418
    .restart local v1    # "result":I
    :sswitch_1
    const/4 v2, 0x4

    :try_start_1
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_1
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 445
    .end local v1    # "result":I
    :catch_0
    move-exception v0

    .line 446
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    const/4 v2, 0x6

    :try_start_2
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    .line 447
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    sget v4, Lcom/android/emailcommon/provider/EmailContent$Account;->DEVICE_IS_BLOCKED:I

    invoke-static {v2, v3, v4}, Lcom/android/exchange/ExchangeService;->blockDevice(JI)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 456
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LoadMore finished mExitStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 457
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 458
    iget v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    sparse-switch v2, :sswitch_data_2

    .line 495
    const-string v2, "Sync ended due to an exception."

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->errorLog(Ljava/lang/String;)V

    goto :goto_1

    .line 422
    .end local v0    # "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    .restart local v1    # "result":I
    :sswitch_2
    const/4 v2, 0x2

    :try_start_3
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_3
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 448
    .end local v1    # "result":I
    :catch_1
    move-exception v0

    .line 449
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 450
    const-string v2, "IOException caught in EasLoadMoreSvc"

    invoke-virtual {p0, v2, v0}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 451
    const/4 v2, 0x1

    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 456
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LoadMore finished mExitStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 457
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 458
    iget v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    sparse-switch v2, :sswitch_data_3

    .line 495
    const-string v2, "Sync ended due to an exception."

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->errorLog(Ljava/lang/String;)V

    goto :goto_1

    .line 426
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "result":I
    :sswitch_3
    const/4 v2, 0x3

    :try_start_5
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_5
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 452
    .end local v1    # "result":I
    :catch_2
    move-exception v0

    .line 453
    .local v0, "e":Ljava/lang/Exception;
    :try_start_6
    const-string v2, "Exception caught in EasLoadMoreSvc"

    invoke-virtual {p0, v2, v0}, Lcom/android/exchange/EasLoadMoreSvc;->userLog(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 454
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 456
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LoadMore finished mExitStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 457
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 458
    iget v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    sparse-switch v2, :sswitch_data_4

    .line 495
    const-string v2, "Sync ended due to an exception."

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->errorLog(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 434
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "result":I
    :sswitch_4
    const/high16 v2, 0x40000

    :try_start_7
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_7
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    .line 456
    .end local v1    # "result":I
    :catchall_0
    move-exception v2

    new-array v3, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LoadMore finished mExitStatus = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 457
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->doneOutOfBand(Lcom/android/exchange/AbstractSyncService;)V

    .line 458
    iget v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I

    sparse-switch v3, :sswitch_data_5

    .line 495
    const-string v3, "Sync ended due to an exception."

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasLoadMoreSvc;->errorLog(Ljava/lang/String;)V

    .line 496
    :cond_4
    :goto_2
    throw v2

    .line 438
    .restart local v1    # "result":I
    :sswitch_5
    const/high16 v2, 0x30000

    :try_start_8
    iput v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mExitStatus:I
    :try_end_8
    .catch Lcom/android/emailcommon/utility/DeviceAccessException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 460
    :sswitch_6
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "EXIT_DONE in EasLoadMoreSvc"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 468
    :sswitch_7
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " case!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 471
    sget-boolean v2, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-nez v2, :cond_2

    .line 472
    sget-object v2, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 473
    sput-boolean v6, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 481
    :sswitch_8
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v9, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 487
    :sswitch_9
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/high16 v4, 0x40000

    invoke-direct {p0, v2, v3, v4, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 491
    :sswitch_a
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v10, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 460
    .end local v1    # "result":I
    .local v0, "e":Lcom/android/emailcommon/utility/DeviceAccessException;
    :sswitch_b
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "EXIT_DONE in EasLoadMoreSvc"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 468
    :sswitch_c
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " case!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 471
    sget-boolean v2, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-nez v2, :cond_2

    .line 472
    sget-object v2, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 473
    sput-boolean v6, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 481
    :sswitch_d
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v9, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 487
    :sswitch_e
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/high16 v4, 0x40000

    invoke-direct {p0, v2, v3, v4, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 491
    :sswitch_f
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v10, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 460
    .local v0, "e":Ljava/io/IOException;
    :sswitch_10
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "EXIT_DONE in EasLoadMoreSvc"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 468
    :sswitch_11
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " case!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 471
    sget-boolean v2, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-nez v2, :cond_2

    .line 472
    sget-object v2, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 473
    sput-boolean v6, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 481
    :sswitch_12
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v9, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 487
    :sswitch_13
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/high16 v4, 0x40000

    invoke-direct {p0, v2, v3, v4, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 491
    :sswitch_14
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v10, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    :sswitch_15
    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "EXIT_DONE in EasLoadMoreSvc"

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_1

    .line 468
    :sswitch_16
    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " case!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {p0, v2}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 471
    sget-boolean v2, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-nez v2, :cond_2

    .line 472
    sget-object v2, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 473
    sput-boolean v6, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_1

    .line 481
    :sswitch_17
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v9, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 487
    :sswitch_18
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/high16 v4, 0x40000

    invoke-direct {p0, v2, v3, v4, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 491
    :sswitch_19
    iget-object v2, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v2, v2, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v2, v3, v10, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_1

    .line 460
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_1a
    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "EXIT_DONE in EasLoadMoreSvc"

    aput-object v4, v3, v7

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    goto/16 :goto_2

    .line 468
    :sswitch_1b
    new-array v3, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sync ended due to CHECK_PROVISIONING_IN_PROGRESS "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " case!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v3}, Lcom/android/exchange/EasLoadMoreSvc;->userLog([Ljava/lang/String;)V

    .line 471
    sget-boolean v3, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    if-nez v3, :cond_4

    .line 472
    sget-object v3, Lcom/android/exchange/EasLoadMoreSvc;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/exchange/EasLoadMoreSvc;->mAccount:Lcom/android/emailcommon/provider/EmailContent$Account;

    iget-wide v4, v4, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v3, v4, v5, v6}, Lcom/android/exchange/ExchangeService;->reloadFolderList(Landroid/content/Context;JZ)V

    .line 473
    sput-boolean v6, Lcom/android/exchange/EasLoadMoreSvc;->CHECK_PROVISIONING_IN_PROGRESS:Z

    goto/16 :goto_2

    .line 481
    :sswitch_1c
    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v4, v5, v9, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_2

    .line 487
    :sswitch_1d
    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    const/high16 v3, 0x40000

    invoke-direct {p0, v4, v5, v3, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_2

    .line 491
    :sswitch_1e
    iget-object v3, p0, Lcom/android/exchange/EasLoadMoreSvc;->mMsg:Lcom/android/emailcommon/provider/EmailContent$Message;

    iget-wide v4, v3, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    invoke-direct {p0, v4, v5, v10, v8}, Lcom/android/exchange/EasLoadMoreSvc;->loadMoreCb(JII)V

    goto/16 :goto_2

    .line 416
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_3
        0x16 -> :sswitch_2
        0x17 -> :sswitch_1
        0x30000 -> :sswitch_5
        0x40000 -> :sswitch_4
        0x100000 -> :sswitch_0
    .end sparse-switch

    .line 458
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_6
        0x3 -> :sswitch_9
        0x4 -> :sswitch_7
        0x6 -> :sswitch_8
        0x7 -> :sswitch_8
        0x30000 -> :sswitch_a
        0x40000 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_b
        0x3 -> :sswitch_e
        0x4 -> :sswitch_c
        0x6 -> :sswitch_d
        0x7 -> :sswitch_d
        0x30000 -> :sswitch_f
        0x40000 -> :sswitch_e
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_10
        0x3 -> :sswitch_13
        0x4 -> :sswitch_11
        0x6 -> :sswitch_12
        0x7 -> :sswitch_12
        0x30000 -> :sswitch_14
        0x40000 -> :sswitch_13
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x0 -> :sswitch_15
        0x3 -> :sswitch_18
        0x4 -> :sswitch_16
        0x6 -> :sswitch_17
        0x7 -> :sswitch_17
        0x30000 -> :sswitch_19
        0x40000 -> :sswitch_18
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x0 -> :sswitch_1a
        0x3 -> :sswitch_1d
        0x4 -> :sswitch_1b
        0x6 -> :sswitch_1c
        0x7 -> :sswitch_1c
        0x30000 -> :sswitch_1e
        0x40000 -> :sswitch_1d
    .end sparse-switch
.end method

.method public userCancelled()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/EasLoadMoreSvc;->mIsCancelledByUser:Z

    .line 52
    return-void
.end method
