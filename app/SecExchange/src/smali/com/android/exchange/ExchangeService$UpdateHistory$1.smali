.class final Lcom/android/exchange/ExchangeService$UpdateHistory$1;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService$UpdateHistory;->maybeStartUpdateHistoryThread(Lcom/android/exchange/ExchangeService$UpdateHistory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$updateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService$UpdateHistory;)V
    .locals 0

    .prologue
    .line 7522
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory$1;->val$updateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 7525
    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 7526
    :cond_0
    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "Starting UpdateHistory thread..."

    :goto_0
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->log(Ljava/lang/String;)V

    .line 7528
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/exchange/ExchangeService$UpdateHistory$1;->val$updateHistory:Lcom/android/exchange/ExchangeService$UpdateHistory;

    const-string v2, "UpdateHistory"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    # setter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$3502(Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 7529
    # getter for: Lcom/android/exchange/ExchangeService;->sUpdateHistoryThread:Ljava/lang/Thread;
    invoke-static {}, Lcom/android/exchange/ExchangeService;->access$3500()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 7531
    :cond_1
    return-void

    .line 7526
    :cond_2
    const-string v0, "Restarting UpdateHistory thread..."

    goto :goto_0
.end method
