.class Lcom/android/exchange/ExchangeService$TaskObserver$1;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService$TaskObserver;->onChange(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/exchange/ExchangeService$TaskObserver;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService$TaskObserver;)V
    .locals 0

    .prologue
    .line 2773
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 2775
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-boolean v0, v0, Lcom/android/exchange/ExchangeService$TaskObserver;->bSync:Z

    if-eqz v0, :cond_4

    .line 2776
    const/4 v6, 0x0

    .line 2777
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 2779
    .local v7, "localchange":I
    sget-object v0, Lcom/android/exchange/ExchangeService;->INSTANCE:Lcom/android/exchange/ExchangeService;

    iget-object v1, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-wide v2, v1, Lcom/android/exchange/ExchangeService$TaskObserver;->mAccountId:J

    const/16 v1, 0x43

    invoke-static {v0, v2, v3, v1}, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->restoreMailboxOfType(Landroid/content/Context;JI)Lcom/android/emailcommon/provider/EmailContent$Mailbox;

    move-result-object v8

    .line 2783
    .local v8, "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    if-eqz v8, :cond_3

    .line 2784
    :try_start_0
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-object v0, v0, Lcom/android/exchange/ExchangeService$TaskObserver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$1800(Lcom/android/exchange/ExchangeService;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Tasks;->UPDATED_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "accountKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2787
    if-eqz v6, :cond_0

    .line 2788
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v7, v0

    .line 2789
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2790
    const/4 v6, 0x0

    .line 2793
    :cond_0
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-object v0, v0, Lcom/android/exchange/ExchangeService$TaskObserver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$1800(Lcom/android/exchange/ExchangeService;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Tasks;->DELETED_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "accountKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2796
    if-eqz v6, :cond_1

    .line 2797
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v7, v0

    .line 2798
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2799
    const/4 v6, 0x0

    .line 2802
    :cond_1
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-object v0, v0, Lcom/android/exchange/ExchangeService$TaskObserver;->this$0:Lcom/android/exchange/ExchangeService;

    # getter for: Lcom/android/exchange/ExchangeService;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/android/exchange/ExchangeService;->access$1800(Lcom/android/exchange/ExchangeService;)Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_sync_dirty= 1 AND accountKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v8, Lcom/android/emailcommon/provider/EmailContent$Mailbox;->mAccountKey:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2805
    if-eqz v6, :cond_2

    .line 2806
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/2addr v7, v0

    .line 2807
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2808
    const/4 v6, 0x0

    .line 2810
    :cond_2
    const-string v0, "Task Observer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " Task Change device count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2811
    if-lez v7, :cond_3

    .line 2812
    iget-object v0, p0, Lcom/android/exchange/ExchangeService$TaskObserver$1;->this$1:Lcom/android/exchange/ExchangeService$TaskObserver;

    iget-object v0, v0, Lcom/android/exchange/ExchangeService$TaskObserver;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v1, 0x2

    const/4 v2, 0x0

    # invokes: Lcom/android/exchange/ExchangeService;->requestSync(Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    invoke-static {v0, v8, v1, v2}, Lcom/android/exchange/ExchangeService;->access$1100(Lcom/android/exchange/ExchangeService;Lcom/android/emailcommon/provider/EmailContent$Mailbox;ILcom/android/exchange/Request;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2816
    :cond_3
    if-eqz v6, :cond_4

    .line 2817
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2821
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "localchange":I
    .end local v8    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :cond_4
    return-void

    .line 2816
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "localchange":I
    .restart local v8    # "mailbox":Lcom/android/emailcommon/provider/EmailContent$Mailbox;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 2817
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method
