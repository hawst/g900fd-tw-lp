.class public Lcom/android/exchange/IllegalHeartbeatException;
.super Lcom/android/exchange/EasException;
.source "IllegalHeartbeatException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final mLegalHeartbeat:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "legalHeartbeat"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/android/exchange/EasException;-><init>()V

    .line 26
    iput p1, p0, Lcom/android/exchange/IllegalHeartbeatException;->mLegalHeartbeat:I

    .line 27
    return-void
.end method
