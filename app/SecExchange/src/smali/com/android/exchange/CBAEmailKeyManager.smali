.class public Lcom/android/exchange/CBAEmailKeyManager;
.super Ljavax/net/ssl/X509ExtendedKeyManager;
.source "CBAEmailKeyManager.java"


# instance fields
.field public chain:[Ljava/security/cert/X509Certificate;

.field private mAlias:Ljava/lang/String;

.field private mSslClient:Lcom/android/exchange/cba/SSLCBAClient;


# direct methods
.method public constructor <init>(Lcom/android/exchange/cba/SSLCBAClient;)V
    .locals 1
    .param p1, "sslClient"    # Lcom/android/exchange/cba/SSLCBAClient;

    .prologue
    .line 24
    invoke-direct {p0}, Ljavax/net/ssl/X509ExtendedKeyManager;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/exchange/CBAEmailKeyManager;->chain:[Ljava/security/cert/X509Certificate;

    .line 25
    iput-object p1, p0, Lcom/android/exchange/CBAEmailKeyManager;->mSslClient:Lcom/android/exchange/cba/SSLCBAClient;

    .line 26
    return-void
.end method


# virtual methods
.method public chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1
    .param p1, "keyType"    # [Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/android/exchange/CBAEmailKeyManager;->mSslClient:Lcom/android/exchange/cba/SSLCBAClient;

    invoke-virtual {v0}, Lcom/android/exchange/cba/SSLCBAClient;->chooseAlias()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/exchange/CBAEmailKeyManager;->mAlias:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/android/exchange/CBAEmailKeyManager;->mAlias:Ljava/lang/String;

    return-object v0
.end method

.method public chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;
    .param p3, "socket"    # Ljava/net/Socket;

    .prologue
    .line 36
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 41
    if-eqz p1, :cond_1

    .line 42
    invoke-static {}, Lcom/android/exchange/cba/SSLCBAClient;->getStaticContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 44
    iget-object v2, p0, Lcom/android/exchange/CBAEmailKeyManager;->mSslClient:Lcom/android/exchange/cba/SSLCBAClient;

    invoke-virtual {v2}, Lcom/android/exchange/cba/SSLCBAClient;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 47
    :cond_0
    :try_start_0
    invoke-static {v0, p1}, Landroid/security/KeyChain;->getCertificateChain(Landroid/content/Context;Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Landroid/security/KeyChainException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 56
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-object v2

    .line 48
    .restart local v0    # "context":Landroid/content/Context;
    :catch_0
    move-exception v1

    .line 49
    .local v1, "e":Landroid/security/KeyChainException;
    invoke-virtual {v1}, Landroid/security/KeyChainException;->printStackTrace()V

    .line 56
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "e":Landroid/security/KeyChainException;
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 50
    .restart local v0    # "context":Landroid/content/Context;
    :catch_1
    move-exception v1

    .line 51
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 52
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v1

    .line 53
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 3
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/exchange/CBAEmailKeyManager;->mAlias:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/exchange/CBAEmailKeyManager;->mAlias:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 69
    if-eqz p1, :cond_1

    .line 70
    invoke-static {}, Lcom/android/exchange/cba/SSLCBAClient;->getStaticContext()Landroid/content/Context;

    move-result-object v0

    .line 71
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 72
    iget-object v2, p0, Lcom/android/exchange/CBAEmailKeyManager;->mSslClient:Lcom/android/exchange/cba/SSLCBAClient;

    invoke-virtual {v2}, Lcom/android/exchange/cba/SSLCBAClient;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 75
    :cond_0
    :try_start_0
    invoke-static {v0, p1}, Landroid/security/KeyChain;->getPrivateKey(Landroid/content/Context;Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_0
    .catch Landroid/security/KeyChainException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 84
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-object v2

    .line 76
    .restart local v0    # "context":Landroid/content/Context;
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Landroid/security/KeyChainException;
    invoke-virtual {v1}, Landroid/security/KeyChainException;->printStackTrace()V

    .line 84
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "e":Landroid/security/KeyChainException;
    :cond_1
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 78
    .restart local v0    # "context":Landroid/content/Context;
    :catch_1
    move-exception v1

    .line 79
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 80
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v1

    .line 81
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 1
    .param p1, "keyType"    # Ljava/lang/String;
    .param p2, "issuers"    # [Ljava/security/Principal;

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method
