.class public Lcom/android/exchange/MessageMoveRequest;
.super Lcom/android/exchange/Request;
.source "MessageMoveRequest.java"


# instance fields
.field public final mMailboxId:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 1
    .param p1, "messageId"    # J
    .param p3, "mailboxId"    # J

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/android/exchange/Request;-><init>(J)V

    .line 27
    iput-wide p3, p0, Lcom/android/exchange/MessageMoveRequest;->mMailboxId:J

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 34
    instance-of v1, p1, Lcom/android/exchange/MessageMoveRequest;

    if-nez v1, :cond_1

    .line 36
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    check-cast p1, Lcom/android/exchange/MessageMoveRequest;

    .end local p1    # "o":Ljava/lang/Object;
    iget-wide v2, p1, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    iget-wide v4, p0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/android/exchange/MessageMoveRequest;->mMessageId:J

    long-to-int v0, v0

    return v0
.end method
