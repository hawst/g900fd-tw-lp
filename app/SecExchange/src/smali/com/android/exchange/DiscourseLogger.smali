.class public Lcom/android/exchange/DiscourseLogger;
.super Ljava/lang/Object;
.source "DiscourseLogger.java"


# instance fields
.field private mBuffer:[Ljava/lang/String;

.field private final mBufferSize:I

.field private mPos:I

.field private mPrefixDate:Z

.field private final mReceivingLine:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "bufferSize"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/exchange/DiscourseLogger;->mPrefixDate:Z

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/exchange/DiscourseLogger;->mReceivingLine:Ljava/lang/StringBuilder;

    .line 48
    iput p1, p0, Lcom/android/exchange/DiscourseLogger;->mBufferSize:I

    .line 49
    invoke-direct {p0}, Lcom/android/exchange/DiscourseLogger;->initBuffer()V

    .line 50
    return-void
.end method

.method private declared-synchronized addLine(Ljava/lang/String;)V
    .locals 6
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/android/exchange/DiscourseLogger;->mPrefixDate:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_0

    .line 66
    :try_start_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 67
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 68
    .local v0, "cal":Ljava/util/Calendar;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 69
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "time="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    .line 75
    .end local v0    # "cal":Ljava/util/Calendar;
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_0
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/android/exchange/DiscourseLogger;->mBuffer:[Ljava/lang/String;

    iget v4, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    aput-object p1, v3, v4

    .line 76
    iget v3, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    .line 77
    iget v3, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    iget v4, p0, Lcom/android/exchange/DiscourseLogger;->mBufferSize:I

    if-lt v3, v4, :cond_1

    .line 78
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 80
    :cond_1
    monitor-exit p0

    return-void

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    :try_start_3
    const-string v3, "DiscourseLogger"

    const-string v4, "addLine: NoSuchMethodError, cannot get current datetime"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 64
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private addReceivingLineToBuffer()V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/android/exchange/DiscourseLogger;->mReceivingLine:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/android/exchange/DiscourseLogger;->mReceivingLine:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/exchange/DiscourseLogger;->addLine(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/android/exchange/DiscourseLogger;->mReceivingLine:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 87
    :cond_0
    return-void
.end method

.method private initBuffer()V
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/android/exchange/DiscourseLogger;->mBufferSize:I

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/exchange/DiscourseLogger;->mBuffer:[Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method getLines()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/android/exchange/DiscourseLogger;->addReceivingLineToBuffer()V

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget v4, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    .line 123
    .local v4, "start":I
    iget v1, p0, Lcom/android/exchange/DiscourseLogger;->mPos:I

    .line 125
    .local v1, "pos":I
    :cond_0
    iget-object v5, p0, Lcom/android/exchange/DiscourseLogger;->mBuffer:[Ljava/lang/String;

    aget-object v3, v5, v1

    .line 126
    .local v3, "s":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 127
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_1
    add-int/lit8 v5, v1, 0x1

    iget v6, p0, Lcom/android/exchange/DiscourseLogger;->mBufferSize:I

    rem-int v1, v5, v6

    .line 130
    if-ne v1, v4, :cond_0

    .line 132
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v2, v5, [Ljava/lang/String;

    .line 133
    .local v2, "ret":[Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 134
    return-object v2
.end method

.method public logLastDiscourse(Ljava/io/PrintWriter;)V
    .locals 7
    .param p1, "writer"    # Ljava/io/PrintWriter;

    .prologue
    .line 172
    const-string v5, "Statistics: "

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0}, Lcom/android/exchange/DiscourseLogger;->getLines()[Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "lines":[Ljava/lang/String;
    array-length v5, v3

    if-nez v5, :cond_1

    .line 180
    :cond_0
    return-void

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/android/exchange/DiscourseLogger;->getLines()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 178
    .local v4, "r":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "   "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 177
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public logString(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 112
    const-string v0, "DiscourseLogger"

    invoke-static {v0, p1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/android/exchange/DiscourseLogger;->addLine(Ljava/lang/String;)V

    .line 114
    return-void
.end method
