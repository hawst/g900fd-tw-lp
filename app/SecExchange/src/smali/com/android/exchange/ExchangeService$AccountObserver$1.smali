.class Lcom/android/exchange/ExchangeService$AccountObserver$1;
.super Ljava/lang/Object;
.source "ExchangeService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/ExchangeService$AccountObserver;-><init>(Lcom/android/exchange/ExchangeService;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/exchange/ExchangeService$AccountObserver;

.field final synthetic val$this$0:Lcom/android/exchange/ExchangeService;

.field final synthetic val$tmpAccountList:Lcom/android/exchange/ExchangeService$AccountList;


# direct methods
.method constructor <init>(Lcom/android/exchange/ExchangeService$AccountObserver;Lcom/android/exchange/ExchangeService;Lcom/android/exchange/ExchangeService$AccountList;)V
    .locals 0

    .prologue
    .line 2347
    iput-object p1, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->this$1:Lcom/android/exchange/ExchangeService$AccountObserver;

    iput-object p2, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->val$this$0:Lcom/android/exchange/ExchangeService;

    iput-object p3, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->val$tmpAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2350
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->val$tmpAccountList:Lcom/android/exchange/ExchangeService$AccountList;

    invoke-virtual {v2}, Lcom/android/exchange/ExchangeService$AccountList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 2351
    .local v0, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    iget v2, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_0

    .line 2357
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->this$1:Lcom/android/exchange/ExchangeService$AccountObserver;

    iget-object v2, v2, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/emailcommon/service/PolicyServiceProxy;->isActive(Landroid/content/Context;Lcom/android/emailcommon/service/PolicySet;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2358
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->this$1:Lcom/android/exchange/ExchangeService$AccountObserver;

    iget-object v2, v2, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/android/emailcommon/service/PolicyServiceProxy;->setAccountHoldFlag(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Account;Z)V

    .line 2360
    const-string v2, "ExchangeService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AccountObserver - isActive true; release hold for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2364
    :cond_1
    const-string v2, "ExchangeService"

    const-string v3, "AccountObserver - policiesRequired"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365
    iget-object v2, p0, Lcom/android/exchange/ExchangeService$AccountObserver$1;->this$1:Lcom/android/exchange/ExchangeService$AccountObserver;

    iget-object v2, v2, Lcom/android/exchange/ExchangeService$AccountObserver;->this$0:Lcom/android/exchange/ExchangeService;

    iget-wide v4, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    invoke-static {v2, v4, v5}, Lcom/android/emailcommon/service/PolicyServiceProxy;->policiesRequired(Landroid/content/Context;J)V

    goto :goto_0

    .line 2370
    .end local v0    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_2
    return-void
.end method
