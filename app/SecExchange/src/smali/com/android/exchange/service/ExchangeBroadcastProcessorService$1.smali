.class Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;
.super Ljava/lang/Object;
.source "ExchangeBroadcastProcessorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->onBootCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;


# direct methods
.method constructor <init>(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 106
    :try_start_0
    iget-object v1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    # getter for: Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->access$000(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->isExistEasAccount(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    # getter for: Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->access$000(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->runningTask(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    invoke-virtual {v1}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->stopSelf()V

    .line 108
    iget-object v1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    # getter for: Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->access$000(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->killMyProcess(Landroid/content/Context;)V

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;->this$0:Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    # getter for: Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->access$000(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/android/exchange/ExchangeService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Email"

    invoke-static {v1, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
