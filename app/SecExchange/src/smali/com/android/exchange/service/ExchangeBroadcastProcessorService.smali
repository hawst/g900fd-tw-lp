.class public Lcom/android/exchange/service/ExchangeBroadcastProcessorService;
.super Landroid/app/IntentService;
.source "ExchangeBroadcastProcessorService.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->setIntentRedelivery(Z)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private onBootCompleted()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;

    invoke-direct {v0, p0}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService$1;-><init>(Lcom/android/exchange/service/ExchangeBroadcastProcessorService;)V

    invoke-static {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    .line 117
    return-void
.end method

.method public static processBroadcastIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "broadcastIntent"    # Landroid/content/Intent;

    .prologue
    .line 60
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "broadcast_receiver"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 63
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 64
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 70
    :try_start_0
    invoke-virtual {p0}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->mContext:Landroid/content/Context;

    .line 72
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "action":Ljava/lang/String;
    const-string v5, "broadcast_receiver"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    const-string v5, "android.intent.extra.INTENT"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 75
    .local v2, "broadcastIntent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "broadcastAction":Ljava/lang/String;
    const-string v5, "Email"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exchange action : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 79
    invoke-direct {p0}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->onBootCompleted()V

    .line 96
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "broadcastAction":Ljava/lang/String;
    .end local v2    # "broadcastIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 80
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "broadcastAction":Ljava/lang/String;
    .restart local v2    # "broadcastIntent":Landroid/content/Intent;
    :cond_1
    const-string v5, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 81
    sget-boolean v5, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 82
    const-string v5, "Email"

    const-string v6, "Login accounts changed; reconciling..."

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_2
    invoke-static {p0}, Lcom/android/exchange/ExchangeService;->reconcileAccounts(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 93
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "broadcastAction":Ljava/lang/String;
    .end local v2    # "broadcastIntent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 94
    .local v3, "e":Ljava/lang/Exception;
    const-string v5, "Email"

    invoke-static {v5, v3}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 85
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "broadcastAction":Ljava/lang/String;
    .restart local v2    # "broadcastIntent":Landroid/content/Intent;
    :cond_3
    :try_start_1
    const-string v5, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 86
    const-string v5, "reason"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 87
    .local v4, "reason":I
    const-string v5, "Email"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "broadcastAction reason = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/android/exchange/service/ExchangeBroadcastProcessorService;->onBootCompleted()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
