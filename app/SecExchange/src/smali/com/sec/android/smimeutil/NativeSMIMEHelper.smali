.class public Lcom/sec/android/smimeutil/NativeSMIMEHelper;
.super Ljava/lang/Object;
.source "NativeSMIMEHelper.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    const-string v0, "opensslsmime"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->SMIMEclinit()V

    .line 140
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native PKCS7encrypt(J[JLjava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native PKCS7sign(Ljava/lang/String;Ljava/lang/String;JJ[JLjava/lang/String;)Z
.end method

.method private static native SMIMEclinit()V
.end method

.method public static openSSLPKCS7Sign(Ljava/io/File;Ljava/io/File;Ljava/security/PrivateKey;[BLjava/util/List;Ljava/lang/String;)Z
    .locals 22
    .param p0, "inputFile"    # Ljava/io/File;
    .param p1, "outputFile"    # Ljava/io/File;
    .param p2, "privateKey"    # Ljava/security/PrivateKey;
    .param p3, "PEMcertificateBytes"    # [B
    .param p5, "signingAlgorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/security/PrivateKey;",
            "[B",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/NullPointerException;,
            Ljava/security/InvalidKeyException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 184
    .local p4, "PEMcertificateChainBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 185
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v19, "Input File does not exist."

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 186
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 187
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v19, "Output File does not exist."

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 189
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "inputFilePath":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 193
    .local v9, "outputFilePath":Ljava/lang/String;
    new-instance v12, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p3

    invoke-direct {v12, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 194
    .local v12, "certIs":Ljava/io/InputStream;
    invoke-static {v12}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v15

    .line 195
    .local v15, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v15}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v16

    .line 196
    .local v16, "openSSLcertEncoded":[B
    invoke-static/range {v16 .. v16}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v4

    .line 198
    .local v4, "signCertRef":J
    invoke-static/range {p2 .. p2}, Lcom/android/org/conscrypt/OpenSSLKey;->fromPrivateKey(Ljava/security/PrivateKey;)Lcom/android/org/conscrypt/OpenSSLKey;

    move-result-object v14

    .line 199
    .local v14, "oKey":Lcom/android/org/conscrypt/OpenSSLKey;
    invoke-virtual {v14}, Lcom/android/org/conscrypt/OpenSSLKey;->getPkeyContext()J

    move-result-wide v6

    .line 202
    .local v6, "evpKeyRef":J
    const/4 v3, 0x0

    new-array v8, v3, [J

    .line 203
    .local v8, "arr1":[J
    if-eqz p4, :cond_2

    .line 204
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v17

    .line 205
    .local v17, "size":I
    move/from16 v0, v17

    new-array v8, v0, [J

    .line 206
    const/16 v18, 0x0

    .local v18, "u":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    .line 207
    move-object/from16 v0, p4

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [B

    .line 208
    .local v10, "a":[B
    new-instance v11, Ljava/io/ByteArrayInputStream;

    invoke-direct {v11, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 209
    .local v11, "cIs":Ljava/io/InputStream;
    invoke-static {v11}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v13

    .line 210
    .local v13, "chainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v13}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v20

    aput-wide v20, v8, v18

    .line 206
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .end local v10    # "a":[B
    .end local v11    # "cIs":Ljava/io/InputStream;
    .end local v13    # "chainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v17    # "size":I
    .end local v18    # "u":I
    :cond_2
    move-object/from16 v3, p5

    .line 213
    invoke-static/range {v2 .. v9}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->PKCS7sign(Ljava/lang/String;Ljava/lang/String;JJ[JLjava/lang/String;)Z

    move-result v3

    return v3
.end method

.method public static openSSLPKCS7encrypt(Ljava/io/File;Ljava/io/File;Ljava/util/List;Ljava/lang/String;)Z
    .locals 16
    .param p0, "inputData"    # Ljava/io/File;
    .param p1, "output"    # Ljava/io/File;
    .param p3, "encryptionAlgorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 218
    .local p2, "PEMcertificateBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 219
    new-instance v14, Ljava/io/FileNotFoundException;

    const-string v15, "Input File does not exist."

    invoke-direct {v14, v15}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 220
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 221
    new-instance v14, Ljava/io/FileNotFoundException;

    const-string v15, "Output File does not exist."

    invoke-direct {v14, v15}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 223
    :cond_1
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 224
    .local v9, "fis":Ljava/io/FileInputStream;
    new-instance v3, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;

    invoke-direct {v3, v9}, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;-><init>(Ljava/io/InputStream;)V

    .line 225
    .local v3, "bis":Lcom/android/org/conscrypt/OpenSSLBIOInputStream;
    invoke-static {v3}, Lcom/android/org/conscrypt/NativeCrypto;->create_BIO_InputStream(Lcom/android/org/conscrypt/OpenSSLBIOInputStream;)J

    move-result-wide v4

    .line 227
    .local v4, "bioRef":J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v8

    .line 228
    .local v8, "certsRefArrLength":I
    new-array v7, v8, [J

    .line 229
    .local v7, "certsRefArr":[J
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v8, :cond_2

    .line 230
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 231
    .local v2, "arr":[B
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 232
    .local v6, "certIs":Ljava/io/InputStream;
    invoke-static {v6}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v11

    .line 233
    .local v11, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v11}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v12

    .line 234
    .local v12, "openSSLcertEncoded":[B
    invoke-static {v12}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v14

    aput-wide v14, v7, v10

    .line 229
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 237
    .end local v2    # "arr":[B
    .end local v6    # "certIs":Ljava/io/InputStream;
    .end local v11    # "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v12    # "openSSLcertEncoded":[B
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    .line 238
    .local v13, "outputFilePath":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-static {v4, v5, v7, v13, v0}, Lcom/sec/android/smimeutil/NativeSMIMEHelper;->PKCS7encrypt(J[JLjava/lang/String;Ljava/lang/String;)Z

    move-result v14

    return v14
.end method
