.class public Lcom/sec/android/openpgp/KeyGenerator;
.super Ljava/lang/Object;
.source "KeyGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/openpgp/KeyGenerator$AlgorithmChoice;
    }
.end annotation


# static fields
.field private static NO_MINUTES_IN_DAY:I

.field private static final PREFERRED_COMPRESSION_ALGORITHMS:[I

.field private static final PREFERRED_HASH_ALGORITHMS:[I

.field private static final PREFERRED_SYMMETRIC_ALGORITHMS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 52
    const v0, 0x15180

    sput v0, Lcom/sec/android/openpgp/KeyGenerator;->NO_MINUTES_IN_DAY:I

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/openpgp/KeyGenerator;->PREFERRED_SYMMETRIC_ALGORITHMS:[I

    .line 59
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/openpgp/KeyGenerator;->PREFERRED_HASH_ALGORITHMS:[I

    .line 63
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/openpgp/KeyGenerator;->PREFERRED_COMPRESSION_ALGORITHMS:[I

    return-void

    .line 54
    nop

    :array_0
    .array-data 4
        0x9
        0x2
        0x3
    .end array-data

    .line 59
    :array_1
    .array-data 4
        0x2
        0x8
    .end array-data

    .line 63
    :array_2
    .array-data 4
        0x1
        0x2
    .end array-data
.end method
