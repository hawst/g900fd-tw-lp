.class public Lcom/sec/android/openpgp/PGPGenericException;
.super Ljava/lang/Exception;
.source "PGPGenericException.java"


# static fields
.field private static final serialVersionUID:J = -0x22bf9013b050b168L


# instance fields
.field private mErrcode:I

.field private underlying:Ljava/lang/Exception;


# virtual methods
.method public getCause()Ljava/lang/Throwable;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/openpgp/PGPGenericException;->underlying:Ljava/lang/Exception;

    return-object v0
.end method
