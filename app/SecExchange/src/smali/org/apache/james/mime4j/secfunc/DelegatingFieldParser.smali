.class public Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;
.super Ljava/lang/Object;
.source "DelegatingFieldParser.java"

# interfaces
.implements Lorg/apache/james/mime4j/secfunc/FieldParser;


# instance fields
.field private defaultParser:Lorg/apache/james/mime4j/secfunc/FieldParser;

.field private parsers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/apache/james/mime4j/secfunc/FieldParser;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->parsers:Ljava/util/Map;

    .line 25
    new-instance v0, Lorg/apache/james/mime4j/secfunc/UnstructuredField$Parser;

    invoke-direct {v0}, Lorg/apache/james/mime4j/secfunc/UnstructuredField$Parser;-><init>()V

    iput-object v0, p0, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->defaultParser:Lorg/apache/james/mime4j/secfunc/FieldParser;

    return-void
.end method


# virtual methods
.method public getParser(Ljava/lang/String;)Lorg/apache/james/mime4j/secfunc/FieldParser;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 38
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->parsers:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/james/mime4j/secfunc/FieldParser;

    .line 39
    .local v0, "field":Lorg/apache/james/mime4j/secfunc/FieldParser;
    if-nez v0, :cond_0

    .line 40
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->defaultParser:Lorg/apache/james/mime4j/secfunc/FieldParser;

    .line 42
    .end local v0    # "field":Lorg/apache/james/mime4j/secfunc/FieldParser;
    :cond_0
    return-object v0
.end method

.method public parse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/james/mime4j/secfunc/Field;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "body"    # Ljava/lang/String;
    .param p3, "raw"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->getParser(Ljava/lang/String;)Lorg/apache/james/mime4j/secfunc/FieldParser;

    move-result-object v0

    .line 47
    .local v0, "parser":Lorg/apache/james/mime4j/secfunc/FieldParser;
    invoke-interface {v0, p1, p2, p3}, Lorg/apache/james/mime4j/secfunc/FieldParser;->parse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/james/mime4j/secfunc/Field;

    move-result-object v1

    return-object v1
.end method

.method public final setFieldParser(Ljava/lang/String;Lorg/apache/james/mime4j/secfunc/FieldParser;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/apache/james/mime4j/secfunc/FieldParser;

    .prologue
    .line 34
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;->parsers:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method
