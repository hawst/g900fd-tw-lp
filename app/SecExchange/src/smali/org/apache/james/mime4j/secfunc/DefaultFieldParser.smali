.class public Lorg/apache/james/mime4j/secfunc/DefaultFieldParser;
.super Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;
.source "DefaultFieldParser.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/DelegatingFieldParser;-><init>()V

    .line 25
    new-instance v0, Lorg/apache/james/mime4j/secfunc/DateTimeField$Parser;

    invoke-direct {v0}, Lorg/apache/james/mime4j/secfunc/DateTimeField$Parser;-><init>()V

    .line 26
    .local v0, "dateTimeParser":Lorg/apache/james/mime4j/secfunc/DateTimeField$Parser;
    const-string v1, "Date"

    invoke-virtual {p0, v1, v0}, Lorg/apache/james/mime4j/secfunc/DefaultFieldParser;->setFieldParser(Ljava/lang/String;Lorg/apache/james/mime4j/secfunc/FieldParser;)V

    .line 27
    const-string v1, "Resent-Date"

    invoke-virtual {p0, v1, v0}, Lorg/apache/james/mime4j/secfunc/DefaultFieldParser;->setFieldParser(Ljava/lang/String;Lorg/apache/james/mime4j/secfunc/FieldParser;)V

    .line 45
    return-void
.end method
