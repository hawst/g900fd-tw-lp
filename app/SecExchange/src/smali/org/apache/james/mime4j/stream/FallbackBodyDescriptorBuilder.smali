.class Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;
.super Ljava/lang/Object;
.source "FallbackBodyDescriptorBuilder.java"

# interfaces
.implements Lorg/apache/james/mime4j/stream/BodyDescriptorBuilder;


# instance fields
.field private boundary:Ljava/lang/String;

.field private charset:Ljava/lang/String;

.field private contentLength:J

.field private mediaType:Ljava/lang/String;

.field private mimeType:Ljava/lang/String;

.field private final monitor:Lorg/apache/james/mime4j/codec/DecodeMonitor;

.field private final parentMimeType:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private subType:Ljava/lang/String;

.field private transferEncoding:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, v0, v0}, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;-><init>(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)V
    .locals 1
    .param p1, "parentMimeType"    # Ljava/lang/String;
    .param p2, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->protocol:Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->parentMimeType:Ljava/lang/String;

    .line 73
    if-eqz p2, :cond_0

    .end local p2    # "monitor":Lorg/apache/james/mime4j/codec/DecodeMonitor;
    :goto_0
    iput-object p2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->monitor:Lorg/apache/james/mime4j/codec/DecodeMonitor;

    .line 74
    invoke-virtual {p0}, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->reset()V

    .line 75
    return-void

    .line 73
    .restart local p2    # "monitor":Lorg/apache/james/mime4j/codec/DecodeMonitor;
    :cond_0
    sget-object p2, Lorg/apache/james/mime4j/codec/DecodeMonitor;->SILENT:Lorg/apache/james/mime4j/codec/DecodeMonitor;

    goto :goto_0
.end method

.method private parseContentType(Lorg/apache/james/mime4j/stream/Field;)V
    .locals 17
    .param p1, "field"    # Lorg/apache/james/mime4j/stream/Field;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 156
    move-object/from16 v0, p1

    instance-of v15, v0, Lorg/apache/james/mime4j/stream/RawField;

    if-eqz v15, :cond_0

    move-object/from16 v11, p1

    .line 157
    check-cast v11, Lorg/apache/james/mime4j/stream/RawField;

    .line 161
    .local v11, "rawfield":Lorg/apache/james/mime4j/stream/RawField;
    :goto_0
    sget-object v15, Lorg/apache/james/mime4j/stream/RawFieldParser;->DEFAULT:Lorg/apache/james/mime4j/stream/RawFieldParser;

    invoke-virtual {v15, v11}, Lorg/apache/james/mime4j/stream/RawFieldParser;->parseRawBody(Lorg/apache/james/mime4j/stream/RawField;)Lorg/apache/james/mime4j/stream/RawBody;

    move-result-object v2

    .line 162
    .local v2, "body":Lorg/apache/james/mime4j/stream/RawBody;
    invoke-virtual {v2}, Lorg/apache/james/mime4j/stream/RawBody;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 163
    .local v6, "main":Ljava/lang/String;
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 164
    .local v10, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v2}, Lorg/apache/james/mime4j/stream/RawBody;->getParams()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/james/mime4j/stream/NameValuePair;

    .line 165
    .local v8, "nmp":Lorg/apache/james/mime4j/stream/NameValuePair;
    invoke-virtual {v8}, Lorg/apache/james/mime4j/stream/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v15

    sget-object v16, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 166
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v8}, Lorg/apache/james/mime4j/stream/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v7, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 159
    .end local v2    # "body":Lorg/apache/james/mime4j/stream/RawBody;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "main":Ljava/lang/String;
    .end local v7    # "name":Ljava/lang/String;
    .end local v8    # "nmp":Lorg/apache/james/mime4j/stream/NameValuePair;
    .end local v10    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v11    # "rawfield":Lorg/apache/james/mime4j/stream/RawField;
    :cond_0
    new-instance v11, Lorg/apache/james/mime4j/stream/RawField;

    invoke-interface/range {p1 .. p1}, Lorg/apache/james/mime4j/stream/Field;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-interface/range {p1 .. p1}, Lorg/apache/james/mime4j/stream/Field;->getBody()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v11, v15, v0}, Lorg/apache/james/mime4j/stream/RawField;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v11    # "rawfield":Lorg/apache/james/mime4j/stream/RawField;
    goto :goto_0

    .line 169
    .restart local v2    # "body":Lorg/apache/james/mime4j/stream/RawBody;
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v6    # "main":Ljava/lang/String;
    .restart local v10    # "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    const/4 v13, 0x0

    .line 170
    .local v13, "type":Ljava/lang/String;
    const/4 v12, 0x0

    .line 171
    .local v12, "subtype":Ljava/lang/String;
    if-eqz v6, :cond_3

    .line 172
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 173
    const/16 v15, 0x2f

    invoke-virtual {v6, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 174
    .local v5, "index":I
    const/4 v14, 0x0

    .line 175
    .local v14, "valid":Z
    const/4 v15, -0x1

    if-eq v5, v15, :cond_2

    .line 176
    const/4 v15, 0x0

    invoke-virtual {v6, v15, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 177
    add-int/lit8 v15, v5, 0x1

    invoke-virtual {v6, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    .line 178
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_2

    .line 179
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 180
    const/4 v14, 0x1

    .line 184
    :cond_2
    if-nez v14, :cond_3

    .line 185
    const/4 v6, 0x0

    .line 186
    const/4 v13, 0x0

    .line 187
    const/4 v12, 0x0

    .line 190
    .end local v5    # "index":I
    .end local v14    # "valid":Z
    :cond_3
    const-string v15, "boundary"

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 192
    .local v1, "b":Ljava/lang/String;
    if-eqz v6, :cond_6

    const-string v15, "multipart/"

    invoke-virtual {v6, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_4

    if-nez v1, :cond_5

    :cond_4
    const-string v15, "multipart/"

    invoke-virtual {v6, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_6

    .line 195
    :cond_5
    move-object/from16 v0, p0

    iput-object v6, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    .line 196
    move-object/from16 v0, p0

    iput-object v13, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mediaType:Ljava/lang/String;

    .line 197
    move-object/from16 v0, p0

    iput-object v12, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->subType:Ljava/lang/String;

    .line 200
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    invoke-static {v15}, Lorg/apache/james/mime4j/util/MimeUtil;->isMultipart(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 201
    move-object/from16 v0, p0

    iput-object v1, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->boundary:Ljava/lang/String;

    .line 204
    :cond_7
    const-string v15, "charset"

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 205
    .local v3, "c":Ljava/lang/String;
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->charset:Ljava/lang/String;

    .line 206
    if-eqz v3, :cond_8

    .line 207
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 208
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_8

    .line 209
    move-object/from16 v0, p0

    iput-object v3, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->charset:Ljava/lang/String;

    .line 213
    :cond_8
    const-string v15, "protocol"

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 214
    .local v9, "p":Ljava/lang/String;
    if-eqz v9, :cond_9

    .line 215
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 216
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v15

    if-lez v15, :cond_9

    .line 217
    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->protocol:Ljava/lang/String;

    .line 220
    :cond_9
    return-void
.end method


# virtual methods
.method public addField(Lorg/apache/james/mime4j/stream/RawField;)Lorg/apache/james/mime4j/stream/Field;
    .locals 8
    .param p1, "field"    # Lorg/apache/james/mime4j/stream/RawField;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 125
    invoke-virtual {p1}, Lorg/apache/james/mime4j/stream/RawField;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "name":Ljava/lang/String;
    const-string v3, "content-transfer-encoding"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->transferEncoding:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 128
    invoke-virtual {p1}, Lorg/apache/james/mime4j/stream/RawField;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 130
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 132
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->transferEncoding:Ljava/lang/String;

    .line 151
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v3, 0x0

    return-object v3

    .line 135
    :cond_1
    const-string v3, "content-length"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v4, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->contentLength:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 136
    invoke-virtual {p1}, Lorg/apache/james/mime4j/stream/RawField;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 137
    .restart local v2    # "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 138
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 140
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->contentLength:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v3, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->monitor:Lorg/apache/james/mime4j/codec/DecodeMonitor;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid content length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ignoring Content-Length header"

    invoke-virtual {v3, v4, v5}, Lorg/apache/james/mime4j/codec/DecodeMonitor;->warn(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 144
    new-instance v3, Lorg/apache/james/mime4j/MimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid Content-Length header: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 148
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "value":Ljava/lang/String;
    :cond_2
    const-string v3, "content-type"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 149
    invoke-direct {p0, p1}, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->parseContentType(Lorg/apache/james/mime4j/stream/Field;)V

    goto :goto_0
.end method

.method public build()Lorg/apache/james/mime4j/stream/BodyDescriptor;
    .locals 10

    .prologue
    .line 93
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    .line 94
    .local v1, "actualMimeType":Ljava/lang/String;
    iget-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mediaType:Ljava/lang/String;

    .line 95
    .local v2, "actualMediaType":Ljava/lang/String;
    iget-object v3, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->subType:Ljava/lang/String;

    .line 96
    .local v3, "actualSubType":Ljava/lang/String;
    iget-object v5, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->charset:Ljava/lang/String;

    .line 97
    .local v5, "actualCharset":Ljava/lang/String;
    iget-object v6, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->protocol:Ljava/lang/String;

    .line 98
    .local v6, "actualProtocol":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 99
    const-string v0, "multipart/digest"

    iget-object v4, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->parentMimeType:Ljava/lang/String;

    invoke-static {v0, v4}, Lorg/apache/james/mime4j/util/MimeUtil;->isSameMimeType(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    const-string v1, "message/rfc822"

    .line 101
    const-string v2, "message"

    .line 102
    const-string v3, "rfc822"

    .line 109
    :cond_0
    :goto_0
    if-nez v5, :cond_1

    const-string v0, "text"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    const-string v5, "us-ascii"

    .line 112
    :cond_1
    new-instance v0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;

    iget-object v4, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->boundary:Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->transferEncoding:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->transferEncoding:Ljava/lang/String;

    :goto_1
    iget-wide v8, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->contentLength:J

    invoke-direct/range {v0 .. v9}, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-object v0

    .line 104
    :cond_2
    const-string v1, "text/plain"

    .line 105
    const-string v2, "text"

    .line 106
    const-string v3, "plain"

    goto :goto_0

    .line 112
    :cond_3
    const-string v7, "7bit"

    goto :goto_1
.end method

.method public newChild()Lorg/apache/james/mime4j/stream/BodyDescriptorBuilder;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;

    iget-object v1, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->monitor:Lorg/apache/james/mime4j/codec/DecodeMonitor;

    invoke-direct {v0, v1, v2}, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;-><init>(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)V

    return-object v0
.end method

.method public final reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mimeType:Ljava/lang/String;

    .line 79
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->subType:Ljava/lang/String;

    .line 80
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->mediaType:Ljava/lang/String;

    .line 81
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->boundary:Ljava/lang/String;

    .line 82
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->charset:Ljava/lang/String;

    .line 83
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->transferEncoding:Ljava/lang/String;

    .line 84
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->contentLength:J

    .line 85
    iput-object v2, p0, Lorg/apache/james/mime4j/stream/FallbackBodyDescriptorBuilder;->protocol:Ljava/lang/String;

    .line 86
    return-void
.end method
