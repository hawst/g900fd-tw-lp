.class public Lorg/apache/james/mime4j/codec/DecoderUtil;
.super Ljava/lang/Object;
.source "DecoderUtil.java"


# static fields
.field private static final PATTERN_ENCODED_WORD:Ljava/util/regex/Pattern;

.field private static log:Lorg/apache/james/mime4j/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "(.*?)=\\?(.+?)\\?(\\w)\\?(.*?)\\?="

    const/16 v1, 0x20

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/apache/james/mime4j/codec/DecoderUtil;->PATTERN_ENCODED_WORD:Ljava/util/regex/Pattern;

    .line 48
    const-class v0, Lorg/apache/james/mime4j/codec/DecoderUtil;

    invoke-static {v0}, Lorg/apache/james/mime4j/LogFactory;->getLog(Ljava/lang/Class;)Lorg/apache/james/mime4j/Log;

    move-result-object v0

    sput-object v0, Lorg/apache/james/mime4j/codec/DecoderUtil;->log:Lorg/apache/james/mime4j/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static chardet(Ljava/lang/String;)Ljava/lang/String;
    .locals 22
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 144
    .end local p0    # "s":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 65
    .restart local p0    # "s":Ljava/lang/String;
    :cond_0
    :try_start_0
    new-instance v14, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 67
    .local v14, "imp":Ljava/io/ByteArrayOutputStream;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 68
    .local v6, "chars":[C
    move-object v3, v6

    .local v3, "arr$":[C
    array-length v15, v3

    .local v15, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v15, :cond_1

    aget-char v5, v3, v13

    .line 69
    .local v5, "c":C
    invoke-virtual {v14, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 68
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 71
    .end local v5    # "c":C
    :cond_1
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 72
    .local v4, "buf":[B
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 74
    const/4 v6, 0x0

    .line 76
    array-length v0, v4

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-static {v4, v0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->isAscii([BI)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 77
    const/16 p0, 0x0

    goto :goto_0

    .line 79
    :cond_2
    const-string v19, "<"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_4

    const-string v19, "@"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_4

    const-string v19, ">"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 81
    const/16 v19, 0x0

    const-string v20, "<"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    const-string v20, "\""

    const-string v21, " "

    invoke-virtual/range {v19 .. v21}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    .line 82
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 83
    move-object v3, v6

    array-length v15, v3

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v15, :cond_3

    aget-char v5, v3, v13

    .line 84
    .restart local v5    # "c":C
    invoke-virtual {v14, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 83
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 85
    .end local v5    # "c":C
    :cond_3
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 86
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 87
    const/4 v6, 0x0

    .line 89
    :cond_4
    invoke-virtual {v14}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 91
    new-instance v9, Lorg/mozilla/universalchardet/UniversalDetector;

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v9, v0}, Lorg/mozilla/universalchardet/UniversalDetector;-><init>(Lorg/mozilla/universalchardet/CharsetListener;)V

    .line 92
    .local v9, "detector":Lorg/mozilla/universalchardet/UniversalDetector;
    const/16 v16, 0x32

    .line 94
    .local v16, "nRefLength":I
    array-length v0, v4

    move/from16 v19, v0

    const/16 v20, 0x32

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_b

    .line 95
    const/16 v17, 0x0

    .line 96
    .local v17, "nTotal":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_3
    const/16 v19, 0x32

    move/from16 v0, v19

    if-ge v12, v0, :cond_5

    .line 97
    const/16 v19, 0x0

    array-length v0, v4

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v4, v0, v1}, Lorg/mozilla/universalchardet/UniversalDetector;->handleData([BII)V

    .line 98
    array-length v0, v4

    move/from16 v19, v0

    add-int v17, v17, v19

    .line 99
    const/16 v19, 0x32

    move/from16 v0, v17

    move/from16 v1, v19

    if-gt v0, v1, :cond_5

    invoke-virtual {v9}, Lorg/mozilla/universalchardet/UniversalDetector;->isDone()Z

    move-result v19

    if-eqz v19, :cond_a

    .line 104
    .end local v12    # "i":I
    .end local v17    # "nTotal":I
    :cond_5
    :goto_4
    invoke-virtual {v9}, Lorg/mozilla/universalchardet/UniversalDetector;->dataEnd()V

    .line 107
    invoke-virtual {v9}, Lorg/mozilla/universalchardet/UniversalDetector;->getDetectedCharset()Ljava/lang/String;

    move-result-object v10

    .line 109
    .local v10, "encoding":Ljava/lang/String;
    if-eqz v10, :cond_c

    .line 110
    sget-object v19, Lorg/apache/james/mime4j/codec/DecoderUtil;->log:Lorg/apache/james/mime4j/Log;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Detected encoding = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lorg/apache/james/mime4j/Log;->debug(Ljava/lang/Object;)V

    .line 111
    sget-object v19, Lorg/mozilla/universalchardet/Constants;->CHARSET_UTF_16BE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    sget-object v19, Lorg/mozilla/universalchardet/Constants;->CHARSET_UTF_16LE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    sget-object v19, Lorg/mozilla/universalchardet/Constants;->CHARSET_UTF_32BE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_6

    sget-object v19, Lorg/mozilla/universalchardet/Constants;->CHARSET_UTF_32LE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 115
    :cond_6
    const/4 v10, 0x0

    .line 120
    :cond_7
    :goto_5
    invoke-static {}, Lorg/apache/james/mime4j/util/CharsetUtil;->getLocalCharset()Ljava/lang/String;

    move-result-object v8

    .line 122
    .local v8, "defaultCharset":Ljava/lang/String;
    if-eqz v8, :cond_8

    const-string v19, "ASCII"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v19

    if-nez v19, :cond_8

    .line 124
    :try_start_1
    invoke-virtual {v9}, Lorg/mozilla/universalchardet/UniversalDetector;->getDetectedAllCharset()Ljava/lang/String;

    move-result-object v11

    .line 125
    .local v11, "getDetectedAllCharset":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 126
    .local v18, "tokens1":[Ljava/lang/String;
    move-object/from16 v3, v18

    .local v3, "arr$":[Ljava/lang/String;
    array-length v15, v3

    const/4 v13, 0x0

    :goto_6
    if-ge v13, v15, :cond_8

    aget-object v7, v3, v13

    .line 127
    .local v7, "charset":Ljava/lang/String;
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v19

    if-eqz v19, :cond_d

    .line 128
    move-object v10, v8

    .line 135
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v7    # "charset":Ljava/lang/String;
    .end local v11    # "getDetectedAllCharset":Ljava/lang/String;
    .end local v18    # "tokens1":[Ljava/lang/String;
    :cond_8
    :goto_7
    if-nez v10, :cond_9

    .line 136
    move-object v10, v8

    .line 139
    :cond_9
    :try_start_2
    invoke-virtual {v9}, Lorg/mozilla/universalchardet/UniversalDetector;->reset()V

    .line 140
    const/4 v9, 0x0

    move-object/from16 p0, v10

    .line 141
    goto/16 :goto_0

    .line 96
    .end local v8    # "defaultCharset":Ljava/lang/String;
    .end local v10    # "encoding":Ljava/lang/String;
    .local v3, "arr$":[C
    .restart local v12    # "i":I
    .restart local v17    # "nTotal":I
    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3

    .line 103
    .end local v12    # "i":I
    .end local v17    # "nTotal":I
    :cond_b
    const/16 v19, 0x0

    array-length v0, v4

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v4, v0, v1}, Lorg/mozilla/universalchardet/UniversalDetector;->handleData([BII)V

    goto/16 :goto_4

    .line 142
    .end local v3    # "arr$":[C
    .end local v4    # "buf":[B
    .end local v6    # "chars":[C
    .end local v9    # "detector":Lorg/mozilla/universalchardet/UniversalDetector;
    .end local v13    # "i$":I
    .end local v14    # "imp":Ljava/io/ByteArrayOutputStream;
    .end local v15    # "len$":I
    .end local v16    # "nRefLength":I
    :catch_0
    move-exception v19

    .line 144
    const/16 p0, 0x0

    goto/16 :goto_0

    .line 117
    .restart local v3    # "arr$":[C
    .restart local v4    # "buf":[B
    .restart local v6    # "chars":[C
    .restart local v9    # "detector":Lorg/mozilla/universalchardet/UniversalDetector;
    .restart local v10    # "encoding":Ljava/lang/String;
    .restart local v13    # "i$":I
    .restart local v14    # "imp":Ljava/io/ByteArrayOutputStream;
    .restart local v15    # "len$":I
    .restart local v16    # "nRefLength":I
    :cond_c
    sget-object v19, Lorg/apache/james/mime4j/codec/DecoderUtil;->log:Lorg/apache/james/mime4j/Log;

    const-string v20, "No encoding detected."

    invoke-virtual/range {v19 .. v20}, Lorg/apache/james/mime4j/Log;->debug(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 126
    .local v3, "arr$":[Ljava/lang/String;
    .restart local v7    # "charset":Ljava/lang/String;
    .restart local v8    # "defaultCharset":Ljava/lang/String;
    .restart local v11    # "getDetectedAllCharset":Ljava/lang/String;
    .restart local v18    # "tokens1":[Ljava/lang/String;
    :cond_d
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 132
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v7    # "charset":Ljava/lang/String;
    .end local v11    # "getDetectedAllCharset":Ljava/lang/String;
    .end local v18    # "tokens1":[Ljava/lang/String;
    :catch_1
    move-exception v19

    goto :goto_7
.end method

.method public static charsetDetect([BI)Ljava/lang/String;
    .locals 3
    .param p0, "data"    # [B
    .param p1, "size"    # I

    .prologue
    .line 521
    new-instance v1, Lorg/mozilla/universalchardet/UniversalDetector;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/mozilla/universalchardet/UniversalDetector;-><init>(Lorg/mozilla/universalchardet/CharsetListener;)V

    .line 523
    .local v1, "detector":Lorg/mozilla/universalchardet/UniversalDetector;
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, p1}, Lorg/mozilla/universalchardet/UniversalDetector;->handleData([BII)V

    .line 524
    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->dataEnd()V

    .line 525
    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->getDetectedCharset()Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "charset":Ljava/lang/String;
    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->reset()V

    .line 528
    return-object v0
.end method

.method static decodeB(Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    .locals 1
    .param p0, "encodedText"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/lang/String;
    .param p2, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 322
    invoke-static {p0, p2}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeBase64(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B

    move-result-object v0

    return-object v0
.end method

.method private static decodeBase64(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    .locals 8
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;

    .prologue
    .line 280
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 281
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 283
    .local v4, "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    :try_start_0
    const-string v6, "US-ASCII"

    invoke-virtual {p0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 285
    .local v2, "bytes":[B
    new-instance v5, Lorg/apache/james/mime4j/codec/Base64InputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v5, v6, p1}, Lorg/apache/james/mime4j/codec/Base64InputStream;-><init>(Ljava/io/InputStream;Lorg/apache/james/mime4j/codec/DecodeMonitor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .local v5, "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    const/4 v0, 0x0

    .line 288
    .local v0, "b":I
    :goto_0
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/james/mime4j/codec/Base64InputStream;->read()I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_1

    .line 289
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 291
    :catch_0
    move-exception v3

    move-object v4, v5

    .line 294
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    :goto_1
    :try_start_2
    sget-object v6, Lorg/apache/james/mime4j/codec/DecoderUtil;->log:Lorg/apache/james/mime4j/Log;

    invoke-virtual {v6, v3}, Lorg/apache/james/mime4j/Log;->error(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 297
    if-eqz v4, :cond_0

    .line 298
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/james/mime4j/codec/Base64InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 304
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 297
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v0    # "b":I
    .restart local v2    # "bytes":[B
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    :cond_1
    if-eqz v5, :cond_2

    .line 298
    :try_start_4
    invoke-virtual {v5}, Lorg/apache/james/mime4j/codec/Base64InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v4, v5

    .line 301
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    goto :goto_2

    .line 299
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    :catch_1
    move-exception v6

    move-object v4, v5

    .line 302
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    goto :goto_2

    .line 296
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    :catchall_0
    move-exception v6

    .line 297
    :goto_3
    if-eqz v4, :cond_3

    .line 298
    :try_start_5
    invoke-virtual {v4}, Lorg/apache/james/mime4j/codec/Base64InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 301
    :cond_3
    :goto_4
    throw v6

    .line 299
    .restart local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    goto :goto_2

    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    goto :goto_4

    .line 296
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v0    # "b":I
    .restart local v2    # "bytes":[B
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/Base64InputStream;
    goto :goto_3

    .line 291
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    :catch_4
    move-exception v3

    goto :goto_1
.end method

.method public static decodeEncodedWords(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "body"    # Ljava/lang/String;

    .prologue
    .line 347
    :try_start_0
    sget-object v1, Lorg/apache/james/mime4j/codec/DecodeMonitor;->SILENT:Lorg/apache/james/mime4j/codec/DecodeMonitor;

    invoke-static {p0, v1}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeEncodedWords(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    .line 349
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-object p0

    .line 348
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 349
    .restart local v0    # "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static decodeEncodedWords(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)Ljava/lang/String;
    .locals 19
    .param p0, "body"    # Ljava/lang/String;
    .param p1, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 365
    const/16 v16, 0x0

    .line 366
    .local v16, "tailIndex":I
    const/4 v10, 0x0

    .line 368
    .local v10, "lastMatchValid":Z
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_1

    .line 437
    .end local p0    # "body":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 370
    .restart local p0    # "body":Ljava/lang/String;
    :cond_1
    const-string v3, "ASCII"

    .line 371
    .local v3, "charset":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v14

    .line 373
    .local v14, "outData":Ljava/nio/CharBuffer;
    sget-object v17, Lorg/apache/james/mime4j/codec/DecoderUtil;->PATTERN_ENCODED_WORD:Ljava/util/regex/Pattern;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .local v11, "matcher":Ljava/util/regex/Matcher;
    :goto_1
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 374
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v15

    .line 375
    .local v15, "separator":Ljava/lang/String;
    const/16 v17, 0x2

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 376
    .local v12, "mimeCharset":Ljava/lang/String;
    const/16 v17, 0x3

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 377
    .local v9, "encoding":Ljava/lang/String;
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    .line 386
    .local v8, "encodedText":Ljava/lang/String;
    const/4 v4, 0x0

    .line 388
    .local v4, "csCharset":Ljava/nio/charset/Charset;
    :try_start_0
    invoke-static {v12}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 389
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Ljava/lang/String;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 390
    :cond_2
    invoke-static {}, Lorg/apache/james/mime4j/util/CharsetUtil;->getLocalCharset()Ljava/lang/String;

    move-result-object v12

    .line 391
    :cond_3
    invoke-static {v12}, Lorg/apache/james/mime4j/util/CharsetUtil;->lookup(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    .line 392
    if-nez v4, :cond_4

    .line 393
    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v4

    .line 396
    :cond_4
    :goto_2
    const/4 v6, 0x0

    .line 398
    .local v6, "decoded":[B
    if-eqz v12, :cond_6

    if-eqz v8, :cond_6

    :try_start_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_6

    const-string v17, "Q"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_5

    const-string v17, "B"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 400
    :cond_5
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/nio/CharBuffer;->put(Ljava/lang/String;)Ljava/nio/CharBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .line 420
    :goto_3
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->end()I

    move-result v16

    .line 421
    if-eqz v6, :cond_a

    const/4 v10, 0x1

    .line 422
    :goto_4
    goto :goto_1

    .line 402
    :cond_6
    :try_start_2
    invoke-virtual {v4}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v5

    .line 403
    .local v5, "csDecoder":Ljava/nio/charset/CharsetDecoder;
    move-object/from16 v0, p1

    invoke-static {v12, v9, v8, v0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->tryDecodeEncodedWord(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v6

    .line 405
    if-nez v6, :cond_7

    .line 406
    const/16 v17, 0x0

    :try_start_3
    move/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/nio/CharBuffer;->put(Ljava/lang/String;)Ljava/nio/CharBuffer;

    move-result-object v14

    goto :goto_3

    .line 408
    :cond_7
    if-eqz v10, :cond_8

    invoke-static {v15}, Lorg/apache/james/mime4j/util/CharsetUtil;->isWhitespace(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_9

    .line 409
    :cond_8
    invoke-virtual {v14, v15}, Ljava/nio/CharBuffer;->put(Ljava/lang/String;)Ljava/nio/CharBuffer;

    move-result-object v14

    .line 411
    :cond_9
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v2

    .line 412
    .local v2, "cb":Ljava/nio/CharBuffer;
    invoke-virtual {v14, v2}, Ljava/nio/CharBuffer;->put(Ljava/nio/CharBuffer;)Ljava/nio/CharBuffer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v14

    goto :goto_3

    .line 421
    .end local v2    # "cb":Ljava/nio/CharBuffer;
    .end local v5    # "csDecoder":Ljava/nio/charset/CharsetDecoder;
    :cond_a
    const/4 v10, 0x0

    goto :goto_4

    .line 424
    .end local v4    # "csCharset":Ljava/nio/charset/Charset;
    .end local v6    # "decoded":[B
    .end local v8    # "encodedText":Ljava/lang/String;
    .end local v9    # "encoding":Ljava/lang/String;
    .end local v12    # "mimeCharset":Ljava/lang/String;
    .end local v15    # "separator":Ljava/lang/String;
    :cond_b
    if-eqz v16, :cond_0

    .line 429
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/nio/CharBuffer;->put(Ljava/lang/String;)Ljava/nio/CharBuffer;

    move-result-object v14

    .line 430
    const-string v13, ""

    .line 432
    .local v13, "out":Ljava/lang/String;
    :try_start_4
    invoke-virtual {v14}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    .line 433
    invoke-virtual {v14}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result-object v13

    :goto_5
    move-object/from16 p0, v13

    .line 437
    goto/16 :goto_0

    .line 434
    :catch_0
    move-exception v7

    .line 435
    .local v7, "e":Ljava/lang/Exception;
    move-object/from16 v13, p0

    goto :goto_5

    .line 417
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v13    # "out":Ljava/lang/String;
    .restart local v4    # "csCharset":Ljava/nio/charset/Charset;
    .restart local v6    # "decoded":[B
    .restart local v8    # "encodedText":Ljava/lang/String;
    .restart local v9    # "encoding":Ljava/lang/String;
    .restart local v12    # "mimeCharset":Ljava/lang/String;
    .restart local v15    # "separator":Ljava/lang/String;
    :catch_1
    move-exception v17

    goto :goto_3

    .line 414
    .restart local v5    # "csDecoder":Ljava/nio/charset/CharsetDecoder;
    :catch_2
    move-exception v17

    goto :goto_3

    .line 394
    .end local v5    # "csDecoder":Ljava/nio/charset/CharsetDecoder;
    .end local v6    # "decoded":[B
    :catch_3
    move-exception v17

    goto/16 :goto_2
.end method

.method public static decodeGeneric(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 220
    if-nez p0, :cond_0

    .line 235
    .end local p0    # "s":Ljava/lang/String;
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 224
    .end local v1    # "result":Ljava/lang/String;
    .restart local p0    # "s":Ljava/lang/String;
    :cond_0
    const-string v2, "=?"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 226
    invoke-static {p0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->chardet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "charset":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 228
    move-object v1, p0

    .end local v0    # "charset":Ljava/lang/String;
    .restart local v1    # "result":Ljava/lang/String;
    :goto_1
    move-object p0, v1

    .line 235
    goto :goto_0

    .line 230
    .end local v1    # "result":Ljava/lang/String;
    .restart local v0    # "charset":Ljava/lang/String;
    :cond_1
    invoke-static {p0, v0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->justDecode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_1

    .line 232
    .end local v0    # "charset":Ljava/lang/String;
    .end local v1    # "result":Ljava/lang/String;
    :cond_2
    invoke-static {p0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeEncodedWords(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_1
.end method

.method static decodeQ(Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    .locals 1
    .param p0, "encodedText"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/lang/String;
    .param p2, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 337
    invoke-static {p0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->replaceUnderscores(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 341
    invoke-static {p0, p2}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeQuotedPrintable(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B

    move-result-object v0

    return-object v0
.end method

.method private static decodeQuotedPrintable(Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    .locals 8
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;

    .prologue
    .line 245
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 246
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 248
    .local v4, "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    :try_start_0
    const-string v6, "US-ASCII"

    invoke-virtual {p0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 250
    .local v2, "bytes":[B
    new-instance v5, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v5, v6, p1}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;-><init>(Ljava/io/InputStream;Lorg/apache/james/mime4j/codec/DecodeMonitor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .local v5, "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    const/4 v0, 0x0

    .line 253
    .local v0, "b":I
    :goto_0
    :try_start_1
    invoke-virtual {v5}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;->read()I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_1

    .line 254
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 256
    :catch_0
    move-exception v3

    move-object v4, v5

    .line 259
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .local v3, "e":Ljava/io/IOException;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    :goto_1
    :try_start_2
    sget-object v6, Lorg/apache/james/mime4j/codec/DecoderUtil;->log:Lorg/apache/james/mime4j/Log;

    invoke-virtual {v6, v3}, Lorg/apache/james/mime4j/Log;->error(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 262
    if-eqz v4, :cond_0

    .line 263
    :try_start_3
    invoke-virtual {v4}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 269
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    return-object v6

    .line 262
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v0    # "b":I
    .restart local v2    # "bytes":[B
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    :cond_1
    if-eqz v5, :cond_2

    .line 263
    :try_start_4
    invoke-virtual {v5}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v4, v5

    .line 266
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    goto :goto_2

    .line 264
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    :catch_1
    move-exception v6

    move-object v4, v5

    .line 267
    .end local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    goto :goto_2

    .line 261
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    :catchall_0
    move-exception v6

    .line 262
    :goto_3
    if-eqz v4, :cond_3

    .line 263
    :try_start_5
    invoke-virtual {v4}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 266
    :cond_3
    :goto_4
    throw v6

    .line 264
    .restart local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    goto :goto_2

    .end local v3    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    goto :goto_4

    .line 261
    .end local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v0    # "b":I
    .restart local v2    # "bytes":[B
    .restart local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    .restart local v4    # "is":Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;
    goto :goto_3

    .line 256
    .end local v0    # "b":I
    .end local v2    # "bytes":[B
    :catch_4
    move-exception v3

    goto :goto_1
.end method

.method protected static isAscii([BI)Z
    .locals 2
    .param p0, "aBuf"    # [B
    .param p1, "aLen"    # I

    .prologue
    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 52
    aget-byte v1, p0, v0

    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_0

    .line 53
    const/4 v1, 0x0

    .line 56
    :goto_1
    return v1

    .line 51
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public static justDecode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/lang/String;

    .prologue
    .line 186
    if-eqz p0, :cond_0

    const-string v8, "=?"

    invoke-virtual {p0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_1

    :cond_0
    move-object v7, p0

    .line 204
    :goto_0
    return-object v7

    .line 190
    :cond_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 194
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    .line 196
    .local v3, "chars":[C
    move-object v0, v3

    .local v0, "arr$":[C
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_2

    aget-char v2, v0, v5

    .line 197
    .local v2, "c":C
    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 196
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 199
    .end local v2    # "c":C
    :cond_2
    new-instance v7, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .local v7, "result":Ljava/lang/String;
    goto :goto_0

    .line 201
    .end local v0    # "arr$":[C
    .end local v3    # "chars":[C
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "result":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 202
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    move-object v7, p0

    .restart local v7    # "result":Ljava/lang/String;
    goto :goto_0
.end method

.method private static varargs monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9
    .param p0, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;
    .param p1, "mimeCharset"    # Ljava/lang/String;
    .param p2, "encoding"    # Ljava/lang/String;
    .param p3, "encodedText"    # Ljava/lang/String;
    .param p4, "dropDesc"    # Ljava/lang/String;
    .param p5, "strings"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 481
    invoke-virtual {p0}, Lorg/apache/james/mime4j/codec/DecodeMonitor;->isListening()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 482
    invoke-static {p1, p2, p3}, Lorg/apache/james/mime4j/codec/DecoderUtil;->recombine(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "encodedWord":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 484
    .local v6, "text":Ljava/lang/StringBuilder;
    move-object v0, p5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 485
    .local v5, "str":Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 487
    .end local v5    # "str":Ljava/lang/String;
    :cond_0
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 491
    .local v2, "exceptionDesc":Ljava/lang/String;
    invoke-virtual {p0, v2, p4}, Lorg/apache/james/mime4j/codec/DecodeMonitor;->warn(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 492
    new-instance v7, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 494
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "encodedWord":Ljava/lang/String;
    .end local v2    # "exceptionDesc":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "text":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method

.method private static recombine(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "mimeCharset"    # Ljava/lang/String;
    .param p1, "encoding"    # Ljava/lang/String;
    .param p2, "encodedText"    # Ljava/lang/String;

    .prologue
    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static replaceUnderscores(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 505
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 507
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 508
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 509
    .local v0, "c":C
    const/16 v3, 0x5f

    if-ne v0, v3, :cond_0

    .line 510
    const-string v3, "=20"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 512
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 516
    .end local v0    # "c":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static tryDecodeEncodedWord(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B
    .locals 8
    .param p0, "mimeCharset"    # Ljava/lang/String;
    .param p1, "encoding"    # Ljava/lang/String;
    .param p2, "encodedText"    # Ljava/lang/String;
    .param p3, "monitor"    # Lorg/apache/james/mime4j/codec/DecodeMonitor;

    .prologue
    .line 444
    invoke-static {p0}, Lorg/apache/james/mime4j/util/CharsetUtil;->lookup(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v6

    .line 445
    .local v6, "charset":Ljava/nio/charset/Charset;
    if-nez v6, :cond_0

    .line 446
    const-string v4, "leaving word encoded"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Mime charser \'"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    aput-object p0, v5, v0

    const/4 v0, 0x2

    const-string v1, "\' doesn\'t have a corresponding Java charset"

    aput-object v1, v5, v0

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/james/mime4j/codec/DecoderUtil;->monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 448
    const/4 v0, 0x0

    .line 475
    :goto_0
    return-object v0

    .line 451
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 452
    const-string v4, "leaving word encoded"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Missing encoded text in encoded word"

    aput-object v1, v5, v0

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/james/mime4j/codec/DecoderUtil;->monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 454
    const/4 v0, 0x0

    goto :goto_0

    .line 458
    :cond_1
    :try_start_0
    const-string v0, "Q"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 459
    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0, p3}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeQ(Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B

    move-result-object v0

    goto :goto_0

    .line 460
    :cond_2
    const-string v0, "B"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 461
    invoke-virtual {v6}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0, p3}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeB(Ljava/lang/String;Ljava/lang/String;Lorg/apache/james/mime4j/codec/DecodeMonitor;)[B

    move-result-object v0

    goto :goto_0

    .line 463
    :cond_3
    const-string v4, "leaving word encoded"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Warning: Unknown encoding in encoded word"

    aput-object v1, v5, v0

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/james/mime4j/codec/DecoderUtil;->monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 465
    const/4 v0, 0x0

    goto :goto_0

    .line 467
    :catch_0
    move-exception v7

    .line 469
    .local v7, "e":Ljava/io/UnsupportedEncodingException;
    const-string v4, "leaving word encoded"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Unsupported encoding ("

    aput-object v1, v5, v0

    const/4 v0, 0x1

    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    const-string v1, ") in encoded word"

    aput-object v1, v5, v0

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/james/mime4j/codec/DecoderUtil;->monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 471
    const/4 v0, 0x0

    goto :goto_0

    .line 472
    .end local v7    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v7

    .line 473
    .local v7, "e":Ljava/lang/RuntimeException;
    const-string v4, "leaving word encoded"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "Could not decode ("

    aput-object v1, v5, v0

    const/4 v0, 0x1

    invoke-virtual {v7}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    const/4 v0, 0x2

    const-string v1, ") encoded word"

    aput-object v1, v5, v0

    move-object v0, p3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lorg/apache/james/mime4j/codec/DecoderUtil;->monitor(Lorg/apache/james/mime4j/codec/DecodeMonitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 475
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
