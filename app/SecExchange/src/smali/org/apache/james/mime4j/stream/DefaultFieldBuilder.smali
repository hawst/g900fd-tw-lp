.class public Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;
.super Ljava/lang/Object;
.source "DefaultFieldBuilder.java"

# interfaces
.implements Lorg/apache/james/mime4j/stream/FieldBuilder;


# static fields
.field private static final FIELD_CHARS:Ljava/util/BitSet;


# instance fields
.field private final buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

.field private final maxlen:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1}, Ljava/util/BitSet;-><init>()V

    sput-object v1, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->FIELD_CHARS:Ljava/util/BitSet;

    .line 36
    const/16 v0, 0x21

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    .line 37
    sget-object v1, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->FIELD_CHARS:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_0
    const/16 v0, 0x3b

    :goto_1
    const/16 v1, 0x7e

    if-gt v0, v1, :cond_1

    .line 40
    sget-object v1, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->FIELD_CHARS:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_1
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "maxlen"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    .line 49
    iput p1, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->maxlen:I

    .line 50
    return-void
.end method


# virtual methods
.method public append(Lorg/apache/james/mime4j/util/ByteArrayBuffer;)V
    .locals 4
    .param p1, "line"    # Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    .prologue
    .line 57
    if-nez p1, :cond_0

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    invoke-virtual {p1}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->buffer()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->append([BII)V

    goto :goto_0
.end method

.method public build()Lorg/apache/james/mime4j/stream/RawField;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v6, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    invoke-virtual {v6}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->length()I

    move-result v4

    .line 75
    .local v4, "len":I
    if-lez v4, :cond_1

    .line 76
    iget-object v6, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    add-int/lit8 v7, v4, -0x1

    invoke-virtual {v6, v7}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->byteAt(I)B

    move-result v6

    const/16 v7, 0xa

    if-ne v6, v7, :cond_0

    .line 77
    add-int/lit8 v4, v4, -0x1

    .line 79
    :cond_0
    iget-object v6, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    add-int/lit8 v7, v4, -0x1

    invoke-virtual {v6, v7}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->byteAt(I)B

    move-result v6

    const/16 v7, 0xd

    if-ne v6, v7, :cond_1

    .line 80
    add-int/lit8 v4, v4, -0x1

    .line 83
    :cond_1
    new-instance v1, Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    iget-object v6, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    invoke-virtual {v6}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->buffer()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v1, v6, v4, v7}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;-><init>([BIZ)V

    .line 84
    .local v1, "copy":Lorg/apache/james/mime4j/util/ByteArrayBuffer;
    sget-object v6, Lorg/apache/james/mime4j/stream/RawFieldParser;->DEFAULT:Lorg/apache/james/mime4j/stream/RawFieldParser;

    invoke-virtual {v6, v1}, Lorg/apache/james/mime4j/stream/RawFieldParser;->parseField(Lorg/apache/james/mime4j/util/ByteSequence;)Lorg/apache/james/mime4j/stream/RawField;

    move-result-object v2

    .line 85
    .local v2, "field":Lorg/apache/james/mime4j/stream/RawField;
    invoke-virtual {v2}, Lorg/apache/james/mime4j/stream/RawField;->getName()Ljava/lang/String;

    move-result-object v5

    .line 86
    .local v5, "name":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v3, v6, :cond_3

    .line 87
    invoke-virtual {v5, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 88
    .local v0, "ch":C
    sget-object v6, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->FIELD_CHARS:Ljava/util/BitSet;

    invoke-virtual {v6, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v6

    if-nez v6, :cond_2

    .line 89
    new-instance v6, Lorg/apache/james/mime4j/MimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIME field name contains illegal characters: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lorg/apache/james/mime4j/stream/RawField;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 86
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "ch":C
    :cond_3
    return-object v2
.end method

.method public getRaw()Lorg/apache/james/mime4j/util/ByteArrayBuffer;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/DefaultFieldBuilder;->buf:Lorg/apache/james/mime4j/util/ByteArrayBuffer;

    invoke-virtual {v0}, Lorg/apache/james/mime4j/util/ByteArrayBuffer;->clear()V

    .line 54
    return-void
.end method
