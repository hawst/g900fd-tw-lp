.class Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;
.super Ljava/lang/Object;
.source "BasicBodyDescriptor.java"

# interfaces
.implements Lorg/apache/james/mime4j/stream/BodyDescriptor;


# instance fields
.field private final boundary:Ljava/lang/String;

.field private final charset:Ljava/lang/String;

.field private final contentLength:J

.field private final mediaType:Ljava/lang/String;

.field private final mimeType:Ljava/lang/String;

.field private final protocol:Ljava/lang/String;

.field private final subType:Ljava/lang/String;

.field private final transferEncoding:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "mediaType"    # Ljava/lang/String;
    .param p3, "subType"    # Ljava/lang/String;
    .param p4, "boundary"    # Ljava/lang/String;
    .param p5, "charset"    # Ljava/lang/String;
    .param p6, "protocol"    # Ljava/lang/String;
    .param p7, "transferEncoding"    # Ljava/lang/String;
    .param p8, "contentLength"    # J

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->mimeType:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->mediaType:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->subType:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->boundary:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->charset:Ljava/lang/String;

    .line 48
    iput-object p6, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->protocol:Ljava/lang/String;

    .line 49
    iput-object p7, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->transferEncoding:Ljava/lang/String;

    .line 50
    iput-wide p8, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->contentLength:J

    .line 51
    return-void
.end method


# virtual methods
.method public getBoundary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->boundary:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getTransferEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->transferEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "[mimeType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, ", mediaType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->mediaType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    const-string v1, ", subType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->subType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v1, ", boundary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->boundary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    const-string v1, ", charset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->charset:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const-string v1, ", protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    iget-object v1, p0, Lorg/apache/james/mime4j/stream/BasicBodyDescriptor;->protocol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
