.class public Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;
.super Ljava/lang/Object;
.source "DateTimeParser.java"

# interfaces
.implements Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserConstants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;,
        Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;
    }
.end annotation


# static fields
.field private static jj_la1_0:[I

.field private static jj_la1_1:[I


# instance fields
.field private jj_expentries:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[I>;"
        }
    .end annotation
.end field

.field private jj_expentry:[I

.field private jj_gen:I

.field jj_input_stream:Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;

.field private jj_kind:I

.field private final jj_la1:[I

.field public jj_nt:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

.field private jj_ntk:I

.field public token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

.field public token_source:Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 577
    invoke-static {}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_0()V

    .line 578
    invoke-static {}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_1()V

    .line 579
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/Reader;

    .prologue
    const/4 v4, 0x7

    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 573
    new-array v1, v4, [I

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    .line 715
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    .line 717
    iput v3, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_kind:I

    .line 630
    new-instance v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;

    invoke-direct {v1, p1, v2, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;-><init>(Ljava/io/Reader;II)V

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_input_stream:Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;

    .line 631
    new-instance v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;

    iget-object v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_input_stream:Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;

    invoke-direct {v1, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;-><init>(Lorg/apache/james/mime4j/secfunc/datetime/parser/SimpleCharStream;)V

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token_source:Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;

    .line 632
    new-instance v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    invoke-direct {v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;-><init>()V

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 633
    iput v3, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    .line 634
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    .line 635
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 636
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    aput v3, v1, v0

    .line 635
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637
    :cond_0
    return-void
.end method

.method private static getMilitaryZoneOffset(C)I
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method private final jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    .locals 3
    .param p1, "kind"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 669
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .local v0, "oldToken":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    iget-object v1, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->next:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    if-eqz v1, :cond_0

    .line 670
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget-object v1, v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->next:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iput-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 673
    :goto_0
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    .line 674
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget v1, v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->kind:I

    if-ne v1, p1, :cond_1

    .line 675
    iget v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    .line 676
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 680
    :goto_1
    return-object v1

    .line 672
    :cond_0
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget-object v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token_source:Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;

    invoke-virtual {v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;->getNextToken()Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v2

    iput-object v2, v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->next:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iput-object v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_0

    .line 678
    :cond_1
    const/16 v1, 0x30

    if-ne p1, v1, :cond_2

    .line 679
    iget v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    .line 680
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 682
    :cond_2
    iput-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 683
    iput p1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_kind:I

    .line 684
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->generateParseException()Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    move-result-object v1

    throw v1
.end method

.method private static jj_la1_0()V
    .locals 1

    .prologue
    .line 582
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_0:[I

    .line 585
    return-void

    .line 582
    nop

    :array_0
    .array-data 4
        0x2
        0x7f0
        0x7f0
        0x7ff800
        0x800000
        -0x1000000
        -0x2000000
    .end array-data
.end method

.method private static jj_la1_1()V
    .locals 1

    .prologue
    .line 588
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_1:[I

    .line 591
    return-void

    .line 588
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0xf
        0xf
    .end array-data
.end method

.method private final jj_ntk()I
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget-object v0, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->next:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iput-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_nt:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    if-nez v0, :cond_0

    .line 710
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token_source:Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;

    invoke-virtual {v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParserTokenManager;->getNextToken()Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->next:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget v0, v1, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    .line 712
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_nt:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget v0, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->kind:I

    iput v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0
.end method

.method private static parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I
    .locals 2
    .param p0, "token"    # Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .prologue
    .line 40
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->image:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final date()Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 298
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->day()I

    move-result v0

    .line 299
    .local v0, "d":I
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->month()I

    move-result v1

    .line 300
    .local v1, "m":I
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->year()Ljava/lang/String;

    move-result-object v2

    .line 303
    .local v2, "y":Ljava/lang/String;
    new-instance v3, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;

    invoke-direct {v3, v2, v1, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;-><init>(Ljava/lang/String;II)V

    return-object v3
.end method

.method public final date_time()Lorg/apache/james/mime4j/secfunc/datetime/DateTime;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 219
    iget v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v0

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 246
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    const/4 v1, 0x1

    iget v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v2, v0, v1

    .line 249
    :goto_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->date()Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;

    move-result-object v8

    .line 250
    .local v8, "d":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->time()Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;

    move-result-object v9

    .line 253
    .local v9, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;
    new-instance v0, Lorg/apache/james/mime4j/secfunc/datetime/DateTime;

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getMonth()I

    move-result v2

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getDay()I

    move-result v3

    invoke-virtual {v9}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getHour()I

    move-result v4

    invoke-virtual {v9}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getMinute()I

    move-result v5

    invoke-virtual {v9}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getSecond()I

    move-result v6

    invoke-virtual {v9}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getZone()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lorg/apache/james/mime4j/secfunc/datetime/DateTime;-><init>(Ljava/lang/String;IIIIII)V

    .end local v8    # "d":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;
    .end local v9    # "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;
    :goto_2
    return-object v0

    .line 219
    :cond_0
    iget v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 227
    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->day_of_week()Ljava/lang/String;

    .line 228
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 243
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->date_time2()Lorg/apache/james/mime4j/secfunc/datetime/DateTime;

    move-result-object v0

    goto :goto_2

    .line 219
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final date_time2()Lorg/apache/james/mime4j/secfunc/datetime/DateTime;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x30

    .line 197
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->month()I

    move-result v10

    .line 198
    .local v10, "m":I
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 199
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->day()I

    move-result v9

    .line 200
    .local v9, "da":I
    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 201
    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 202
    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 203
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->year()Ljava/lang/String;

    move-result-object v12

    .line 204
    .local v12, "y":Ljava/lang/String;
    new-instance v8, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;

    invoke-direct {v8, v12, v10, v9}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;-><init>(Ljava/lang/String;II)V

    .line 205
    .local v8, "d":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->time()Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;

    move-result-object v11

    .line 208
    .local v11, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;
    new-instance v0, Lorg/apache/james/mime4j/secfunc/datetime/DateTime;

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getMonth()I

    move-result v2

    invoke-virtual {v8}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Date;->getDay()I

    move-result v3

    invoke-virtual {v11}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getHour()I

    move-result v4

    invoke-virtual {v11}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getMinute()I

    move-result v5

    invoke-virtual {v11}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getSecond()I

    move-result v6

    invoke-virtual {v11}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;->getZone()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lorg/apache/james/mime4j/secfunc/datetime/DateTime;-><init>(Ljava/lang/String;IIIIII)V

    return-object v0
.end method

.method public final day()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 310
    const/16 v1, 0x2e

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 313
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    invoke-static {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I

    move-result v1

    return v1
.end method

.method public final day_of_week()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 261
    iget v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    if-ne v0, v3, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v0

    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 284
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    const/4 v1, 0x2

    iget v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v2, v0, v1

    .line 285
    invoke-direct {p0, v3}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 286
    new-instance v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;-><init>()V

    throw v0

    .line 261
    :cond_0
    iget v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 263
    :pswitch_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 290
    :goto_1
    iget-object v0, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    iget-object v0, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->image:Ljava/lang/String;

    return-object v0

    .line 266
    :pswitch_1
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 269
    :pswitch_2
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 272
    :pswitch_3
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 275
    :pswitch_4
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 278
    :pswitch_5
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 281
    :pswitch_6
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public generateParseException()Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x31

    const/4 v6, 0x1

    .line 720
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->removeAllElements()V

    .line 721
    new-array v3, v7, [Z

    .line 722
    .local v3, "la1tokens":[Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v7, :cond_0

    .line 723
    aput-boolean v8, v3, v1

    .line 722
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 725
    :cond_0
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_kind:I

    if-ltz v4, :cond_1

    .line 726
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_kind:I

    aput-boolean v6, v3, v4

    .line 727
    const/4 v4, -0x1

    iput v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_kind:I

    .line 729
    :cond_1
    const/4 v1, 0x0

    :goto_1
    const/4 v4, 0x7

    if-ge v1, v4, :cond_5

    .line 730
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    aget v4, v4, v1

    iget v5, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    if-ne v4, v5, :cond_4

    .line 731
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    const/16 v4, 0x20

    if-ge v2, v4, :cond_4

    .line 732
    sget-object v4, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_0:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    .line 733
    aput-boolean v6, v3, v2

    .line 735
    :cond_2
    sget-object v4, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1_1:[I

    aget v4, v4, v1

    shl-int v5, v6, v2

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 736
    add-int/lit8 v4, v2, 0x20

    aput-boolean v6, v3, v4

    .line 731
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 729
    .end local v2    # "j":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 741
    :cond_5
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v7, :cond_7

    .line 742
    aget-boolean v4, v3, v1

    if-eqz v4, :cond_6

    .line 743
    new-array v4, v6, [I

    iput-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentry:[I

    .line 744
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentry:[I

    aput v1, v4, v8

    .line 745
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentry:[I

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 741
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 748
    :cond_7
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    new-array v0, v4, [[I

    .line 749
    .local v0, "exptokseq":[[I
    const/4 v1, 0x0

    :goto_4
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v1, v4, :cond_8

    .line 750
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_expentries:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    aput-object v4, v0, v1

    .line 749
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 752
    :cond_8
    new-instance v4, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    iget-object v5, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->token:Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    sget-object v6, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->tokenImage:[Ljava/lang/String;

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;-><init>(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;[[I[Ljava/lang/String;)V

    return-object v4
.end method

.method public final hour()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 446
    const/16 v1, 0x2e

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 449
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    invoke-static {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I

    move-result v1

    return v1
.end method

.method public final minute()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 456
    const/16 v1, 0x2e

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 459
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    invoke-static {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I

    move-result v1

    return v1
.end method

.method public final month()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xc

    const/16 v1, 0xb

    const/4 v0, 0x3

    const/4 v4, -0x1

    .line 319
    iget v3, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    if-ne v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v3

    :goto_0
    packed-switch v3, :pswitch_data_0

    .line 405
    iget-object v1, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    iget v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v2, v1, v0

    .line 406
    invoke-direct {p0, v4}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 407
    new-instance v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    invoke-direct {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;-><init>()V

    throw v0

    .line 319
    :cond_0
    iget v3, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 321
    :pswitch_0
    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 324
    const/4 v0, 0x1

    .line 401
    :goto_1
    return v0

    .line 328
    :pswitch_1
    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 331
    const/4 v0, 0x2

    goto :goto_1

    .line 335
    :pswitch_2
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    goto :goto_1

    .line 342
    :pswitch_3
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 345
    const/4 v0, 0x4

    goto :goto_1

    .line 349
    :pswitch_4
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 352
    const/4 v0, 0x5

    goto :goto_1

    .line 356
    :pswitch_5
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 359
    const/4 v0, 0x6

    goto :goto_1

    .line 363
    :pswitch_6
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 366
    const/4 v0, 0x7

    goto :goto_1

    .line 370
    :pswitch_7
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 373
    const/16 v0, 0x8

    goto :goto_1

    .line 377
    :pswitch_8
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 380
    const/16 v0, 0x9

    goto :goto_1

    .line 384
    :pswitch_9
    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 387
    const/16 v0, 0xa

    goto :goto_1

    .line 391
    :pswitch_a
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move v0, v1

    .line 394
    goto :goto_1

    .line 398
    :pswitch_b
    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move v0, v2

    .line 401
    goto :goto_1

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final obs_zone()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 511
    iget v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    if-ne v2, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v2

    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 557
    iget-object v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    const/4 v3, 0x6

    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v4, v2, v3

    .line 558
    invoke-direct {p0, v5}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 559
    new-instance v2, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    invoke-direct {v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;-><init>()V

    throw v2

    .line 511
    :cond_0
    iget v2, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 513
    :pswitch_0
    const/16 v2, 0x19

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 514
    const/4 v1, 0x0

    .line 563
    .local v1, "z":I
    :goto_1
    mul-int/lit8 v2, v1, 0x64

    return v2

    .line 517
    .end local v1    # "z":I
    :pswitch_1
    const/16 v2, 0x1a

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 518
    const/4 v1, 0x0

    .line 519
    .restart local v1    # "z":I
    goto :goto_1

    .line 521
    .end local v1    # "z":I
    :pswitch_2
    const/16 v2, 0x1b

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 522
    const/4 v1, -0x5

    .line 523
    .restart local v1    # "z":I
    goto :goto_1

    .line 525
    .end local v1    # "z":I
    :pswitch_3
    const/16 v2, 0x1c

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 526
    const/4 v1, -0x4

    .line 527
    .restart local v1    # "z":I
    goto :goto_1

    .line 529
    .end local v1    # "z":I
    :pswitch_4
    const/16 v2, 0x1d

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 530
    const/4 v1, -0x6

    .line 531
    .restart local v1    # "z":I
    goto :goto_1

    .line 533
    .end local v1    # "z":I
    :pswitch_5
    const/16 v2, 0x1e

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 534
    const/4 v1, -0x5

    .line 535
    .restart local v1    # "z":I
    goto :goto_1

    .line 537
    .end local v1    # "z":I
    :pswitch_6
    const/16 v2, 0x1f

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 538
    const/4 v1, -0x7

    .line 539
    .restart local v1    # "z":I
    goto :goto_1

    .line 541
    .end local v1    # "z":I
    :pswitch_7
    const/16 v2, 0x20

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 542
    const/4 v1, -0x6

    .line 543
    .restart local v1    # "z":I
    goto :goto_1

    .line 545
    .end local v1    # "z":I
    :pswitch_8
    const/16 v2, 0x21

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 546
    const/4 v1, -0x8

    .line 547
    .restart local v1    # "z":I
    goto :goto_1

    .line 549
    .end local v1    # "z":I
    :pswitch_9
    const/16 v2, 0x22

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 550
    const/4 v1, -0x7

    .line 551
    .restart local v1    # "z":I
    goto :goto_1

    .line 553
    .end local v1    # "z":I
    :pswitch_a
    const/16 v2, 0x23

    invoke-direct {p0, v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 554
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    iget-object v2, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->image:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->getMilitaryZoneOffset(C)I

    move-result v1

    .line 555
    .restart local v1    # "z":I
    goto :goto_1

    .line 511
    nop

    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final parseAll()Lorg/apache/james/mime4j/secfunc/datetime/DateTime;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 182
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->date_time()Lorg/apache/james/mime4j/secfunc/datetime/DateTime;

    move-result-object v0

    .line 183
    .local v0, "dt":Lorg/apache/james/mime4j/secfunc/datetime/DateTime;
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 186
    return-object v0
.end method

.method public final second()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 466
    const/16 v1, 0x2e

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 469
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    invoke-static {v0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I

    move-result v1

    return v1
.end method

.method public final time()Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/16 v6, 0x17

    .line 423
    const/4 v2, 0x0

    .line 424
    .local v2, "s":I
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->hour()I

    move-result v0

    .line 425
    .local v0, "h":I
    invoke-direct {p0, v6}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 426
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->minute()I

    move-result v1

    .line 427
    .local v1, "m":I
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v4

    :goto_0
    packed-switch v4, :pswitch_data_0

    .line 433
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    const/4 v5, 0x4

    iget v6, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v6, v4, v5

    .line 436
    :goto_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->zone()I

    move-result v3

    .line 439
    .local v3, "z":I
    new-instance v4, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;

    invoke-direct {v4, v0, v1, v2, v3}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser$Time;-><init>(IIII)V

    return-object v4

    .line 427
    .end local v3    # "z":I
    :cond_0
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 429
    :pswitch_0
    invoke-direct {p0, v6}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 430
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->second()I

    move-result v2

    .line 431
    goto :goto_1

    .line 427
    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
    .end packed-switch
.end method

.method public final year()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    .line 414
    const/16 v1, 0x2e

    invoke-direct {p0, v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 417
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    iget-object v1, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->image:Ljava/lang/String;

    return-object v1
.end method

.method public final zone()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 477
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    if-ne v4, v3, :cond_0

    invoke-direct {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk()I

    move-result v4

    :goto_0
    packed-switch v4, :pswitch_data_0

    .line 497
    iget-object v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_la1:[I

    const/4 v5, 0x5

    iget v6, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_gen:I

    aput v6, v4, v5

    .line 498
    invoke-direct {p0, v3}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    .line 499
    new-instance v3, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;

    invoke-direct {v3}, Lorg/apache/james/mime4j/secfunc/datetime/parser/ParseException;-><init>()V

    throw v3

    .line 477
    :cond_0
    iget v4, p0, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_ntk:I

    goto :goto_0

    .line 479
    :pswitch_0
    const/16 v4, 0x18

    invoke-direct {p0, v4}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v0

    .line 480
    .local v0, "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    const/16 v4, 0x2e

    invoke-direct {p0, v4}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->jj_consume_token(I)Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;

    move-result-object v1

    .line 481
    .local v1, "u":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    invoke-static {v1}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->parseDigits(Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;)I

    move-result v4

    iget-object v5, v0, Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;->image:Ljava/lang/String;

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    mul-int v2, v4, v3

    .line 503
    .end local v0    # "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    .end local v1    # "u":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    .local v2, "z":I
    :goto_2
    return v2

    .line 481
    .end local v2    # "z":I
    .restart local v0    # "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    .restart local v1    # "u":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 494
    .end local v0    # "t":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    .end local v1    # "u":Lorg/apache/james/mime4j/secfunc/datetime/parser/Token;
    :pswitch_1
    invoke-virtual {p0}, Lorg/apache/james/mime4j/secfunc/datetime/parser/DateTimeParser;->obs_zone()I

    move-result v2

    .line 495
    .restart local v2    # "z":I
    goto :goto_2

    .line 477
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
