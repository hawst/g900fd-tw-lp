.class public Lorg/xbill/DNS/Zone;
.super Ljava/lang/Object;
.source "Zone.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/xbill/DNS/Zone$ZoneIterator;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x7ff5d5cd9cf8a31eL


# instance fields
.field private NS:Lorg/xbill/DNS/RRset;

.field private SOA:Lorg/xbill/DNS/SOARecord;

.field private data:Ljava/util/Map;

.field private dclass:I

.field private hasWild:Z

.field private origin:Lorg/xbill/DNS/Name;

.field private originNode:Ljava/lang/Object;


# direct methods
.method static access$000(Lorg/xbill/DNS/Zone;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lorg/xbill/DNS/Zone;

    .prologue
    .line 15
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->data:Ljava/util/Map;

    return-object v0
.end method

.method static access$100(Lorg/xbill/DNS/Zone;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/xbill/DNS/Zone;

    .prologue
    .line 15
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->originNode:Ljava/lang/Object;

    return-object v0
.end method

.method static access$200(Lorg/xbill/DNS/Zone;Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
    .locals 1
    .param p0, "x0"    # Lorg/xbill/DNS/Zone;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lorg/xbill/DNS/Zone;->allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;

    move-result-object v0

    return-object v0
.end method

.method static access$300(Lorg/xbill/DNS/Zone;Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
    .locals 1
    .param p0, "x0"    # Lorg/xbill/DNS/Zone;
    .param p1, "x1"    # Ljava/lang/Object;
    .param p2, "x2"    # I

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v0

    return-object v0
.end method

.method static access$400(Lorg/xbill/DNS/Zone;)Lorg/xbill/DNS/Name;
    .locals 1
    .param p0, "x0"    # Lorg/xbill/DNS/Zone;

    .prologue
    .line 15
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->origin:Lorg/xbill/DNS/Name;

    return-object v0
.end method

.method private declared-synchronized allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;
    .locals 5
    .param p1, "types"    # Ljava/lang/Object;

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    instance-of v3, p1, Ljava/util/List;

    if-eqz v3, :cond_0

    .line 237
    move-object v0, p1

    check-cast v0, Ljava/util/List;

    move-object v2, v0

    .line 238
    .local v2, "typelist":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/xbill/DNS/RRset;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/xbill/DNS/RRset;

    check-cast v3, [Lorg/xbill/DNS/RRset;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    .end local v2    # "typelist":Ljava/util/List;
    :goto_0
    monitor-exit p0

    return-object v3

    .line 240
    :cond_0
    :try_start_1
    move-object v0, p1

    check-cast v0, Lorg/xbill/DNS/RRset;

    move-object v1, v0

    .line 241
    .local v1, "set":Lorg/xbill/DNS/RRset;
    const/4 v3, 0x1

    new-array v3, v3, [Lorg/xbill/DNS/RRset;

    const/4 v4, 0x0

    aput-object v1, v3, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 236
    .end local v1    # "set":Lorg/xbill/DNS/RRset;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
    .locals 1
    .param p1, "name"    # Lorg/xbill/DNS/Name;

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->data:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized lookup(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;
    .locals 13
    .param p1, "name"    # Lorg/xbill/DNS/Name;
    .param p2, "type"    # I

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v12, p0, Lorg/xbill/DNS/Zone;->origin:Lorg/xbill/DNS/Name;

    invoke-virtual {p1, v12}, Lorg/xbill/DNS/Name;->subdomain(Lorg/xbill/DNS/Name;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 341
    const/4 v12, 0x1

    invoke-static {v12}, Lorg/xbill/DNS/SetResponse;->ofType(I)Lorg/xbill/DNS/SetResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 422
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v8

    .line 343
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lorg/xbill/DNS/Name;->labels()I

    move-result v3

    .line 344
    .local v3, "labels":I
    iget-object v12, p0, Lorg/xbill/DNS/Zone;->origin:Lorg/xbill/DNS/Name;

    invoke-virtual {v12}, Lorg/xbill/DNS/Name;->labels()I

    move-result v5

    .line 346
    .local v5, "olabels":I
    move v9, v5

    .local v9, "tlabels":I
    :goto_1
    if-gt v9, v3, :cond_d

    .line 347
    if-ne v9, v5, :cond_3

    const/4 v2, 0x1

    .line 348
    .local v2, "isOrigin":Z
    :goto_2
    if-ne v9, v3, :cond_4

    const/4 v1, 0x1

    .line 350
    .local v1, "isExact":Z
    :goto_3
    if-eqz v2, :cond_5

    .line 351
    iget-object v10, p0, Lorg/xbill/DNS/Zone;->origin:Lorg/xbill/DNS/Name;

    .line 357
    .local v10, "tname":Lorg/xbill/DNS/Name;
    :goto_4
    invoke-direct {p0, v10}, Lorg/xbill/DNS/Zone;->exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;

    move-result-object v11

    .line 358
    .local v11, "types":Ljava/lang/Object;
    if-nez v11, :cond_7

    .line 346
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 347
    .end local v1    # "isExact":Z
    .end local v2    # "isOrigin":Z
    .end local v10    # "tname":Lorg/xbill/DNS/Name;
    .end local v11    # "types":Ljava/lang/Object;
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 348
    .restart local v2    # "isOrigin":Z
    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    .line 352
    .restart local v1    # "isExact":Z
    :cond_5
    if-eqz v1, :cond_6

    .line 353
    move-object v10, p1

    .restart local v10    # "tname":Lorg/xbill/DNS/Name;
    goto :goto_4

    .line 355
    .end local v10    # "tname":Lorg/xbill/DNS/Name;
    :cond_6
    new-instance v10, Lorg/xbill/DNS/Name;

    sub-int v12, v3, v9

    invoke-direct {v10, p1, v12}, Lorg/xbill/DNS/Name;-><init>(Lorg/xbill/DNS/Name;I)V

    .restart local v10    # "tname":Lorg/xbill/DNS/Name;
    goto :goto_4

    .line 362
    .restart local v11    # "types":Ljava/lang/Object;
    :cond_7
    if-nez v2, :cond_8

    .line 363
    const/4 v12, 0x2

    invoke-direct {p0, v11, v12}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v4

    .line 364
    .local v4, "ns":Lorg/xbill/DNS/RRset;
    if-eqz v4, :cond_8

    .line 365
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x3

    invoke-direct {v8, v12, v4}, Lorg/xbill/DNS/SetResponse;-><init>(ILorg/xbill/DNS/RRset;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 340
    .end local v1    # "isExact":Z
    .end local v2    # "isOrigin":Z
    .end local v3    # "labels":I
    .end local v4    # "ns":Lorg/xbill/DNS/RRset;
    .end local v5    # "olabels":I
    .end local v9    # "tlabels":I
    .end local v10    # "tname":Lorg/xbill/DNS/Name;
    .end local v11    # "types":Ljava/lang/Object;
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 370
    .restart local v1    # "isExact":Z
    .restart local v2    # "isOrigin":Z
    .restart local v3    # "labels":I
    .restart local v5    # "olabels":I
    .restart local v9    # "tlabels":I
    .restart local v10    # "tname":Lorg/xbill/DNS/Name;
    .restart local v11    # "types":Ljava/lang/Object;
    :cond_8
    if-eqz v1, :cond_9

    const/16 v12, 0xff

    if-ne p2, v12, :cond_9

    .line 371
    :try_start_2
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x6

    invoke-direct {v8, v12}, Lorg/xbill/DNS/SetResponse;-><init>(I)V

    .line 372
    .local v8, "sr":Lorg/xbill/DNS/SetResponse;
    invoke-direct {p0, v11}, Lorg/xbill/DNS/Zone;->allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;

    move-result-object v7

    .line 373
    .local v7, "sets":[Lorg/xbill/DNS/RRset;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_5
    array-length v12, v7

    if-ge v0, v12, :cond_0

    .line 374
    aget-object v12, v7, v0

    invoke-virtual {v8, v12}, Lorg/xbill/DNS/SetResponse;->addRRset(Lorg/xbill/DNS/RRset;)V

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 382
    .end local v0    # "i":I
    .end local v7    # "sets":[Lorg/xbill/DNS/RRset;
    .end local v8    # "sr":Lorg/xbill/DNS/SetResponse;
    :cond_9
    if-eqz v1, :cond_b

    .line 383
    invoke-direct {p0, v11, p2}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v6

    .line 384
    .local v6, "rrset":Lorg/xbill/DNS/RRset;
    if-eqz v6, :cond_a

    .line 385
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x6

    invoke-direct {v8, v12}, Lorg/xbill/DNS/SetResponse;-><init>(I)V

    .line 386
    .restart local v8    # "sr":Lorg/xbill/DNS/SetResponse;
    invoke-virtual {v8, v6}, Lorg/xbill/DNS/SetResponse;->addRRset(Lorg/xbill/DNS/RRset;)V

    goto :goto_0

    .line 389
    .end local v8    # "sr":Lorg/xbill/DNS/SetResponse;
    :cond_a
    const/4 v12, 0x5

    invoke-direct {p0, v11, v12}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v6

    .line 390
    if-eqz v6, :cond_c

    .line 391
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x4

    invoke-direct {v8, v12, v6}, Lorg/xbill/DNS/SetResponse;-><init>(ILorg/xbill/DNS/RRset;)V

    goto :goto_0

    .line 394
    .end local v6    # "rrset":Lorg/xbill/DNS/RRset;
    :cond_b
    const/16 v12, 0x27

    invoke-direct {p0, v11, v12}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v6

    .line 395
    .restart local v6    # "rrset":Lorg/xbill/DNS/RRset;
    if-eqz v6, :cond_c

    .line 396
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x5

    invoke-direct {v8, v12, v6}, Lorg/xbill/DNS/SetResponse;-><init>(ILorg/xbill/DNS/RRset;)V

    goto/16 :goto_0

    .line 401
    :cond_c
    if-eqz v1, :cond_2

    .line 402
    const/4 v12, 0x2

    invoke-static {v12}, Lorg/xbill/DNS/SetResponse;->ofType(I)Lorg/xbill/DNS/SetResponse;

    move-result-object v8

    goto/16 :goto_0

    .line 405
    .end local v1    # "isExact":Z
    .end local v2    # "isOrigin":Z
    .end local v6    # "rrset":Lorg/xbill/DNS/RRset;
    .end local v10    # "tname":Lorg/xbill/DNS/Name;
    .end local v11    # "types":Ljava/lang/Object;
    :cond_d
    iget-boolean v12, p0, Lorg/xbill/DNS/Zone;->hasWild:Z

    if-eqz v12, :cond_10

    .line 406
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_6
    sub-int v12, v3, v5

    if-ge v0, v12, :cond_10

    .line 407
    add-int/lit8 v12, v0, 0x1

    invoke-virtual {p1, v12}, Lorg/xbill/DNS/Name;->wild(I)Lorg/xbill/DNS/Name;

    move-result-object v10

    .line 409
    .restart local v10    # "tname":Lorg/xbill/DNS/Name;
    invoke-direct {p0, v10}, Lorg/xbill/DNS/Zone;->exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;

    move-result-object v11

    .line 410
    .restart local v11    # "types":Ljava/lang/Object;
    if-nez v11, :cond_f

    .line 406
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 413
    :cond_f
    invoke-direct {p0, v11, p2}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v6

    .line 414
    .restart local v6    # "rrset":Lorg/xbill/DNS/RRset;
    if-eqz v6, :cond_e

    .line 415
    new-instance v8, Lorg/xbill/DNS/SetResponse;

    const/4 v12, 0x6

    invoke-direct {v8, v12}, Lorg/xbill/DNS/SetResponse;-><init>(I)V

    .line 416
    .restart local v8    # "sr":Lorg/xbill/DNS/SetResponse;
    invoke-virtual {v8, v6}, Lorg/xbill/DNS/SetResponse;->addRRset(Lorg/xbill/DNS/RRset;)V

    goto/16 :goto_0

    .line 422
    .end local v0    # "i":I
    .end local v6    # "rrset":Lorg/xbill/DNS/RRset;
    .end local v8    # "sr":Lorg/xbill/DNS/SetResponse;
    .end local v10    # "tname":Lorg/xbill/DNS/Name;
    .end local v11    # "types":Ljava/lang/Object;
    :cond_10
    const/4 v12, 0x1

    invoke-static {v12}, Lorg/xbill/DNS/SetResponse;->ofType(I)Lorg/xbill/DNS/SetResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    goto/16 :goto_0
.end method

.method private nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 6
    .param p1, "sb"    # Ljava/lang/StringBuffer;
    .param p2, "node"    # Ljava/lang/Object;

    .prologue
    .line 523
    invoke-direct {p0, p2}, Lorg/xbill/DNS/Zone;->allRRsets(Ljava/lang/Object;)[Lorg/xbill/DNS/RRset;

    move-result-object v3

    .line 524
    .local v3, "sets":[Lorg/xbill/DNS/RRset;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 525
    aget-object v2, v3, v0

    .line 526
    .local v2, "rrset":Lorg/xbill/DNS/RRset;
    invoke-virtual {v2}, Lorg/xbill/DNS/RRset;->rrs()Ljava/util/Iterator;

    move-result-object v1

    .line 527
    .local v1, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 528
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 529
    :cond_0
    invoke-virtual {v2}, Lorg/xbill/DNS/RRset;->sigs()Ljava/util/Iterator;

    move-result-object v1

    .line 530
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 531
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 524
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 533
    .end local v1    # "it":Ljava/util/Iterator;
    .end local v2    # "rrset":Lorg/xbill/DNS/RRset;
    :cond_2
    return-void
.end method

.method private declared-synchronized oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;
    .locals 6
    .param p1, "types"    # Ljava/lang/Object;
    .param p2, "type"    # I

    .prologue
    .line 247
    monitor-enter p0

    const/16 v4, 0xff

    if-ne p2, v4, :cond_0

    .line 248
    :try_start_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "oneRRset(ANY)"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 247
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 249
    :cond_0
    :try_start_1
    instance-of v4, p1, Ljava/util/List;

    if-eqz v4, :cond_3

    .line 250
    move-object v0, p1

    check-cast v0, Ljava/util/List;

    move-object v2, v0

    .line 251
    .local v2, "list":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 252
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/xbill/DNS/RRset;

    .line 253
    .local v3, "set":Lorg/xbill/DNS/RRset;
    invoke-virtual {v3}, Lorg/xbill/DNS/RRset;->getType()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-ne v4, p2, :cond_2

    .line 261
    .end local v1    # "i":I
    .end local v2    # "list":Ljava/util/List;
    .end local v3    # "set":Lorg/xbill/DNS/RRset;
    :cond_1
    :goto_1
    monitor-exit p0

    return-object v3

    .line 251
    .restart local v1    # "i":I
    .restart local v2    # "list":Ljava/util/List;
    .restart local v3    # "set":Lorg/xbill/DNS/RRset;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 257
    .end local v1    # "i":I
    .end local v2    # "list":Ljava/util/List;
    .end local v3    # "set":Lorg/xbill/DNS/RRset;
    :cond_3
    :try_start_2
    move-object v0, p1

    check-cast v0, Lorg/xbill/DNS/RRset;

    move-object v3, v0

    .line 258
    .restart local v3    # "set":Lorg/xbill/DNS/RRset;
    invoke-virtual {v3}, Lorg/xbill/DNS/RRset;->getType()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-eq v4, p2, :cond_1

    .line 261
    .end local v3    # "set":Lorg/xbill/DNS/RRset;
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public AXFR()Ljava/util/Iterator;
    .locals 2

    .prologue
    .line 518
    new-instance v0, Lorg/xbill/DNS/Zone$ZoneIterator;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lorg/xbill/DNS/Zone$ZoneIterator;-><init>(Lorg/xbill/DNS/Zone;Z)V

    return-object v0
.end method

.method public findExactMatch(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/RRset;
    .locals 2
    .param p1, "name"    # Lorg/xbill/DNS/Name;
    .param p2, "type"    # I

    .prologue
    .line 446
    invoke-direct {p0, p1}, Lorg/xbill/DNS/Zone;->exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;

    move-result-object v0

    .line 447
    .local v0, "types":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 448
    const/4 v1, 0x0

    .line 449
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0, p2}, Lorg/xbill/DNS/Zone;->oneRRset(Ljava/lang/Object;I)Lorg/xbill/DNS/RRset;

    move-result-object v1

    goto :goto_0
.end method

.method public findRecords(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;
    .locals 1
    .param p1, "name"    # Lorg/xbill/DNS/Name;
    .param p2, "type"    # I

    .prologue
    .line 434
    invoke-direct {p0, p1, p2}, Lorg/xbill/DNS/Zone;->lookup(Lorg/xbill/DNS/Name;I)Lorg/xbill/DNS/SetResponse;

    move-result-object v0

    return-object v0
.end method

.method public getNS()Lorg/xbill/DNS/RRset;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->NS:Lorg/xbill/DNS/RRset;

    return-object v0
.end method

.method public getSOA()Lorg/xbill/DNS/SOARecord;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lorg/xbill/DNS/Zone;->SOA:Lorg/xbill/DNS/SOARecord;

    return-object v0
.end method

.method public declared-synchronized toMasterFile()Ljava/lang/String;
    .locals 5

    .prologue
    .line 540
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lorg/xbill/DNS/Zone;->data:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 541
    .local v2, "zentries":Ljava/util/Iterator;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 542
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lorg/xbill/DNS/Zone;->originNode:Ljava/lang/Object;

    invoke-direct {p0, v1, v3}, Lorg/xbill/DNS/Zone;->nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    .line 543
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 544
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 545
    .local v0, "entry":Ljava/util/Map$Entry;
    iget-object v3, p0, Lorg/xbill/DNS/Zone;->origin:Lorg/xbill/DNS/Name;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 546
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lorg/xbill/DNS/Zone;->nodeToString(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 540
    .end local v0    # "entry":Ljava/util/Map$Entry;
    .end local v1    # "sb":Ljava/lang/StringBuffer;
    .end local v2    # "zentries":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 548
    .restart local v1    # "sb":Ljava/lang/StringBuffer;
    .restart local v2    # "zentries":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    monitor-exit p0

    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 556
    invoke-virtual {p0}, Lorg/xbill/DNS/Zone;->toMasterFile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
