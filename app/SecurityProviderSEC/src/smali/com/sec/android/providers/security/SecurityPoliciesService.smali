.class public Lcom/sec/android/providers/security/SecurityPoliciesService;
.super Landroid/app/Service;
.source "SecurityPoliciesService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private getPasswordExpirationDate()J
    .locals 4

    .prologue
    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private passwordExpired()V
    .locals 2

    .prologue
    .line 95
    const-string v0, "SecurityPoliciesService"

    const-string v1, "Password expired!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method private setupPasswordExpiration()V
    .locals 10

    .prologue
    .line 65
    const-string v5, "SecurityPoliciesService"

    const-string v8, "setupPasswordExpiration"

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const/16 v4, 0x29a

    .line 69
    .local v4, "requestCode":I
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 70
    .local v2, "intent":Landroid/content/Intent;
    const-class v5, Lcom/sec/android/providers/security/SecurityPoliciesService;

    invoke-virtual {v2, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 71
    const-string v5, "com.android.security.PASSWORD_EXPIRED"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const/4 v1, 0x0

    .line 73
    .local v1, "flags":I
    invoke-static {p0, v4, v2, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 76
    .local v3, "operation":Landroid/app/PendingIntent;
    invoke-direct {p0}, Lcom/sec/android/providers/security/SecurityPoliciesService;->getPasswordExpirationDate()J

    move-result-wide v6

    .line 77
    .local v6, "triggerAtTime":J
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/providers/security/SecurityPoliciesService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v8, "alarm"

    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 79
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 81
    .end local v0    # "alarmManager":Landroid/app/AlarmManager;
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "SecurityPoliciesService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-direct {p0}, Lcom/sec/android/providers/security/SecurityPoliciesService;->setupPasswordExpiration()V

    .line 29
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 30
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 60
    const-string v0, "SecurityPoliciesService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 62
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 34
    const-string v1, "SecurityPoliciesService"

    const-string v2, "onStartCommand"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 39
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 40
    const-string v1, "SecurityPoliciesService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    const-string v1, "com.android.security.PASSWORD_EXPIRED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/providers/security/SecurityPoliciesService;->passwordExpired()V

    .line 50
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    if-nez p1, :cond_1

    .line 51
    const-string v1, "SecurityPoliciesService"

    const-string v2, "Intetnt is null - onStartCommand"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v1, -0x1

    .line 54
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0
.end method
