.class public Lcom/samsung/android/mdm/DodBanner;
.super Landroid/app/Service;
.source "DodBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mdm/DodBanner$BannerHandler;,
        Lcom/samsung/android/mdm/DodBanner$Status;
    }
.end annotation


# static fields
.field private static mIsMultiWindowEnabled:I

.field private static mSelf:Lcom/samsung/android/mdm/DodBanner;


# instance fields
.field private bannerText:Landroid/widget/TextView;

.field private emergencyCall:Landroid/widget/Button;

.field private iAgree:Landroid/widget/Button;

.field private lastAccessDate:Landroid/widget/TextView;

.field private mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

.field private mDodBanner:Lcom/samsung/android/mdm/DodView;

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mHandler:Lcom/samsung/android/mdm/DodBanner$BannerHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

.field private mStatusBarDisableCount:I

.field private mStatusBarManager:Landroid/app/StatusBarManager;

.field private mTelMgr:Landroid/telephony/TelephonyManager;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mWindowManager:Landroid/view/WindowManager;

.field private sdf:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 43
    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->iAgree:Landroid/widget/Button;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->emergencyCall:Landroid/widget/Button;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 51
    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->lastAccessDate:Landroid/widget/TextView;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->bannerText:Landroid/widget/TextView;

    .line 69
    sget-object v0, Lcom/samsung/android/mdm/DodBanner$Status;->INVISIBLE:Lcom/samsung/android/mdm/DodBanner$Status;

    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

    .line 225
    new-instance v0, Lcom/samsung/android/mdm/DodBanner$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/mdm/DodBanner$3;-><init>(Lcom/samsung/android/mdm/DodBanner;)V

    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 239
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/mdm/DodBanner;)Landroid/app/enterprise/SecurityPolicy;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/mdm/DodBanner;)Ljava/text/SimpleDateFormat;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->sdf:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/mdm/DodBanner;)Lcom/samsung/android/mdm/DodView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/mdm/DodBanner;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/mdm/DodBanner;)Lcom/samsung/android/mdm/DodBanner$Status;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/mdm/DodBanner;Lcom/samsung/android/mdm/DodBanner$Status;)Lcom/samsung/android/mdm/DodBanner$Status;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;
    .param p1, "x1"    # Lcom/samsung/android/mdm/DodBanner$Status;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/mdm/DodBanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->softResetIfNeeded()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/mdm/DodBanner;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdm/DodBanner;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->checkTopActivity()V

    return-void
.end method

.method private checkTopActivity()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 260
    const-string v4, "activity"

    invoke-virtual {p0, v4}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 263
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 264
    .local v3, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 265
    .local v2, "pkgName":Ljava/lang/String;
    const-string v4, "com.android.phone"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 266
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->reset()V

    .line 273
    :goto_0
    return-void

    .line 269
    :cond_0
    const-string v4, "DodBanner"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pkg Name = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 272
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getHandler()Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private disableMultiWindow()V
    .locals 5

    .prologue
    .line 301
    monitor-enter p0

    .line 303
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_enabled"

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/samsung/android/mdm/DodBanner;->mIsMultiWindowEnabled:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_enabled"

    const/4 v3, 0x0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 310
    monitor-exit p0

    .line 311
    return-void

    .line 305
    :catch_0
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DodBanner"

    const-string v2, "Multi Window Setting not found"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private disableStatusBar()V
    .locals 2

    .prologue
    .line 277
    monitor-enter p0

    .line 279
    :try_start_0
    iget v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarDisableCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarDisableCount:I

    if-nez v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 284
    :cond_0
    monitor-exit p0

    .line 286
    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 324
    const/4 v6, -0x1

    .line 325
    .local v6, "stretch":I
    const/16 v7, 0x7d2

    .line 326
    .local v7, "type":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d2

    move v2, v1

    move v5, v4

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 329
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenOrientation:I

    .line 331
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, 0x1000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 333
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 336
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    .line 337
    const-string v1, "Dod_Banner"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 339
    return-object v0
.end method

.method public static getService()Lcom/samsung/android/mdm/DodBanner;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/samsung/android/mdm/DodBanner;->mSelf:Lcom/samsung/android/mdm/DodBanner;

    return-object v0
.end method

.method private reenableMultiWindow()V
    .locals 4

    .prologue
    .line 314
    monitor-enter p0

    .line 315
    :try_start_0
    sget v0, Lcom/samsung/android/mdm/DodBanner;->mIsMultiWindowEnabled:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "multi_window_enabled"

    sget v2, Lcom/samsung/android/mdm/DodBanner;->mIsMultiWindowEnabled:I

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 319
    :cond_0
    monitor-exit p0

    .line 320
    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private reenableStatusBar()V
    .locals 2

    .prologue
    .line 289
    monitor-enter p0

    .line 290
    :try_start_0
    iget v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarDisableCount:I

    if-lez v0, :cond_0

    .line 291
    iget v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarDisableCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarDisableCount:I

    if-nez v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarManager:Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 297
    :cond_0
    monitor-exit p0

    .line 298
    return-void

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private reset()V
    .locals 9

    .prologue
    .line 129
    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 130
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const/high16 v4, 0x7f030000

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 132
    .local v0, "banner":Landroid/view/View;
    new-instance v4, Lcom/samsung/android/mdm/DodView;

    invoke-direct {v4, p0}, Lcom/samsung/android/mdm/DodView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    .line 134
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/samsung/android/mdm/DodView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    const v5, 0x7f060001

    invoke-virtual {v4, v5}, Lcom/samsung/android/mdm/DodView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->bannerText:Landroid/widget/TextView;

    .line 138
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->bannerText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 140
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    const/high16 v5, 0x7f060000

    invoke-virtual {v4, v5}, Lcom/samsung/android/mdm/DodView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->lastAccessDate:Landroid/widget/TextView;

    .line 142
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "dd MMM yyyy kk:mm:ss z"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->sdf:Ljava/text/SimpleDateFormat;

    .line 144
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 146
    sget-object v4, Lcom/samsung/android/mdm/DodBanner$Status;->VISIBLE:Lcom/samsung/android/mdm/DodBanner$Status;

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

    .line 148
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    const v5, 0x7f060003

    invoke-virtual {v4, v5}, Lcom/samsung/android/mdm/DodView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->iAgree:Landroid/widget/Button;

    .line 149
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->iAgree:Landroid/widget/Button;

    new-instance v5, Lcom/samsung/android/mdm/DodBanner$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/mdm/DodBanner$1;-><init>(Lcom/samsung/android/mdm/DodBanner;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    const v5, 0x7f060002

    invoke-virtual {v4, v5}, Lcom/samsung/android/mdm/DodView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->emergencyCall:Landroid/widget/Button;

    .line 163
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mTelMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->isVoiceCapable()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    if-nez v4, :cond_0

    .line 165
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->emergencyCall:Landroid/widget/Button;

    new-instance v5, Lcom/samsung/android/mdm/DodBanner$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/mdm/DodBanner$2;-><init>(Lcom/samsung/android/mdm/DodBanner;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/mdm/DodBanner;->lastAccessDate:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Last Access : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    invoke-virtual {v4}, Landroid/app/enterprise/SecurityPolicy;->getDeviceLastAccessDate()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->sdf:Ljava/text/SimpleDateFormat;

    iget-object v7, p0, Lcom/samsung/android/mdm/DodBanner;->sdf:Ljava/text/SimpleDateFormat;

    iget-object v8, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    invoke-virtual {v8}, Landroid/app/enterprise/SecurityPolicy;->getDeviceLastAccessDate()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    invoke-virtual {v4}, Landroid/app/enterprise/SecurityPolicy;->getRebootBannerText()Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "bannerMessage":Ljava/lang/String;
    const-string v4, "DodBanner"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Text string"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    if-eqz v1, :cond_2

    .line 201
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->bannerText:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    .end local v1    # "bannerMessage":Ljava/lang/String;
    :goto_2
    return-void

    .line 186
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->emergencyCall:Landroid/widget/Button;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 193
    :cond_1
    :try_start_1
    const-string v4, ""

    goto :goto_1

    .line 205
    .restart local v1    # "bannerMessage":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/mdm/DodBanner;->bannerText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f040002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 210
    .end local v1    # "bannerMessage":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 211
    .local v2, "e":Ljava/text/ParseException;
    const-string v4, "DodBanner"

    const-string v5, "Exception is parsing date..."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private softResetIfNeeded()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->getLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->disableStatusBar()V

    .line 123
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->disableMultiWindow()V

    .line 124
    sget-object v0, Lcom/samsung/android/mdm/DodBanner$Status;->VISIBLE:Lcom/samsung/android/mdm/DodBanner$Status;

    iput-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;

    .line 125
    return-void
.end method


# virtual methods
.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mHandler:Lcom/samsung/android/mdm/DodBanner$BannerHandler;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 346
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 89
    sput-object p0, Lcom/samsung/android/mdm/DodBanner;->mSelf:Lcom/samsung/android/mdm/DodBanner;

    .line 91
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 92
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 94
    const-string v1, "window"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;

    .line 95
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mTelMgr:Landroid/telephony/TelephonyManager;

    .line 96
    const-string v1, "enterprise_policy"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 98
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    .line 101
    :cond_0
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/samsung/android/mdm/DodBanner;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/StatusBarManager;

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mStatusBarManager:Landroid/app/StatusBarManager;

    .line 103
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "DodBanner"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mHandlerThread:Landroid/os/HandlerThread;

    .line 104
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 105
    new-instance v1, Lcom/samsung/android/mdm/DodBanner$BannerHandler;

    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mdm/DodBanner$BannerHandler;-><init>(Lcom/samsung/android/mdm/DodBanner;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mHandler:Lcom/samsung/android/mdm/DodBanner$BannerHandler;

    .line 106
    const-string v1, "DodBanner"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "User id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->reset()V

    .line 109
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/enterprise/SecurityPolicy;->setDodBannerVisibleStatus(Z)Z

    .line 111
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->disableStatusBar()V

    .line 112
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->disableMultiWindow()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mSecPolicy:Landroid/app/enterprise/SecurityPolicy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/enterprise/SecurityPolicy;->setDodBannerVisibleStatus(Z)Z

    .line 115
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public stopService(Z)V
    .locals 3
    .param p1, "killService"    # Z

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 217
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->reenableStatusBar()V

    .line 218
    invoke-direct {p0}, Lcom/samsung/android/mdm/DodBanner;->reenableMultiWindow()V

    .line 219
    if-eqz p1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/samsung/android/mdm/DodBanner;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/mdm/DodBanner;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 221
    invoke-virtual {p0}, Lcom/samsung/android/mdm/DodBanner;->stopSelf()V

    .line 223
    :cond_0
    return-void
.end method
