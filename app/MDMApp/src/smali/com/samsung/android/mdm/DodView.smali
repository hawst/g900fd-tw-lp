.class public Lcom/samsung/android/mdm/DodView;
.super Landroid/widget/LinearLayout;
.source "DodView.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPersona:Landroid/os/PersonaManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    iput-object p1, p0, Lcom/samsung/android/mdm/DodView;->mContext:Landroid/content/Context;

    .line 17
    iget-object v0, p0, Lcom/samsung/android/mdm/DodView;->mContext:Landroid/content/Context;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    .line 18
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 23
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 25
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    .line 30
    .local v0, "userId":I
    iget-object v1, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    if-eqz v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 33
    iget-object v1, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1, v0}, Landroid/os/PersonaManager;->lockPersona(I)V

    .line 34
    iget-object v1, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    iget-object v2, p0, Lcom/samsung/android/mdm/DodView;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v2, v0}, Landroid/os/PersonaManager;->getParentId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    .line 35
    invoke-static {}, Lcom/samsung/android/mdm/DodBanner;->getService()Lcom/samsung/android/mdm/DodBanner;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/mdm/DodBanner;->stopService(Z)V

    .line 40
    .end local v0    # "userId":I
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method
