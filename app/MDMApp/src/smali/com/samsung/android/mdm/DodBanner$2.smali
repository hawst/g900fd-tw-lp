.class Lcom/samsung/android/mdm/DodBanner$2;
.super Ljava/lang/Object;
.source "DodBanner.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/mdm/DodBanner;->reset()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mdm/DodBanner;


# direct methods
.method constructor <init>(Lcom/samsung/android/mdm/DodBanner;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 170
    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    # getter for: Lcom/samsung/android/mdm/DodBanner;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v2}, Lcom/samsung/android/mdm/DodBanner;->access$300(Lcom/samsung/android/mdm/DodBanner;)Landroid/view/WindowManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    # getter for: Lcom/samsung/android/mdm/DodBanner;->mDodBanner:Lcom/samsung/android/mdm/DodView;
    invoke-static {v3}, Lcom/samsung/android/mdm/DodBanner;->access$200(Lcom/samsung/android/mdm/DodBanner;)Lcom/samsung/android/mdm/DodView;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 171
    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    sget-object v3, Lcom/samsung/android/mdm/DodBanner$Status;->HIDDEN_EMERGENCY_CALL:Lcom/samsung/android/mdm/DodBanner$Status;

    # setter for: Lcom/samsung/android/mdm/DodBanner;->mBannerStatus:Lcom/samsung/android/mdm/DodBanner$Status;
    invoke-static {v2, v3}, Lcom/samsung/android/mdm/DodBanner;->access$402(Lcom/samsung/android/mdm/DodBanner;Lcom/samsung/android/mdm/DodBanner$Status;)Lcom/samsung/android/mdm/DodBanner$Status;

    .line 172
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 173
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 175
    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    invoke-virtual {v2, v0}, Lcom/samsung/android/mdm/DodBanner;->startActivity(Landroid/content/Intent;)V

    .line 176
    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    invoke-virtual {v2}, Lcom/samsung/android/mdm/DodBanner;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 177
    .local v1, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/samsung/android/mdm/DodBanner$2;->this$0:Lcom/samsung/android/mdm/DodBanner;

    invoke-virtual {v2}, Lcom/samsung/android/mdm/DodBanner;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 181
    return-void
.end method
