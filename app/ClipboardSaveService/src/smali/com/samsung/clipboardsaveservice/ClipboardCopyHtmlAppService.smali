.class public Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;
.super Landroid/app/Service;
.source "ClipboardCopyHtmlAppService.java"


# instance fields
.field final LIMIT_FILE_SIZE:I

.field mContext:Landroid/content/Context;

.field private mDarkTheme:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mDarkTheme:Z

    .line 28
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->LIMIT_FILE_SIZE:I

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 36
    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 38
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    .line 40
    new-instance v8, Landroid/os/Binder;

    invoke-direct {v8}, Landroid/os/Binder;-><init>()V

    .line 41
    .local v8, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    .line 43
    .local v2, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v2, v8, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 44
    const-string v19, "ClipboardServiceEx"

    const-string v20, "setProcessForeground"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    const-string v19, "clipboardEx"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/sec/clipboard/ClipboardExManager;

    .line 54
    .local v12, "mCM":Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v14

    .line 55
    .local v14, "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    const-string v19, "htmlPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 56
    .local v9, "htmlPath":Ljava/lang/String;
    const-string v7, ""

    .line 58
    .local v7, "firstImagePath":Ljava/lang/String;
    const/4 v5, 0x1

    .line 59
    .local v5, "defaultTheme":Z
    const-string v19, "ro.build.scafe.cream"

    invoke-static/range {v19 .. v19}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "black"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 60
    const/4 v5, 0x1

    .line 64
    :goto_1
    const-string v19, "darkTheme"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mDarkTheme:Z

    .line 65
    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "defaultTheme :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", mDarkTheme :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mDarkTheme:Z

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/sec/clipboard/util/ClipboardProcText;->getImgFileNameFormHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 71
    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 76
    :cond_0
    :goto_2
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_1

    .line 78
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v19, :cond_1

    const-string v19, "ClipboardServiceEx"

    const-string v20, "getFirstImage : FileName is empty."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_1
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_2

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "firstImagePath = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_2
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_6

    const/16 v19, 0x0

    const/16 v20, 0x7

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    const-string v20, "file://"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v19

    if-nez v19, :cond_6

    .line 86
    const/16 v19, 0x7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 87
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_3

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getFilePathBitmap : Substring Filepath  - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :cond_3
    :goto_3
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_4

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "after firstImagePath = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    :cond_4
    const/16 v16, 0x0

    .line 118
    .local v16, "pastePath":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->getPackageName()Ljava/lang/String;

    move-result-object v19

    const-string v20, "sec_container_1."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_d

    .line 119
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "data/data1/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->getPackageName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/temp/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 123
    :goto_4
    const/4 v4, 0x0

    .line 124
    .local v4, "copyFile":Ljava/io/File;
    if-nez v7, :cond_e

    .line 125
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. received firstImagePath is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    .line 210
    .end local v4    # "copyFile":Ljava/io/File;
    .end local v16    # "pastePath":Ljava/lang/String;
    :goto_5
    return-void

    .line 45
    .end local v5    # "defaultTheme":Z
    .end local v7    # "firstImagePath":Ljava/lang/String;
    .end local v9    # "htmlPath":Ljava/lang/String;
    .end local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .end local v14    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    :catch_0
    move-exception v6

    .line 46
    .local v6, "e":Landroid/os/RemoteException;
    const-string v19, "ClipboardServiceEx"

    const-string v20, "setProcessForeground fail!"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 62
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v5    # "defaultTheme":Z
    .restart local v7    # "firstImagePath":Ljava/lang/String;
    .restart local v9    # "htmlPath":Ljava/lang/String;
    .restart local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .restart local v14    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 72
    :catch_1
    move-exception v6

    .line 73
    .local v6, "e":Ljava/lang/Exception;
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v19, :cond_0

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getFirstImage : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 88
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_6
    const-string v19, "storage/emulated/"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 90
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_3

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "directly use firstImagePath...getFilePathBitmap : Substring Filepath  - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 92
    :cond_7
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v19, :cond_8

    const-string v19, "ClipboardServiceEx"

    const-string v20, "This file type should be set directly to ClipboardExManager."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_8
    new-instance v13, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    invoke-direct {v13}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;-><init>()V

    .line 94
    .local v13, "mClipHtml":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    invoke-virtual {v13, v9}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->SetHTMLFragment(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_a

    .line 95
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v19, :cond_9

    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Error Copy Clipboard [SetHTMLFragment.SetHTMLFragment]"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_9
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 100
    :cond_a
    const/4 v3, 0x0

    .line 101
    .local v3, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mDarkTheme:Z

    move/from16 v19, v0

    if-eqz v19, :cond_c

    .line 102
    new-instance v3, Landroid/view/ContextThemeWrapper;

    .end local v3    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x1030128

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 107
    .restart local v3    # "context":Landroid/content/Context;
    :goto_6
    invoke-virtual {v12, v3, v13}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v19

    if-nez v19, :cond_b

    .line 108
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v19, :cond_b

    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Error Copy Clipboard [setData]"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 104
    :cond_c
    new-instance v3, Landroid/view/ContextThemeWrapper;

    .end local v3    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x103012b

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v3    # "context":Landroid/content/Context;
    goto :goto_6

    .line 121
    .end local v3    # "context":Landroid/content/Context;
    .end local v13    # "mClipHtml":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    .restart local v16    # "pastePath":Ljava/lang/String;
    :cond_d
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->getFilesDir()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_4

    .line 130
    .restart local v4    # "copyFile":Ljava/io/File;
    :cond_e
    new-instance v4, Ljava/io/File;

    .end local v4    # "copyFile":Ljava/io/File;
    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 132
    .restart local v4    # "copyFile":Ljava/io/File;
    new-instance v15, Ljava/io/File;

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 135
    .local v15, "pasteFile":Ljava/io/File;
    invoke-virtual {v14, v4}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v19

    if-nez v19, :cond_f

    .line 136
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. No target file"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 142
    :cond_f
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 144
    .local v18, "savePath":Ljava/lang/String;
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_10

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "savePath = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_10
    invoke-virtual {v14, v15}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 151
    invoke-virtual {v15}, Ljava/io/File;->isDirectory()Z

    move-result v19

    if-eqz v19, :cond_12

    invoke-virtual {v15}, Ljava/io/File;->canRead()Z

    move-result v19

    if-eqz v19, :cond_12

    invoke-virtual {v15}, Ljava/io/File;->canWrite()Z

    move-result v19

    if-eqz v19, :cond_12

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_12

    .line 152
    invoke-virtual {v15}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 153
    .local v11, "list":[Ljava/io/File;
    if-eqz v11, :cond_12

    array-length v0, v11

    move/from16 v19, v0

    const/16 v20, 0x6

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_12

    .line 154
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_7
    array-length v0, v11

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v10, v0, :cond_12

    .line 155
    aget-object v19, v11, v10

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->isFile()Z

    move-result v19

    if-eqz v19, :cond_11

    aget-object v19, v11, v10

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_11

    .line 156
    aget-object v19, v11, v10

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z

    .line 154
    :cond_11
    add-int/lit8 v10, v10, 0x1

    goto :goto_7

    .line 163
    .end local v10    # "i":I
    .end local v11    # "list":[Ljava/io/File;
    :cond_12
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v19

    if-nez v19, :cond_18

    .line 164
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v14, v4, v0}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v17

    .line 166
    .local v17, "saveFinished":Z
    if-eqz v17, :cond_17

    .line 167
    new-instance v13, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;

    invoke-direct {v13}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;-><init>()V

    .line 171
    .restart local v13    # "mClipHtml":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    invoke-virtual {v13, v9}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->SetHTMLFragment(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_13

    .line 172
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Error Copy Clipboard [SetHTMLFragment.SetHTMLFragment]"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 177
    :cond_13
    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->SetFirstImgPath(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_14

    .line 178
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Error Copy Clipboard [SetHTMLFragment.SetFirstImgPath]"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 183
    :cond_14
    sget-boolean v19, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v19, :cond_15

    const-string v19, "ClipboardServiceEx"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "ClipboardCopyHtmlAppService. mClipHtml.GetFirstImgPath() :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v13}, Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;->GetFirstImgPath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_15
    const/4 v3, 0x0

    .line 186
    .restart local v3    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mDarkTheme:Z

    move/from16 v19, v0

    if-eqz v19, :cond_16

    .line 187
    new-instance v3, Landroid/view/ContextThemeWrapper;

    .end local v3    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x1030128

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 192
    .restart local v3    # "context":Landroid/content/Context;
    :goto_8
    invoke-virtual {v12, v3, v13}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v19

    if-nez v19, :cond_18

    .line 193
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Error Copy Clipboard [setData]"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 189
    :cond_16
    new-instance v3, Landroid/view/ContextThemeWrapper;

    .end local v3    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x103012b

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v3    # "context":Landroid/content/Context;
    goto :goto_8

    .line 198
    .end local v3    # "context":Landroid/content/Context;
    .end local v13    # "mClipHtml":Landroid/sec/clipboard/data/list/ClipboardDataHTMLFragment;
    :cond_17
    const-string v19, "ClipboardServiceEx"

    const-string v20, "ClipboardCopyHtmlAppService. Failed to copy file"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    .end local v17    # "saveFinished":Z
    :cond_18
    :try_start_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v19

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-interface {v2, v8, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 204
    const-string v19, "ClipboardServiceEx"

    const-string v20, "setProcessForeground!"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 209
    :goto_9
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyHtmlAppService;->stopSelf(I)V

    goto/16 :goto_5

    .line 205
    :catch_2
    move-exception v6

    .line 206
    .local v6, "e":Landroid/os/RemoteException;
    const-string v19, "ClipboardServiceEx"

    const-string v20, "setProcessForeground fail!"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_9
.end method
