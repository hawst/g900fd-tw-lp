.class public Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;
.super Landroid/app/Service;
.source "ClipboardCaptureFileAppService.java"


# static fields
.field private static mToast:Landroid/widget/Toast;


# instance fields
.field final LIMIT_FILE_SIZE:I

.field mContext:Landroid/content/Context;

.field private mDarkTheme:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    .line 35
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->LIMIT_FILE_SIZE:I

    return-void
.end method

.method private CompareFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 35
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "dest"    # Ljava/lang/String;

    .prologue
    .line 40
    const/4 v14, 0x5

    .line 41
    .local v14, "compareCount":I
    const/16 v15, 0x80

    .line 43
    .local v15, "compareSize":I
    const/4 v2, 0x0

    .line 44
    .local v2, "Result":Z
    const/16 v21, 0x0

    .line 45
    .local v21, "fisSrc":Ljava/io/FileInputStream;
    const/16 v19, 0x0

    .line 48
    .local v19, "fisDest":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v22, Ljava/io/FileInputStream;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .local v22, "fisSrc":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v20, Ljava/io/FileInputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a

    .line 55
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .local v20, "fisDest":Ljava/io/FileInputStream;
    const/16 v18, 0x0

    .line 58
    .local v18, "fileSize":I
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v18, v0

    .line 60
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v18

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 61
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 62
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 165
    .end local v2    # "Result":Z
    .end local v18    # "fileSize":I
    .local v3, "Result":I
    :goto_0
    return v3

    .line 50
    .end local v3    # "Result":I
    .restart local v2    # "Result":Z
    :catch_0
    move-exception v16

    .line 51
    .local v16, "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v3, v2

    .line 52
    .restart local v3    # "Result":I
    goto :goto_0

    .line 66
    .end local v3    # "Result":I
    .end local v16    # "e":Ljava/io/FileNotFoundException;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v18    # "fileSize":I
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_1
    move-exception v16

    .line 67
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .line 70
    :try_start_3
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 71
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_2
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 75
    .restart local v3    # "Result":I
    goto :goto_0

    .line 72
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_2
    move-exception v17

    .line 73
    .local v17, "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 78
    .end local v16    # "e":Ljava/io/IOException;
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_0
    const/16 v31, 0x1

    move/from16 v0, v18

    move/from16 v1, v31

    if-ge v0, v1, :cond_1

    .line 80
    :try_start_4
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 81
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_3
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 85
    .restart local v3    # "Result":I
    goto :goto_0

    .line 82
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_3
    move-exception v17

    .line 83
    .restart local v17    # "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 88
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_1
    const/16 v31, 0x80

    move/from16 v0, v18

    move/from16 v1, v31

    if-gt v0, v1, :cond_2

    move/from16 v9, v18

    .line 90
    .local v9, "buffSize":I
    :goto_4
    const/16 v25, 0x0

    .line 92
    .local v25, "iCnt":I
    div-int v30, v18, v9

    .line 93
    .local v30, "tmp":I
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-lt v0, v1, :cond_3

    const/16 v25, 0x5

    .line 96
    :goto_5
    const/16 v26, 0x0

    .line 98
    .local v26, "offset":I
    mul-int v31, v9, v25

    sub-int v30, v18, v31

    .line 99
    div-int v26, v30, v25

    .line 102
    const/4 v4, 0x1

    .line 104
    .local v4, "bSameData":Z
    const/4 v7, 0x0

    .line 105
    .local v7, "bisSrc":Ljava/io/BufferedInputStream;
    const/4 v5, 0x0

    .line 108
    .local v5, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v27, 0x0

    .line 109
    .local v27, "position":I
    :try_start_5
    new-array v0, v9, [B

    move-object/from16 v29, v0

    .line 110
    .local v29, "readSrcData":[B
    new-array v0, v9, [B

    move-object/from16 v28, v0

    .line 112
    .local v28, "readDestData":[B
    new-instance v8, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v22

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 113
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .local v8, "bisSrc":Ljava/io/BufferedInputStream;
    :try_start_6
    new-instance v6, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 118
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .local v6, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v23, 0x0

    .local v23, "i1":I
    :goto_6
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    if-eqz v4, :cond_6

    .line 119
    const/16 v31, 0x0

    :try_start_7
    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v8, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v6, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    .line 121
    const/16 v24, 0x0

    .local v24, "i2":I
    :goto_7
    move/from16 v0, v24

    if-ge v0, v9, :cond_5

    if-eqz v4, :cond_5

    .line 122
    aget-byte v31, v29, v24

    aget-byte v32, v28, v24

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_4

    const/4 v4, 0x1

    .line 121
    :goto_8
    add-int/lit8 v24, v24, 0x1

    goto :goto_7

    .line 88
    .end local v4    # "bSameData":Z
    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v23    # "i1":I
    .end local v24    # "i2":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    :cond_2
    const/16 v9, 0x80

    goto :goto_4

    .restart local v9    # "buffSize":I
    .restart local v25    # "iCnt":I
    .restart local v30    # "tmp":I
    :cond_3
    move/from16 v25, v30

    .line 93
    goto :goto_5

    .line 122
    .restart local v4    # "bSameData":Z
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    .restart local v24    # "i2":I
    .restart local v26    # "offset":I
    .restart local v27    # "position":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_4
    const/4 v4, 0x0

    goto :goto_8

    .line 126
    .end local v24    # "i2":I
    :cond_5
    add-int v31, v9, v26

    add-int v27, v27, v31

    .line 128
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v8, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J

    move-result-wide v12

    .line 129
    .local v12, "checkingSkipSrc":J
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-wide v10

    .line 131
    .local v10, "checkingSkipDest":J
    const-wide/16 v32, 0x0

    cmp-long v31, v12, v32

    if-lez v31, :cond_6

    const-wide/16 v32, 0x0

    cmp-long v31, v10, v32

    if-gtz v31, :cond_c

    .line 143
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :cond_6
    if-eqz v8, :cond_7

    .line 144
    :try_start_8
    invoke-virtual {v8}, Ljava/io/BufferedInputStream;->close()V

    .line 146
    :cond_7
    if-eqz v6, :cond_8

    .line 147
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    .line 149
    :cond_8
    if-eqz v22, :cond_9

    .line 150
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 152
    :cond_9
    if-eqz v20, :cond_a

    .line 153
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :cond_a
    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 163
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    :cond_b
    :goto_9
    move v2, v4

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 165
    .restart local v3    # "Result":I
    goto/16 :goto_0

    .line 118
    .end local v3    # "Result":I
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v10    # "checkingSkipDest":J
    .restart local v12    # "checkingSkipSrc":J
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v23    # "i1":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_c
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_6

    .line 155
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :catch_4
    move-exception v16

    .line 156
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardCopyFileAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 160
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_9

    .line 135
    .end local v16    # "e":Ljava/io/IOException;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    :catch_5
    move-exception v16

    .line 136
    .restart local v16    # "e":Ljava/io/IOException;
    :goto_a
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 137
    const/4 v4, 0x0

    .line 143
    if-eqz v7, :cond_d

    .line 144
    :try_start_a
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 146
    :cond_d
    if-eqz v5, :cond_e

    .line 147
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 149
    :cond_e
    if-eqz v22, :cond_f

    .line 150
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 152
    :cond_f
    if-eqz v20, :cond_b

    .line 153
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_9

    .line 155
    :catch_6
    move-exception v16

    .line 156
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardCopyFileAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 142
    .end local v16    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v31

    .line 143
    :goto_b
    if-eqz v7, :cond_10

    .line 144
    :try_start_b
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 146
    :cond_10
    if-eqz v5, :cond_11

    .line 147
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 149
    :cond_11
    if-eqz v22, :cond_12

    .line 150
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 152
    :cond_12
    if-eqz v20, :cond_13

    .line 153
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 159
    :cond_13
    :goto_c
    throw v31

    .line 155
    :catch_7
    move-exception v16

    .line 156
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "ClipboardCopyFileAppService. close : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 142
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :catchall_1
    move-exception v31

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catchall_2
    move-exception v31

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .line 135
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    :catch_8
    move-exception v16

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catch_9
    move-exception v16

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .line 50
    .end local v4    # "bSameData":Z
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v18    # "fileSize":I
    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .end local v23    # "i1":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    :catch_a
    move-exception v16

    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 421
    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 422
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 423
    iget-boolean v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    if-eqz v1, :cond_0

    .line 424
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 425
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    .line 433
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 437
    :goto_1
    return-void

    .line 427
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 428
    .restart local v0    # "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    goto :goto_0

    .line 431
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 435
    :cond_2
    const-string v1, "ClipboardServiceEx"

    const-string v2, "ClipboardCopyFileAppService. mContext is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 416
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 35
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 175
    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 177
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    .line 179
    new-instance v18, Landroid/os/Binder;

    invoke-direct/range {v18 .. v18}, Landroid/os/Binder;-><init>()V

    .line 180
    .local v18, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    .line 182
    .local v3, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v32

    const/16 v33, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-interface {v3, v0, v1, v2}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 183
    const-string v32, "ClipboardServiceEx"

    const-string v33, "setProcessForeground"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    const-string v32, "clipboardEx"

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/sec/clipboard/ClipboardExManager;

    .line 194
    .local v21, "mCM":Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v23

    .line 195
    .local v23, "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    const-string v32, "copyPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 196
    .local v7, "copyPath":Ljava/lang/String;
    const-string v32, "extraDataPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 198
    .local v13, "extraDataPath":Ljava/lang/String;
    const/4 v10, 0x1

    .line 199
    .local v10, "defaultTheme":Z
    const-string v32, "ro.build.scafe.cream"

    invoke-static/range {v32 .. v32}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    const-string v33, "black"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v32

    if-eqz v32, :cond_0

    .line 200
    const/4 v10, 0x1

    .line 204
    :goto_1
    const-string v32, "darkTheme"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v32

    move/from16 v0, v32

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    .line 205
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "defaultTheme :"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, ", mDarkTheme :"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    move/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/16 v27, 0x0

    .line 209
    .local v27, "pastePath":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v32

    const-string v33, "sec_container_1."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_1

    .line 210
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "data/data1/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/temp/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 215
    :goto_2
    const/4 v6, 0x0

    .line 216
    .local v6, "copyFile":Ljava/io/File;
    if-nez v7, :cond_2

    .line 217
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. received copyPath is null"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    .line 411
    :goto_3
    return-void

    .line 184
    .end local v6    # "copyFile":Ljava/io/File;
    .end local v7    # "copyPath":Ljava/lang/String;
    .end local v10    # "defaultTheme":Z
    .end local v13    # "extraDataPath":Ljava/lang/String;
    .end local v21    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .end local v23    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .end local v27    # "pastePath":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 185
    .local v11, "e":Landroid/os/RemoteException;
    const-string v32, "ClipboardServiceEx"

    const-string v33, "setProcessForeground fail!"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 202
    .end local v11    # "e":Landroid/os/RemoteException;
    .restart local v7    # "copyPath":Ljava/lang/String;
    .restart local v10    # "defaultTheme":Z
    .restart local v13    # "extraDataPath":Ljava/lang/String;
    .restart local v21    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .restart local v23    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    :cond_0
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 212
    .restart local v27    # "pastePath":Ljava/lang/String;
    :cond_1
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->getFilesDir()Ljava/io/File;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    goto :goto_2

    .line 222
    .restart local v6    # "copyFile":Ljava/io/File;
    :cond_2
    new-instance v6, Ljava/io/File;

    .end local v6    # "copyFile":Ljava/io/File;
    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 224
    .restart local v6    # "copyFile":Ljava/io/File;
    new-instance v26, Ljava/io/File;

    invoke-direct/range {v26 .. v27}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v26, "pasteFile":Ljava/io/File;
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-nez v32, :cond_3

    .line 228
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. No target file"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto :goto_3

    .line 234
    :cond_3
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 237
    .local v29, "savePath":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 241
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->isDirectory()Z

    move-result v32

    if-eqz v32, :cond_5

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->canRead()Z

    move-result v32

    if-eqz v32, :cond_5

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->canWrite()Z

    move-result v32

    if-eqz v32, :cond_5

    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_5

    .line 242
    invoke-virtual/range {v26 .. v26}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v20

    .line 243
    .local v20, "list":[Ljava/io/File;
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v32, v0

    const/16 v33, 0x6

    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_5

    .line 244
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_4
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v32, v0

    move/from16 v0, v19

    move/from16 v1, v32

    if-ge v0, v1, :cond_5

    .line 245
    aget-object v32, v20, v19

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->isFile()Z

    move-result v32

    if-eqz v32, :cond_4

    aget-object v32, v20, v19

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->exists()Z

    move-result v32

    if-eqz v32, :cond_4

    .line 246
    aget-object v32, v20, v19

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->delete()Z

    .line 244
    :cond_4
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 253
    .end local v19    # "i":I
    .end local v20    # "list":[Ljava/io/File;
    :cond_5
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-nez v32, :cond_11

    .line 254
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v6, v1}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v28

    .line 257
    .local v28, "saveFinished":Z
    if-eqz v28, :cond_10

    .line 258
    new-instance v22, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    invoke-direct/range {v22 .. v22}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;-><init>()V

    .line 262
    .local v22, "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->SetBitmapPath(Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_6

    .line 263
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Error Copy Clipboard [SetBitmap]"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 268
    :cond_6
    const/4 v4, 0x0

    .line 269
    .local v4, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    move/from16 v32, v0

    if-eqz v32, :cond_b

    .line 270
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x1030128

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 276
    .restart local v4    # "context":Landroid/content/Context;
    :goto_5
    const/4 v14, 0x0

    .line 277
    .local v14, "extraDataSavePath":Ljava/lang/String;
    if-eqz v13, :cond_7

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v32

    if-lez v32, :cond_7

    .line 278
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v12, "extraDataFile":Ljava/io/File;
    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-nez v32, :cond_c

    .line 280
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "ClipboardCopyFileAppService. No target file. extraDataPath :"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    .end local v12    # "extraDataFile":Ljava/io/File;
    :cond_7
    :goto_6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v24

    .line 302
    .local v24, "myUserId":I
    move/from16 v9, v24

    .line 305
    .local v9, "currentuser":I
    invoke-virtual/range {v21 .. v21}, Landroid/sec/clipboard/ClipboardExManager;->getPersonaId()I

    move-result v9

    .line 306
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "current user"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    move/from16 v0, v24

    if-eq v0, v9, :cond_f

    const/16 v32, 0x64

    move/from16 v0, v32

    if-ge v9, v0, :cond_8

    const/16 v32, 0x64

    move/from16 v0, v24

    move/from16 v1, v32

    if-lt v0, v1, :cond_f

    .line 317
    :cond_8
    new-instance v30, Ljava/io/File;

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 318
    .local v30, "savedfile":Ljava/io/File;
    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-eqz v32, :cond_9

    .line 319
    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->delete(Ljava/io/File;)V

    .line 321
    :cond_9
    if-eqz v14, :cond_a

    .line 322
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 323
    .local v16, "extrasavedfile":Ljava/io/File;
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-eqz v32, :cond_a

    .line 324
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->delete(Ljava/io/File;)V

    .line 404
    .end local v4    # "context":Landroid/content/Context;
    .end local v9    # "currentuser":I
    .end local v14    # "extraDataSavePath":Ljava/lang/String;
    .end local v16    # "extrasavedfile":Ljava/io/File;
    .end local v22    # "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    .end local v24    # "myUserId":I
    .end local v28    # "saveFinished":Z
    .end local v30    # "savedfile":Ljava/io/File;
    :cond_a
    :goto_7
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v32

    const/16 v33, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-interface {v3, v0, v1, v2}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 405
    const-string v32, "ClipboardServiceEx"

    const-string v33, "setProcessForeground!"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 410
    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 272
    .restart local v4    # "context":Landroid/content/Context;
    .restart local v22    # "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    .restart local v28    # "saveFinished":Z
    :cond_b
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x103012b

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v4    # "context":Landroid/content/Context;
    goto/16 :goto_5

    .line 282
    .restart local v12    # "extraDataFile":Ljava/io/File;
    .restart local v14    # "extraDataSavePath":Ljava/lang/String;
    :cond_c
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 283
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-nez v32, :cond_e

    .line 284
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v12, v1}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v17

    .line 285
    .local v17, "finished":Z
    if-eqz v17, :cond_d

    .line 286
    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->SetExtraDataPath(Ljava/lang/String;)Z

    .line 288
    const-string v15, ""

    .line 289
    .local v15, "extraFilePath":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->HasExtraData()Z

    move-result v32

    if-eqz v32, :cond_7

    .line 290
    invoke-virtual/range {v22 .. v22}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->GetExtraDataPath()Ljava/lang/String;

    goto/16 :goto_6

    .line 293
    .end local v15    # "extraFilePath":Ljava/lang/String;
    :cond_d
    const-string v32, "ClipboardServiceEx"

    const-string v33, "could not copy..."

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 296
    .end local v17    # "finished":Z
    :cond_e
    const-string v32, "ClipboardServiceEx"

    const-string v33, "extraDataSavePath file already exists..."

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 328
    .end local v12    # "extraDataFile":Ljava/io/File;
    .restart local v9    # "currentuser":I
    .restart local v24    # "myUserId":I
    :cond_f
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Landroid/sec/clipboard/ClipboardExManager;->setDataWithoutNoti(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 329
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Error Copy Clipboard [setData]"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 334
    .end local v4    # "context":Landroid/content/Context;
    .end local v9    # "currentuser":I
    .end local v14    # "extraDataSavePath":Ljava/lang/String;
    .end local v22    # "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    .end local v24    # "myUserId":I
    :cond_10
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Failed to copy file"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 338
    .end local v28    # "saveFinished":Z
    :cond_11
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v7, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->CompareFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_18

    .line 339
    new-instance v31, Ljava/util/StringTokenizer;

    const-string v32, "."

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    .local v31, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 341
    .local v5, "convertPath":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v25

    .line 343
    .local v25, "parentName":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v8

    .line 345
    .local v8, "count":I
    const/16 v32, 0x1

    move/from16 v0, v32

    if-le v8, v0, :cond_12

    .line 346
    const/16 v19, 0x1

    .restart local v19    # "i":I
    :goto_9
    move/from16 v0, v19

    if-ge v0, v8, :cond_12

    .line 347
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 346
    add-int/lit8 v19, v19, 0x1

    goto :goto_9

    .line 351
    .end local v19    # "i":I
    :cond_12
    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "-"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "."

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 355
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v32

    if-eqz v32, :cond_14

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    if-eqz v32, :cond_13

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    const v33, 0x7f030002

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->showToast(Ljava/lang/String;)V

    .line 358
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. The same file already exists"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 360
    :cond_13
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. mContext is null"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 363
    :cond_14
    new-instance v32, Ljava/io/File;

    move-object/from16 v0, v32

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v32

    invoke-virtual {v0, v6, v1}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v28

    .line 366
    .restart local v28    # "saveFinished":Z
    if-eqz v28, :cond_17

    .line 367
    new-instance v22, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;

    invoke-direct/range {v22 .. v22}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;-><init>()V

    .line 369
    .restart local v22    # "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/sec/clipboard/data/list/ClipboardDataBitmap;->SetBitmapPath(Ljava/lang/String;)Z

    move-result v32

    if-nez v32, :cond_15

    .line 370
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Error Copy Clipboard [SetBitmap]"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 375
    :cond_15
    const/4 v4, 0x0

    .line 376
    .restart local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mDarkTheme:Z

    move/from16 v32, v0

    if-eqz v32, :cond_16

    .line 377
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x1030128

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 382
    .restart local v4    # "context":Landroid/content/Context;
    :goto_a
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Landroid/sec/clipboard/ClipboardExManager;->setDataWithoutNoti(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v32

    if-nez v32, :cond_a

    .line 383
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Error Copy Clipboard [setData]"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 379
    :cond_16
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    const v33, 0x103012b

    move-object/from16 v0, v32

    move/from16 v1, v33

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v4    # "context":Landroid/content/Context;
    goto :goto_a

    .line 388
    .end local v4    # "context":Landroid/content/Context;
    .end local v22    # "mClipBmp":Landroid/sec/clipboard/data/list/ClipboardDataBitmap;
    :cond_17
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. Failed to copy file"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 394
    .end local v5    # "convertPath":Ljava/lang/String;
    .end local v8    # "count":I
    .end local v25    # "parentName":Ljava/lang/String;
    .end local v28    # "saveFinished":Z
    .end local v31    # "st":Ljava/util/StringTokenizer;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    if-eqz v32, :cond_19

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v32

    const v33, 0x7f030002

    invoke-virtual/range {v32 .. v33}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCaptureFileAppService;->showToast(Ljava/lang/String;)V

    .line 396
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. The same file already exists"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 398
    :cond_19
    const-string v32, "ClipboardServiceEx"

    const-string v33, "ClipboardCopyFileAppService. mContext is null"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 406
    :catch_1
    move-exception v11

    .line 407
    .restart local v11    # "e":Landroid/os/RemoteException;
    const-string v32, "ClipboardServiceEx"

    const-string v33, "setProcessForeground fail!!"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_8
.end method
