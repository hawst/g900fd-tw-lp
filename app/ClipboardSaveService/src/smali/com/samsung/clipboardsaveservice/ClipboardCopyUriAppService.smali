.class public Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;
.super Landroid/app/Service;
.source "ClipboardCopyUriAppService.java"


# static fields
.field private static mToast:Landroid/widget/Toast;


# instance fields
.field final LIMIT_FILE_SIZE:I

.field mContext:Landroid/content/Context;

.field private mDarkTheme:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    .line 36
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->LIMIT_FILE_SIZE:I

    return-void
.end method

.method private CompareFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 35
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "dest"    # Ljava/lang/String;

    .prologue
    .line 41
    const/4 v14, 0x5

    .line 42
    .local v14, "compareCount":I
    const/16 v15, 0x80

    .line 44
    .local v15, "compareSize":I
    const/4 v2, 0x0

    .line 45
    .local v2, "Result":Z
    const/16 v21, 0x0

    .line 46
    .local v21, "fisSrc":Ljava/io/FileInputStream;
    const/16 v19, 0x0

    .line 49
    .local v19, "fisDest":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v22, Ljava/io/FileInputStream;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .local v22, "fisSrc":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v20, Ljava/io/FileInputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a

    .line 56
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .local v20, "fisDest":Ljava/io/FileInputStream;
    const/16 v18, 0x0

    .line 58
    .local v18, "fileSize":I
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v18, v0

    .line 59
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v18

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 60
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 61
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 162
    .end local v2    # "Result":Z
    .end local v18    # "fileSize":I
    .local v3, "Result":I
    :goto_0
    return v3

    .line 51
    .end local v3    # "Result":I
    .restart local v2    # "Result":Z
    :catch_0
    move-exception v16

    .line 52
    .local v16, "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v3, v2

    .line 53
    .restart local v3    # "Result":I
    goto :goto_0

    .line 64
    .end local v3    # "Result":I
    .end local v16    # "e":Ljava/io/FileNotFoundException;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v18    # "fileSize":I
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_1
    move-exception v16

    .line 65
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .line 67
    :try_start_3
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 68
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_2
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 72
    .restart local v3    # "Result":I
    goto :goto_0

    .line 69
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_2
    move-exception v17

    .line 70
    .local v17, "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 75
    .end local v16    # "e":Ljava/io/IOException;
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_0
    const/16 v31, 0x1

    move/from16 v0, v18

    move/from16 v1, v31

    if-ge v0, v1, :cond_1

    .line 77
    :try_start_4
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 78
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_3
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 82
    .restart local v3    # "Result":I
    goto :goto_0

    .line 79
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_3
    move-exception v17

    .line 80
    .restart local v17    # "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 85
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_1
    const/16 v31, 0x80

    move/from16 v0, v18

    move/from16 v1, v31

    if-gt v0, v1, :cond_2

    move/from16 v9, v18

    .line 87
    .local v9, "buffSize":I
    :goto_4
    const/16 v25, 0x0

    .line 89
    .local v25, "iCnt":I
    div-int v30, v18, v9

    .line 90
    .local v30, "tmp":I
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-lt v0, v1, :cond_3

    const/16 v25, 0x5

    .line 93
    :goto_5
    const/16 v26, 0x0

    .line 95
    .local v26, "offset":I
    mul-int v31, v9, v25

    sub-int v30, v18, v31

    .line 96
    div-int v26, v30, v25

    .line 99
    const/4 v4, 0x1

    .line 101
    .local v4, "bSameData":Z
    const/4 v7, 0x0

    .line 102
    .local v7, "bisSrc":Ljava/io/BufferedInputStream;
    const/4 v5, 0x0

    .line 105
    .local v5, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v27, 0x0

    .line 106
    .local v27, "position":I
    :try_start_5
    new-array v0, v9, [B

    move-object/from16 v29, v0

    .line 107
    .local v29, "readSrcData":[B
    new-array v0, v9, [B

    move-object/from16 v28, v0

    .line 109
    .local v28, "readDestData":[B
    new-instance v8, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v22

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 110
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .local v8, "bisSrc":Ljava/io/BufferedInputStream;
    :try_start_6
    new-instance v6, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 116
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .local v6, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v23, 0x0

    .local v23, "i1":I
    :goto_6
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    if-eqz v4, :cond_6

    .line 117
    const/16 v31, 0x0

    :try_start_7
    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v8, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v6, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    .line 119
    const/16 v24, 0x0

    .local v24, "i2":I
    :goto_7
    move/from16 v0, v24

    if-ge v0, v9, :cond_5

    if-eqz v4, :cond_5

    .line 120
    aget-byte v31, v29, v24

    aget-byte v32, v28, v24

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_4

    const/4 v4, 0x1

    .line 119
    :goto_8
    add-int/lit8 v24, v24, 0x1

    goto :goto_7

    .line 85
    .end local v4    # "bSameData":Z
    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v23    # "i1":I
    .end local v24    # "i2":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    :cond_2
    const/16 v9, 0x80

    goto :goto_4

    .restart local v9    # "buffSize":I
    .restart local v25    # "iCnt":I
    .restart local v30    # "tmp":I
    :cond_3
    move/from16 v25, v30

    .line 90
    goto :goto_5

    .line 120
    .restart local v4    # "bSameData":Z
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    .restart local v24    # "i2":I
    .restart local v26    # "offset":I
    .restart local v27    # "position":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_4
    const/4 v4, 0x0

    goto :goto_8

    .line 124
    .end local v24    # "i2":I
    :cond_5
    add-int v31, v9, v26

    add-int v27, v27, v31

    .line 126
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v8, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J

    move-result-wide v12

    .line 127
    .local v12, "checkingSkipSrc":J
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-wide v10

    .line 129
    .local v10, "checkingSkipDest":J
    const-wide/16 v32, 0x0

    cmp-long v31, v12, v32

    if-lez v31, :cond_6

    const-wide/16 v32, 0x0

    cmp-long v31, v10, v32

    if-gtz v31, :cond_c

    .line 141
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :cond_6
    if-eqz v8, :cond_7

    .line 142
    :try_start_8
    invoke-virtual {v8}, Ljava/io/BufferedInputStream;->close()V

    .line 144
    :cond_7
    if-eqz v6, :cond_8

    .line 145
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    .line 147
    :cond_8
    if-eqz v22, :cond_9

    .line 148
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 150
    :cond_9
    if-eqz v20, :cond_a

    .line 151
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :cond_a
    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 160
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    :cond_b
    :goto_9
    move v2, v4

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 162
    .restart local v3    # "Result":I
    goto/16 :goto_0

    .line 116
    .end local v3    # "Result":I
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v10    # "checkingSkipDest":J
    .restart local v12    # "checkingSkipSrc":J
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v23    # "i1":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_c
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_6

    .line 153
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :catch_4
    move-exception v16

    .line 154
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardCopyUriAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 157
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_9

    .line 133
    .end local v16    # "e":Ljava/io/IOException;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    :catch_5
    move-exception v16

    .line 134
    .restart local v16    # "e":Ljava/io/IOException;
    :goto_a
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 135
    const/4 v4, 0x0

    .line 141
    if-eqz v7, :cond_d

    .line 142
    :try_start_a
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 144
    :cond_d
    if-eqz v5, :cond_e

    .line 145
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 147
    :cond_e
    if-eqz v22, :cond_f

    .line 148
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 150
    :cond_f
    if-eqz v20, :cond_b

    .line 151
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_9

    .line 153
    :catch_6
    move-exception v16

    .line 154
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardCopyUriAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 140
    .end local v16    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v31

    .line 141
    :goto_b
    if-eqz v7, :cond_10

    .line 142
    :try_start_b
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 144
    :cond_10
    if-eqz v5, :cond_11

    .line 145
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 147
    :cond_11
    if-eqz v22, :cond_12

    .line 148
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 150
    :cond_12
    if-eqz v20, :cond_13

    .line 151
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 156
    :cond_13
    :goto_c
    throw v31

    .line 153
    :catch_7
    move-exception v16

    .line 154
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "ClipboardCopyUriAppService. close : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 140
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :catchall_1
    move-exception v31

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catchall_2
    move-exception v31

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .line 133
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    :catch_8
    move-exception v16

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catch_9
    move-exception v16

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .line 51
    .end local v4    # "bSameData":Z
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v18    # "fileSize":I
    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .end local v23    # "i1":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    :catch_a
    move-exception v16

    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 370
    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 371
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 372
    iget-boolean v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    if-eqz v1, :cond_0

    .line 373
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 374
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    .line 382
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 386
    :goto_1
    return-void

    .line 376
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 377
    .restart local v0    # "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    goto :goto_0

    .line 380
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 384
    :cond_2
    const-string v1, "ClipboardServiceEx"

    const-string v2, "ClipboardCopyUriAppService. mContext is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 365
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 27
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 170
    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 172
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    .line 175
    const-string v24, "clipboardEx"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/sec/clipboard/ClipboardExManager;

    .line 178
    .local v12, "mCM":Landroid/sec/clipboard/ClipboardExManager;
    new-instance v9, Landroid/os/Binder;

    invoke-direct {v9}, Landroid/os/Binder;-><init>()V

    .line 179
    .local v9, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    .line 181
    .local v3, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v24

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v3, v9, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 182
    const-string v24, "ClipboardServiceEx"

    const-string v25, "setProcessForeground"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v14

    .line 189
    .local v14, "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    const-string v24, "uriPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 190
    .local v23, "uriPath":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 191
    .local v21, "uri":Landroid/net/Uri;
    invoke-virtual/range {v21 .. v21}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v23

    .line 193
    const/4 v7, 0x1

    .line 194
    .local v7, "defaultTheme":Z
    const-string v24, "ro.build.scafe.cream"

    invoke-static/range {v24 .. v24}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "black"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 195
    const/4 v7, 0x1

    .line 199
    :goto_1
    const-string v24, "darkTheme"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    .line 200
    const-string v24, "ClipboardServiceEx"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "defaultTheme :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ", mDarkTheme :"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const/16 v17, 0x0

    .line 205
    .local v17, "pastePath":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->getPackageName()Ljava/lang/String;

    move-result-object v24

    const-string v25, "sec_container_1."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 206
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "data/data1/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->getPackageName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/temp/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 211
    :goto_2
    const/16 v22, 0x0

    .line 212
    .local v22, "uriFile":Ljava/io/File;
    if-nez v23, :cond_2

    .line 213
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. received uriPath is null"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    .line 360
    :goto_3
    return-void

    .line 183
    .end local v7    # "defaultTheme":Z
    .end local v14    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .end local v17    # "pastePath":Ljava/lang/String;
    .end local v21    # "uri":Landroid/net/Uri;
    .end local v22    # "uriFile":Ljava/io/File;
    .end local v23    # "uriPath":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 184
    .local v8, "e":Landroid/os/RemoteException;
    const-string v24, "ClipboardServiceEx"

    const-string v25, "setProcessForeground fail!"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 197
    .end local v8    # "e":Landroid/os/RemoteException;
    .restart local v7    # "defaultTheme":Z
    .restart local v14    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .restart local v21    # "uri":Landroid/net/Uri;
    .restart local v23    # "uriPath":Ljava/lang/String;
    :cond_0
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 208
    .restart local v17    # "pastePath":Ljava/lang/String;
    :cond_1
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->getFilesDir()Ljava/io/File;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto :goto_2

    .line 217
    .restart local v22    # "uriFile":Ljava/io/File;
    :cond_2
    new-instance v22, Ljava/io/File;

    .end local v22    # "uriFile":Ljava/io/File;
    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .restart local v22    # "uriFile":Ljava/io/File;
    new-instance v16, Ljava/io/File;

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .local v16, "pasteFile":Ljava/io/File;
    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v24

    if-nez v24, :cond_3

    .line 223
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. No target file"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto :goto_3

    .line 229
    :cond_3
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 232
    .local v19, "savePath":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 237
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->isDirectory()Z

    move-result v24

    if-eqz v24, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->canRead()Z

    move-result v24

    if-eqz v24, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->canWrite()Z

    move-result v24

    if-eqz v24, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_5

    .line 238
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 239
    .local v11, "list":[Ljava/io/File;
    if-eqz v11, :cond_5

    array-length v0, v11

    move/from16 v24, v0

    const/16 v25, 0x6

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_5

    .line 240
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4
    array-length v0, v11

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v10, v0, :cond_5

    .line 241
    aget-object v24, v11, v10

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->isFile()Z

    move-result v24

    if-eqz v24, :cond_4

    aget-object v24, v11, v10

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_4

    .line 242
    aget-object v24, v11, v10

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    .line 240
    :cond_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 249
    .end local v10    # "i":I
    .end local v11    # "list":[Ljava/io/File;
    :cond_5
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v24

    if-nez v24, :cond_b

    .line 250
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v14, v0, v1}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v18

    .line 252
    .local v18, "saveFinished":Z
    if-eqz v18, :cond_9

    .line 253
    new-instance v13, Landroid/sec/clipboard/data/list/ClipboardDataUri;

    invoke-direct {v13}, Landroid/sec/clipboard/data/list/ClipboardDataUri;-><init>()V

    .line 256
    .local v13, "mClipUri":Landroid/sec/clipboard/data/list/ClipboardDataUri;
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->SetUri(Landroid/net/Uri;)Z

    move-result v24

    if-nez v24, :cond_6

    .line 257
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [SetUri]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 261
    :cond_6
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->setPreviewImgPath(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_7

    .line 262
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [setPreviewImgPath]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 267
    :cond_7
    const/4 v4, 0x0

    .line 268
    .local v4, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    move/from16 v24, v0

    if-eqz v24, :cond_8

    .line 269
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x1030128

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 274
    .restart local v4    # "context":Landroid/content/Context;
    :goto_5
    invoke-virtual {v12, v4, v13}, Landroid/sec/clipboard/ClipboardExManager;->setDataWithoutSendingOrginalClipboard(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v24

    if-nez v24, :cond_a

    .line 275
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [setDataWithoutSendingOrginalClipboard]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 271
    :cond_8
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x103012b

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v4    # "context":Landroid/content/Context;
    goto :goto_5

    .line 280
    .end local v4    # "context":Landroid/content/Context;
    .end local v13    # "mClipUri":Landroid/sec/clipboard/data/list/ClipboardDataUri;
    :cond_9
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Failed to copy file"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    .end local v18    # "saveFinished":Z
    :cond_a
    :goto_6
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v24

    const/16 v25, 0x0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v3, v9, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 354
    const-string v24, "ClipboardServiceEx"

    const-string v25, "setProcessForeground!"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 359
    :goto_7
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 284
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->CompareFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_13

    .line 285
    new-instance v20, Ljava/util/StringTokenizer;

    const-string v24, "."

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    .local v20, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    .line 287
    .local v5, "convertPath":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    .line 289
    .local v15, "parentName":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v6

    .line 291
    .local v6, "count":I
    const/16 v24, 0x1

    move/from16 v0, v24

    if-le v6, v0, :cond_c

    .line 292
    const/4 v10, 0x1

    .restart local v10    # "i":I
    :goto_8
    if-ge v10, v6, :cond_c

    .line 293
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 292
    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    .line 297
    .end local v10    # "i":I
    :cond_c
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "-"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "."

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 301
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    if-eqz v24, :cond_d

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f030002

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->showToast(Ljava/lang/String;)V

    .line 304
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. The same file already exists"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 306
    :cond_d
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. mContext is null"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 309
    :cond_e
    new-instance v24, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v14, v0, v1}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v18

    .line 311
    .restart local v18    # "saveFinished":Z
    if-eqz v18, :cond_12

    .line 312
    new-instance v13, Landroid/sec/clipboard/data/list/ClipboardDataUri;

    invoke-direct {v13}, Landroid/sec/clipboard/data/list/ClipboardDataUri;-><init>()V

    .line 314
    .restart local v13    # "mClipUri":Landroid/sec/clipboard/data/list/ClipboardDataUri;
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->SetUri(Landroid/net/Uri;)Z

    move-result v24

    if-nez v24, :cond_f

    .line 315
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [SetUri]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 319
    :cond_f
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/data/list/ClipboardDataUri;->setPreviewImgPath(Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_10

    .line 320
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [setPreviewImgPath]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 325
    :cond_10
    const/4 v4, 0x0

    .line 326
    .restart local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mDarkTheme:Z

    move/from16 v24, v0

    if-eqz v24, :cond_11

    .line 327
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x1030128

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 332
    .restart local v4    # "context":Landroid/content/Context;
    :goto_9
    invoke-virtual {v12, v4, v13}, Landroid/sec/clipboard/ClipboardExManager;->setDataWithoutSendingOrginalClipboard(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v24

    if-nez v24, :cond_a

    .line 333
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Error Copy Clipboard [setDataWithoutSendingOrginalClipboard]"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 329
    :cond_11
    new-instance v4, Landroid/view/ContextThemeWrapper;

    .end local v4    # "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x103012b

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .restart local v4    # "context":Landroid/content/Context;
    goto :goto_9

    .line 338
    .end local v4    # "context":Landroid/content/Context;
    .end local v13    # "mClipUri":Landroid/sec/clipboard/data/list/ClipboardDataUri;
    :cond_12
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. Failed to copy file"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 344
    .end local v5    # "convertPath":Ljava/lang/String;
    .end local v6    # "count":I
    .end local v15    # "parentName":Ljava/lang/String;
    .end local v18    # "saveFinished":Z
    .end local v20    # "st":Ljava/util/StringTokenizer;
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    if-eqz v24, :cond_14

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    const v25, 0x7f030002

    invoke-virtual/range {v24 .. v25}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyUriAppService;->showToast(Ljava/lang/String;)V

    .line 346
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. The same file already exists"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 348
    :cond_14
    const-string v24, "ClipboardServiceEx"

    const-string v25, "ClipboardCopyUriAppService. mContext is null"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 355
    :catch_1
    move-exception v8

    .line 356
    .restart local v8    # "e":Landroid/os/RemoteException;
    const-string v24, "ClipboardServiceEx"

    const-string v25, "setProcessForeground fail!"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_7
.end method
