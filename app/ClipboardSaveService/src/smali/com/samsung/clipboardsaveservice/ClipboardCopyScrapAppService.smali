.class public Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;
.super Landroid/app/Service;
.source "ClipboardCopyScrapAppService.java"


# instance fields
.field mContext:Landroid/content/Context;

.field mFileHelper:Landroid/sec/clipboard/util/FileHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    .line 38
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mFileHelper:Landroid/sec/clipboard/util/FileHelper;

    return-void
.end method

.method private convertByteToInt([B)I
    .locals 6
    .param p1, "b"    # [B

    .prologue
    .line 137
    const/4 v4, 0x0

    aget-byte v4, p1, v4

    and-int/lit16 v0, v4, 0xff

    .line 138
    .local v0, "s1":I
    const/4 v4, 0x1

    aget-byte v4, p1, v4

    and-int/lit16 v1, v4, 0xff

    .line 139
    .local v1, "s2":I
    const/4 v4, 0x2

    aget-byte v4, p1, v4

    and-int/lit16 v2, v4, 0xff

    .line 140
    .local v2, "s3":I
    const/4 v4, 0x3

    aget-byte v4, p1, v4

    and-int/lit16 v3, v4, 0xff

    .line 141
    .local v3, "s4":I
    shl-int/lit8 v4, v0, 0x18

    shl-int/lit8 v5, v1, 0x10

    add-int/2addr v4, v5

    shl-int/lit8 v5, v2, 0x8

    add-int/2addr v4, v5

    shl-int/lit8 v5, v3, 0x0

    add-int/2addr v4, v5

    return v4
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 11
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dst"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    const/4 v7, 0x0

    .line 43
    .local v7, "inStream":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 44
    .local v9, "outStream":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 45
    .local v1, "inChannel":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 48
    .local v6, "outChannel":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .end local v7    # "inStream":Ljava/io/FileInputStream;
    .local v8, "inStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 51
    .end local v9    # "outStream":Ljava/io/FileOutputStream;
    .local v10, "outStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 52
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 54
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    .line 56
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 57
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    .line 59
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 60
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v9, v10

    .end local v10    # "outStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 67
    .end local v8    # "inStream":Ljava/io/FileInputStream;
    .restart local v7    # "inStream":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "ex":Ljava/io/IOException;
    :goto_1
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 63
    :cond_1
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 64
    :cond_2
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 65
    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    goto :goto_0

    .line 61
    .end local v0    # "ex":Ljava/io/IOException;
    .end local v7    # "inStream":Ljava/io/FileInputStream;
    .restart local v8    # "inStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "inStream":Ljava/io/FileInputStream;
    .restart local v7    # "inStream":Ljava/io/FileInputStream;
    goto :goto_1

    .end local v7    # "inStream":Ljava/io/FileInputStream;
    .end local v9    # "outStream":Ljava/io/FileOutputStream;
    .restart local v8    # "inStream":Ljava/io/FileInputStream;
    .restart local v10    # "outStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v0

    move-object v9, v10

    .end local v10    # "outStream":Ljava/io/FileOutputStream;
    .restart local v9    # "outStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "inStream":Ljava/io/FileInputStream;
    .restart local v7    # "inStream":Ljava/io/FileInputStream;
    goto :goto_1
.end method


# virtual methods
.method public copyScrapMetaTextfileinLocalPath(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 11
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 104
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    sget-object v10, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/scrap"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 105
    .local v4, "imageDir":Ljava/lang/String;
    const/4 v5, 0x0

    .line 108
    .local v5, "png_path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v0, "dirFile":Ljava/io/File;
    iget-object v8, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mFileHelper:Landroid/sec/clipboard/util/FileHelper;

    invoke-virtual {v8, v0}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 112
    const/4 v8, 0x1

    if-eq p2, v8, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    move-object v6, v5

    .line 133
    .end local v5    # "png_path":Ljava/lang/String;
    .local v6, "png_path":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 116
    .end local v6    # "png_path":Ljava/lang/String;
    .restart local v5    # "png_path":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 118
    .local v3, "file_name":Ljava/lang/StringBuffer;
    invoke-static {}, Landroid/sec/clipboard/util/StringHelper;->getUniqueString()Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 120
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".SC"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 122
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v7, "srcFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    .local v1, "dstFile":Ljava/io/File;
    :try_start_0
    invoke-direct {p0, v7, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v6, v5

    .line 133
    .end local v5    # "png_path":Ljava/lang/String;
    .restart local v6    # "png_path":Ljava/lang/String;
    goto :goto_0

    .line 128
    .end local v6    # "png_path":Ljava/lang/String;
    .restart local v5    # "png_path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 130
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public copyScrapPngfileinLocalPath(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 11
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 70
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    sget-object v10, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/scrap"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "imageDir":Ljava/lang/String;
    const/4 v5, 0x0

    .line 74
    .local v5, "png_path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "dirFile":Ljava/io/File;
    iget-object v8, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mFileHelper:Landroid/sec/clipboard/util/FileHelper;

    invoke-virtual {v8, v0}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 78
    const/4 v8, 0x1

    if-eq p2, v8, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    move-object v6, v5

    .line 100
    .end local v5    # "png_path":Ljava/lang/String;
    .local v6, "png_path":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 82
    .end local v6    # "png_path":Ljava/lang/String;
    .restart local v5    # "png_path":Ljava/lang/String;
    :cond_0
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 84
    .local v3, "file_name":Ljava/lang/StringBuffer;
    invoke-static {}, Landroid/sec/clipboard/util/StringHelper;->getUniqueString()Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 85
    const-string v8, ".png"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 87
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 89
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 90
    .local v7, "srcFile":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .local v1, "dstFile":Ljava/io/File;
    :try_start_0
    invoke-direct {p0, v7, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v6, v5

    .line 100
    .end local v5    # "png_path":Ljava/lang/String;
    .restart local v6    # "png_path":Ljava/lang/String;
    goto :goto_0

    .line 95
    .end local v6    # "png_path":Ljava/lang/String;
    .restart local v5    # "png_path":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 97
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public deleteSmartClipfolder()V
    .locals 8

    .prologue
    .line 212
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/scrap"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "imageDir":Ljava/lang/String;
    const/4 v2, 0x0

    .line 215
    .local v2, "dir_path":Ljava/lang/String;
    move-object v2, v4

    .line 217
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 220
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "children":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 222
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v0

    if-ge v3, v5, :cond_0

    .line 223
    new-instance v5, Ljava/io/File;

    aget-object v6, v0, v3

    invoke-direct {v5, v1, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 222
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "children":[Ljava/lang/String;
    .end local v3    # "i":I
    :cond_0
    return-void
.end method

.method public getRawBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "bmp_path"    # Ljava/lang/String;

    .prologue
    .line 145
    const/4 v8, 0x0

    .line 146
    .local v8, "readBmp":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    .line 148
    .local v3, "fileOpen":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    .end local v3    # "fileOpen":Ljava/io/FileInputStream;
    .local v4, "fileOpen":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 151
    const/4 v10, 0x4

    new-array v9, v10, [B

    .line 152
    .local v9, "width":[B
    const/4 v10, 0x4

    new-array v5, v10, [B

    .line 154
    .local v5, "height":[B
    invoke-virtual {v4, v9}, Ljava/io/FileInputStream;->read([B)I

    .line 155
    invoke-virtual {v4, v5}, Ljava/io/FileInputStream;->read([B)I

    .line 157
    invoke-direct {p0, v9}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->convertByteToInt([B)I

    move-result v6

    .line 158
    .local v6, "intWidth":I
    invoke-direct {p0, v5}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->convertByteToInt([B)I

    move-result v7

    .line 160
    .local v7, "intheight":I
    mul-int v10, v6, v7

    mul-int/lit8 v10, v10, 0x4

    new-array v1, v10, [B

    .line 161
    .local v1, "data":[B
    sget-boolean v10, Landroid/sec/clipboard/data/ClipboardDefine;->DEBUG:Z

    if-eqz v10, :cond_0

    const-string v10, "ClipboardServiceEx"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Load Image Width : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " , height : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    invoke-virtual {v4, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_0

    .line 165
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 166
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 167
    invoke-virtual {v8, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v3, v4

    .line 173
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "data":[B
    .end local v4    # "fileOpen":Ljava/io/FileInputStream;
    .end local v5    # "height":[B
    .end local v6    # "intWidth":I
    .end local v7    # "intheight":I
    .end local v9    # "width":[B
    .restart local v3    # "fileOpen":Ljava/io/FileInputStream;
    :goto_0
    return-object v8

    .line 168
    :catch_0
    move-exception v2

    .line 169
    .local v2, "e":Ljava/io/IOException;
    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 170
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    :goto_2
    throw v10

    .end local v3    # "fileOpen":Ljava/io/FileInputStream;
    .restart local v4    # "fileOpen":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v3, v4

    .end local v4    # "fileOpen":Ljava/io/FileInputStream;
    .restart local v3    # "fileOpen":Ljava/io/FileInputStream;
    goto :goto_2

    .line 168
    .end local v3    # "fileOpen":Ljava/io/FileInputStream;
    .restart local v4    # "fileOpen":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "fileOpen":Ljava/io/FileInputStream;
    .restart local v3    # "fileOpen":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public getScrapPngImagePathfromBmpRawData(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 10
    .param p1, "bmp_image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 178
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    sget-object v9, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/scrap"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "imageDir":Ljava/lang/String;
    const/4 v5, 0x0

    .line 182
    .local v5, "png_path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "dirFile":Ljava/io/File;
    iget-object v7, p0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mFileHelper:Landroid/sec/clipboard/util/FileHelper;

    invoke-virtual {v7, v0}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 186
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 188
    .local v2, "file_name":Ljava/lang/StringBuffer;
    invoke-static {}, Landroid/sec/clipboard/util/StringHelper;->getUniqueString()Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 189
    const-string v7, ".png"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 191
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 193
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v6, "pngfile":Ljava/io/File;
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 197
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 198
    .local v4, "out":Ljava/io/FileOutputStream;
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {p1, v7, v8, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 199
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 208
    .end local v4    # "out":Ljava/io/FileOutputStream;
    :goto_0
    return-object v5

    .line 200
    :catch_0
    move-exception v1

    .line 202
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 203
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 205
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 358
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 24
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 234
    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 236
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    .line 238
    new-instance v10, Landroid/os/Binder;

    invoke-direct {v10}, Landroid/os/Binder;-><init>()V

    .line 239
    .local v10, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    .line 241
    .local v3, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-interface {v3, v10, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 242
    const-string v22, "ClipboardServiceEx"

    const-string v23, "setProcessForeground"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    const-string v22, "clipboardEx"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/sec/clipboard/ClipboardExManager;

    .line 252
    .local v12, "mCM":Landroid/sec/clipboard/ClipboardExManager;
    invoke-virtual {v12}, Landroid/sec/clipboard/ClipboardExManager;->getScrapDataSize()I

    move-result v22

    if-nez v22, :cond_0

    .line 253
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->deleteSmartClipfolder()V

    .line 256
    :cond_0
    const-string v22, "bitmapPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 258
    .local v5, "bmp_path":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v22

    const-string v23, "repository"

    invoke-virtual/range {v22 .. v23}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    .line 260
    .local v20, "smartClip":Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 261
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v22, "previewImageRect"

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Landroid/graphics/Rect;

    .line 262
    .local v13, "mCropRect":Landroid/graphics/Rect;
    if-nez v13, :cond_1

    .line 263
    new-instance v13, Landroid/graphics/Rect;

    .end local v13    # "mCropRect":Landroid/graphics/Rect;
    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    .line 266
    .restart local v13    # "mCropRect":Landroid/graphics/Rect;
    :cond_1
    if-eqz v20, :cond_b

    .line 267
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getCapturedImageFilePath()Ljava/lang/String;

    move-result-object v19

    .line 269
    .local v19, "original_path":Ljava/lang/String;
    if-nez v19, :cond_6

    .line 270
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to get png File Path try to get Bitmap path"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    if-eqz v5, :cond_5

    .line 272
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->getRawBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 274
    .local v4, "bm":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_4

    .line 275
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->getScrapPngImagePathfromBmpRawData(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v18

    .line 277
    .local v18, "newfilePath":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getCapturedImageFileStyle()I

    move-result v11

    .line 279
    .local v11, "imagestyle":I
    if-eqz v18, :cond_2

    .line 280
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v11}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->setCapturedImage(Ljava/lang/String;I)V

    .line 283
    :cond_2
    new-instance v7, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;

    invoke-direct {v7}, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;-><init>()V

    .line 284
    .local v7, "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;->SetSmartClip(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Z

    .line 286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v12, v0, v7}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 287
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScrapAppService. success to copy file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    .end local v4    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    .end local v11    # "imagestyle":I
    .end local v18    # "newfilePath":Ljava/lang/String;
    .end local v19    # "original_path":Ljava/lang/String;
    :goto_1
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v22

    const/16 v23, 0x0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-interface {v3, v10, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 347
    const-string v22, "ClipboardServiceEx"

    const-string v23, "setProcessForeground!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 352
    :goto_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->stopSelf(I)V

    .line 353
    return-void

    .line 243
    .end local v5    # "bmp_path":Ljava/lang/String;
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .end local v13    # "mCropRect":Landroid/graphics/Rect;
    .end local v20    # "smartClip":Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;
    :catch_0
    move-exception v8

    .line 244
    .local v8, "e":Landroid/os/RemoteException;
    const-string v22, "ClipboardServiceEx"

    const-string v23, "setProcessForeground fail!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 289
    .end local v8    # "e":Landroid/os/RemoteException;
    .restart local v4    # "bm":Landroid/graphics/Bitmap;
    .restart local v5    # "bmp_path":Ljava/lang/String;
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v7    # "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    .restart local v11    # "imagestyle":I
    .restart local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .restart local v13    # "mCropRect":Landroid/graphics/Rect;
    .restart local v18    # "newfilePath":Ljava/lang/String;
    .restart local v19    # "original_path":Ljava/lang/String;
    .restart local v20    # "smartClip":Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;
    :cond_3
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to copy file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 293
    .end local v7    # "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    .end local v11    # "imagestyle":I
    .end local v18    # "newfilePath":Ljava/lang/String;
    :cond_4
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to make bitmap path"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 296
    .end local v4    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to get bitmap path"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 299
    :cond_6
    invoke-virtual {v12}, Landroid/sec/clipboard/ClipboardExManager;->showUIFloatingIcon()V

    .line 300
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v12, v13, v0}, Landroid/sec/clipboard/ClipboardExManager;->sendCropRect(Landroid/graphics/Rect;Z)V

    .line 301
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScrapAppService. success to copy file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getCapturedImageFileStyle()I

    move-result v11

    .line 305
    .restart local v11    # "imagestyle":I
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->copyScrapPngfileinLocalPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v18

    .line 307
    .restart local v18    # "newfilePath":Ljava/lang/String;
    if-eqz v18, :cond_7

    .line 308
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v11}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->setCapturedImage(Ljava/lang/String;I)V

    .line 312
    :cond_7
    const-string v22, "file_path_html"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getMetaTag(Ljava/lang/String;)Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTagArray;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/smartclip/SmartClipMetaTagArrayImpl;

    .line 314
    .local v16, "metaTagTextFile":Lcom/samsung/android/smartclip/SmartClipMetaTagArrayImpl;
    if-eqz v16, :cond_9

    .line 316
    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/smartclip/SmartClipMetaTagArrayImpl;->size()I

    move-result v15

    .line 318
    .local v15, "metaTagSize":I
    const/4 v9, 0x0

    .local v9, "fileIndex":I
    :goto_3
    if-ge v9, v15, :cond_9

    .line 319
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/samsung/android/smartclip/SmartClipMetaTagArrayImpl;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;

    .line 321
    .local v21, "tempSlookFile":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    if-eqz v21, :cond_8

    .line 322
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;->getValue()Ljava/lang/String;

    move-result-object v14

    .line 323
    .local v14, "metaFilePath":Ljava/lang/String;
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v22

    if-lez v22, :cond_8

    .line 324
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v14, v1}, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->copyScrapMetaTextfileinLocalPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v17

    .line 326
    .local v17, "newMetaFilePath":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;->setValue(Ljava/lang/String;)V

    .line 318
    .end local v14    # "metaFilePath":Ljava/lang/String;
    .end local v17    # "newMetaFilePath":Ljava/lang/String;
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 332
    .end local v9    # "fileIndex":I
    .end local v15    # "metaTagSize":I
    .end local v21    # "tempSlookFile":Lcom/samsung/android/sdk/look/smartclip/SlookSmartClipMetaTag;
    :cond_9
    new-instance v7, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;

    invoke-direct {v7}, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;-><init>()V

    .line 333
    .restart local v7    # "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;->SetSmartClip(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Z

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardCopyScrapAppService;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v12, v0, v7}, Landroid/sec/clipboard/ClipboardExManager;->setData(Landroid/content/Context;Landroid/sec/clipboard/data/ClipboardData;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 336
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScrapAppService. success to copy file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 338
    :cond_a
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to copy file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 342
    .end local v7    # "cbCmartClip":Landroid/sec/clipboard/data/list/ClipboardDataSmartClip;
    .end local v11    # "imagestyle":I
    .end local v16    # "metaTagTextFile":Lcom/samsung/android/smartclip/SmartClipMetaTagArrayImpl;
    .end local v18    # "newfilePath":Ljava/lang/String;
    .end local v19    # "original_path":Ljava/lang/String;
    :cond_b
    const-string v22, "ClipboardServiceEx"

    const-string v23, "ClipboardCopyScarpService. Failed to getScrap Data"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 348
    :catch_1
    move-exception v8

    .line 349
    .restart local v8    # "e":Landroid/os/RemoteException;
    const-string v22, "ClipboardServiceEx"

    const-string v23, "setProcessForeground fail!"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_2
.end method
