.class public Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;
.super Landroid/app/Service;
.source "ClipboardSaveFileAppService.java"


# static fields
.field private static mToast:Landroid/widget/Toast;


# instance fields
.field mContext:Landroid/content/Context;

.field private mDarkTheme:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mDarkTheme:Z

    return-void
.end method

.method private CompareFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 35
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "dest"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v14, 0x5

    .line 37
    .local v14, "compareCount":I
    const/16 v15, 0x80

    .line 39
    .local v15, "compareSize":I
    const/4 v2, 0x0

    .line 40
    .local v2, "Result":Z
    const/16 v21, 0x0

    .line 41
    .local v21, "fisSrc":Ljava/io/FileInputStream;
    const/16 v19, 0x0

    .line 44
    .local v19, "fisDest":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v22, Ljava/io/FileInputStream;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .local v22, "fisSrc":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v20, Ljava/io/FileInputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a

    .line 51
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .local v20, "fisDest":Ljava/io/FileInputStream;
    const/16 v18, 0x0

    .line 54
    .local v18, "fileSize":I
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v18, v0

    .line 56
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v32

    move-wide/from16 v0, v32

    long-to-int v0, v0

    move/from16 v31, v0

    move/from16 v0, v18

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    .line 57
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 58
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 161
    .end local v2    # "Result":Z
    .end local v18    # "fileSize":I
    .local v3, "Result":I
    :goto_0
    return v3

    .line 46
    .end local v3    # "Result":I
    .restart local v2    # "Result":Z
    :catch_0
    move-exception v16

    .line 47
    .local v16, "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move v3, v2

    .line 48
    .restart local v3    # "Result":I
    goto :goto_0

    .line 62
    .end local v3    # "Result":I
    .end local v16    # "e":Ljava/io/FileNotFoundException;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v18    # "fileSize":I
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_1
    move-exception v16

    .line 63
    .local v16, "e":Ljava/io/IOException;
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .line 66
    :try_start_3
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 67
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_2
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 71
    .restart local v3    # "Result":I
    goto :goto_0

    .line 68
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_2
    move-exception v17

    .line 69
    .local v17, "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 74
    .end local v16    # "e":Ljava/io/IOException;
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_0
    const/16 v31, 0x1

    move/from16 v0, v18

    move/from16 v1, v31

    if-ge v0, v1, :cond_1

    .line 76
    :try_start_4
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 77
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :goto_3
    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 81
    .restart local v3    # "Result":I
    goto :goto_0

    .line 78
    .end local v3    # "Result":I
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    :catch_3
    move-exception v17

    .line 79
    .restart local v17    # "e1":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 84
    .end local v17    # "e1":Ljava/io/IOException;
    :cond_1
    const/16 v31, 0x80

    move/from16 v0, v18

    move/from16 v1, v31

    if-gt v0, v1, :cond_2

    move/from16 v9, v18

    .line 86
    .local v9, "buffSize":I
    :goto_4
    const/16 v25, 0x0

    .line 88
    .local v25, "iCnt":I
    div-int v30, v18, v9

    .line 89
    .local v30, "tmp":I
    const/16 v31, 0x5

    move/from16 v0, v30

    move/from16 v1, v31

    if-lt v0, v1, :cond_3

    const/16 v25, 0x5

    .line 92
    :goto_5
    const/16 v26, 0x0

    .line 94
    .local v26, "offset":I
    mul-int v31, v9, v25

    sub-int v30, v18, v31

    .line 95
    div-int v26, v30, v25

    .line 98
    const/4 v4, 0x1

    .line 100
    .local v4, "bSameData":Z
    const/4 v7, 0x0

    .line 101
    .local v7, "bisSrc":Ljava/io/BufferedInputStream;
    const/4 v5, 0x0

    .line 104
    .local v5, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v27, 0x0

    .line 105
    .local v27, "position":I
    :try_start_5
    new-array v0, v9, [B

    move-object/from16 v29, v0

    .line 106
    .local v29, "readSrcData":[B
    new-array v0, v9, [B

    move-object/from16 v28, v0

    .line 108
    .local v28, "readDestData":[B
    new-instance v8, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v22

    invoke-direct {v8, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 109
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .local v8, "bisSrc":Ljava/io/BufferedInputStream;
    :try_start_6
    new-instance v6, Ljava/io/BufferedInputStream;

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 114
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .local v6, "bisDest":Ljava/io/BufferedInputStream;
    const/16 v23, 0x0

    .local v23, "i1":I
    :goto_6
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    if-eqz v4, :cond_6

    .line 115
    const/16 v31, 0x0

    :try_start_7
    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v8, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v6, v0, v1, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v31

    if-lez v31, :cond_5

    .line 117
    const/16 v24, 0x0

    .local v24, "i2":I
    :goto_7
    move/from16 v0, v24

    if-ge v0, v9, :cond_5

    if-eqz v4, :cond_5

    .line 118
    aget-byte v31, v29, v24

    aget-byte v32, v28, v24

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_4

    const/4 v4, 0x1

    .line 117
    :goto_8
    add-int/lit8 v24, v24, 0x1

    goto :goto_7

    .line 84
    .end local v4    # "bSameData":Z
    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v23    # "i1":I
    .end local v24    # "i2":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    :cond_2
    const/16 v9, 0x80

    goto :goto_4

    .restart local v9    # "buffSize":I
    .restart local v25    # "iCnt":I
    .restart local v30    # "tmp":I
    :cond_3
    move/from16 v25, v30

    .line 89
    goto :goto_5

    .line 118
    .restart local v4    # "bSameData":Z
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    .restart local v24    # "i2":I
    .restart local v26    # "offset":I
    .restart local v27    # "position":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_4
    const/4 v4, 0x0

    goto :goto_8

    .line 122
    .end local v24    # "i2":I
    :cond_5
    add-int v31, v9, v26

    add-int v27, v27, v31

    .line 124
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v8, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J

    move-result-wide v12

    .line 125
    .local v12, "checkingSkipSrc":J
    move/from16 v0, v27

    int-to-long v0, v0

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/io/BufferedInputStream;->skip(J)J
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-wide v10

    .line 127
    .local v10, "checkingSkipDest":J
    const-wide/16 v32, 0x0

    cmp-long v31, v12, v32

    if-lez v31, :cond_6

    const-wide/16 v32, 0x0

    cmp-long v31, v10, v32

    if-gtz v31, :cond_c

    .line 139
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :cond_6
    if-eqz v8, :cond_7

    .line 140
    :try_start_8
    invoke-virtual {v8}, Ljava/io/BufferedInputStream;->close()V

    .line 142
    :cond_7
    if-eqz v6, :cond_8

    .line 143
    invoke-virtual {v6}, Ljava/io/BufferedInputStream;->close()V

    .line 145
    :cond_8
    if-eqz v22, :cond_9

    .line 146
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 148
    :cond_9
    if-eqz v20, :cond_a

    .line 149
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :cond_a
    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 159
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    :cond_b
    :goto_9
    move v2, v4

    move-object/from16 v19, v20

    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    move v3, v2

    .line 161
    .restart local v3    # "Result":I
    goto/16 :goto_0

    .line 114
    .end local v3    # "Result":I
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v19    # "fisDest":Ljava/io/FileInputStream;
    .end local v21    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v10    # "checkingSkipDest":J
    .restart local v12    # "checkingSkipSrc":J
    .restart local v20    # "fisDest":Ljava/io/FileInputStream;
    .restart local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v23    # "i1":I
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :cond_c
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_6

    .line 151
    .end local v10    # "checkingSkipDest":J
    .end local v12    # "checkingSkipSrc":J
    :catch_4
    move-exception v16

    .line 152
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardSaveFileAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .line 156
    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_9

    .line 131
    .end local v16    # "e":Ljava/io/IOException;
    .end local v23    # "i1":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    :catch_5
    move-exception v16

    .line 132
    .restart local v16    # "e":Ljava/io/IOException;
    :goto_a
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 133
    const/4 v4, 0x0

    .line 139
    if-eqz v7, :cond_d

    .line 140
    :try_start_a
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 142
    :cond_d
    if-eqz v5, :cond_e

    .line 143
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 145
    :cond_e
    if-eqz v22, :cond_f

    .line 146
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 148
    :cond_f
    if-eqz v20, :cond_b

    .line 149
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_9

    .line 151
    :catch_6
    move-exception v16

    .line 152
    const-string v31, "ClipboardServiceEx"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "ClipboardSaveFileAppService. close : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 138
    .end local v16    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v31

    .line 139
    :goto_b
    if-eqz v7, :cond_10

    .line 140
    :try_start_b
    invoke-virtual {v7}, Ljava/io/BufferedInputStream;->close()V

    .line 142
    :cond_10
    if-eqz v5, :cond_11

    .line 143
    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V

    .line 145
    :cond_11
    if-eqz v22, :cond_12

    .line 146
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V

    .line 148
    :cond_12
    if-eqz v20, :cond_13

    .line 149
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 155
    :cond_13
    :goto_c
    throw v31

    .line 151
    :catch_7
    move-exception v16

    .line 152
    .restart local v16    # "e":Ljava/io/IOException;
    const-string v32, "ClipboardServiceEx"

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v34, "ClipboardSaveFileAppService. close : "

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 138
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v16    # "e":Ljava/io/IOException;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v28    # "readDestData":[B
    .restart local v29    # "readSrcData":[B
    :catchall_1
    move-exception v31

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catchall_2
    move-exception v31

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_b

    .line 131
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v23    # "i1":I
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    :catch_8
    move-exception v16

    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v23    # "i1":I
    :catch_9
    move-exception v16

    move-object v5, v6

    .end local v6    # "bisDest":Ljava/io/BufferedInputStream;
    .restart local v5    # "bisDest":Ljava/io/BufferedInputStream;
    move-object v7, v8

    .end local v8    # "bisSrc":Ljava/io/BufferedInputStream;
    .restart local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    goto :goto_a

    .line 46
    .end local v4    # "bSameData":Z
    .end local v5    # "bisDest":Ljava/io/BufferedInputStream;
    .end local v7    # "bisSrc":Ljava/io/BufferedInputStream;
    .end local v9    # "buffSize":I
    .end local v18    # "fileSize":I
    .end local v20    # "fisDest":Ljava/io/FileInputStream;
    .end local v23    # "i1":I
    .end local v25    # "iCnt":I
    .end local v26    # "offset":I
    .end local v27    # "position":I
    .end local v28    # "readDestData":[B
    .end local v29    # "readSrcData":[B
    .end local v30    # "tmp":I
    .restart local v19    # "fisDest":Ljava/io/FileInputStream;
    :catch_a
    move-exception v16

    move-object/from16 v21, v22

    .end local v22    # "fisSrc":Ljava/io/FileInputStream;
    .restart local v21    # "fisSrc":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 317
    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 318
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 319
    iget-boolean v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mDarkTheme:Z

    if-eqz v1, :cond_0

    .line 320
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 321
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    .line 329
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 333
    :goto_1
    return-void

    .line 323
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 324
    .restart local v0    # "context":Landroid/content/Context;
    invoke-static {v0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    goto :goto_0

    .line 327
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    sget-object v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 331
    :cond_2
    const-string v1, "ClipboardServiceEx"

    const-string v2, "ClipboardSaveFileAppService. mContext is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 312
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 24
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 169
    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 171
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    .line 173
    new-instance v10, Landroid/os/Binder;

    invoke-direct {v10}, Landroid/os/Binder;-><init>()V

    .line 174
    .local v10, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    .line 176
    .local v3, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v3, v10, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 177
    const-string v21, "ClipboardServiceEx"

    const-string v22, "setProcessForeground"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_0
    const-string v21, "clipboardEx"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/sec/clipboard/ClipboardExManager;

    .line 187
    .local v12, "mCM":Landroid/sec/clipboard/ClipboardExManager;
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v13

    .line 188
    .local v13, "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    const-string v21, "copyPath"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 189
    .local v6, "copyPath":Ljava/lang/String;
    const/16 v17, 0x0

    .line 190
    .local v17, "pastePath":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const-string v22, "sec_container_1."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 191
    const-string v21, "pastePath"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 196
    :goto_1
    sget-boolean v21, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v21, :cond_0

    const-string v21, "ClipboardServiceEx"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "ClipboardSaveFileAppService. Copy Path : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    sget-boolean v21, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v21, :cond_1

    const-string v21, "ClipboardServiceEx"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "ClipboardSaveFileAppService. Paste Path : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_1
    const/4 v8, 0x1

    .line 200
    .local v8, "defaultTheme":Z
    const-string v21, "ro.build.scafe.cream"

    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "black"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 201
    const/4 v8, 0x1

    .line 205
    :goto_2
    const-string v21, "darkTheme"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mDarkTheme:Z

    .line 206
    const-string v21, "ClipboardServiceEx"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "defaultTheme :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", mDarkTheme :"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mDarkTheme:Z

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    .local v5, "copyFile":Ljava/io/File;
    new-instance v16, Ljava/io/File;

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 212
    .local v16, "pasteFile":Ljava/io/File;
    invoke-virtual {v13, v5}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v21

    if-nez v21, :cond_4

    .line 213
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. No target file"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->stopSelf(I)V

    .line 307
    :goto_3
    return-void

    .line 178
    .end local v5    # "copyFile":Ljava/io/File;
    .end local v6    # "copyPath":Ljava/lang/String;
    .end local v8    # "defaultTheme":Z
    .end local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .end local v13    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .end local v16    # "pasteFile":Ljava/io/File;
    .end local v17    # "pastePath":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 179
    .local v9, "e":Landroid/os/RemoteException;
    const-string v21, "ClipboardServiceEx"

    const-string v22, "setProcessForeground fail!"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 193
    .end local v9    # "e":Landroid/os/RemoteException;
    .restart local v6    # "copyPath":Ljava/lang/String;
    .restart local v12    # "mCM":Landroid/sec/clipboard/ClipboardExManager;
    .restart local v13    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .restart local v17    # "pastePath":Ljava/lang/String;
    :cond_2
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Landroid/sec/clipboard/util/FileHelper;->getSDCardPath()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/Clipboard/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1

    .line 203
    .restart local v8    # "defaultTheme":Z
    :cond_3
    const/4 v8, 0x0

    goto :goto_2

    .line 218
    .restart local v5    # "copyFile":Ljava/io/File;
    .restart local v16    # "pasteFile":Ljava/io/File;
    :cond_4
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 219
    .local v19, "savePath":Ljava/lang/String;
    sget-boolean v21, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v21, :cond_5

    const-string v21, "ClipboardServiceEx"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "ClipboardSaveFileAppService. savePath : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_5
    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/util/FileHelper;->makeDir(Ljava/io/File;)V

    .line 224
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v21

    if-nez v21, :cond_9

    .line 225
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v13, v5, v0}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v18

    .line 228
    .local v18, "saveFinished":Z
    if-eqz v18, :cond_8

    .line 229
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. save finished with pastePath"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f030001

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->showToast(Ljava/lang/String;)V

    .line 231
    const/4 v14, 0x0

    .line 232
    .local v14, "mediaIntent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const-string v22, "sec_container_1."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 233
    new-instance v14, Landroid/content/Intent;

    .end local v14    # "mediaIntent":Landroid/content/Intent;
    const-string v21, "sec_container_1.android.intent.action.MEDIA_MOUNTED"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file://mnt_1/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 238
    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 299
    .end local v14    # "mediaIntent":Landroid/content/Intent;
    .end local v18    # "saveFinished":Z
    :cond_6
    :goto_5
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v21

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v3, v10, v0, v1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 300
    const-string v21, "ClipboardServiceEx"

    const-string v22, "setProcessForeground!"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 306
    :goto_6
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->stopSelf(I)V

    goto/16 :goto_3

    .line 235
    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    .restart local v18    # "saveFinished":Z
    :cond_7
    new-instance v14, Landroid/content/Intent;

    .end local v14    # "mediaIntent":Landroid/content/Intent;
    const-string v21, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file://"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    goto :goto_4

    .line 243
    .end local v14    # "mediaIntent":Landroid/content/Intent;
    :cond_8
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. Failed to save file"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 247
    .end local v18    # "saveFinished":Z
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v6, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->CompareFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-nez v21, :cond_f

    .line 248
    new-instance v20, Ljava/util/StringTokenizer;

    const-string v21, "."

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .local v20, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 250
    .local v4, "convertPath":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    .line 252
    .local v15, "parentName":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v7

    .line 254
    .local v7, "count":I
    const/16 v21, 0x1

    move/from16 v0, v21

    if-le v7, v0, :cond_a

    .line 255
    const/4 v11, 0x1

    .local v11, "i":I
    :goto_7
    if-ge v11, v7, :cond_a

    .line 256
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 255
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 260
    .end local v11    # "i":I
    :cond_a
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "-"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 262
    sget-boolean v21, Landroid/sec/clipboard/data/ClipboardDefine;->INFO_DEBUG:Z

    if-eqz v21, :cond_b

    const-string v21, "ClipboardServiceEx"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "ClipboardSaveFileAppService. convertPath = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_b
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 265
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. The same file already exists"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f030002

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 268
    :cond_c
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v13, v5, v0}, Landroid/sec/clipboard/util/FileHelper;->fileCopy(Ljava/io/File;Ljava/io/File;)Z

    move-result v18

    .line 271
    .restart local v18    # "saveFinished":Z
    if-eqz v18, :cond_e

    .line 272
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. save finished with convertPath"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f030001

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->showToast(Ljava/lang/String;)V

    .line 275
    const/4 v14, 0x0

    .line 276
    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v21

    const-string v22, "sec_container_1"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 277
    new-instance v14, Landroid/content/Intent;

    .end local v14    # "mediaIntent":Landroid/content/Intent;
    const-string v21, "sec_container_1.android.intent.action.MEDIA_MOUNTED"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file://mnt_1/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 282
    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    if-eqz v21, :cond_6

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_5

    .line 279
    :cond_d
    new-instance v14, Landroid/content/Intent;

    .end local v14    # "mediaIntent":Landroid/content/Intent;
    const-string v21, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "file://"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v14, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .restart local v14    # "mediaIntent":Landroid/content/Intent;
    goto :goto_8

    .line 287
    .end local v14    # "mediaIntent":Landroid/content/Intent;
    :cond_e
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. Failed to save file"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 293
    .end local v4    # "convertPath":Ljava/lang/String;
    .end local v7    # "count":I
    .end local v15    # "parentName":Ljava/lang/String;
    .end local v18    # "saveFinished":Z
    .end local v20    # "st":Ljava/util/StringTokenizer;
    :cond_f
    const-string v21, "ClipboardServiceEx"

    const-string v22, "ClipboardSaveFileAppService. The same file already exists"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f030002

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/clipboardsaveservice/ClipboardSaveFileAppService;->showToast(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 301
    :catch_1
    move-exception v9

    .line 302
    .restart local v9    # "e":Landroid/os/RemoteException;
    const-string v21, "ClipboardServiceEx"

    const-string v22, "setProcessForeground fail!"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-virtual {v9}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_6
.end method
