.class public Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;
.super Landroid/app/Service;
.source "ClipboardDeleteFileAppService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 33
    new-instance v6, Landroid/os/Binder;

    invoke-direct {v6}, Landroid/os/Binder;-><init>()V

    .line 34
    .local v6, "foregroundToken":Landroid/os/IBinder;
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 36
    .local v0, "am":Landroid/app/IActivityManager;
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v10

    const/4 v11, 0x1

    invoke-interface {v0, v6, v10, v11}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 37
    const-string v10, "ClipboardServiceEx"

    const-string v11, "setProcessForeground"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :goto_0
    invoke-static {}, Landroid/sec/clipboard/util/FileHelper;->getInstance()Landroid/sec/clipboard/util/FileHelper;

    move-result-object v7

    .line 44
    .local v7, "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    const-string v10, "deletePath"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    .local v2, "deletePath":Ljava/lang/String;
    const-string v10, "extraDataPath"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 48
    .local v5, "extraDataPath":Ljava/lang/String;
    const/4 v9, 0x0

    .line 49
    .local v9, "pastePath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "sec_container_1."

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 50
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "data/data1/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/temp/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 55
    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .local v1, "deleteFile":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .local v8, "pasteFile":Ljava/io/File;
    invoke-virtual {v7, v1}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 60
    const-string v10, "ClipboardServiceEx"

    const-string v11, "ClipboardDeleteFileAppService. No target file"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-virtual {p0, p2}, Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;->stopSelf(I)V

    .line 84
    :goto_2
    return-void

    .line 38
    .end local v1    # "deleteFile":Ljava/io/File;
    .end local v2    # "deletePath":Ljava/lang/String;
    .end local v5    # "extraDataPath":Ljava/lang/String;
    .end local v7    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .end local v8    # "pasteFile":Ljava/io/File;
    .end local v9    # "pastePath":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 39
    .local v3, "e":Landroid/os/RemoteException;
    const-string v10, "ClipboardServiceEx"

    const-string v11, "setProcessForeground fail!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v2    # "deletePath":Ljava/lang/String;
    .restart local v5    # "extraDataPath":Ljava/lang/String;
    .restart local v7    # "mFileHelper":Landroid/sec/clipboard/util/FileHelper;
    .restart local v9    # "pastePath":Ljava/lang/String;
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 64
    .restart local v1    # "deleteFile":Ljava/io/File;
    .restart local v8    # "pasteFile":Ljava/io/File;
    :cond_1
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v8}, Ljava/io/File;->canRead()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 65
    const-string v10, "ClipboardServiceEx"

    const-string v11, "ClipboardDeleteFileAppService. Temp file will be deleted"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {v7, v1}, Landroid/sec/clipboard/util/FileHelper;->delete(Ljava/io/File;)V

    .line 67
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 68
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v4, "extraDataFile":Ljava/io/File;
    invoke-virtual {v7, v4}, Landroid/sec/clipboard/util/FileHelper;->checkFile(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 70
    invoke-virtual {v7, v4}, Landroid/sec/clipboard/util/FileHelper;->delete(Ljava/io/File;)V

    .line 77
    .end local v4    # "extraDataFile":Ljava/io/File;
    :cond_2
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v10

    const/4 v11, 0x0

    invoke-interface {v0, v6, v10, v11}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 78
    const-string v10, "ClipboardServiceEx"

    const-string v11, "setProcessForeground!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 83
    :goto_3
    invoke-virtual {p0, p2}, Lcom/samsung/clipboardsaveservice/ClipboardDeleteFileAppService;->stopSelf(I)V

    goto :goto_2

    .line 79
    :catch_1
    move-exception v3

    .line 80
    .restart local v3    # "e":Landroid/os/RemoteException;
    const-string v10, "ClipboardServiceEx"

    const-string v11, "setProcessForeground fail!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3
.end method
