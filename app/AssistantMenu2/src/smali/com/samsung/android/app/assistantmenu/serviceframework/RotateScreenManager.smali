.class public Lcom/samsung/android/app/assistantmenu/serviceframework/RotateScreenManager;
.super Ljava/lang/Object;
.source "RotateScreenManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getScreenRotationMode(Landroid/content/ContentResolver;)I
    .locals 2
    .param p0, "mResovler"    # Landroid/content/ContentResolver;

    .prologue
    .line 25
    const-string v0, "user_rotation"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static isAutoScreenRotation(Landroid/content/ContentResolver;)Z
    .locals 3
    .param p0, "mResovler"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    const-string v2, "accelerometer_rotation"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 32
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static setAutoScreenRotation(Landroid/content/ContentResolver;Z)V
    .locals 2
    .param p0, "mResovler"    # Landroid/content/ContentResolver;
    .param p1, "isAuto"    # Z

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 37
    const-string v0, "accelerometer_rotation"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 40
    :goto_0
    return-void

    .line 39
    :cond_0
    const-string v0, "accelerometer_rotation"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public static setScreenRotationMode(Landroid/content/ContentResolver;I)V
    .locals 1
    .param p0, "mResovler"    # Landroid/content/ContentResolver;
    .param p1, "Rotationtype"    # I

    .prologue
    .line 21
    const-string v0, "user_rotation"

    invoke-static {p0, v0, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 22
    return-void
.end method
