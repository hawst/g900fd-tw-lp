.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;
.super Ljava/lang/Object;
.source "ZoomControlGridUI.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 458
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressChanged() :: progress = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fromUser = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    if-eqz p3, :cond_0

    .line 461
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I

    .line 463
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 467
    const-string v0, "ZoomControlGridUI"

    const-string v1, "onStartTrackingTouch()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 470
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 471
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 472
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x1

    .line 476
    const-string v0, "ZoomControlGridUI"

    const-string v1, "onStopTrackingTouch()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomWithBounce()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    .line 478
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 479
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 480
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 481
    return-void
.end method
