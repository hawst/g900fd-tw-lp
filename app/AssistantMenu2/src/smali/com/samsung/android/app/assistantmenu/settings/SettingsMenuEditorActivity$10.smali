.class Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;
.super Ljava/lang/Object;
.source "SettingsMenuEditorActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 532
    const-string v1, "SettingsMenuEditorActivity"

    const-string v2, "[c] #2 onAnimationEnd!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterUpper:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1900(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 534
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewAdapterLower:Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$2000(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->notifyDataSetChanged()V

    .line 536
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetOrientationPortraite()Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$2100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x4

    .line 539
    .local v0, "columnItemCount":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v1

    rem-int/2addr v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 542
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 545
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$10;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    const/4 v2, 0x0

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$300(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;ZI)V

    .line 550
    :cond_1
    return-void

    .line 536
    .end local v0    # "columnItemCount":I
    :cond_2
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 528
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 523
    return-void
.end method
