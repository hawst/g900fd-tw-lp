.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;
.super Ljava/lang/Object;
.source "ZoomControlGridUI.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MakeAnimationZoomPointer(F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->EndFakeZoomPointerAction()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    .line 1177
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1178
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1172
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v1, 0x0

    .line 1161
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1162
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1164
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1165
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1167
    :cond_1
    return-void
.end method
