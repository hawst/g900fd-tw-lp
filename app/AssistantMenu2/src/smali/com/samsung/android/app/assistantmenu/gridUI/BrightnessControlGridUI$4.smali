.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0

    .prologue
    .line 849
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v0, 0x0

    .line 853
    if-eqz p3, :cond_1

    .line 856
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 857
    sget-boolean v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-nez v1, :cond_0

    .line 858
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v1, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z

    .line 859
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 860
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 864
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 866
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v2, :cond_2

    :goto_0
    add-int/2addr v0, p2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v1, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$102(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;I)I

    .line 869
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)V

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 872
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 873
    return-void

    .line 866
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v0

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 878
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 879
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 880
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 885
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 886
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 887
    return-void
.end method
