.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;
.super Landroid/database/ContentObserver;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 397
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 400
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 401
    const-string v0, "AssistantMenuService"

    const-string v1, "stop AssistantMenuService by ContentObserver mKidsModeObserver"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mStartId:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->stopSelf(I)V

    .line 404
    :cond_0
    return-void
.end method
