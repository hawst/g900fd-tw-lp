.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;
.super Landroid/os/Handler;
.source "FingerMouseGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 143
    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_magnifier_size"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 145
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_value"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 147
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    aget v3, v3, v4

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/accessibility/AccessibilityManager;->enableMagnifier(IIF)V

    .line 148
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V

    .line 149
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 150
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$602(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 151
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$702(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_value"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/database/ContentObserver;

    move-result-object v3

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 159
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_magnifier_size"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/database/ContentObserver;

    move-result-object v3

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 164
    :cond_0
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
