.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;
.super Landroid/os/Handler;
.source "ZoomControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 0

    .prologue
    .line 402
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 405
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 407
    :pswitch_0
    const-string v0, "ZoomControlGridUI"

    const-string v1, "MSG_GRID_SEEKBAR_DISABLE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 409
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 410
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 411
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    goto :goto_0

    .line 416
    :pswitch_1
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MSG_GRID_SEEKBAR_ZOOM - mIsExpand : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mZoomRatio : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v2

    int-to-float v2, v2

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    goto :goto_0

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I

    move-result v2

    int-to-float v2, v2

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    goto :goto_0

    .line 427
    :pswitch_2
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MSG_GRID_SEEKBAR_SET_PROGRESS - SeekBar SetPostion : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 434
    :pswitch_3
    const-string v0, "ZoomControlGridUI"

    const-string v1, "MSG_GRID_SEEKBAR_ENABLE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 437
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 438
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 439
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 441
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->EndFakeZoomPointerAction()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    goto/16 :goto_0

    .line 405
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
