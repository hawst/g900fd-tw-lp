.class public Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;
.super Ljava/lang/Object;
.source "SettingsGridViewItem.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field private mIcon:I

.field private mName:Ljava/lang/String;


# direct methods
.method constructor <init>(ILcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;Ljava/lang/String;)V
    .locals 0
    .param p1, "icon"    # I
    .param p2, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mIcon:I

    .line 21
    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mName:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 23
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;)I
    .locals 2
    .param p1, "otherItem"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mName:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 6
    check-cast p1, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->compareTo(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;)I

    move-result v0

    return v0
.end method

.method getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-object v0
.end method

.method getIcon()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mIcon:I

    return v0
.end method

.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method setAct(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    .locals 0
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 47
    return-void
.end method

.method setIcon(I)V
    .locals 0
    .param p1, "icon"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mIcon:I

    .line 39
    return-void
.end method

.method setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->mName:Ljava/lang/String;

    .line 43
    return-void
.end method
