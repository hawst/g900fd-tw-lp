.class public final Lcom/samsung/android/app/assistantmenu/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BrightnessDivider:I = 0x7f0d0020

.field public static final BrightnessIcon:I = 0x7f0d001f

.field public static final RotateIcon:I = 0x7f0d0058

.field public static final ZoomIcon:I = 0x7f0d0071

.field public static final autoProgressImage:I = 0x7f0d0022

.field public static final auto_Check:I = 0x7f0d000f

.field public static final auto_CheckLayout:I = 0x7f0d000e

.field public static final auto_check_topmargin_layout:I = 0x7f0d0021

.field public static final auto_seekBar:I = 0x7f0d0018

.field public static final auto_seekbar_bg:I = 0x7f0d0017

.field public static final autobrightness_backgroud:I = 0x7f0d001d

.field public static final bright_title:I = 0x7f0d000c

.field public static final brightness_backgroud:I = 0x7f0d001c

.field public static final brightness_titlebar:I = 0x7f0d000b

.field public static final brightness_topmargin_layout:I = 0x7f0d001e

.field public static final checkBoxBrightness_button:I = 0x7f0d0010

.field public static final controlbar:I = 0x7f0d007f

.field public static final cornerTabButton:I = 0x7f0d0006

.field public static final currentpage:I = 0x7f0d0042

.field public static final cursor_cue_img:I = 0x7f0d0008

.field public static final cursor_cue_layout:I = 0x7f0d0007

.field public static final divider:I = 0x7f0d0043

.field public static final enable_single_tap_checkbox:I = 0x7f0d0079

.field public static final floatingUIBackgroundImageView:I = 0x7f0d0049

.field public static final floatingUIBtnNext:I = 0x7f0d0046

.field public static final floatingUIBtnPrev:I = 0x7f0d0045

.field public static final floatingUIFmBackground:I = 0x7f0d0028

.field public static final floatingUIHoverNext:I = 0x7f0d0048

.field public static final floatingUIHoverPrev:I = 0x7f0d0047

.field public static final floatingUILayoutBackground:I = 0x7f0d003e

.field public static final floatingUILayoutFloatingUI:I = 0x7f0d003f

.field public static final floatingUILayoutFm:I = 0x7f0d0027

.field public static final floatingUILayoutPager:I = 0x7f0d0040

.field public static final fm_pointer_speed:I = 0x7f0d007c

.field public static final fm_scrollDown:I = 0x7f0d002f

.field public static final fm_scrollUp:I = 0x7f0d002e

.field public static final fm_swipeLeft:I = 0x7f0d002c

.field public static final fm_swipeRight:I = 0x7f0d002d

.field public static final fmpointer_fast:I = 0x7f0d007b

.field public static final fmpointer_slow:I = 0x7f0d007a

.field public static final gridBrightnessControlLayout:I = 0x7f0d000a

.field public static final gridBrightnessControlLayoutBackground:I = 0x7f0d0009

.field public static final gridFmControlLayoutMain:I = 0x7f0d0024

.field public static final gridRotateButtonCenter:I = 0x7f0d0053

.field public static final gridRotateButtonLeft:I = 0x7f0d0051

.field public static final gridRotateButtonRight:I = 0x7f0d0055

.field public static final gridRotateCenterBar:I = 0x7f0d005a

.field public static final gridRotateControlTitle:I = 0x7f0d004e

.field public static final gridRotateImageCenter:I = 0x7f0d0054

.field public static final gridRotateImageLeft:I = 0x7f0d0052

.field public static final gridRotateImageRight:I = 0x7f0d0056

.field public static final gridRotateLayout:I = 0x7f0d004b

.field public static final gridRotateLeftBar:I = 0x7f0d0059

.field public static final gridRotateOutsideLayout:I = 0x7f0d0057

.field public static final gridRotateRightBar:I = 0x7f0d005b

.field public static final gridRotateScreen:I = 0x7f0d004c

.field public static final gridRotateStateControlLayoutBackground:I = 0x7f0d004a

.field public static final gridVolumeControlButtonLeft:I = 0x7f0d0037

.field public static final gridVolumeControlButtonMinus:I = 0x7f0d0038

.field public static final gridVolumeControlButtonMute:I = 0x7f0d0063

.field public static final gridVolumeControlButtonPlus:I = 0x7f0d003a

.field public static final gridVolumeControlButtonRight:I = 0x7f0d0039

.field public static final gridVolumeControlButtonSound:I = 0x7f0d0060

.field public static final gridVolumeControlButtonVibrate:I = 0x7f0d005d

.field public static final gridVolumeControlIcon:I = 0x7f0d003b

.field public static final gridVolumeControlLayout:I = 0x7f0d0032

.field public static final gridVolumeControlLayoutBackground:I = 0x7f0d0031

.field public static final gridVolumeControlMuteBar:I = 0x7f0d0066

.field public static final gridVolumeControlMuteIcon:I = 0x7f0d0064

.field public static final gridVolumeControlSoundBar:I = 0x7f0d0068

.field public static final gridVolumeControlSoundIcon:I = 0x7f0d0061

.field public static final gridVolumeControlTitle:I = 0x7f0d0034

.field public static final gridVolumeControlVibrateBar:I = 0x7f0d0067

.field public static final gridVolumeControlVibrateIcon:I = 0x7f0d005e

.field public static final gridZoomControlButtonMinus:I = 0x7f0d006f

.field public static final gridZoomControlButtonPlus:I = 0x7f0d0070

.field public static final gridZoomControlLayout:I = 0x7f0d0069

.field public static final gridZoomControlLayoutMain:I = 0x7f0d006a

.field public static final grid_BrightnessClosed:I = 0x7f0d000d

.field public static final grid_FmClosed:I = 0x7f0d0026

.field public static final grid_FmMagnifier:I = 0x7f0d0025

.field public static final grid_FmMove:I = 0x7f0d0030

.field public static final grid_FmScroll:I = 0x7f0d002b

.field public static final grid_FmSwipe:I = 0x7f0d002a

.field public static final grid_FmTouch:I = 0x7f0d0029

.field public static final grid_RotateStateClosed:I = 0x7f0d004f

.field public static final grid_VolumeClosed:I = 0x7f0d0035

.field public static final grid_VolumeDivider:I = 0x7f0d003c

.field public static final grid_ZoomClosed:I = 0x7f0d006d

.field public static final gridbrightnessControlButtonDown:I = 0x7f0d001a

.field public static final gridbrightnessControlButtonUp:I = 0x7f0d001b

.field public static final gridviewlower:I = 0x7f0d007e

.field public static final gridviewupper:I = 0x7f0d007d

.field public static final img:I = 0x7f0d0082

.field public static final item:I = 0x7f0d0081

.field public static final level:I = 0x7f0d0013

.field public static final level_layout:I = 0x7f0d0012

.field public static final level_text:I = 0x7f0d0019

.field public static final magnifier_img:I = 0x7f0d0072

.field public static final magnifier_zoom:I = 0x7f0d0073

.field public static final menubutton:I = 0x7f0d0003

.field public static final menubuttonBg:I = 0x7f0d0002

.field public static final menubutton_icon:I = 0x7f0d0004

.field public static final menubutton_name:I = 0x7f0d0005

.field public static final noAutoProgressImage:I = 0x7f0d0023

.field public static final noSupportLightSensor:I = 0x7f0d0011

.field public static final pagelayout:I = 0x7f0d0041

.field public static final rotate_status:I = 0x7f0d0050

.field public static final rotate_titlebar:I = 0x7f0d004d

.field public static final seekBar:I = 0x7f0d0016

.field public static final seekbar_level:I = 0x7f0d0015

.field public static final seekbar_topmargin_layout:I = 0x7f0d0014

.field public static final settings_menuedit_background:I = 0x7f0d0080

.field public static final sfMainStartservice:I = 0x7f0d0000

.field public static final sfMainStopservice:I = 0x7f0d0001

.field public static final singletap_noti:I = 0x7f0d0078

.field public static final subimg:I = 0x7f0d0083

.field public static final text:I = 0x7f0d0084

.field public static final totalpage:I = 0x7f0d0044

.field public static final volumeProgressImage:I = 0x7f0d003d

.field public static final volume_control_button:I = 0x7f0d0036

.field public static final volume_status:I = 0x7f0d005c

.field public static final volume_text_mute:I = 0x7f0d0065

.field public static final volume_text_sound:I = 0x7f0d0062

.field public static final volume_text_vibrate:I = 0x7f0d005f

.field public static final volume_titlebar:I = 0x7f0d0033

.field public static final zoom_layer:I = 0x7f0d0074

.field public static final zoom_minus:I = 0x7f0d0075

.field public static final zoom_plus:I = 0x7f0d0077

.field public static final zoom_seekbar:I = 0x7f0d0076

.field public static final zoom_seekbar_layout:I = 0x7f0d006e

.field public static final zoom_title:I = 0x7f0d006c

.field public static final zoom_titlebar:I = 0x7f0d006b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
