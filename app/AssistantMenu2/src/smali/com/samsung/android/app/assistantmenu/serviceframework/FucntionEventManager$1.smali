.class final Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;
.super Ljava/lang/Object;
.source "FucntionEventManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->takeScreenshot(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 126
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 127
    :try_start_0
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$100()Landroid/content/ServiceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;->val$context:Landroid/content/Context;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$100()Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 129
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$102(Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;

    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$1;->val$context:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_COPY:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 134
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    const-string v0, "FunctionEventManager"

    const-string v1, "[c] sScreenshotTimeout run()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return-void

    .line 134
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
