.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;)V
    .locals 0

    .prologue
    .line 807
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 810
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 812
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # operator++ for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1208(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F

    .line 813
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAccelRateVal:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)F

    move-result v3

    # -= operator for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$124(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;F)I

    .line 815
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    .line 817
    .local v1, "minBrightness":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v2

    if-ge v2, v1, :cond_2

    .line 818
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$102(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;I)I

    .line 819
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 832
    .end local v1    # "minBrightness":I
    :cond_0
    return-void

    .line 815
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v1

    goto :goto_1

    .line 823
    .restart local v1    # "minBrightness":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;->this$1:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 826
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 827
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
