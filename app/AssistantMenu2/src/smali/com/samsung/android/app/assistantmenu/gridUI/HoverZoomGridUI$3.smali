.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;
.super Landroid/database/ContentObserver;
.source "HoverZoomGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v4, 0x0

    .line 187
    const-string v1, "HoverZoomGridUI"

    const-string v2, "1,mHoverPadSizeObserver onChange"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomPadDisplayed:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_magnifier_size"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 190
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_value"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mScaleSize:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;I)I

    .line 192
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v3

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mMagnifierSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    aget v3, v3, v4

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mScaleSize:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/accessibility/AccessibilityManager;->enableMagnifier(IIF)V

    .line 193
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
