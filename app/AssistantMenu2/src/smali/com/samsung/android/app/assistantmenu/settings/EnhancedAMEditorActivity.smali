.class public Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;
.super Landroid/preference/PreferenceActivity;
.source "EnhancedAMEditorActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field CheckboxPreference:Z

.field private final KEY_EDIT_EAM:Ljava/lang/String;

.field private final KEY_EDIT_EAMSCREEN:Ljava/lang/String;

.field private final KEY_SELECTALL_EAMOPTIONS:Ljava/lang/String;

.field ListPreference:Ljava/lang/String;

.field private appList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private appsWithEAM:I

.field private checkCount:I

.field private checkboxPref:Landroid/preference/CheckBoxPreference;

.field context:Landroid/app/Activity;

.field private eamPrefScreen:Landroid/preference/PreferenceScreen;

.field private mActionBarLayout:Landroid/view/View;

.field mActionBarSwitch:Landroid/widget/Switch;

.field private mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

.field private mEAMEnable:Z

.field private mEAMprov:I

.field private mLoadComplete:Z

.field private messageHandler:Landroid/os/Handler;

.field packMngr:Landroid/content/pm/PackageManager;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private sIsPhone:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "EnhancedAMEditorActivity"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 56
    const-string v0, "eam_active"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->KEY_EDIT_EAM:Ljava/lang/String;

    .line 57
    const-string v0, "eam_options"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->KEY_EDIT_EAMSCREEN:Ljava/lang/String;

    .line 58
    const-string v0, "selectAll_pref"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->KEY_SELECTALL_EAMOPTIONS:Ljava/lang/String;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMEnable:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mLoadComplete:Z

    .line 61
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    .line 62
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    .line 63
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    .line 64
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 65
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appList:Ljava/util/ArrayList;

    .line 69
    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->sIsPhone:Z

    .line 70
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;-><init>(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->messageHandler:Landroid/os/Handler;

    return-void
.end method

.method private OrderAppData()V
    .locals 5

    .prologue
    .line 375
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 376
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v4, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 377
    .local v1, "chkPref":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "appTitle":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 379
    .local v3, "order":I
    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    .line 375
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 381
    .end local v0    # "appTitle":Ljava/lang/String;
    .end local v1    # "chkPref":Landroid/preference/CheckBoxPreference;
    .end local v3    # "order":I
    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mLoadComplete:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->OrderAppData()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->messageHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    return v0
.end method

.method static synthetic access$208(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    return v0
.end method

.method static synthetic access$308(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->resizeIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/preference/PreferenceScreen;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    return v0
.end method

.method public static isPhone(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 370
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 371
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private resizeIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "original"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 384
    new-instance v1, Lcom/samsung/android/app/assistantmenu/util/IconResizer;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/assistantmenu/util/IconResizer;-><init>(Landroid/content/Context;)V

    .line 385
    .local v1, "mIconResizer":Lcom/samsung/android/app/assistantmenu/util/IconResizer;
    const/4 v0, 0x0

    .line 386
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/assistantmenu/util/IconResizer;->createIconThumbnail(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 387
    return-object v0
.end method

.method private updateAssistantMenuSaving()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 212
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Done "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    if-nez v1, :cond_2

    .line 215
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4}, Landroid/widget/Switch;->setChecked(Z)V

    .line 226
    :cond_1
    return-void

    .line 221
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 222
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected fetchData()V
    .locals 3

    .prologue
    .line 283
    const-string v0, ""

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 284
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;-><init>(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->start()V

    .line 367
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    if-eqz p2, :cond_2

    .line 259
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 259
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_0
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    .line 264
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu_eam_enable"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 267
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Done Setting 1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    .end local v0    # "i":I
    :cond_1
    :goto_1
    return-void

    .line 268
    :cond_2
    if-nez p2, :cond_1

    .line 270
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 271
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 273
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu_eam_enable"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 276
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    .line 277
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Done Setting 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x10

    const/4 v5, -0x2

    const/4 v4, 0x0

    .line 86
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 87
    const v1, 0x7f050001

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->addPreferencesFromResource(I)V

    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    .line 90
    iput-object p0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    .line 91
    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {p0, v1, v4}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu_eam_enable"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    .line 96
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Done "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMEnable:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v1, Landroid/widget/Switch;

    invoke-direct {v1, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 99
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 102
    .local v0, "padding":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, v4, v4, v0, v4}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 103
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 105
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02015a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const v4, 0x800015

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v1, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarLayout:Landroid/view/View;

    .line 115
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->requestFocus()Z

    .line 117
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 119
    const-string v1, "eam_options"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    .line 122
    const-string v1, "selectAll_pref"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    .line 123
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 124
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    .line 125
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->fetchData()V

    .line 127
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 131
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 132
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 200
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 208
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 202
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->finish()V

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mLoadComplete:Z

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    const-string v1, "eam_app_count"

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 241
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    if-nez v0, :cond_1

    .line 242
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 246
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "assistant_menu_eam_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 248
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Done Setting 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_1
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 252
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1, "arg0"    # Landroid/preference/Preference;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 137
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    .local v3, "key":Ljava/lang/String;
    move-object v0, p2

    .line 138
    check-cast v0, Ljava/lang/Boolean;

    .line 139
    .local v0, "checkValue":Ljava/lang/Boolean;
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 141
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v5, "selectAll_pref"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 142
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "arg1":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 143
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 144
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    .line 149
    :goto_0
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 150
    sget-object v6, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] increment count:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 158
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 159
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 149
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 146
    .end local v2    # "i":I
    :cond_0
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    goto :goto_0

    .line 163
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v5, v2}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_2

    .line 169
    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 194
    .end local v2    # "i":I
    .end local p1    # "arg0":Landroid/preference/Preference;
    :cond_3
    :goto_3
    return v8

    .line 172
    .restart local p1    # "arg0":Landroid/preference/Preference;
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_4
    const/4 v4, 0x0

    .line 173
    .local v4, "value":I
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-ne v5, v9, :cond_5

    .line 174
    const/4 v4, 0x1

    .line 176
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    .line 177
    sget-object v5, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] increment count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_4
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 184
    check-cast p1, Landroid/preference/CheckBoxPreference;

    .end local p1    # "arg0":Landroid/preference/Preference;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "arg1":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {p1, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 185
    sget-object v5, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] updateEAM:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "aplication status"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 187
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    if-ne v5, v6, :cond_6

    .line 188
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v9}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_3

    .line 179
    .restart local p1    # "arg0":Landroid/preference/Preference;
    .restart local p2    # "arg1":Ljava/lang/Object;
    :cond_5
    const/4 v4, 0x0

    .line 180
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    .line 181
    sget-object v5, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] decrement count:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 189
    .end local p1    # "arg0":Landroid/preference/Preference;
    .end local p2    # "arg1":Ljava/lang/Object;
    :cond_6
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I

    if-ge v5, v6, :cond_3

    .line 190
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v8}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 232
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 233
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->updateAssistantMenuSaving()V

    .line 234
    return-void
.end method
