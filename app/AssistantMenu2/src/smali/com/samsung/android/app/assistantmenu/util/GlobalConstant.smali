.class public Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;
.super Ljava/lang/Object;
.source "GlobalConstant.java"


# static fields
.field public static ACTION_BOOT_COMPLETED:Ljava/lang/String; = null

.field public static ACTION_CLIPBOARD_COPY:Ljava/lang/String; = null

.field public static ACTION_CLIPBOARD_DISMISS:Ljava/lang/String; = null

.field public static ACTION_CLIPBOARD_SHOW:Ljava/lang/String; = null

.field public static ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = null

.field public static ACTION_COVER_OPEN:Ljava/lang/String; = null

.field public static ACTION_PACKAGE_ADDED:Ljava/lang/String; = null

.field public static ACTION_PACKAGE_CHANGED:Ljava/lang/String; = null

.field public static ACTION_PACKAGE_INSTALLED:Ljava/lang/String; = null

.field public static ACTION_PACKAGE_REPLACED:Ljava/lang/String; = null

.field public static ACTION_PACKAGE_UNINSTALLED:Ljava/lang/String; = null

.field public static ACTION_RECENTSPANEL_CHANGED:Ljava/lang/String; = null

.field public static ACTION_RECENTSPANEL_CLOSE:Ljava/lang/String; = null

.field public static ACTION_RECENTSPANEL_OPEN:Ljava/lang/String; = null

.field public static ACTION_REQUEST_AXT9INFO_CLOSE:Ljava/lang/String; = null

.field public static ACTION_RESPONSE_AXT9INFO:Ljava/lang/String; = null

.field public static ACTION_RESPONSE_AXT9INFO_TYPE_CHANGED:Ljava/lang/String; = null

.field public static ACTION_RINGER_MODE_CHANGED:Ljava/lang/String; = null

.field public static ACTION_SCREEN_OFF:Ljava/lang/String; = null

.field public static ACTION_SCREEN_ON:Ljava/lang/String; = null

.field public static ACTION_SHARING_COMPLETE:Ljava/lang/String; = null

.field public static ACTION_SIDESYNC_START:Ljava/lang/String; = null

.field public static ACTION_STATUSBAR_COLLAPSE:Ljava/lang/String; = null

.field public static ACTION_STATUSBAR_EXPAND:Ljava/lang/String; = null

.field public static ACTION_STATUSBAR_EXPANDED_NOTI:Ljava/lang/String; = null

.field public static ACTION_STATUSBAR_EXPANDED_SETTING:Ljava/lang/String; = null

.field public static ACTION_VOLUME_CHANGED:Ljava/lang/String; = null

.field public static final ATT:Z

.field public static BTKEYBOARD_EXTRA_STATE:Ljava/lang/String; = null

.field public static final BTKEYBOARD_STATE_CONNECTED:I = 0x2

.field public static DMB_LOCK:Ljava/lang/String; = null

.field public static final EAM_PREF_APP_COUNT:Ljava/lang/String; = "eam_app_count"

.field public static final FEATURE_COCKTAILBAR:Ljava/lang/String; = "com.sec.feature.cocktailbar"

.field public static final FEATURE_OVERLAYMAGNIFIER:Ljava/lang/String; = "com.sec.feature.overlaymagnifier"

.field public static IS_MOVABLE_KEYPAD:Ljava/lang/String; = null

.field public static IS_VISIBLE_WINDOW:Ljava/lang/String; = null

.field public static PLAYER_LOCK:Ljava/lang/String; = null

.field public static RecentAppListActivity:Ljava/lang/String; = null

.field public static SHARING_ACC_SETTINGS:Ljava/lang/String; = null

.field public static SHARING_ACC_SETTINGS_PREF:Ljava/lang/String; = null

.field public static final SPR:Z

.field public static SystemSettingAllSoundOff:Ljava/lang/String; = null

.field public static final TMO:Z

.field public static TOP_CLASS_ALARMALERT:Ljava/lang/String; = null

.field public static TOP_CLASS_ALARMSMARTALERT:Ljava/lang/String; = null

.field public static TOP_CLASS_DMB:Ljava/lang/String; = null

.field public static TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String; = null

.field public static TOP_CLASS_TIMEALARM:Ljava/lang/String; = null

.field public static TOP_CLASS_VIDEOPLAYER:Ljava/lang/String; = null

.field public static final USA:Z

.field public static final USCC:Z

.field public static final VZW:Z

.field public static final appInstallparsing:I = 0x1

.field public static final appUninstallparsing:I = 0x2

.field public static final appUpdateparsing:I = 0x3

.field public static assistantmenuSettingsPrefs:Ljava/lang/String; = null

.field public static final completeParsing:I = 0x0

.field public static cornerTabBottomPosY:Ljava/lang/String; = null

.field public static cornerTabDisplayHeight:Ljava/lang/String; = null

.field public static cornerTabPortrait:Ljava/lang/String; = null

.field public static cornerTabPosX:Ljava/lang/String; = null

.field public static cornerTabPosY:Ljava/lang/String; = null

.field public static cursorGridPosX:Ljava/lang/String; = null

.field public static cursorGridPosY:Ljava/lang/String; = null

.field public static cursorPadSize:Ljava/lang/String; = null

.field public static dominantHandChanged:Ljava/lang/String; = null

.field public static fingerMousePref:Ljava/lang/String; = null

.field public static gWindowType:I = 0x0

.field public static hoverZoomPref:Ljava/lang/String; = null

.field public static invalidList:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            ">;"
        }
    .end annotation
.end field

.field public static keyControlBarPosition:Ljava/lang/String; = null

.field public static keyGridViewLowerHeight:Ljava/lang/String; = null

.field public static keyGridViewUpperHeight:Ljava/lang/String; = null

.field public static keyLowerItemTotalCount:Ljava/lang/String; = null

.field public static keyMenuItemTotalCount:Ljava/lang/String; = null

.field public static pageTotalItem:I = 0x0

.field private static final sProductName:Ljava/lang/String;

.field public static final sUseAutoBrightnessDetail:Z

.field public static final sUseQuickSettingToggle:Z = true

.field public static touchDeviceID:I

.field public static touchLockSensetive:F

.field public static zoomPadSize:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 23
    sput v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->touchDeviceID:I

    .line 25
    const/high16 v0, 0x42200000    # 40.0f

    sput v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->touchLockSensetive:F

    .line 49
    sput v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->pageTotalItem:I

    .line 62
    const-string v0, "com.samsung.clipboardsaveservice.CLIPBOARD_COPY_RECEIVER"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_COPY:Ljava/lang/String;

    .line 64
    const-string v0, "com.android.systemui.statusbar.EXPANDED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPAND:Ljava/lang/String;

    .line 66
    const-string v0, "com.android.systemui.statusbar.COLLAPSED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_COLLAPSE:Ljava/lang/String;

    .line 68
    const-string v0, "com.android.systemui.statusbar.EXPANDED_NOTI"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_NOTI:Ljava/lang/String;

    .line 70
    const-string v0, "com.android.systemui.statusbar.EXPANDED_SETTING"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_SETTING:Ljava/lang/String;

    .line 72
    const-string v0, "com.android.systemui.recent.RECENTSPANEL_OPEN"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_OPEN:Ljava/lang/String;

    .line 74
    const-string v0, "com.android.systemui.recent.RECENTSPANEL_CLOSE"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CLOSE:Ljava/lang/String;

    .line 76
    const-string v0, "android.intent.action.PACKAGE_ADDED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_ADDED:Ljava/lang/String;

    .line 78
    const-string v0, "android.intent.action.PACKAGE_INSTALL"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_INSTALLED:Ljava/lang/String;

    .line 80
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_UNINSTALLED:Ljava/lang/String;

    .line 82
    const-string v0, "android.intent.action.PACKAGE_REPLACED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_REPLACED:Ljava/lang/String;

    .line 84
    const-string v0, "android.intent.action.PACKAGE_CHANGED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_PACKAGE_CHANGED:Ljava/lang/String;

    .line 86
    const-string v0, "android.intent.action.BOOT_COMPLETED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_BOOT_COMPLETED:Ljava/lang/String;

    .line 88
    const-string v0, "com.samsung.android.app.shareaccessibilitysettings.SHARING_COMPLETE"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SHARING_COMPLETE:Ljava/lang/String;

    .line 90
    const-string v0, "com.samsung.android.app.shareaccessibilitysettings"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SHARING_ACC_SETTINGS:Ljava/lang/String;

    .line 92
    const-string v0, "com.samsung.android.app.shareaccessibilitysettings_preferences"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SHARING_ACC_SETTINGS_PREF:Ljava/lang/String;

    .line 94
    const-string v0, "ResponseAxT9Info"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO:Ljava/lang/String;

    .line 96
    const-string v0, "ResponseAxT9InfoTypeChanged"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO_TYPE_CHANGED:Ljava/lang/String;

    .line 98
    const-string v0, "AxT9IME.isVisibleWindow"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->IS_VISIBLE_WINDOW:Ljava/lang/String;

    .line 100
    const-string v0, "AxT9IME.isMovableKeypad"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->IS_MOVABLE_KEYPAD:Ljava/lang/String;

    .line 102
    const-string v0, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    .line 104
    const-string v0, "android.bluetooth.profile.extra.STATE"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->BTKEYBOARD_EXTRA_STATE:Ljava/lang/String;

    .line 106
    const-string v0, "ShowClipboardDialog"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_SHOW:Ljava/lang/String;

    .line 108
    const-string v0, "dismissClipboardDialog"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_DISMISS:Ljava/lang/String;

    .line 110
    const-string v0, "android.intent.action.SCREEN_ON"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SCREEN_ON:Ljava/lang/String;

    .line 112
    const-string v0, "android.intent.action.SCREEN_OFF"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SCREEN_OFF:Ljava/lang/String;

    .line 114
    const-string v0, "com.samsung.cover.OPEN"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_COVER_OPEN:Ljava/lang/String;

    .line 116
    const-string v0, "com.samsung.axt9info.close"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_REQUEST_AXT9INFO_CLOSE:Ljava/lang/String;

    .line 118
    const-string v0, "android.media.RINGER_MODE_CHANGED"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RINGER_MODE_CHANGED:Ljava/lang/String;

    .line 120
    const-string v0, "android.media.VOLUME_CHANGED_ACTION"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_VOLUME_CHANGED:Ljava/lang/String;

    .line 122
    const-string v0, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SIDESYNC_START:Ljava/lang/String;

    .line 124
    const-string v0, "com.sec.android.app.videoplayer.PLAYER_LOCK"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->PLAYER_LOCK:Ljava/lang/String;

    .line 126
    const-string v0, "com.sec.app.dmb.PLAYER_LOCK"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->DMB_LOCK:Ljava/lang/String;

    .line 128
    const-string v0, "com.sec.android.app.videoplayer"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    .line 130
    const-string v0, "com.sec.android.app.dmb"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_DMB:Ljava/lang/String;

    .line 132
    const-string v0, "com.sec.android.app.clockpackage.alarm.AlarmAlert"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_ALARMALERT:Ljava/lang/String;

    .line 134
    const-string v0, "com.sec.android.app.clockpackage.alarm.AlarmSmartAlert"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_ALARMSMARTALERT:Ljava/lang/String;

    .line 136
    const-string v0, "com.sec.android.app.clockpackage.timer.TimerAlarm"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_TIMEALARM:Ljava/lang/String;

    .line 138
    const-string v0, "com.sec.android.app.simcardmanagement.dsnetwork"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    .line 140
    const-string v0, "com.sec.android.RECENTSWINDOW_SHOWING"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CHANGED:Ljava/lang/String;

    .line 146
    const-string v0, "ASSISTANTMENU_SETTINGS_PREFS"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    .line 149
    const-string v0, "menuItemTotalCount"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyMenuItemTotalCount:Ljava/lang/String;

    .line 151
    const-string v0, "lowerItemTotalCount"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyLowerItemTotalCount:Ljava/lang/String;

    .line 153
    const-string v0, "gridViewUpperHeight"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyGridViewUpperHeight:Ljava/lang/String;

    .line 155
    const-string v0, "gridViewLowerHeight"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyGridViewLowerHeight:Ljava/lang/String;

    .line 157
    const-string v0, "controlBarPosition"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->keyControlBarPosition:Ljava/lang/String;

    .line 159
    const-string v0, "cornerTabPosX"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    .line 161
    const-string v0, "cornerTabPosY"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    .line 163
    const-string v0, "cornerTabPortrait"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    .line 165
    const-string v0, "cornerTabBottomPosY"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    .line 167
    const-string v0, "cornerTabDisplayHeight"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    .line 169
    const-string v0, "dominantHandChanged"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    .line 171
    const-string v0, "FM_PREFERENCE"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->fingerMousePref:Ljava/lang/String;

    .line 173
    const-string v0, "HZ_PREFERENCE"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->hoverZoomPref:Ljava/lang/String;

    .line 175
    const-string v0, "cursorGridPosX"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosX:Ljava/lang/String;

    .line 177
    const-string v0, "cursorGridPosY"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosY:Ljava/lang/String;

    .line 179
    const-string v0, "cursorPadSize"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorPadSize:Ljava/lang/String;

    .line 181
    const-string v0, "zoomPadSize"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->zoomPadSize:Ljava/lang/String;

    .line 184
    const-string v0, "all_sound_off"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SystemSettingAllSoundOff:Ljava/lang/String;

    .line 187
    const-string v0, "RecentsActivity"

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->RecentAppListActivity:Ljava/lang/String;

    .line 190
    const/16 v0, 0x830

    sput v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->invalidList:Ljava/util/ArrayList;

    .line 202
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    .line 209
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "vzw"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->VZW:Z

    .line 211
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "att"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "uc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ATT:Z

    .line 213
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "spr"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SPR:Z

    .line 215
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "tmo"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "TMB"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TMO:Z

    .line 217
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sProductName:Ljava/lang/String;

    const-string v3, "usc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->USCC:Z

    .line 219
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->VZW:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ATT:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SPR:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TMO:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->USCC:Z

    if-eqz v0, :cond_5

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->USA:Z

    .line 221
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->USA:Z

    if-nez v0, :cond_6

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_AUTOMATIC_BRIGHTNESS_DETAIL"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "off"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Setting_ConfigAutomaticBrightnessDetail"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_3
    sput-boolean v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    return-void

    :cond_3
    move v0, v1

    .line 211
    goto :goto_0

    :cond_4
    move v0, v1

    .line 215
    goto :goto_1

    :cond_5
    move v0, v1

    .line 219
    goto :goto_2

    :cond_6
    move v2, v1

    .line 221
    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
