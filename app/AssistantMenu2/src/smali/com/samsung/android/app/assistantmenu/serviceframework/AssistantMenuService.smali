.class public Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.super Landroid/app/Service;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$10;,
        Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;,
        Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;
    }
.end annotation


# static fields
.field private static final ARG_BOTH:I = 0x2

.field private static final ARG_VERTICAL:I = 0x1

.field private static final FLASH_ANNOTATE_CLASS_NAME:Ljava/lang/String; = "com.sec.spen.flashannotate.FlashAnnotateActivity"

.field public static final MSG_ASSISTANT_UPDATE:I = 0x1

.field public static final MSG_AUTO_EXIT_TIMER_EXPIRED:I = 0x7

.field public static final MSG_DOMINANT_HAND_CHANGE:I = 0x6

.field public static final MSG_EXIT_CURRENT_SCREEN:I = 0x2

.field public static final MSG_LOCALE_CHANGED:I = 0x64

.field public static final MSG_MENU_BUTTON_ACT:I = 0x4

.field public static final MSG_NONE:I = 0x0

.field public static final MSG_QUICKPANEL_STATE_CHANGE:I = 0x5

.field public static final MSG_RETURN_TO_MENU:I = 0x3

.field public static final MSG_ROTATE_STATE_CHANGE:I = 0x1

.field public static final MSG_SCREEN_OFF:I = 0x65

.field private static final NOTI_FOREGROUND_ID:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "AssistantMenuService"

.field private static actOptions:Ljava/lang/String;

.field private static fragOptions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static iconCount:I

.field private static isAutoExit:Z

.field private static isMultInstance:Z

.field private static isResume:Z

.field private static mFindoFeatureChecked:Z

.field private static mFindoFeatureEnabled:Z

.field private static recentActivity:Ljava/lang/String;

.field private static recentPackname:Ljava/lang/String;


# instance fields
.field private final SCREEN_TIMEOUT_DELAY:J

.field private final TALKBACK_ON:I

.field private dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

.field private dmb_locked:Z

.field private iBinder:Landroid/os/IBinder;

.field private isEamUpdateApp:I

.field private isFMClosedViaKeyboard:Z

.field private isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

.field mAm:Landroid/app/IActivityManager;

.field mAssistantMenuSettingPrefListner:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

.field private mAutoRotation:Z

.field private mBTKeyboardState:Z

.field private mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

.field mCallListener:Landroid/telephony/PhoneStateListener;

.field private final mCarModeObserver:Landroid/database/ContentObserver;

.field private mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

.field private final mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mDualFolderType:Z

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

.field final mForegroundToken:Landroid/os/IBinder;

.field private mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

.field private mHandMode:I

.field private final mHandler:Landroid/os/Handler;

.field private mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

.field private mIsCocktailBarModel:Z

.field private mIsFolderClose:Z

.field private mIsMoveableIME:Z

.field private mIsOpenClipboard:Z

.field private mIsOpenIME:Z

.field private mIsOpenQuickNoti:Z

.field private mIsOpenQuickSetting:Z

.field private mIsOpenQuickpanel:Z

.field private mIsOpenRecentspanel:Z

.field private mIsVertical:Z

.field private mIsVisibleIME:Z

.field private final mKidsModeObserver:Landroid/database/ContentObserver;

.field private mLocale:Ljava/util/Locale;

.field final mMessenger:Landroid/os/Messenger;

.field private final mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mReceiver:Landroid/content/BroadcastReceiver;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

.field private mStartId:I

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field final mTimerHandler:Landroid/os/Handler;

.field mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field private mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

.field private mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

.field private shouldOpenMenu:Z

.field private video_locked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    sput-boolean v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isResume:Z

    .line 213
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 215
    sput v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 217
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    .line 219
    sput-boolean v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z

    .line 224
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    .line 243
    sput-boolean v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureChecked:Z

    .line 244
    sput-boolean v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureEnabled:Z

    .line 247
    const-string v0, ""

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    .line 248
    sput-boolean v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 89
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 102
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTimerHandler:Landroid/os/Handler;

    .line 105
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .line 108
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .line 111
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .line 114
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .line 117
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .line 120
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    .line 123
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .line 126
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .line 128
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    .line 131
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 139
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 142
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAutoRotation:Z

    .line 144
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    .line 146
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    .line 148
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mDualFolderType:Z

    .line 150
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    .line 152
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z

    .line 154
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z

    .line 156
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    .line 158
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    .line 160
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z

    .line 162
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z

    .line 164
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z

    .line 166
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z

    .line 168
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsFolderClose:Z

    .line 170
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsCocktailBarModel:Z

    .line 174
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    .line 175
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 200
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->SCREEN_TIMEOUT_DELAY:J

    .line 210
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->TALKBACK_ON:I

    .line 221
    new-instance v0, Landroid/os/Messenger;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mMessenger:Landroid/os/Messenger;

    .line 223
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;

    .line 227
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 232
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->shouldOpenMenu:Z

    .line 233
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    .line 234
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z

    .line 240
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mForegroundToken:Landroid/os/IBinder;

    .line 241
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    .line 246
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    .line 264
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    .line 397
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$2;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mKidsModeObserver:Landroid/database/ContentObserver;

    .line 407
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCarModeObserver:Landroid/database/ContentObserver;

    .line 421
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 693
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingPrefListner:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 764
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCallListener:Landroid/telephony/PhoneStateListener;

    .line 2414
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$8;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 2423
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$9;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method private Closekeyboard()V
    .locals 3

    .prologue
    .line 851
    const-string v1, "AssistantMenuService"

    const-string v2, "[c] DoAxt9ImeClose"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_REQUEST_AXT9INFO_CLOSE:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 854
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->sendBroadcast(Landroid/content/Intent;)V

    .line 855
    return-void
.end method

.method private DeactivateStatusBar()V
    .locals 2

    .prologue
    .line 840
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 841
    .local v0, "mStatusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 842
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 844
    :cond_0
    return-void
.end method

.method private ExcuteAct(Ljava/lang/Object;)V
    .locals 14
    .param p1, "act"    # Ljava/lang/Object;

    .prologue
    const/16 v11, 0x1388

    const-wide/16 v12, 0x32

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 894
    const/4 v6, 0x0

    .line 896
    .local v6, "topActivity":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopActivity()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 900
    :goto_0
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$10;->$SwitchMap$com$samsung$android$app$assistantmenu$AssistantMenuUIAct$Act:[I

    move-object v7, p1

    check-cast v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    .line 1548
    .end local p1    # "act":Ljava/lang/Object;
    :cond_0
    :goto_1
    return-void

    .line 897
    .restart local p1    # "act":Ljava/lang/Object;
    :catch_0
    move-exception v2

    .line 898
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 903
    .end local v2    # "e":Ljava/lang/Exception;
    :pswitch_0
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 904
    :cond_1
    const-string v7, "AssistantMenuService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[c] ExcuteAct - PressMenuKey is not operated:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 912
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 915
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 916
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto :goto_1

    .line 921
    :pswitch_1
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 922
    :cond_3
    const-string v7, "AssistantMenuService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[c] ExcuteAct - PressMenuKey is not operated:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 930
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 932
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->menuButtonTapEvent()V

    .line 934
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 935
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 940
    :pswitch_2
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    .line 950
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->homeButtonTapEvent()V

    .line 952
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    invoke-static {v7, v11}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;I)V

    .line 953
    sput-boolean v9, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z

    goto/16 :goto_1

    .line 957
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-ne v7, v9, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "parental_control_block_key"

    invoke-static {v7, v8, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eq v7, v9, :cond_0

    .line 961
    :cond_5
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->backButtonTapEvent()V

    .line 963
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    invoke-static {v7, v11}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;I)V

    .line 964
    sput-boolean v9, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z

    goto/16 :goto_1

    .line 969
    :pswitch_4
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    .line 973
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 975
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    if-nez v7, :cond_6

    .line 976
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .line 978
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ShowView()V

    .line 979
    iput-boolean v10, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    .line 980
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 985
    :pswitch_5
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    .line 989
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 990
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    if-nez v7, :cond_7

    .line 991
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    .line 994
    :cond_7
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ShowView()V

    .line 995
    iput-boolean v10, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    .line 996
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1008
    :pswitch_6
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-eq v7, v9, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_8

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1013
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    move-object v7, p1

    .line 1015
    check-cast v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v7, v8, :cond_9

    .line 1017
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowQuickPanel(Landroid/content/Context;)V

    .line 1035
    .end local p1    # "act":Ljava/lang/Object;
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1036
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1019
    .restart local p1    # "act":Ljava/lang/Object;
    :cond_9
    check-cast p1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .end local p1    # "act":Ljava/lang/Object;
    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne p1, v7, :cond_a

    .line 1021
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowSettingsPanel(Landroid/content/Context;)V

    goto :goto_2

    .line 1024
    :cond_a
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_c

    .line 1025
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z

    if-eqz v7, :cond_b

    .line 1026
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowSettingsPanel(Landroid/content/Context;)V

    goto :goto_2

    .line 1028
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowQuickPanel(Landroid/content/Context;)V

    goto :goto_2

    .line 1031
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowQuickPanel(Landroid/content/Context;)V

    goto :goto_2

    .line 1044
    .restart local p1    # "act":Ljava/lang/Object;
    :pswitch_7
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-eq v7, v9, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_d

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    :cond_d
    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_ALARMALERT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_ALARMSMARTALERT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_TIMEALARM:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z

    if-eqz v7, :cond_e

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_DMB:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1059
    :cond_e
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->checkPhoneIncomingCall()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1060
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->checkPhoneShowingIncomingPopup()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1065
    :cond_f
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_10

    .line 1066
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1069
    :cond_10
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1071
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowRecentAppList()V

    .line 1073
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1074
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1080
    :pswitch_8
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_11

    .line 1081
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1085
    :cond_11
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_12

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1092
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1098
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-eqz v7, :cond_13

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-nez v7, :cond_13

    const-string v7, "audio"

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    invoke-virtual {v7}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v7

    if-nez v7, :cond_13

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isDeviceProvisioned()Z

    move-result v7

    if-eq v7, v9, :cond_0

    .line 1104
    :cond_13
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1106
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    if-nez v7, :cond_14

    .line 1107
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .line 1109
    :cond_14
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ShowView()V

    .line 1111
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1125
    :pswitch_9
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1126
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->lockButtonTapEvent()V

    .line 1128
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1129
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1134
    :pswitch_a
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_15

    .line 1135
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1139
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_16

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isPowerOffAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1146
    :cond_16
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-eqz v7, :cond_17

    .line 1147
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemovePreviousAction()V

    .line 1150
    :cond_17
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_18

    .line 1151
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1154
    :cond_18
    sget-boolean v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TMO:Z

    if-ne v7, v9, :cond_19

    .line 1155
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->PowerOff(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 1159
    :cond_19
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1169
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->PowerOff(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 1171
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 1174
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->RemoveView()V

    .line 1175
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ShowView()V

    goto/16 :goto_1

    .line 1184
    :pswitch_b
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_1a

    .line 1185
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1188
    :cond_1a
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1190
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v7}, Landroid/view/accessibility/AccessibilityManager;->openGlobalActions()V

    .line 1194
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1195
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1204
    :pswitch_c
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_1b

    .line 1205
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1208
    :cond_1b
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_1c

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isScreenCaptureEnabledInternal(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1214
    :cond_1c
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1216
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTimerHandler:Landroid/os/Handler;

    new-instance v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$7;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$7;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    invoke-virtual {v7, v8, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1222
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1228
    :pswitch_d
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    .line 1232
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_1d

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v7

    if-nez v7, :cond_1d

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-nez v7, :cond_1d

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    if-nez v7, :cond_1d

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1e

    .line 1234
    :cond_1d
    const-string v7, "AssistantMenuService"

    const-string v8, "[c] ExcuteAct - ZoomControl is not operated!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1238
    :cond_1e
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1240
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    if-nez v7, :cond_1f

    .line 1241
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .line 1243
    :cond_1f
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ShowView()V

    .line 1245
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1250
    :pswitch_e
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    .line 1252
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_20

    .line 1253
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1257
    :cond_20
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_21

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1264
    :cond_21
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-eq v7, v9, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1269
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1271
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    if-nez v7, :cond_22

    .line 1272
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    .line 1275
    :cond_22
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ShowView()V

    .line 1277
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1284
    :pswitch_f
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_23

    .line 1285
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1289
    :cond_23
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_24

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isPowerOffAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1296
    :cond_24
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-eqz v7, :cond_25

    .line 1297
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemovePreviousAction()V

    .line 1300
    :cond_25
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_26

    .line 1301
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1304
    :cond_26
    sget-boolean v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TMO:Z

    if-ne v7, v9, :cond_27

    .line 1305
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->Restart(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 1309
    :cond_27
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1319
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->Restart(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 1321
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 1324
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->RemoveView()V

    .line 1325
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ShowView()V

    goto/16 :goto_1

    .line 1331
    :pswitch_10
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_28

    .line 1332
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1336
    :cond_28
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_29

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1343
    :cond_29
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-eq v7, v8, :cond_0

    .line 1347
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1352
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_2a

    .line 1353
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1356
    :cond_2a
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1358
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    if-nez v7, :cond_2b

    .line 1359
    new-instance v7, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;-><init>(Landroid/app/Service;)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    .line 1361
    :cond_2b
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->ShowView()V

    .line 1363
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1369
    :pswitch_11
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v7, :cond_2c

    .line 1370
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "enterprise_policy"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 1374
    :cond_2c
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v7, :cond_2d

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1381
    :cond_2d
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isUnsecureKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-eq v7, v9, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1391
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetRecentAppList()Z

    move-result v7

    if-eqz v7, :cond_2e

    .line 1392
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemovePreviousAction()V

    .line 1395
    :cond_2e
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_2f

    .line 1396
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1399
    :cond_2f
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1405
    :try_start_1
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v8, 0x10008000

    invoke-virtual {v7, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v4

    .line 1407
    .local v4, "i":Landroid/content/Intent;
    const-string v7, ":android:show_fragment"

    const-class v8, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1409
    const-string v7, "ACCESSIBILITY_SETTINGS_BACK"

    const/4 v8, 0x1

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1410
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->startActivity(Landroid/content/Intent;)V

    .line 1412
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1414
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 1415
    .end local v4    # "i":Landroid/content/Intent;
    :catch_1
    move-exception v2

    .line 1416
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v7, "AssistantMenuService"

    const-string v8, "Setting open fail!!"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1422
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :pswitch_12
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v7

    if-eqz v7, :cond_32

    .line 1424
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_30

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1430
    :cond_30
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1431
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->homeButtonLongTapEvent()V

    .line 1434
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 1435
    .local v5, "resolver":Landroid/content/ContentResolver;
    const-string v7, "haptic_feedback_enabled"

    invoke-static {v5, v7, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v9, :cond_31

    .line 1437
    const-string v7, "vibrator"

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Vibrator;

    invoke-virtual {v7, v12, v13}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1440
    :cond_31
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1441
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1444
    .end local v5    # "resolver":Landroid/content/ContentResolver;
    :cond_32
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v7

    if-eq v7, v9, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_33

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopActivity()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1454
    :cond_33
    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    if-eqz v7, :cond_34

    .line 1455
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DeactivateStatusBar()V

    .line 1458
    :cond_34
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1460
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowRecentAppList()V

    .line 1462
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1463
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1469
    :pswitch_13
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_35

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1474
    :cond_35
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1476
    const-string v0, "multi_window_enabled"

    .line 1477
    .local v0, "DB_MULTI_WINDOW_MODE_ON":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v0, v10}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-ne v7, v9, :cond_36

    .line 1478
    new-instance v3, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v3, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1479
    .local v3, "flashBarIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.FlashBarService"

    const-string v8, "com.sec.android.app.FlashBarService.MultiWindowTrayService"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1481
    sget-object v7, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v3, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 1483
    const-string v7, "vibrator"

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/Vibrator;

    invoke-virtual {v7, v12, v13}, Landroid/os/Vibrator;->vibrate(J)V

    .line 1485
    .end local v3    # "flashBarIntent":Landroid/content/Intent;
    :cond_36
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1486
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1490
    .end local v0    # "DB_MULTI_WINDOW_MODE_ON":Ljava/lang/String;
    :pswitch_14
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1492
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->lockButtonLongTapEvent()V

    .line 1494
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1495
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1499
    :pswitch_15
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1500
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1504
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1506
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->hasFindoFeature(Landroid/content/Context;)Z

    move-result v1

    .line 1508
    .local v1, "bHasFindo":Z
    if-eqz v1, :cond_37

    const-string v7, "ro.product.name"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "jalte"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_37

    const-string v7, "ro.product.name"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "jflte"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_37

    .line 1510
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->menuButtonLongTapEventForFindo()V

    .line 1514
    :goto_3
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1515
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1512
    :cond_37
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->menuButtonLongTapEvent()V

    goto :goto_3

    .line 1520
    .end local v1    # "bHasFindo":Z
    :pswitch_16
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1522
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_SIMCARDMANAGEMENT:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    if-eqz v7, :cond_38

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->TOP_CLASS_VIDEOPLAYER:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1528
    :cond_38
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1529
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->menuButtonTapEvent()V

    .line 1531
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1532
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 1537
    :pswitch_17
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1539
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowDeviceOptions(Landroid/content/Context;)V

    .line 1541
    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1542
    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_1

    .line 900
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method private RemoveChildScreens()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1817
    const-string v0, "AssistantMenuService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] RemoveChildScreens()+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1819
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$10;->$SwitchMap$com$samsung$android$app$assistantmenu$serviceframework$AssistantMenuService$ViewMode:[I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1892
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1821
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-eqz v0, :cond_0

    .line 1822
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->RemoveView()V

    .line 1823
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    goto :goto_0

    .line 1828
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    if-eqz v0, :cond_0

    .line 1829
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->RemoveView()V

    .line 1830
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    goto :goto_0

    .line 1842
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    if-eqz v0, :cond_0

    .line 1843
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->RemoveView()V

    .line 1844
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    goto :goto_0

    .line 1856
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    if-eqz v0, :cond_0

    .line 1857
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->RemoveView()V

    .line 1858
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    goto :goto_0

    .line 1863
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    if-eqz v0, :cond_0

    .line 1864
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->RemoveView()V

    .line 1865
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    goto :goto_0

    .line 1870
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    if-eqz v0, :cond_0

    .line 1871
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->RemoveView()V

    .line 1872
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ClearMagnifierStatus()V

    .line 1873
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    goto :goto_0

    .line 1878
    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    if-eqz v0, :cond_0

    .line 1879
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 1880
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ClearMagnifierStatus()V

    .line 1881
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    goto :goto_0

    .line 1819
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch
.end method

.method private RemoveChildScreensWithoutPokeWakelock()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1898
    const-string v2, "AssistantMenuService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] justRemoveChildScreens()+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1899
    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$10;->$SwitchMap$com$samsung$android$app$assistantmenu$serviceframework$AssistantMenuService$ViewMode:[I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1942
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1901
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    if-eqz v2, :cond_0

    .line 1902
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->RemoveViewWithoutPokeWakelock()V

    .line 1903
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    goto :goto_0

    .line 1907
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    if-eqz v2, :cond_0

    .line 1908
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->RemoveViewWithoutPokeWakelock()V

    .line 1909
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    goto :goto_0

    .line 1913
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    if-eqz v2, :cond_0

    .line 1914
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->RemoveViewWithoutPokeWakelock()V

    .line 1915
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    goto :goto_0

    .line 1919
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    if-eqz v2, :cond_0

    .line 1920
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadxPos()I

    move-result v0

    .line 1921
    .local v0, "padXpos":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadyPos()I

    move-result v1

    .line 1922
    .local v1, "padYpos":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->editCursorPreference(II)V

    .line 1923
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->RemoveView()V

    .line 1924
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ClearMagnifierStatus()V

    .line 1925
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    goto :goto_0

    .line 1929
    .end local v0    # "padXpos":I
    .end local v1    # "padYpos":I
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    if-eqz v2, :cond_0

    .line 1930
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadxPos()I

    move-result v0

    .line 1931
    .restart local v0    # "padXpos":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadyPos()I

    move-result v1

    .line 1932
    .restart local v1    # "padYpos":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->editCursorPreference(II)V

    .line 1933
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 1934
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ClearMagnifierStatus()V

    .line 1935
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    goto :goto_0

    .line 1899
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private RemovePreviousAction()V
    .locals 4

    .prologue
    .line 826
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->backButtonTapEvent()V

    .line 829
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 833
    :goto_0
    return-void

    .line 830
    :catch_0
    move-exception v0

    .line 831
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private ShowGridUI()V
    .locals 2

    .prologue
    .line 1948
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    if-eqz v0, :cond_0

    .line 1949
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->Closekeyboard()V

    .line 1952
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z

    if-eqz v0, :cond_1

    .line 1953
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DismissClipboard()V

    .line 1956
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->shouldOpenMenu:Z

    .line 1957
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1959
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->getisNormal()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1960
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1962
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->shouldOpenMenu:Z

    .line 1963
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-nez v0, :cond_2

    .line 1964
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;-><init>(Landroid/app/Service;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .line 1966
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowView()V

    .line 1968
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 1972
    :goto_0
    return-void

    .line 1970
    :cond_3
    const-string v0, "AssistantMenuService"

    const-string v1, "mCornertabUI == null or mCornertabUI is not normal"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->ShowGridUI()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->Closekeyboard()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->ExcuteAct(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->clearChildScreensinLockMode()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mStartId:I

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z

    return p1
.end method

.method static synthetic access$2102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "x1"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    return p1
.end method

.method static synthetic access$3202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 84
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$3302(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 84
    sput p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    return p0
.end method

.method static synthetic access$3400()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$3502(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 84
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    return-object v0
.end method

.method static synthetic access$800()Z
    .locals 1

    .prologue
    .line 84
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z

    return v0
.end method

.method static synthetic access$802(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 84
    sput-boolean p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z

    return p0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    return-void
.end method

.method private checkPhoneIncomingCall()Z
    .locals 5

    .prologue
    .line 873
    const/4 v1, 0x0

    .line 875
    .local v1, "incomingCall":Z
    :try_start_0
    const-string v3, "phone"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    .line 878
    .local v2, "telephonyService":Lcom/android/internal/telephony/ITelephony;
    if-eqz v2, :cond_0

    .line 879
    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    move v3, v1

    .line 885
    .end local v2    # "telephonyService":Lcom/android/internal/telephony/ITelephony;
    :goto_0
    return v3

    .line 882
    :catch_0
    move-exception v0

    .line 883
    .local v0, "ex":Landroid/os/RemoteException;
    const-string v3, "AssistantMenuService"

    const-string v4, "RemoteException from getPhoneInterface()"

    invoke-static {v3, v4, v0}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 885
    .end local v0    # "ex":Landroid/os/RemoteException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private checkPhoneShowingIncomingPopup()Z
    .locals 2

    .prologue
    .line 857
    const/4 v0, 0x0

    .line 869
    .local v0, "checkPopup":Z
    const/4 v1, 0x0

    return v1
.end method

.method private clearChildScreensinLockMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 791
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    if-eqz v0, :cond_2

    .line 792
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreensWithoutPokeWakelock()V

    .line 793
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 794
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-nez v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabVisibility()Z

    move-result v0

    if-nez v0, :cond_1

    .line 796
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 797
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 809
    :cond_1
    :goto_0
    return-void

    .line 800
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-eqz v0, :cond_1

    .line 801
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->RemoveView()V

    .line 802
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .line 804
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabVisibility()Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 806
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto :goto_0
.end method

.method public static getActivityOptions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2276
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    return-object v0
.end method

.method private getAutoRotation()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 817
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static getFragOptionCount()I
    .locals 1

    .prologue
    .line 2280
    sget v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    return v0
.end method

.method public static getFragOptions()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2288
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getRecentActivity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2268
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    return-object v0
.end method

.method public static getRecentPackName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2307
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    return-object v0
.end method

.method private getTopActivityPreference(Ljava/lang/String;)V
    .locals 6
    .param p1, "topClass"    # Ljava/lang/String;

    .prologue
    .line 2312
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    invoke-virtual {v3, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getPackAMData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    .line 2313
    sget-object v3, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 2315
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 2317
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2319
    .local v1, "applicationName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    sget-object v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    .line 2321
    const-string v3, "AssistantMenuService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] updateEAM:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "application status"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2327
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "applicationName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2323
    :catch_0
    move-exception v2

    .line 2324
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized hasFindoFeature(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 254
    const-class v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureChecked:Z

    if-eqz v0, :cond_0

    .line 255
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    :goto_0
    monitor-exit v1

    return v0

    .line 259
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "com.sec.feature.findo"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureEnabled:Z

    .line 260
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureChecked:Z

    .line 261
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFindoFeatureEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isFragState()Z
    .locals 1

    .prologue
    .line 2296
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isResume:Z

    return v0
.end method

.method public static setFragOptions(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2292
    .local p0, "arrList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    .line 2293
    return-void
.end method

.method public static setFragState(Z)V
    .locals 0
    .param p0, "stt"    # Z

    .prologue
    .line 2300
    sput-boolean p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isResume:Z

    .line 2301
    return-void
.end method

.method public static setRecentActivity(Ljava/lang/String;)V
    .locals 0
    .param p0, "recFrag"    # Ljava/lang/String;

    .prologue
    .line 2272
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 2273
    return-void
.end method

.method public static settActivityOptions(Ljava/lang/String;)V
    .locals 0
    .param p0, "recFrag"    # Ljava/lang/String;

    .prologue
    .line 2284
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    .line 2285
    return-void
.end method


# virtual methods
.method public CornertabUiUpdate()V
    .locals 1

    .prologue
    .line 2103
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    if-eqz v0, :cond_0

    .line 2104
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 2105
    :cond_0
    return-void
.end method

.method public DismissClipboard()V
    .locals 3

    .prologue
    .line 2112
    const-string v1, "AssistantMenuService"

    const-string v2, "[c] DismissClipboard()+"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2113
    const-string v1, "clipboardEx"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/sec/clipboard/ClipboardExManager;

    .line 2114
    .local v0, "clipEx":Landroid/sec/clipboard/ClipboardExManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2115
    invoke-virtual {v0}, Landroid/sec/clipboard/ClipboardExManager;->dismissUIDataDialog()V

    .line 2117
    :cond_0
    return-void
.end method

.method public GetAssistantMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2095
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public GetAutoRotation()Z
    .locals 1

    .prologue
    .line 2086
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAutoRotation:Z

    return v0
.end method

.method public GetHandMode()I
    .locals 1

    .prologue
    .line 2077
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    return v0
.end method

.method public GetHanlder()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1809
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public GetIsCornertabMove()Z
    .locals 1

    .prologue
    .line 2059
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetMoveableIME()Z
    .locals 1

    .prologue
    .line 2050
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z

    return v0
.end method

.method public GetOpenClipboard()Z
    .locals 1

    .prologue
    .line 2068
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z

    return v0
.end method

.method public GetOpenIME()Z
    .locals 1

    .prologue
    .line 2041
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public GetQuickNoti()Z
    .locals 1

    .prologue
    .line 2023
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z

    return v0
.end method

.method public GetQuickPanel()Z
    .locals 1

    .prologue
    .line 2014
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z

    return v0
.end method

.method public GetQuickSettings()Z
    .locals 1

    .prologue
    .line 2032
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z

    return v0
.end method

.method public GetRecentAppList()Z
    .locals 2

    .prologue
    .line 2004
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopClass()Ljava/lang/String;

    move-result-object v0

    .line 2005
    .local v0, "topClass":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->RecentAppListActivity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    return v1
.end method

.method public GetTopActivity()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2126
    const-string v2, ""

    .line 2128
    .local v2, "className":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 2129
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 2130
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 2132
    .local v4, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 2133
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] GetTopActivity:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2138
    .end local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :goto_0
    return-object v2

    .line 2134
    :catch_0
    move-exception v3

    .line 2135
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public GetTopClass()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2168
    const-string v3, ""

    .line 2170
    .local v3, "pkgName":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 2171
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 2172
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 2174
    .local v4, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 2175
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] GetTopCalss:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2180
    .end local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :goto_0
    return-object v3

    .line 2176
    :catch_0
    move-exception v2

    .line 2177
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public GetTopPackageName()Ljava/lang/String;
    .locals 8

    .prologue
    .line 2147
    const-string v3, ""

    .line 2149
    .local v3, "pkgName":Ljava/lang/String;
    :try_start_0
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 2150
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    .line 2151
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 2153
    .local v4, "topActivity":Landroid/content/ComponentName;
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 2154
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] GetTopPackageName:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2159
    .end local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :goto_0
    return-object v3

    .line 2155
    :catch_0
    move-exception v2

    .line 2156
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public GetVertical()Z
    .locals 1

    .prologue
    .line 1995
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    return v0
.end method

.method public IsKidsMode()I
    .locals 3

    .prologue
    .line 2360
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "kids_home_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getdbHandler()Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;
    .locals 1

    .prologue
    .line 2304
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    return-object v0
.end method

.method public isCocktailBarModel()Z
    .locals 1

    .prologue
    .line 2364
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsCocktailBarModel:Z

    return v0
.end method

.method public isCornerTab()Z
    .locals 2

    .prologue
    .line 2374
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v0, v1, :cond_0

    .line 2375
    const/4 v0, 0x1

    .line 2377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isDeviceProvisioned()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2368
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_provisioned"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isMonkeyRunning()Z
    .locals 8

    .prologue
    .line 2334
    const/4 v1, 0x0

    .line 2335
    .local v1, "checked":Z
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2338
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v5, 0x100

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 2339
    .local v2, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v2, :cond_1

    .line 2340
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2341
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 2343
    .local v4, "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.android.commands.monkey"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2344
    const/4 v1, 0x1

    .line 2345
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MonkeyRunning check : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 2351
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    .end local v4    # "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->IsKidsMode()I

    move-result v5

    if-nez v5, :cond_2

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    or-int/2addr v5, v1

    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public isShouldOpenMenu()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->shouldOpenMenu:Z

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1685
    const-string v0, "AssistantMenuService"

    const-string v1, "onBind()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1687
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1756
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1758
    const/4 v1, 0x0

    .line 1759
    .local v1, "needUIupdate":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v7, :cond_3

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsFolderClose:Z

    .line 1760
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "sub_lcd_auto_lock"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1762
    .local v0, "mAutoLockmode":I
    const-string v2, "AssistantMenuService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[c] onConfigurationChanged:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1764
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mDualFolderType:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v2, v5, :cond_4

    .line 1765
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    if-eqz v2, :cond_0

    .line 1766
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->ScreenRotateStatusChanged()V

    .line 1774
    :cond_0
    :goto_1
    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mLocale:Ljava/util/Locale;

    if-eq v2, v5, :cond_1

    .line 1775
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x64

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1776
    iget-object v2, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mLocale:Ljava/util/Locale;

    .line 1779
    :cond_1
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v7, :cond_5

    .line 1780
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    if-eqz v2, :cond_2

    .line 1781
    const/4 v1, 0x1

    .line 1782
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    .line 1791
    :cond_2
    :goto_2
    if-nez v1, :cond_6

    .line 1797
    :goto_3
    return-void

    .end local v0    # "mAutoLockmode":I
    :cond_3
    move v2, v4

    .line 1759
    goto :goto_0

    .line 1768
    .restart local v0    # "mAutoLockmode":I
    :cond_4
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mDualFolderType:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsFolderClose:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v2, v5, :cond_0

    .line 1769
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    if-eqz v2, :cond_0

    if-ne v0, v3, :cond_0

    .line 1770
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 1785
    :cond_5
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    if-nez v2, :cond_2

    .line 1786
    const/4 v1, 0x1

    .line 1787
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    goto :goto_2

    .line 1795
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1587
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 1588
    const-string v4, "AssistantMenuService"

    const-string v7, "onCreate()+"

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1589
    new-instance v3, Landroid/app/Notification;

    invoke-direct {v3}, Landroid/app/Notification;-><init>()V

    .line 1590
    .local v3, "notification":Landroid/app/Notification;
    const/16 v4, 0x40

    iput v4, v3, Landroid/app/Notification;->flags:I

    .line 1592
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mLocale:Ljava/util/Locale;

    .line 1595
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x11

    if-le v4, v7, :cond_4

    .line 1596
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    if-eqz v4, :cond_0

    .line 1598
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v8

    const/4 v9, 0x1

    invoke-interface {v4, v7, v8, v9}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1609
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1610
    .local v2, "filter":Landroid/content/IntentFilter;
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_COPY:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1611
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPAND:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1612
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_COLLAPSE:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1613
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_NOTI:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1614
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_SETTING:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1615
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_OPEN:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1616
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CLOSE:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1617
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1618
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO_TYPE_CHANGED:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1619
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_SHOW:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1620
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_DISMISS:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1621
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SCREEN_ON:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1622
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SCREEN_OFF:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1623
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_COVER_OPEN:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1624
    const-string v4, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1625
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SIDESYNC_START:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1626
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->PLAYER_LOCK:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1627
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1628
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CHANGED:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1629
    const-string v4, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1630
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->DMB_LOCK:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1634
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1635
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mMessenger:Landroid/os/Messenger;

    invoke-virtual {v4}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;

    .line 1639
    new-instance v4, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    .line 1641
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingPrefListner:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v4, v7}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1643
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {p0, v4, v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;

    .line 1645
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 1646
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCallListener:Landroid/telephony/PhoneStateListener;

    const/16 v8, 0x20

    invoke-virtual {v4, v7, v8}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 1648
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getAutoRotation()Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAutoRotation:Z

    .line 1650
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v5, :cond_5

    move v4, v5

    :goto_1
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVertical:Z

    .line 1653
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v7, "com.sec.feature.folder_type"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v7, "com.sec.feature.dual_lcd"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v6, v5

    :cond_1
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mDualFolderType:Z

    .line 1656
    const-string v4, "dominant_hand_type"

    invoke-static {p0, v4, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    .line 1660
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "assistant_menu_dominant_hand_type"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1662
    .local v0, "dominant_hand_type":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    if-eq v0, v4, :cond_2

    .line 1663
    const-string v4, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] assistant_menu_dominant_hand_type not equals:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 1665
    iput v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I

    .line 1668
    :cond_2
    new-instance v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;-><init>(Landroid/app/Service;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .line 1669
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ShowView()V

    .line 1670
    sget-object v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 1672
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "assistant_menu"

    invoke-static {v4, v6, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1674
    const-string v4, "accessibility"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 1675
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;

    invoke-virtual {v4, v6}, Landroid/view/accessibility/AccessibilityManager;->assistantMenuRegister(Landroid/os/IBinder;)V

    .line 1678
    const-string v4, "com.sec.feature.cocktailbar"

    invoke-static {p0, v4}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-ne v4, v5, :cond_3

    .line 1679
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsCocktailBarModel:Z

    .line 1681
    :cond_3
    return-void

    .line 1599
    .end local v0    # "dominant_hand_type":I
    .end local v2    # "filter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v1

    .line 1601
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "AssistantMenuService"

    const-string v7, "error on acessing ActivityManagerService API"

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1605
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_4
    const/16 v4, 0x3e8

    invoke-virtual {p0, v4, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->startForeground(ILandroid/app/Notification;)V

    .line 1606
    const-string v4, "AssistantMenuService"

    const-string v7, "error on acessing startForeground !!!"

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .restart local v2    # "filter":Landroid/content/IntentFilter;
    :cond_5
    move v4, v6

    .line 1650
    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 1708
    const-string v1, "AssistantMenuService"

    const-string v2, "onDestroy()+"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1710
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1712
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAssistantMenuSettingPrefListner:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 1715
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCallListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 1717
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V

    .line 1719
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->RemoveView()V

    .line 1720
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .line 1722
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "assistant_menu"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1725
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-le v1, v2, :cond_0

    .line 1726
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    if-eqz v1, :cond_0

    .line 1728
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1733
    :goto_0
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAm:Landroid/app/IActivityManager;

    .line 1737
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->stopForeground(Z)V

    .line 1738
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v1, :cond_1

    .line 1739
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1, v5}, Landroid/view/accessibility/AccessibilityManager;->assistantMenuRegister(Landroid/os/IBinder;)V

    .line 1743
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCarModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1744
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 1745
    return-void

    .line 1729
    :catch_0
    move-exception v0

    .line 1731
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AssistantMenuService"

    const-string v2, "error on acessing ActivityManagerService API"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 1581
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 1582
    const-string v0, "AssistantMenuService"

    const-string v1, "onStart()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1583
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 1694
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 1697
    iput p3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mStartId:I

    .line 1701
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "car_mode_on"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCarModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1703
    const/4 v0, 0x3

    return v0
.end method

.method public setFMClosedStatus(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 2356
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z

    .line 2357
    return-void
.end method

.method public showTest()V
    .locals 2

    .prologue
    .line 1976
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    if-eqz v0, :cond_0

    .line 1977
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 1980
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-nez v0, :cond_1

    .line 1981
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;-><init>(Landroid/app/Service;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    .line 1983
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ShowView()V

    .line 1985
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 1986
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->shouldOpenMenu:Z

    .line 1987
    return-void
.end method

.method public updateEAM(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 2184
    const-string v5, "ActivityName"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 2185
    const-string v5, "register"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    sput-boolean v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isResume:Z

    .line 2186
    const-string v5, "FragmentName"

    const-string v6, ""

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2187
    .local v1, "fragName":Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 2188
    .local v0, "actName":Ljava/lang/String;
    const-string v5, "AssistantMenuService"

    const-string v6, "updateEAM- invoked from framework iconName:"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2189
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] updateEAM:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2190
    sget-boolean v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isResume:Z

    if-eqz v5, :cond_6

    .line 2191
    iput v10, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    .line 2192
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] updateEAM:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2193
    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getTopActivityPreference(Ljava/lang/String;)V

    .line 2194
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] updateEAM:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2195
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    if-ne v5, v10, :cond_5

    .line 2196
    const-string v5, "IconName"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2197
    .local v2, "iconName":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 2198
    const-string v5, ";"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2199
    .local v3, "icons":[Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 2200
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2202
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2204
    sget v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    array-length v6, v3

    add-int/2addr v5, v6

    sput v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2205
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] add in DS:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2262
    .end local v2    # "iconName":Ljava/lang/String;
    .end local v3    # "icons":[Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    if-eqz v5, :cond_1

    .line 2263
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ReloadView()V

    .line 2265
    :cond_1
    return-void

    .line 2209
    .restart local v2    # "iconName":Ljava/lang/String;
    .restart local v3    # "icons":[Ljava/lang/String;
    :cond_2
    sput-boolean v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    goto :goto_0

    .line 2212
    :cond_3
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2213
    sget v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    array-length v6, v3

    add-int/2addr v5, v6

    sput v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2214
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[c] add in DS:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2218
    :cond_4
    sput-boolean v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    goto :goto_0

    .line 2223
    .end local v2    # "iconName":Ljava/lang/String;
    .end local v3    # "icons":[Ljava/lang/String;
    :cond_5
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 2224
    sput v11, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2225
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 2226
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    .line 2227
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    goto :goto_0

    .line 2230
    :cond_6
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isEamUpdateApp:I

    if-ne v5, v10, :cond_9

    .line 2231
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-eqz v5, :cond_8

    sget-boolean v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    if-nez v5, :cond_8

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    if-eqz v5, :cond_8

    .line 2233
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2235
    .local v4, "keyVal":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 2236
    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2237
    .restart local v3    # "icons":[Ljava/lang/String;
    sget v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    array-length v6, v3

    sub-int/2addr v5, v6

    sput v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2238
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remove in DS:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2253
    .end local v3    # "icons":[Ljava/lang/String;
    .end local v4    # "keyVal":Ljava/lang/String;
    :cond_7
    :goto_1
    sput-boolean v11, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    goto/16 :goto_0

    .line 2242
    :cond_8
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-eqz v5, :cond_7

    sget-boolean v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMultInstance:Z

    if-nez v5, :cond_7

    .line 2244
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2245
    .restart local v4    # "keyVal":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 2246
    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2247
    .restart local v3    # "icons":[Ljava/lang/String;
    sget v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    array-length v6, v3

    sub-int/2addr v5, v6

    sput v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2248
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249
    const-string v5, "AssistantMenuService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remove in DS:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2255
    .end local v3    # "icons":[Ljava/lang/String;
    .end local v4    # "keyVal":Ljava/lang/String;
    :cond_9
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;

    .line 2256
    sput v11, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I

    .line 2257
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 2258
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;

    .line 2259
    const-string v5, ""

    sput-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentPackname:Ljava/lang/String;

    goto/16 :goto_0
.end method
