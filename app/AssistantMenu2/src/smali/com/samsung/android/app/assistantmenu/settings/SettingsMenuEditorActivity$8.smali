.class Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;
.super Ljava/lang/Object;
.source "SettingsMenuEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 369
    const-string v1, "SettingsMenuEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mGridViewTouchListener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 442
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :cond_1
    :goto_1
    return v0

    .line 373
    :pswitch_0
    const-string v0, "SettingsMenuEditorActivity"

    const-string v1, "mGridViewTouchListener_ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 383
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    const-string v1, "SettingsMenuEditorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mGridViewTouchListener - ACTION_MOVE_X: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ACTION_MOVE_Y: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$700(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)F

    move-result v3

    sub-float/2addr v2, v3

    # += operator for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpX:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$616(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F

    .line 388
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$900(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)F

    move-result v3

    sub-float/2addr v2, v3

    # += operator for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mWdigetItemBmpY:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$816(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F

    .line 390
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionX:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$702(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F

    .line 391
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mPrePositionY:F
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$902(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;F)F

    .line 393
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->UpdateWidget()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1000(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    .line 397
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->IsScrollArea()Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 398
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 399
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mScrollDirection:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1300(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$SCROLL_MOTION;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1

    .line 405
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 407
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1400(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 408
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->FindNearItem()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    goto/16 :goto_1

    .line 422
    :pswitch_2
    const-string v1, "SettingsMenuEditorActivity"

    const-string v2, "mGridViewTouchListener_ACTION_CANCEL"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 424
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->RemoveWidget()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1600(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    .line 425
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AssignDragItem()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1700(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    goto/16 :goto_1

    .line 431
    :pswitch_3
    const-string v1, "SettingsMenuEditorActivity"

    const-string v2, "mGridViewTouchListener_ACTION_UP"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsWidgetVisible:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$500(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->RemoveWidget()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1600(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    .line 435
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$8;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AssignDragItem()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1700(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    goto/16 :goto_1

    .line 371
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
