.class public Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
.super Ljava/lang/Object;
.source "AlertDialogUI.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "DialogActivity"

.field private static sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;


# instance fields
.field private mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mDialogMessage:Ljava/lang/String;

.field private mDialogTitle:Ljava/lang/String;

.field private mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mIntent:Landroid/content/Intent;

.field private mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mResource:Landroid/content/res/Resources;

.field private mViewMode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 60
    return-void
.end method

.method private InitDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 111
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_4

    .line 112
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 113
    .local v0, "isLightTheme":Z
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v2, v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0048

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    :goto_1
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0014

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    :goto_2
    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    .line 158
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7de

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 159
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 161
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 162
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    :goto_3
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 174
    .end local v0    # "isLightTheme":Z
    :goto_4
    return-void

    .line 113
    .restart local v0    # "isLightTheme":Z
    :cond_0
    const/4 v1, 0x4

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)V

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)V

    goto :goto_2

    .line 162
    :cond_3
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)V

    goto :goto_3

    .line 171
    .end local v0    # "isLightTheme":Z
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method static synthetic access$002(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-direct {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .line 55
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    return-object v0
.end method

.method public static removeInstance()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->sDialogActivity:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .line 64
    return-void
.end method


# virtual methods
.method public CloseDialog()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    .line 192
    :cond_0
    return-void
.end method

.method public IsDialogDisplay()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x1

    .line 181
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ShowDialog(Landroid/content/Context;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mode"    # Ljava/lang/String;
    .param p3, "okClickListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p4, "cancelClickListener"    # Landroid/content/DialogInterface$OnClickListener;
    .param p5, "dismissListener"    # Landroid/content/DialogInterface$OnDismissListener;

    .prologue
    const v4, 0x7f0a0058

    const v3, 0x7f0a0054

    const v2, 0x7f0a004a

    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mContext:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    .line 83
    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 84
    iput-object p4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mCancelClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 85
    iput-object p5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    .line 103
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->InitDialog()V

    .line 104
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0a0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0a0017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    goto :goto_0

    .line 97
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mViewMode:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0a001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogTitle:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0a0013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->mDialogMessage:Ljava/lang/String;

    goto :goto_0
.end method
