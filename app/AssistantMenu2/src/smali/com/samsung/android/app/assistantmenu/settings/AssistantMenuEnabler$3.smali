.class Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;
.super Ljava/lang/Object;
.source "AssistantMenuEnabler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->showSingleTapModeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

.field final synthetic val$edit:Landroid/content/SharedPreferences$Editor;

.field final synthetic val$mcheck_assistantmenu:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;Landroid/content/SharedPreferences$Editor;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->val$mcheck_assistantmenu:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 194
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    const-string v1, "pref_assistant_noti"

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->val$mcheck_assistantmenu:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 195
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->val$edit:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 196
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_interaction"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$102(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;I)I

    .line 198
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mSingleTapMode:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$100(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)I

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_interaction"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "assistant_menu"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 204
    const-string v0, "AssistantMenuEnabler"

    const-string v1, "Assistant menu switch on"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler$3;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;)Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 206
    return-void
.end method
