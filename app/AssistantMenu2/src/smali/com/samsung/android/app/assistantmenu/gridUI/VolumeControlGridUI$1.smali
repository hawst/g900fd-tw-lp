.class Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;
.super Landroid/os/Handler;
.source "VolumeControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 158
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 173
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    sget-object v3, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->moveVolume(Landroid/media/AudioManager;ILcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;Z)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    goto :goto_0

    .line 163
    :cond_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    sget-object v3, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->DOWN:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->moveVolume(Landroid/media/AudioManager;ILcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;Z)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    goto :goto_0

    .line 167
    :cond_1
    const-string v0, "VolumeControlGridUI"

    const-string v1, "MSG_VOLUME_UPDATE Error"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
