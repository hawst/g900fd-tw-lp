.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;
.super Landroid/content/BroadcastReceiver;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 433
    if-nez p2, :cond_1

    .line 690
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 438
    .local v4, "action":Ljava/lang/String;
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[c] onReceive:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_COPY:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabVisibility()Z

    move-result v20

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_0

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    goto :goto_0

    .line 446
    :cond_2
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_COLLAPSE:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 451
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v16

    .line 452
    .local v16, "mMsg":Landroid/os/Message;
    const/16 v20, 0x5

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/os/Message;->what:I

    .line 453
    const/16 v20, 0x2

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 456
    .end local v16    # "mMsg":Landroid/os/Message;
    :cond_3
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPAND:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 459
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v16

    .line 460
    .restart local v16    # "mMsg":Landroid/os/Message;
    const/16 v20, 0x5

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/os/Message;->what:I

    .line 461
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/os/Message;->arg1:I

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 464
    .end local v16    # "mMsg":Landroid/os/Message;
    :cond_4
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_NOTI:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 469
    :cond_5
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_STATUSBAR_EXPANDED_SETTING:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickpanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickSetting:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenQuickNoti:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 474
    :cond_6
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CLOSE:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2302(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 477
    :cond_7
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_SIDESYNC_START:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 478
    const-string v20, "AssistantMenuService"

    const-string v21, "stop AssistantMenuService by sisdesync"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mStartId:I
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->stopSelf(I)V

    goto/16 :goto_0

    .line 481
    :cond_8
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->PLAYER_LOCK:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 482
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "isLocked"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2402(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 483
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "video player lock, video_locked = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->video_locked:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 485
    :cond_9
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->DMB_LOCK:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "isLocked"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 487
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "dmb lock, dmb_locked = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->dmb_locked:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 489
    :cond_a
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_OPEN:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2302(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_0

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 496
    :cond_b
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_14

    .line 497
    const/4 v9, 0x0

    .line 498
    .local v9, "isOpenIME":Z
    const/4 v10, 0x0

    .line 499
    .local v10, "isVisibleIME":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->IS_MOVABLE_KEYPAD:Ljava/lang/String;

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2602(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 500
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->IS_VISIBLE_WINDOW:Ljava/lang/String;

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "input_method"

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    .line 502
    .local v5, "im":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v9

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "keyguard"

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 505
    .local v11, "km":Landroid/app/KeyguardManager;
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[c] IS_OPEN_IME #1:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", IS_VISIBLE_IME: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", IS_MOVABLE_KEYPAD:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    if-eqz v20, :cond_c

    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v9, v0, :cond_c

    if-nez v10, :cond_c

    .line 508
    const/4 v9, 0x0

    .line 511
    :cond_c
    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v20

    if-eqz v20, :cond_10

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z
    invoke-static {v0, v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2802(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static {v0, v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1202(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_d

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 519
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v20

    if-eqz v20, :cond_e

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 523
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    if-eqz v20, :cond_f

    if-eqz v10, :cond_f

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadxPos()I

    move-result v17

    .line 525
    .local v17, "padXpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadyPos()I

    move-result v18

    .line 526
    .local v18, "padYpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->editCursorPreference(II)V

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 532
    .end local v17    # "padXpos":I
    .end local v18    # "padYpos":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    if-eqz v20, :cond_0

    if-eqz v10, :cond_0

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadxPos()I

    move-result v17

    .line 534
    .restart local v17    # "padXpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadyPos()I

    move-result v18

    .line 535
    .restart local v18    # "padYpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->editCursorPreference(II)V

    .line 536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 541
    .end local v17    # "padXpos":I
    .end local v18    # "padYpos":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    move/from16 v0, v20

    if-eq v0, v9, :cond_0

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static {v0, v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1202(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_11

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 548
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v20

    if-eqz v20, :cond_12

    .line 549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 551
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    if-eqz v20, :cond_13

    if-eqz v9, :cond_13

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadxPos()I

    move-result v17

    .line 553
    .restart local v17    # "padXpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPadyPos()I

    move-result v18

    .line 554
    .restart local v18    # "padYpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->editCursorPreference(II)V

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 559
    .end local v17    # "padXpos":I
    .end local v18    # "padYpos":I
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    if-eqz v20, :cond_0

    if-eqz v9, :cond_0

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadxPos()I

    move-result v17

    .line 561
    .restart local v17    # "padXpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->getPadyPos()I

    move-result v18

    .line 562
    .restart local v18    # "padYpos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->editCursorPreference(II)V

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 569
    .end local v5    # "im":Landroid/view/inputmethod/InputMethodManager;
    .end local v9    # "isOpenIME":Z
    .end local v10    # "isVisibleIME":Z
    .end local v11    # "km":Landroid/app/KeyguardManager;
    .end local v17    # "padXpos":I
    .end local v18    # "padYpos":I
    :cond_14
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_16

    .line 570
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->BTKEYBOARD_EXTRA_STATE:Ljava/lang/String;

    const/16 v21, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 571
    .local v19, "state":I
    const-string v20, "android.bluetooth.profile.extra.isKeyboard"

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 573
    .local v6, "isKeyboard":Z
    if-eqz v6, :cond_0

    .line 574
    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_15

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2702(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 577
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBTKeyboardState:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2702(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    goto/16 :goto_0

    .line 580
    .end local v6    # "isKeyboard":Z
    .end local v19    # "state":I
    :cond_16
    const-string v20, "android.intent.action.USER_PRESENT"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_18

    .line 581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1202(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsVisibleIME:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2802(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_17

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 588
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    goto/16 :goto_0

    .line 590
    :cond_18
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RESPONSE_AXT9INFO_TYPE_CHANGED:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_19

    .line 591
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->IS_MOVABLE_KEYPAD:Ljava/lang/String;

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 592
    .local v7, "isMoveableIME":Z
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[c] IS_VISIBLE_WINDOW #2:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", IS_MOVABLE_KEYPAD:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    move/from16 v0, v20

    if-eq v0, v7, :cond_0

    .line 595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsMoveableIME:Z
    invoke-static {v0, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2602(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    goto/16 :goto_0

    .line 602
    .end local v7    # "isMoveableIME":Z
    :cond_19
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_SHOW:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1c

    .line 603
    const-string v20, "AssistantMenuService"

    const-string v21, "[c] Action: ShowClipboardDialog"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    if-nez v20, :cond_0

    .line 605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1402(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_1a

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 610
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v20

    if-eqz v20, :cond_1b

    .line 611
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 615
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1002(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1502(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto/16 :goto_0

    .line 622
    :cond_1c
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_DISMISS:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1d

    .line 623
    const-string v20, "AssistantMenuService"

    const-string v21, "[c] Action: dismissClipboardDialog"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1402(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    goto/16 :goto_0

    .line 633
    :cond_1d
    const-string v20, "android.intent.action.SCREEN_ON"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1f

    .line 634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "keyguard"

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 635
    .restart local v11    # "km":Landroid/app/KeyguardManager;
    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v20

    if-nez v20, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v20

    if-nez v20, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 638
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 642
    .end local v11    # "km":Landroid/app/KeyguardManager;
    :cond_1f
    const-string v20, "android.intent.action.SCREEN_OFF"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_22

    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "keyguard"

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/KeyguardManager;

    .line 645
    .restart local v11    # "km":Landroid/app/KeyguardManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v21

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2902(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;)Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    .line 646
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2900(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->IsDialogDisplay()Z

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_20

    invoke-virtual {v11}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v20

    if-eqz v20, :cond_20

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAlertDialog:Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2900(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->CloseDialog()V

    .line 648
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V

    .line 651
    :cond_20
    invoke-virtual {v11}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v20

    if-eqz v20, :cond_21

    .line 652
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->clearChildScreensinLockMode()V
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    goto/16 :goto_0

    .line 654
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "lock_screen_lock_after_timeout"

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    move/from16 v0, v20

    int-to-long v12, v0

    .line 656
    .local v12, "lock_timeout":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x65

    const-wide/16 v22, 0x7d0

    add-long v22, v22, v12

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 658
    .end local v11    # "km":Landroid/app/KeyguardManager;
    .end local v12    # "lock_timeout":J
    :cond_22
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_COVER_OPEN:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_24

    .line 659
    const/4 v8, 0x0

    .line 661
    .local v8, "isOpen":Z
    if-eqz p2, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 662
    const-string v20, "coverOpen"

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 663
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[c] Action: ACTION_COVER_OPEN:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    if-eqz v8, :cond_23

    .line 665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    sget-object v21, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    iput-object v0, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_0

    .line 668
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v20

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    goto/16 :goto_0

    .line 672
    .end local v8    # "isOpen":Z
    :cond_24
    sget-object v20, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RECENTSPANEL_CHANGED:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_25

    .line 673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    const-string v21, "isShowing"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z
    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2302(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Z)Z

    .line 674
    const-string v20, "AssistantMenuService"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "[c] Action: ACTION_RECENTSPANEL_CHANGED:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v22, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenRecentspanel:Z
    invoke-static/range {v22 .. v22}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 675
    :cond_25
    const-string v20, "android.intent.action.USER_SWITCHED"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_27

    .line 676
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v14

    .line 677
    .local v14, "mCurrentUserId1":I
    const-string v20, "android.intent.extra.user_handle"

    const/16 v21, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 678
    .local v15, "mCurrentUserId4":I
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "currid"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "broadid"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 679
    if-ne v15, v14, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v20, v0

    if-eqz v20, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/IBinder;

    move-result-object v20

    if-eqz v20, :cond_26

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iBinder:Landroid/os/IBinder;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/IBinder;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/view/accessibility/AccessibilityManager;->assistantMenuRegister(Landroid/os/IBinder;)V

    .line 682
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "FmMagnifier"

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_0

    .line 683
    const-string v20, "AssistantMenuService"

    const-string v21, "user switched! removed Magnifier"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$4;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v20

    const/16 v21, 0x2

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 687
    .end local v14    # "mCurrentUserId1":I
    .end local v15    # "mCurrentUserId4":I
    :cond_27
    const-string v20, "AssistantMenuService"

    const-string v21, "[c] onReceiver not operated!"

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
