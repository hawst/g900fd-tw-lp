.class public Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;
.super Ljava/lang/Object;
.source "AssistantMenuUIAct.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    }
.end annotation


# static fields
.field public static actImageResourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static actStringResourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static tempCnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    .line 77
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020090

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020095

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020098

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    :goto_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0200a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0200a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020094

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0200a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0200a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02009b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02009a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020099

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02009e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020093

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02009f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020091

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020096

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02009c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020092

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0200a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    .line 107
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0046

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0037

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0042

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :goto_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0066

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a004f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0050

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0047

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0051

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a001a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0054

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a000c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a003b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a004a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0026

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0038

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sput v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->tempCnt:I

    return-void

    .line 83
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actImageResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020097

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 115
    :cond_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a003f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    .locals 2
    .param p0, "service"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .param p1, "act"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v0

    .line 145
    .local v0, "handle":Landroid/os/Handler;
    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 146
    return-void
.end method
