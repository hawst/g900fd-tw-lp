.class Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;
.super Ljava/lang/Object;
.source "VolumeControlGridUI.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 688
    if-eqz p3, :cond_5

    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-nez v0, :cond_0

    if-ge p2, v3, :cond_0

    .line 690
    const-string v0, "VolumeControlGridUI"

    const-string v1, "onProgressChanged, now allowed set volume under MINVALUE during call"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 721
    :goto_0
    return-void

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 696
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-nez v0, :cond_1

    .line 697
    add-int/lit8 p2, p2, -0x1

    .line 699
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    invoke-static {v1, v2, p2}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->setVolume(Landroid/media/AudioManager;II)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 706
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-nez v0, :cond_2

    .line 707
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # operator++ for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$008(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    .line 710
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    .line 712
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 717
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->setRingerMode(Landroid/media/AudioManager;I)V

    .line 719
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 720
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    goto :goto_0

    .line 714
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 715
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    goto :goto_1
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 725
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 726
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 727
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 7
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x2

    .line 732
    const/4 v0, 0x4

    .line 733
    .local v0, "mSound":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 734
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 737
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 738
    const/4 v0, 0x0

    .line 741
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 742
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v3, v5, v0}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 748
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$902(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 749
    return-void

    .line 744
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-ne v1, v6, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 745
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v6, v5, v0}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0
.end method
