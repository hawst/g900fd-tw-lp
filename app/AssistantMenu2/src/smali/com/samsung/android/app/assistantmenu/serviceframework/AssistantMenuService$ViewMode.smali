.class final enum Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;
.super Ljava/lang/Enum;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum DeviceOption:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum PowerOff:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum RecentappList:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum Restart:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum Setting:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

.field public static final enum ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "GridMenuUI"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "ZoomControl"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "VolumeControl"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "Restart"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->Restart:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "BrightnessControl"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "RotateControl"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "PowerOff"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->PowerOff:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "Setting"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->Setting:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "RecentappList"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RecentappList:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "DeviceOption"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->DeviceOption:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "FingerMouse"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    const-string v1, "HoverZoom"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    .line 85
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->Restart:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->PowerOff:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->Setting:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RecentappList:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->DeviceOption:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 85
    const-class v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v0}, [Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    return-object v0
.end method
