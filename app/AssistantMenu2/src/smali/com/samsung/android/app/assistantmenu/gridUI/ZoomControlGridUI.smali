.class public Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
.super Ljava/lang/Object;
.source "ZoomControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;,
        Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;
    }
.end annotation


# instance fields
.field private final MSG_GRID_SEEKBAR_DISABLE:I

.field private final MSG_GRID_SEEKBAR_ENABLE:I

.field private final MSG_GRID_SEEKBAR_SET_PROGRESS:I

.field private final MSG_GRID_SEEKBAR_ZOOM:I

.field private final SEEKBAR_POSITION_CENTER:I

.field private final SEEKBAR_POSITION_HIGH:I

.field private final SEEKBAR_POSITION_LOW:I

.field private final SEEKBAR_POSITION_MIDDLE:I

.field private final TAG:Ljava/lang/String;

.field private final ZOOM_SCALE_PER_CLICK:I

.field private mBounceDegree:F

.field private mBtnZoomIn:Landroid/widget/ImageButton;

.field private mBtnZoomOut:Landroid/widget/ImageButton;

.field private mCompleteBounce:Z

.field private mCompleteZoom:Z

.field private mDisplaySize:Landroid/graphics/Point;

.field private mFakeAction:Z

.field mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mIBtnZoomRemoveView:Landroid/widget/LinearLayout;

.field private mIVFakeZoomPointLeft:Landroid/widget/ImageView;

.field private mIVFakeZoomPointRight:Landroid/widget/ImageView;

.field private mIVZoomPoint:Landroid/widget/ImageView;

.field private mInitZoomPointX:F

.field private mInitZoomPointY:F

.field private mIsExpand:Z

.field private mIsTouchable:Z

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mParentSvcHandler:Landroid/os/Handler;

.field private mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

.field private mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarCurrentPosition:I

.field private final mSeekBarEventHandler:Landroid/os/Handler;

.field private mService:Landroid/app/Service;

.field private mWm:Landroid/view/WindowManager;

.field mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mZoomControlView:Landroid/widget/RelativeLayout;

.field private mZoomControlViewLimitX:I

.field private mZoomControlViewLimitY:I

.field private mZoomControlViewMain:Landroid/widget/RelativeLayout;

.field mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mZoomPointX:F

.field private mZoomPointXmove:F

.field private mZoomPointY:F

.field private mZoomPointYmove:F

.field private mZoomRatio:I

.field private mZoomSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mZoomTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 5
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v1, "ZoomControlGridUI"

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->TAG:Ljava/lang/String;

    .line 44
    const/16 v1, 0x64

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->SEEKBAR_POSITION_CENTER:I

    .line 46
    const/16 v1, 0x4b

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->SEEKBAR_POSITION_HIGH:I

    .line 48
    const/16 v1, 0x32

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->SEEKBAR_POSITION_MIDDLE:I

    .line 50
    const/16 v1, 0x19

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->SEEKBAR_POSITION_LOW:I

    .line 52
    const/16 v1, 0x14

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZOOM_SCALE_PER_CLICK:I

    .line 54
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    .line 56
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewMain:Landroid/widget/RelativeLayout;

    .line 62
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    .line 64
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    .line 66
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 68
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 70
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 72
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    .line 74
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    .line 76
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIBtnZoomRemoveView:Landroid/widget/LinearLayout;

    .line 78
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    .line 80
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    .line 82
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    .line 84
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    .line 86
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    .line 88
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomTitle:Landroid/widget/TextView;

    .line 90
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    .line 92
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    .line 94
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 96
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 98
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    .line 100
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    .line 115
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I

    .line 122
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 124
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    .line 126
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    .line 394
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MSG_GRID_SEEKBAR_DISABLE:I

    .line 396
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MSG_GRID_SEEKBAR_ZOOM:I

    .line 398
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MSG_GRID_SEEKBAR_SET_PROGRESS:I

    .line 400
    const/4 v1, 0x3

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MSG_GRID_SEEKBAR_ENABLE:I

    .line 402
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;

    .line 454
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 134
    const-string v1, "ZoomControlGridUI"

    const-string v2, "ZoomControlGridUI(Service service)"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    .line 136
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->init(Landroid/content/Context;)V

    move-object v0, p1

    .line 138
    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 139
    .local v0, "svc":Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 140
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 141
    return-void
.end method

.method private EndFakeZoomPointerAction()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1201
    const-string v0, "ZoomControlGridUI"

    const-string v1, "EndFakeZoomPointerAction()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1203
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z

    .line 1205
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1206
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    .line 1208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 1209
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1210
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1211
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    .line 1215
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1216
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    .line 1218
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    .line 1219
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1220
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1221
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    .line 1224
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 1225
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1227
    :cond_4
    return-void
.end method

.method private GetDistanceFromMiddle(II)I
    .locals 3
    .param p1, "currentPosition"    # I
    .param p2, "middlePostion"    # I

    .prologue
    .line 506
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetDistanceFromMiddle() :: currentPosition = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", middlePostion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method private Init()V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x0

    const v10, 0x7f090074

    const/4 v9, 0x1

    const/4 v1, -0x2

    .line 148
    const-string v0, "ZoomControlGridUI"

    const-string v2, "Init()"

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const v2, 0x7f030016

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d006a

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewMain:Landroid/widget/RelativeLayout;

    .line 162
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const v4, 0x1000028

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f09005a

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 174
    .local v8, "displayWidth":I
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/Service;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 176
    .local v6, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int v2, v8, v2

    invoke-interface {v6, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 181
    .local v7, "cornertabX":I
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v0}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f09005a

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    if-le v7, v0, :cond_3

    .line 185
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v2, 0x800055

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 200
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09006e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 202
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090075

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 208
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 213
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 214
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v12

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 217
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_4

    .line 218
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v12

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 228
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 229
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 231
    iput v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    .line 232
    iput v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    .line 234
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0070

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    .line 235
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0016

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d006d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIBtnZoomRemoveView:Landroid/widget/LinearLayout;

    .line 238
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d006c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomTitle:Landroid/widget/TextView;

    .line 240
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    .line 241
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    const v1, 0x7f0200be

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 242
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 244
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 249
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIBtnZoomRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIBtnZoomRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 258
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomSeekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 262
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 263
    const/16 v0, 0x64

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    .line 264
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 266
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    .line 267
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    .line 269
    iput-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z

    .line 271
    return-void

    .line 170
    .end local v6    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v7    # "cornertabX":I
    .end local v8    # "displayWidth":I
    :cond_1
    const v0, 0x7f090059

    goto/16 :goto_0

    .line 181
    .restart local v6    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v7    # "cornertabX":I
    .restart local v8    # "displayWidth":I
    :cond_2
    const v0, 0x7f090059

    goto/16 :goto_1

    .line 189
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v2, 0x800053

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto/16 :goto_2

    .line 221
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    goto/16 :goto_3
.end method

.method private MakeAnimationZoomPointer(F)V
    .locals 20
    .param p1, "ratio"    # F

    .prologue
    .line 1044
    const-string v2, "ZoomControlGridUI"

    const-string v3, "MakeAnimationZoomPointer()"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z

    if-eqz v2, :cond_0

    .line 1046
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->EndFakeZoomPointerAction()V

    .line 1048
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z

    .line 1050
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-nez v2, :cond_2

    .line 1194
    :cond_1
    :goto_0
    return-void

    .line 1054
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    float-to-int v0, v2

    move/from16 v19, v0

    .line 1055
    .local v19, "zoomPointX":I
    if-gez v19, :cond_3

    .line 1056
    const/16 v19, 0x0

    .line 1057
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    move/from16 v0, v19

    if-le v0, v2, :cond_4

    .line 1058
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    sub-int v19, v2, v3

    .line 1061
    :cond_4
    new-instance v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    .line 1062
    new-instance v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    .line 1063
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    const v3, 0x7f0200bc

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1064
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1067
    new-instance v2, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    .line 1068
    new-instance v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    .line 1069
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    const v3, 0x7f0200bc

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1073
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    sget v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const/16 v6, 0x28

    const/4 v7, -0x3

    invoke-direct/range {v2 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 1077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v3, 0x800033

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1078
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v3, v3, v19

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1081
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    float-to-int v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v4}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09006d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1085
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointLeft:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mRLFakeZoomPointRight:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1090
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1091
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1094
    const/16 v16, 0x0

    .line 1095
    .local v16, "tranAniLeft":Landroid/view/animation/TranslateAnimation;
    const/16 v17, 0x0

    .line 1097
    .local v17, "tranAniRight":Landroid/view/animation/TranslateAnimation;
    const/4 v2, 0x2

    new-array v12, v2, [I

    .line 1099
    .local v12, "location":[I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 1102
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 1106
    const/4 v2, 0x0

    aget v2, v12, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090068

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v9, v2, v3

    .line 1109
    .local v9, "centerPoint":I
    int-to-float v2, v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, p1, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v11, v2

    .line 1111
    .local v11, "leftPoint":I
    if-gez v11, :cond_5

    .line 1112
    const/4 v11, 0x0

    .line 1116
    :cond_5
    int-to-float v2, v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090072

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v15, v2

    .line 1118
    .local v15, "rightPoint":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090073

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    if-le v15, v2, :cond_6

    .line 1121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090073

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v15, v2, v3

    .line 1126
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    .line 1127
    .local v10, "config":Landroid/content/res/Configuration;
    invoke-virtual {v10}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 1128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09006c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 1130
    .local v8, "boundary":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    sub-int v14, v2, v8

    .line 1131
    .local v14, "rightBoundary":I
    const/4 v2, 0x0

    aget v2, v12, v2

    add-int/2addr v2, v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090067

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int v13, v2, v3

    .line 1134
    .local v13, "location_RTL":I
    sub-int v9, v13, v14

    .line 1136
    int-to-float v2, v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v15, v2

    .line 1138
    if-lez v15, :cond_7

    .line 1139
    const/4 v15, 0x0

    .line 1142
    :cond_7
    int-to-float v2, v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v3}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090072

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float v4, p1, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v11, v2

    .line 1145
    .end local v8    # "boundary":I
    .end local v13    # "location_RTL":I
    .end local v14    # "rightBoundary":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v2}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090076

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 1147
    .local v18, "yPoint":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z

    if-eqz v2, :cond_9

    .line 1148
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "tranAniLeft":Landroid/view/animation/TranslateAnimation;
    int-to-float v2, v9

    int-to-float v3, v11

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v18

    int-to-float v5, v0

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1149
    .restart local v16    # "tranAniLeft":Landroid/view/animation/TranslateAnimation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "tranAniRight":Landroid/view/animation/TranslateAnimation;
    int-to-float v2, v9

    int-to-float v3, v15

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v18

    int-to-float v5, v0

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1155
    .restart local v17    # "tranAniRight":Landroid/view/animation/TranslateAnimation;
    :goto_1
    const/high16 v2, 0x40400000    # 3.0f

    mul-float v2, v2, p1

    float-to-long v2, v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1156
    const/high16 v2, 0x40400000    # 3.0f

    mul-float v2, v2, p1

    float-to-long v2, v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1158
    new-instance v2, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$5;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1187
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 1188
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 1189
    const/4 v2, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 1190
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/animation/TranslateAnimation;->setFillBefore(Z)V

    .line 1192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 1151
    :cond_9
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "tranAniLeft":Landroid/view/animation/TranslateAnimation;
    int-to-float v2, v11

    int-to-float v3, v9

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v18

    int-to-float v5, v0

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1152
    .restart local v16    # "tranAniLeft":Landroid/view/animation/TranslateAnimation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "tranAniRight":Landroid/view/animation/TranslateAnimation;
    int-to-float v2, v15

    int-to-float v3, v9

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v18

    int-to-float v5, v0

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v17    # "tranAniRight":Landroid/view/animation/TranslateAnimation;
    goto :goto_1
.end method

.method private ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    .locals 3
    .param p1, "mode"    # Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .param p2, "ratio"    # F

    .prologue
    .line 681
    const-string v0, "ZoomControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ZoomControl() with mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 684
    invoke-direct {p0, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->MakeAnimationZoomPointer(F)V

    .line 686
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 697
    return-void
.end method

.method private ZoomWithBounce()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 490
    const-string v0, "ZoomControlGridUI"

    const-string v3, "ZoomWithBounce()"

    invoke-static {v0, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    if-le v0, v4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z

    .line 492
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->GetDistanceFromMiddle(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I

    .line 493
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 494
    new-instance v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$BounceActionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 495
    return-void

    :cond_0
    move v0, v2

    .line 491
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mFakeAction:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->EndFakeZoomPointerAction()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    return p1
.end method

.method static synthetic access$1212(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    return v0
.end method

.method static synthetic access$1220(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarCurrentPosition:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomWithBounce()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;II)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->GetDistanceFromMiddle(II)I

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBounceDegree:F

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    return v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    return v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    return v0
.end method

.method static synthetic access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    return p1
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomRatio:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    .param p1, "x1"    # Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .param p2, "x2"    # F

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    return-void
.end method


# virtual methods
.method public RemoveView()V
    .locals 3

    .prologue
    .line 311
    const-string v1, "ZoomControlGridUI"

    const-string v2, "RemoveView()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 315
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 318
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    .line 320
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 321
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 323
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :cond_1
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v0

    .line 326
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public RemoveViewWithoutPokeWakelock()V
    .locals 3

    .prologue
    .line 334
    const-string v1, "ZoomControlGridUI"

    const-string v2, "RemoveView()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 341
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 344
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 346
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :cond_1
    :goto_0
    return-void

    .line 348
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public ScreenRotateStatusChanged()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const v2, 0x7f090074

    .line 705
    const-string v0, "ZoomControlGridUI"

    const-string v1, "ScreenRotateStatusChanged()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 735
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 738
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-ge v0, v1, :cond_1

    .line 739
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    div-float/2addr v0, v3

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 751
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 752
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 754
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 757
    :cond_0
    return-void

    .line 742
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    goto :goto_0
.end method

.method public ShowView()V
    .locals 4

    .prologue
    .line 278
    const-string v1, "ZoomControlGridUI"

    const-string v2, "ShowView()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->Init()V

    .line 281
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 283
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    const v2, 0x7f040003

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 284
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 303
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 304
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v3, 0x41a00000    # 20.0f

    const/4 v2, 0x1

    .line 357
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 390
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 391
    return-void

    .line 360
    :pswitch_1
    const-string v0, "ZoomControlGridUI"

    const-string v1, "onClick() :: R.id.gridZoomControlButtonPlus"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z

    .line 362
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z

    .line 363
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    goto :goto_0

    .line 368
    :pswitch_2
    const-string v0, "ZoomControlGridUI"

    const-string v1, "onClick() :: R.id.gridZoomControlButtonMinus"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsExpand:Z

    .line 370
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z

    .line 371
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    goto :goto_0

    .line 375
    :pswitch_3
    const-string v0, "ZoomControlGridUI"

    const-string v1, "onClick() :: R.id.grid_ZoomClosed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 377
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointLeft:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVFakeZoomPointRight:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    goto :goto_0

    .line 357
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d006d
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 22
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 513
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIsTouchable:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 514
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v17

    const v18, 0x7f0d0069

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getId()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    .line 515
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v17

    packed-switch v17, :pswitch_data_0

    .line 640
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v16

    .line 642
    .local v16, "viewID":I
    const v17, 0x7f0d0070

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_1

    const v17, 0x7f0d006f

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 644
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v17

    packed-switch v17, :pswitch_data_1

    .line 669
    .end local v16    # "viewID":I
    :cond_2
    :goto_1
    const/16 v17, 0x0

    return v17

    .line 517
    :pswitch_0
    const-string v17, "ZoomControlGridUI"

    const-string v18, "onTouch() :: ACTION_DOWN"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    .line 519
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    .line 521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    .line 522
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    .line 524
    const-string v17, "ZoomControlGridUI"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mInitZoomPointX : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", mInitZoomPointY : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    const v18, 0x7f0200c0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 532
    :pswitch_1
    const-string v17, "ZoomControlGridUI"

    const-string v18, "onTouch() :: ACTION_UP"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f09006b

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    add-int v18, v18, v19

    sub-int v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    .line 537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_3

    .line 538
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f090069

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    add-int v19, v19, v20

    sub-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-lez v17, :cond_5

    .line 544
    new-instance v17, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v19

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;FF)V

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 550
    :cond_3
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v8, v0, [I

    .line 551
    .local v8, "location":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    .line 552
    const/16 v17, 0x0

    aget v17, v8, v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    .line 553
    const/16 v17, 0x1

    aget v17, v8, v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 557
    const v5, 0x800053

    .line 558
    .local v5, "bottomLeft":I
    const/16 v17, 0x0

    aget v17, v8, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v18, v0

    const/high16 v19, 0x41300000    # 11.0f

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v18

    sub-int v11, v17, v18

    .line 559
    .local v11, "pointX":I
    const/16 v17, 0x1

    aget v17, v8, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getHeight()I

    move-result v18

    add-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v18, v0

    const/high16 v19, 0x41300000    # 11.0f

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v18

    sub-int v12, v17, v18

    .line 561
    .local v12, "pointY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v5, :cond_4

    .line 562
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlView:Landroid/widget/RelativeLayout;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    .line 564
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v0, v11, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v12, :cond_5

    .line 566
    new-instance v17, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v19

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;FF)V

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 571
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getWidth()I

    move-result v17

    add-int v11, v11, v17

    .line 572
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitX:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v11, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomControlViewLimitY:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v12, :cond_5

    .line 574
    new-instance v17, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v18

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v19

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;FF)V

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Void;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$ComeBackZoomPointerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 581
    .end local v5    # "bottomLeft":I
    .end local v8    # "location":[I
    .end local v11    # "pointX":I
    .end local v12    # "pointY":I
    :cond_5
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    .line 582
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    .line 583
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointX:F

    .line 584
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mInitZoomPointY:F

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    const v18, 0x7f0200be

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 590
    :pswitch_2
    const-string v17, "ZoomControlGridUI"

    const-string v18, "onTouch() :: ACTION_MOVE"

    invoke-static/range {v17 .. v18}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v9

    .line 592
    .local v9, "newX":F
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    .line 593
    .local v10, "newY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mService:Landroid/app/Service;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f09006c

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 596
    .local v6, "boundary":I
    move v7, v6

    .line 597
    .local v7, "leftBoundary":I
    move v15, v6

    .line 598
    .local v15, "topBoundary":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    sub-int v13, v17, v6

    .line 599
    .local v13, "rightBoundary":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mDisplaySize:Landroid/graphics/Point;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    sub-int v4, v17, v6

    .line 601
    .local v4, "bottomBoundary":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    move/from16 v18, v0

    sub-float v18, v9, v18

    add-float v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 602
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    move/from16 v18, v0

    sub-float v18, v10, v18

    add-float v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 604
    const/4 v14, 0x0

    .line 606
    .local v14, "skipUpdate":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    move/from16 v17, v0

    int-to-float v0, v7

    move/from16 v18, v0

    cmpg-float v17, v17, v18

    if-gez v17, :cond_6

    .line 607
    int-to-float v0, v7

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 609
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    move/from16 v17, v0

    int-to-float v0, v15

    move/from16 v18, v0

    cmpg-float v17, v17, v18

    if-gez v17, :cond_7

    .line 610
    int-to-float v0, v15

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 612
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getWidth()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-float v17, v17, v18

    int-to-float v0, v13

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-lez v17, :cond_8

    .line 613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getWidth()I

    move-result v17

    sub-int v17, v13, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    .line 615
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getHeight()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-float v17, v17, v18

    int-to-float v0, v4

    move/from16 v18, v0

    cmpl-float v17, v17, v18

    if-lez v17, :cond_9

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getHeight()I

    move-result v17

    sub-int v17, v4, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    .line 619
    :cond_9
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointXmove:F

    .line 620
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointYmove:F

    .line 622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 624
    const/4 v14, 0x1

    .line 627
    :cond_a
    if-nez v14, :cond_0

    .line 628
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointX:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointY:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mWm:Landroid/view/WindowManager;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    move-object/from16 v19, v0

    invoke-interface/range {v17 .. v19}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 646
    .end local v4    # "bottomBoundary":I
    .end local v6    # "boundary":I
    .end local v7    # "leftBoundary":I
    .end local v9    # "newX":F
    .end local v10    # "newY":F
    .end local v13    # "rightBoundary":I
    .end local v14    # "skipUpdate":Z
    .end local v15    # "topBoundary":I
    .restart local v16    # "viewID":I
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 647
    const v17, 0x7f0d0070

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_b

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_1

    .line 649
    :cond_b
    const v17, 0x7f0d006f

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_1

    .line 655
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 656
    const v17, 0x7f0d0070

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_c

    .line 657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomOut:Landroid/widget/ImageButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_1

    .line 658
    :cond_c
    const v17, 0x7f0d006f

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mBtnZoomIn:Landroid/widget/ImageButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_1

    .line 515
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 644
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public updateZoomText()V
    .locals 2

    .prologue
    .line 1234
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1235
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomTitle:Landroid/widget/TextView;

    const v1, 0x7f0a006e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1236
    :cond_0
    return-void
.end method
