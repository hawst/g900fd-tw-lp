.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;
.super Landroid/database/ContentObserver;
.source "FingerMouseGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v3, 0x0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_magnifier_size"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 172
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_value"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I

    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v2

    aget v1, v1, v2

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->enableMagnifier(IIF)V

    .line 174
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V

    .line 175
    const-string v0, "FingerMouseGridUIHoverZoom"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHoverScaleObserver mScaleSize = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    return-void
.end method
