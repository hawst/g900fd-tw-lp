.class public Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;
.super Landroid/view/View;
.source "CursorView.java"


# instance fields
.field cursor:Landroid/graphics/Bitmap;

.field mShowCursor:Z

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 23
    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->x:F

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->y:F

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setDrawingCacheEnabled(Z)V

    .line 41
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 42
    return-void
.end method


# virtual methods
.method public Update(FF)V
    .locals 0
    .param p1, "nx"    # F
    .param p2, "ny"    # F

    .prologue
    .line 26
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->x:F

    .line 27
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->y:F

    .line 28
    return-void
.end method

.method public getCursorHeight()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    return v0
.end method

.method public getCursorWidth()I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    return v0
.end method

.method public isCursorShown()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->mShowCursor:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 80
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->mShowCursor:Z

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->x:F

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->y:F

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 82
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I
    .param p5, "arg4"    # I

    .prologue
    .line 95
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public setCursorImage(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "size"    # I

    .prologue
    .line 57
    if-nez p2, :cond_0

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003c

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    .line 64
    :goto_0
    return-void

    .line 61
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020039

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setCursorStatus(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->mShowCursor:Z

    .line 32
    return-void
.end method

.method public setMagnifierCursorImage(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02016b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    .line 52
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate()V

    .line 53
    return-void
.end method

.method public setNotificationCursorImage(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "size"    # I

    .prologue
    .line 68
    if-nez p2, :cond_0

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    .line 75
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->cursor:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public showCursor(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setCursorStatus(Z)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate()V

    .line 47
    return-void
.end method
