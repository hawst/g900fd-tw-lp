.class public Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
.super Ljava/lang/Object;
.source "VolumeControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final ALPHA_BACKGROUND:I

.field private final ALPHA_DIM_HALF:I

.field private final ALPHA_DIM_QUARTER:I

.field private final ALPHA_NORMAL:I

.field private final ALPHA_SELECTED:I

.field private final ARG_VOLUME_DOWN:I

.field private final ARG_VOLUME_UP:I

.field private final COLOR_BACKGROUND:I

.field private final COLOR_DIM:I

.field private final COLOR_DIM_TEXT:I

.field private final COLOR_NORMAL:I

.field private final COLOR_SELECTED:I

.field private final MIN_VOICECALL_VOLUME:I

.field private final MSG_TOUCH_VOLUME_UPDATE:I

.field private final MSG_VOLUME_UPDATE:I

.field private final TAG:Ljava/lang/String;

.field private final VOLUME_STREAM_MUSIC_HEIGHT:I

.field private final VOLUME_STREAM_RING_HEIGHT:I

.field private isDimUI:Z

.field private mAccelRateVal:F

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBtnSound:Landroid/widget/ImageButton;

.field private mBtnVibrate:Landroid/widget/ImageButton;

.field private mBtnVolumeDown:Landroid/widget/ImageButton;

.field private mBtnVolumeUp:Landroid/widget/ImageButton;

.field private mButtonBackground:Landroid/widget/ImageView;

.field private mButtonMinus:Landroid/widget/ImageView;

.field private mButtonPlus:Landroid/widget/ImageView;

.field private mCurStreamType:I

.field private mCurVolumeState:I

.field private mCurVolumeVal:I

.field private final mHandler:Landroid/os/Handler;

.field private mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

.field private mIVSoundIcon:Landroid/widget/ImageView;

.field private mIVVibrateIcon:Landroid/widget/ImageView;

.field private mIsLongkeyProcessing:Z

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private final mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mParentSvcHandler:Landroid/os/Handler;

.field private mPrevVolumeState:I

.field mReceiver:Landroid/content/BroadcastReceiver;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mService:Landroid/app/Service;

.field private mSound:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mVibrate:Landroid/widget/TextView;

.field private mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

.field private final mVolumeControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

.field private mVolumeControlMainLayoutParam:Landroid/widget/RelativeLayout$LayoutParams;

.field private mVolumeStreamHeight:I


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 7
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v6, -0x1

    const/high16 v5, -0x1000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-string v1, "VolumeControlGridUI"

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->TAG:Ljava/lang/String;

    .line 55
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->MIN_VOICECALL_VOLUME:I

    .line 57
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->MSG_VOLUME_UPDATE:I

    .line 59
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->MSG_TOUCH_VOLUME_UPDATE:I

    .line 61
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ARG_VOLUME_UP:I

    .line 63
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ARG_VOLUME_DOWN:I

    .line 67
    const v1, 0x7f09005e

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->VOLUME_STREAM_RING_HEIGHT:I

    .line 69
    const v1, 0x7f09005d

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->VOLUME_STREAM_MUSIC_HEIGHT:I

    .line 71
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->COLOR_BACKGROUND:I

    .line 73
    const v1, -0xb451b0

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->COLOR_SELECTED:I

    .line 75
    const v1, -0xafafb0

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->COLOR_NORMAL:I

    .line 77
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->COLOR_DIM:I

    .line 79
    const/high16 v1, -0x78000000

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->COLOR_DIM_TEXT:I

    .line 81
    const/16 v1, 0xff

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ALPHA_SELECTED:I

    .line 83
    const/16 v1, 0x66

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ALPHA_NORMAL:I

    .line 85
    const/16 v1, 0x19

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ALPHA_BACKGROUND:I

    .line 87
    const/16 v1, 0x80

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ALPHA_DIM_HALF:I

    .line 89
    const/16 v1, 0x40

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ALPHA_DIM_QUARTER:I

    .line 91
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    .line 93
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    .line 95
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    .line 97
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayoutParam:Landroid/widget/RelativeLayout$LayoutParams;

    .line 99
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 103
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    .line 105
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    .line 107
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    .line 109
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mTitle:Landroid/widget/TextView;

    .line 111
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVibrate:Landroid/widget/TextView;

    .line 113
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSound:Landroid/widget/TextView;

    .line 115
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    .line 117
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    .line 119
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

    .line 121
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    .line 124
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 126
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    .line 128
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    .line 130
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I

    .line 132
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    .line 134
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAccelRateVal:F

    .line 136
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z

    .line 138
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    .line 140
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    .line 142
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonBackground:Landroid/widget/ImageView;

    .line 144
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 146
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonPlus:Landroid/widget/ImageView;

    .line 148
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonMinus:Landroid/widget/ImageView;

    .line 150
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->isDimUI:Z

    .line 155
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mHandler:Landroid/os/Handler;

    .line 598
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 684
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 764
    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 180
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    move-object v0, p1

    .line 182
    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 183
    .local v0, "svc":Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 184
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 185
    return-void
.end method

.method private ClickOutSideLayout()V
    .locals 2

    .prologue
    .line 757
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 758
    return-void
.end method

.method private Init()V
    .locals 13

    .prologue
    .line 288
    invoke-static {}, Landroid/media/AudioManager;->getActiveStreamType()I

    move-result v10

    iput v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    .line 289
    iget v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3

    .line 290
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const v11, 0x7f03000f

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    .line 292
    const v10, 0x7f09005e

    iput v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    .line 299
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0032

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    .line 304
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    check-cast v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v10

    if-eqz v10, :cond_4

    const v10, 0x7f09005a

    :goto_1
    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 307
    .local v6, "displayWidth":I
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    check-cast v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v10

    if-eqz v10, :cond_5

    const v10, 0x7f090059

    :goto_2
    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 311
    .local v5, "displayHeight":I
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    sget-object v11, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/app/Service;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 313
    .local v1, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v10, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v11}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090015

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int v11, v6, v11

    invoke-interface {v1, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 317
    .local v3, "cornertabX":I
    sget-object v10, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-interface {v1, v10, v11}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 318
    .local v4, "cornertabY":I
    sget-object v10, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    invoke-interface {v1, v10, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 325
    .local v2, "cornertabDisplayHeight":I
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090066

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v11}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget v12, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-direct {v7, v10, v11}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 329
    .local v7, "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    check-cast v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v10

    if-eqz v10, :cond_6

    const v10, 0x7f09005a

    :goto_3
    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    if-le v3, v10, :cond_7

    .line 333
    const/16 v10, 0xb

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 334
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090062

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 344
    :goto_4
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09005f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 347
    iget v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v11}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget v12, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    if-le v4, v10, :cond_0

    .line 349
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    sub-int v10, v4, v10

    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 353
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09005f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    sub-int v10, v2, v10

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v11}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget v12, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    sub-int v8, v10, v11

    .line 356
    .local v8, "maxCornertabBottomMargin":I
    iget v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    if-le v10, v8, :cond_8

    .end local v8    # "maxCornertabBottomMargin":I
    :goto_5
    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 359
    const/16 v10, 0xc

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 361
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 367
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0039

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    .line 369
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0037

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    .line 371
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0035

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

    .line 375
    iget v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-direct {p0, v10}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->SetCurrentVolumeStreamTypeIcon(I)V

    .line 377
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0016

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/SeekBar;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    .line 378
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v10, v11}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 380
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 381
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 382
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v10, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    iget v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_1

    .line 386
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0060

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    .line 388
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    invoke-virtual {v10}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    const/high16 v11, -0x1000000

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 389
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    invoke-virtual {v10}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    const/16 v11, 0x19

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 390
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d005d

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    .line 392
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    invoke-virtual {v10}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    const/high16 v11, -0x1000000

    sget-object v12, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 393
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    invoke-virtual {v10}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v10

    const/16 v11, 0x19

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 394
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0035

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

    .line 397
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d005e

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    .line 399
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0061

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    .line 402
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d003a

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonPlus:Landroid/widget/ImageView;

    .line 403
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0038

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonMinus:Landroid/widget/ImageView;

    .line 405
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 406
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnSound:Landroid/widget/ImageButton;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 408
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVibrate:Landroid/widget/ImageButton;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 410
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d005f

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVibrate:Landroid/widget/TextView;

    .line 411
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0062

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSound:Landroid/widget/TextView;

    .line 422
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 423
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    iget-object v11, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 425
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 426
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 428
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 429
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v10}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayoutParam:Landroid/widget/RelativeLayout$LayoutParams;

    .line 432
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    const v11, 0x7f0d0031

    invoke-virtual {v10, v11}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 434
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const-string v11, "audio"

    invoke-virtual {v10, v11}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/media/AudioManager;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    .line 436
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIBtnVolumeRemoveView:Landroid/widget/LinearLayout;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setHoverPopupType(I)V

    .line 438
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->SystemSettingAllSoundOff:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 440
    .local v0, "allSoundOff":I
    const/4 v10, 0x1

    if-ne v0, v10, :cond_2

    .line 442
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 443
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f0a0061

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    .line 444
    .local v9, "soundoffToast":Landroid/widget/Toast;
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 456
    .end local v9    # "soundoffToast":Landroid/widget/Toast;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->UpdateUI()V

    .line 457
    return-void

    .line 294
    .end local v0    # "allSoundOff":I
    .end local v1    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v2    # "cornertabDisplayHeight":I
    .end local v3    # "cornertabX":I
    .end local v4    # "cornertabY":I
    .end local v5    # "displayHeight":I
    .end local v6    # "displayWidth":I
    .end local v7    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const v11, 0x7f03000b

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout;

    iput-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    .line 296
    const v10, 0x7f09005d

    iput v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeStreamHeight:I

    goto/16 :goto_0

    .line 304
    :cond_4
    const v10, 0x7f090059

    goto/16 :goto_1

    .line 307
    .restart local v6    # "displayWidth":I
    :cond_5
    const v10, 0x7f09005a

    goto/16 :goto_2

    .line 329
    .restart local v1    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v2    # "cornertabDisplayHeight":I
    .restart local v3    # "cornertabX":I
    .restart local v4    # "cornertabY":I
    .restart local v5    # "displayHeight":I
    .restart local v7    # "layoutparams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    const v10, 0x7f090059

    goto/16 :goto_3

    .line 339
    :cond_7
    const/16 v10, 0x9

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 340
    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v10}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f090061

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iput v10, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto/16 :goto_4

    .line 356
    .restart local v8    # "maxCornertabBottomMargin":I
    :cond_8
    iget v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_5
.end method

.method private SetCurrentVolumeStreamTypeIcon(I)V
    .locals 3
    .param p1, "streamType"    # I

    .prologue
    .line 541
    packed-switch p1, :pswitch_data_0

    .line 558
    :pswitch_0
    const-string v0, "VolumeControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current stream type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    return-void

    .line 541
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private SetVolumeControlGridUIParams()V
    .locals 7

    .prologue
    const/4 v1, -0x1

    .line 510
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_0

    .line 511
    const/4 v6, 0x0

    .line 512
    .local v6, "height":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const v4, 0x1000028

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 519
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "volumeControlGridUI"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 520
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x53

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 522
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 523
    const v6, 0x7f09005e

    .line 527
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayoutParam:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-virtual {v1}, Landroid/app/Service;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 529
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayoutParam:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 531
    .end local v6    # "height":I
    :cond_0
    return-void

    .line 525
    .restart local v6    # "height":I
    :cond_1
    const v6, 0x7f09005d

    goto :goto_0
.end method

.method private UpdateUI()V
    .locals 3

    .prologue
    .line 566
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getVolume(Landroid/media/AudioManager;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 567
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getRingerMode(Landroid/media/AudioManager;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    .line 569
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    if-nez v0, :cond_2

    .line 571
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 572
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 578
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 579
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    if-nez v0, :cond_3

    .line 580
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->setDimUI(Z)V

    .line 586
    :cond_0
    :goto_1
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 593
    :cond_1
    return-void

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 575
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 582
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->setDimUI(Z)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    return p1
.end method

.method static synthetic access$008(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->UpdateUI()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I

    return p1
.end method


# virtual methods
.method public RemoveView()V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 486
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 492
    return-void
.end method

.method public RemoveViewWithoutPokeWakelock()V
    .locals 2

    .prologue
    .line 497
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Service;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StopAutoExit(Landroid/os/Handler;)V

    .line 501
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 504
    return-void
.end method

.method public ScreenRotateStatusChanged()V
    .locals 0

    .prologue
    .line 829
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->RemoveView()V

    .line 830
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ShowView()V

    .line 831
    return-void
.end method

.method public ShowView()V
    .locals 5

    .prologue
    .line 463
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 464
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v3, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 465
    const-string v3, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 466
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/app/Service;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 468
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->Init()V

    .line 470
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 471
    .local v2, "wm":Landroid/view/WindowManager;
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->SetVolumeControlGridUIParams()V

    .line 472
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUIView:Landroid/widget/RelativeLayout;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 474
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const v4, 0x7f040003

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 475
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVolumeControlMainLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 477
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 478
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 479
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0a0068

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 189
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 190
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 192
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 272
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->setRingerMode(Landroid/media/AudioManager;I)V

    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0d0060

    if-ne v1, v2, :cond_1

    .line 276
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getVolume(Landroid/media/AudioManager;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 279
    :cond_1
    return-void

    .line 194
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v6

    if-nez v6, :cond_2

    :goto_1
    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->moveVolume(Landroid/media/AudioManager;ILcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;Z)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 196
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getRingerMode(Landroid/media/AudioManager;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    goto :goto_0

    :cond_2
    move v1, v2

    .line 194
    goto :goto_1

    .line 200
    :sswitch_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v4, "SEC_FLOATING_FEATURE_AUDIO_USE_SILENTMODE_BY_VOL_KEY"

    invoke-virtual {v3, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 203
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    if-ne v3, v1, :cond_3

    .line 205
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getRingerMode(Landroid/media/AudioManager;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    .line 206
    const-string v3, "VolumeControlGridUI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] VolumeDown Button #2 - mCurStreamType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    if-ne v3, v6, :cond_3

    .line 209
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 210
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    .line 212
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    const-string v4, "vibrator"

    invoke-virtual {v3, v4}, Landroid/app/Service;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 214
    .local v0, "vibrator":Landroid/os/Vibrator;
    const-wide/16 v4, 0x32

    invoke-virtual {v0, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    .line 219
    .end local v0    # "vibrator":Landroid/os/Vibrator;
    :cond_3
    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    if-eq v3, v6, :cond_4

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 237
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I

    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->DOWN:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v6}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v6

    if-nez v6, :cond_5

    :goto_2
    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->moveVolume(Landroid/media/AudioManager;ILcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;Z)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 239
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getRingerMode(Landroid/media/AudioManager;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 237
    goto :goto_2

    .line 244
    :sswitch_2
    iget-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->isDimUI:Z

    if-eqz v3, :cond_6

    .line 245
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v2, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 247
    :cond_6
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I

    .line 248
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    goto/16 :goto_0

    .line 253
    :sswitch_3
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->isDimUI:Z

    if-eqz v2, :cond_7

    .line 254
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mService:Landroid/app/Service;

    invoke-static {v2, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 256
    :cond_7
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    goto/16 :goto_0

    .line 260
    :sswitch_4
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ClickOutSideLayout()V

    goto/16 :goto_0

    .line 264
    :sswitch_5
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z

    .line 265
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 192
    :sswitch_data_0
    .sparse-switch
        0x7f0d0031 -> :sswitch_4
        0x7f0d0035 -> :sswitch_5
        0x7f0d0037 -> :sswitch_1
        0x7f0d0039 -> :sswitch_0
        0x7f0d005d -> :sswitch_2
        0x7f0d0060 -> :sswitch_3
    .end sparse-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 793
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 823
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 824
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 825
    return v4

    .line 795
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 799
    :pswitch_2
    const/4 v0, 0x4

    .line 802
    .local v0, "mSound":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 803
    :cond_1
    const/4 v0, 0x0

    .line 806
    :cond_2
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAccelRateVal:F

    .line 807
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z

    .line 808
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v1, v1, v3

    if-gez v1, :cond_0

    .line 810
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v4, v0}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_0

    .line 793
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    .line 795
    :pswitch_data_1
    .packed-switch 0x7f0d0037
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setDimUI(Z)V
    .locals 7
    .param p1, "isDim"    # Z

    .prologue
    const/high16 v2, -0x78000000

    const/16 v6, 0xff

    const/4 v5, 0x0

    const/high16 v4, -0x1000000

    const/4 v3, 0x1

    .line 834
    if-eqz p1, :cond_0

    .line 835
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 836
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 837
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v4, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 838
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 839
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonPlus:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 840
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonMinus:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 841
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVibrate:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 842
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSound:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 843
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v5}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 844
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 845
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 846
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->isDimUI:Z

    .line 870
    :goto_0
    return-void

    .line 849
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    if-ne v0, v3, :cond_2

    .line 850
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, -0xb451b0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 851
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 852
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, -0xafafb0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 853
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 861
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonPlus:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 862
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mButtonMinus:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 863
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mVibrate:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 864
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSound:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 865
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 866
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 867
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mBtnVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 868
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->isDimUI:Z

    goto :goto_0

    .line 855
    :cond_2
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 856
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, -0xafafb0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 857
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVVibrateIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 858
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const v1, -0xb451b0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 859
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIVSoundIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1
.end method
