.class Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;
.super Ljava/lang/Object;
.source "SettingsMenuEditorActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->InitUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0

    .prologue
    .line 279
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 281
    if-eqz p2, :cond_0

    .line 282
    const-string v0, "SettingsMenuEditorActivity"

    const-string v1, "Lower got the focus!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->changeLayoutByLowerLine()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    .line 288
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$4;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->ChangelayoutByLine(ZI)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$300(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;ZI)V

    goto :goto_0
.end method
