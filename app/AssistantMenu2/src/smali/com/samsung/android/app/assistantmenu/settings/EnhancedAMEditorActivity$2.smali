.class Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;
.super Ljava/lang/Thread;
.source "EnhancedAMEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->fetchData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final CHROME_SUPPORT_PACKAGE:Ljava/lang/String;

.field private final CONTACT_PACKAGE:Ljava/lang/String;

.field private final PHONE_PACKAGE:Ljava/lang/String;

.field private final SETTINGS_PACKAGE:Ljava/lang/String;

.field permEAM:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V
    .locals 1

    .prologue
    .line 284
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 285
    const-string v0, "com.samsung.android.app.assistantmenu.permission.ADVERTISE_ASSISTANTMENU"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->permEAM:Ljava/lang/String;

    .line 286
    const-string v0, "com.android.contacts"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->CONTACT_PACKAGE:Ljava/lang/String;

    .line 287
    const-string v0, "com.chrome.deviceextras.samsung"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->CHROME_SUPPORT_PACKAGE:Ljava/lang/String;

    .line 288
    const-string v0, "com.android.phone"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->PHONE_PACKAGE:Ljava/lang/String;

    .line 289
    const-string v0, "com.android.settings"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->SETTINGS_PACKAGE:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 293
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    const/16 v19, 0x1000

    invoke-virtual/range {v18 .. v19}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v12

    .line 296
    .local v12, "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/PackageInfo;

    .line 297
    .local v16, "resInfo":Landroid/content/pm/PackageInfo;
    if-eqz v16, :cond_0

    .line 299
    const/16 v17, 0x0

    .line 300
    .local v17, "state":Z
    move-object/from16 v0, v16

    iget-object v14, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 302
    .local v14, "permInfo":[Ljava/lang/String;
    if-eqz v14, :cond_1

    .line 303
    move-object v5, v14

    .local v5, "arr$":[Ljava/lang/String;
    array-length v10, v5

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v10, :cond_1

    aget-object v13, v5, v9

    .line 304
    .local v13, "perm":Ljava/lang/String;
    const-string v18, "com.samsung.android.app.assistantmenu.permission.ADVERTISE_ASSISTANTMENU"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 305
    const/16 v17, 0x1

    .line 310
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v13    # "perm":Ljava/lang/String;
    :cond_1
    if-eqz v17, :cond_0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "com.chrome.deviceextras.samsung"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "com.android.settings"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 314
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 315
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 316
    .local v4, "applicationName":Ljava/lang/String;
    const-string v18, "EAMEditor"

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    const-string v19, "com.android.contacts"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->isPhone(Landroid/content/Context;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 320
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    const-string v19, "com.android.phone"

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v15

    .line 321
    .local v15, "phoneApp":Landroid/content/pm/ApplicationInfo;
    const/4 v3, 0x0

    .line 322
    .local v3, "appName":Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 325
    :cond_3
    if-eqz v3, :cond_4

    .line 326
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    .line 333
    .end local v3    # "appName":Ljava/lang/String;
    .end local v15    # "phoneApp":Landroid/content/pm/ApplicationInfo;
    :cond_4
    :goto_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # operator++ for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$308(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    .line 334
    new-instance v6, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 335
    .local v6, "checkboxPref":Landroid/preference/CheckBoxPreference;
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setKey(Ljava/lang/String;)V

    .line 336
    invoke-virtual {v6, v4}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appList:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$500(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v19

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->resizeIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$600(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mAssistantMenuSettingsPrefs:Landroid/content/SharedPreferences;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$700(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/content/SharedPreferences;

    move-result-object v18

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 341
    .local v11, "mAppEnable":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_7

    .line 342
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # operator++ for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$208(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    .line 344
    sget-object v18, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[c] increment count:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->context:Landroid/app/Activity;

    move-object/from16 v18, v0

    check-cast v18, Landroid/preference/Preference$OnPreferenceChangeListener;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->eamPrefScreen:Landroid/preference/PreferenceScreen;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$800(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/preference/PreferenceScreen;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mEAMprov:I
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$900(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v18

    if-nez v18, :cond_0

    .line 355
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 359
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "applicationName":Ljava/lang/String;
    .end local v6    # "checkboxPref":Landroid/preference/CheckBoxPreference;
    .end local v11    # "mAppEnable":I
    .end local v12    # "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v14    # "permInfo":[Ljava/lang/String;
    .end local v16    # "resInfo":Landroid/content/pm/PackageInfo;
    .end local v17    # "state":Z
    :catch_0
    move-exception v7

    .line 360
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 362
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appList:Ljava/util/ArrayList;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$500(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Ljava/util/ArrayList;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->OrderAppData()V
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$1000(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->messageHandler:Landroid/os/Handler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$1100(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/os/Handler;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 365
    return-void

    .line 303
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v9    # "i$":I
    .restart local v10    # "len$":I
    .restart local v12    # "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v13    # "perm":Ljava/lang/String;
    .restart local v14    # "permInfo":[Ljava/lang/String;
    .restart local v16    # "resInfo":Landroid/content/pm/PackageInfo;
    .restart local v17    # "state":Z
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 346
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v13    # "perm":Ljava/lang/String;
    .restart local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v4    # "applicationName":Ljava/lang/String;
    .restart local v6    # "checkboxPref":Landroid/preference/CheckBoxPreference;
    .restart local v11    # "mAppEnable":I
    :cond_7
    const/16 v18, 0x0

    :try_start_3
    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 347
    sget-object v18, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "[c] decrement count:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$2;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_3

    .line 328
    .end local v6    # "checkboxPref":Landroid/preference/CheckBoxPreference;
    .end local v11    # "mAppEnable":I
    :catch_1
    move-exception v18

    goto/16 :goto_2
.end method
