.class public final enum Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;
.super Ljava/lang/Enum;
.source "VolumeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VolumeMove"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

.field public static final enum DOWN:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

.field public static final enum UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    const-string v1, "UP"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->DOWN:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    .line 18
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->DOWN:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    invoke-virtual {v0}, [Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    return-object v0
.end method
