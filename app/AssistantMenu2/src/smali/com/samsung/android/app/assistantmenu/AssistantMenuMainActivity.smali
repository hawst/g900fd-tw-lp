.class public Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;
.super Landroid/app/Activity;
.source "AssistantMenuMainActivity.java"


# instance fields
.field public final DEBUG:Z

.field private final TAG:Ljava/lang/String;

.field private mComponentName:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    const-string v0, "AssistantMenuMainActivity"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->TAG:Ljava/lang/String;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->DEBUG:Z

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->mComponentName:Landroid/content/ComponentName;

    return-void
.end method

.method private BtnUIEnd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "assistant_menu"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->getServiceTaskName()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "Now Service Stoped"

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 92
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->stopService(Landroid/content/Intent;)Z

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->mComponentName:Landroid/content/ComponentName;

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    const-string v0, "Already Service Stoped"

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private BtnUIStart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "assistant_menu"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 66
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->getServiceTaskName()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const-string v0, "Now Service Started"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->mComponentName:Landroid/content/ComponentName;

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_0
    const-string v0, "Already Service Started"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->BtnUIStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->BtnUIEnd()V

    return-void
.end method

.method private getServiceTaskName()Z
    .locals 8

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "checked":Z
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 105
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v5, 0x100

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v2

    .line 106
    .local v2, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v2, :cond_1

    .line 107
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 108
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 110
    .local v4, "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    const-string v5, "AssistantMenuMainActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Service name :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v5, v4, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 113
    const/4 v1, 0x1

    .line 114
    const-string v5, "AssistantMenuMainActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Service is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    .end local v4    # "runningTaskInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_1
    return v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->setContentView(I)V

    .line 37
    const/high16 v2, 0x7f0d0000

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 38
    .local v0, "sfMainStartservice":Landroid/widget/Button;
    new-instance v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity$1;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    const v2, 0x7f0d0001

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 46
    .local v1, "sfMainStopservice":Landroid/widget/Button;
    new-instance v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity$2;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method
