.class public Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;
.super Landroid/preference/PreferenceFragment;
.source "AssistantMenuPreferenceFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;,
        Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;
    }
.end annotation


# static fields
.field public static final ASSISTANT_MENU_PAD_SIZE:Ljava/lang/String; = "assistant_menu_pad_size"

.field public static final ASSISTANT_MENU_POINTER_SIZE:Ljava/lang/String; = "assistant_menu_pointer_size"

.field public static final ASSISTANT_MENU_POINTER_SPEED:Ljava/lang/String; = "assistant_menu_pointer_speed"

.field public static final DOMINANT_HAND_LEFT:I = 0x0

.field public static final DOMINANT_HAND_RIGHT:I = 0x1

.field public static final DOMINANT_HAND_TYPE:Ljava/lang/String; = "dominant_hand_type"

.field public static final HOVER_ZOOM_MAGNIFIER_SIZE:Ljava/lang/String; = "hover_zoom_magnifier_size"

.field public static final KEY_MAGNIFIER_CATEGORY:Ljava/lang/String; = "magnifier_settings"

.field public static final KEY_MAGNIFIER_SIZE:Ljava/lang/String; = "magnifier_size"

.field public static final PAD_SIZE:Ljava/lang/String; = "pad_size"

.field public static final PAD_SIZE_LARGE:I = 0x2

.field public static final PAD_SIZE_MED:I = 0x1

.field public static final PAD_SIZE_SMALL:I = 0x0

.field public static final POINTER_SCALE_SIZE:I = 0x14

.field public static final POINTER_SIZE:Ljava/lang/String; = "pointer_size"

.field public static final POINTER_SIZE_SMALL:I = 0x0

.field public static final POINTER_SPEED:Ljava/lang/String; = "pointer_speed"

.field public static final POINTER_SPEED_NORAML:I = 0x2

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "com.sec.android.assistantmenu_preferences"

.field public static final ZOOM_PAD_SIZE_LARGE:I = 0x2

.field public static final ZOOM_PAD_SIZE_MED:I = 0x1

.field public static final ZOOM_PAD_SIZE_SMALL:I

.field private static mOpenDetailMenu:Z

.field private static mOpenDetailMenuKey:Ljava/lang/String;


# instance fields
.field private final ASSISTANT_MENU_ONOFF:Ljava/lang/String;

.field private final KEY_DOMINANT_HAND:Ljava/lang/String;

.field private final KEY_EAMSETTINGS_MENU:Ljava/lang/String;

.field private final KEY_EDIT_MENU:Ljava/lang/String;

.field private final KEY_PAD_SIZE:Ljava/lang/String;

.field private final KEY_POINTER_SIZE:Ljava/lang/String;

.field private final KEY_POINTER_SPEED:Ljava/lang/String;

.field private final MSG_ZOOM_PERCENTAGE_UPDATE:I

.field private final TAG:Ljava/lang/String;

.field private bSupportHoverZoom:Z

.field private mActionBarLayout:Landroid/view/View;

.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

.field private mAssistantMenuObserver:Landroid/database/ContentObserver;

.field private mDominantHand:Landroid/preference/ListPreference;

.field private mEAMSettingsMenu:Landroid/preference/Preference;

.field private mEditMenu:Landroid/preference/Preference;

.field private mFmMagnifierObserver:Landroid/database/ContentObserver;

.field mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

.field private mMagnifierSize:Landroid/preference/ListPreference;

.field mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

.field private mMagnifier_settings:Landroid/preference/PreferenceCategory;

.field private mPadSize:Landroid/preference/ListPreference;

.field private mPointerSize:Landroid/preference/ListPreference;

.field private mPointerSpeed:Landroid/preference/ListPreference;

.field private mPreferenceFragmentActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenu:Z

    .line 145
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenuKey:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 56
    const-string v0, "AssistantMenuPreferenceFragment"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->TAG:Ljava/lang/String;

    .line 60
    const-string v0, "dominant_hand_side"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_DOMINANT_HAND:Ljava/lang/String;

    .line 62
    const-string v0, "menu_edit"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_EDIT_MENU:Ljava/lang/String;

    .line 64
    const-string v0, "eam_active"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_EAMSETTINGS_MENU:Ljava/lang/String;

    .line 66
    const-string v0, "fmpointer_speed"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_POINTER_SPEED:Ljava/lang/String;

    .line 68
    const-string v0, "fmpointer_size"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_POINTER_SIZE:Ljava/lang/String;

    .line 70
    const-string v0, "fmpad_size"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->KEY_PAD_SIZE:Ljava/lang/String;

    .line 72
    const-string v0, "assistant_menu"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->ASSISTANT_MENU_ONOFF:Ljava/lang/String;

    .line 82
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->MSG_ZOOM_PERCENTAGE_UPDATE:I

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->bSupportHoverZoom:Z

    .line 149
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$1;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuObserver:Landroid/database/ContentObserver;

    .line 156
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$2;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mFmMagnifierObserver:Landroid/database/ContentObserver;

    .line 592
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->updateAssistantMenuSaving()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->updatePointerSizeMenuStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->refreshImageView(I)V

    return-void
.end method

.method public static getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 175
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 176
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 170
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    .local v0, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 166
    const-string v0, "com.sec.android.assistantmenu_preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "pkg"    # Ljava/lang/String;

    .prologue
    .line 522
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 523
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 525
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    :goto_0
    return v1

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    .line 528
    const-string v3, "AssistantMenuPreferenceFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package not found : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private refreshImageView(I)V
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 535
    const-string v0, "AssistantMenuPreferenceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "refreshImageView : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    if-nez v0, :cond_0

    .line 539
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->setHoverZoomImageLevel(I)V

    .line 538
    const-string v0, "AssistantMenuPreferenceFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mMagnifierImage.setMagnifierLevel(level) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setDefaultValue(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 194
    const-string v0, "com.sec.android.assistantmenu_preferences"

    const v1, 0x7f050003

    invoke-static {p0, v0, v2, v1, v2}, Landroid/preference/PreferenceManager;->setDefaultValues(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 196
    return-void
.end method

.method public static setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 187
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 188
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 189
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 190
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 191
    return-void
.end method

.method public static setSharedPreference(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 180
    invoke-static {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 181
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 182
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 183
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    return-void
.end method

.method private updateAssistantMenuSaving()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 479
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 481
    .local v0, "assistantMenu_state":I
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "FmMagnifier"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 483
    .local v1, "fmMAgnifier_state":I
    if-nez v0, :cond_1

    .line 484
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->bSupportHoverZoom:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    invoke-virtual {v2, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;->setEnabledZoomButton(Z)V

    .line 485
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    invoke-virtual {v2, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->setSwitchChecked(Z)V

    .line 486
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 487
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEditMenu:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 488
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEAMSettingsMenu:Landroid/preference/Preference;

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 489
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 490
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 491
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 492
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    .line 514
    :goto_0
    return-void

    .line 494
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->bSupportHoverZoom:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    invoke-virtual {v2, v6}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;->setEnabledZoomButton(Z)V

    .line 495
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    invoke-virtual {v2, v6}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->setSwitchChecked(Z)V

    .line 496
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 497
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEditMenu:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 498
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEAMSettingsMenu:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 502
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v4, "dominant_hand_type"

    invoke-static {v3, v4, v6}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 503
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 504
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 505
    if-ne v1, v6, :cond_3

    .line 506
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 511
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 512
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v6}, Landroid/preference/PreferenceCategory;->setEnabled(Z)V

    goto :goto_0

    .line 508
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updatePointerSizeMenuStatus()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 465
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 467
    .local v0, "assistantMenu_state":I
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "FmMagnifier"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 469
    .local v1, "fmMAgnifier_state":I
    if-ne v0, v5, :cond_0

    .line 470
    if-ne v1, v5, :cond_1

    .line 471
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v4}, Landroid/preference/ListPreference;->setEnabled(Z)V

    .line 476
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v2, v5}, Landroid/preference/ListPreference;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 517
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x10

    const/4 v10, -0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 200
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 201
    const-string v4, "AssistantMenuPreferenceFragment"

    const-string v7, "onCreate()"

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v7, "com.sec.feature.overlaymagnifier"

    invoke-static {v4, v7}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->bSupportHoverZoom:Z

    .line 205
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v3

    .line 213
    .local v3, "preferenceManager":Landroid/preference/PreferenceManager;
    const-string v4, "com.sec.android.assistantmenu_preferences"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    .line 215
    const v4, 0x7f050002

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->addPreferencesFromResource(I)V

    .line 218
    new-instance v4, Landroid/widget/Switch;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-direct {v4, v7}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 220
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    instance-of v4, v4, Landroid/preference/PreferenceActivity;

    if-eqz v4, :cond_1

    .line 221
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f090001

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 223
    .local v2, "padding":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v4, v6, v6, v2, v6}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 224
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v4

    if-nez v4, :cond_0

    .line 225
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020159

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/Switch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 226
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02015a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/Switch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 228
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9, v9}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 230
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v8, Landroid/app/ActionBar$LayoutParams;

    const v9, 0x800015

    invoke-direct {v8, v10, v10, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v4, v7, v8}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 235
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 236
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    .line 237
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v4}, Landroid/widget/Switch;->requestFocus()Z

    .line 240
    .end local v2    # "padding":I
    :cond_1
    new-instance v4, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-direct {v4, v7, v8}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;-><init>(Landroid/content/Context;Landroid/widget/Switch;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    .line 243
    const-string v4, "dominant_hand_side"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    .line 244
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    if-eqz v4, :cond_2

    .line 245
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v8, "dominant_hand_type"

    invoke-static {v7, v8, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 249
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v4, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 252
    :cond_2
    const-string v4, "menu_edit"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEditMenu:Landroid/preference/Preference;

    .line 253
    const-string v4, "eam_active"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mEAMSettingsMenu:Landroid/preference/Preference;

    .line 255
    const-string v4, "fmpointer_speed"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    .line 256
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    if-eqz v4, :cond_3

    .line 257
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v8, "pointer_speed"

    const/4 v9, 0x2

    invoke-static {v7, v8, v9}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 259
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    invoke-virtual {v4, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 264
    :cond_3
    const-string v4, "fmpointer_size"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    .line 266
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    if-eqz v4, :cond_4

    .line 267
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v8, "pointer_size"

    invoke-static {v7, v8, v6}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 270
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v4, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 275
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v7, "com.samsung.android.app.galaxyfinder"

    invoke-direct {p0, v4, v7}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v4

    const-string v7, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_SEARCH_VER_TWO"

    invoke-virtual {v4, v7}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 277
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v7, "extra_fragment_bundle_key"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 279
    .local v1, "extra_bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_6

    .line 280
    const-string v4, "extra_from_search"

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 281
    .local v0, "extraFromSearch":Z
    if-eqz v0, :cond_6

    .line 282
    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenu:Z

    .line 283
    const-string v4, "extra_parent_preference_key"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenuKey:Ljava/lang/String;

    .line 287
    .end local v0    # "extraFromSearch":Z
    .end local v1    # "extra_bundle":Landroid/os/Bundle;
    :cond_6
    const-string v4, "fmpad_size"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    .line 288
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    if-eqz v4, :cond_7

    .line 289
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v8, "pad_size"

    invoke-static {v7, v8, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 290
    const-string v4, "AssistantMenuPreferenceFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "PAD_SIZE - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v8}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v7}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v4, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 295
    :cond_7
    const-string v4, "magnifier_settings"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceCategory;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    .line 296
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->bSupportHoverZoom:Z

    if-eqz v4, :cond_a

    .line 297
    new-instance v7, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v9, "assistant_menu"

    invoke-static {v4, v9, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v5, :cond_9

    move v4, v5

    :goto_0
    invoke-direct {v7, p0, v8, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;Landroid/content/Context;Z)V

    iput-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    .line 298
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;->setOrder(I)V

    .line 299
    new-instance v4, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "hover_zoom_value"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    invoke-direct {v4, p0, v5, v7}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;-><init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    .line 300
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    invoke-virtual {v4, v10}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->setOrder(I)V

    .line 302
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierImage:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 303
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierZoom:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageZoom;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 305
    const-string v4, "magnifier_size"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/ListPreference;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    .line 306
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    if-eqz v4, :cond_8

    .line 307
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "hover_zoom_magnifier_size"

    invoke-static {v5, v7, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 308
    const-string v4, "AssistantMenuPreferenceFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MagnifierSize - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    invoke-virtual {v6}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    invoke-virtual {v5}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    invoke-virtual {v4, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 315
    :cond_8
    :goto_1
    return-void

    :cond_9
    move v4, v6

    .line 297
    goto/16 :goto_0

    .line 313
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifier_settings:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    .line 461
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 462
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->pause()V

    .line 373
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mFmMagnifierObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 375
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 376
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 396
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 399
    .local v0, "key":Ljava/lang/String;
    const-string v2, "dominant_hand_side"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, p2

    .line 400
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 403
    .local v1, "value":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v3, "dominant_hand_type"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 404
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mDominantHand:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 407
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu_dominant_hand_type"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 447
    .end local v1    # "value":I
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 410
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_0
    const-string v2, "fmpointer_speed"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, p2

    .line 411
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 412
    .restart local v1    # "value":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v3, "pointer_speed"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 413
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 414
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSpeed:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 416
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu_pointer_speed"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 418
    .end local v1    # "value":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    const-string v2, "fmpointer_size"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v2, p2

    .line 419
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 420
    .restart local v1    # "value":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v3, "pointer_size"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 421
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 422
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPointerSize:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 424
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu_pointer_size"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 426
    .end local v1    # "value":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_2
    const-string v2, "fmpad_size"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, p2

    .line 427
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 428
    .restart local v1    # "value":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v3, "pad_size"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->setSharedPreference(Landroid/content/Context;Ljava/lang/String;I)V

    .line 429
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 430
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPadSize:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 431
    const-string v2, "AssistantMenuPreferenceFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPreferenceChange  padsize -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu_pad_size"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 434
    .end local v1    # "value":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_3
    const-string v2, "fmpointer_speed1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 435
    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 436
    .restart local v1    # "value":I
    const-string v2, "AssistantMenuPreferenceFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changed speed value - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 437
    .end local v1    # "value":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_4
    const-string v2, "magnifier_size"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object v2, p2

    .line 438
    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 439
    .restart local v1    # "value":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {v2, p2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 440
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mMagnifierSize:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 441
    const-string v2, "AssistantMenuPreferenceFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPreferenceChange  MagnifierSize -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hover_zoom_magnifier_size"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto/16 :goto_0

    .line 444
    .end local v1    # "value":I
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_5
    const-string v2, "AssistantMenuPreferenceFragment"

    const-string v3, "onPreferenceChange"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 13

    .prologue
    const/16 v5, 0x10

    const/4 v12, -0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 319
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 321
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    instance-of v2, v2, Landroid/preference/PreferenceActivity;

    if-eqz v2, :cond_0

    .line 322
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f090001

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 324
    .local v7, "padding":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v10, v10, v7, v10}, Landroid/widget/Switch;->setPaddingRelative(IIII)V

    .line 325
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 327
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Landroid/app/ActionBar$LayoutParams;

    const v9, 0x800015

    invoke-direct {v5, v12, v12, v9}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v4, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 332
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    .line 335
    .end local v7    # "padding":I
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->updateAssistantMenuSaving()V

    .line 336
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuEnabler:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuEnabler;->resume()V

    .line 337
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v11}, Landroid/widget/Switch;->setEnabled(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "assistant_menu"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mAssistantMenuObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v4, v11, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 343
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "FmMagnifier"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mFmMagnifierObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v4, v11, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 348
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mPreferenceFragmentActivity:Landroid/app/Activity;

    const-string v4, "com.samsung.android.app.galaxyfinder"

    invoke-direct {p0, v2, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v4, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_SEARCH_VER_TWO"

    invoke-virtual {v2, v4}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 350
    :cond_1
    sget-boolean v2, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenu:Z

    if-eqz v2, :cond_4

    .line 351
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 352
    .local v0, "ps":Landroid/preference/PreferenceScreen;
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 353
    .local v1, "list":Landroid/widget/ListView;
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getRootAdapter()Landroid/widget/ListAdapter;

    move-result-object v8

    .line 354
    .local v8, "preferenceAdapter":Landroid/widget/ListAdapter;
    const/4 v3, 0x0

    .line 355
    .local v3, "position":I
    const/4 v3, 0x0

    :goto_0
    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v3, v2, :cond_3

    .line 356
    invoke-interface {v8, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/preference/Preference;

    .line 357
    .local v6, "check_item":Landroid/preference/Preference;
    sget-object v2, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenuKey:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 358
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setSelection(I)V

    .line 359
    invoke-virtual {v6}, Landroid/preference/Preference;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/preference/PreferenceScreen;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 355
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 364
    .end local v6    # "check_item":Landroid/preference/Preference;
    :cond_3
    sput-boolean v10, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mOpenDetailMenu:Z

    .line 368
    .end local v0    # "ps":Landroid/preference/PreferenceScreen;
    .end local v1    # "list":Landroid/widget/ListView;
    .end local v3    # "position":I
    .end local v8    # "preferenceAdapter":Landroid/widget/ListAdapter;
    :cond_4
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 453
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 454
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V

    .line 456
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 383
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStart()V

    .line 384
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 389
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->mActionBarLayout:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 391
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStop()V

    .line 392
    return-void
.end method
