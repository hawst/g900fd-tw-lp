.class public Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;
.super Ljava/lang/Object;
.source "VolumeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static getMaxVolume(Landroid/media/AudioManager;I)I
    .locals 1
    .param p0, "manager"    # Landroid/media/AudioManager;
    .param p1, "type"    # I

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public static getRingerMode(Landroid/media/AudioManager;)I
    .locals 1
    .param p0, "manager"    # Landroid/media/AudioManager;

    .prologue
    .line 108
    invoke-virtual {p0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    return v0
.end method

.method public static getVolume(Landroid/media/AudioManager;I)I
    .locals 1
    .param p0, "manager"    # Landroid/media/AudioManager;
    .param p1, "type"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public static moveVolume(Landroid/media/AudioManager;ILcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;Z)I
    .locals 3
    .param p0, "manager"    # Landroid/media/AudioManager;
    .param p1, "type"    # I
    .param p2, "move"    # Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;
    .param p3, "isSound"    # Z

    .prologue
    const/4 v2, 0x1

    .line 31
    const/4 v0, 0x0

    .line 33
    .local v0, "mSound":I
    if-eqz p3, :cond_1

    if-eqz p1, :cond_1

    .line 34
    invoke-virtual {p0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 43
    :goto_0
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;->UP:Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager$VolumeMove;

    if-ne p2, v1, :cond_2

    .line 45
    invoke-virtual {p0, p1, v2, v0}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 51
    :goto_1
    invoke-static {p0, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getVolume(Landroid/media/AudioManager;I)I

    move-result v1

    return v1

    .line 37
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 40
    :cond_1
    const/16 v0, 0x800

    goto :goto_0

    .line 49
    :cond_2
    const/4 v1, -0x1

    invoke-virtual {p0, p1, v1, v0}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    goto :goto_1
.end method

.method public static setRingerMode(Landroid/media/AudioManager;I)V
    .locals 0
    .param p0, "manager"    # Landroid/media/AudioManager;
    .param p1, "ringtoneMode"    # I

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 99
    return-void
.end method

.method public static setVolume(Landroid/media/AudioManager;II)I
    .locals 1
    .param p0, "manager"    # Landroid/media/AudioManager;
    .param p1, "type"    # I
    .param p2, "value"    # I

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 65
    invoke-static {p0, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getVolume(Landroid/media/AudioManager;I)I

    move-result v0

    return v0
.end method
