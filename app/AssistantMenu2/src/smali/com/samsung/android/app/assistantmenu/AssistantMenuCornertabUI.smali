.class public Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
.super Ljava/lang/Object;
.source "AssistantMenuCornertabUI.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;,
        Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;
    }
.end annotation


# static fields
.field private static CORNERTAB_GUIDELINE_FULL_X:[I

.field private static CORNERTAB_GUIDELINE_FULL_Y:[I

.field public static mOrientGuideLineX:[I

.field public static mOrientGuideLineY:[I


# instance fields
.field private final CORNERTAB_LONGPRESS_TIME:I

.field private final EVENT_CORNERTABWIDGET_GONE:I

.field private final EVENT_CORNERTAB_ANIPROCESS:I

.field private final EVENT_CORNERTAB_GONE:I

.field private final EVENT_CORNERTAB_LONGPRESS:I

.field private final EVENT_KEYBOARD_ANIPROCESS:I

.field private final TAG:Ljava/lang/String;

.field isKeyboardDropping:Z

.field private isNormal:Z

.field private isUpdatedByKeyboard:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBtnCornertab:Landroid/widget/Button;

.field private mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mCornertabView:Landroid/widget/RelativeLayout;

.field private mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

.field private mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

.field private mCornertabWidgetX:I

.field private mCornertabWidgetY:I

.field private mDisplaySize:Landroid/graphics/Point;

.field private mGuideAdjustPosXidx:I

.field private mGuideAdjustPosYidx:I

.field mGuideAnimation:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;

.field private final mHandler:Landroid/os/Handler;

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mPm:Landroid/os/PersonaManager;

.field private mPreviousOpenIME:I

.field private mPreviousWidgetX:I

.field private mPreviousWidgetY:I

.field private mResources:Landroid/content/res/Resources;

.field private mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

.field private mServiceHandler:Landroid/os/Handler;

.field private mServicePreference:Landroid/content/SharedPreferences;

.field public touchCnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 110
    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_X:[I

    .line 123
    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_Y:[I

    .line 133
    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    .line 135
    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    return-void
.end method

.method public constructor <init>(Landroid/app/Service;)V
    .locals 4
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v0, "AssistantMenuCornertabUI"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->TAG:Ljava/lang/String;

    .line 54
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServiceHandler:Landroid/os/Handler;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mAudioManager:Landroid/media/AudioManager;

    .line 66
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    .line 68
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetX:I

    .line 70
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetY:I

    .line 72
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    .line 74
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    .line 76
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    .line 80
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 82
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

    .line 86
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    .line 90
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 92
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    .line 94
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 98
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosXidx:I

    .line 100
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosYidx:I

    .line 102
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->touchCnt:I

    .line 104
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAnimation:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;

    .line 137
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_LONGPRESS_TIME:I

    .line 139
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->EVENT_CORNERTAB_LONGPRESS:I

    .line 141
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->EVENT_CORNERTAB_ANIPROCESS:I

    .line 143
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->EVENT_CORNERTAB_GONE:I

    .line 145
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->EVENT_CORNERTABWIDGET_GONE:I

    .line 147
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->EVENT_KEYBOARD_ANIPROCESS:I

    .line 150
    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPm:Landroid/os/PersonaManager;

    .line 154
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    .line 158
    iput-boolean v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    .line 163
    iput-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    .line 165
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    .line 198
    check-cast p1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .end local p1    # "service":Landroid/app/Service;
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 199
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServiceHandler:Landroid/os/Handler;

    .line 201
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mAudioManager:Landroid/media/AudioManager;

    .line 206
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPm:Landroid/os/PersonaManager;

    .line 210
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    .line 213
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->Init()V

    .line 214
    return-void
.end method

.method private CalculateGuideAdjustPos()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1035
    const/4 v1, 0x0

    .line 1036
    .local v1, "intMin":I
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v2, v2

    if-lez v2, :cond_0

    .line 1037
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1039
    :cond_0
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosXidx:I

    .line 1041
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1042
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    aget v3, v3, v0

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1044
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    aget v3, v3, v0

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1046
    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosXidx:I

    .line 1041
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1050
    :cond_2
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1051
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1054
    :cond_3
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosYidx:I

    .line 1056
    const/4 v0, 0x1

    :goto_1
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 1057
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    aget v3, v3, v0

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 1059
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    aget v3, v3, v0

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1061
    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosYidx:I

    .line 1056
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1064
    :cond_5
    return-void
.end method

.method private ChangeOrientGuideLine()V
    .locals 3

    .prologue
    .line 235
    const-string v0, "AssistantMenuCornertabUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] ChangeOrientGuideLine()+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_X:[I

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    .line 247
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_Y:[I

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    .line 260
    :goto_0
    return-void

    .line 257
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_Y:[I

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    .line 258
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_X:[I

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    goto :goto_0
.end method

.method private CornertabGuideAniPostExecute()V
    .locals 10

    .prologue
    .line 1296
    const-string v6, "AssistantMenuCornertabUI"

    const-string v7, "[c] CornertabGuideAniPostExecute()+"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int v1, v6, v7

    .line 1299
    .local v1, "cornertabViewMaxX":I
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int v2, v6, v7

    .line 1301
    .local v2, "cornertabViewMaxY":I
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 1302
    .local v3, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    if-le v7, v1, :cond_2

    .end local v1    # "cornertabViewMaxX":I
    :goto_0
    invoke-interface {v3, v6, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1305
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    if-le v6, v2, :cond_3

    .end local v2    # "cornertabViewMaxY":I
    :goto_1
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 1308
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 1310
    .local v0, "adjustCornerTabPosY":I
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetIsCornertabMove()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1311
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f09004f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int v4, v6, v7

    .line 1318
    .local v4, "keyboardHeight":I
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    sub-int v5, v4, v6

    .line 1319
    .local v5, "maxCornertabViewY":I
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    if-ge v6, v5, :cond_5

    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 1322
    .end local v4    # "keyboardHeight":I
    .end local v5    # "maxCornertabViewY":I
    :cond_1
    :goto_3
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    invoke-interface {v3, v6, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1323
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f090012

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    sub-int/2addr v7, v0

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1326
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-interface {v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1327
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1331
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 1333
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1334
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x4

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1335
    return-void

    .line 1302
    .end local v0    # "adjustCornerTabPosY":I
    .restart local v1    # "cornertabViewMaxX":I
    .restart local v2    # "cornertabViewMaxY":I
    :cond_2
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    goto/16 :goto_0

    .line 1305
    .end local v1    # "cornertabViewMaxX":I
    :cond_3
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    goto :goto_1

    .line 1311
    .end local v2    # "cornertabViewMaxY":I
    .restart local v0    # "adjustCornerTabPosY":I
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f09004e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int v4, v6, v7

    goto :goto_2

    .restart local v4    # "keyboardHeight":I
    .restart local v5    # "maxCornertabViewY":I
    :cond_5
    move v0, v5

    .line 1319
    goto :goto_3
.end method

.method private CornertabGuideAnimation()V
    .locals 14

    .prologue
    const-wide/16 v12, 0xfa

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 1240
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CalculateGuideAdjustPos()V

    .line 1242
    const-string v2, "AssistantMenuCornertabUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] CornertabGuideAnimation()+ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosXidx:I

    aget v5, v5, v6

    int-to-float v5, v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosYidx:I

    aget v5, v5, v6

    int-to-float v5, v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAnimation:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;

    const-string v3, "x"

    new-array v4, v8, [I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    aput v5, v4, v9

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosXidx:I

    aget v6, v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v5

    aput v5, v4, v10

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1253
    .local v0, "guideAnimX":Landroid/animation/ValueAnimator;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAnimation:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetGuideAnimation;

    const-string v3, "y"

    new-array v4, v8, [I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    aput v5, v4, v9

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    iget v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mGuideAdjustPosYidx:I

    aget v6, v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v5

    aput v5, v4, v10

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1257
    .local v1, "guideAnimY":Landroid/animation/ValueAnimator;
    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1258
    new-instance v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$2;

    invoke-direct {v2, p0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1271
    invoke-virtual {v1, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1272
    new-instance v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$3;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;Landroid/animation/ValueAnimator;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1285
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1286
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1287
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1288
    return-void
.end method

.method private GetWindowSize(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 982
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 990
    return-void
.end method

.method private Init()V
    .locals 2

    .prologue
    .line 221
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] Init()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_X:[I

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CORNERTAB_GUIDELINE_FULL_Y:[I

    .line 226
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->GetWindowSize(Landroid/content/Context;)V

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ChangeOrientGuideLine()V

    .line 228
    return-void
.end method

.method private IsCornertabWidgetViewVisible()Z
    .locals 1

    .prologue
    .line 972
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private KeyboardAnimation(III)V
    .locals 11
    .param p1, "curPos"    # I
    .param p2, "desPos"    # I
    .param p3, "increase"    # I

    .prologue
    const v10, 0x7f090012

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 419
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_3

    .line 420
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 423
    .local v1, "wm":Landroid/view/WindowManager;
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 425
    .local v2, "x":I
    add-int v3, p1, p3

    .line 427
    .local v3, "y":I
    if-lez p3, :cond_4

    .line 428
    if-le v3, p2, :cond_0

    move v3, p2

    .line 435
    :cond_0
    :goto_0
    if-ltz v2, :cond_1

    .line 436
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput v2, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 438
    :cond_1
    if-ltz v3, :cond_2

    .line 439
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v5, v3

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 442
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 443
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 445
    if-eq v3, p2, :cond_6

    .line 446
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v6, 0x5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v3, p2, v7}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 468
    .end local v1    # "wm":Landroid/view/WindowManager;
    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_3
    :goto_1
    return-void

    .line 430
    .restart local v1    # "wm":Landroid/view/WindowManager;
    .restart local v2    # "x":I
    .restart local v3    # "y":I
    :cond_4
    if-ge v3, p2, :cond_5

    move v3, p2

    :cond_5
    goto :goto_0

    .line 449
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 450
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    invoke-interface {v0, v4, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 451
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    sub-int/2addr v5, p2

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 455
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 456
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 457
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isShouldOpenMenu()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 458
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    .line 459
    iput-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    .line 460
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    .line 461
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->showTest()V

    .line 463
    :cond_7
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    .line 464
    iput-boolean v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    .line 465
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_1
.end method

.method private PressCornertabLongpress()V
    .locals 5

    .prologue
    .line 949
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] PressCornertabLongpress()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    .line 955
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x2

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 958
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->setVisibility(I)V

    .line 961
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->invalidate()V

    .line 963
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 964
    return-void
.end method

.method private SetCornertabViewParams()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 608
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 613
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "cornertabUI"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 624
    return-void
.end method

.method private SetCornertabViewParams(II)V
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 633
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetCornertabViewParams()V

    .line 635
    if-ltz p1, :cond_0

    .line 636
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 640
    :cond_0
    if-ltz p2, :cond_1

    .line 641
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f090012

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, p2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 644
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x53

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 645
    return-void
.end method

.method private SetCornertabWidgetViewParams()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 934
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    const v4, 0x40008

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

    .line 939
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x53

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 941
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "cornertabWidgetView"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 942
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->PressCornertabLongpress()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CornertabGuideAniPostExecute()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;III)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->KeyboardAnimation(III)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public ChangeHandMode()V
    .locals 5

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    .local v0, "cornertabPosX":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHandMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 269
    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 270
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v0

    .line 276
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v4, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x2

    aget v3, v3, v4

    int-to-float v3, v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView(II)V

    .line 281
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 282
    .local v1, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v2, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 283
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 284
    return-void

    .line 272
    .end local v1    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f090015

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int v0, v2, v3

    goto :goto_0
.end method

.method public ChangePosition(Z)V
    .locals 9
    .param p1, "isUpdate"    # Z

    .prologue
    const v8, 0x7f090012

    .line 1082
    const/4 v0, 0x0

    .line 1083
    .local v0, "cornertabPosX":I
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v1

    .line 1088
    .local v1, "cornertabPosY":I
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHandMode()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 1089
    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v5, v5

    if-lez v5, :cond_5

    .line 1090
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    aget v6, v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v0

    .line 1096
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1097
    .local v2, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    invoke-interface {v2, v5, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1099
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetIsCornertabMove()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1100
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f09004f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int v3, v5, v6

    .line 1106
    .local v3, "keyboardHeight":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v4, v3, v5

    .line 1108
    .local v4, "maxCornertabViewY":I
    if-ge v1, v4, :cond_7

    .line 1110
    .end local v3    # "keyboardHeight":I
    .end local v4    # "maxCornertabViewY":I
    :cond_2
    :goto_2
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    invoke-interface {v2, v5, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1111
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    sub-int/2addr v6, v1

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1114
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1115
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v6

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1116
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1118
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    if-eqz v5, :cond_3

    .line 1119
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x2

    aget v6, v6, v7

    int-to-float v6, v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    .line 1123
    :cond_3
    if-eqz p1, :cond_4

    .line 1124
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView()V

    .line 1126
    :cond_4
    return-void

    .line 1092
    .end local v2    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f090015

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int v0, v5, v6

    goto/16 :goto_0

    .line 1100
    .restart local v2    # "e":Landroid/content/SharedPreferences$Editor;
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f09004e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int v3, v5, v6

    goto :goto_1

    .restart local v3    # "keyboardHeight":I
    .restart local v4    # "maxCornertabViewY":I
    :cond_7
    move v1, v4

    .line 1108
    goto :goto_2
.end method

.method public IsCornertabVisibility()Z
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public RemoveView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 549
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 552
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 565
    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    .line 567
    :cond_1
    return-void
.end method

.method public ScreenRotateStatusChanged()V
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->GetWindowSize(Landroid/content/Context;)V

    .line 1072
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ChangeOrientGuideLine()V

    .line 1073
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ChangePosition(Z)V

    .line 1074
    return-void
.end method

.method public SetButtonBackground(I)V
    .locals 1
    .param p1, "resourceNum"    # I

    .prologue
    .line 596
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 597
    return-void
.end method

.method public SetVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 575
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 577
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 578
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 579
    return-void
.end method

.method public ShowView()V
    .locals 13

    .prologue
    const v12, 0x7f090015

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 475
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const/high16 v7, 0x7f0b0000

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->setTheme(I)V

    .line 476
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v7, "window"

    invoke-virtual {v5, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 479
    .local v4, "wm":Landroid/view/WindowManager;
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    if-nez v5, :cond_2

    .line 480
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v7, 0x7f030002

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    .line 481
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    const v7, 0x7f0d0006

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    .line 482
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v5, p0}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 486
    const/4 v1, 0x0

    .line 487
    .local v1, "cornertabPosX":I
    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v5, v5

    if-lez v5, :cond_4

    .line 488
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    sget-object v10, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v10, v10

    add-int/lit8 v10, v10, -0x1

    aget v9, v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v8

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .end local v1    # "cornertabPosX":I
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    .line 496
    .restart local v1    # "cornertabPosX":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v10, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v10, v10

    add-int/lit8 v10, v10, -0x2

    aget v9, v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v8

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 502
    .local v2, "cornertabPosY":I
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    invoke-interface {v5, v7, v11}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v7

    if-ne v5, v7, :cond_0

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v5, v7, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 506
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 507
    .local v3, "prefsEdit":Landroid/content/SharedPreferences$Editor;
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v7

    invoke-interface {v3, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 508
    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 509
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 511
    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v5, v5

    if-lez v5, :cond_6

    .line 512
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHandMode()I

    move-result v5

    if-ne v5, v11, :cond_5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    sget-object v8, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    int-to-float v7, v7

    invoke-static {v5, v7}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v1

    .line 518
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v8, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v8, v8

    add-int/lit8 v8, v8, -0x2

    aget v7, v7, v8

    int-to-float v7, v7

    invoke-static {v5, v7}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v2

    .line 523
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ChangePosition(Z)V

    .line 526
    .end local v3    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :cond_1
    if-lez v2, :cond_8

    .end local v2    # "cornertabPosY":I
    :goto_2
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetCornertabViewParams(II)V

    .line 528
    .end local v1    # "cornertabPosX":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 532
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    if-nez v5, :cond_3

    .line 533
    new-instance v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v5, p0, v6}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;-><init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    .line 534
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->setVisibility(I)V

    .line 536
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetCornertabWidgetViewParams()V

    .line 538
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetViewParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 540
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v6, 0x7f040001

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 541
    .local v0, "alphaAni":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v5, v0}, Landroid/widget/Button;->startAnimation(Landroid/view/animation/Animation;)V

    .line 542
    return-void

    .line 492
    .end local v0    # "alphaAni":Landroid/view/animation/Animation;
    .restart local v1    # "cornertabPosX":I
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int/2addr v8, v9

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .end local v1    # "cornertabPosX":I
    iput v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    .restart local v1    # "cornertabPosX":I
    goto/16 :goto_0

    .restart local v2    # "cornertabPosY":I
    .restart local v3    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :cond_5
    move v1, v6

    .line 512
    goto :goto_1

    .line 515
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHandMode()I

    move-result v5

    if-ne v5, v11, :cond_7

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int v1, v5, v7

    :goto_3
    goto :goto_1

    :cond_7
    move v1, v6

    goto :goto_3

    .end local v3    # "prefsEdit":Landroid/content/SharedPreferences$Editor;
    :cond_8
    move v2, v6

    .line 526
    goto :goto_2
.end method

.method public UpdateView()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 291
    invoke-virtual {p0, v0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->UpdateView(II)V

    .line 292
    return-void
.end method

.method public UpdateView(II)V
    .locals 12
    .param p1, "cornertabPosX"    # I
    .param p2, "cornertabPosY"    # I

    .prologue
    .line 302
    const/4 v1, 0x0

    .line 303
    .local v1, "isKeyboardAnimaitonRun":Z
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    .line 304
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenIME()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    iget-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    if-eqz v6, :cond_1

    .line 305
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    .line 308
    :cond_1
    if-ltz p1, :cond_3

    .line 309
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 310
    .local v0, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    invoke-interface {v0, v6, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 311
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 322
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    :goto_0
    if-ltz p2, :cond_5

    .line 323
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    .restart local v0    # "e":Landroid/content/SharedPreferences$Editor;
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    invoke-interface {v0, v6, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 325
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f090012

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    sub-int/2addr v7, p2

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 330
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 331
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 401
    :goto_1
    if-nez v1, :cond_2

    .line 402
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v7, "window"

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 405
    .local v5, "wm":Landroid/view/WindowManager;
    const-string v6, "AssistantMenuCornertabUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[c] UpdateView:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    if-lez p2, :cond_c

    .end local p2    # "cornertabPosY":I
    :goto_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetCornertabViewParams(II)V

    .line 407
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v5, v6, v7}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    .end local v5    # "wm":Landroid/view/WindowManager;
    :cond_2
    return-void

    .line 313
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .restart local p2    # "cornertabPosY":I
    :cond_3
    sget-object v6, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v6, v6

    if-lez v6, :cond_4

    .line 314
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    sget-object v10, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineX:[I

    array-length v10, v10

    add-int/lit8 v10, v10, -0x1

    aget v9, v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    goto/16 :goto_0

    .line 317
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    goto/16 :goto_0

    .line 333
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    sget-object v7, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v9, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    sget-object v10, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mOrientGuideLineY:[I

    array-length v10, v10

    add-int/lit8 v10, v10, -0x2

    aget v9, v9, v10

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v8

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p2

    .line 336
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetIsCornertabMove()Z

    move-result v6

    if-nez v6, :cond_6

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 341
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v7, "keyguard"

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    .line 343
    .local v3, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v3}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 344
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    const v7, 0x7f020003

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 347
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f09004f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int v2, v6, v7

    .line 355
    .local v2, "keyboardHeight":I
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v6

    sub-int v4, v2, v6

    .line 356
    .local v4, "maxCornertabViewY":I
    if-le p2, v4, :cond_7

    .line 357
    const/4 v1, 0x1

    .line 358
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    .line 359
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 360
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x5

    iget-object v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v10, 0x7f090013

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, p2, v4, v9}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 367
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    .line 368
    move p2, v4

    .line 390
    .end local v2    # "keyboardHeight":I
    .end local v3    # "km":Landroid/app/KeyguardManager;
    .end local v4    # "maxCornertabViewY":I
    :cond_7
    :goto_5
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServicePreference:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 391
    .restart local v0    # "e":Landroid/content/SharedPreferences$Editor;
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosY:Ljava/lang/String;

    invoke-interface {v0, v6, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 392
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabBottomPosY:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f090012

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    sub-int/2addr v7, p2

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 396
    sget-object v6, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabDisplayHeight:Ljava/lang/String;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-interface {v0, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 397
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1

    .line 346
    .end local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .restart local v3    # "km":Landroid/app/KeyguardManager;
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    const v7, 0x7f02000b

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 347
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f09004e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    sub-int v2, v6, v7

    goto :goto_4

    .line 370
    .end local v3    # "km":Landroid/app/KeyguardManager;
    :cond_a
    iget v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    if-eqz v6, :cond_b

    .line 371
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    const v7, 0x7f020003

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 372
    const/4 v1, 0x1

    .line 373
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isKeyboardDropping:Z

    .line 374
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 375
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x5

    iget v9, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    iget-object v10, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;

    const v11, 0x7f090014

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v8, p2, v9, v10}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 382
    iget p2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    .line 383
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousOpenIME:I

    goto/16 :goto_5

    .line 385
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    const v7, 0x7f020003

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 386
    const-string v6, "AssistantMenuCornertabUI"

    const-string v7, "[c] UpdateView"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 406
    .restart local v0    # "e":Landroid/content/SharedPreferences$Editor;
    .restart local v5    # "wm":Landroid/view/WindowManager;
    :cond_c
    const/4 p2, 0x0

    goto/16 :goto_2
.end method

.method public getIsUpdatedByKeyboard()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    return v0
.end method

.method public getisNormal()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 649
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCornerTab()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMonkeyRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] onTouch() - Monkey is Running"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    :cond_0
    :goto_0
    return v5

    .line 687
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 756
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] CornertabUI.java, onTouchEvent error"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 689
    :pswitch_0
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] onTouch() - MotionEvent.ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 693
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 694
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 697
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetX:I

    .line 698
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetY:I

    goto :goto_0

    .line 703
    :pswitch_1
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] onTouch() - MotionEvent.ACTION_UP"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 707
    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    .line 708
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenIME()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isUpdatedByKeyboard:Z

    if-eqz v0, :cond_3

    .line 709
    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->isNormal:Z

    .line 712
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabWidgetViewVisible()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 715
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CornertabGuideAnimation()V

    goto :goto_0

    .line 717
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mAudioManager:Landroid/media/AudioManager;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 718
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mServiceHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 724
    :pswitch_2
    const-string v0, "AssistantMenuCornertabUI"

    const-string v1, "[c] onTouch() - MotionEvent.ACTION_CANCEL"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 730
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabWidgetViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->CornertabGuideAnimation()V

    goto/16 :goto_0

    .line 742
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCornerTab()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 743
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->IsCornertabWidgetViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 744
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetX:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I

    .line 745
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetY:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I

    .line 747
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetX:I

    .line 748
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mPreviousWidgetY:I

    .line 750
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->invalidate()V

    goto/16 :goto_0

    .line 687
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public updateDescription()V
    .locals 3

    .prologue
    .line 1342
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0028

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1343
    .local v0, "s":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 1344
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mBtnCornertab:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1345
    :cond_0
    return-void
.end method
