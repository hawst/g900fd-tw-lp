.class Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;
.super Landroid/os/Handler;
.source "AssistantMenuCornertabUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 169
    monitor-enter p0

    .line 170
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 190
    :goto_0
    monitor-exit p0

    .line 191
    return-void

    .line 172
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->PressCornertabLongpress()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$000(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)V

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 175
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$100(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->invalidate()V

    .line 176
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 179
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$200(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 182
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetView:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$100(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->setVisibility(I)V

    goto :goto_0

    .line 185
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    # invokes: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->KeyboardAnimation(III)V
    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$300(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;III)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
