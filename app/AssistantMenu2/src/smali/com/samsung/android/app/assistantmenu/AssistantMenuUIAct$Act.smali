.class public final enum Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
.super Ljava/lang/Enum;
.source "AssistantMenuUIAct.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Act"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLongBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLongHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLongMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLongPowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLongRecentAppsKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressLonglockKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

.field public static final enum ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "None"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 21
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressBackKey"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 23
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressHomeKey"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 25
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressMenuKey"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 27
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "ScreenCapture"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 29
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "ScreenRotate"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 31
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "ZoomControl"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 33
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "VolumeControl"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 35
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "SettingEnter"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 37
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "QuickPanel"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 39
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "QuickSettings"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 41
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "NotificationPanel"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 43
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "RecentappList"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 45
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "DeviceOption"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 47
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "Restart"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 49
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "BrightnessControl"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 51
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "ScreenLock"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 53
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PowerOff"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 55
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLongHomeKey"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 57
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLongBackKey"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 59
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLongRecentAppsKey"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongRecentAppsKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 61
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLonglockKey"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLonglockKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 63
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLongMenuKey"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 65
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "PressLongPowerOff"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongPowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 67
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "ShortCut"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 69
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "FingerMouse"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 71
    new-instance v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const-string v1, "HoverZoom"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 17
    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongRecentAppsKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLonglockKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongPowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->$VALUES:[Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->$VALUES:[Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v0}, [Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    return-object v0
.end method
