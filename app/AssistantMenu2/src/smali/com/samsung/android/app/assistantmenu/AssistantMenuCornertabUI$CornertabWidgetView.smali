.class Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;
.super Landroid/view/View;
.source "AssistantMenuCornertabUI.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DrawAllocation"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CornertabWidgetView"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private cornertabWidgetBmp:Landroid/graphics/Bitmap;

.field private loadPaint:Landroid/graphics/Paint;

.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0xff

    const/4 v2, 0x0

    .line 819
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    .line 820
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 813
    const-string v0, "CornertabWidgetView"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->TAG:Ljava/lang/String;

    .line 821
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->loadPaint:Landroid/graphics/Paint;

    .line 822
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->loadPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 823
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->loadPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 824
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->loadPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 826
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020003

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    .line 833
    return-void
.end method


# virtual methods
.method public getCornertabWidgetBmp()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 847
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 866
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$400(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v2

    .line 867
    .local v2, "x":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v3

    .line 869
    .local v3, "y":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$600(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetIsCornertabMove()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$600(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetOpenClipboard()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 870
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$600(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$200(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$800(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09004f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v0, v4, v5

    .line 878
    .local v0, "keyboardHeight":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$200(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    sub-int v1, v0, v4

    .line 879
    .local v1, "maxCornertabViewY":I
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    if-gez v4, :cond_4

    const/4 v4, 0x0

    :goto_1
    # setter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v5, v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$502(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I

    move-result v3

    .line 880
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    if-le v4, v1, :cond_5

    move v3, v1

    .line 890
    .end local v0    # "keyboardHeight":I
    .end local v1    # "maxCornertabViewY":I
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$400(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_8

    .line 891
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int v2, v5, v6

    # setter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I
    invoke-static {v4, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$402(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I

    .line 896
    :cond_2
    :goto_3
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    int-to-float v5, v2

    int-to-float v6, v3

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->loadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 897
    return-void

    .line 870
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$200(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mResources:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$800(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09004e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v0, v4, v5

    goto :goto_0

    .line 879
    .restart local v0    # "keyboardHeight":I
    .restart local v1    # "maxCornertabViewY":I
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    goto :goto_1

    .line 880
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v3

    goto :goto_2

    .line 883
    .end local v0    # "keyboardHeight":I
    .end local v1    # "maxCornertabViewY":I
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_7

    .line 884
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mDisplaySize:Landroid/graphics/Point;
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$700(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)Landroid/graphics/Point;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->cornertabWidgetBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    sub-int v3, v5, v6

    # setter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$502(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I

    goto/16 :goto_2

    .line 885
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$500(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    if-gez v4, :cond_1

    .line 886
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetY:I
    invoke-static {v4, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$502(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I

    goto/16 :goto_2

    .line 892
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$400(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;)I

    move-result v4

    if-gez v4, :cond_2

    .line 893
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI$CornertabWidgetView;->this$0:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->mCornertabWidgetX:I
    invoke-static {v4, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->access$402(Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;I)I

    goto/16 :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 909
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 913
    const-string v0, "CornertabWidgetView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] CornertabWidgetView onTouchEvent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
