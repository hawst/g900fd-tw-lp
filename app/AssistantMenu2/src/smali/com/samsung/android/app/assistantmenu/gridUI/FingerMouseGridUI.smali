.class public Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
.super Ljava/lang/Object;
.source "FingerMouseGridUI.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseAction;,
        Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;
    }
.end annotation


# static fields
.field public static final HOVER_ZOOM_MAGNIFIER_SIZE:Ljava/lang/String; = "hover_zoom_magnifier_size"

.field public static final HOVER_ZOOM_VALUE:Ljava/lang/String; = "hover_zoom_value"

.field private static final SCROLL_ACTION_MOVE_STEPS:I = 0x4


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final TAG_HOVERZOOM:Ljava/lang/String;

.field private an:Landroid/view/animation/Animation;

.field private dblCursX:F

.field private dblCursY:F

.field private isCueShown:Z

.field private isCursorChange:Z

.field private isPokeLockEnable:Z

.field private mCount_hold:I

.field private mCursorCue:Landroid/widget/ImageView;

.field private mCursorCueLayout:Landroid/widget/RelativeLayout;

.field private mCursorLayoutHeight:I

.field private mCursorLayoutWidth:I

.field private mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

.field private mCursorX:F

.field private mCursorY:F

.field private mDeviceWidth:I

.field private mDeviceheight:I

.field private final mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mDouble:Z

.field private mDoubleTap:Z

.field private mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mFmGridUIMain:Landroid/widget/RelativeLayout;

.field private mFmGridUITab:Landroid/widget/RelativeLayout;

.field private mHoldCursorFlag:Z

.field private mHoverPadSizeObserver:Landroid/database/ContentObserver;

.field private mHoverScaleObserver:Landroid/database/ContentObserver;

.field private mHoverZoomEnabled:I

.field private mHoverZoomPadDisplayed:Z

.field private final mHoverZoomRotateHandler:Landroid/os/Handler;

.field private mIBtnFmMagnifier:Landroid/widget/LinearLayout;

.field private mIBtnFmMoveView:Landroid/widget/ImageView;

.field private mIBtnFmRemoveView:Landroid/widget/LinearLayout;

.field private mIBtnFmTouchView:Landroid/widget/LinearLayout;

.field private mInitX:I

.field private mInitY:I

.field private mInvalidateMargin:I

.field private mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

.field private mLongPress:Z

.field mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

.field private mMagnifierSize:I

.field mManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mOffsetCursorPosition:I

.field private final mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mPadSize:I

.field private final mParentSvcHandler:Landroid/os/Handler;

.field private mPointerSize:I

.field private mPointerSpeed:I

.field private mResource:Landroid/content/res/Resources;

.field mResourceBackGround:Landroid/graphics/drawable/Drawable;

.field private mScaleSize:I

.field private mScrollDown:Landroid/widget/ImageView;

.field private mScrollUp:Landroid/widget/ImageView;

.field private mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

.field private mSupportHoverZoom:Z

.field private mSwipeLeft:Landroid/widget/ImageView;

.field private mSwipeRight:Landroid/widget/ImageView;

.field private mTabX:I

.field private mTabY:I

.field private final mThreshold:I

.field private mTouchGestureDetector:Landroid/view/GestureDetector;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mfloatingUILayoutFm:Landroid/widget/ImageView;

.field private movePad:Z

.field private movePtr:Z

.field private pCurX:F

.field private pCurY:F

.field private params_cursorCue:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/app/Service;)V
    .locals 10
    .param p1, "service"    # Landroid/app/Service;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-string v4, "FingerMouseGridUI"

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->TAG:Ljava/lang/String;

    .line 47
    const-string v4, "FingerMouseGridUIHoverZoom"

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->TAG_HOVERZOOM:Ljava/lang/String;

    .line 53
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 55
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    .line 57
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 59
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 63
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    .line 65
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 67
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    .line 69
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    .line 71
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;

    .line 73
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    .line 75
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    .line 77
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    .line 83
    iput v9, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    .line 85
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    .line 87
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    .line 89
    iput v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    .line 91
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    .line 95
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z

    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePtr:Z

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDouble:Z

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDoubleTap:Z

    .line 97
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    .line 99
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    .line 101
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    .line 103
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F

    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F

    .line 107
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isPokeLockEnable:Z

    .line 109
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResourceBackGround:Landroid/graphics/drawable/Drawable;

    .line 113
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLongPress:Z

    .line 115
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    .line 118
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 120
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCue:Landroid/widget/ImageView;

    .line 122
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    .line 124
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCueShown:Z

    .line 126
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSupportHoverZoom:Z

    .line 128
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    .line 130
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    .line 132
    iput-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->an:Landroid/view/animation/Animation;

    .line 133
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCount_hold:I

    .line 134
    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    iput v7, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    .line 135
    const/4 v4, 0x5

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mThreshold:I

    .line 136
    const/16 v4, 0x1e

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mOffsetCursorPosition:I

    .line 137
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoldCursorFlag:Z

    .line 139
    const/16 v4, 0x5a

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    .line 141
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomRotateHandler:Landroid/os/Handler;

    .line 167
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    .line 180
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$3;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    .line 781
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$6;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 791
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$7;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    move-object v4, p1

    .line 202
    check-cast v4, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .line 205
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "accessibility"

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "com.sec.feature.overlaymagnifier"

    invoke-static {v4, v5}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSupportHoverZoom:Z

    .line 212
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    .line 214
    check-cast p1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    .end local p1    # "service":Landroid/app/Service;
    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetHanlder()Landroid/os/Handler;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mParentSvcHandler:Landroid/os/Handler;

    .line 216
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    .line 218
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 220
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pointer_speed"

    invoke-static {v4, v5, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .local v3, "pointer_speed":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    if-eq v3, v4, :cond_0

    move v4, v3

    :goto_1
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    .line 224
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pointer_size"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .local v2, "pointer_size":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    if-eq v2, v4, :cond_1

    move v4, v2

    :goto_2
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    .line 228
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "assistant_menu_pad_size"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .local v1, "pad_size":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    if-eq v1, v4, :cond_2

    move v4, v1

    :goto_3
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    .line 232
    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    .line 234
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    if-nez v4, :cond_3

    .line 235
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f090019

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    .line 240
    :goto_4
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    .line 241
    iput-boolean v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    .line 242
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "FmMagnifier"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 243
    const-string v4, "FingerMouseGridUI"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[c] pointer_speed="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pointer_size= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pad_size="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mmMagnifierSize="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void

    .line 206
    .end local v1    # "pad_size":I
    .end local v2    # "pointer_size":I
    .end local v3    # "pointer_speed":I
    .restart local p1    # "service":Landroid/app/Service;
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 220
    .end local v0    # "e":Ljava/lang/Exception;
    .end local p1    # "service":Landroid/app/Service;
    .restart local v3    # "pointer_speed":I
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    goto/16 :goto_1

    .line 224
    .restart local v2    # "pointer_size":I
    :cond_1
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    goto/16 :goto_2

    .line 228
    .restart local v1    # "pad_size":I
    :cond_2
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    goto :goto_3

    .line 237
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f090018

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    goto :goto_4
.end method

.method private ConnectUIObject()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 288
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    if-le v1, v3, :cond_0

    .line 289
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    .line 291
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->setLayoutDimensions(I)V

    .line 292
    new-array v0, v3, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v2

    .line 299
    .local v0, "fmGridLayout":[[I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 300
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    aget-object v2, v0, v5

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    aget v2, v2, v3

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 304
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0024

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    .line 306
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0029

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    .line 308
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0026

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    .line 310
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0025

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;

    .line 312
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0030

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    .line 314
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScrollDown:Landroid/widget/ImageView;

    .line 316
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d002e

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScrollUp:Landroid/widget/ImageView;

    .line 318
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d002c

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSwipeLeft:Landroid/widget/ImageView;

    .line 320
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d002d

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSwipeRight:Landroid/widget/ImageView;

    .line 322
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0028

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;

    .line 325
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v2, 0x7f030003

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    .line 327
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0008

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCue:Landroid/widget/ImageView;

    .line 329
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCue:Landroid/widget/ImageView;

    const v2, 0x7f02003f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 331
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->an:Landroid/view/animation/Animation;

    .line 332
    return-void

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    aget-object v2, v0, v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    aget v2, v2, v3

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    goto/16 :goto_0

    .line 292
    :array_0
    .array-data 4
        0x7f03000a
        0x7f030008
        0x7f030009
    .end array-data

    :array_1
    .array-data 4
        0x7f030007
        0x7f030005
        0x7f030006
    .end array-data
.end method

.method private EnabledMagnifier()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/high16 v7, 0x41f00000    # 30.0f

    const/4 v6, 0x1

    .line 741
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomRotateHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 743
    iput v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    .line 744
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "FmMagnifier"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 746
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_magnifier_size"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    .line 747
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_value"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    .line 748
    const-string v1, "FingerMouseGridUIHoverZoom"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick mMagnifierSize = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ,onClick mScaleSize = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setMagnifierCursorImage(Landroid/content/Context;)V

    .line 754
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09001c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    .line 755
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f09001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->updateCursorFromGesture(FF)V

    .line 757
    :cond_0
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v4, 0x7f09001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 758
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f09001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->updateCursorFromGesture(FF)V

    .line 762
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    aget v2, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagPadSize:Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    aget v3, v3, v4

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    int-to-float v4, v4

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/accessibility/AccessibilityManager;->enableMagnifier(IIF)V

    .line 763
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {v1, v2, v3}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V

    .line 764
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    sub-float/2addr v2, v7

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    sub-float/2addr v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    .line 765
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate()V

    .line 766
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 772
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_value"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 775
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "hover_zoom_magnifier_size"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 779
    return-void

    .line 767
    :catch_0
    move-exception v0

    .line 768
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private Init()V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 247
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 248
    .local v1, "screen_size":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 249
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    .line 250
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    .line 254
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f090017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v0, v2

    .line 255
    .local v0, "deltaX":F
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    sub-float/2addr v2, v0

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    .line 256
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    .line 258
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->SetGridUIParams()V

    .line 260
    new-instance v2, Landroid/view/GestureDetector;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    new-instance v4, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;)V

    invoke-direct {v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTouchGestureDetector:Landroid/view/GestureDetector;

    .line 262
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ConnectUIObject()V

    .line 263
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->SetEventListener()V

    .line 264
    return-void
.end method

.method private SetEventListener()V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmTouchView:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$4;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMoveView:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$5;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 451
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 452
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSupportHoverZoom:Z

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScrollDown:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 458
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScrollUp:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 459
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSwipeLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 460
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSwipeRight:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 461
    return-void

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private SetGridUIParams()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 267
    sget v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->gWindowType:I

    .line 269
    .local v3, "windowParamType":I
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x28

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 277
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    const/high16 v2, -0x80000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit16 v1, v1, 0x8ad

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 284
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDoubleTap:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDoubleTap:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLongPress:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLongPress:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDouble:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDouble:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTouchGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePad:Z

    return p1
.end method

.method static synthetic access$1902(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePtr:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitX:I

    return p1
.end method

.method static synthetic access$202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInitY:I

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmRemoveView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mIBtnFmMagnifier:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mfloatingUILayoutFm:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getBackGroundImage(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    return v0
.end method

.method static synthetic access$2702(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    return p1
.end method

.method static synthetic access$2712(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    return v0
.end method

.method static synthetic access$2802(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    return p1
.end method

.method static synthetic access$2812(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    return v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mScaleSize:I

    return p1
.end method

.method static synthetic access$3100(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    return v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mSupportHoverZoom:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->EnabledMagnifier()V

    return-void
.end method

.method static synthetic access$3700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorOnTop()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorBottom()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->closeStatusBarPanel()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    return p1
.end method

.method static synthetic access$702(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p1, "x1"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method private changeCursorStatus(Z)V
    .locals 3
    .param p1, "isChange"    # Z

    .prologue
    .line 647
    if-eqz p1, :cond_2

    .line 648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    .line 649
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setNotificationCursorImage(Landroid/content/Context;I)V

    .line 650
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorOnTop()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 651
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->addCue()V

    .line 657
    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    .line 658
    return-void

    .line 653
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    .line 654
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setCursorImage(Landroid/content/Context;I)V

    .line 655
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->removeCue()V

    goto :goto_0
.end method

.method private closeStatusBarPanel()V
    .locals 3

    .prologue
    .line 661
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 663
    .local v0, "mStatusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 664
    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    .line 665
    :cond_0
    return-void
.end method

.method private getBackGroundImage(I)I
    .locals 3
    .param p1, "padSize"    # I

    .prologue
    const/4 v2, 0x1

    .line 1250
    const/4 v0, 0x0

    .line 1251
    .local v0, "backgroundResid":I
    packed-switch p1, :pswitch_data_0

    .line 1276
    :goto_0
    return v0

    .line 1253
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1254
    const v0, 0x7f02004a

    goto :goto_0

    .line 1256
    :cond_0
    const v0, 0x7f020030

    .line 1258
    goto :goto_0

    .line 1260
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1261
    const v0, 0x7f020046

    goto :goto_0

    .line 1263
    :cond_1
    const v0, 0x7f02002c

    .line 1265
    goto :goto_0

    .line 1267
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1268
    const v0, 0x7f020042

    goto :goto_0

    .line 1270
    :cond_2
    const v0, 0x7f020028

    .line 1272
    goto :goto_0

    .line 1251
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getHandmode()I
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 705
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f09005a

    :goto_0
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 707
    .local v2, "displayWidth":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->assistantmenuSettingsPrefs:Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 709
    .local v0, "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPosX:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f090015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v5, v2, v5

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 714
    .local v1, "cornertabX":I
    div-int/lit8 v3, v2, 0x2

    if-le v1, v3, :cond_1

    .line 715
    const/4 v3, 0x1

    .line 717
    :goto_1
    return v3

    .line 705
    .end local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .end local v1    # "cornertabX":I
    .end local v2    # "displayWidth":I
    :cond_0
    const v3, 0x7f090059

    goto :goto_0

    .restart local v0    # "assistantMenuSettingsPrefs":Landroid/content/SharedPreferences;
    .restart local v1    # "cornertabX":I
    .restart local v2    # "displayWidth":I
    :cond_1
    move v3, v4

    .line 717
    goto :goto_1
.end method

.method private injectAccessibilityMotionEvent(IZ)V
    .locals 2
    .param p1, "eventAction"    # I
    .param p2, "isDouble"    # Z

    .prologue
    .line 871
    const-string v0, "FingerMouseGridUI"

    const-string v1, "injectAccessibilityMotionEvent()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;IZ)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 951
    return-void
.end method

.method private isCursorBottom()Z
    .locals 4

    .prologue
    .line 639
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f09001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 640
    const/4 v0, 0x1

    .line 642
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCursorOnTop()Z
    .locals 3

    .prologue
    .line 631
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 632
    const/4 v0, 0x1

    .line 634
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLayoutDimensions(I)V
    .locals 2
    .param p1, "padSize"    # I

    .prologue
    .line 722
    packed-switch p1, :pswitch_data_0

    .line 738
    :goto_0
    return-void

    .line 724
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    .line 725
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 728
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    .line 729
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 732
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    .line 733
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    goto :goto_0

    .line 722
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public ClearMagnifierStatus()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 553
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 554
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "FmMagnifier"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 556
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->disableMagnifier()V

    .line 557
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    :goto_0
    iput v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    .line 563
    :cond_0
    return-void

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public InjectDoubleTapEvent(JJ[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IFFIII)V
    .locals 19
    .param p1, "downTime"    # J
    .param p3, "eventTime"    # J
    .param p5, "properties"    # [Landroid/view/MotionEvent$PointerProperties;
    .param p6, "pointerCoords"    # [Landroid/view/MotionEvent$PointerCoords;
    .param p7, "metaState"    # I
    .param p8, "xPrecision"    # F
    .param p9, "yPrecision"    # F
    .param p10, "deviceId"    # I
    .param p11, "edgeFlag"    # I
    .param p12, "eventFlag"    # I

    .prologue
    .line 1036
    const/16 v17, 0x0

    .line 1037
    .local v17, "mMotionEvent":Landroid/view/MotionEvent;
    new-instance v16, Landroid/app/Instrumentation;

    invoke-direct/range {v16 .. v16}, Landroid/app/Instrumentation;-><init>()V

    .line 1039
    .local v16, "insObj":Landroid/app/Instrumentation;
    const-wide/16 v0, 0x32

    add-long p3, p3, v0

    .line 1041
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v15, p12

    :try_start_0
    invoke-static/range {v0 .. v15}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 1048
    invoke-virtual/range {v16 .. v17}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 1050
    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v14, 0x0

    move-wide/from16 v0, p1

    move-wide/from16 v2, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v15, p12

    invoke-static/range {v0 .. v15}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    .line 1056
    invoke-virtual/range {v16 .. v17}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1058
    if-eqz v17, :cond_0

    .line 1059
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 1060
    const/16 v17, 0x0

    .line 1063
    :cond_0
    return-void

    .line 1058
    :catchall_0
    move-exception v0

    if-eqz v17, :cond_1

    .line 1059
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 1060
    const/16 v17, 0x0

    :cond_1
    throw v0
.end method

.method public RemoveView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 519
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 522
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 524
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->removeCue()V

    .line 525
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    .line 527
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 528
    iput-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 530
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-ne v1, v4, :cond_1

    .line 532
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->hideMagnifier()V

    .line 533
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 538
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverScaleObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 539
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverPadSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 542
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->CloseDialog()V

    .line 543
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    .line 547
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomRotateHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 548
    return-void

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 544
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public ScreenRotateStatusChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 571
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->RemoveView()V

    .line 572
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ShowView()V

    .line 573
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-ne v0, v4, :cond_0

    .line 574
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomRotateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 576
    :cond_0
    const-string v0, "FingerMouseGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init ScreenRotateStatusChanged mMagnifierSize= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mMagnifierSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-void
.end method

.method public ShowView()V
    .locals 9

    .prologue
    const/4 v1, -0x1

    const/4 v8, 0x1

    .line 468
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v2, :cond_1

    .line 469
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->Init()V

    .line 471
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->initCursorPreference()V

    .line 473
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabX:I

    .line 474
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mTabY:I

    .line 476
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUITab:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v4, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 478
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const v4, 0x7f040003

    invoke-static {v2, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    .line 479
    .local v6, "alphaAni":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUIMain:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 482
    new-instance v2, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {v2, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    .line 484
    const/16 v3, 0x7d6

    .line 485
    .local v3, "windowParamType":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v3, 0x8ad

    .line 487
    :cond_2
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const v4, 0x1000118

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 496
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 497
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0a0027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 499
    iput-boolean v8, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isPokeLockEnable:Z

    .line 500
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-ne v1, v8, :cond_3

    .line 501
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setMagnifierCursorImage(Landroid/content/Context;)V

    .line 503
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {v1, v2, v4}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V

    .line 504
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    :cond_3
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changeCursorStatus(Z)V

    .line 510
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    if-nez v1, :cond_4

    .line 511
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 513
    :cond_4
    return-void

    .line 505
    :catch_0
    move-exception v7

    .line 506
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public addCue()V
    .locals 6

    .prologue
    const/4 v1, -0x2

    .line 1302
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCueShown:Z

    if-nez v0, :cond_2

    .line 1303
    const/16 v3, 0x7d6

    .line 1304
    .local v3, "windowParamType":I
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v3, 0x8ad

    .line 1306
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_1

    .line 1307
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x118

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 1315
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1317
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1318
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCue:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->an:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCueShown:Z

    .line 1321
    .end local v3    # "windowParamType":I
    :cond_2
    return-void
.end method

.method public changePadSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 1246
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    .line 1247
    return-void
.end method

.method public changePointerSize(I)V
    .locals 3
    .param p1, "size"    # I

    .prologue
    .line 1241
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    .line 1242
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setCursorImage(Landroid/content/Context;I)V

    .line 1243
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    .line 1244
    return-void
.end method

.method public changePointerSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 1238
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    .line 1239
    return-void
.end method

.method public editCursorPreference(II)V
    .locals 6
    .param p1, "xPos"    # I
    .param p2, "yPos"    # I

    .prologue
    .line 668
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    .line 669
    .local v1, "dominantMode":I
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->fingerMousePref:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 671
    .local v0, "cursorPreference":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 672
    .local v2, "e":Landroid/content/SharedPreferences$Editor;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosX:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 673
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosY:Ljava/lang/String;

    invoke-interface {v2, v3, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 674
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 675
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 676
    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorPadSize:Ljava/lang/String;

    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 677
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 678
    return-void
.end method

.method public getPadxPos()I
    .locals 3

    .prologue
    .line 1280
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v1, :cond_1

    .line 1281
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1288
    :cond_0
    :goto_0
    return v0

    .line 1283
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1284
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    sub-int v0, v1, v2

    .line 1285
    .local v0, "valuePos":I
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1288
    .end local v0    # "valuePos":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPadyPos()I
    .locals 3

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 1295
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1297
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method getPointerCoordinates(Landroid/view/MotionEvent$PointerCoords;I)Landroid/view/MotionEvent$PointerCoords;
    .locals 5
    .param p1, "initial"    # Landroid/view/MotionEvent$PointerCoords;
    .param p2, "eventAction"    # I

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40800000    # 4.0f

    .line 954
    const/4 v2, 0x1

    new-array v0, v2, [Landroid/view/MotionEvent$PointerCoords;

    .line 955
    .local v0, "motion_EventpointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v1, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 956
    .local v1, "pc1":Landroid/view/MotionEvent$PointerCoords;
    move-object v1, p1

    .line 957
    packed-switch p2, :pswitch_data_0

    .line 977
    :goto_0
    aput-object v1, v0, v4

    .line 978
    aget-object v2, v0, v4

    return-object v2

    .line 959
    :pswitch_0
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 960
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 963
    :pswitch_1
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 964
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 967
    :pswitch_2
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 968
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 971
    :pswitch_3
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    mul-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    div-float/2addr v2, v3

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 972
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    iput v2, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    goto :goto_0

    .line 957
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public initCursorPreference()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 681
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getHandmode()I

    move-result v3

    .line 683
    .local v3, "dominantHand":I
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->fingerMousePref:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 685
    .local v0, "cursorPreference":Landroid/content/SharedPreferences;
    const/4 v2, 0x0

    .line 686
    .local v2, "defaultX":I
    if-ne v3, v7, :cond_0

    .line 687
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    sub-int v2, v4, v5

    .line 688
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isCocktailBarModel()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f090011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 690
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v6, 0x7f090016

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int v1, v4, v5

    .line 692
    .local v1, "deafaultY":I
    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cornerTabPortrait:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetVertical()Z

    move-result v5

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorPadSize:Ljava/lang/String;

    invoke-interface {v0, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPadSize:I

    if-ne v4, v5, :cond_1

    sget-object v4, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->dominantHandChanged:Ljava/lang/String;

    invoke-interface {v0, v4, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v3, :cond_2

    .line 696
    :cond_1
    invoke-virtual {p0, v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->editCursorPreference(II)V

    .line 698
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosX:Ljava/lang/String;

    invoke-interface {v0, v5, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 699
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->cursorGridPosY:Ljava/lang/String;

    invoke-interface {v0, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 700
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutHeight:I

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 701
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorLayoutWidth:I

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 702
    return-void
.end method

.method public injectMotionEvent(J[Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerCoords;IFFIIII)V
    .locals 25
    .param p1, "downTime"    # J
    .param p3, "properties"    # [Landroid/view/MotionEvent$PointerProperties;
    .param p4, "pointerCoords"    # Landroid/view/MotionEvent$PointerCoords;
    .param p5, "metaState"    # I
    .param p6, "xPrecision"    # F
    .param p7, "yPrecision"    # F
    .param p8, "deviceId"    # I
    .param p9, "edgeFlag"    # I
    .param p10, "eventFlag"    # I
    .param p11, "eventAction"    # I

    .prologue
    .line 985
    const/16 v20, 0x0

    .line 986
    .local v20, "mMotionEvent":Landroid/view/MotionEvent;
    new-instance v19, Landroid/app/Instrumentation;

    invoke-direct/range {v19 .. v19}, Landroid/app/Instrumentation;-><init>()V

    .line 987
    .local v19, "insObj":Landroid/app/Instrumentation;
    const/4 v2, 0x1

    new-array v9, v2, [Landroid/view/MotionEvent$PointerCoords;

    .line 988
    .local v9, "motion_EventpointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v21, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v21 .. v21}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 989
    .local v21, "pc1":Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v21, p4

    .line 991
    const/16 v22, 0x0

    .line 992
    .local v22, "stepX":F
    const/16 v23, 0x0

    .line 993
    .local v23, "stepY":F
    packed-switch p11, :pswitch_data_0

    .line 1010
    :goto_0
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    const/4 v2, 0x4

    move/from16 v0, v18

    if-ge v0, v2, :cond_0

    .line 1011
    :try_start_0
    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    add-float v2, v2, v22

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 1012
    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    add-float v2, v2, v23

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 1013
    const/4 v2, 0x0

    aput-object v21, v9, v2

    .line 1014
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 1015
    .local v4, "eventTime":J
    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    move-wide/from16 v2, p1

    move-object/from16 v8, p3

    move/from16 v10, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move/from16 v15, p9

    move/from16 v17, p10

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v20

    .line 1020
    invoke-virtual/range {v19 .. v20}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 995
    .end local v4    # "eventTime":J
    .end local v18    # "i":I
    :pswitch_0
    const/high16 v23, -0x3d380000    # -100.0f

    .line 996
    goto :goto_0

    .line 998
    :pswitch_1
    const/high16 v23, 0x42c80000    # 100.0f

    .line 999
    goto :goto_0

    .line 1001
    :pswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v22, v2, v3

    .line 1002
    goto :goto_0

    .line 1004
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    neg-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x41000000    # 8.0f

    div-float v22, v2, v3

    .line 1005
    goto :goto_0

    .line 1023
    .restart local v18    # "i":I
    :cond_0
    if-eqz v20, :cond_1

    .line 1024
    invoke-virtual/range {v20 .. v20}, Landroid/view/MotionEvent;->recycle()V

    .line 1025
    const/16 v20, 0x0

    .line 1026
    const/16 v21, 0x0

    .line 1027
    const/16 v19, 0x0

    .line 1031
    :cond_1
    return-void

    .line 1023
    :catchall_0
    move-exception v2

    if-eqz v20, :cond_2

    .line 1024
    invoke-virtual/range {v20 .. v20}, Landroid/view/MotionEvent;->recycle()V

    .line 1025
    const/16 v20, 0x0

    .line 1026
    const/16 v21, 0x0

    .line 1027
    const/16 v19, 0x0

    :cond_2
    throw v2

    .line 993
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 803
    const-string v0, "FingerMouseGridUI"

    const-string v1, " onClick  "

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 868
    :goto_0
    :pswitch_0
    return-void

    .line 806
    :pswitch_1
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.grid_FmClosed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mFmGridUILayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->editCursorPreference(II)V

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mParentSvcHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 811
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->setFMClosedStatus(Z)V

    goto :goto_0

    .line 816
    :pswitch_2
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.grid_FmMagnifier"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 818
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-nez v0, :cond_2

    .line 819
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_magnifier"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 820
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->removeInstance()V

    .line 821
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->getInstance()Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0038

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mOkClickListener:Landroid/content/DialogInterface$OnClickListener;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/app/assistantmenu/gridUI/AlertDialogUI;->ShowDialog(Landroid/content/Context;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 825
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->EnabledMagnifier()V

    goto :goto_0

    .line 828
    :cond_2
    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    .line 829
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "FmMagnifier"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 830
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    if-eqz v0, :cond_3

    .line 831
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setNotificationCursorImage(Landroid/content/Context;I)V

    .line 836
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->disableMagnifier()V

    .line 837
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 841
    :goto_2
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    goto/16 :goto_0

    .line 833
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSize:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->setCursorImage(Landroid/content/Context;I)V

    goto :goto_1

    .line 838
    :catch_0
    move-exception v6

    .line 839
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 846
    .end local v6    # "e":Ljava/lang/Exception;
    :pswitch_3
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.fm_scrollDown"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    invoke-direct {p0, v3, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto/16 :goto_0

    .line 851
    :pswitch_4
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.fm_scrollUp"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    invoke-direct {p0, v4, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto/16 :goto_0

    .line 856
    :pswitch_5
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.fm_swipeLeft"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    const/4 v0, 0x3

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto/16 :goto_0

    .line 861
    :pswitch_6
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onClick() :: R.id.fm_swipeRight"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    const/4 v0, 0x4

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V

    goto/16 :goto_0

    .line 804
    :pswitch_data_0
    .packed-switch 0x7f0d0025
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public removeCue()V
    .locals 2

    .prologue
    .line 1324
    iget-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCueShown:Z

    if-eqz v0, :cond_0

    .line 1325
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorCueLayout:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 1327
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->params_cursorCue:Landroid/view/WindowManager$LayoutParams;

    .line 1328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCueShown:Z

    .line 1330
    :cond_0
    return-void
.end method

.method public update(FF)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/high16 v8, 0x41f00000    # 30.0f

    const/high16 v7, 0x40a00000    # 5.0f

    .line 1169
    iput-boolean v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoldCursorFlag:Z

    .line 1172
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    cmpg-float v5, v5, p1

    if-gtz v5, :cond_5

    .line 1173
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    sub-int v2, v5, v6

    .line 1174
    .local v2, "left":I
    float-to-int v5, p1

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    add-int v3, v5, v6

    .line 1181
    .local v3, "right":I
    :goto_0
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    cmpg-float v5, v5, p2

    if-gtz v5, :cond_6

    .line 1182
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    div-int/lit8 v6, v6, 0x2

    sub-int v4, v5, v6

    .line 1183
    .local v4, "top":I
    float-to-int v5, p2

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    mul-int/lit8 v6, v6, 0x2

    add-int v0, v5, v6

    .line 1190
    .local v0, "bottom":I
    :goto_1
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    if-ne v5, v9, :cond_9

    .line 1192
    :try_start_0
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    add-float/2addr v5, v7

    cmpl-float v5, v5, p1

    if-lez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    sub-float/2addr v5, v7

    cmpg-float v5, v5, p1

    if-gez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    add-float/2addr v5, v7

    cmpl-float v5, v5, p2

    if-lez v5, :cond_8

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    sub-float/2addr v5, v7

    cmpg-float v5, v5, p2

    if-gez v5, :cond_8

    .line 1193
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCount_hold:I

    const/4 v6, 0x5

    if-ne v5, v6, :cond_7

    .line 1194
    const-string v5, "FingerMouseGridUI"

    const-string v6, "stabel cursor"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoldCursorFlag:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1211
    :goto_2
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isSecureKeyguardLocked()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->isKeyguardLocked()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1212
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isPokeLockEnable:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    if-eqz v5, :cond_1

    .line 1213
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 1214
    :cond_1
    iput-boolean v10, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isPokeLockEnable:Z

    .line 1216
    :cond_2
    cmpl-float v5, p1, v11

    if-nez v5, :cond_3

    cmpl-float v5, p2, v11

    if-eqz v5, :cond_a

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->isCursorShown()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1217
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5, v9}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->showCursor(Z)V

    .line 1234
    :cond_4
    :goto_3
    return-void

    .line 1176
    .end local v0    # "bottom":I
    .end local v2    # "left":I
    .end local v3    # "right":I
    .end local v4    # "top":I
    :cond_5
    float-to-int v5, p1

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    sub-int v2, v5, v6

    .line 1177
    .restart local v2    # "left":I
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    add-int v3, v5, v6

    .restart local v3    # "right":I
    goto/16 :goto_0

    .line 1185
    :cond_6
    float-to-int v5, p2

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    div-int/lit8 v6, v6, 0x2

    sub-int v4, v5, v6

    .line 1186
    .restart local v4    # "top":I
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mInvalidateMargin:I

    mul-int/lit8 v6, v6, 0x2

    add-int v0, v5, v6

    .restart local v0    # "bottom":I
    goto :goto_1

    .line 1197
    :cond_7
    :try_start_1
    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCount_hold:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCount_hold:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1204
    :catch_0
    move-exception v1

    .line 1205
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_2

    .line 1200
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_8
    :try_start_2
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurX:F

    .line 1201
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->pCurY:F

    .line 1202
    const/4 v5, 0x0

    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCount_hold:I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 1208
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5, p1, p2}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    goto :goto_2

    .line 1219
    :cond_a
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    if-ne v5, v9, :cond_b

    .line 1221
    :try_start_3
    iget-boolean v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoldCursorFlag:Z

    if-nez v5, :cond_4

    .line 1222
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    sub-float v6, p1, v8

    sub-float v7, p2, v8

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    .line 1223
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5, v2, v4, v3, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate(IIII)V

    .line 1224
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v5, p1, p2}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_3

    .line 1226
    :catch_1
    move-exception v1

    .line 1227
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 1230
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_b
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    sub-float v6, p1, v8

    sub-float v7, p2, v8

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->Update(FF)V

    .line 1231
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorView:Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/gridUI/CursorView;->invalidate()V

    goto :goto_3
.end method

.method public updateCursorFromGesture(FF)V
    .locals 9
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 582
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mPointerSpeed:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/high16 v6, 0x40400000    # 3.0f

    div-float v3, v4, v6

    .line 584
    .local v3, "mul":F
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->movePtr:Z

    if-eqz v4, :cond_5

    .line 585
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    mul-float v6, v3, p1

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    .line 586
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    mul-float v6, v3, p2

    add-float/2addr v4, v6

    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    .line 588
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f09001c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int v0, v4, v6

    .line 589
    .local v0, "cursorXposMax":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f09001d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    sub-int v1, v4, v6

    .line 591
    .local v1, "cursorYposMax":I
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomEnabled:I

    if-ne v4, v8, :cond_0

    .line 592
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceWidth:I

    .line 593
    iget v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDeviceheight:I

    .line 596
    :cond_0
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    int-to-float v6, v0

    cmpl-float v4, v4, v6

    if-lez v4, :cond_6

    int-to-float v4, v0

    :goto_0
    iput v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    .line 597
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    int-to-float v6, v1

    cmpl-float v4, v4, v6

    if-lez v4, :cond_8

    int-to-float v5, v1

    :cond_1
    :goto_1
    iput v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    .line 600
    :try_start_0
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mHoverZoomPadDisplayed:Z

    if-ne v4, v8, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mManager:Landroid/view/accessibility/AccessibilityManager;

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {v4, v5, v6}, Landroid/view/accessibility/AccessibilityManager;->showMagnifier(FF)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorOnTop()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorBottom()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 606
    :cond_4
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    if-nez v4, :cond_9

    .line 607
    invoke-direct {p0, v8}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changeCursorStatus(Z)V

    .line 619
    .end local v0    # "cursorXposMax":I
    .end local v1    # "cursorYposMax":I
    :cond_5
    :goto_3
    return-void

    .line 596
    .restart local v0    # "cursorXposMax":I
    .restart local v1    # "cursorYposMax":I
    :cond_6
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_7

    move v4, v5

    goto :goto_0

    :cond_7
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    goto :goto_0

    .line 597
    :cond_8
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_1

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    goto :goto_1

    .line 601
    :catch_0
    move-exception v2

    .line 602
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 609
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    goto :goto_3

    .line 612
    :cond_a
    iget-boolean v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorChange:Z

    if-eqz v4, :cond_b

    .line 613
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changeCursorStatus(Z)V

    goto :goto_3

    .line 615
    :cond_b
    iget v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F

    iget v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    goto :goto_3
.end method

.method public updateStatusBarStatus()V
    .locals 2

    .prologue
    .line 622
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    .line 623
    .local v0, "isQuickPanelOpen":Z
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorBottom()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorOnTop()Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 624
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changeCursorStatus(Z)V

    .line 628
    :goto_0
    return-void

    .line 626
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changeCursorStatus(Z)V

    goto :goto_0
.end method
