.class public Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;
.super Landroid/app/IntentService;
.source "AMMetaDataParserService.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field confLang:Landroid/content/res/Configuration;

.field private dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

.field packMngr:Landroid/content/pm/PackageManager;

.field resLang:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 42
    const-string v0, "AMMetaDataParserService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    .line 37
    const-string v0, "AMMetaDataParserService"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    .line 44
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->confLang:Landroid/content/res/Configuration;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->confLang:Landroid/content/res/Configuration;

    new-instance v1, Ljava/util/Locale;

    const-string v2, "en"

    invoke-direct {v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 46
    return-void
.end method

.method private constructDBdata(Landroid/content/pm/ComponentInfo;Ljava/lang/String;)V
    .locals 29
    .param p1, "actInfo"    # Landroid/content/pm/ComponentInfo;
    .param p2, "packName"    # Ljava/lang/String;

    .prologue
    .line 142
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Resource data:Loop for running activity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ComponentInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v16, v0

    .line 144
    .local v16, "metaBundle":Landroid/os/Bundle;
    if-eqz v16, :cond_4

    .line 145
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    const-string v2, "Resource data:Inside bundle  check"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual/range {v16 .. v16}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 147
    .local v15, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getResourcePackageName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/16 v26, 0x0

    .line 149
    .local v26, "value":I
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    .line 150
    .local v25, "valKey":Ljava/lang/Object;
    move-object/from16 v0, v25

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 151
    check-cast v25, Ljava/lang/Integer;

    .end local v25    # "valKey":Ljava/lang/Object;
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 154
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Resource data:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    if-eqz v26, :cond_0

    .line 157
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    .line 158
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->confLang:Landroid/content/res/Configuration;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move/from16 v0, v26

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v18

    .line 160
    .local v18, "resName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getResourceName:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move/from16 v0, v26

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getResourceTypeName(I)Ljava/lang/String;

    move-result-object v19

    .line 162
    .local v19, "resType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getResourceTypeName"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const-string v1, "xml"

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "xml"

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move/from16 v0, v26

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v27

    .line 166
    .local v27, "xmlParser":Landroid/content/res/XmlResourceParser;
    if-eqz v27, :cond_0

    .line 167
    const/16 v22, 0x0

    .line 168
    .local v22, "state":Z
    :goto_1
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_7

    .line 169
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 170
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v24

    .line 171
    .local v24, "tagName":Ljava/lang/String;
    const-string v1, "assistant-list"

    move-object/from16 v0, v24

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 172
    const/16 v22, 0x1

    .line 174
    :cond_2
    const-string v1, "button"

    move-object/from16 v0, v24

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v22, :cond_3

    .line 177
    const/4 v1, 0x0

    const-string v2, "resource"

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v1, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v20

    .line 178
    .local v20, "resid":I
    const/4 v1, 0x0

    const-string v2, "action"

    move-object/from16 v0, v27

    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 181
    .local v9, "intentAction":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move/from16 v0, v20

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 182
    .local v21, "resourceValue":Landroid/graphics/drawable/Drawable;
    check-cast v21, Landroid/graphics/drawable/BitmapDrawable;

    .end local v21    # "resourceValue":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 183
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    new-instance v23, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v23 .. v23}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 184
    .local v23, "stream":Ljava/io/ByteArrayOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v12, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 185
    invoke-virtual/range {v23 .. v23}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    .line 186
    .local v10, "bitMapData":[B
    const/4 v1, 0x0

    const-string v2, "name"

    move-object/from16 v0, v27

    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 187
    .local v6, "label":Ljava/lang/String;
    const-string v1, "@string"

    invoke-virtual {v6, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 188
    const/4 v1, 0x0

    const-string v2, "name"

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-interface {v0, v1, v2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v17

    .line 189
    .local v17, "nameid":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->resLang:Landroid/content/res/Resources;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 190
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Icon data: Resource"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v28, v0

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->longValue()J

    move-result-wide v7

    const/4 v11, 0x1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v11}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BI)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->addAMData(Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;)V

    .line 203
    .end local v6    # "label":Ljava/lang/String;
    .end local v9    # "intentAction":Ljava/lang/String;
    .end local v10    # "bitMapData":[B
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "nameid":I
    .end local v20    # "resid":I
    .end local v23    # "stream":Ljava/io/ByteArrayOutputStream;
    .end local v24    # "tagName":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3

    goto/16 :goto_1

    .line 211
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "key":Ljava/lang/String;
    .end local v16    # "metaBundle":Landroid/os/Bundle;
    .end local v18    # "resName":Ljava/lang/String;
    .end local v19    # "resType":Ljava/lang/String;
    .end local v22    # "state":Z
    .end local v26    # "value":I
    .end local v27    # "xmlParser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v13

    .line 212
    .local v13, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v13}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 221
    .end local v13    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    :goto_3
    return-void

    .line 193
    .restart local v6    # "label":Ljava/lang/String;
    .restart local v9    # "intentAction":Ljava/lang/String;
    .restart local v10    # "bitMapData":[B
    .restart local v12    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "key":Ljava/lang/String;
    .restart local v16    # "metaBundle":Landroid/os/Bundle;
    .restart local v18    # "resName":Ljava/lang/String;
    .restart local v19    # "resType":Ljava/lang/String;
    .restart local v20    # "resid":I
    .restart local v22    # "state":Z
    .restart local v23    # "stream":Ljava/io/ByteArrayOutputStream;
    .restart local v24    # "tagName":Ljava/lang/String;
    .restart local v26    # "value":I
    .restart local v27    # "xmlParser":Landroid/content/res/XmlResourceParser;
    :cond_5
    const/4 v1, 0x0

    :try_start_1
    const-string v2, "name"

    move-object/from16 v0, v27

    invoke-interface {v0, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 194
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Icon data: String"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v28, v0

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p1

    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    const-wide/16 v7, 0x0

    const/4 v11, 0x1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v11}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BI)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->addAMData(Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    .line 213
    .end local v6    # "label":Ljava/lang/String;
    .end local v9    # "intentAction":Ljava/lang/String;
    .end local v10    # "bitMapData":[B
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "key":Ljava/lang/String;
    .end local v16    # "metaBundle":Landroid/os/Bundle;
    .end local v18    # "resName":Ljava/lang/String;
    .end local v19    # "resType":Ljava/lang/String;
    .end local v20    # "resid":I
    .end local v22    # "state":Z
    .end local v23    # "stream":Ljava/io/ByteArrayOutputStream;
    .end local v24    # "tagName":Ljava/lang/String;
    .end local v26    # "value":I
    .end local v27    # "xmlParser":Landroid/content/res/XmlResourceParser;
    :catch_1
    move-exception v13

    .line 214
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 198
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "key":Ljava/lang/String;
    .restart local v16    # "metaBundle":Landroid/os/Bundle;
    .restart local v18    # "resName":Ljava/lang/String;
    .restart local v19    # "resType":Ljava/lang/String;
    .restart local v22    # "state":Z
    .restart local v26    # "value":I
    .restart local v27    # "xmlParser":Landroid/content/res/XmlResourceParser;
    :cond_6
    :try_start_2
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 199
    if-nez v22, :cond_3

    .line 205
    :cond_7
    invoke-interface/range {v27 .. v27}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 215
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "key":Ljava/lang/String;
    .end local v16    # "metaBundle":Landroid/os/Bundle;
    .end local v18    # "resName":Ljava/lang/String;
    .end local v19    # "resType":Ljava/lang/String;
    .end local v22    # "state":Z
    .end local v26    # "value":I
    .end local v27    # "xmlParser":Landroid/content/res/XmlResourceParser;
    :catch_2
    move-exception v13

    .line 216
    .local v13, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v13}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_3

    .line 217
    .end local v13    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_3
    move-exception v13

    .line 218
    .local v13, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {v13}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_3
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 23
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 52
    :try_start_0
    const-string v15, "com.samsung.android.app.assistantmenu.permission.ADVERTISE_ASSISTANTMENU"

    .line 54
    .local v15, "permEAM":Ljava/lang/String;
    const-string v21, "trigger"

    const/16 v22, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 56
    .local v9, "iSerExtra":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    .line 57
    packed-switch v9, :pswitch_data_0

    .line 136
    .end local v9    # "iSerExtra":I
    .end local v15    # "permEAM":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 59
    .restart local v9    # "iSerExtra":I
    .restart local v15    # "permEAM":Ljava/lang/String;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->deleteAllAMdata()V

    .line 60
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v21, 0x0

    aput-object v15, v18, v21

    .line 61
    .local v18, "permName":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    const/16 v22, 0x1001

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackagesHoldingPermissions([Ljava/lang/String;I)Ljava/util/List;

    move-result-object v13

    .line 63
    .local v13, "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/pm/PackageInfo;

    .line 64
    .local v19, "resInfo":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v19

    iget-object v12, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 66
    .local v12, "packName":Ljava/lang/String;
    const/16 v20, 0x0

    .line 67
    .local v20, "state":Z
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    move-object/from16 v17, v0

    .line 69
    .local v17, "permInfo":[Ljava/lang/String;
    if-eqz v17, :cond_2

    .line 70
    move-object/from16 v5, v17

    .local v5, "arr$":[Ljava/lang/String;
    array-length v10, v5

    .local v10, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v10, :cond_2

    aget-object v14, v5, v8

    .line 71
    .local v14, "perm":Ljava/lang/String;
    const-string v21, "com.samsung.android.app.assistantmenu.permission.ADVERTISE_ASSISTANTMENU"

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 72
    const/16 v20, 0x1

    .line 77
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v10    # "len$":I
    .end local v14    # "perm":Ljava/lang/String;
    :cond_2
    if-eqz v20, :cond_1

    .line 80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    const/16 v22, 0x81

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v12, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 82
    .local v11, "packInfo":Landroid/content/pm/PackageInfo;
    if-eqz v11, :cond_1

    .line 83
    iget-object v4, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    .line 84
    .local v4, "actInfoList":[Landroid/content/pm/ActivityInfo;
    if-eqz v4, :cond_1

    .line 85
    move-object v5, v4

    .local v5, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v10, v5

    .restart local v10    # "len$":I
    const/4 v8, 0x0

    .restart local v8    # "i$":I
    :goto_2
    if-ge v8, v10, :cond_1

    aget-object v3, v5, v8

    .line 86
    .local v3, "actInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v12}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->constructDBdata(Landroid/content/pm/ComponentInfo;Ljava/lang/String;)V

    .line 85
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 70
    .end local v3    # "actInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "actInfoList":[Landroid/content/pm/ActivityInfo;
    .end local v11    # "packInfo":Landroid/content/pm/PackageInfo;
    .local v5, "arr$":[Ljava/lang/String;
    .restart local v14    # "perm":Ljava/lang/String;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 93
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v10    # "len$":I
    .end local v12    # "packName":Ljava/lang/String;
    .end local v13    # "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v14    # "perm":Ljava/lang/String;
    .end local v17    # "permInfo":[Ljava/lang/String;
    .end local v18    # "permName":[Ljava/lang/String;
    .end local v19    # "resInfo":Landroid/content/pm/PackageInfo;
    .end local v20    # "state":Z
    :pswitch_1
    const-string v21, "package"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 94
    .restart local v12    # "packName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15, v12}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 95
    .local v17, "permInfo":I
    if-nez v17, :cond_0

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    const/16 v22, 0x81

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v12, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 97
    .restart local v11    # "packInfo":Landroid/content/pm/PackageInfo;
    if-eqz v11, :cond_0

    .line 98
    iget-object v4, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    .line 99
    .restart local v4    # "actInfoList":[Landroid/content/pm/ActivityInfo;
    if-eqz v4, :cond_0

    .line 100
    move-object v5, v4

    .local v5, "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v10, v5

    .restart local v10    # "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_3
    if-ge v7, v10, :cond_0

    aget-object v3, v5, v7

    .line 101
    .restart local v3    # "actInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v12}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->constructDBdata(Landroid/content/pm/ComponentInfo;Ljava/lang/String;)V

    .line 100
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 108
    .end local v3    # "actInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "actInfoList":[Landroid/content/pm/ActivityInfo;
    .end local v5    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v7    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "packInfo":Landroid/content/pm/PackageInfo;
    .end local v12    # "packName":Ljava/lang/String;
    .end local v17    # "permInfo":I
    :pswitch_2
    const-string v21, "package"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 109
    .restart local v12    # "packName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->deleteAMData(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 130
    .end local v9    # "iSerExtra":I
    .end local v12    # "packName":Ljava/lang/String;
    .end local v15    # "permEAM":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 132
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 112
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v9    # "iSerExtra":I
    .restart local v15    # "permEAM":Ljava/lang/String;
    :pswitch_3
    :try_start_1
    const-string v21, "package"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 113
    .restart local v12    # "packName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15, v12}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 114
    .local v16, "permGrant":I
    if-nez v16, :cond_0

    .line 115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->deleteAMData(Ljava/lang/String;)V

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->packMngr:Landroid/content/pm/PackageManager;

    move-object/from16 v21, v0

    const/16 v22, 0x81

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v12, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 117
    .restart local v11    # "packInfo":Landroid/content/pm/PackageInfo;
    if-eqz v11, :cond_0

    .line 118
    iget-object v4, v11, Landroid/content/pm/PackageInfo;->activities:[Landroid/content/pm/ActivityInfo;

    .line 119
    .restart local v4    # "actInfoList":[Landroid/content/pm/ActivityInfo;
    if-eqz v4, :cond_0

    .line 120
    move-object v5, v4

    .restart local v5    # "arr$":[Landroid/content/pm/ActivityInfo;
    array-length v10, v5

    .restart local v10    # "len$":I
    const/4 v7, 0x0

    .restart local v7    # "i$":I
    :goto_4
    if-ge v7, v10, :cond_0

    aget-object v3, v5, v7

    .line 121
    .restart local v3    # "actInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v12}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMMetaDataParserService;->constructDBdata(Landroid/content/pm/ComponentInfo;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 120
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 133
    .end local v3    # "actInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "actInfoList":[Landroid/content/pm/ActivityInfo;
    .end local v5    # "arr$":[Landroid/content/pm/ActivityInfo;
    .end local v7    # "i$":I
    .end local v9    # "iSerExtra":I
    .end local v10    # "len$":I
    .end local v11    # "packInfo":Landroid/content/pm/PackageInfo;
    .end local v12    # "packName":Ljava/lang/String;
    .end local v15    # "permEAM":Ljava/lang/String;
    .end local v16    # "permGrant":I
    :catch_1
    move-exception v6

    .line 134
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
