.class public Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;
.super Ljava/lang/Object;
.source "ZoomManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;,
        Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    }
.end annotation


# static fields
.field public static final BOUNDARY:I = 0x23

.field public static final SEEK_BAR_DISTANCE:I = 0x64

.field private static final TAG:Ljava/lang/String; = "ZoomManager"

.field private static mContext:Landroid/content/Context;

.field private static mDisWidth:I

.field private static mZoomDis:F

.field private static packageOffsetIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mZoomDis:F

    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    .line 35
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private static GenerateTochEvent(IIIII)V
    .locals 46
    .param p0, "y"    # I
    .param p1, "leftStart"    # I
    .param p2, "leftEnd"    # I
    .param p3, "rightStart"    # I
    .param p4, "rightEnd"    # I

    .prologue
    .line 260
    sget v13, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->touchDeviceID:I

    .line 261
    .local v13, "deviceId":I
    const/high16 v11, 0x3f800000    # 1.0f

    .line 262
    .local v11, "xPrecision":F
    const/high16 v12, 0x3f800000    # 1.0f

    .line 263
    .local v12, "yPrecision":F
    const/4 v14, 0x0

    .line 264
    .local v14, "edgeFlag":I
    const/16 v44, 0x5

    .line 265
    .local v44, "loopCnt":I
    const/16 v17, 0x28

    .line 266
    .local v17, "delay":I
    const/high16 v16, -0x80000000

    .line 268
    .local v16, "AMOTION_EVENT_FLAG_WINDOW_IS_ACCESSIBILITY":I
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 269
    .local v2, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 271
    .local v4, "eventTime":J
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 272
    .local v9, "coord":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x2

    new-array v0, v6, [Landroid/view/MotionEvent$PointerCoords;

    move-object/from16 v25, v0

    .line 274
    .local v25, "coords":[Landroid/view/MotionEvent$PointerCoords;
    const/4 v6, 0x1

    new-array v8, v6, [I

    const/4 v6, 0x0

    const/4 v7, 0x0

    aput v7, v8, v6

    .line 277
    .local v8, "pointerId":[I
    const/4 v6, 0x2

    new-array v0, v6, [I

    move-object/from16 v24, v0

    fill-array-data v24, :array_0

    .line 281
    .local v24, "pointerIds":[I
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    if-nez v6, :cond_1

    .line 282
    const/16 v44, 0x1e

    .line 283
    const/16 v17, 0x32

    .line 299
    :cond_0
    :goto_0
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v9, v6

    .line 300
    const/4 v6, 0x0

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v25, v6

    .line 301
    const/4 v6, 0x1

    new-instance v7, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v7}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v7, v25, v6

    .line 303
    const/4 v6, 0x0

    aget-object v6, v9, v6

    const/high16 v7, 0x3f800000    # 1.0f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 304
    const/4 v6, 0x0

    aget-object v6, v25, v6

    const/high16 v7, 0x3f800000    # 1.0f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 305
    const/4 v6, 0x1

    aget-object v6, v25, v6

    const/high16 v7, 0x3f800000    # 1.0f

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 307
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move/from16 v0, p0

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 308
    const/4 v6, 0x0

    aget-object v6, v25, v6

    move/from16 v0, p0

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 309
    const/4 v6, 0x1

    aget-object v6, v25, v6

    move/from16 v0, p0

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 311
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move/from16 v0, p1

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 312
    const/4 v6, 0x0

    aget-object v6, v25, v6

    move/from16 v0, p1

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 313
    const/4 v6, 0x1

    aget-object v6, v25, v6

    move/from16 v0, p3

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 315
    new-instance v43, Landroid/app/Instrumentation;

    invoke-direct/range {v43 .. v43}, Landroid/app/Instrumentation;-><init>()V

    .line 317
    .local v43, "inst":Landroid/app/Instrumentation;
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "left : from "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " to "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "right : from "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p3

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " to "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, p4

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_DOWN :: x = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v9, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v9, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v15, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v45

    .line 323
    .local v45, "mMotionEvent":Landroid/view/MotionEvent;
    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 325
    move/from16 v0, v17

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 327
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_POINTER_DOWN :: x1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", x2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const/16 v22, 0x105

    const/16 v23, 0x2

    const/16 v26, 0x0

    const/16 v31, 0x0

    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v32, v16

    invoke-static/range {v18 .. v32}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v45

    .line 332
    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 334
    move/from16 v0, v17

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 336
    const/16 v42, 0x0

    .local v42, "i":I
    :goto_1
    move/from16 v0, v42

    move/from16 v1, v44

    if-ge v0, v1, :cond_6

    .line 338
    const/4 v6, 0x0

    aget-object v6, v25, v6

    iget v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    sub-int v10, p2, p1

    div-int v10, v10, v44

    int-to-float v10, v10

    add-float/2addr v7, v10

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 339
    const/4 v6, 0x1

    aget-object v6, v25, v6

    iget v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    sub-int v10, p4, p3

    div-int v10, v10, v44

    int-to-float v10, v10

    add-float/2addr v7, v10

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 341
    move/from16 v0, v17

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 343
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_MOVE :: x1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", x2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const/16 v22, 0x2

    const/16 v23, 0x2

    const/16 v26, 0x0

    const/16 v31, 0x0

    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v32, v16

    invoke-static/range {v18 .. v32}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v45

    .line 348
    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 336
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_1

    .line 284
    .end local v42    # "i":I
    .end local v43    # "inst":Landroid/app/Instrumentation;
    .end local v45    # "mMotionEvent":Landroid/view/MotionEvent;
    :cond_1
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    .line 285
    const/16 v44, 0x14

    .line 286
    const/16 v17, 0x32

    goto/16 :goto_0

    .line 287
    :cond_2
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v7, 0xa

    if-ne v6, v7, :cond_3

    .line 288
    const/16 v44, 0xa

    .line 289
    const/16 v17, 0x32

    goto/16 :goto_0

    .line 290
    :cond_3
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v7, 0xd

    if-ne v6, v7, :cond_4

    .line 291
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 292
    :cond_4
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v7, 0xf

    if-ne v6, v7, :cond_5

    .line 293
    const/16 v44, 0xf

    goto/16 :goto_0

    .line 294
    :cond_5
    sget v6, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v7, 0x13

    if-ne v6, v7, :cond_0

    .line 295
    const/16 v44, 0x19

    .line 296
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 351
    .restart local v42    # "i":I
    .restart local v43    # "inst":Landroid/app/Instrumentation;
    .restart local v45    # "mMotionEvent":Landroid/view/MotionEvent;
    :cond_6
    move/from16 v0, v17

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 352
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_POINTER_UP :: x1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y1 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", x2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y2 = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x1

    aget-object v10, v25, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    const/16 v22, 0x106

    const/16 v23, 0x2

    const/16 v26, 0x0

    const/16 v31, 0x0

    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    move/from16 v27, v11

    move/from16 v28, v12

    move/from16 v29, v13

    move/from16 v30, v14

    move/from16 v32, v16

    invoke-static/range {v18 .. v32}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v45

    .line 359
    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 361
    move/from16 v0, v17

    int-to-long v6, v0

    add-long/2addr v4, v6

    .line 363
    const/4 v6, 0x0

    aget-object v6, v9, v6

    move/from16 v0, p2

    int-to-float v7, v0

    iput v7, v6, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 364
    const-string v6, "ZoomManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ACTION_UP :: x = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v9, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", y = "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v10, 0x0

    aget-object v10, v9, v10

    iget v10, v10, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const/16 v30, 0x1

    const/16 v31, 0x1

    const/16 v34, 0x0

    const/16 v39, 0x0

    move-wide/from16 v26, v2

    move-wide/from16 v28, v4

    move-object/from16 v32, v24

    move-object/from16 v33, v9

    move/from16 v35, v11

    move/from16 v36, v12

    move/from16 v37, v13

    move/from16 v38, v14

    move/from16 v40, v16

    invoke-static/range {v26 .. v40}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v45

    .line 367
    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 370
    sub-long v6, v4, v2

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :goto_2
    return-void

    .line 371
    :catch_0
    move-exception v41

    .line 373
    .local v41, "e":Ljava/lang/InterruptedException;
    invoke-virtual/range {v41 .. v41}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 277
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public static GetPackageExpandOffset(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;I)I
    .locals 3
    .param p0, "type"    # Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .param p1, "distance"    # I

    .prologue
    .line 158
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    if-ne p0, v0, :cond_0

    sget v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    .line 159
    int-to-float v0, p1

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->access$100()[F

    move-result-object v1

    sget v2, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    aget v1, v1, v2

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 163
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    if-ne p0, v0, :cond_1

    sget v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 164
    int-to-float v0, p1

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->access$100()[F

    move-result-object v1

    sget v2, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    aget v1, v1, v2

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 167
    :cond_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    if-ne p0, v0, :cond_2

    sget v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    .line 168
    int-to-float v0, p1

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->access$100()[F

    move-result-object v1

    sget v2, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    aget v1, v1, v2

    mul-float/2addr v0, v1

    float-to-int p1, v0

    .line 170
    :cond_2
    return p1
.end method

.method public static Zoom(Landroid/view/WindowManager$LayoutParams;Landroid/view/View;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    .locals 12
    .param p0, "p"    # Landroid/view/WindowManager$LayoutParams;
    .param p1, "v"    # Landroid/view/View;
    .param p2, "type"    # Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .param p3, "ratio"    # F

    .prologue
    .line 187
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 188
    .local v5, "point":Landroid/graphics/Point;
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    const-string v9, "window"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 190
    iget v8, v5, Landroid/graphics/Point;->x:I

    sput v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    .line 191
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    const/high16 v9, 0x40400000    # 3.0f

    invoke-static {v8, v9}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->dpToPixel(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    sput v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mZoomDis:F

    .line 196
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09006c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 199
    .local v0, "boundary":I
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->GetOffsetOfPackageIndex()I

    move-result v8

    sput v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    .line 200
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    mul-int/lit8 v9, v0, 0x2

    sub-int/2addr v8, v9

    int-to-float v8, v8

    mul-float/2addr v8, p3

    const/high16 v9, 0x42c80000    # 100.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    sub-float/2addr v8, v9

    float-to-int v3, v8

    .line 202
    .local v3, "moveDistance4Zoom":I
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_1

    .line 203
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->GetPackageOffset(I)F

    move-result v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-nez v8, :cond_0

    .line 255
    :goto_0
    return-void

    .line 206
    :cond_0
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    invoke-static {v8}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->GetPackageOffset(I)F

    move-result v4

    .line 209
    .local v4, "packageOffsetValue":F
    int-to-float v8, v3

    mul-float/2addr v8, v4

    float-to-int v3, v8

    .line 213
    invoke-static {p2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->GetPackageExpandOffset(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;I)I

    move-result v3

    .line 217
    .end local v4    # "packageOffsetValue":F
    :cond_1
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    mul-int/lit8 v9, v0, 0x2

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x2

    if-le v3, v8, :cond_2

    .line 218
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    sub-int/2addr v8, v9

    mul-int/lit8 v9, v0, 0x2

    sub-int/2addr v8, v9

    add-int/lit8 v3, v8, -0x2

    .line 223
    :cond_2
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v9, 0xf

    if-eq v8, v9, :cond_3

    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/16 v9, 0x13

    if-ne v8, v9, :cond_4

    :cond_3
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_4

    .line 225
    int-to-double v8, v3

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v10

    double-to-int v3, v8

    .line 228
    :cond_4
    const/high16 v8, 0x42940000    # 74.0f

    cmpl-float v8, p3, v8

    if-lez v8, :cond_5

    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->packageOffsetIndex:I

    const/4 v9, 0x7

    if-ne v8, v9, :cond_5

    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    if-ne p2, v8, :cond_5

    .line 229
    int-to-double v8, v3

    const-wide v10, 0x3fdd70a3d70a3d71L    # 0.46

    mul-double/2addr v8, v10

    double-to-int v3, v8

    .line 232
    :cond_5
    iget v8, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int/lit8 v2, v8, -0x1

    .line 233
    .local v2, "leftX2":I
    iget v8, p0, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    add-int/lit8 v6, v8, 0x1

    .line 235
    .local v6, "rightX1":I
    const-string v8, "ZoomManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  leftX2 : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "  rightX1  : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    div-int/lit8 v8, v3, 0x2

    sub-int v1, v2, v8

    .line 238
    .local v1, "leftX1":I
    div-int/lit8 v8, v3, 0x2

    add-int v7, v6, v8

    .line 240
    .local v7, "rightX2":I
    if-ge v1, v0, :cond_6

    .line 241
    sub-int v8, v1, v0

    add-int/lit8 v8, v8, 0x1

    sub-int/2addr v7, v8

    .line 242
    add-int/lit8 v1, v0, -0x1

    .line 245
    :cond_6
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    sub-int/2addr v8, v0

    if-le v7, v8, :cond_7

    .line 246
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    sub-int/2addr v8, v0

    sub-int v8, v7, v8

    add-int/lit8 v8, v8, -0x1

    sub-int/2addr v1, v8

    .line 247
    sget v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mDisWidth:I

    sub-int/2addr v8, v0

    add-int/lit8 v7, v8, 0x1

    .line 250
    :cond_7
    sget-object v8, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    if-ne p2, v8, :cond_8

    .line 251
    iget v8, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    invoke-static {v8, v2, v1, v6, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->GenerateTochEvent(IIIII)V

    goto/16 :goto_0

    .line 253
    :cond_8
    iget v8, p0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    invoke-static {v8, v1, v2, v7, v6}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->GenerateTochEvent(IIIII)V

    goto/16 :goto_0
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    sput-object p0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;

    .line 153
    return-void
.end method
