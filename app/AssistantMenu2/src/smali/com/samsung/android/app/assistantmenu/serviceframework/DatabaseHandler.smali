.class public Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "DatabaseHandler.java"


# static fields
.field private static final ACTNAME:Ljava/lang/String; = "actname"

.field private static final AMACTION:Ljava/lang/String; = "amintent"

.field private static final AMICON:Ljava/lang/String; = "amicon"

.field private static final AMID:Ljava/lang/String; = "id"

.field private static final AMLABEL:Ljava/lang/String; = "amlabel"

.field private static final AMSTATE:Ljava/lang/String; = "amstate"

.field private static final AMTITLE:Ljava/lang/String; = "amtitle"

.field private static final APPNAME:Ljava/lang/String; = "appname"

.field private static final ASSISTANT_TABLE:Ljava/lang/String; = "AMData"

.field private static final DATABASE_NAME:Ljava/lang/String; = "AssistantMenu"

.field private static final DATABASE_VERSION:I = 0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const-string v0, "AssistantMenu"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public addAMData(Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;)V
    .locals 6
    .param p1, "amdata"    # Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 65
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 66
    .local v1, "values":Landroid/content/ContentValues;
    if-eqz p1, :cond_0

    .line 67
    const-string v2, "id"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getID()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 68
    const-string v2, "appname"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v2, "actname"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getActName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v2, "amtitle"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v2, "amlabel"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmLblName()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 72
    const-string v2, "amintent"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAmAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v2, "amicon"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getDrawable()[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 74
    const-string v2, "amstate"

    invoke-virtual {p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 77
    const-string v2, "AMData"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 79
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 80
    return-void
.end method

.method public deleteAMData(Ljava/lang/String;)V
    .locals 6
    .param p1, "packName"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 161
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AMData"

    const-string v2, "appname=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 163
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 164
    return-void
.end method

.method public deleteAllAMdata()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 169
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AMData"

    invoke-virtual {v0, v1, v2, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 170
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 171
    return-void
.end method

.method public getAMData(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "actName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 86
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v12, "amdataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;>;"
    const/4 v13, 0x0

    .line 88
    .local v13, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 89
    .local v10, "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    if-eqz p1, :cond_2

    .line 91
    :try_start_0
    const-string v1, "AMData"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "appname"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "actname"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "amtitle"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "amlabel"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "amintent"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "amicon"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "amstate"

    aput-object v4, v2, v3

    const-string v3, "actname=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 94
    if-eqz v13, :cond_1

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v11, v10

    .line 96
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .local v11, "amdata":Ljava/lang/Object;
    :try_start_1
    new-instance v10, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-direct {v10}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 97
    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    const/4 v1, 0x0

    :try_start_2
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .end local v11    # "amdata":Ljava/lang/Object;
    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v10, v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setID(J)V

    .line 98
    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAppName(Ljava/lang/String;)V

    .line 99
    const/4 v1, 0x2

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setActName(Ljava/lang/String;)V

    .line 100
    const/4 v1, 0x3

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setTitleName(Ljava/lang/String;)V

    .line 101
    const/4 v1, 0x4

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v10, v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmLblName(J)V

    .line 102
    const/4 v1, 0x5

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmAction(Ljava/lang/String;)V

    .line 103
    const/4 v1, 0x6

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setDrawable([B)V

    .line 104
    const/4 v1, 0x7

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v10, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setState(I)V

    .line 105
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const/4 v10, 0x0

    .line 107
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 112
    :cond_1
    if-eqz v13, :cond_2

    .line 113
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_2
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 118
    return-object v12

    .line 109
    :catch_0
    move-exception v9

    .line 110
    .local v9, "Ex":Ljava/lang/NumberFormatException;
    :goto_1
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 112
    if-eqz v13, :cond_2

    .line 113
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 112
    .end local v9    # "Ex":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v1

    :goto_2
    if-eqz v13, :cond_3

    .line 113
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1

    .line 112
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v11    # "amdata":Ljava/lang/Object;
    :catchall_1
    move-exception v1

    move-object v10, v11

    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_2

    .line 109
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :catch_1
    move-exception v9

    move-object v10, v11

    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_1
.end method

.method public getAllAMData()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v3, "amdataList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;>;"
    const-string v6, "SELECT  * FROM AMData ORDER BY id"

    .line 127
    .local v6, "selectQuery":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 128
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 129
    .local v4, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 132
    .local v1, "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    if-eqz v4, :cond_1

    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move-object v2, v1

    .line 134
    .end local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .local v2, "amdata":Ljava/lang/Object;
    :try_start_1
    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 135
    .restart local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    const/4 v7, 0x0

    :try_start_2
    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .end local v2    # "amdata":Ljava/lang/Object;
    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setID(J)V

    .line 136
    const/4 v7, 0x1

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAppName(Ljava/lang/String;)V

    .line 137
    const/4 v7, 0x2

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setActName(Ljava/lang/String;)V

    .line 138
    const/4 v7, 0x3

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setTitleName(Ljava/lang/String;)V

    .line 139
    const/4 v7, 0x4

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v1, v8, v9}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmLblName(J)V

    .line 140
    const/4 v7, 0x5

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmAction(Ljava/lang/String;)V

    .line 141
    const/4 v7, 0x6

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setDrawable([B)V

    .line 142
    const/4 v7, 0x7

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v7}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setState(I)V

    .line 143
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    const/4 v1, 0x0

    .line 145
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v7

    if-nez v7, :cond_0

    .line 150
    :cond_1
    if-eqz v4, :cond_2

    .line 151
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 154
    :cond_2
    :goto_0
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 155
    return-object v3

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "Ex":Ljava/lang/NumberFormatException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    if-eqz v4, :cond_2

    .line 151
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 150
    .end local v0    # "Ex":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v4, :cond_3

    .line 151
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v7

    .line 150
    .end local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v2    # "amdata":Ljava/lang/Object;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .restart local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_2

    .line 147
    .end local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :catch_1
    move-exception v0

    move-object v1, v2

    .restart local v1    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_1
.end method

.method public getAmDataCount()I
    .locals 5

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 176
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v0, "SELECT  * FROM AMData"

    .line 178
    .local v0, "countQuery":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 179
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 180
    .local v3, "tt":I
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 181
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 183
    return v3
.end method

.method public getPackAMData(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "actName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v5, 0x0

    .line 242
    const/4 v9, 0x0

    .line 243
    .local v9, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 244
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v10, 0x0

    .line 245
    .local v10, "packData":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 246
    const-string v1, "AMData"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v7

    const-string v3, "appname"

    aput-object v3, v2, v11

    const/4 v3, 0x2

    const-string v4, "actname"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "amtitle"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "amlabel"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "amintent"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "amicon"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "amstate"

    aput-object v4, v2, v3

    const-string v3, "actname=?"

    new-array v4, v11, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 250
    if-eqz v9, :cond_1

    .line 251
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 254
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 255
    const/4 v9, 0x0

    .line 258
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 259
    return-object v10
.end method

.method public getSingleAMData(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .locals 13
    .param p1, "actName"    # Ljava/lang/String;
    .param p2, "titleName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 209
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AMData"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v7

    const-string v3, "appname"

    aput-object v3, v2, v8

    const-string v3, "actname"

    aput-object v3, v2, v6

    const-string v3, "amtitle"

    aput-object v3, v2, v4

    const/4 v3, 0x4

    const-string v4, "amlabel"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "amintent"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "amicon"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "amstate"

    aput-object v4, v2, v3

    const-string v3, "amtitle=? AND actname=?"

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v7

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v8

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 216
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 219
    .local v10, "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    if-eqz v12, :cond_0

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    new-instance v11, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    invoke-direct {v11}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;-><init>()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .local v11, "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    const/4 v1, 0x0

    :try_start_1
    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setID(J)V

    .line 222
    const/4 v1, 0x1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAppName(Ljava/lang/String;)V

    .line 223
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setActName(Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setTitleName(Ljava/lang/String;)V

    .line 225
    const/4 v1, 0x4

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v11, v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmLblName(J)V

    .line 226
    const/4 v1, 0x5

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setAmAction(Ljava/lang/String;)V

    .line 227
    const/4 v1, 0x6

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setDrawable([B)V

    .line 228
    const/4 v1, 0x7

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v11, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->setState(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v10, v11

    .line 233
    .end local v11    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :cond_0
    if-eqz v12, :cond_1

    .line 234
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 237
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 238
    return-object v10

    .line 230
    :catch_0
    move-exception v9

    .line 231
    .local v9, "Ex":Ljava/lang/NumberFormatException;
    :goto_1
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 233
    if-eqz v12, :cond_1

    .line 234
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 233
    .end local v9    # "Ex":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v1

    :goto_2
    if-eqz v12, :cond_2

    .line 234
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    .line 233
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v11    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :catchall_1
    move-exception v1

    move-object v10, v11

    .end local v11    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_2

    .line 230
    .end local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v11    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    :catch_1
    move-exception v9

    move-object v10, v11

    .end local v11    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    .restart local v10    # "amdata":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    goto :goto_1
.end method

.method public getTopClassName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "actName"    # Ljava/lang/String;
    .param p2, "intentString"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v11, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 188
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "AMData"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "id"

    aput-object v3, v2, v11

    const-string v3, "appname"

    aput-object v3, v2, v10

    const-string v3, "actname"

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const-string v4, "amtitle"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "amlabel"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "amintent"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "amicon"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "amstate"

    aput-object v4, v2, v3

    const-string v3, "amintent=? AND actname=?"

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v11

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v10

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 191
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 192
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 193
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 194
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 195
    const/4 v9, 0x0

    move v1, v10

    .line 204
    :goto_0
    return v1

    .line 198
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 199
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 200
    const/4 v9, 0x0

    move v1, v11

    .line 201
    goto :goto_0

    :cond_1
    move v1, v11

    .line 204
    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 41
    const-string v0, "CREATE TABLE AMData(id INTEGER PRIMARY KEY,appname TEXT,actname TEXT,amtitle TEXT,amlabel INTEGER,amintent TEXT,amicon BLOB,amstate INTEGER )"

    .line 44
    .local v0, "CREATE_AM_TABLE":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 51
    const-string v0, "DROP TABLE IF EXISTS AMData"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 55
    return-void
.end method
