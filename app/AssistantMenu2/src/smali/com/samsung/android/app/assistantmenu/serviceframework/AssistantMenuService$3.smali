.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;
.super Landroid/database/ContentObserver;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 407
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 411
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "car_mode_on"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 412
    const-string v1, "AssistantMenuService"

    const-string v2, "stop AssistantMenuService by Carmode"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$3;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mStartId:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1800(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->stopSelf(I)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
