.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;
.super Landroid/os/Handler;
.source "BrightnessControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 138
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 153
    :goto_0
    return-void

    .line 140
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 145
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->UpdateUI()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 146
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)V

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mScreenBrightnessMinimum:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$300(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
