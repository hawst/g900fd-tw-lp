.class Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;
.super Landroid/os/Handler;
.source "AssistantMenuGridUIMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 174
    const-string v3, "AssistantMenuGridUIMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[c] handleMessage: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 179
    :pswitch_0
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 180
    .local v0, "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    const/4 v1, 0x0

    .line 182
    .local v1, "actlong":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_3

    .line 183
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    .line 198
    :goto_1
    if-eqz v1, :cond_0

    .line 199
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLonglockKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongPowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v1, v3, :cond_2

    .line 201
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/media/AudioManager;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 203
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto :goto_0

    .line 184
    :cond_3
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_4

    .line 185
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto :goto_1

    .line 186
    :cond_4
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_5

    .line 187
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongRecentAppsKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto :goto_1

    .line 188
    :cond_5
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_6

    .line 189
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLonglockKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto :goto_1

    .line 190
    :cond_6
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_7

    .line 191
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongPowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto :goto_1

    .line 192
    :cond_7
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v0, v3, :cond_8

    .line 193
    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressLongMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    goto :goto_1

    .line 195
    :cond_8
    const-string v3, "AssistantMenuGridUIMenu"

    const-string v4, "[c] Message EVENT_MENU_LONGPRESS: not longpress item!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 208
    .end local v0    # "act":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    .end local v1    # "actlong":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    .line 209
    .local v2, "curr":I
    if-nez v2, :cond_9

    .line 210
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 216
    :goto_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mIsViewPageLooping:Z
    invoke-static {v3, v6}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Z)Z

    goto/16 :goto_0

    .line 211
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_a

    .line 212
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$1;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mViewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/support/v4/view/ViewPager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v6}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_2

    .line 214
    :cond_a
    const-string v3, "AssistantMenuGridUIMenu"

    const-string v4, "[c] onPageScrollStateChanged - no viewpager changed!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 176
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
