.class Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;
.super Landroid/content/BroadcastReceiver;
.source "VolumeControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 768
    if-eqz p2, :cond_1

    .line 769
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 770
    .local v0, "action":Ljava/lang/String;
    const-string v1, "VolumeControlGridUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] onReceive, action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 773
    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_RINGER_MODE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 774
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 775
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mPrevVolumeState:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$902(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 776
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.media.EXTRA_RINGER_MODE"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 787
    .end local v0    # "action":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->UpdateUI()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V

    .line 788
    return-void

    .line 778
    .restart local v0    # "action":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_VOLUME_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 779
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    goto :goto_0

    .line 782
    :cond_3
    const-string v1, "VolumeControlGridUI"

    const-string v2, "onReceive not command."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
