.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;
.super Ljava/lang/Object;
.source "FingerMouseGridUI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

.field final synthetic val$eventAction:I

.field final synthetic val$isDouble:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;IZ)V
    .locals 0

    .prologue
    .line 872
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iput p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    iput-boolean p3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$isDouble:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 44

    .prologue
    .line 876
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 877
    .local v2, "downTime":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 878
    .local v4, "eventTime":J
    sget v14, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->touchDeviceID:I

    .line 879
    .local v14, "deviceId":I
    const/4 v15, 0x0

    .line 880
    .local v15, "edgeFlag":I
    const/4 v10, 0x0

    .line 881
    .local v10, "metaState":I
    const/high16 v17, -0x80000000

    .line 882
    .local v17, "AMOTION_EVENT_FLAG_WINDOW_IS_ACCESSIBILITY":I
    const/high16 v12, 0x3f800000    # 1.0f

    .line 883
    .local v12, "xPrecision":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 884
    .local v13, "yPrecision":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v42

    .line 885
    .local v42, "x":F
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v6}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v43

    .line 886
    .local v43, "y":F
    new-instance v38, Landroid/app/Instrumentation;

    invoke-direct/range {v38 .. v38}, Landroid/app/Instrumentation;-><init>()V

    .line 888
    .local v38, "inst":Landroid/app/Instrumentation;
    const/4 v6, 0x2

    new-array v8, v6, [Landroid/view/MotionEvent$PointerProperties;

    .line 889
    .local v8, "properties":[Landroid/view/MotionEvent$PointerProperties;
    new-instance v41, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct/range {v41 .. v41}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    .line 890
    .local v41, "pp1":Landroid/view/MotionEvent$PointerProperties;
    const/4 v6, 0x0

    move-object/from16 v0, v41

    iput v6, v0, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 891
    const/4 v6, 0x1

    move-object/from16 v0, v41

    iput v6, v0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 893
    const/4 v6, 0x0

    aput-object v41, v8, v6

    .line 895
    const/4 v6, 0x1

    new-array v9, v6, [Landroid/view/MotionEvent$PointerCoords;

    .line 896
    .local v9, "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    new-instance v23, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v23 .. v23}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 897
    .local v23, "pc1":Landroid/view/MotionEvent$PointerCoords;
    const/16 v40, 0x0

    .line 898
    .local v40, "pc2":Landroid/view/MotionEvent$PointerCoords;
    move/from16 v0, v42

    move-object/from16 v1, v23

    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 899
    move/from16 v0, v43

    move-object/from16 v1, v23

    iput v0, v1, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 900
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, v23

    iput v6, v0, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 901
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v0, v23

    iput v6, v0, Landroid/view/MotionEvent$PointerCoords;->size:F

    .line 902
    const/4 v6, 0x0

    aput-object v23, v9, v6

    .line 904
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    if-lez v6, :cond_0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    const/4 v7, 0x4

    if-gt v6, v7, :cond_0

    .line 905
    const-string v6, "FingerMouseGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent SWIPERIGHT1 :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    new-instance v40, Landroid/view/MotionEvent$PointerCoords;

    .end local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    invoke-direct/range {v40 .. v40}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 907
    .restart local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    move-object/from16 v0, v23

    invoke-virtual {v6, v0, v7}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->getPointerCoordinates(Landroid/view/MotionEvent$PointerCoords;I)Landroid/view/MotionEvent$PointerCoords;

    move-result-object v40

    .line 908
    const/4 v6, 0x0

    aput-object v40, v9, v6

    .line 911
    :cond_0
    const-string v6, "FingerMouseGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent ACTION_DOWN :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 919
    .local v39, "mMotionEvent":Landroid/view/MotionEvent;
    invoke-virtual/range {v38 .. v39}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 921
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    if-lez v6, :cond_1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    const/4 v7, 0x4

    if-gt v6, v7, :cond_1

    .line 922
    const-string v6, "FingerMouseGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent SWIPERIGHT2 :: x = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->x:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v11, ", y = "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v11, 0x0

    aget-object v11, v9, v11

    iget v11, v11, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$eventAction:I

    move/from16 v30, v0

    move-wide/from16 v20, v2

    move-object/from16 v22, v8

    move/from16 v24, v10

    move/from16 v25, v12

    move/from16 v26, v13

    move/from16 v27, v14

    move/from16 v28, v15

    move/from16 v29, v17

    invoke-virtual/range {v19 .. v30}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectMotionEvent(J[Landroid/view/MotionEvent$PointerProperties;Landroid/view/MotionEvent$PointerCoords;IFFIIII)V

    .line 929
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 931
    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v11, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v39

    .line 937
    invoke-virtual/range {v38 .. v39}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    .line 939
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$isDouble:Z

    if-eqz v6, :cond_2

    .line 940
    const-string v6, "FingerMouseGridUI"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "injectAccessibilityMotionEvent isDouble : "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->val$isDouble:Z

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-object/from16 v25, v0

    move-wide/from16 v26, v2

    move-wide/from16 v28, v4

    move-object/from16 v30, v8

    move-object/from16 v31, v9

    move/from16 v32, v10

    move/from16 v33, v12

    move/from16 v34, v13

    move/from16 v35, v14

    move/from16 v36, v15

    move/from16 v37, v17

    invoke-virtual/range {v25 .. v37}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->InjectDoubleTapEvent(JJ[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IFFIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 949
    .end local v2    # "downTime":J
    .end local v4    # "eventTime":J
    .end local v8    # "properties":[Landroid/view/MotionEvent$PointerProperties;
    .end local v9    # "pointerCoords":[Landroid/view/MotionEvent$PointerCoords;
    .end local v10    # "metaState":I
    .end local v12    # "xPrecision":F
    .end local v13    # "yPrecision":F
    .end local v14    # "deviceId":I
    .end local v15    # "edgeFlag":I
    .end local v17    # "AMOTION_EVENT_FLAG_WINDOW_IS_ACCESSIBILITY":I
    .end local v23    # "pc1":Landroid/view/MotionEvent$PointerCoords;
    .end local v38    # "inst":Landroid/app/Instrumentation;
    .end local v39    # "mMotionEvent":Landroid/view/MotionEvent;
    .end local v40    # "pc2":Landroid/view/MotionEvent$PointerCoords;
    .end local v41    # "pp1":Landroid/view/MotionEvent$PointerProperties;
    .end local v42    # "x":F
    .end local v43    # "y":F
    :cond_2
    :goto_0
    return-void

    .line 946
    :catch_0
    move-exception v18

    .line 947
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
