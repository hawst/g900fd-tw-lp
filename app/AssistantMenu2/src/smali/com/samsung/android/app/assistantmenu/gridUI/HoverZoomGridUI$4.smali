.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;
.super Ljava/lang/Object;
.source "HoverZoomGridUI.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->SetEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 324
    const-string v3, "HoverZoomGridUI"

    const-string v4, "onTouch pad!!!~~~~~~"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 326
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 352
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mTouchGestureDetector:Landroid/view/GestureDetector;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/view/GestureDetector;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    return v3

    .line 328
    :pswitch_0
    const-string v3, "HoverZoomGridUI"

    const-string v4, "onTouch in double tab section move - "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1200(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 330
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v4

    sub-float v1, v3, v4

    .line 331
    .local v1, "distanceX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v4

    sub-float v2, v3, v4

    .line 332
    .local v2, "distanceY":F
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 333
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v3, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 334
    const-string v3, "HoverZoomGridUI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACTION_MOVE  move coordinates x:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->updateCursorFromGesture(FF)V

    goto :goto_0

    .line 339
    .end local v1    # "distanceX":F
    .end local v2    # "distanceY":F
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 340
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V
    invoke-static {v3, v5, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;IZ)V

    .line 341
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1602(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 343
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1300(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v3, v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 345
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v3, v6}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 346
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z
    invoke-static {v3, v5}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    goto/16 :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
