.class public Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;
.super Ljava/lang/Object;
.source "ZoomManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PackageOffset"
.end annotation


# static fields
.field private static mPackageName:[Ljava/lang/String;

.field private static mPackageOffset:[F

.field private static mPlusOffset:[F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x15

    .line 46
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.sec.pcw"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.sec.android.app.sbrowser"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.sec.android.app.camera"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.android.chrome"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.dropbox.android"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "flipboard.app"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.google.android.apps.books"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.sec.android.app.shealth"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.sec.android.widgetapp.diotek.smemo"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.calendar"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.samsung.android.app.episodes"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.sec.android.app.translator"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.infraware.polarisviewer5"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.sec.android.app.videoplayer"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.infraware.polarisoffice5"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.sec.android.app.easylauncher"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.samsung.android.app.assistantmenu"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.google.android.apps.plus"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.android.email"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPackageName:[Ljava/lang/String;

    .line 70
    new-array v0, v3, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPackageOffset:[F

    .line 94
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F

    return-void

    .line 70
    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3f800000    # 1.0f
        0x3ecccccd    # 0.4f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3fa66666    # 1.3f
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
        0x0
        0x0
        0x3f4ccccd    # 0.8f
        0x0
    .end array-data

    .line 94
    :array_1
    .array-data 4
        0x3f733333    # 0.95f
        0x3f666666    # 0.9f
        0x3f99999a    # 1.2f
        0x3f666666    # 0.9f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
        0x3fa66666    # 1.3f
        0x3fab851f    # 1.34f
        0x3f99999a    # 1.2f
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
        0x3f6147ae    # 0.88f
        0x3ecccccd    # 0.4f
        0x3f8ccccd    # 1.1f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f666666    # 0.9f
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetMoveDistance(I)F
    .locals 1
    .param p0, "packageOffset"    # I

    .prologue
    .line 142
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F

    aget v0, v0, p0

    return v0
.end method

.method public static GetOffsetOfPackageIndex()I
    .locals 7

    .prologue
    .line 119
    const/4 v4, 0x0

    .line 121
    .local v4, "packageName":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->access$000()Landroid/content/Context;

    move-result-object v5

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 122
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v5, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 123
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPackageName:[Ljava/lang/String;

    array-length v3, v5

    .line 125
    .local v3, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 126
    sget-object v5, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPackageName:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 134
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v2    # "i":I
    .end local v3    # "length":I
    :goto_1
    return v2

    .line 125
    .restart local v0    # "am":Landroid/app/ActivityManager;
    .restart local v2    # "i":I
    .restart local v3    # "length":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 130
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v2    # "i":I
    .end local v3    # "length":I
    :catch_0
    move-exception v1

    .line 131
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 134
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public static GetPackageOffset(I)F
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 138
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPackageOffset:[F

    aget v0, v0, p0

    return v0
.end method

.method static synthetic access$100()[F
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$PackageOffset;->mPlusOffset:[F

    return-object v0
.end method
