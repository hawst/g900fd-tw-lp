.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 742
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 840
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 841
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 842
    return v2

    .line 745
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-nez v0, :cond_1

    .line 750
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z

    .line 751
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 752
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 758
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$902(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z

    .line 760
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 792
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 795
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 796
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->sUseAutoBrightnessDetail:Z

    if-nez v0, :cond_2

    .line 797
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mBrightnessAutoEnable:Z
    invoke-static {v0, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z

    .line 798
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCBAutoBrightness:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 799
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessMode()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 805
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$902(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)Z

    .line 807
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$3;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 742
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d001a
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
