.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;
.super Ljava/lang/Object;
.source "HoverZoomGridUI.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V
    .locals 0

    .prologue
    .line 746
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;

    .prologue
    .line 749
    const-string v0, "HoverZoomGridUI"

    const-string v1, "[c] onDismiss()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mHoverZoomEnabled:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 751
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 752
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$8;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->setFMClosedStatus(Z)V

    .line 754
    :cond_0
    return-void
.end method
