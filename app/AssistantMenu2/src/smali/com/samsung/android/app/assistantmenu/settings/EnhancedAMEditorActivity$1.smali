.class Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;
.super Landroid/os/Handler;
.source "EnhancedAMEditorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 73
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # setter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->mLoadComplete:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$002(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;Z)Z

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$100(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 76
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] Oncreate :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "aplication status"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$300(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$300(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$400(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkCount:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$200(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->appsWithEAM:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$300(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity$1;->this$0:Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->checkboxPref:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;->access$400(Lcom/samsung/android/app/assistantmenu/settings/EnhancedAMEditorActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method
