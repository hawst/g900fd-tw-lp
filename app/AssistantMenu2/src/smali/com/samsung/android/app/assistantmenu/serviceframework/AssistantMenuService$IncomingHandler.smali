.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;
.super Landroid/os/Handler;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IncomingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0

    .prologue
    .line 2379
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    .line 2381
    const-string v2, "AssistantMenuService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] handleMessage: Incoming Handler"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2383
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 2410
    :cond_0
    :goto_0
    return-void

    .line 2385
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 2387
    .local v1, "msgFromApp":Landroid/os/Bundle;
    if-eqz v1, :cond_1

    .line 2388
    const-string v2, "AssistantMenuService"

    const-string v3, "onHandleMessage getData via bundle received"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 2389
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "assistant_menu_eam_enable"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2391
    .local v0, "mEAMprov":I
    const-string v2, "AssistantMenuService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EAM enabled "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 2393
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->updateEAM(Landroid/os/Bundle;)V

    .line 2402
    .end local v0    # "mEAMprov":I
    :cond_1
    :goto_1
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$800()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2403
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$IncomingHandler;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;I)V

    goto :goto_0

    .line 2396
    .restart local v0    # "mEAMprov":I
    :cond_2
    const-string v2, ""

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->recentActivity:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3202(Ljava/lang/String;)Ljava/lang/String;

    .line 2397
    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->iconCount:I
    invoke-static {v5}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3302(I)I

    .line 2398
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->fragOptions:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3400()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2399
    const-string v2, ""

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->actOptions:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3502(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 2383
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
