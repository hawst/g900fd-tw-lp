.class public final Lcom/samsung/android/app/assistantmenu/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_height:I = 0x7f090000

.field public static final action_bar_switch_padding:I = 0x7f090001

.field public static final airview_magnifier_height_large:I = 0x7f090002

.field public static final airview_magnifier_height_medium:I = 0x7f090003

.field public static final airview_magnifier_height_small:I = 0x7f090004

.field public static final airview_magnifier_width_large:I = 0x7f090005

.field public static final airview_magnifier_width_medium:I = 0x7f090006

.field public static final airview_magnifier_width_small:I = 0x7f090007

.field public static final brightnessmenu_bottom_margin:I = 0x7f090008

.field public static final brightnessmenu_custormseekbar_heigh:I = 0x7f090009

.field public static final brightnessmenu_custormseekbar_margin:I = 0x7f09000a

.field public static final brightnessmenu_custormseekbar_padding_left:I = 0x7f09000b

.field public static final brightnessmenu_custormseekbar_padding_right:I = 0x7f09000c

.field public static final brightnessmenu_height:I = 0x7f09000d

.field public static final brightnessmenu_left_margin:I = 0x7f09000e

.field public static final brightnessmenu_right_margin:I = 0x7f09000f

.field public static final brightnessmenu_width:I = 0x7f090010

.field public static final cocktailbar_panel_margin:I = 0x7f090011

.field public static final cornertab_height:I = 0x7f090012

.field public static final cornertab_keyboard_ani_minus_gain:I = 0x7f090013

.field public static final cornertab_keyboard_ani_plus_gain:I = 0x7f090014

.field public static final cornertab_width:I = 0x7f090015

.field public static final cursor_bottom_margin:I = 0x7f090016

.field public static final cursor_initial_pos_deltaX:I = 0x7f090017

.field public static final cursor_mouse_pointer_l_width:I = 0x7f090018

.field public static final cursor_mouse_pointer_s_width:I = 0x7f090019

.field public static final cursor_statusBar:I = 0x7f09001a

.field public static final cursor_statusbar_bottom_margin:I = 0x7f09001b

.field public static final cursor_xpos_max:I = 0x7f09001c

.field public static final cursor_ypos_max:I = 0x7f09001d

.field public static final custormseekbar_progress_height:I = 0x7f09001e

.field public static final gridmenu_bottom_margin:I = 0x7f09001f

.field public static final gridmenu_button_gain:I = 0x7f090020

.field public static final gridmenu_height:I = 0x7f090021

.field public static final gridmenu_height_big:I = 0x7f090022

.field public static final gridmenu_height_med:I = 0x7f090023

.field public static final gridmenu_height_small:I = 0x7f090024

.field public static final gridmenu_left_margin:I = 0x7f090025

.field public static final gridmenu_next_button_area_down:I = 0x7f090026

.field public static final gridmenu_next_button_area_right:I = 0x7f090027

.field public static final gridmenu_next_button_area_up:I = 0x7f090028

.field public static final gridmenu_next_page_distance_gain:I = 0x7f090029

.field public static final gridmenu_next_page_sound_gain:I = 0x7f09002a

.field public static final gridmenu_page_left_margin:I = 0x7f09002b

.field public static final gridmenu_page_top_margin:I = 0x7f09002c

.field public static final gridmenu_prev_button_area_down:I = 0x7f09002d

.field public static final gridmenu_prev_button_area_right:I = 0x7f09002e

.field public static final gridmenu_prev_button_area_up:I = 0x7f09002f

.field public static final gridmenu_prev_page_distance_gain:I = 0x7f090030

.field public static final gridmenu_prev_page_sound_gain:I = 0x7f090031

.field public static final gridmenu_right_margin:I = 0x7f090032

.field public static final gridmenu_singleline_bottom_padding:I = 0x7f090033

.field public static final gridmenu_singleline_left_padding:I = 0x7f090034

.field public static final gridmenu_singleline_right_padding:I = 0x7f090035

.field public static final gridmenu_singleline_width_length:I = 0x7f090036

.field public static final gridmenu_twoline_fontsize:I = 0x7f090037

.field public static final gridmenu_width:I = 0x7f090038

.field public static final gridmenu_width_big:I = 0x7f090039

.field public static final gridmenu_width_med:I = 0x7f09003a

.field public static final gridmenu_width_small:I = 0x7f09003b

.field public static final gridview_setting_actionbar_land:I = 0x7f09003c

.field public static final gridview_setting_actionbar_port:I = 0x7f09003d

.field public static final gridview_setting_cotrolbar:I = 0x7f09003e

.field public static final gridview_setting_top_margin_land:I = 0x7f09003f

.field public static final gridview_setting_top_margin_port:I = 0x7f090040

.field public static final gridview_twoline_fontsize:I = 0x7f090041

.field public static final gridview_upper_1line_height_land:I = 0x7f090042

.field public static final gridview_upper_1line_height_port:I = 0x7f090043

.field public static final gridview_upper_2line_height_land:I = 0x7f090044

.field public static final gridview_upper_2line_height_port:I = 0x7f090045

.field public static final gridview_upper_3line_height_land:I = 0x7f090046

.field public static final gridview_upper_3line_height_port:I = 0x7f090047

.field public static final gridview_upper_4line_height_port:I = 0x7f090048

.field public static final gridview_upper_5line_height_port:I = 0x7f090049

.field public static final gridview_upper_item_ani_x_land:I = 0x7f09004a

.field public static final gridview_upper_item_ani_x_port:I = 0x7f09004b

.field public static final gridview_upper_item_ani_y_land:I = 0x7f09004c

.field public static final gridview_upper_item_ani_y_port:I = 0x7f09004d

.field public static final keyboard_landscape_height:I = 0x7f09004e

.field public static final keyboard_portrait_height:I = 0x7f09004f

.field public static final lock_screen_shortcut_app_icon_size:I = 0x7f090050

.field public static final padding_large:I = 0x7f090051

.field public static final padding_medium:I = 0x7f090052

.field public static final padding_small:I = 0x7f090053

.field public static final rotatemenu_bottom_margin:I = 0x7f090054

.field public static final rotatemenu_height:I = 0x7f090055

.field public static final rotatemenu_left_margin:I = 0x7f090056

.field public static final rotatemenu_right_margin:I = 0x7f090057

.field public static final rotatemenu_width:I = 0x7f090058

.field public static final screen_height:I = 0x7f090059

.field public static final screen_width:I = 0x7f09005a

.field public static final setting_action_bar_height:I = 0x7f09005b

.field public static final status_bar_height:I = 0x7f09005c

.field public static final volume_stream_music_height:I = 0x7f09005d

.field public static final volume_stream_ring_height:I = 0x7f09005e

.field public static final volumemenu_bottom_margin:I = 0x7f09005f

.field public static final volumemenu_button_width:I = 0x7f090060

.field public static final volumemenu_left_margin:I = 0x7f090061

.field public static final volumemenu_right_margin:I = 0x7f090062

.field public static final volumemenu_seekbar_bg_side_margin:I = 0x7f090063

.field public static final volumemenu_seekbar_side_margin:I = 0x7f090064

.field public static final volumemenu_seekbar_width:I = 0x7f090065

.field public static final volumemenu_width:I = 0x7f090066

.field public static final zoommenu_center_poin_rtl:I = 0x7f090067

.field public static final zoommenu_center_point:I = 0x7f090068

.field public static final zoommenu_comback_zoompointer_logic_margin:I = 0x7f090069

.field public static final zoommenu_control_view_limit_y_land:I = 0x7f09006a

.field public static final zoommenu_control_view_limit_y_port:I = 0x7f09006b

.field public static final zoommenu_display_boundary:I = 0x7f09006c

.field public static final zoommenu_fake_zoom_boundary:I = 0x7f09006d

.field public static final zoommenu_left_margin:I = 0x7f09006e

.field public static final zoommenu_left_point:I = 0x7f09006f

.field public static final zoommenu_pointer_landscape_y:I = 0x7f090070

.field public static final zoommenu_pointer_margin_y_to_map:I = 0x7f090071

.field public static final zoommenu_right_point:I = 0x7f090072

.field public static final zoommenu_right_point_boundary:I = 0x7f090073

.field public static final zoommenu_size_zoompointer:I = 0x7f090074

.field public static final zoommenu_top_margin:I = 0x7f090075

.field public static final zoommenu_y_point:I = 0x7f090076


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
