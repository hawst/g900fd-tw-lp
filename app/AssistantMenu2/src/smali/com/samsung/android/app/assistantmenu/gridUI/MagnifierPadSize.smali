.class public Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;
.super Ljava/lang/Object;
.source "MagnifierPadSize.java"


# instance fields
.field x:[I

.field y:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    .line 9
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    .line 11
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v3

    .line 12
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v4

    .line 13
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->x:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v5

    .line 15
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v3

    .line 16
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v4

    .line 17
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/MagnifierPadSize;->y:[I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    aput v1, v0, v5

    .line 18
    return-void

    .line 8
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 9
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method
