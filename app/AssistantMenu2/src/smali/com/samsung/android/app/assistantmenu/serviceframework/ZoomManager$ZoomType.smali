.class public final enum Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
.super Ljava/lang/Enum;
.source "ZoomManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ZoomType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

.field public static final enum CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

.field public static final enum EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    const-string v1, "CONTRACT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->EXPAND:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->CONTRACT:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->$VALUES:[Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    invoke-virtual {v0}, [Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    return-object v0
.end method
