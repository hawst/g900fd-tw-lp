.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;
.super Landroid/os/Handler;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 266
    const-string v1, "AssistantMenuService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] handleMessage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v3, v3, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 270
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->updateDescription()V

    .line 274
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 275
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->updateZoomText()V

    goto :goto_0

    .line 281
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ScreenRotateStatusChanged()V

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->GridMenuUI:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_3

    .line 285
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->ScreenRotateStatusChanged()V

    goto :goto_0

    .line 288
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_4

    .line 289
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mBrightnessControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->ScreenRotateStatusChanged()V

    goto :goto_0

    .line 292
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->RotateControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_5

    .line 293
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mRotateStateControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/RotateStateControlGridUI;->ScreenRotateStatusChanged()V

    goto/16 :goto_0

    .line 296
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->VolumeControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_6

    .line 297
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ScreenRotateStatusChanged()V

    goto/16 :goto_0

    .line 300
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->ZoomControl:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_7

    .line 301
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mZoomControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ScreenRotateStatusChanged()V

    goto/16 :goto_0

    .line 304
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->FingerMouse:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_8

    .line 305
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 306
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ScreenRotateStatusChanged()V

    goto/16 :goto_0

    .line 308
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v1, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->HoverZoom:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    if-ne v1, v2, :cond_9

    .line 309
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ScreenRotateStatusChanged()V

    goto/16 :goto_0

    .line 314
    :cond_9
    const-string v1, "AssistantMenuService"

    const-string v2, "[c] handler MSG_ROTATE_STATE_CHANGE not operated"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 319
    :sswitch_2
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->removeMessages(I)V

    .line 320
    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isAutoExit:Z
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$802(Z)Z

    .line 324
    :sswitch_3
    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->removeMessages(I)V

    .line 326
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->RemoveChildScreens()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$900(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    .line 328
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 329
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 331
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    sget-object v2, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;->None:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    iput-object v2, v1, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mViewMode:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$ViewMode;

    goto/16 :goto_0

    .line 336
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isMonkeyRunning()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 337
    const-string v1, "AssistantMenuService"

    const-string v2, "[c] onTouch() - Monkey is Running"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 341
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboard:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 342
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->ShowGridUI()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1100(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    goto/16 :goto_0

    .line 344
    :cond_c
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenIME:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 346
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->Closekeyboard()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1300(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    .line 349
    :cond_d
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mIsOpenClipboard:Z
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1400(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 350
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->DismissClipboard()V

    .line 353
    :cond_e
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 354
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->SetVisibility(I)V

    .line 356
    :cond_f
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->isFMClosedViaKeyboardAct:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v2

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->ExcuteAct(Ljava/lang/Object;)V
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 361
    :sswitch_5
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->ExcuteAct(Ljava/lang/Object;)V
    invoke-static {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 365
    :sswitch_6
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 366
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->RemoveView()V

    .line 367
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ShowView()V

    .line 369
    :cond_10
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 370
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 371
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ShowView()V

    goto/16 :goto_0

    .line 375
    :sswitch_7
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    if-eqz v1, :cond_11

    .line 376
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->updateStatusBarStatus()V

    .line 378
    :cond_11
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 379
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->updateStatusBarStatus()V

    goto/16 :goto_0

    .line 386
    :sswitch_8
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 387
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 388
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # invokes: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->clearChildScreensinLockMode()V
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$1700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V

    goto/16 :goto_0

    .line 268
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_7
        0x6 -> :sswitch_6
        0x7 -> :sswitch_2
        0x64 -> :sswitch_0
        0x65 -> :sswitch_8
    .end sparse-switch
.end method
