.class Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;
.super Landroid/os/Handler;
.source "FucntionEventManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

.field final synthetic val$myConn:Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;Landroid/os/Looper;Landroid/content/ServiceConnection;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->val$myConn:Landroid/content/ServiceConnection;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 165
    const-string v0, "FunctionEventManager"

    const-string v1, "[c] handleMessage()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$000()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 168
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->val$myConn:Landroid/content/ServiceConnection;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$100()Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->val$context:Landroid/content/Context;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$100()Landroid/content/ServiceConnection;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 170
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$102(Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;

    .line 171
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$200()Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

    iget-object v2, v2, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->val$sScreenshotTimeout:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->val$context:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/util/GlobalConstant;->ACTION_CLIPBOARD_COPY:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 176
    :cond_0
    monitor-exit v1

    .line 177
    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
