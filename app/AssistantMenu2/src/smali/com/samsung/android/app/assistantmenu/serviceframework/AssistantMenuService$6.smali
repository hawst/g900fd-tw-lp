.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;
.super Landroid/telephony/PhoneStateListener;
.source "AssistantMenuService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0

    .prologue
    .line 764
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 2
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 766
    packed-switch p1, :pswitch_data_0

    .line 784
    const-string v0, "AssistantMenuService"

    const-string v1, "[c] PhoneStateListener default!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 768
    :pswitch_0
    const-string v0, "AssistantMenuService"

    const-string v1, "[c] CALL_STATE_RINGING"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 772
    :pswitch_1
    const-string v0, "AssistantMenuService"

    const-string v1, "[c] CALL_STATE_IDLE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->RemoveView()V

    .line 775
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$6;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mVolumeControlUI:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$500(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->ShowView()V

    goto :goto_0

    .line 780
    :pswitch_2
    const-string v0, "AssistantMenuService"

    const-string v1, "[c] CALL_STATE_OFFHOOK"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 766
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
