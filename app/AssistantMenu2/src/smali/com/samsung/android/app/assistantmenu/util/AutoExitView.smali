.class public Lcom/samsung/android/app/assistantmenu/util/AutoExitView;
.super Ljava/lang/Object;
.source "AutoExitView.java"


# static fields
.field private static final NO_REPLY_TOUCH_TIME:I = 0x3a98


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static StartAutoExit(Landroid/os/Handler;)V
    .locals 3
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x7

    .line 36
    invoke-virtual {p0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 37
    const-wide/16 v0, 0x3a98

    invoke-virtual {p0, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 39
    return-void
.end method

.method public static StartAutoExit(Landroid/os/Handler;I)V
    .locals 3
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "noReplyTouchTime"    # I

    .prologue
    const/4 v2, 0x7

    .line 24
    invoke-virtual {p0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 25
    int-to-long v0, p1

    invoke-virtual {p0, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 27
    return-void
.end method

.method public static StopAutoExit(Landroid/os/Handler;)V
    .locals 1
    .param p0, "handler"    # Landroid/os/Handler;

    .prologue
    .line 47
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 48
    return-void
.end method
