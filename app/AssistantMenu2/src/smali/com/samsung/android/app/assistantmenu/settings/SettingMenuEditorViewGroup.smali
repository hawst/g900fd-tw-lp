.class public Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;
.super Landroid/widget/LinearLayout;
.source "SettingMenuEditorViewGroup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final HANDLE_ID:I = 0x7f0d007f

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

.field private final GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

.field private handle:Landroid/widget/ImageView;

.field private mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

.field private mDisplayHeight:I

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

    .line 32
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->init(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

    .line 32
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->init(Landroid/content/Context;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_PORTRAIT_COLUMN_ITEM_COUNT:I

    .line 32
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GRIDVIEW_LANDSCAPE_COLUMN_ITEM_COUNT:I

    .line 47
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->init(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method private ChangelayoutByGap(I)V
    .locals 10
    .param p1, "gapY"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v7, 0x7f090043

    const v5, 0x7f090042

    const/4 v6, 0x2

    .line 103
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int v3, v4, p1

    .line 104
    .local v3, "x":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetOrientationPortraite()Z

    move-result v0

    .line 106
    .local v0, "isPortrait":Z
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 162
    :cond_0
    :goto_0
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iput v3, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 165
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetGridViewLowerHeight(I)I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->requestLayout()V

    .line 168
    return-void

    .line 110
    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 113
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto :goto_0

    .line 114
    :cond_2
    if-ltz p1, :cond_c

    .line 115
    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v4, v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mGridViewUpper:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getCount()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetGridViewLine(I)I

    move-result v1

    .line 117
    .local v1, "line":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 118
    const/4 v2, 0x0

    .line 120
    .local v2, "maxHeight":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetOrientationPortraite()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 121
    if-ne v1, v9, :cond_3

    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 154
    :goto_1
    if-le v3, v2, :cond_0

    .line 155
    move v3, v2

    goto :goto_0

    .line 124
    :cond_3
    if-ne v1, v6, :cond_4

    .line 125
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090045

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 127
    :cond_4
    const/4 v4, 0x3

    if-ne v1, v4, :cond_5

    .line 128
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090047

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 130
    :cond_5
    const/4 v4, 0x4

    if-ne v1, v4, :cond_6

    .line 131
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090048

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 133
    :cond_6
    const/4 v4, 0x5

    if-ne v1, v4, :cond_7

    .line 134
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090049

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 137
    :cond_7
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v5, "ChangelayoutByGap else"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 140
    :cond_8
    if-ne v1, v9, :cond_9

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 143
    :cond_9
    if-ne v1, v6, :cond_a

    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090044

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 146
    :cond_a
    const/4 v4, 0x3

    if-ne v1, v4, :cond_b

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090046

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 150
    :cond_b
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v5, "ChangelayoutByLine else"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 158
    .end local v1    # "line":I
    .end local v2    # "maxHeight":I
    :cond_c
    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v5, "ChangelayoutByGap else"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private GetGridViewLine(I)I
    .locals 3
    .param p1, "gridViewItemCount"    # I

    .prologue
    const/16 v2, 0xc

    .line 220
    const/4 v0, -0x1

    .line 222
    .local v0, "rowLine":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetOrientationPortraite()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 223
    if-ltz p1, :cond_0

    .line 224
    const/4 v1, 0x4

    if-gt p1, v1, :cond_1

    .line 225
    const/4 v0, 0x1

    .line 251
    :cond_0
    :goto_0
    return v0

    .line 226
    :cond_1
    const/16 v1, 0x8

    if-gt p1, v1, :cond_2

    .line 227
    const/4 v0, 0x2

    goto :goto_0

    .line 228
    :cond_2
    if-gt p1, v2, :cond_3

    .line 229
    const/4 v0, 0x3

    goto :goto_0

    .line 230
    :cond_3
    const/16 v1, 0x10

    if-gt p1, v1, :cond_4

    .line 231
    const/4 v0, 0x4

    goto :goto_0

    .line 232
    :cond_4
    const/16 v1, 0x14

    if-gt p1, v1, :cond_5

    .line 233
    const/4 v0, 0x5

    goto :goto_0

    .line 235
    :cond_5
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v2, "GetGridViewLine [GetOrientationState is true] else"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239
    :cond_6
    if-ltz p1, :cond_0

    .line 240
    const/4 v1, 0x6

    if-gt p1, v1, :cond_7

    .line 241
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    :cond_7
    if-gt p1, v2, :cond_8

    .line 243
    const/4 v0, 0x2

    goto :goto_0

    .line 244
    :cond_8
    const/16 v1, 0x12

    if-gt p1, v1, :cond_9

    .line 245
    const/4 v0, 0x3

    goto :goto_0

    .line 247
    :cond_9
    sget-object v1, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v2, "GetGridViewLine [GetOrientationState is false] else"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private GetGridViewLowerHeight(I)I
    .locals 3
    .param p1, "gridViewUpperHeight"    # I

    .prologue
    .line 171
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mDisplayHeight:I

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetActionBarHeight()I

    move-result v1

    sub-int/2addr v0, v1

    sub-int/2addr v0, p1

    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetTopMargin()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private GetOrientationPortraite()Z
    .locals 2

    .prologue
    .line 206
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 207
    .local v0, "orientation":I
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 208
    :cond_0
    const/4 v1, 0x1

    .line 211
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private GetTopMargin()I
    .locals 3

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 189
    .local v0, "top_margin":I
    invoke-direct {p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->GetOrientationPortraite()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 197
    :goto_0
    return v0

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    check-cast p1, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .line 52
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mWindowManager:Landroid/view/WindowManager;

    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 54
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 60
    const v1, 0x7f0d007f

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    .line 61
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 64
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 66
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->mDisplayHeight:I

    .line 68
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v2, 0x7f020062

    .line 74
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 98
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 76
    :pswitch_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v1, "mControlbartouchListener_ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    const v1, 0x7f020063

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 81
    :pswitch_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v1, "mControlbartouchListener_ACTION_UP"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 86
    :pswitch_2
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    const-string v1, "mControlbartouchListener_ACTION_CANCEL"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->handle:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 91
    :pswitch_3
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mControlbartouchListener_ACTION_MOVE_Y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingMenuEditorViewGroup;->ChangelayoutByGap(I)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
