.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;
.super Landroid/content/BroadcastReceiver;
.source "BrightnessControlGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 305
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 308
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 309
    const-string v2, "status"

    const/4 v3, 0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 310
    .local v1, "battStatus":I
    const-string v2, "level"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-gt v2, v6, :cond_1

    if-eq v1, v5, :cond_1

    .line 311
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->setButtonsDisable()V
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 316
    .end local v1    # "battStatus":I
    :cond_0
    :goto_0
    return-void

    .line 312
    .restart local v1    # "battStatus":I
    :cond_1
    const-string v2, "level"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-gt v2, v6, :cond_0

    if-ne v1, v5, :cond_0

    .line 313
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->setButtonsEnable()V
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    goto :goto_0
.end method
