.class public Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
.super Ljava/lang/Object;
.source "AMDATA.java"


# instance fields
.field _actname:Ljava/lang/String;

.field _amaction:Ljava/lang/String;

.field _amlabel:J

.field _amtitle:Ljava/lang/String;

.field _appname:Ljava/lang/String;

.field _id:J

.field _image:[B

.field state:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BI)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "app"    # Ljava/lang/String;
    .param p4, "act"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "label"    # J
    .param p8, "action"    # Ljava/lang/String;
    .param p9, "image"    # [B
    .param p10, "state"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-wide p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_id:J

    .line 23
    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_appname:Ljava/lang/String;

    .line 24
    iput-object p4, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_actname:Ljava/lang/String;

    .line 25
    iput-object p5, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amtitle:Ljava/lang/String;

    .line 26
    iput-wide p6, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amlabel:J

    .line 27
    iput-object p8, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amaction:Ljava/lang/String;

    .line 28
    iput-object p9, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_image:[B

    .line 29
    iput p10, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->state:I

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 110
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;

    .line 111
    .local v0, "compData":Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;
    const-string v2, "AMDATA"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Icon data:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getActName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v2, "AMDATA"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Icon data:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getActName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getActName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getActName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->getTitleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    :cond_0
    :goto_0
    return v5

    .line 124
    :catch_0
    move-exception v1

    .line 125
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 127
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v2

    goto :goto_0
.end method

.method public getActName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_actname:Ljava/lang/String;

    return-object v0
.end method

.method public getAmAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amaction:Ljava/lang/String;

    return-object v0
.end method

.method public getAmLblName()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amlabel:J

    return-wide v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_appname:Ljava/lang/String;

    return-object v0
.end method

.method public getDrawable()[B
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_image:[B

    return-object v0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_id:J

    return-wide v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->state:I

    return v0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amtitle:Ljava/lang/String;

    return-object v0
.end method

.method public setActName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_actname:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setAmAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amaction:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setAmLblName(J)V
    .locals 1
    .param p1, "name"    # J

    .prologue
    .line 77
    iput-wide p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amlabel:J

    .line 78
    return-void
.end method

.method public setAppName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_appname:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setDrawable([B)V
    .locals 0
    .param p1, "name"    # [B

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_image:[B

    .line 96
    return-void
.end method

.method public setID(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 39
    iput-wide p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_id:J

    .line 40
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->state:I

    .line 106
    return-void
.end method

.method public setTitleName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AMDATA;->_amtitle:Ljava/lang/String;

    .line 64
    return-void
.end method
