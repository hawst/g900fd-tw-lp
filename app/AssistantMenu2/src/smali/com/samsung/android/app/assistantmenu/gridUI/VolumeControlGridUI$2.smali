.class Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;
.super Ljava/lang/Object;
.source "VolumeControlGridUI.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 602
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 673
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 674
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 676
    return v3

    .line 606
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;Z)Z

    .line 608
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2$1;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 628
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mIsLongkeyProcessing:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$302(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;Z)Z

    .line 630
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_AUDIO_USE_SILENTMODE_BY_VOL_KEY"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 634
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->getRingerMode(Landroid/media/AudioManager;)I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 635
    const-string v0, "VolumeControlGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] VolumeDown Long Button - mCurStreamType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurStreamType:I
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeVal:I
    invoke-static {v0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$002(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 640
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v0, v4}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$502(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;I)I

    .line 643
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)Landroid/media/AudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->mCurVolumeState:I
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/VolumeManager;->setRingerMode(Landroid/media/AudioManager;I)V

    .line 652
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2$2;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/VolumeControlGridUI$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 602
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0037
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
