.class Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;
.super Ljava/lang/Object;
.source "SettingsMenuEditorActivity.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V
    .locals 0

    .prologue
    .line 495
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 509
    const-string v0, "SettingsMenuEditorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] onAnimationEnd!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mUpperDetectedItemId:I
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1800(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mIsTranslateAnimationEnd:Z
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1400(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # getter for: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->mSettingsMenuEditorHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$1200(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$9;->this$0:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    # invokes: Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->AfterTranslateAnimationEnd()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->access$100(Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;)V

    .line 515
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 505
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 500
    return-void
.end method
