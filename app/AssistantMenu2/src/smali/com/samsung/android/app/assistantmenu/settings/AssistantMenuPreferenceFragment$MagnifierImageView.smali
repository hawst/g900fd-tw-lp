.class public Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;
.super Landroid/preference/Preference;
.source "AssistantMenuPreferenceFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MagnifierImageView"
.end annotation


# instance fields
.field public currentMag:I

.field public img:Landroid/widget/ImageView;

.field mContext:Landroid/content/Context;

.field private mIcon:[I

.field private mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;Landroid/content/Context;I)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "setValue"    # I

    .prologue
    .line 558
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->this$0:Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;

    .line 559
    invoke-direct {p0, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 549
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->mIcon:[I

    .line 560
    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->mContext:Landroid/content/Context;

    .line 561
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->setLayoutResource(I)V

    .line 562
    iput p3, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->currentMag:I

    .line 563
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->setSelectable(Z)V

    .line 564
    return-void

    .line 549
    nop

    :array_0
    .array-data 4
        0x7f02013a
        0x7f02013b
        0x7f02013c
        0x7f02013d
        0x7f02013e
        0x7f02013f
        0x7f020140
        0x7f020141
    .end array-data
.end method


# virtual methods
.method public ImageChange(I)V
    .locals 3
    .param p1, "zoomvalue"    # I

    .prologue
    .line 580
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    .line 589
    :goto_0
    return-void

    .line 581
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 582
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 583
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const v1, 0x3f4ccccd    # 0.8f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 584
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 585
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->mIcon:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 587
    .end local v0    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->mIcon:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 573
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 574
    const v0, 0x7f0d0072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->img:Landroid/widget/ImageView;

    .line 575
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->currentMag:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->ImageChange(I)V

    .line 576
    const-string v0, "AssistantMenuPreferenceFragment"

    const-string v1, "Magnifier image"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-void
.end method

.method public setHoverZoomImageLevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 567
    iput p1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->currentMag:I

    .line 568
    iget v0, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->currentMag:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment$MagnifierImageView;->ImageChange(I)V

    .line 569
    return-void
.end method
