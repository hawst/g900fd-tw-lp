.class Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;
.super Ljava/lang/Object;
.source "ZoomControlGridUI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->ZoomControl(Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

.field final synthetic val$mode:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

.field final synthetic val$ratio:F


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V
    .locals 0

    .prologue
    .line 686
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->val$mode:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    iput p3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->val$ratio:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 688
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    iget-object v0, v0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mZoomPointLayoutParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mIVZoomPoint:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->val$mode:Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->val$ratio:F

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager;->Zoom(Landroid/view/WindowManager$LayoutParams;Landroid/view/View;Lcom/samsung/android/app/assistantmenu/serviceframework/ZoomManager$ZoomType;F)V

    .line 691
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;Z)Z

    .line 692
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteZoom:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mCompleteBounce:Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI$4;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->mSeekBarEventHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/ZoomControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 695
    :cond_0
    return-void
.end method
