.class public Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;
.super Ljava/lang/Object;
.source "SettingsUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SettingsUtil"

.field public static mIconImgMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static mIconStringMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static mNotPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mPrefsDefaltValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mShortImgMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mShortImgMap:Ljava/util/HashMap;

    .line 37
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020067

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020069

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020070

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020066

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :goto_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020072

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020075

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020074

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020073

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020076

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020077

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020092

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f020078

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->None:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0046

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a000c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a001a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a004a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a000b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0037

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->isGridSettings()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0042

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :goto_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a004f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0051

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a003b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0054

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a005d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0066

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0026

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a0038

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "0"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "1"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "2"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "3"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "4"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->FingerMouse:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "5"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "6"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->VolumeControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "7"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "8"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenCapture:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "9"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ZoomControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "10"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->DeviceOption:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "11"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "0"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->BrightnessControl:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "1"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "2"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->Restart:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mNotPrefsDefaltValue:Ljava/util/HashMap;

    const-string v1, "3"

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenRotate:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-void

    .line 46
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconImgMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f02006d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 69
    :cond_1
    sget-object v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mIconStringMap:Ljava/util/HashMap;

    sget-object v1, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressMenuKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    const v2, 0x7f0a003f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static AddOptionalMenuIfNeed(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    const/4 v0, 0x0

    .line 118
    .local v0, "bResult":Z
    const/4 v1, 0x0

    .line 120
    .local v1, "findHoverZoom":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 121
    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    .line 120
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 126
    :cond_1
    const-string v3, "com.sec.feature.overlaymagnifier"

    invoke-static {p0, v3}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v1, :cond_2

    .line 127
    sget-object v3, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/app/assistantmenu/settings/SettingsUtil;->mPrefsDefaltValue:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->HoverZoom:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const/4 v0, 0x1

    .line 129
    const-string v3, "SettingsUtil"

    const-string v4, "Added Act.HoverZoom"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :goto_1
    return v0

    .line 131
    :cond_2
    const-string v3, "SettingsUtil"

    const-string v4, "Already exist Act.HoverZoom"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static AddToShortcuts(ILjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "rowId"    # I
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "icon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 108
    return-void
.end method

.method public static DeleteShortcuts(I)V
    .locals 0
    .param p0, "rowId"    # I

    .prologue
    .line 113
    return-void
.end method

.method public static isGridSettings()Z
    .locals 2

    .prologue
    .line 141
    const-string v0, "ro.build.scafe"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "americano"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "j"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "h"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ks"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    const/4 v0, 0x0

    .line 146
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
