.class public Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
.super Ljava/lang/Object;
.source "LcdManager.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field protected static final AWAKE_INTERVAL_DEFAULT_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "AssistantMenuLCDmanager"

.field private static final TIMEOUT:I = 0x1

.field private static mKeyguardManager:Landroid/app/KeyguardManager;

.field private static mPM:Landroid/os/PowerManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static mWakelockSequence:I

.field private static sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;


# instance fields
.field private mWakeLockHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager$1;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    .line 63
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mPM:Landroid/os/PowerManager;

    .line 64
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 65
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mPM:Landroid/os/PowerManager;

    const v1, 0x1000001a

    const-string v2, "AssistantMenuService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 67
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->handleTimeout(I)V

    return-void
.end method

.method public static dismissKeyguard()V
    .locals 3

    .prologue
    .line 198
    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 200
    .local v1, "wm":Landroid/view/IWindowManager;
    :try_start_0
    invoke-interface {v1}, Landroid/view/IWindowManager;->dismissKeyguard()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const-class v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 53
    :cond_0
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleTimeout(I)V
    .locals 2
    .param p1, "seq"    # I

    .prologue
    .line 77
    monitor-enter p0

    .line 78
    :try_start_0
    const-string v0, "AssistantMenuLCDmanager"

    const-string v1, "handleTimeout"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakelockSequence:I

    if-ne p1, v0, :cond_0

    .line 81
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 83
    :cond_0
    monitor-exit p0

    .line 84
    return-void

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isKeyguardLocked()Z
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x1

    .line 194
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecureKeyguardLocked()Z
    .locals 2

    .prologue
    .line 179
    const/4 v0, 0x0

    .line 180
    .local v0, "bLockscreen":Z
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_0

    .line 181
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    const/4 v0, 0x1

    .line 185
    :cond_0
    return v0
.end method

.method public static isUnsecureKeyguardLocked()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "bLockscreen":Z
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_0

    .line 165
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    const/4 v0, 0x1

    .line 169
    :cond_0
    return v0
.end method


# virtual methods
.method public finalize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 148
    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mPM:Landroid/os/PowerManager;

    .line 149
    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 150
    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 152
    sput-object v0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->sLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    .line 153
    return-void
.end method

.method public pokeWakelock()V
    .locals 1

    .prologue
    .line 119
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock(I)V

    .line 120
    return-void
.end method

.method public pokeWakelock(I)V
    .locals 5
    .param p1, "holdMs"    # I

    .prologue
    .line 129
    monitor-enter p0

    .line 130
    :try_start_0
    const-string v1, "AssistantMenuLCDmanager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] pokeWakelock("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    sget-object v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 134
    sget v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakelockSequence:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakelockSequence:I

    .line 135
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    sget v3, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakelockSequence:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 136
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 137
    monitor-exit p0

    .line 138
    return-void

    .line 137
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
