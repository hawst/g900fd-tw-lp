.class public Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;
.super Landroid/preference/PreferenceActivity;
.source "AssistantMenu.java"


# static fields
.field public static final KEY_ACCESSIBILITY_SETTINGS_BACK:Ljava/lang/String; = "ACCESSIBILITY_SETTINGS_BACK"


# instance fields
.field extra:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected isValidFragment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fragmentName"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 65
    return-void
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/preference/PreferenceActivity$Header;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "target":Ljava/util/List;, "Ljava/util/List<Landroid/preference/PreferenceActivity$Header;>;"
    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->loadHeadersFromResource(ILjava/util/List;)V

    .line 36
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 21
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ACCESSIBILITY_SETTINGS_BACK"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->extra:Z

    .line 24
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->extra:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 25
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenu;->finish()V

    .line 49
    const/4 v0, 0x1

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
