.class Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "FingerMouseGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FingerMouseTouchGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V
    .locals 0

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    .param p2, "x1"    # Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$1;

    .prologue
    .line 1066
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 1132
    const-string v0, "FingerMouseGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDoubleTap x:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " y:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDouble:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    .line 1134
    return v3
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 1141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1162
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1144
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1145
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1146
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDoubleTap:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    goto :goto_0

    .line 1153
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1155
    const-string v0, "FingerMouseGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDoubleTapEvent x up:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " y:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mDoubleTap:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1102(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    goto :goto_0

    .line 1141
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->update(FF)V

    .line 1072
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1079
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1096
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1105
    :goto_0
    return-void

    .line 1098
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1099
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;F)F

    .line 1100
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mLongPress:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;Z)Z

    goto :goto_0

    .line 1096
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1086
    neg-float p3, p3

    .line 1087
    neg-float p4, p4

    .line 1088
    const-string v0, "FingerMouseGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onScroll   mCursorX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$400(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCursorY:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1089
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    invoke-virtual {v0, p3, p4}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->updateCursorFromGesture(FF)V

    .line 1091
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1110
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 1111
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onSingleTapConfirmed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorOnTop()Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3700(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowQuickPanel(Landroid/content/Context;)V

    .line 1119
    :goto_0
    return v2

    .line 1114
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->isCursorBottom()Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3800(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1115
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->closeStatusBarPanel()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$3900(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;)V

    goto :goto_0

    .line 1117
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI$FingerMouseTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->injectAccessibilityMotionEvent(IZ)V
    invoke-static {v0, v2, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;IZ)V

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1125
    const-string v0, "FingerMouseGridUI"

    const-string v1, "onSingleTapUp"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
