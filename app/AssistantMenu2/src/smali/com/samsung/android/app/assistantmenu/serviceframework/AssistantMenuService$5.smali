.class Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;
.super Ljava/lang/Object;
.source "AssistantMenuService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)V
    .locals 0

    .prologue
    .line 693
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 6
    .param p1, "prefs"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 702
    const-string v2, "AssistantMenuService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] onSharedPreferenceChanged:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const-string v2, "dominant_hand_type"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 706
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v4, "dominant_hand_type"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandMode:I
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$3102(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;I)I

    .line 711
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 712
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mCornertabUI:Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$000(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/AssistantMenuCornertabUI;->ChangeHandMode()V

    .line 714
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$2200(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 716
    :cond_1
    const-string v2, "pointer_speed"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 717
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v3, "pointer_speed"

    const/4 v4, 0x2

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 722
    .local v1, "speed":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 723
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changePointerSpeed(I)V

    .line 724
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 725
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changePointerSpeed(I)V

    .line 728
    .end local v1    # "speed":I
    :cond_3
    const-string v2, "pointer_size"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 729
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v3, "pointer_size"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 734
    .local v0, "size":I
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 735
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changePointerSize(I)V

    .line 736
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 737
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changePointerSize(I)V

    .line 740
    .end local v0    # "size":I
    :cond_5
    const-string v2, "pad_size"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 741
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    const-string v3, "pad_size"

    invoke-static {v2, v3, v5}, Lcom/samsung/android/app/assistantmenu/settings/AssistantMenuPreferenceFragment;->getSharedPreference(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 746
    .restart local v0    # "size":I
    const-string v2, "AssistantMenuService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] onSharedPreferenceChanged padsize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 748
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->changePadSize(I)V

    .line 749
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->RemoveView()V

    .line 750
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mFingerMouseGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$600(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/FingerMouseGridUI;->ShowView()V

    .line 752
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 753
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->changePadSize(I)V

    .line 754
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->RemoveView()V

    .line 755
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService$5;->this$0:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->mHoverZoomGridUI:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->access$700(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;)Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->ShowView()V

    .line 761
    .end local v0    # "size":I
    :cond_7
    :goto_0
    return-void

    .line 759
    :cond_8
    const-string v2, "AssistantMenuService"

    const-string v3, "[c] onSharedPreferenceChanged - not operated!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
