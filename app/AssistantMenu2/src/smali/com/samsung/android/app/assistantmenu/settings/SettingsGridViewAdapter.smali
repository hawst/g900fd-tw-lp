.class public Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingsGridViewAdapter.java"


# static fields
.field private static mIsLongClick:Z


# instance fields
.field private final EVENT_LONG_PRESS:I

.field private final TAG:Ljava/lang/String;

.field private mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

.field private final mHandler:Landroid/os/Handler;

.field private mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItemArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLayout:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mIsLongClick:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p4, "imageMode"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;",
            ">;",
            "Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    .local p3, "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    const-string v0, "SettingsGridViewAdapter"

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->TAG:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->EVENT_LONG_PRESS:I

    .line 45
    new-instance v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter$1;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mHandler:Landroid/os/Handler;

    move-object v0, p1

    .line 69
    check-cast v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    .line 70
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 71
    iput-object p3, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    .line 72
    iput p2, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mLayout:I

    .line 73
    iput-object p4, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;)Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;)Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mIsLongClick:Z

    return v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mIsLongClick:Z

    return p0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 86
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    const v9, 0x7f0d0081

    .line 92
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt p1, v5, :cond_0

    .line 93
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mLayout:I

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 201
    :goto_0
    return-object v5

    .line 96
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getAct()Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    move-result-object v1

    .line 98
    .local v1, "itemAct":Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mLayout:I

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 100
    const v5, 0x7f0d0082

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 101
    .local v0, "imageView":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getIcon()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 103
    const v5, 0x7f0d0083

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 105
    .local v2, "subImageView":Landroid/widget/ImageView;
    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->SettingEnter:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v1, v5}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 107
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->PLUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    if-ne v5, v6, :cond_6

    .line 108
    const v5, 0x7f020064

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    :cond_1
    :goto_1
    const v5, 0x7f0d0084

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 117
    .local v3, "textView":Landroid/widget/TextView;
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v6

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 118
    .local v4, "width":F
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090036

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_2

    .line 120
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    iget-object v7, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v7}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090041

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->pixelToDp(Landroid/content/Context;F)F

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 126
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mItemArray:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewItem;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetIsWidgetVisible()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 131
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->MINUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetUpperDetectedItemId()I

    move-result v5

    if-eq v5, p1, :cond_4

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->PLUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mActivity:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;

    invoke-virtual {v5}, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity;->GetLowerDetectedItemId()I

    move-result v5

    if-ne v5, p1, :cond_5

    .line 135
    :cond_4
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, -0x333334

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 136
    invoke-virtual {p2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setAlpha(F)V

    .line 140
    :cond_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 142
    new-instance v5, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter$2;-><init>(Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;)V

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    move-object v5, p2

    .line 201
    goto/16 :goto_0

    .line 109
    .end local v3    # "textView":Landroid/widget/TextView;
    .end local v4    # "width":F
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/settings/SettingsGridViewAdapter;->mImageMode:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    sget-object v6, Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;->MINUS_IMAGE_MODE:Lcom/samsung/android/app/assistantmenu/settings/SettingsMenuEditorActivity$IMAGE_MODE;

    if-ne v5, v6, :cond_7

    .line 110
    const v5, 0x7f020065

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 112
    :cond_7
    const-string v5, "SettingsGridViewAdapter"

    const-string v6, "getView"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
