.class Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HoverZoomGridUI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HoverZoomTouchGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;
    .param p2, "x1"    # Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$1;

    .prologue
    .line 1006
    invoke-direct {p0, p1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;-><init>(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 1072
    const-string v0, "HoverZoomGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDoubleTap x:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " y:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDouble:Z
    invoke-static {v0, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1602(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    .line 1074
    return v3
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 1081
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1102
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 1084
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1085
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1086
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    goto :goto_0

    .line 1093
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1094
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1095
    const-string v0, "HoverZoomGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDoubleTapEvent x up:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " y:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mDoubleTap:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1202(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    goto :goto_0

    .line 1081
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->update(FF)V

    .line 1012
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1019
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1036
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1045
    :goto_0
    return-void

    .line 1038
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursX:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1402(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1039
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->dblCursY:F
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1502(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;F)F

    .line 1040
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mLongPress:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1302(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;Z)Z

    goto :goto_0

    .line 1036
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1026
    neg-float p3, p3

    .line 1027
    neg-float p4, p4

    .line 1028
    const-string v0, "HoverZoomGridUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onScroll   mCursorX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorX:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCursorY:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mCursorY:F
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1029
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    invoke-virtual {v0, p3, p4}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->updateCursorFromGesture(FF)V

    .line 1031
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1050
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    .line 1051
    const-string v0, "HoverZoomGridUI"

    const-string v1, "onSingleTapConfirmed"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorOnTop()Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->ShowQuickPanel(Landroid/content/Context;)V

    .line 1059
    :goto_0
    return v2

    .line 1054
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->isCursorBottom()Z
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3800(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetQuickPanel()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1055
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->closeStatusBarPanel()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$3900(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;)V

    goto :goto_0

    .line 1057
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI$HoverZoomTouchGestureListener;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->injectAccessibilityMotionEvent(IZ)V
    invoke-static {v0, v2, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;->access$1700(Lcom/samsung/android/app/assistantmenu/gridUI/HoverZoomGridUI;IZ)V

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1065
    const-string v0, "HoverZoomGridUI"

    const-string v1, "onSingleTapUp"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
