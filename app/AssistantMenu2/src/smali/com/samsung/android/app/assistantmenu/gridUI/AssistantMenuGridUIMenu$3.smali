.class Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;
.super Ljava/lang/Object;
.source "AssistantMenuGridUIMenu.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->MakeButton(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;I)Landroid/widget/RelativeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field downX:I

.field downY:I

.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

.field final synthetic val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    .locals 0

    .prologue
    .line 1002
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v7, 0x7f090020

    const/4 v6, 0x0

    .line 1008
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1101
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] mMenuButton: onTouch() default"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1105
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return v6

    .line 1011
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_0
    :try_start_0
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] mMenuButton: onTouch() ACTION_DOWN"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$500(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 1014
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 1016
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downX:I

    .line 1017
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downY:I

    .line 1018
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x384

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1021
    :catch_0
    move-exception v0

    .line 1022
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0

    .line 1027
    .end local v0    # "e":Ljava/lang/IndexOutOfBoundsException;
    :pswitch_1
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] mMenuButton: onTouch() ACTION_MOVE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downX:I

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downY:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(IIII)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-le v2, v3, :cond_0

    .line 1031
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 1036
    :pswitch_2
    const-string v2, "AssistantMenuGridUIMenu"

    const-string v3, "[c] mMenuButton: onTouch() ACTION_UP"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressHomeKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PressBackKey:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->RecentappList:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ScreenLock:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->PowerOff:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-eq v2, v3, :cond_0

    .line 1043
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$700(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1045
    iget v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downX:I

    iget v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->downY:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/app/assistantmenu/util/DisplayUtil;->GetDistance(IIII)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mResource:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$800(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 1060
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$000(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Landroid/media/AudioManager;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1063
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v2, v3, :cond_4

    move-object v2, p1

    .line 1064
    check-cast v2, Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v4

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v4, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1069
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->NotificationPanel:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto/16 :goto_0

    .line 1070
    :cond_2
    check-cast p1, Landroid/widget/ImageButton;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v4

    sget-object v2, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->actStringResourceMap:Ljava/util/HashMap;

    sget-object v5, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v4, v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1075
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->QuickSettings:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto/16 :goto_0

    .line 1077
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto/16 :goto_0

    .line 1079
    .restart local p1    # "view":Landroid/view/View;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    sget-object v3, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;->ShortCut:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    if-ne v2, v3, :cond_6

    .line 1080
    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    check-cast p1, Landroid/widget/ImageButton;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/widget/ImageButton;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$902(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Ljava/lang/String;)Ljava/lang/String;

    .line 1082
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1083
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;->GetTopClass()Ljava/lang/String;

    move-result-object v1

    .line 1084
    .local v1, "topClasser":Ljava/lang/String;
    const-string v2, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] mMenuButton: onTouch() default"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1085
    const-string v2, "AssistantMenuGridUIMenu"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[c] mMenuButton: onTouch() topclass"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 1087
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->dbHandler:Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$1000(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->currIntentAction:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$900(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/app/assistantmenu/serviceframework/DatabaseHandler;->getTopClassName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1088
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->launchBroadcastToApps(Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$1100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto/16 :goto_0

    .line 1090
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    const v3, 0x7f0a0024

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1094
    .end local v1    # "topClasser":Ljava/lang/String;
    .restart local p1    # "view":Landroid/view/View;
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->mService:Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;
    invoke-static {v2}, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;->access$100(Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu;)Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/assistantmenu/gridUI/AssistantMenuGridUIMenu$3;->val$act:Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;

    invoke-static {v2, v3}, Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct;->invokeAct(Lcom/samsung/android/app/assistantmenu/serviceframework/AssistantMenuService;Lcom/samsung/android/app/assistantmenu/AssistantMenuUIAct$Act;)V

    goto/16 :goto_0

    .line 1008
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
