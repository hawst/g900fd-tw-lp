.class Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;
.super Ljava/lang/Object;
.source "BrightnessControlGridUI.java"

# interfaces
.implements Lcom/samsung/android/app/assistantmenu/gridUI/CustomSeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V
    .locals 0

    .prologue
    .line 893
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public CustomSeekBarProgressChanged()V
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    iget-object v1, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mAutoSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$200(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    # setter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mCurBrightnessVal:I
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$102(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;I)I

    .line 896
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetBrightness(Z)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;Z)V

    .line 898
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # invokes: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->SetAutoBrightnessLevel()V
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1600(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)V

    .line 899
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 900
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 901
    return-void
.end method

.method public CustomeSeekBarStopTrackingTouch()V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mLcdManager:Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1400(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/assistantmenu/serviceframework/LcdManager;->pokeWakelock()V

    .line 905
    iget-object v0, p0, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI$5;->this$0:Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;

    # getter for: Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->mParentSvcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;->access$1500(Lcom/samsung/android/app/assistantmenu/gridUI/BrightnessControlGridUI;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/assistantmenu/util/AutoExitView;->StartAutoExit(Landroid/os/Handler;)V

    .line 906
    return-void
.end method
