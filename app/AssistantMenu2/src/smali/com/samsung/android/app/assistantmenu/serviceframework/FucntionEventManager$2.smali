.class final Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;
.super Ljava/lang/Object;
.source "FucntionEventManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->takeScreenshot(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$sScreenshotTimeout:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;->val$sScreenshotTimeout:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 8
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 152
    const-string v5, "FunctionEventManager"

    const-string v6, "[c] onServiceConnected()+"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotLock:Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$000()Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 155
    :try_start_0
    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$100()Landroid/content/ServiceConnection;

    move-result-object v5

    if-eq v5, p0, :cond_0

    .line 156
    monitor-exit v6

    .line 187
    :goto_0
    return-void

    .line 158
    :cond_0
    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 159
    .local v2, "messenger":Landroid/os/Messenger;
    const/4 v5, 0x0

    const/4 v7, 0x1

    invoke-static {v5, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 160
    .local v3, "msg":Landroid/os/Message;
    move-object v4, p0

    .line 162
    .local v4, "myConn":Landroid/content/ServiceConnection;
    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;

    # getter for: Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->sScreenshotHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager;->access$200()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v1, p0, v5, v4}, Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2$1;-><init>(Lcom/samsung/android/app/assistantmenu/serviceframework/FucntionEventManager$2;Landroid/os/Looper;Landroid/content/ServiceConnection;)V

    .line 179
    .local v1, "h":Landroid/os/Handler;
    new-instance v5, Landroid/os/Messenger;

    invoke-direct {v5, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v5, v3, Landroid/os/Message;->replyTo:Landroid/os/Messenger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    :try_start_1
    invoke-virtual {v2, v3}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :goto_1
    :try_start_2
    monitor-exit v6

    goto :goto_0

    .end local v1    # "h":Landroid/os/Handler;
    .end local v2    # "messenger":Landroid/os/Messenger;
    .end local v3    # "msg":Landroid/os/Message;
    .end local v4    # "myConn":Landroid/content/ServiceConnection;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 183
    .restart local v1    # "h":Landroid/os/Handler;
    .restart local v2    # "messenger":Landroid/os/Messenger;
    .restart local v3    # "msg":Landroid/os/Message;
    .restart local v4    # "myConn":Landroid/content/ServiceConnection;
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 191
    const-string v0, "FunctionEventManager"

    const-string v1, "[c] onServiceDisconnected()+"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    return-void
.end method
