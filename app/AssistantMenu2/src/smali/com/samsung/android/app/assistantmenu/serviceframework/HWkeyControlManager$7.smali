.class final Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$7;
.super Ljava/lang/Object;
.source "HWkeyControlManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;->backButtonLongTapEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 141
    :try_start_0
    new-instance v1, Landroid/view/KeyEvent;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-direct {v1, v3, v4}, Landroid/view/KeyEvent;-><init>(II)V

    .line 142
    .local v1, "event":Landroid/view/KeyEvent;
    new-instance v2, Landroid/app/Instrumentation;

    invoke-direct {v2}, Landroid/app/Instrumentation;-><init>()V

    .line 143
    .local v2, "ins":Landroid/app/Instrumentation;
    invoke-virtual {v2, v1}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 144
    const-wide/16 v4, 0x3e8

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 145
    new-instance v1, Landroid/view/KeyEvent;

    .end local v1    # "event":Landroid/view/KeyEvent;
    const/4 v3, 0x1

    const/4 v4, 0x4

    invoke-direct {v1, v3, v4}, Landroid/view/KeyEvent;-><init>(II)V

    .line 146
    .restart local v1    # "event":Landroid/view/KeyEvent;
    invoke-virtual {v2, v1}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    .line 147
    const-string v3, "HWkeyControlManager"

    const-string v4, "long back button!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v1    # "event":Landroid/view/KeyEvent;
    .end local v2    # "ins":Landroid/app/Instrumentation;
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
