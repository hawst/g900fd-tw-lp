.class public Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager;
.super Ljava/lang/Object;
.source "HWkeyControlManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HWkeyControlManager"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static backButtonLongTapEvent()V
    .locals 2

    .prologue
    .line 138
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$7;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$7;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 153
    return-void
.end method

.method public static backButtonTapEvent()V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$6;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$6;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 129
    return-void
.end method

.method public static homeButtonLongTapEvent()V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$5;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$5;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 110
    return-void
.end method

.method public static homeButtonTapEvent()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$4;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$4;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 91
    return-void
.end method

.method public static lockButtonLongTapEvent()V
    .locals 2

    .prologue
    .line 183
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$9;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$9;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method

.method public static lockButtonTapEvent()V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$8;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$8;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 174
    return-void
.end method

.method public static menuButtonLongTapEvent()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$2;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$2;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 53
    return-void
.end method

.method public static menuButtonLongTapEventForFindo()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$3;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$3;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 72
    return-void
.end method

.method public static menuButtonTapEvent()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$1;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 34
    return-void
.end method

.method public static recentButtonTapEvent()V
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$10;

    invoke-direct {v1}, Lcom/samsung/android/app/assistantmenu/serviceframework/HWkeyControlManager$10;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 230
    return-void
.end method
